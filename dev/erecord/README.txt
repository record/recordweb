=======================================================
erecord, web services for vle models
=======================================================

The erecord project provides some web services for models developed with Vle
(several Vle versions), such as the Record platform ones.

Content hierarchy : docs, repositories, erecord (the erecord package),
databases, factory...

See __init__.py files, docs directory.

