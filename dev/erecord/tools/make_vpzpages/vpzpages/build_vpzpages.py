"""build_vpzpages

Builds html home pages for simulators of erecord

If folder sys.argv[1] exists then html pages are generated into it under
gen and private subdirectories, else generated_gen and generated_private
folders are created and html pages are generated into them.

run : see "cleanvpzpages" and "genvpzpages" rules into ../Makefile

"""

import os
import sys
import uuid

apps_home = os.path.normpath(os.path.join("..", "..", "erecord", "apps"))

if "erecordroot" in os.environ.keys() :
    erecordroot = os.environ["erecordroot"]
    apps_home = os.path.normpath(os.path.join(erecordroot,
                                              "erecord", "erecord", "apps"))
if apps_home not in sys.path :
    sys.path.insert(0, apps_home)

from erecord_cmn.utils.dir_and_file import clean_dir
from erecord_cmn.utils.dir_and_file import create_dir_if_absent
from erecord_cmn.utils.using.send_get_and_receive import send_get_and_receive
from erecord_cmn.utils.using.send_post_and_receive import send_post_and_receive

base_url = "http://erecord.toulouse.inra.fr:8000/"
#base_url = "http://127.0.0.1:8000/"
#base_url = "http://147.100.179.168:8000/"

###############################################################################
# in relation with db/vpz resource

def content_vpz_tree(vlevpz):
    """ Content of the simulator (vlevpz) in 'tree' style of presentation

    Returns used information
    """

    #vlevpz.keys()
    vpz_id = vlevpz['id']
    vpz_name = vlevpz['name']
    vpz_verbose_name = vlevpz['verbose_name']
    vlepkg = vlevpz['vlepkg']
    pkg_name = vlepkg['name']
    pkg_id = vlepkg['id']
    vlerep = vlepkg['vlerep']
    rep_name = vlerep['name']
    rep_id = vlerep['id']
    vleversion = vlerep['vleversion']
    version_name = vleversion['name']
    version_id = vleversion['id']

    comment= "\n"
    comment=comment+ 'Simulateur : ' + "\n"
    comment=comment+ '  - name : ' + vpz_name + "\n"
    comment=comment+ '  - verbose_name : ' + vlevpz['verbose_name'] + "\n"
    comment=comment+ 'Modele : ' + "\n"
    comment=comment+ '  - name : ' + pkg_name + "\n"
    comment=comment+ '  - verbose_name : ' + vlepkg['verbose_name'] + "\n"
    comment=comment+ 'Depot de modeles : ' + "\n"
    comment=comment+ '  - name : ' + rep_name + "\n"
    comment=comment+ '  - verbose_name : ' + vlerep['verbose_name'] + "\n"
    comment=comment+ 'Version vle : ' + "\n"
    comment=comment+ '  - name : ' + version_name + "\n"
    comment=comment+ '  - verbose_name : ' + vleversion['verbose_name'] + "\n"
    comment=comment+ "\n"

    return (version_name, rep_name, pkg_name, vpz_name,
            version_id, rep_id, pkg_id, vpz_id, comment)

###############################################################################
# in relation with vpz/input resource

def content_simulation_inputs_tree(vpzinput):
    """ Content of simulation input information (vpzinput) in 'tree' style of
    presentation

    Returns used information
    """

    keys = vpzinput.keys()

    # default
    vleduration = None
    vlebegin = None
    conds = None
    views = None

    # duration
    if "vleduration" in keys :
        vleduration = vpzinput['vleduration']
        #duration_value = vleduration['value']
        #duration_verbose_name = vleduration['verbose_name']
        #duration_id = vleduration['id']
        #print("duration value : ", duration_value)

    # begin
    if "vlebegin" in keys :
        vlebegin = vpzinput['vlebegin']
        #begin_value = vlebegin['value']
        #begin_verbose_name = vlebegin['verbose_name']
        #begin_id = vlebegin['id']
        #print("begin value : ", begin_value)

    # conditions and parameters
    if "vlecond_list" in keys :
        conds = vpzinput['vlecond_list']
        #for cond in conds :
            ##cond.keys()
            #cond_verbose_name = cond['verbose_name']
            #cond_id = cond['id']
            #cond_name = cond['name']
            #print("\nCondition name ", cond_name)
            #print("List of its parameters (id, cname, pname, type, value, selected, verbose_name) :")
            #pars = cond['vlepar_list']
            #for par in pars :
                ##par.keys()
                #par_verbose_name = par['verbose_name']
                #par_id = par['id']
                #par_cname = par['cname']
                #par_pname = par['pname']
                #par_type = par['type']
                #par_value = json.loads(par['value'])
                #par_selected = par['selected']
                #print("- ", par_id, par_cname, par_pname, par_type, par_value, par_selected, par_verbose_name)
                ##type(par_value)

    # views and output datas identity
    if "vleview_list" in keys :
        views = vpzinput['vleview_list']
        #for view in views :
            ##view.keys()
            #view_verbose_name = view['verbose_name']
            #view_id = view['id']
            #view_name = view['name']
            #view_type = view['type']
            #view_timestep = view['timestep']
            #view_output_format = view['output_format']
            #view_output_location = view['output_location']
            #view_output_name = view['output_name']
            #view_output_plugin = view['output_plugin']
            #print("\nView name ", view_name)
            #print("List of its output datas (id, vname, oname, shortname, selected, verbose_name) :")
            #outs = view['vleout_list']
            #for out in outs :
                ##out.keys()
                #out_verbose_name = out['verbose_name']
                #out_id = out['id']
                #out_vname = out['vname']
                #out_oname = out['oname']
                #out_shortname = out['shortname']
                #out_selected = out['selected']
                #print("- ", out_id, out_vname, out_oname, out_shortname, out_selected, out_verbose_name)
    return (vleduration, vlebegin, conds, views)

###############################################################################
# misc

def get_home_file_name(homepath, version_name, rep_name, pkg_name, vpz_name,
                       to_create=True) :

    version_name = version_name.replace("/","__")
    rep_name = rep_name.replace("/","__")
    pkg_name = pkg_name.replace("/","__")
    vpz_name = vpz_name.replace("/","__")
    versionpath = os.path.join(homepath, version_name)
    reppath = os.path.join(versionpath, rep_name)
    pkgpath = os.path.join(reppath, pkg_name)
    vpzpath = os.path.join(pkgpath, vpz_name)
    if to_create :
        create_dir_if_absent(versionpath)
        create_dir_if_absent(reppath)
        create_dir_if_absent(pkgpath)
        create_dir_if_absent(vpzpath)
    file_name = "home.html"
    file_name = os.path.join(vpzpath, file_name)
    return file_name

def get_full_name(version_name, rep_name, pkg_name, vpz_name) :
    """Returns full (unique) name of a simulator """

    return (version_name +' / '+rep_name +' / '+pkg_name+' / '+ vpz_name)

###############################################################################
# html code text

def get_txt(csrf_token_value, access,
            vleduration, vlebegin, conds, views, pkg_id) :
    """ Builds and returns html code text

    access values : "public", "limited"
    """

    txt=     '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">' + "\n"
    txt=txt+ '<html xmlns="http://www.w3.org/1999/xhtml">' + "\n"
    txt=txt+ '' + "\n"
    txt=txt+ '<head>' + "\n"
    txt=txt+ '' + "\n"
    txt=txt+ '<meta http-equiv="Content-Type" content="text/xhtml;charset=UTF-8"/>' + "\n"
    txt=txt+ '<title>' + full_name + ' by erecord</title>' + "\n"
    txt=txt+ '<link href="tabs.css" rel="stylesheet" type="text/css"/>' + "\n"
    txt=txt+ '<link href="doxygen.css" rel="stylesheet" type="text/css"/>' + "\n"
    txt=txt+ '' + "\n"
    txt=txt+ '<style type="text/css">' + "\n"
    txt=txt+ '    body,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Times New Roman"; font-size:medium }' + "\n"
    txt=txt+ '</style>' + "\n"
    txt=txt+ '' + "\n"
    txt=txt+ '<style>' + "\n"
    txt=txt+ '  table { border-collapse: collapse; }' + "\n"
    txt=txt+ '  table, td, th { border: 1px solid black; text-align: left; font-weight: normal; padding: 8px; }' + "\n"
    txt=txt+ '  p.instructions, div.instructions { color: magenta; font-size : small; font-style : italic; }' + "\n"
    txt=txt+ '  p.foruser { color: blue; font-style : italic; }' + "\n"
    txt=txt+ '</style>' + "\n"
    txt=txt+ '' + "\n"
    txt=txt+ '</head>' + "\n"
    txt=txt+ '<body>' + "\n"
    txt=txt+ '' + "\n"
    txt=txt+ '<script>' + "\n"
    txt=txt+ '' + "\n"

    txt=txt+ '  // csrf_token' + "\n"
    txt=txt+ '  var csrf_token_value = "'+ csrf_token_value +'";' + "\n"
    txt=txt+ '  document.cookie = "csrf_token=' + csrf_token_value + '";' + "\n"
    input_txt = '\'<input name="csrf_token" value="'+csrf_token_value+'" />\''
    txt=txt+ '  document.getElementById("csrf_token").innerHTML = ' + input_txt + ';' + "\n"
    txt=txt+ '' + "\n"

    txt=txt+ '  function toggle_visibility(id)' + "\n"
    txt=txt+ '  { // to show/hide element id' + "\n"
    txt=txt+ '    var e = document.getElementById(id);' + "\n"
    txt=txt+ '    if (e!=null){' + "\n"
    txt=txt+ '        if (e.style.display == \'block\'){ e.style.display = \'none\';' + "\n"
    txt=txt+ '        } else { e.style.display = \'block\'; }' + "\n"
    txt=txt+ '    }' + "\n"
    txt=txt+ '  }' + "\n"
    txt=txt+ '' + "\n"
    txt=txt+ '</script>' + "\n"
    txt=txt+ '' + "\n"
    txt=txt+ '<div style="background-color:LightYellow">' + "\n"
    txt=txt+ '' + "\n"

    txt=txt+ '  <h1 id="model" align=center>Simulateur ' + vpz_name + ' </h1>' + "\n"
    txt=txt+ '  <p align=center><i> - Simulateur ' + vpz_name + ' du modele ' + pkg_name + ' du depot de modeles ' + rep_name + ' de la version vle ' + version_name + ' - </i></p>' + "\n"
    txt=txt+ '' + "\n"

    txt=txt+ '  <h2>Modification et simulation</h2>' + "\n"
    txt=txt+ '' + "\n"
    txt=txt+ '  <form action="'+base_url+'vpz/output/" method="post" enctype="multipart/form-data" target=\'_blank\' >' + "\n"
    txt=txt+ '' + "\n"
    txt=txt+ '    <p class="foruser">Vous pouvez <input type="submit" value="simuler" /> ' + vpz_name + ' apres l\'avoir modifie ci-dessous, si vous le souhaitez.</p>' + "\n"

    if access != "public" :
        txt=txt+ '' + "\n"
        txt=txt+ '    <p class="foruser">Au prealable vous devez vous identifier par token : <input name="jwt" value="" /> <i>(simulateur en acces limite)</i> </p>' + "\n"

    txt=txt+ '    <p class="foruser">Aide :<br>' + "\n"

    if access != "public" :
        txt=txt+ '    <a href="'+base_url+'acs/jwt/obtain/" target="_blank"><span style="color:red;"><b>to get a token</b></span></a><br>' + "\n"

    txt=txt+ '    <a href=http://aa.usno.navy.mil/data/docs/JulianDate.php target="_blank">Julian Date Converter</a> (pour dates : valeur de begin...) <br>' + "\n"

    url_datalist = base_url + "db/pkg/" +str(pkg_id)+ "/datalist"
    url_datalist_json = url_datalist + "/?format=json"
    url_datalist_yaml = url_datalist + "/?format=yaml"
    txt=txt+ '    <a href=' + url_datalist + ' target="_blank">Fichiers de donnees disponibles par defaut pour le simulateur</a> (<a href=' + url_datalist_json + ' target="_blank">json</a> , <a href=' + url_datalist_yaml + ' target="_blank">yaml</a>)' + "\n"
    txt=txt+ '    </p>' + "\n"
    
    txt=txt+ '' + "\n"
    txt=txt+ '    <div style="background-color:MintCream">' + "\n"
    txt=txt+ '    <br>' + "\n"
    txt=txt+ '    <h3>Informations generales</h3>' + "\n"
    txt=txt+ '' + "\n"
    txt=txt+ '    <table cellspacing="0" border="0">' + "\n"
    txt=txt+ '      <colgroup width="312"></colgroup>' + "\n"
    txt=txt+ '      <colgroup width="161"></colgroup>' + "\n"
    txt=txt+ '' + "\n"

    is_empty = True # default

    if vleduration is not None :
        is_empty = False
        txt=txt+ '  <tr>' + "\n"
        txt=txt+ '    <td height="20" align="left"><b>duration</b></td>' + "\n"
        txt=txt+ '    <td align="right" sdval="5365" sdnum="1036;">' + "\n"
        txt=txt+ '      <input type="number" name="duration" value='+str(vleduration['value'])+' >' + "\n"
        txt=txt+ '    </td>' + "\n"
        txt=txt+ '  </tr>' + "\n"
    
    if vlebegin is not None :
        is_empty = False
        txt=txt+ '  <tr>' + "\n"
        txt=txt+ '    <td height="20" align="left"><b>begin</b></td>' + "\n"
        txt=txt+ '    <td align="right" sdval="2454024" sdnum="1036;">' + "\n"
        txt=txt+ '      <input type="number" name="begin" value='+ str(vlebegin['value'])+' >' + "\n"
        txt=txt+ '    </td>' + "\n"
        txt=txt+ '  </tr>' + "\n"

    # plan
    txt=txt+ '  <tr>' + "\n"
    txt=txt+ '    <td height="20" align="left"><b>type de simulation</b></td>' + "\n"
    txt=txt+ '    <td align="right" sdval="2454024" sdnum="1036;">' + "\n"
    txt=txt+ '      <select name="plan">' + "\n"
    txt=txt+ '        <option value="single">simulation simple (plan=single)</option>' + "\n"
    txt=txt+ '        <option value="linear">simulation multiple (plan=linear)</option>' + "\n"
    txt=txt+ '      </select>' + "\n"
    txt=txt+ '    </td>' + "\n"
    txt=txt+ '  </tr>' + "\n"
    is_empty = False

    # datafolder
    txt=txt+ '  <tr>' + "\n"
    txt=txt+ '    <td height="20" align="left"><b>dossier des donnees d\'entree (optionnel)</b></td>' + "\n"
    txt=txt+ '    <td align="right" sdval="2454024" sdnum="1036;">' + "\n"
    txt=txt+ '      <input name="datafolder" accept=".zip" type="file">' + "\n"
    txt=txt+ '    </td>' + "\n"
    txt=txt+ '  </tr>' + "\n"
    is_empty = False

    # datafoldercopy
    txt=txt+ '  <tr>' + "\n"
    txt=txt+ '    <td height="20" align="left"><b>forme de prise en compte du dossier des donnees d\'entree</b></td>' + "\n"
    txt=txt+ '    <td align="right" sdval="2454024" sdnum="1036;">' + "\n"
    txt=txt+ '      <select name="datafoldercopy">' + "\n"
    txt=txt+ '        <option value="overwrite">ajout au dossier original (datafoldercopy=overwrite)</option>' + "\n"
    txt=txt+ '        <option value="replace">remplacement du dossier original (datafoldercopy=replace)</option>' + "\n"
    txt=txt+ '      </select>' + "\n"
    txt=txt+ '    </td>' + "\n"
    txt=txt+ '  </tr>' + "\n"
    is_empty = False

    txt=txt+ '    </table>' + "\n"
    txt=txt+ '' + "\n"

    if is_empty :
        txt=txt+ '' + "\n"
        txt=txt+ '    <p><i>Vide</i></p>' + "\n"
        txt=txt+ '' + "\n"

    is_empty = True # default

    txt=txt+ '    <h3>Parametres</h3>' + "\n"
    if conds is not None :
        for cond in conds :
            cond_name = cond['name']
    
            txt=txt+ '' + "\n"
            txt=txt+ '    <p><a href=#' +cond_name+ ' onclick="toggle_visibility(\'more_'+cond_name+'\');"><i>open/close</i></a> groupe de parametres ' + cond_name + '</p>' + "\n"
            txt=txt+ '    <div class="block" id="more_' +cond_name+ '" style="display:None;" >' + "\n"
    
            txt=txt+ '    <table cellspacing="0" border="0">' + "\n"
            txt=txt+ '      <colgroup width="312"></colgroup>' + "\n"
            txt=txt+ '      <colgroup width="161"></colgroup>' + "\n"
            txt=txt+ '      <colgroup width="175"></colgroup>' + "\n"
    
            txt=txt+ '      <tr>' + "\n"
            txt=txt+ '        <td height="20" align="left" bgcolor="#CCFFCC"><i>nom du parametre</i></td>' + "\n"
            txt=txt+ '        <td align="left" bgcolor="#CCFFCC"><i>type</i></td>' + "\n"
            txt=txt+ '        <td align="left" bgcolor="#CCFFCC"><i>valeur</i></td>' + "\n"
            txt=txt+ '      </tr>' + "\n"
    
            pars = cond['vlepar_list']
            for par in pars :
                is_empty = False
                par_cname = par['cname']
                par_pname = par['pname']
                par_type = par['type']
                par_value = par['value'] # par_value = json.loads(par['value'])
                par_name = par_cname + '.' + par_pname
    
                txt=txt+ '      <tr>' + "\n"
                txt=txt+ '        <td height="20" align="left" bgcolor="#ECFFF1"><b>'+par_pname+ '</b></td>' + "\n"
                txt=txt+ '        <td align="left" bgcolor="#ECFFF1">' +par_type+ '</td>' + "\n"
                txt=txt+ '        <td align="left" bgcolor="#ECFFF1"><textarea name="'+ par_name + '" rows="2" cols="80">'+str(par_value)+'</textarea></td>' + "\n"
                txt=txt+ '      </tr>' + "\n"
            txt=txt+ '    </table>' + "\n"
            txt=txt+ '    </div>' + "\n"

    if is_empty :
        txt=txt+ '' + "\n"
        txt=txt+ '    <p><i>Vide</i></p>' + "\n"
        txt=txt+ '' + "\n"
    
    txt=txt+ '    <h3>Selection des donnees de sortie retournees</h3>' + "\n"
    txt=txt+ '' + "\n"
    txt=txt+ '    <p class="foruser">Vous pouvez cocher/decocher ci-dessous selon votre choix les groupes de donnees de sortie et les donnees de sortie afin de ne recevoir que les coches :</p>' + "\n"
    txt=txt+ '' + "\n"

    txt=txt+ '    - toutes les sorties <input type="checkbox" name="outselect" value="all" checked/><br />' + "\n"
    if views is not None :
        for view in views :
            view_name = view['name']
            txt=txt+ '    - groupe de donnees de sortie<input type="checkbox" name="outselect" value="' + view_name + '" />' + view_name + '<br />' + "\n"
        for view in views :
            outs = view['vleout_list']
            for out in outs :
                out_vname = out['vname']
                out_oname = out['oname']
                out_name = out_vname + '.' + out_oname
                txt=txt+ '    - donnee de sortie <input type="checkbox" name="outselect" value="' +out_name+ '" />' +out_name+ '<br />' + "\n"

    txt=txt+ '    <input type="hidden" name="vpz" value=' + str(vpz_id) + ' />' + "\n"
    txt=txt+ '    <input type="hidden" name="style" value="compact" />' + "\n"
    txt=txt+ '    <input type="hidden" name="format" value="html" />' + "\n"
    #txt=txt+ '    <input type="hidden" name="plan" value="single" />' + "\n"
    txt=txt+ '    <input type="hidden" name="restype" value="dataframe" />' + "\n"
    txt=txt+ '    <input type="hidden" name="mode" value="storage" />' + "\n"
    txt=txt+ '    <div id="csrf_token"></div>' + "\n"
    txt=txt+ '' + "\n"
    txt=txt+ '    <br>' + "\n"
    txt=txt+ '    </div>' + "\n"
    txt=txt+ '  </form>' + "\n"
    txt=txt+ '' + "\n"

    small_txt = full_name
    txt=txt+ '</div>' + "\n"
    txt=txt+ '' + "\n"
    txt=txt+ '<hr size="1"/><address style="text-align: right;"><small>' + small_txt + '</small></address>' + "\n"
    txt=txt+ '' + "\n"

    txt=txt + "<!-- "+ txtcomment + " -->" + "\n"
    txt=txt+ '' + "\n"
    txt=txt+ '</body>' + "\n"
    txt=txt+ '</html>' + "\n"

    return txt
 
###############################################################################
#                                  MAIN
###############################################################################

print(sys.argv, " command")

rootpath = "." # default
genpath = os.path.join(rootpath, "generated_gen") # default
privatepath = os.path.join(rootpath, "generated_private")
if len(sys.argv) >= 2 :
    path = sys.argv[1]
    if os.path.exists(path):
        if os.path.isdir(path):
            rootpath = path
            genpath = os.path.join(rootpath, "gen")
            privatepath = os.path.join(rootpath, "private")
homepath = os.path.join(genpath, "homes")
create_dir_if_absent(rootpath)
clean_dir(genpath)
clean_dir(homepath)
print("html home pages generated into ", homepath)

privatehomepath = os.path.join(privatepath, "homes")
clean_dir(privatepath)
clean_dir(privatehomepath)
print("limited access case / html home pages generated into ", privatehomepath)

# token value
fichier=open("token_value", 'r')
texte=fichier.readline()
fichier.close()
token_value=texte[0:-1] 
print("")
print("token_value : ", token_value)
print("")

# db/vpz
url = base_url + "db/vpz/"
options = {'mode':'tree', 'format':'json'}
vlevpz_list = send_get_and_receive(url=url,options=options)

for vlevpz in vlevpz_list :

    (version_name, rep_name, pkg_name, vpz_name,
     version_id, rep_id, pkg_id, vpz_id, comment) = content_vpz_tree(vlevpz)
    txtcomment = comment

    full_name = get_full_name(version_name= version_name, rep_name=rep_name,
                              pkg_name=pkg_name, vpz_name=vpz_name)
    txtcomment=txtcomment + "Nom complet (unique) du simulateur : " + full_name + "\n" + "\n"
    print("building html home pages of simulator ", full_name,
          " (Id", vpz_id , ")")

    # vpz/input
    inputdata = {"vpz":vpz_id, "jwt":token_value,
                 "mode":"tree", "format":"json", "outselect":"all"}
    responsedata = send_post_and_receive(
        url=base_url+"vpz/input/",
        inputdata=inputdata)
    (vleduration, vlebegin, conds, views) = content_simulation_inputs_tree(
                                                         vpzinput=responsedata)

    # db/vpz/access
    url = base_url+"acs/vpz/"+str(vpz_id)+"/access/"
    responsedata = send_get_and_receive( url=url, options={'format':'json'})
    access = responsedata["access"]

    if access == "public" :
        # html code into home file
        file_name = get_home_file_name(homepath=homepath,
                                 version_name=version_name, rep_name=rep_name,
                                 pkg_name=pkg_name, vpz_name=vpz_name)
    else : # "limited"
        # html code into home file
        file_name = get_home_file_name(homepath=privatehomepath,
                                 version_name=version_name, rep_name=rep_name,
                                 pkg_name=pkg_name, vpz_name=vpz_name)

    file = open(file_name, "w")
    
    id_v = str(rep_id)+"_"+str(pkg_id)+"_"+str(vpz_id)
    uuid_v = str(uuid.uuid4())
    csrf_token_value = id_v + "_" + uuid_v

    txt = get_txt(csrf_token_value, access,
                  vleduration, vlebegin, conds, views, pkg_id)

    file.write(txt)
    file.close() 

    # html code into home file ("public" or "limited" access)
    relative_file_name = get_home_file_name(homepath="..",
                                 version_name=version_name, rep_name=rep_name,
                                 pkg_name=pkg_name, vpz_name=vpz_name,
                                 to_create=False)

    # links into byids folder 

    if access == "public" :
        linkpath = os.path.join(homepath, ".by_ids")
    else : # "limited"
        linkpath = os.path.join(privatehomepath, ".by_ids")

    create_dir_if_absent(linkpath)
    dest_file_name = str(rep_id)+"__"+str(pkg_id)+"__"+str(vpz_id)+"___home.html"
    dest = os.path.join(linkpath, dest_file_name)
    if not os.path.exists(dest):
        src = relative_file_name
        os.symlink(src, dest)

###############################################################################

