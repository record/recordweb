###############################################################################
# 
#       Return (print) a JWT value for username
# 
# 1st parameter : username
# 2nd parameter : password
# 
###############################################################################

import os
import sys

apps_home = os.path.normpath(os.path.join("..", "..", "erecord", "apps"))

if "erecordroot" in os.environ.keys() :
    erecordroot = os.environ["erecordroot"]
    apps_home = os.path.normpath(os.path.join(erecordroot,
                                              "erecord", "erecord", "apps"))
if apps_home not in sys.path :
    sys.path.insert(0, apps_home)

from erecord_cmn.utils.using.obtain_jwt import obtain_jwt

###############################################################################
#                                  MAIN
###############################################################################

token_value = "unknown" # default

base_url = "http://erecord.toulouse.inra.fr:8000/"
#base_url = "http://127.0.0.1:8000/"
#base_url = "http://147.100.179.168:8000/"

username = None
password = None
if len(sys.argv) >= 3 :
    username = sys.argv[1]
    password = sys.argv[2]
    token_value = obtain_jwt(username=username, password=password,
                             base_url=base_url)
else :
    token_value = "parameters username, password missing"

print(token_value)

###############################################################################

