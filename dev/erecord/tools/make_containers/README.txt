*******************************************************************************

             Vle and models repositories installed into containers

*******************************************************************************

General
=======

A container is built for each models repository (more exactly for at least one 
models repository, for one or more models repositories).

Technology
----------

  Containers are built with Singularity http://singularity.lbl.gov

Install Singularity
-------------------

  See install_singularity.txt

Generalities about Singularity
------------------------------

  - Ways used to build a container :

      sudo singularity build containername.simg containername.def
      sudo singularity build containername.simg another_containername.simg

  - Conversions :

    - from .simg to .img :
      sudo singularity build --writable containername.img containername.simg
    - from .img to .simg :
      sudo singularity build containername.simg containername.img

  - Help :

      singularity help containername.simg


A models repository container
=============================

Content
-------

  The models repository 'repname', in 'vle-x' vle version, contains some vle
  packages. Its container contains (all in the appropriate vle-x version) :

        /opt/erecord/repositories/vle-x/repname/rep.simg :
          - vle
          - erecord package 
          - its vle packages 

Build rep.simg step by step
---------------------------

- Principle :

  Sources : VLE.def, erecord.def, rep.def

  Commands :
  - Install vle :            sudo singularity build rep.simg VLE.def
  - Add erecord :            sudo singularity build rep.simg erecord.def
  - Add 'repname' packages : sudo singularity build rep.simg rep.def

- In practise :

  1) Build intermediate containers for vle, then for erecord :

   - Build VLE.simg :

     Source   : VLE.def
     Command  : sudo singularity build VLE.simg VLE.def

   - Build erecord.simg :

     Source   : VLE.simg, erecord.def
     Commands : sudo singularity build erecord.simg VLE.simg
                sudo singularity build erecord.simg erecord.def

  2) Build rep.simg :

     Sources  : erecord.simg, rep.def

     Commands : sudo singularity build rep.simg erecord.simg
                sudo singularity build rep.simg rep.def


