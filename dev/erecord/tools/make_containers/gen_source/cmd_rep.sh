#!/bin/bash

# Note : VLEVERSION and REPNAME must have been defined before calling cmd_rep.sh

SINGULARITY_HOME=/opt/erecord/tools/make_containers
GEN_SOURCE_PATH=$SINGULARITY_HOME/gen_source
INCLUDE_REP_PATH=$GEN_SOURCE_PATH/include/rep
if ! [ -d $GEN_SOURCE_PATH/tmp ]
then
    mkdir $GEN_SOURCE_PATH/tmp
fi
TMP_GENERATED_DEF=$GEN_SOURCE_PATH/tmp/generated.def
TMP_TMP_DEF=$GEN_SOURCE_PATH/tmp/tmp.def

DEF_HOME=$SINGULARITY_HOME/$VLEVERSION/repositories/$REPNAME

cat $DEF_HOME/template.txt > $TMP_GENERATED_DEF

sed "/Header__/ r $INCLUDE_REP_PATH/Header.txt" $TMP_GENERATED_DEF > $TMP_TMP_DEF
cat $TMP_TMP_DEF > $TMP_GENERATED_DEF

sed "/Help__/ r $INCLUDE_REP_PATH/Help.txt" $TMP_GENERATED_DEF > $TMP_TMP_DEF
cat $TMP_TMP_DEF > $TMP_GENERATED_DEF

sed "/Post__/ r $INCLUDE_REP_PATH/Post.txt" $TMP_GENERATED_DEF > $TMP_TMP_DEF
cat $TMP_TMP_DEF > $TMP_GENERATED_DEF

sed "/Environment__/ r $INCLUDE_REP_PATH/Environment.txt" $TMP_GENERATED_DEF > $TMP_TMP_DEF
cat $TMP_TMP_DEF > $TMP_GENERATED_DEF

# __VLEVERSIONVALUE__ replaced by VLEVERSION value
# __REPNAMEVALUE__    replaced by REPNAME    value
cat $TMP_GENERATED_DEF | sed -e "s/__VLEVERSIONVALUE__/$VLEVERSION/g" > $TMP_TMP_DEF
cat $TMP_TMP_DEF | sed -e "s/__REPNAMEVALUE__/$REPNAME/g" > $TMP_GENERATED_DEF

cat $TMP_GENERATED_DEF > $DEF_HOME/generated.def

