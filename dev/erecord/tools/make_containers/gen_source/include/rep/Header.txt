#
# VLEVERSION=__VLEVERSIONVALUE__
# REPNAME=__REPNAMEVALUE__
#
#                      Singularity recipe file for
#
#      Container of __REPNAMEVALUE__ models repository (in __VLEVERSIONVALUE__ version)
#
#
# - rep.def : __REPNAMEVALUE__ packages (in __VLEVERSIONVALUE__ version)
#
# - Build container named rep.simg :
#
#   - Install vle : sudo singularity build rep.simg VLE.simg
#   - Add erecord : sudo singularity build rep.simg erecord.def
#   - Add __REPNAMEVALUE__ : sudo singularity build rep.simg rep.def
#
# - Help :
#
#     singularity help rep.simg
#
# - More (how it is used by erecord...) : see README.txt
