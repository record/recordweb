###############################################################################
#
#Header__
#
###############################################################################

%help

#Help__

%post

#Post__

  echo ""
  echo "###  Install recordb packages  ###"
  echo ""

  if ! [ -d /tmp/VLE ]
  then
      mkdir /tmp/VLE
  fi

  # Install some vle packages
  if [ -d /tmp/VLE/packages ]
  then
      rm -fr /tmp/VLE/packages
  fi
  cd /tmp/VLE
  git clone https://github.com/vle-forge/packages.git
  cd packages
  git checkout master1.1
  ##./build.sh -c    # see if [ $all_ok = 0 ] then exit -1
  vle -P vle.output configure build
  vle -P vle.extension.celldevs configure build
  vle -P vle.extension.cellqss configure build
  vle -P vle.extension.decision configure build
  vle -P vle.extension.difference-equation configure build
  vle -P vle.extension.differential-equation configure build
  vle -P vle.extension.dsdevs configure build
  vle -P vle.extension.fsa configure build
  vle -P vle.extension.petrinet configure build
  #vle -P vle.examples configure build
  vle -P ext.muparser configure build
  vle -P vle.forrester configure build
  vle -P ext.lua configure build
  vle -P vle.ibm configure build 

  # Install recordb packages
  if [ -d /tmp/VLE/RECORD ]
  then
      rm -fr /tmp/VLE/RECORD
  fi
  cd /tmp/VLE
  git clone https://forgemia.inra.fr/record/RECORD.git
  cd /tmp/VLE/RECORD
  git checkout master1.1
  cd /tmp/VLE/RECORD/pkgs
  #./build.sh -c

  vle -P tester configure build
  vle -P meteo configure build
  vle -P wwdm configure build
  vle -P 2CV configure build
  vle -P ext.Eigen configure build
  vle -P DEtimeStep configure build
  vle -P gluePhysic configure build
  vle -P LotkaVolterra configure build
  vle -P DateTime configure build
  vle -P record.eigen configure build
  vle -P record.eigen_test configure build
  vle -P WACSgen configure build
  vle -P Multiformalism configure build
  vle -P glue configure build
  vle -P GenCSVcan configure build
  vle -P record.reader configure build
  vle -P record.tester configure build
  vle -P record.tester_test configure build
  vle -P record.reader_test configure build
  vle -P record.recursive configure build
  vle -P record.recursive_test configure build
  vle -P record.optim configure build
  vle -P record.optim_test configure build
  vle -P flood_wave configure build
  vle -P CaliFloPP configure build
  vle -P rvleSimScript configure build

  #Not built :
  #vle -P GenGIScan configure build
  #vle -P MilSol configure build
  #vle -P minicrop configure build
  #vle -P herbsim configure build
  #vle -P modelisad configure build
  #vle -P azodyn configure build
  #vle -P carbone configure build
  #vle -P carboneR configure build
  #vle -P Decision configure build
  #vle -P executive configure build
  #vle -P openalea configure build
  #vle -P pydynamics configure build
  #vle -P record.meteo configure build
  #vle -P record.meteo_test configure build
  #vle -P RinsideGVLE configure build
  #vle -P spudgro configure build
  #vle -P UseCppLib configure build
  #vle -P UseCppLib_dep configure build
  #vle -P UseFortranLib configure build
  #vle -P UsePythonLib configure build
  #vle -P vle.output.postgresql configure build
  #vle -P weed configure build
  #vle -P wheatpest configure build
  #vle -P wheatpest_V8 configure build
  #vle -P 2CVdt configure build

  # TPs :

  cd /tmp/VLE/RECORD/formation/tp2_3/pkgs
  vle -P tp2_3_correction configure build

  cd /tmp/VLE/RECORD/formation/tp2_4/pkgs
  vle -P tp2_4 configure build
  vle -P tp2_4_correction configure build

  cd /tmp/VLE/RECORD/formation/tp3_2/pkgs
  vle -P tp3_2 configure build
  vle -P tp3_2_correction configure build

  cd /tmp/VLE/RECORD/formation/tp4_1/pkgs
  vle -P tp4_1 configure build
  vle -P tp4_1_correction configure build

  cd /tmp/VLE/RECORD/formation/tp5_1/pkgs
  vle -P tp5_1 configure build
  vle -P tp5_1_correction configure build

  cd /tmp/VLE/RECORD/formation/tp5_2/pkgs
  vle -P tp5_2 configure build
  vle -P tp5_2_correction configure build

  cd /tmp/VLE/RECORD/formation/tp5_4/pkgs
  vle -P tp5_4 configure build
  vle -P tp5_4_correction configure build

  cd /tmp/VLE/RECORD/formation/tpForrester/pkgs
  vle -P tpForrester configure build
  vle -P tpForrester_correction configure build

  chmod -R 777 $VLE_HOME

  # Nettoyage de temporaire
  rm -fr /tmp/VLE/packages
  rm -fr /tmp/VLE/RECORD

%environment

#Environment__

# ===================================

