###############################################################################
#
#Header__
#
###############################################################################

%help

#Help__

%post

#Post__

  echo ""
  echo "###  Install vle packages  ###"
  echo ""

  if ! [ -d /tmp/VLE ]
  then
      mkdir /tmp/VLE
  fi

  # Install vle packages
  if [ -d /tmp/VLE/packages ]
  then
      rm -fr /tmp/VLE/packages
  fi
  cd /tmp/VLE
  git clone git://github.com/vle-forge/packages.git
  cd packages
  git checkout master
  ##./build.sh -c    # see if [ $all_ok = 0 ] then exit -1

  vle -P vle.reader configure build
  vle -P vle.tester configure build
  vle -P vle.discrete-time configure build
  vle -P vle.extension.celldevs configure build
  vle -P vle.extension.decision configure build
  vle -P vle.extension.dsdevs configure build
  vle -P vle.extension.fsa configure build
  vle -P vle.extension.petrinet configure build
  vle -P vle.ode configure build
  vle -P vle.ode_test configure build
  vle -P vle.recursive configure build
  vle -P vle.discrete-time.decision configure build
  vle -P vle.reader_test configure build
  vle -P vle.extension.cellqss configure build
  vle -P vle.discrete-time_test configure build
  vle -P vle.recursive_test configure build
  vle -P vle.examples configure build
  vle -P vle.tester_test configure build
  vle -P vle.discrete-time.decision_test configure build
  vle -P ext.lua configure build
  vle -P ext.muparser configure build
  vle -P ext.qwt configure build
  vle -P ext.shapelib configure build
  vle -P vle.adaptative-qss.examples configure build
  #vle -P gvle.decision configure build
  #vle -P gvle.discrete-time configure build
  #vle -P gvle.forrester configure build
  #vle -P gvle.simulating.plan configure build

  chmod -R 777 $VLE_HOME

  # Nettoyage de temporaire
  rm -fr /tmp/VLE/packages

%environment

#Environment__

# ===================================

