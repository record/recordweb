"""factory

*This package is part of erecord - web services for vle models*

:copyright: Copyright (C) 2019-2024 INRA http://www.inra.fr.
:license: GPLv3, see LICENSE file for more details.
:authors: see AUTHORS file.

Factory of the erecord project

The factory directory is the workspace to manage the information produced 
for and during processing.

Content hierarchy :
See the packages of the factory package : erecord, install, static, log, run, media...

In particular 'erecordenv' under 'install' subdirectory : the python virtualenv

"""
