.. _url_erecord:

=====================
http://127.0.0.1:8000
=====================

Access to erecord web services
==============================

**The erecord web services are online at** http://127.0.0.1:8000.

You have to use the complete URL of the required resource,

for example http://127.0.0.1:8000/db/vpz as URL for the *'GET db/vpz'* resource.

