.. _tests:

====
Test
====

.. _unittest:

Unit test
=========

Introduction
------------

The erecord project software tests have been made with some testing tools from
django framework and the django application django-rest-framework *(based on
python libraries)*.

The software tests can be automatically run. They should be run after
software modifications to verify non regression.

Tests description
-----------------

The written tests imply GET vpz/input, POST vpz/input, and POST vpz/output
resources. Tests are written for 2 cases :

    - case of a simulator based on a VpzPath : cf test_vpz_from_vpzpath.
    - case of a simulator defined by a VleVpz (of a VlePkg of a VleRep) : cf
      test_vpz_from_vlevpz *(.. under construction ..)*.

Running tests
-------------

Tests can be run using the test command of the django project’s manage.py
utility :

    python manage.py test

This command runs all the TestCase (subclasses) found into all the test*.py
files.
 
.. note::
   *To run tests, ACTIVITY_PERSISTENCE should value True (see the configuration
   file config.py of the erecord_cmn application).*


.. _benchmark:

Benchmark
=========

*... under construction ...*

Benchmarking a request
----------------------

When calling erecord web services by cURL command line tool, it is possible to get information after the completed transfer, thanks to the "w" option. The available variables are (see http://curl.haxx.se/docs/manpage.html) :

- content_type : the Content-Type of the requested document, if there was any.
- filename_effective : the ultimate filename that curl writes out to. This is only meaningful if curl is told to write to a file with the --remote-name or --output option. It's most useful in combination with the --remote-header-name option.
- ftp_entry_path : the initial path curl ended up in when logging on to the remote FTP server.
- http_code : the numerical response code that was found in the last retrieved HTTP(S) or FTP(s) transfer. In 7.18.2 the alias response_code was added to show the same info.
- http_connect : the numerical code that was found in the last response (from a proxy) to a curl CONNECT request.
- local_ip : the IP address of the local end of the most recently done connection - can be either IPv4 or IPv6 
- local_port : the local port number of the most recently done connection 
- num_connects Number of new connects made in the recent transfer.
- num_redirects Number of redirects that were followed in the request.
- redirect_url When an HTTP request was made without -L to follow redirects, this variable will show the actual URL a redirect would take you to.
- remote_ip : the remote IP address of the most recently done connection - can be either IPv4 or IPv6
- remote_port : the remote port number of the most recently done connection
- size_download : the total amount of bytes that were downloaded.
- size_header : the total amount of bytes of the downloaded headers.
- size_request : the total amount of bytes that were sent in the HTTP request.
- size_upload : the total amount of bytes that were uploaded.
- speed_download : the average download speed that curl measured for the complete download. Bytes per second.
- speed_upload : the average upload speed that curl measured for the complete upload. Bytes per second.
- ssl_verify_result : the result of the SSL peer certificate verification that was requested. 0 means the verification was successful.
- **time_appconnect** : the time, in seconds, it took from the start until the SSL/SSH/etc connect/handshake to the remote host was completed.
- **time_connect** : the time, in seconds, it took from the start until the TCP connect to the remote host (or proxy) was completed.
- **time_namelookup** : the time, in seconds, it took from the start until the name resolving was completed.
- **time_pretransfer** : the time, in seconds, it took from the start until the file transfer was just about to begin. This includes all pre-transfer commands and negotiations that are specific to the particular protocol(s) involved.
- **time_redirect** : the time, in seconds, it took for all redirection steps include name lookup, connect, pretransfer and transfer before the final transaction was started. time_redirect shows the complete execution time for multiple redirections.
- **time_starttransfer** : the time, in seconds, it took from the start until the first byte was just about to be transferred. This includes time_pretransfer and also the time the server needed to calculate the result.
- **time_total** : the total time, in seconds, that the full operation lasted. The time will be displayed with millisecond resolution.
- url_effective : the URL that was fetched last. This is most meaningful if you've told curl to follow location: headers.

Examples
--------

The information format can be specified as a literal "string" or into a file.

.. literalinclude:: bm_curl_calls.txt

With curl-time-format.txt content :

.. literalinclude:: bm_curl-time-format.txt

With curl-format.txt content :

.. literalinclude:: bm_curl-format.txt

