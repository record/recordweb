.. _software_content:

================
Software content
================

The **erecord** root project contains :

  - **erecord** :

    The erecord subdirectory (python package) contains the software
    production (source code...). See : :ref:`erecord_package`

  - **databases** : 

    The erecord package software relies on information that is stored on the
    databases subdirectory. See :ref:`databases`

  - **repositories** :

    The erecord package software relies on models that are physically
    installed on the repositories subdirectory, that contains the models
    repositories *(several, as many as wanted)*.

    In order to be used, a model must have been 
    (i) physically stored into a models repository of the repositories directory,
    (ii) recorded into the appropriate database of the databases directory.

    See : :ref:`repositories`

  - **docs** :

    The docs subdirectory contains the erecord project documentation.
    See : :ref:`documentation`

  - **factory** :

    The factory subdirectory is the workspace dedicated to the management of
    the information produced for and during processing.

    Before installation and running, it is empty.

    It is used for several purposes :

        - erecordenv : the python virtualenv
        - static
        - docs : generated online documentation
        - log
        - run
        - media

  - etc.

List of Python modules : :ref:`modindex`

