.. _erecord_package:

===============
erecord package
===============

The **erecord package** contains the production (source code...). Based on :term:`django`, it mainly contains :

  - projects : django projects of the erecord package
  - apps : django applications of the erecord package. The django
    applications are subdirectories of apps. The 'apps' path must be
    included into PYTHONPATH (see the settings.py file of the django project).

API reference :

  .. toctree::
     :maxdepth: 2

     ../ref/autogen/project/erecord

  - Applications of erecord package contained into the erecord/apps subdirectory :

    .. toctree::
       :maxdepth: 2

       ../ref/autogen/APPS/index

