.. _erecord_ip:

=====================
Intellectual property
=====================

The erecord software production is the :ref:`erecord_package`.

:ref:`AUTHORS` | :ref:`LICENSE` | :ref:`citing_erecord`

.. literalinclude:: ERECORD

