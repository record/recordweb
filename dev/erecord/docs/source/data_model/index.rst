.. _data_model:

==========
Data model
==========

The **domain** described by erecord project relies on concepts from Vle *(package, vpz...)* that can as well be given other names, depending on the adopted point of vue.

For example, some 'equivalent' vocabulary : 

  =====================  =====================  =============================
  **Record**             **Vle**                **erecord objects**
  =====================  =====================  =============================
  Agronomic model        package, project       :term:`vle package`, VlePkg
  ---------------------  ---------------------  -----------------------------
  Simulator              vle vpz, vpz file      :term:`vpz (as VleVpz)`
  =====================  =====================  =============================


**Data model** by applications :

  :ref:`erecord_db application <dm_erecord_db>`
  | :ref:`erecord_vpz application <dm_erecord_vpz>`
  | :ref:`erecord_acs application <dm_erecord_acs>`

.. _dm_erecord_db:

Data model of erecord_db application
------------------------------------

Schema
++++++

    - :ref:`dm_vleversion`
        - many :ref:`dm_vlerep`
            - many :ref:`dm_vlepkg`
                - many :ref:`dm_vlevpz`

.. _dm_vleversion:

VleVersion
++++++++++

  Vle version.

.. _dm_vlerep:

VleRep
++++++

  Models repository.

  A VleRep corresponds with a physical :term:`models repository` providing the required environment for running simulations.

.. _dm_vlepkg:

VlePkg
++++++

  Vle package.

  Other denominations : package (vle notion) ; project (vle notion) ;
  :term:`vle model` ; model ; agronomic model ; vle package ; vle project.

  It corresponds with the vle notion of package, or project.

  A VlePkg is attached to a VleRep providing the physical required environnement.

.. _dm_vlevpz:

VleVpz
++++++

  Vpz file.

  Other denominations : vpz (vle notion) ; vpz file (vle notion) ;
  :term:`simulator` ; simulation scenario.

  It corresponds with the vle notion of vpz, or vpz file.

  A VleVpz is attached to a VlePkg that is attached to a VleRep providing the physical required environnement.

.. _dm_erecord_vpz:

Data model of erecord_vpz application
-------------------------------------

Schema
++++++

    - :ref:`dm_vpzpath`
    - :ref:`dm_vpzact`
        - one :ref:`dm_vpzorigin`
        - one :ref:`dm_vpzinput`
            - one :ref:`dm_vlebegin`
            - one :ref:`dm_vleduration`
            - many :ref:`dm_vlecond`
                - many :ref:`dm_vlepar`
            - many :ref:`dm_vleview`
                - many :ref:`dm_vleout`
        - one :ref:`dm_vpzoutput`
        - one :ref:`dm_vpzworkspace`

:ref:`dm_vpzact` 'comes' (is built) from a :ref:`dm_vpzpath` or a :ref:`dm_vlevpz`

.. _dm_vlebegin:

VleBegin
++++++++

  Vle begin

.. _dm_vlecond:

VleCond
+++++++

  Vle condition

.. _dm_vleduration:

VleDuration
+++++++++++

  Vle duration

.. _dm_vleout:

VleOut
++++++

  Vle output data

  Vle output data (of a VleView)

.. _dm_vlepar:

VlePar
++++++

  Vle parameter 

  Vle parameter (of a VleCond).

  It contains some information about the parameter (name, value...), that is got or not into a request answer (it depends on the options given into the request).

  A value type can value : "boolean", "integer", "double", "string", "xml", "set", "map", "tuple", "table", "matrix, "none", "".

.. _dm_vleview:

VleView
+++++++

  Vle view

.. _dm_vpzact:

VpzAct
++++++

  Vpz activity

  It is defined from a VleVpz or a VpzPath


.. _dm_vpzinput:

VpzInput
++++++++

  Vpz input

  Other denominations : input information of a vpz ; vpz input information,
  input conditions of a simulator.

  The vpz input information corresponds with the configuration (experiment) defined into the vpz file (information such as parameters, duration, begin...).

.. _dm_vpzorigin:

VpzOrigin
+++++++++

  Vpz origin

.. _dm_vpzoutput:

VpzOutput
+++++++++

  Vpz output

  Other denominations : output information of a vpz ; vpz output information,
  simulation results of a simulator.

  The vpz output information corresponds with the simulation results.

.. _dm_vpzpath:

VpzPath
+++++++

  Vpz path

  A VpzPath corresponds with a vpz file absolute path that gives : its location into a vle package into a models repository (providing the required environment for running simulations) and its vpz name.

.. _dm_vpzworkspace:

VpzWorkspace
++++++++++++

  Vpz workspace

  A VpzWorkspace is attached to a VpzAct. It defines a workspace directory dedicated to the Vpz activity (manipulation).

.. _dm_erecord_acs:

Data model of erecord_acs application
-------------------------------------

Schema
++++++

    - :ref:`dm_vpzlock`

        - many User

        - many :ref:`dm_vpzpath`
        - many :ref:`dm_vlevpz`

    - :ref:`dm_activitylock`

        - one User

        - one :ref:`dm_vpzact`

        - one :ref:`dm_vpzorigin`
        - one :ref:`dm_vpzinput`
        - one :ref:`dm_vpzoutput`
        - one :ref:`dm_vlebegin`
        - one :ref:`dm_vleduration`
        - many :ref:`dm_vlecond`
        - many :ref:`dm_vlepar`
        - many :ref:`dm_vleview`
        - many :ref:`dm_vleout`
        - one :ref:`dm_vpzworkspace`


    - :ref:`dm_loadlock`

        - one User

        - one uploaded document (UploadVpzDocument)
        - one downloadable document (DownloadVpzDocument)

A lock makes the link between an object and its authorized users. A lock may be manually created by the admin or generated by software, it depends on its use case (the locked object : VleVpz, VpzAct...).

.. _dm_vpzlock:

VpzLock
+++++++

  Lock dedicated to VleVpz and VpzPath.

.. _dm_activitylock:

ActivityLock
++++++++++++

  Lock dedicated to an activity ie to one VpzAct and its parts VpzOrigin VpzInput VpzOutput VleBegin VleDuration VleCond, VlePar, VleView, VleOut, VpzWorkspace.

.. _dm_loadlock:

LoadLock
++++++++

  Lock dedicated to uploaded document (UploadVpzDocument) and downloadable document (DownloadVpzDocument).

