.. _faqs_developers:

===============
Developers FAQs
===============

Where can I find help as developer ?
------------------------------------

  - :ref:`How to install/deploy the erecord software <install>`
  - :ref:`How to install/declare a model <repositories>`
  - :ref:`How to build documentation <documentation>`
  - :ref:`Testing the erecord software <tests>`

