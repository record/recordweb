.. _repositories:

===================
Models repositories
===================


The :term:`erecord` software manages some models repositories containing some vle models, containing the simulators that are proposed to users to be manipulated (seen, modified, simulated).

Before being taken into account, a :term:`simulator` must have been (i) physically stored into a models repository and (ii) recorded into an appropriate database (see :ref:`databases`).

Each models repository is physically installed into the 'repositories' directory, as a :term:`Singularity` **container**.
The container file of a models repository contains :

- its self independant and consistent group of :term:`vle package` s (including extensions packages, other dependencies...)
- anything else required to be able to use its simulators : :term:`Vle` and if needed some other tools, libraries...
- in particular the erecord vle package that is used by erecord software (the erecord vle package is delivered with erecord software).

More
====

.. literalinclude:: ../../../tools/make_containers/README.txt

Installation
============

See :ref:`Installation of models repositories container files <install_containers>` 

