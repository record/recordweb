.. _databases:

Databases
=========

The :term:`erecord` software manages some models repositories containing some vle models, containing the simulators that are proposed to users to be manipulated
(seen, modified, simulated).

Before being taken into account, a :term:`simulator` must have been
(i) physically stored into a models repository (see :ref:`repositories`) and
(ii) recorded into an appropriate database.

To record a :term:`simulator` :

- A possibility is to rely on 3 elements of the database, that are associated with the :term:`erecord_db` application : models repositories (:ref:`dm_vlerep`), vle models (vle packages) of a models repository (see :ref:`dm_vlepkg`), simulators (vpz file) of a vle model of a models repository (see :ref:`dm_vlevpz`).

  In that first case, both erecord_db and erecord_vpz applications are needed to launch simulations.

- Another possibility is to rely on 1 element of the database, that is associated with the :term:`erecord_vpz` application : the vpz file absolute path (see :ref:`dm_vpzpath`) of the simulator. This path determines the simulator vpz file, but also its vle model and its models repository.

  In that second case, the erecord_vpz application is self sufficient to launch simulations.

