.. _erecord_contacts:

========
Contacts
========

    You may find answers to your own questions in :ref:`faqs`.

    You can send mail to Nathalie Rousse (INRA, MIAT, RECORD Team Member)
    : nathalie.rousse@inra.fr (*help, notice a bug, ask for new functions...*).

