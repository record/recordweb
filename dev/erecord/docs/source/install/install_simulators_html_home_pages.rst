.. _install_simulators_html_home_pages:

==========================
Simulators HTML home pages
==========================

Prerequisite
============

In development case, you must begin by modifying base_url into /opt/erecord/tools/make_vpzpages/vpzpages/build_vpzpages.py,get_token.py (127.0.0.1 address)

Installation of simulators HTML home pages
==========================================

  .. literalinclude:: ../../../erecord/install/prod/install_vpz_html_home_pages.txt


