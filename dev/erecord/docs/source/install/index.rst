.. _install:

============
Installation
============

  - :ref:`install_general`

  - Installation procedure :
    :ref:`in development case <install_dev>`
    | :ref:`in production case <install_prod>`

  - Simulators delivery :
    :ref:`container files <install_containers>` |
    | :ref:`HTML home pages <install_simulators_html_home_pages>`

  - Misc : :ref:`admin help <install_help>`


.. _install_general:

General
=======

During development, the erecord project software can be installed on the development server provided by :term:`django`. Then once developed, it can be deployed on a production server in order to be used by web users. This implies a production environment, in addition to the tools and libraries already used for development.

Source software
---------------

The erecord project source software contains the 'repositories' subdirectory that is aimed at hosting the models repositories container files that will be delivered to the erecord project.

Once the installation finished, the resulting hierarchy is such as :

    .. literalinclude:: hierarchy_source.txt

Development environment
-----------------------

The erecord project software is developed with :

    - python language (python 2.7 version),
    - django framework,
    - some django applications, like for example django-rest-framework. 
    - SQLite for databases.
    - Sphinx for documentation.

    See the requirement.txt file :

    .. literalinclude:: ../../../erecord/install/requirement.txt

.. note:: This does not include the development of the models repositories container files, considered as a delivery for erecord project.

Additional production environment
---------------------------------

In our case, the software has been deployed on a virtual machine with **Debian 9.5** (*Stretch*), **Apache2.4** server and **mod_wsgi**.

    *Django’s primary deployment platform is WSGI, the Python standard for web
    servers and applications. mod_wsgi is an Apache module which can host any
    Python WSGI application, including Django.*

.. _install_procedure:

Installation procedure
======================

  - :ref:`install_dev`

  - :ref:`install_prod`

.. _install_simulators:

Simulators delivery
===================

  - :ref:`Models repositories container files <install_containers>`

  - :ref:`Simulators HTML home pages <install_simulators_html_home_pages>`

.. _install_misc:

Misc
====

  - :ref:`install_help`

