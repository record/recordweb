.. _install_prod:

=========================================
Installation procedure in production case
=========================================

  :ref:`Installation of erecord <install_prod_erecord>`
  | :ref:`install_prod_log`

.. _install_prod_erecord:

Installation of erecord in production case
==========================================

  .. literalinclude:: ../../../erecord/install/prod/install.txt

.. _install_prod_log:

Installation of log file and using it
=====================================

  .. literalinclude:: ../../../erecord/install/prod/install_log.txt

