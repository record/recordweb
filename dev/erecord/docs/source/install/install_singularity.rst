.. _install_singularity:

===========================
Installation of singularity
===========================

  .. literalinclude:: ../../../tools/make_containers/install_singularity.txt

