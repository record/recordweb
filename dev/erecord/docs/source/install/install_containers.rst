.. _install_containers:

===================================
Models repositories container files
===================================

Some models repositories container files are delivered to erecord project, installed into its 'repositories' subdirectory.
The models repositories have previously been developed with :term:`vle` and then have been incorporated into :term:`singularity` container files.

Prerequisite
============

  :ref:`install_singularity`

Installation of models repositories container files
===================================================

.. note:: 
   The following install description is given as an example that each
   developer can adapt to his own set of models repositories container files.
   **Only some parts of it are delivered into erecord software**, as an
   example.  The delivered source files (.def, .txt) are :

   - **vle-1.1.3** version case :
     VLE.def *(to build VLE.simg)* ;
     template_erecord.txt *(to generate erecord.def to build erecord.simg)* ;
     template.txt *(to generate rep.def to build rep.simg)* for
     **recordb** and **vle** models repositories

   - **vle-2.0.1** version case :
     VLE.def *(to build VLE.simg)* ;
     template_erecord.txt *(to generate erecord.def to build erecord.simg)* ;
     template.txt *(to generate rep.def to build rep.simg)* for
     **recordb**, **vle**, **sunflo** and **azodyn** models repositories.

.. literalinclude:: ../../../tools/make_containers/install_containers.txt 

