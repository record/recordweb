.. _description:

===========
Description
===========

  .. toctree::
     :maxdepth: 1

     ../software_content/index

  - :ref:`erecord_package`
     
  .. toctree::
     :maxdepth: 1

     ../databases/index

     ../data_model/index

     ../repositories/index

     ../documentation/index

     ../features/index

