
A erecord visitor who would like to use a simulator that is in limited access doesn't need to register himself into erecord. He has to contact the model owner who will accept or not to give him access to the model, ie who will give or not the name and password of one of the authorized users that he manages (:ref:`more exactly <auth_ask_for_access>`).

