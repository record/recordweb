.. _feat_storage:

======================
Web storage capability
======================

Introduction
------------

Web storage has to do with calling the erecord web services
**from a web browser**, more precisely with requests in 'html' format from a
web browser.

Web storage supports persistent data storage on the client side.

The erecord web storage capability offers the possibility of **storing some
simulation results into the web browser**, in order to use them later on
(stored datas persist after the browser has been closed).

Technical details
-----------------

There are two main web storage types : local storage and session storage.

The web storage type that is used in the erecord project is **local storage**,
where the stored data have no expiration date : they persist after the browser
has been closed, until they are cleaned (by an application or the browser user).

The local storage is based on the HTML local storage object window.localStorage.

So it can be directly managed by the tools offered by the browser in this purpose : tools to look at localStorage, to delete some parts of it...

Some browsers that support the web storage : Chrome 4.0, Internet Explorer 8.0, Firefox 3.5, Safari 4.0, Opera 11.5.

For more details : http://en.wikipedia.org/wiki/Web_storage

Web storage in erecord
----------------------

Web storage is useful only in the case of :ref:`req_post_vpz_output`, the
resource to run a simulator after modification.

If the :ref:`req_post_vpz_output` request is sent from a web browser in 'html'
format and 'storage' mode, then the returned web page gives access to the web
storage information : the maybe stored results of previous simulations, that
the user will thus be able to look at (in numeric, graphic...).

Web storage information contains simulation results only in 'dataframe' restype.
More precisely, the results of a simulation can be stored only if they come
from a :ref:`req_post_vpz_output` request that is sent from a web browser in
'html' format and 'storage' mode and 'compact' style and 'dataframe' restype.

.. note::
   The behaviour described above supposes that the used web browser supports
   web storage.

To save the results of a simulation
+++++++++++++++++++++++++++++++++++

To be able to save the results of a simulation, send the
:ref:`req_post_vpz_output` request from a web browser with at least the
following parameters :

- format=html (see :ref:`opt_format`)
- mode=storage (see :ref:`opt_storage`)
- mode=compact (see :ref:`opt_style`)
- mode=dataframe (see :ref:`opt_plan_restype`)

To access to the stored results of previous simulations
+++++++++++++++++++++++++++++++++++++++++++++++++++++++

To be able to access to the stored results of previous simulations, send the
:ref:`req_post_vpz_output` request from a web browser with at least the
following parameters :

- format=html (see :ref:`opt_format`)
- mode=storage (see :ref:`opt_storage`)

