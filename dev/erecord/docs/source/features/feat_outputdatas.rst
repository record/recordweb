.. _feat_outputdatas:

================================
Output datas of a simulator
================================

.. _outputdatas_views:

Output datas and views
======================

  Into the :ref:`output information <dm_vpzoutput>`, the simulator
  :ref:`output datas <dm_vleout>` are gathered in some
  :ref:`views <dm_vleview>` (sets of output datas).

.. _outpoutdatas_ident:

Identification
==============

  It is necessary to know how an :ref:`output data <dm_vleout>` of a simulator 
  is identified, in order to be able to select it into a request.

  A simulator :ref:`output data <dm_vleout>` is identified by its
  'selection_name' value *(concatenation of its 'vname' and 'oname')*.
  A simulator :ref:`view <dm_vleview>` is identified by its 'selection_name'
  value.

  To get those identification information *('selection_name')*, for example
  you can use the :ref:`req_post_vpz_input` resource with 'all' as
  :ref:`opt_outselect` value. Try :

.. literalinclude:: ../examples/wwdm/req_post_vpz_input_140.txt

.. _outputdatas_select:

Selecting some output datas
===========================

  To select :ref:`output datas <dm_vleout>` of a simulator, it is
  necessary to know their information identification (see above).

  You can choose to return only some :ref:`output datas <dm_vleout>` or
  :ref:`views <dm_vleview>` of a simulator, ie ask for filtering some of
  them (see :ref:`opt_outselect` option).

