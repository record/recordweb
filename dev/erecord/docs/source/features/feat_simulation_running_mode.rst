.. _feat_simulation_running_mode:

================================
Simulation running mode
================================

You can choose the simulation running mode, through values for 
:ref:`plan <opt_plan_restype>` and :ref:`restype <opt_plan_restype>` options.


Multiple simulation
===================

Asking for a multiple simulation consists in :

    - choosing 'linear' value as :ref:`plan <opt_plan_restype>` option.

    - :ref:`modifying some parameters <parameters_modif>` giving them
      multiple values.

.. note::
   For a multiple simulation composed by N simulations, each parameter has
   exactly N values or only 1 value. If not, the simulation fails.
 
