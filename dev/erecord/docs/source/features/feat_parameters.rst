.. _feat_parameters:

================================
Parameters of a simulator
================================

.. _parameters_conditions:

Parameters and conditions
=========================

  Into the :ref:`input information <dm_vpzinput>`, the simulator
  :ref:`parameters <dm_vlepar>` are gathered in some
  :ref:`conditions <dm_vlecond>` (sets of parameters).

.. _parameters_ident:

Identification
==============

  It is necessary to know how a :ref:`parameter <dm_vlepar>` of a simulator 
  is identified, in order to be able to select it or to modify its value
  into a request.

  A simulator :ref:`parameter <dm_vlepar>` is identified by its 'cname' and
  'pname' values, its 'selection_name' value *(concatenation of 'cname' and
  'pname')*. A simulator :ref:`condition <dm_vlecond>` is identified by its
  'selection_name' value.

  To get those identification information
  *('selection_name', 'cname', 'pname')*, for example you can use the
  :ref:`req_get_vpz_input` resource. Try :

  http://127.0.0.1:8000/vpz/input/?vpz=266&mode=compact&format=json

  http://127.0.0.1:8000/vpz/input/?vpz=266&mode=compactlist&format=json

.. _parameters_select:

Selecting some parameters
=========================

  To select :ref:`parameters <dm_vlepar>` of a simulator, it is
  necessary to know their information identification (see above).

  You can choose to return only some :ref:`parameters <dm_vlepar>` or
  :ref:`conditions <dm_vlecond>` of a simulator, ie ask for filtering some of
  them (see :ref:`opt_parselect` option).

.. _parameters_modif:

Modifying some parameters
=========================

  To modify values of some :ref:`parameters <dm_vlepar>` of a simulator, it is
  necessary to know their information identification (see above).

  A :ref:`parameter <dm_vlepar>` value can be modified by two different ways :
  with :ref:`pars <opt_pars>` or :ref:`cname.pname <opt_pars>`.

