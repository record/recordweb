.. _feat_datafolder:

================================
Modifying the input datas folder
================================

Introduction
------------

For some resources, it is possible to **modify the input datas folder** of
the simulator according to a posted file (optional).

How to modify the input datas folder
------------------------------------

.. include:: ../webapi/desc_vpz_modify_datafolder.rst

Parameters : :ref:`opt_datafolder` | :ref:`opt_datafoldercopy`

Concerned resources
-------------------

Some resources for which the input datas folder can be modified :

    :ref:`req_post_vpz_output` | :ref:`req_post_vpz_experiment` |
    :ref:`req_post_vpz_report` | :ref:`req_post_vpz_inout`

Some examples
-------------

Examples in command line with cURL illustrating how to modify the input datas folder by using datafolder and datafoldercopy.

  For the simulator 'wwdm.vpz' whose Id is  :ref:`wwdm_simulator_id`,
  enter the cURL commands in a command line window.

  .. literalinclude:: ../examples/include/intro_datafolder.txt

- :ref:`req_post_vpz_output` :

    .. literalinclude:: ../examples/wwdm/req_post_vpz_output_135_3.txt

- :ref:`req_post_vpz_experiment` :

    .. literalinclude:: ../examples/experiment/post_3/request.txt

- :ref:`req_post_vpz_report` :

    .. literalinclude:: ../examples/report/req_post_vpz_report_160_3.txt


- :ref:`req_post_vpz_inout` :

    .. literalinclude:: ../examples/wwdm/req_post_vpz_inout_130_3.txt

