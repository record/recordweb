
.. _feat_vleversion:

====================
Several vle versions
====================

Introduction
------------

erecord (*from the vle-x version of erecord*) accepts models repositories developped with **different vle versions**.

Description
-----------

A vle version is defined by :

- the **vle version name**, structured as vle-V.rev.

  For example vle-1.1.3, vle-2.0.0, vle-1.1...

How is it managed
-----------------

In case of a simulator defined/recorded as vpz (:ref:`dm_vlevpz`) :

  - Each vle version (:ref:`dm_vleversion`) is recorded into the erecord database.
  - Each models repository (:ref:`dm_vlerep`) is attached to a vle version (:ref:`dm_vleversion`).

In case of a simulator defined/recorded as vpzpath (:ref:`dm_vpzpath`) :

    The vle version of such a simulator is deduced from its vpzpath value.

