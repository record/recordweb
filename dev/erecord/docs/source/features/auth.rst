.. _auth:

=============================
JSON Web Token authentication
=============================

Overview
========

Authentication is required for the **models** that are in **limited access**. Authentication is managed into erecord by JWT (**JSON Web Token**). See :ref:`more information <feat_access>`.

If you want to use a model that is in limited access, you have to identify yourself.

In practice
===========

To send a request about a simulator that is in limited access (:ref:`req_get_vpz_input`, :ref:`req_post_vpz_output`, :ref:`req_post_vpz_report`, :ref:`req_get_slm_download`...), you must identify yourself by giving an available JWT value as a :ref:`opt_jwt` parameter of the request.

So you must previously have acquired an available JWT value : :ref:`auth_howto_get_jwt`.

A JWT value is available only for a limited duration so some days after having acquired it, you will have to ask for a new one.

Illustration
============

**In this fictional example** :

    The user (*username="alphateam"*, *password="ahpla2017"*) has access authorization for some simulators that are in limited access, among which there is the simulator whose Id is *999*.

Get an available JWT value
--------------------------

  **Call** : :ref:`In command line with cURL <post_acs_jwt_obtain_curl>`

  **As a result** :

    An available JWT value, corresponding with user *"alphateam"*, is :

    eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Ind3ZG10ZWFtIiwidXNlcl9pZCI6MiwiZW1haWwiOiIiLCJleHAiOjE0OTg4NjY2NjV9.kQhtSlfY0NWIXnig7p5J-wdq0_p81m5FwQc7gLze6vE

  This JWT value is available for all the simulators that are authorized for user *"alphateam"*, in particular for the simulator whose Id is *999*.

Use JWT value to access the simulator in limited access
-------------------------------------------------------

  With the JWT value previously got, one can send requests about the
  simulator whose Id is *999*, in order to access its parameters values
  and/or simulation results.

  **Calls** :
  :ref:`In command line with cURL and in a webbrowser <jwt_authenticated_requests>`


See also 
========

    :ref:`feat_access` | :ref:`auth_howto_get_jwt`


