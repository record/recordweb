.. _auth_howto_get_jwt:

=================================
How to get a JWT (JSON Web Token)
=================================

Overview
========

You must identify yourself to use a simulator that is in limited access : identification is done thanks to a JWT value that must be given as a :ref:`opt_jwt` parameter of requests about it.

To get a JWT that is available for a simulator in limited access you want to use, you have to send a :ref:`req_post_acs_jwt_obtain` request where you enter some parameters that identify yourself as a user authorized for this simulator.

Procedure
=========

Step 1. Ask for access authorization
------------------------------------

  See :ref:`auth_ask_for_access`

Step 2. Ask for an available JWT value
--------------------------------------

  To get a JWT value, send a :ref:`req_post_acs_jwt_obtain` request with the
  previously given information (*username* and *password*) as parameters values.

    See an :ref:`Example in command line with cURL <post_acs_jwt_obtain_curl>`

  When this JWT value has expired, ask for a new one (Step 2.).

More
====

See :ref:`feat_access` | :ref:`auth`

