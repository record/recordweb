# Configuration file for the Sphinx documentation builder of erecord.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
#sys.path.insert(0, os.path.abspath('.'))

os.environ['DJANGO_SETTINGS_MODULE'] = 'projects.ws.ws.settings'

from django.conf import settings
base_dir = os.path.dirname(os.path.abspath(__file__))
apps_home = os.path.normpath(os.path.join(base_dir, "..", "..", "erecord",
                                          "apps"))
if apps_home not in sys.path :
    sys.path.insert(0, apps_home)
from erecord_cmn.configs.config import PROJECT_HOME
from erecord_cmn.configs.config import PACKAGE_HOME

if PROJECT_HOME not in sys.path :
    sys.path.insert(0, PROJECT_HOME)
if PACKAGE_HOME not in sys.path :
    sys.path.insert(0, PACKAGE_HOME)

import django
django.setup()

# -- Project information -----------------------------------------------------

project = 'erecord'
copyright = '2019-2024 INRA http://www.inra.fr'
author = 'Nathalie Rousse - INRA'

# The full version, including alpha/beta/rc tags
release = '2.0'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [ 'sphinx.ext.autodoc',
             # 'sphinx.ext.doctest', 'sphinx.ext.intersphinx', 'sphinx.ext.coverage', 'sphinx.ext.ifconfig', 'sphinx.ext.viewcode',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
#html_theme = 'alabaster'
html_theme_path = ['_themes']
html_theme = 'erecord' # from agogo

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

