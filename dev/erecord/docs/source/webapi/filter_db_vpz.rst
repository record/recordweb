.. _filter_db_vpz:

===
vpz
===

The 'vpz' option is used to specify which :term:`simulator` to keep (filtering all the other ones). The simulator under consideration is a :ref:`dm_vlevpz`.

- vpz

    - value : id of the relevant :ref:`dm_vlevpz`

.. note::

   To know the existing simulators : :ref:`req_get_db_vpz` resource.

   You will find help in the :ref:`examples <examples>`, especially
   in :ref:`wwdm_ident`.

