.. _req_get_db_vle_id:

=======================
GET db/vle/{Id}
=======================

URL
===

`http://127.0.0.1:8000/db/vle/{Id} <http://127.0.0.1:8000/db/vle/{Id}>`_

Description
===========

Returns the identification information of the :term:`vle version` having
the Id value (as :ref:`dm_vleversion`).

Details
-------

*style*
    .. include:: desc_db_style.rst

Request parameters
==================

* *Id* : id value of the vle version as :ref:`dm_vleversion`

* :ref:`opt_style`

* :ref:`opt_format`

Response result
===============

The returned result is about :ref:`dm_vleversion`.

Example
=======

Example :
:ref:`In a webbrowser <db_resources_webbrowser>` |
:ref:`In command line with cURL <db_resources_curl>` |
:ref:`In Python language <db_resources_python>` |
:ref:`In R language <db_resources_r>` |
:ref:`In PHP language <db_resources_php>` 

