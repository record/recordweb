.. _req_get_db_vle:

=======================
GET db/vle
=======================

URL
===

http://127.0.0.1:8000/db/vle

Description
===========

Returns the identification information of some :term:`vle version`\ s
(as :ref:`dm_vleversion`).

Details
-------

*style*
    .. include:: desc_db_style.rst

Request parameters
==================

* :ref:`filter_db_vle`

* :ref:`opt_style`

* :ref:`opt_format`

Response result
===============

The returned result is about :ref:`dm_vleversion`.

Example
=======

Example :
:ref:`In a webbrowser <db_resources_webbrowser>` |
:ref:`In command line with cURL <db_resources_curl>` |
:ref:`In Python language <db_resources_python>` |
:ref:`In R language <db_resources_r>` |
:ref:`In PHP language <db_resources_php>`

