.. _opt_report:

======
report
======

The 'report' option is used to choose which reports are built into the
returned folder.

* report

    - value 'vpz' : for the vpz file into the report folder.
    - value 'csv' : for csv file(s) into the report folder.
    - value 'xls' : for a xls file into the report folder (several worksheets).
    - value 'txt' : for txt file(s) into the report folder.
    - value 'all' : for all the available files above (txt, csv...) into the
      report folder.

.. note::

   *Some resources for which the report option is available :*

   :ref:`req_get_vpz_report`, :ref:`req_post_vpz_report`.

