.. _filter_db_rep:

===
rep
===

The 'rep' option is used to specify which :term:`models repository` to keep (filtering all the other ones). The models repository under consideration is a :ref:`dm_vlerep`.

- rep

    - value : id of the relevant :ref:`dm_vlerep`

.. note::

   To know the existing models repositories : :ref:`req_get_db_rep` resource

   You will find help in the :ref:`examples <examples>`, especially
   in :ref:`wwdm_ident`.

