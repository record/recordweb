
The posted xls file of the :ref:`req_post_vpz_experiment` request has the same
format and structure than the xls file returned by the
:ref:`req_get_vpz_experiment` request.

