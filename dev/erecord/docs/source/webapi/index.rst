.. _webapi:

=======
Web API
=======

The :term:`erecord` web services allow to edit, modify and simulate some
models developed with :term:`Vle`, such as the :term:`Record` platform ones.

The **'vpz'** resources allow to **edit, modify and run simulators**.

To act on a simulator, it is necessary to identify it into the :ref:`database <databases>`. If the simulator has been recorded into the database as a :ref:`dm_vlevpz` (of a :ref:`dm_vlepkg` of a :ref:`dm_vlerep`) then its Id can be found by the **'db'** resources, that allow to **identify the models repositories, their models and simulators**.  If the simulator has been recorded into the database as a :ref:`dm_vpzpath` then its Id can be found by the **'vpz'** resources.

Models are in **public access** or in **limited access**. **Authentication** is required in limited access case (see :ref:`auth`). The **'acs'** resources allow to **get and verify a token**, to **get information about simulators access** (simulators as :ref:`dm_vlevpz` or as :ref:`dm_vpzpath`).

.. _webapi_vpz:

vpz resources
=============

To edit, modify, run simulators
-------------------------------

- **Edit, modify, run a vpz** :

=========================== ===================================== ===
Resource                    Description 
=========================== ===================================== ===
:ref:`req_get_vpz_input`    to see a simulator
--------------------------- ------------------------------------- ---
:ref:`req_post_vpz_input`   to see a modified simulator           :ref:`Try <url_input_menu>`
--------------------------- ------------------------------------- ---
:ref:`req_get_vpz_output`   to run a simulator
--------------------------- ------------------------------------- ---
:ref:`req_post_vpz_output`  to run a modified simulator           :ref:`Try <url_output_menu>`
--------------------------- ------------------------------------- ---
:ref:`req_get_vpz_inout`    to see and run a simulator
--------------------------- ------------------------------------- ---
:ref:`req_post_vpz_inout`   to see and run a modified simulator   :ref:`Try <url_inout_menu>`
=========================== ===================================== ===

- **Experiment plan by xls files** :

=============================== ======================================== ====
Resource                        Description 
=============================== ======================================== ====
:ref:`req_get_vpz_experiment`   to see a simulator by xls file           :ref:`Try <url_inoutmodify_menu>` ('first step')
------------------------------- ---------------------------------------- ----
:ref:`req_post_vpz_experiment`  to run a modified simulator by xls file  :ref:`Try <url_inoutmodify_menu>`
=============================== ======================================== ====

- **File reports on a vpz** :

===================================== =========================================
Resource                              Description 
===================================== =========================================
:ref:`req_get_vpz_report`             for reports about a simulator
                                      (conditions and results)
------------------------------------- -----------------------------------------
:ref:`req_post_vpz_report`            for reports about a modified simulator 
                                      (conditions and results)
------------------------------------- -----------------------------------------
:ref:`req_get_vpz_report_conditions`  for reports about a simulator conditions
------------------------------------- -----------------------------------------
:ref:`req_post_vpz_report_conditions` for reports about a modified simulator conditions
===================================== =========================================

To identify simulators
----------------------

- **Lists and details** (simulators as :ref:`dm_vpzpath`) :

==============================  ===============================================
Resource                        Description 
==============================  ===============================================
:ref:`req_get_vpz_vpzpath`      to identify some simulators (as :ref:`dm_vpzpath`)
------------------------------  -----------------------------------------------
:ref:`req_get_vpz_vpzpath_id`   to identify a simulator (as :ref:`dm_vpzpath`)
==============================  ===============================================

.. _webapi_db:

db resources
============

To identify vle versions, models repositories, models, simulators
-----------------------------------------------------------------

- **Lists and details** :

===========================  ==============================================
Resource                     Description 
===========================  ==============================================
:ref:`req_get_db_vle`        to identify some vle versions
---------------------------  ----------------------------------------------
:ref:`req_get_db_vle_id`     to identify a vle version
---------------------------  ----------------------------------------------
:ref:`req_get_db_rep`        to identify some models repositories
---------------------------  ----------------------------------------------
:ref:`req_get_db_rep_id`     to identify a models repository
---------------------------  ----------------------------------------------
:ref:`req_get_db_pkg`        to identify some models
---------------------------  ----------------------------------------------
:ref:`req_get_db_pkg_id`     to identify a model
---------------------------  ----------------------------------------------
:ref:`req_get_db_vpz`        to identify some simulators
---------------------------  ----------------------------------------------
:ref:`req_get_db_vpz_id`     to identify a simulator
===========================  ==============================================

- **Names, List of data files** :

=================================== ==========================================
Resource                            Description 
=================================== ==========================================
:ref:`req_get_db_rep_id_name`       to get the name of a models repository
----------------------------------- ------------------------------------------
:ref:`req_get_db_pkg_id_name`       to get the name of a model
----------------------------------- ------------------------------------------
:ref:`req_get_db_pkg_id_datalist`   to get the list of names of data files of a model
----------------------------------- ------------------------------------------
:ref:`req_get_db_vpz_id_name`       to get the name of a simulator
=================================== ==========================================

.. _webapi_slm:

slm resources
=============

To download some result files
-----------------------------

============================= ==========================================
Resource                      Description 
============================= ==========================================
:ref:`req_get_slm_download`   to download a result file (coming from a previous request)
============================= ==========================================

.. _webapi_acs:

acs resources
=============

To manage simulators access (public or limited)
-----------------------------------------------

- **Authentication** by **JWT (JSON Web Token)** for limited access case :

=================================== ============================
Resource                            Description 
=================================== ============================
:ref:`req_post_acs_jwt_obtain`      to get a JWT value
----------------------------------- ----------------------------
:ref:`req_post_acs_jwt_verify`      to verify a JWT value
=================================== ============================

- **access information** for simulators as :ref:`dm_vlevpz` :

===================================== ======================================
Resource                              Description 
===================================== ======================================
:ref:`req_get_acs_vpz_id_access`      to get the access type (limited or public) of a simulator (vpz) 
------------------------------------- --------------------------------------
:ref:`req_get_acs_vpz_id_user_id`     to get the list of id of a simulator (vpz) authorized users
------------------------------------- --------------------------------------
:ref:`req_get_acs_vpz_id_user_name`   to get the list of name of a simulator (vpz) authorized users
------------------------------------- --------------------------------------
:ref:`req_get_acs_vpz_accessible_id`  to get the list of id of the accessible simulators (vpz)
===================================== ======================================

- **access information** for simulators as :ref:`dm_vpzpath` :

========================================== =================================
Resource                                   Description 
========================================== =================================
:ref:`req_get_acs_vpzpath_id_access`       to get the access type (limited or public) of a simulator (vpzpath)
------------------------------------------ ---------------------------------
:ref:`req_get_acs_vpzpath_id_user_id`      to get the list of id of a simulator (vpzpath) authorized users
------------------------------------------ ---------------------------------
:ref:`req_get_acs_vpzpath_id_user_name`    to get the list of name of a simulator (vpzpath) authorized users
------------------------------------------ ---------------------------------
:ref:`req_get_acs_vpzpath_accessible_id`   to get the list of id of the accessible simulators (vpzpath)
========================================== =================================

