.. _req_post_acs_jwt_verify:

=======================
POST acs/jwt/verify
=======================

URL
===

http://127.0.0.1:8000/acs/jwt/verify

:ref:`Try it <url_jwt_verify>`

Description
===========

Checks the veracity of a token (JSON Web Token) value, returning the token
value if it is valid.

Help
----

A JWT value is available for a limited duration. For more, see
:ref:`JWT in practice <auth>`.

Request parameters
==================

**Required** : token.

* *token* : JWT value to be verified.

Response result
===============

"token" : the returned result is the JWT value if it is valid.

Example
=======

*Under construction*

