.. _opt_format:

======
format
======

The 'format' option is used to choose the format.

All the following format values are not supported in all the situations.

* format
    
    - value api
    - value json
    - value yaml
    - value xml
    - value html

