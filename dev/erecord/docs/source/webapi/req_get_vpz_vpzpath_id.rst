.. _req_get_vpz_vpzpath_id:

=======================
GET vpz/vpzpath/{Id}
=======================

URL
===

`http://127.0.0.1:8000/vpz/vpzpath/{Id} <http://127.0.0.1:8000/vpz/vpzpath/{Id}>`_

Description
===========

Returns the identification information of the :term:`simulator` (as :ref:`dm_vpzpath`) having the Id value (as :ref:`dm_vpzpath`).

Details
-------

*style* (TO BE CONFIRMED)
    .. include:: desc_db_style.rst

Request parameters
==================

Response result
===============

Example
=======

*Under construction*

