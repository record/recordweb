.. _opt_key:

===
key
===

The 'key' option is used to identify a downloadable file.

- key

    - value : value of the key that identifies the desired downloadable file.

.. note::

   *Where such a key can be found, where does it come from ?*

   The resources having :ref:`opt_todownload` option allow to ask for
   downloading a result file later on, instead of receiving the result
   directly into the request response. In that case, the request response has
   returned a **key that identifies the downloadable file** and that will then
   be required to ask for downloading it (see :ref:`req_get_slm_download`).

