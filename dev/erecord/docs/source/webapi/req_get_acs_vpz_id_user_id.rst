.. _req_get_acs_vpz_id_user_id:

========================
GET acs/vpz/{Id}/user/id
========================

URL
===

http://127.0.0.1:8000/acs/vpz/{Id}/user/id

Description
===========

Returns the list of id of users authorized for a :term:`simulator` (as :ref:`dm_vlevpz`) having the Id value (as :ref:`dm_vlevpz`).

Request parameters
==================

* *Id* : id value of the simulator as :ref:`dm_vlevpz`

* :ref:`opt_format`

Response result
===============

"list" : list of users ids.

Example
=======

*Under construction*

