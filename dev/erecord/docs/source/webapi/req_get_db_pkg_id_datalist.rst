.. _req_get_db_pkg_id_datalist:

========================
GET db/pkg/{Id}/datalist
========================

URL
===

http://127.0.0.1:8000/db/pkg/id/datalist

Description
===========

Returns the list of names of data files of the :term:`vle model` having the Id value (as :ref:`dm_vlepkg`). Data files are those present into its :term:`vle package` default data folder.

Request parameters
==================

* *Id* : id value of the vle model as :ref:`dm_vlepkg`

* :ref:`opt_format`

Response result
===============

"datalist" : list of data file names.

Example
=======

*Under construction*

