.. _filter_db_pkg:

===
pkg
===

The 'pkg' option is used to specify which :term:`vle model` to keep (filtering all the other ones). The vle model under consideration is a :ref:`dm_vlepkg`.

- pkg

    - value : id of the relevant :ref:`dm_vlepkg`

.. note::

   To know the existing vle models : :ref:`req_get_db_pkg` resource.

   You will find help in the :ref:`examples <examples>`, especially
   in :ref:`wwdm_ident`.

