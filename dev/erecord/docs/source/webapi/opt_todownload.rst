.. _opt_todownload:

===================
mode for todownload
===================

The :ref:`'mode' option <opt_mode>` is used to choose whether the content
result is going to be directly sent or will have to be downloaded later on.

Value 'todownload' (mode=todownload) for the content result to be downloaded.

.. note::

   *How to download a result file later on ?*

   When a request has asked for downloading a result file later on
   (mode=todownload), the request response has returned a **key that
   identifies the downloadable result file**.

   This key value will then be required as :ref:`opt_key` option of the
   resource :ref:`req_get_slm_download` that allows to download the result file.

.. note::

   *Some resources having this option :*

   :ref:`req_get_vpz_report_conditions`, :ref:`req_post_vpz_report_conditions`,
   :ref:`req_get_vpz_report`, :ref:`req_post_vpz_report`,
   :ref:`req_get_vpz_experiment`, :ref:`req_post_vpz_experiment`.

