.. _opt_style:

==============
mode for style
==============

The :ref:`'mode' option <opt_mode>` is used to select the style presentation.

* mode 

    - value 'link' (mode=link) for presentation by links.
    - value 'tree' (mode=tree) for presentation by opening the tree.
    - value 'compact' (mode=compact).
    - value 'compactlist' (mode=compactlist).

.. note:: 'style' can also be used as option name (like 'mode') :
   style=link, style=tree, style=compact, style=compactlist.

The values 'link' and 'tree' are the main style values.

In 'link' value case, the information given as links is accessible only if the
erecord software has been set to make it possible.

The values 'compact' and 'compactlist' are two other style values, that are
not always available.

.. note::
   'compact' and 'compactlist' values can be helpful to prepare a following 
   command (where parameters modification, output datas selection...), can be
   handy to then exploit the returned datas (output datas, parameters...). 

.. note::
   see also :ref:`opt_format`

.. note::

   *Some resources for which the mode option is available to choose a style 
   value into the main values 'link' and 'tree' :*

   :ref:`req_get_db_rep`, :ref:`req_get_db_rep_id`,
   :ref:`req_get_db_pkg`, :ref:`req_get_db_pkg_id`,
   :ref:`req_get_db_vpz`, :ref:`req_get_db_vpz_id`,

   *Some resources for which the mode option is available to choose a style 
   value into 'link', 'tree', 'compact', 'compactlist' :*

   :ref:`req_get_vpz_input`, :ref:`req_post_vpz_input`,
   :ref:`req_get_vpz_output`, :ref:`req_post_vpz_output`,
   :ref:`req_get_vpz_inout`, :ref:`req_post_vpz_inout`,

