.. _req_get_vpz_input:

=======================
GET vpz/input
=======================

URL
===

http://127.0.0.1:8000/vpz/input

Description
===========

Returns the **input conditions** of a simulator (see :term:`input information of a vpz`).

Details
-------

*jwt*
    .. include:: desc_acs_jwt.rst

*style*
    .. include:: desc_vpz_style_input.rst


Request parameters
==================

**Required** : either vpz or vpzpath must be provided.

**Required** : jwt in limited access case.

* :ref:`opt_vpz_choice`

* :ref:`opt_parselect`

* :ref:`opt_style`

* :ref:`opt_format`

* :ref:`opt_jwt`

Response result
===============

The returned result is about :ref:`dm_vpzinput`.

Example
=======

Example :
:ref:`In command line with cURL <jwt_authenticated_requests>` (Authenticated request 1.)

