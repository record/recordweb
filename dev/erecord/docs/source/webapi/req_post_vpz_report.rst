.. _req_post_vpz_report:

=======================
POST vpz/report
=======================

URL
===

http://127.0.0.1:8000/vpz/report

Description
===========

Maybe modifies the input conditions of a simulator (see :term:`input information of a vpz`) *(depending on the request parameters)*.
In addition with this input conditions modification (optional), maybe modifies the **input datas folder** of the simulator according to a posted file (optional).
Then runs the simulation, then produces and returns the required **reports**,
from the **simulation results** (see :term:`output information of a vpz`) and
also the **input conditions** (see :term:`input information of a vpz`).

Details
-------

*jwt*
    .. include:: desc_acs_jwt.rst

*input information*

    .. include:: desc_vpz_modify_input.rst

*input datas folder*
    .. include:: desc_vpz_modify_datafolder.rst

*filtering*

    It is possible to ask for filtering some of the returned information (see
    'parselect', 'outselect').

*reports*

    The built reports are gathered and returned into a '.zip' file.

Request parameters
==================

**Required** : either vpz or vpzpath must be provided.

**Required** : jwt in limited access case.

* :ref:`opt_vpz_choice`

* :ref:`opt_plan_restype`

* :ref:`opt_begin`
* :ref:`opt_duration`
* :ref:`opt_pars`

* :ref:`opt_datafolder`
* :ref:`opt_datafoldercopy`

* :ref:`opt_parselect`
* :ref:`opt_outselect`

* :ref:`opt_report`
* :ref:`opt_bycol`
* :ref:`opt_todownload`
* :ref:`opt_format`

* :ref:`opt_jwt`

Response result
===============

The returned result is reports about both :ref:`dm_vpzoutput` and
:ref:`dm_vpzinput`.

Example
=======

- Examples in command line with cURL :

  .. literalinclude:: ../examples/report/req_post_vpz_report_160.txt

- Another example :
  :ref:`In command line with cURL <jwt_authenticated_requests>`
  (Authenticated request 3.)

- Example modifying the input datas folder (using :ref:`opt_datafolder`, :ref:`opt_datafoldercopy`) :

  .. literalinclude:: ../examples/include/intro_datafolder.txt

  .. literalinclude:: ../examples/report/req_post_vpz_report_160_3.txt

