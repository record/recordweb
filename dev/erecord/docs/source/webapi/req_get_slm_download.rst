.. _req_get_slm_download:

=======================
GET slm/download
=======================

URL
===

http://127.0.0.1:8000/slm/download

Description
===========

Returns the downloadable file that is identified by 'key' value.

.. note::
   A downloadable file comes from a previous request (*such as*
   :ref:`req_get_vpz_report_conditions`, :ref:`req_post_vpz_report`,
   :ref:`req_post_vpz_experiment`...) that had been sent by asking to download
   the result file later on (see :ref:`opt_todownload` option).

Request parameters
==================

**Required** : key must be provided.

**Required** : jwt in limited access case.

* :ref:`opt_key`

* :ref:`opt_jwt`

Response result
===============

The returned result is the requested downloadable file.

Example
=======

Example :
:ref:`In a webbrowser <jwt_authenticated_requests>` (Authenticated request 4.)

