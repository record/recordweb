.. _req_get_db_vpz:

=======================
GET db/vpz
=======================

URL
===

http://127.0.0.1:8000/db/vpz

Description
===========

Returns the identification information of some :term:`simulator`\ s (as :ref:`dm_vlevpz`).

Details
-------

*style*
    .. include::desc_db_style.rst

Request parameters
==================

* :ref:`filter_db_rep`

* :ref:`filter_db_pkg`

* :ref:`filter_db_vpz`

* :ref:`opt_style` 

* :ref:`opt_format` 

Response result
===============

The returned result is about :ref:`dm_vlevpz`.

Example
=======

Example :
:ref:`In a webbrowser <db_resources_webbrowser>` |
:ref:`In command line with cURL <db_resources_curl>` |
:ref:`In Python language <db_resources_python>` |
:ref:`In R language <db_resources_r>` |
:ref:`In PHP language <db_resources_php>` 

See also use cases into : :ref:`example about simulator identification <wwdm_ident>`

