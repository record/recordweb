.. _opt_begin:

=====
begin
=====

* begin
    
    - value : the new experiment begin value.

.. note::

   *Some resources for which the begin option is available :*

   :ref:`req_post_vpz_input`,
   :ref:`req_post_vpz_output`,
   :ref:`req_post_vpz_inout`,
   :ref:`req_post_vpz_report`,
   :ref:`req_post_vpz_report_conditions`.

