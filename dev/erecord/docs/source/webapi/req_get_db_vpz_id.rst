.. _req_get_db_vpz_id:

=======================
GET db/vpz/{Id}
=======================

URL
===

`http://127.0.0.1:8000/db/vpz/{Id} <http://127.0.0.1:8000/db/vpz/{Id}>`_

Description
===========

Returns the identification information of the :term:`simulator` (as :ref:`dm_vlevpz`) having the Id value (as :ref:`dm_vlevpz`).

Details
-------

*style*
    .. include:: desc_db_style.rst

Request parameters
==================

* *Id* : id value of the simulator as :ref:`dm_vlevpz`

* :ref:`opt_style`

* :ref:`opt_format`

Response result
===============

The returned result is about :ref:`dm_vlevpz`.

Example
=======

Example :
:ref:`In a webbrowser <db_resources_webbrowser>` |
:ref:`In command line with cURL <db_resources_curl>` |
:ref:`In Python language <db_resources_python>` |
:ref:`In R language <db_resources_r>` |
:ref:`In PHP language <db_resources_php>` 

See also use cases into : :ref:`example about simulator identification <wwdm_ident>`

