.. _opt_vpz_choice:

==============
vpz or vpzpath
==============

The 'vpz' and 'vpzpath' options are used to choose a vpz. The vpz under consideration is a :term:`vpz (as VpzAct)`. Its source information is either a :ref:`dm_vlevpz` ('vpz' option case) or a :ref:`dm_vpzpath` ('vpzpath' option case).

The vpz is selected by one and only one of those options : vpz, vpzpath.

- vpz

    - value : id of the relevant :ref:`dm_vlevpz`

.. note::

   To know the existing :ref:`dm_vlevpz` : :ref:`req_get_db_vpz` resource.

   You will find help in the :ref:`examples <examples>`, especially
   in :ref:`wwdm_ident`.


- vpzpath

    - value : id of the relevant :ref:`dm_vpzpath`

.. note::
   To know the existing :ref:`dm_vpzpath` : :ref:`req_get_vpz_vpzpath` resource.

