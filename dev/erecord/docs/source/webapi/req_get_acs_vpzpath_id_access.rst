.. _req_get_acs_vpzpath_id_access:

===========================
GET acs/vpzpath/{Id}/access
===========================

URL
===

http://127.0.0.1:8000/acs/vpzpath/{Id}/access

Description
===========

Returns the access type of the :term:`simulator` (as :ref:`dm_vpzpath`)
having the Id value (as :ref:`dm_vpzpath`).

Request parameters
==================

* *Id* : id value of the simulator as :ref:`dm_vpzpath`

* :ref:`opt_format`

Response result
===============

* :ref:`access <val_access_type>`

Example
=======

*Under construction*

