.. _opt_duration:

========
duration
========

* duration

    - value : the new experiment duration value.

.. note::

   *Some resources for which the duration option is available :*

   :ref:`req_post_vpz_input`,
   :ref:`req_post_vpz_output`,
   :ref:`req_post_vpz_inout`,
   :ref:`req_post_vpz_report`,
   :ref:`req_post_vpz_report_conditions`.

