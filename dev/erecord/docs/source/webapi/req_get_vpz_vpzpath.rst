.. _req_get_vpz_vpzpath:

=======================
GET vpz/vpzpath
=======================

URL
===

http://127.0.0.1:8000/vpz/vpzpath

Description
===========

Returns the identification information of some :term:`simulator`\ s (as :ref:`dm_vpzpath`).


Details
-------

*style* (TO BE CONFIRMED)
    .. include::desc_db_style.rst


Request parameters
==================

Response result
===============

Example
=======

*Under construction*

