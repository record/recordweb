
In addition to the style values 'link' and 'tree', the 'compact' and
'compactlist' values are also available.

The 'compact' value is for a compact presentation keeping only some useful
parts of information.

The 'compactlist' value is for a presentation even compacter than with
'compact' style, where in particular the parameters are named by their
'selection_name'.

.. note::
   Values ordered from the fuller to the lighter style presentation : 'tree',
   'compact', 'compactlist'.

