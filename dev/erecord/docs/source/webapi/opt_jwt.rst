.. _opt_jwt:

===
jwt
===

The 'jwt' option is used to give an available JWT value, required in limited
access case.

- jwt

    - value : an available JWT (JSON Web Token) value.

.. note::

   To get a JWT value : :ref:`req_post_acs_jwt_obtain` request.

   See also : :ref:`auth_howto_get_jwt`

   For more : :ref:`auth`

