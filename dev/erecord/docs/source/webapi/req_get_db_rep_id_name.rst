.. _req_get_db_rep_id_name:

=======================
GET db/rep/{Id}/name
=======================

URL
===

http://127.0.0.1:8000/db/rep/{Id}/name

Description
===========

Returns the name of the :term:`models repository` having the Id value (as
:ref:`dm_vlerep`).

Request parameters
==================

* *Id* : id value of the models repository as :ref:`dm_vlerep`

* :ref:`opt_format`

Response result
===============

"name" : name of the models repository as :ref:`dm_vlerep`

Example
=======

*Under construction*

