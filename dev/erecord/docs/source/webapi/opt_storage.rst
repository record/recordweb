.. _opt_storage:

================
mode for storage
================

The :ref:`'mode' option <opt_mode>` is used to choose whether the
:ref:`web storage capability <feat_storage>` (if supported by the web
browser) is used or not.

Value 'storage' (mode=storage) for the web storage to be used.

.. note::

   *Some resources for which the mode option is available to choose storage :*

   :ref:`req_post_vpz_output`.

