.. _req_get_db_rep:

=======================
GET db/rep
=======================

URL
===

http://127.0.0.1:8000/db/rep

Description
===========

Returns the identification information of some :term:`models repository`\ s
(as :ref:`dm_vlerep`).

Details
-------

*style*
    .. include:: desc_db_style.rst

Request parameters
==================

* :ref:`filter_db_rep`

* :ref:`opt_style`

* :ref:`opt_format`

Response result
===============

The returned result is about :ref:`dm_vlerep`.

Example
=======

Example :
:ref:`In a webbrowser <db_resources_webbrowser>` |
:ref:`In command line with cURL <db_resources_curl>` |
:ref:`In Python language <db_resources_python>` |
:ref:`In R language <db_resources_r>` |
:ref:`In PHP language <db_resources_php>`

See also use cases into : :ref:`example about simulator identification <wwdm_ident>`

