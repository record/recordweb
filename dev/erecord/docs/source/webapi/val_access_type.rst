.. _val_access_type:

===========
access type
===========

An access can be 'public' for a public access case or 'limited' for a limited access case.


For more see :ref:`feat_access` | :ref:`auth`.

