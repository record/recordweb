
For example the :download:`posted xls file<../examples/experiment/post/in/experiment.xls>` of a :ref:`req_post_vpz_experiment` request has been written from the :download:`xls file<../examples/experiment/get/out/experiment.xls>` returned by a :ref:`req_get_vpz_experiment` request.

