.. _req_post_vpz_output:

=======================
POST vpz/output
=======================

URL
===

http://127.0.0.1:8000/vpz/output

:ref:`Try it <url_output_menu>`

Description
===========

Maybe modifies the input conditions of a simulator (see :term:`input information of a vpz`) *(depending on the request parameters)*.
In addition with this input conditions modification (optional), maybe modifies the **input datas folder** of the simulator according to a posted file (optional).
Then runs the simulation and returns the **simulation results** (see :term:`output information of a vpz`).

Details
-------

*jwt*
    .. include:: desc_acs_jwt.rst

*input information*

    .. include:: desc_vpz_modify_input.rst

*input datas folder*
    .. include:: desc_vpz_modify_datafolder.rst

*filtering*

    It is possible to ask for filtering some of the returned information (see
    'outselect').

*style*
    .. include:: desc_vpz_style_output.rst

*style, restype*
    .. include:: desc_vpz_style_and_restype.rst

Request parameters
==================

**Required** : either vpz or vpzpath must be provided.

**Required** : jwt in limited access case.

* :ref:`opt_vpz_choice`

* :ref:`opt_plan_restype`

* :ref:`opt_begin`
* :ref:`opt_duration`
* :ref:`opt_pars`

* :ref:`opt_datafolder`
* :ref:`opt_datafoldercopy`

* :ref:`opt_outselect`

* :ref:`opt_style`

* :ref:`opt_storage`

* :ref:`opt_format`

* :ref:`opt_jwt`

Response result
===============

The returned result is about :ref:`dm_vpzoutput`.

Example
=======

Example : 
:ref:`In a webbrowser <post_vpz_output_webbrowser>` |
:ref:`In command line with cURL <post_vpz_output_curl>` |
:ref:`In Python language <post_vpz_output_python>` |
:ref:`In R language <post_vpz_output_r>` |
:ref:`In PHP language <post_vpz_output_php>`

Example :
:ref:`In command line with cURL <jwt_authenticated_requests>` (Authenticated request 2.)

