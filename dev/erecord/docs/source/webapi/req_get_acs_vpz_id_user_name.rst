.. _req_get_acs_vpz_id_user_name:

==========================
GET acs/vpz/{Id}/user/name
==========================

URL
===

http://127.0.0.1:8000/acs/vpz/{Id}/user/name

Description
===========

Returns the list of username of users authorized for a :term:`simulator` (as :ref:`dm_vlevpz`) having the Id value (as :ref:`dm_vlevpz`).

Request parameters
==================

* *Id* : id value of the simulator as :ref:`dm_vlevpz`

* :ref:`opt_format`

Response result
===============

"list" : list of users username.

Example
=======

*Under construction*

