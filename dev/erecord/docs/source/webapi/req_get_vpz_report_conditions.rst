.. _req_get_vpz_report_conditions:

=========================
GET vpz/report/conditions
=========================

URL
===

http://127.0.0.1:8000/vpz/report/conditions

Description
===========

Produces and returns the required **reports**, from the **input conditions**
of a simulator (see :term:`input information of a vpz`).

Details
-------

*jwt*
    .. include:: desc_acs_jwt.rst

*reports*

    The built reports are gathered and returned into a '.zip' file.

Request parameters
==================

**Required** : either vpz or vpzpath must be provided.

**Required** : jwt in limited access case.

* :ref:`opt_vpz_choice`

* :ref:`opt_bycol`
* :ref:`opt_todownload`
* :ref:`opt_format`

* :ref:`opt_jwt`

Response result
===============

The returned result is reports about the simulation conditions (from
:ref:`dm_vpzinput`).

Example
=======

.. literalinclude:: ../examples/report/req_get_vpz_report_conditions_163.txt

