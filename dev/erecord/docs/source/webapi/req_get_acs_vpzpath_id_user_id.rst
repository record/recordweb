.. _req_get_acs_vpzpath_id_user_id:

============================
GET acs/vpzpath/{Id}/user/id
============================

URL
===

http://127.0.0.1:8000/acs/vpzpath/{Id}/user/id

Description
===========

Returns the list of id of users authorized for a :term:`simulator` (as :ref:`dm_vpzpath`) having the Id value (as :ref:`dm_vpzpath`).

Request parameters
==================

* *Id* : id value of the simulator as :ref:`dm_vpzpath`

* :ref:`opt_format`

Response result
===============

"list" : list of users ids.

Example
=======

*Under construction*

