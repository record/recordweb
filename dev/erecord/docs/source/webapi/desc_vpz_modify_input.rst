
It is possible to modify some of the :term:`vpz input information` (see 'pars',
'begin'...) and in that case the vpz taken into consideration is the vpz
updated by the modifications.

