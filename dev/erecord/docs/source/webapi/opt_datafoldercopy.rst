.. _opt_datafoldercopy:

==============
datafoldercopy
==============

The 'datafoldercopy' option is used to choose the way how to take into account
the new input datas folder posted by the user (see :ref:`opt_datafolder`
parameter).

* datafoldercopy

    - value 'replace' : the new input datas folder replaces the original one.

    - value 'overwrite' : the content of the new input datas folder is added
      to the original one (by overwriting).

.. note::

   *Some resources for which the datafoldercopy option is available to choose the way how to take into account the posted input datas folder :*

   :ref:`req_post_vpz_output`,
   :ref:`req_post_vpz_inout`,
   :ref:`req_post_vpz_experiment`,
   :ref:`req_post_vpz_report`.

