.. _req_get_db_pkg_id_name:

=======================
GET db/pkg/{Id}/name
=======================

URL
===

http://127.0.0.1:8000/db/pkg/{Id}/name

Description
===========

Returns the name of the :term:`vle model` having the Id value (as :ref:`dm_vlepkg`).  

Request parameters
==================

* *Id* : id value of the vle model as :ref:`dm_vlepkg`

* :ref:`opt_format`

Response result
===============

"name" : name of of the vle model as :ref:`dm_vlepkg`.

Example
=======

*Under construction*

