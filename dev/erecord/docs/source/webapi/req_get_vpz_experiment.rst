.. _req_get_vpz_experiment:

=======================
GET vpz/experiment
=======================

URL
===

http://127.0.0.1:8000/vpz/experiment

Description
===========

Returns **a xls file** containing the **experiment conditions** of the
simulator (see :term:`input information of a vpz`).

Details
-------

*jwt*
    .. include:: desc_acs_jwt.rst

The **returned file** is a xls file (see below
:ref:`get_vpz_experiment_returned_file`).

Help
----

See :ref:`an example about how to know the simulators that can be chosen
with the 'vpz' option <wwdm_ident>`.

A :ref:`req_get_vpz_experiment` command may be useful before a
:ref:`req_post_vpz_experiment` command, in order to easierly build the xls
file that is required into :ref:`req_post_vpz_experiment` request. Indeed :

    - .. include:: experimentfile_format.rst
    - .. include:: experimentfile_help.rst
    - .. include:: experimentfile_example.rst

Request parameters
==================

**Required** : either vpz or vpzpath must be provided.

**Required** : jwt in limited access case.

* :ref:`opt_vpz_choice`
* :ref:`opt_todownload`
* :ref:`opt_format`

* :ref:`opt_jwt`

Response result
===============

The returned result is a file report about :ref:`dm_vpzinput`.

.. _get_vpz_experiment_returned_file:

The returned file
-----------------

  The **returned xls file** contains the experiment conditions of the
  simulator :

    - *General information* :
      plan
      *(with a default value among
      the 'single' and 'linear' available values)*,
      restype
      *(with a default value among
      the 'dataframe' and 'matrix' available values)*,
      begin, duration.

    - *Parameters values* : list of all the parameters values.

    - *Parameters identification* : list of all the parameters.

    - *Output datas identification* : list of the output datas.

  Limit : in case of xls file overflow
  *(sheets limited to 256 columns and 65536 rows)* the results will be
  truncated (without any warning).

  :download:`Example of a returned xls file<../examples/experiment/get/out/experiment.xls>`

Example
=======

Example : :ref:`In command line with cURL <example_experiment>` ('first step').

Example : Into menus of the ":ref:`url_inoutmodify_menu`" web page, the
*'First step : download the xls file to fill in'* consists in a
:ref:`req_get_vpz_experiment` request.

