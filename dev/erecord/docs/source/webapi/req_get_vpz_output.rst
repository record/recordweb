.. _req_get_vpz_output:

=======================
GET vpz/output
=======================

URL
===

http://127.0.0.1:8000/vpz/output

Description
===========

Returns the **simulation results** of a simulator (see :term:`output information of a vpz`) after having run simulation.

Details
-------

*jwt*
    .. include:: desc_acs_jwt.rst

*style*
    .. include:: desc_vpz_style_output.rst

*style, restype*
    .. include:: desc_vpz_style_and_restype.rst

Request parameters
==================

**Required** : either vpz or vpzpath must be provided.

**Required** : jwt in limited access case.

* :ref:`opt_vpz_choice`

* :ref:`opt_plan_restype`

* :ref:`opt_style`

* :ref:`opt_format`

* :ref:`opt_jwt`

Response result
===============

The returned result is about :ref:`dm_vpzoutput`.

Example
=======

*Under construction*

