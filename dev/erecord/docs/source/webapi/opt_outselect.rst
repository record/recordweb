.. _opt_outselect:

=========
outselect
=========

The 'outselect' option is used to choose the restituted output datas.

* outselect : 

    - value 'all' : to select all output datas of all views.

    - value viewname : to select all output datas of the view named viewname.

    - value vname.oname : to select the ouput data named oname of the view
      named vname (the output datas of this view, that are not selected like
      this, are unselected).

.. note::

   *Some resources for which the outselect option is available :*

   :ref:`req_post_vpz_output`,
   :ref:`req_post_vpz_inout`,
   :ref:`req_post_vpz_input` (verification case),
   :ref:`req_post_vpz_report`.

.. note::
   See :ref:`feat_outputdatas`

