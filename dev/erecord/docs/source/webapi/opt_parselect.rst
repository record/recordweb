.. _opt_parselect:

=========
parselect
=========

The 'parselect' option is used to choose the restituted parameters.

* parselect

    - value 'all' : to select all parameters of all conditions.

    - value condname : to select all parameters of the condition named condname.

    - value cname.pname : to select the parameter named pname of the condition
      named cname (the parameters of this condition, that are not selected
      like this, are unselected).

.. note::

   *Some resources for which the outselect option is available :*

   :ref:`req_get_vpz_input`,
   :ref:`req_post_vpz_input`,
   :ref:`req_post_vpz_inout`,
   :ref:`req_post_vpz_report`,
   :ref:`req_post_vpz_report_conditions`.

.. note::
   See :ref:`feat_parameters`

