.. _opt_datafolder:

==========
datafolder
==========

The 'datafolder' parameter is the path of the posted **zip file** containing
the **new input datas folder** to be taken into account.

The new input datas folder  **must be named 'data'**.

The original input datas folder of the simulator will be replaced or
overwritten by the new one, depending on the
':ref:`opt_datafoldercopy`' option value.

.. note::
   Anything else than the folder named 'data', that may be contained into
   the posted zip file **will be ignored** *(files, folders...)*.

.. note::
   The **user** must ensure that datas files delivered into the new
   input datas folder corresponds with **what the simulator expects**
   (format, values...).

