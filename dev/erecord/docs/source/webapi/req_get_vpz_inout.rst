.. _req_get_vpz_inout:

=======================
GET vpz/inout
=======================

URL
===

http://127.0.0.1:8000/vpz/inout

Description
===========

Returns the **simulation results** of a simulator (see :term:`output information of a vpz`) after having run simulation, and returns also its **input conditions** (see :term:`input information of a vpz`).

Details
-------

*jwt*
    .. include:: desc_acs_jwt.rst

*style*
    .. include:: desc_vpz_style_inout.rst

Request parameters
==================

**Required** : either vpz or vpzpath must be provided.

**Required** : jwt in limited access case.

* :ref:`opt_vpz_choice`

* :ref:`opt_plan_restype`

* :ref:`opt_style`
* :ref:`opt_format`

* :ref:`opt_jwt`

Response result
===============

The returned result is about both :ref:`dm_vpzoutput` and :ref:`dm_vpzinput`.

Example
=======

*Under construction*

