.. _opt_access:

======
access
======

* access 

    - value 'public' for a public access case.
    - value 'limited' for a limited access case.

For more see :ref:`feat_access` | :ref:`auth`.

