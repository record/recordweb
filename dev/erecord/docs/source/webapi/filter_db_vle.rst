.. _filter_db_vle:

===
vle
===

The 'vle' option is used to specify which :term:`vle version` to keep (filtering all the other ones). The vle version under consideration is a :ref:`dm_vleversion`.

- vle

    - value : id of the relevant :ref:`dm_vleversion`

.. note::

   To know the existing vle versions : :ref:`req_get_db_vle` resource

