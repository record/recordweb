.. _req_post_vpz_input:

=======================
POST vpz/input
=======================

URL
===

http://127.0.0.1:8000/vpz/input

:ref:`Try it <url_input_menu>`

Description
===========

Returns the **input conditions** of a simulator (see :term:`input information of a vpz`), maybe after modifications *(depending on the request parameters)*.

Details
-------

*jwt*
    .. include:: desc_acs_jwt.rst

*input information*

    .. include:: desc_vpz_modify_input.rst

*filtering*

    It is possible to ask for filtering some of the returned information (see
    'parselect').

*style*
    .. include:: desc_vpz_style_input.rst

Request parameters
==================

**Required** : either vpz or vpzpath must be provided.

**Required** : jwt in limited access case.

* :ref:`opt_vpz_choice`

* :ref:`opt_begin`
* :ref:`opt_duration`
* :ref:`opt_pars`

* :ref:`opt_parselect`
* :ref:`opt_outselect`

* :ref:`opt_style`

* :ref:`opt_format`

* :ref:`opt_jwt`

Response result
===============

The returned result is about :ref:`dm_vpzinput`.

Example
=======

Example : 
:ref:`In a webbrowser <post_vpz_input_webbrowser>` |
:ref:`In command line with cURL <post_vpz_input_curl>` |
:ref:`In Python language <post_vpz_input_python>` |
:ref:`In R language <post_vpz_input_r>` |
:ref:`In PHP language <post_vpz_input_php>`

