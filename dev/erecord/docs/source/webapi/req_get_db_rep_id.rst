.. _req_get_db_rep_id:

=======================
GET db/rep/{Id}
=======================

URL
===

`http://127.0.0.1:8000/db/rep/{Id} <http://127.0.0.1:8000/db/rep/{Id}>`_

Description
===========

Returns the identification information of the :term:`models repository` having
the Id value (as :ref:`dm_vlerep`).

Details
-------

*style*
    .. include:: desc_db_style.rst

Request parameters
==================

* *Id* : id value of the models repository as :ref:`dm_vlerep`

* :ref:`opt_style`

* :ref:`opt_format`

Response result
===============

The returned result is about :ref:`dm_vlerep`.

Example
=======

Example :
:ref:`In a webbrowser <db_resources_webbrowser>` |
:ref:`In command line with cURL <db_resources_curl>` |
:ref:`In Python language <db_resources_python>` |
:ref:`In R language <db_resources_r>` |
:ref:`In PHP language <db_resources_php>` 

See also use cases into : :ref:`example about simulator identification <wwdm_ident>`

