.. _req_post_acs_jwt_obtain:

=======================
POST acs/jwt/obtain
=======================

URL
===

http://127.0.0.1:8000/acs/jwt/obtain

:ref:`Try it <url_jwt_obtain>`

Description
===========

Returns a JWT (JSON Web Token) value.

For more see :ref:`auth` | :ref:`auth_howto_get_jwt`.

Details
-------

*username, password* : :ref:`how to get them <auth_ask_for_access>`

Help
----

The JWT value can then be used as a :ref:`opt_jwt` parameter of requests in limited access case (see :ref:`auth`).

Request parameters
==================

**Required** : username and password.

* *username*

* *password*

Response result
===============

"token" : the returned result is an available JWT value.

Example
=======

Example :
:ref:`In command line with cURL <post_acs_jwt_obtain_curl>`

