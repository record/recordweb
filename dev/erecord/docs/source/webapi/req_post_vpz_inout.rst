.. _req_post_vpz_inout:

=======================
POST vpz/inout
=======================

URL
===

http://127.0.0.1:8000/vpz/inout

:ref:`Try it <url_inout_menu>`

Description
===========

Maybe modifies the input conditions of a simulator (see :term:`input information of a vpz`) *(depending on the request parameters)*.
In addition with this input conditions modification (optional), maybe modifies the **input datas folder** of the simulator according to a posted file (optional).
Then runs the simulation and returns the **simulation results** (see :term:`output information of a vpz`) and also the **input conditions** (see :term:`input information of a vpz`).

Details
-------

*jwt*
    .. include:: desc_acs_jwt.rst

*input information*

    .. include:: desc_vpz_modify_input.rst

*input datas folder*
    .. include:: desc_vpz_modify_datafolder.rst

*filtering*

    It is possible to ask for filtering some of the returned information (see
    'parselect', 'outselect').

*style*
    .. include:: desc_vpz_style_inout.rst

Request parameters
==================

**Required** : either vpz or vpzpath must be provided.

**Required** : jwt in limited access case.

* :ref:`opt_vpz_choice`

* :ref:`opt_plan_restype`

* :ref:`opt_begin`
* :ref:`opt_duration`
* :ref:`opt_pars`

* :ref:`opt_datafolder`
* :ref:`opt_datafoldercopy`

* :ref:`opt_parselect`
* :ref:`opt_outselect`

* :ref:`opt_style`
* :ref:`opt_format`

* :ref:`opt_jwt`

Response result
===============

The returned result is about both :ref:`dm_vpzoutput` and :ref:`dm_vpzinput`.

Example
=======

Example : 
:ref:`In a webbrowser <post_vpz_inout_webbrowser>` |
:ref:`In command line with cURL <post_vpz_inout_curl>` |
:ref:`In Python language <post_vpz_inout_python>` |
:ref:`In R language <post_vpz_inout_r>` |
:ref:`In PHP language <post_vpz_inout_php>`

