.. _req_get_vpz_report:

=======================
GET vpz/report
=======================

URL
===

http://127.0.0.1:8000/vpz/report

Description
===========

Produces and returns the required **reports**, from the **simulation results** of a simulator (see :term:`output information of a vpz`) after having run simulation, and also from its **input conditions** (see :term:`input information of a vpz`).

Details
-------

*jwt*
    .. include:: desc_acs_jwt.rst

*reports*

    The built reports are gathered and returned into a '.zip' file.

Request parameters
==================

**Required** : either vpz or vpzpath must be provided.

**Required** : jwt in limited access case.

* :ref:`opt_vpz_choice`

* :ref:`opt_plan_restype`

* :ref:`opt_report`
* :ref:`opt_bycol`
* :ref:`opt_todownload`
* :ref:`opt_format`

* :ref:`opt_jwt`

Response result
===============

The returned result is reports about both :ref:`dm_vpzoutput` and
:ref:`dm_vpzinput`.

Example
=======

.. literalinclude:: ../examples/report/req_get_vpz_report_161.txt

