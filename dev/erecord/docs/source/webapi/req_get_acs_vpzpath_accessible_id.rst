.. _req_get_acs_vpzpath_accessible_id:

=============================
GET acs/vpzpath/accessible/id
=============================

URL
===

http://127.0.0.1:8000/acs/vpzpath/accessible/id

Description
===========

Returns the list of id of the accessible :term:`simulator`\ s (as :ref:`dm_vpzpath`), according to the parameters values.

Details
-------

The accessibility depends on the parameters values (*access*, *jwt*) used to filter the list.

*jwt*
    .. include:: desc_acs_jwt.rst

Request parameters
==================

* :ref:`opt_access`

* :ref:`opt_jwt`

* :ref:`opt_format`

Response result
===============

"list" : list of id of the accessible :term:`simulator`\ s (as :ref:`dm_vpzpath`).

Example
=======

*Under construction*

