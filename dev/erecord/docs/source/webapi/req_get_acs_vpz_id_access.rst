.. _req_get_acs_vpz_id_access:

=======================
GET acs/vpz/{Id}/access
=======================

URL
===

http://127.0.0.1:8000/acs/vpz/{Id}/access

Description
===========

Returns the access type of the :term:`simulator` (as :ref:`dm_vlevpz`) having the Id value (as :ref:`dm_vlevpz`).

Request parameters
==================

* *Id* : id value of the simulator as :ref:`dm_vlevpz`

* :ref:`opt_format`

Response result
===============

* :ref:`access <val_access_type>`

Example
=======

*Under construction*

