.. _erecord_code_access:

==================
Source code access
==================

Repository
==========

The erecord source code **repository** is hosted on the **recordweb**
project of **forgemia** (*forge of MIA Department of INRA Institute*) :
https://forgemia.inra.fr/record/recordweb.

Docs
====

The erecord source code **documentation** :
:ref:`Source code API reference <apiref_index>`.

