.. _webui:

==================
Web User Interface
==================

The `Web User Interface <http://127.0.0.1:8000/db/menu/>`_ makes easier calling some :term:`erecord` web services.

Main menu
=========

The **main menu** allows to select a simulator, and then see or/and run it.

Access to main menu http://127.0.0.1:8000/home


Submenus
=======================

There are **some other entry points** :

- Menu to see a simulator *after having selected it if {Id} was not given* :
  `http://127.0.0.1:8000/db/menu/input/{Id} <http://127.0.0.1:8000/db/menu/input/{Id}>`_

- Menu to run a simulator *after having selected it if {Id} was not given* :
  `http://127.0.0.1:8000/db/menu/output/{Id} <http://127.0.0.1:8000/db/menu/output/{Id}>`_

- Menu to see and run a simulator
  *after having selected it if {Id} was not given* :
  `http://127.0.0.1:8000/db/menu/inout/{Id} <http://127.0.0.1:8000/db/menu/inout/{Id}>`_

- Menu to modify and run a simulator
  *after having selected it if {Id} was not given* :
  `http://127.0.0.1:8000/db/menu/experiment/{Id} <http://127.0.0.1:8000/db/menu/experiment/{Id}>`_

- Menu to see or/and modify or/and run a simulator
  *after having selected it if {Id} was not given* :
  `http://127.0.0.1:8000/db/menu/{Id} <http://127.0.0.1:8000/db/menu/{Id}>`_

.. note::
   The :ref:`filter_db_rep`, :ref:`filter_db_pkg` and
   :ref:`filter_db_vpz` options can be used to filter the simulators
   shown to be selected.

   Examples :

   `http://127.0.0.1:8000/db/menu/input/?rep=2 <http://127.0.0.1:8000/db/menu/input/?rep=2>`_

   `http://127.0.0.1:8000/db/menu/?pkg=19 <http://127.0.0.1:8000/db/menu/?pkg=19>`_

   `http://127.0.0.1:8000/db/menu/output/?pkg=41 <http://127.0.0.1:8000/db/menu/output/?pkg=41>`_

.. note::
   About some of the URLs above : *'menu'* can be directly used instead of
   *'db/menu'*

