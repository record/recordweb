#!/usr/bin/env Rscript

# ****************************************************************************
#                  WWDM model - Sensitivity Analysis
#
# Sensitivity of LAI and U to A, B, Eb, Eimax, K, Lmax, TI parameters
#
# Parameters features :
#
# A     :  0.0065   [ 0.0035, 0.01   ]
# B     :  0.00205  [ 0.0011, 0.0025 ]
# Eb    :  1.85     [ 0.9   , 2.8    ]
# Eimax :  0.94     [ 0.9   , 0.99   ]
# K     :  0.7      [ 0.6   , 0.8    ]
# Lmax  :  7.5      [ 3     , 12     ]
# TI    :  900      [ 700   , 1100   ]
#
# ****************************************************************************

library('RCurl')
library('rjson')

sim = function(inputsmatrix)
{
    header = c('Content-Type'='application/json', Accept='application/json')
    options(RCurlOptions=list(followlocation=TRUE))

    postfields = toJSON(list(
        vpz=266, # id=266 of wwdm.vpz
        duration=120, begin=2453982.0,
        cond_wwdm.A     = inputsmatrix[,1],
        cond_wwdm.B     = inputsmatrix[,2],
        cond_wwdm.Eb    = inputsmatrix[,3],
        cond_wwdm.Eimax = inputsmatrix[,4],
        cond_wwdm.K     = inputsmatrix[,5],
        cond_wwdm.Lmax  = inputsmatrix[,6],
        cond_wwdm.TI    = inputsmatrix[,7],
        mode=c("tree", "linear", "dataframe"),
        outselect="all", format="json"))

    res = postForm(uri="http://erecord.toulouse.inra.fr:8000/vpz/output/",
        .opts=list(postfields=postfields, httpheader=header))
    
    responsedata = fromJSON(res)
    res = fromJSON(responsedata$res)
    return(res)
}

# returns LAI value for each (A, B, Eb, Eimax, K, Lmax, TI) of inputsmatrix
simLAI = function(inputsmatrix)
{
    res = sim(inputsmatrix)
    lai = unlist(lapply( 1:nrow(inputsmatrix),
                         function(i){mean(res[[i]]$view$`top:wwdm.LAI`)}))
    return(lai)
}

# returns U value for each (A, B, Eb, Eimax, K, Lmax, TI) of inputsmatrix
simU = function(inputsmatrix)
{
    res = sim(inputsmatrix)
    U = unlist(lapply( 1:nrow(inputsmatrix),
                       function(i){mean(res[[i]]$view$`top:wwdm.U`)}))
    return(U)
}

library('sensitivity')

LAI_sensitivity = function()
{
    sensitivity = fast99(
       model = simLAI,
       factors = c("A", "B", "Eb", "Eimax", "K", "Lmax", "TI"),
       n = 70,
       q = c("qunif", "qunif", "qunif", "qunif", "qunif", "qunif", "qunif"),
       q.arg = list(list(min = 0.0035, max = 0.01),
                    list(min = 0.0011, max = 0.0025),
                    list(min = 0.9   , max = 2.8),
                    list(min = 0.9   , max = 0.99),
                    list(min = 0.6   , max = 0.8 ),
                    list(min = 3.0   , max = 12.0),
                    list(min = 700.0 , max = 1100.0)))
    return(sensitivity)
}

U_sensitivity = function()
{
    sensitivity = fast99(
       model = simU,
       factors = c("A", "B", "Eb", "Eimax", "K", "Lmax", "TI"),
       n = 70,
       q = c("qunif", "qunif", "qunif", "qunif", "qunif", "qunif", "qunif"),
       q.arg = list(list(min = 0.0035, max = 0.01),
                    list(min = 0.0011, max = 0.0025),
                    list(min = 0.9   , max = 2.8),
                    list(min = 0.9   , max = 0.99),
                    list(min = 0.6   , max = 0.8 ),
                    list(min = 3.0   , max = 12.0),
                    list(min = 700.0 , max = 1100.0)))
    return(sensitivity)
}

#
# source( "wwdm.R" )
#
# s = LAI_sensitivity()
# print(s)
# plot(s)
#
# s = U_sensitivity()
# print(s)
# plot(s)
#

