.. _meansrecord_trainingcourse:

============================
Means Record training course
============================

A **training course** had been launched (2014) in order to study web services
as a solution to manage models simulations from other softwares (or from a
web browser, by command lines...).

The aim of this training course was to produce web services meeting some needs
of a specific user (Means platform) in relation with a specific model (MicMac
crop system) of the Record platform.

*Training subject "Développement de Web Services, mise en oeuvre appliquée à deux plates- formes INRA : MEANS et RECORD"*

See `Web services of the Record platform meeting some needs of the Means platform <http://147.100.179.168/misc/webrecord/meansrecord2014.pdf>`_ *(fr) (Services web de la plateforme Record répondant à des besoins de la plateforme Means)*.

