.. _recordschool:

======================
The recordschool model
======================

The **recordschool** model is a model of the :term:`Record` platform, that has been built for the researchers training *"Approches interdisciplinaires de la modélisation des agroécosystèmes"* organised by :term:`INRA` in mars 2017.

This model corresponds with the **recordschool** :term:`models repository` of erecord web services and so **can be requested by erecord web services**, as shown by the following examples :

- See : `recordschool as erecord web services <http://147.100.179.168/docs/models/recordschool/recordschool_erecord.pdf>`_ *(fr) (recordschool sous forme de services web)*.

- See : `R programs of model analysis <http://147.100.179.168/docs/models/recordschool/sequence_8_3_hackaton_3.pdf>`_ that use erecord web services for recordschool simulations *(fr) (Programmes R d'analyse de modèle appelant les services web erecord pour les simulations du modèle recordschool)*.

