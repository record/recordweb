.. _anaeefrance_project:

====================
AnaEE-France project
====================

AnaEE-France project
====================

`AnaEE-France <http://anaeefrance.toulouse.inra.fr/index.php/en>`_ (Analysis and Experimentation on Ecosystems - France) is a national research infrastructure for the study of continental ecosystems and their biodiversity. It offers the scientific community a broad range of 21 services in the form of experimental platforms (in controlled, semi-natural or natural environments), analysis platforms and shared instruments. AnaEE-France also provides access to data and modeling platforms.

AnaEE-France as client of erecord web services
==============================================

Indicators
----------

AnaEE-France project uses some erecord web services to produce some indicators :
`indicators <http://anaeefrance.toulouse.inra.fr/index.php/en/resources/indicators>`_

Some technical details
----------------------

The AnaEE-France requests calling erecord web services are written in PHP language.

