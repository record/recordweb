.. _example_jwt:

============================
Example about authentication
============================

.. toctree::
   :maxdepth: 1

   jwt/post_acs_jwt_obtain.rst
   jwt/requests.rst

