.. _example_wwdm:

===============================
Example based on the WWDM model
===============================

.. toctree::
   :maxdepth: 1

   Presentation of the WWDM model <wwdm/model>

   Identifying the 'wwdm.vpz' simulator <wwdm/ident>

-  How to :ref:`See <wwdm_see>` | :ref:`Modify & See <wwdm_modify_see>`
   the simulator

   How to :ref:`Run <wwdm_run>` | :ref:`Modify & Run <wwdm_modify_run>`
   the simulator

   How to :ref:`See & Run <wwdm_see_run>` | :ref:`Modify & See & Run <wwdm_modify_see_run>`
   the simulator

.. toctree::
   :maxdepth: 1

   A html home page for the simulator <wwdm/homepage>

- :ref:`Model Analysis (sensitivity analysis) <model_analysis>`

