.. _db_resources_intro:

This example is about the following :ref:`'db' resources <webapi_db>` :

  :ref:`req_get_db_vle` | :ref:`req_get_db_vle_id`

  :ref:`req_get_db_rep` | :ref:`req_get_db_rep_id`

  :ref:`req_get_db_pkg` | :ref:`req_get_db_pkg_id`

  :ref:`req_get_db_vpz` | :ref:`req_get_db_vpz_id`

