..  _db_resources_webbrowser:

====================================================
Example of some 'db' resources calls in a webbrowser
====================================================

.. include:: intro.rst

Enter in a webbrowser the URLs given bellow.

.. _db_call_050:

Calls
=====

- URLs for all the vle versions :

  http://127.0.0.1:8000/db/vle/

- URLs for the vle version with id=2 :

  http://127.0.0.1:8000/db/vle/2/

- URLs for all the models repositories :

  http://127.0.0.1:8000/db/rep/

- URLs for the models repository with id=2 ("recordb") :

  http://127.0.0.1:8000/db/rep/2/

- URLs for all the models of the models repository with id=2 ("recordb") :

  http://127.0.0.1:8000/db/pkg/?rep=2

- URLs for the model with id=19 ("2CV") :

  http://127.0.0.1:8000/db/pkg/19/

- URLs for all the simulators of the model with id=19 ("2CV") :

  http://127.0.0.1:8000/db/vpz/?pkg=19

- URLs for the model with id=41 ("wwdm") :

  http://127.0.0.1:8000/db/pkg/41/

- URLs for the simulator with id=266 ("wwdm.vpz") :

  http://127.0.0.1:8000/db/vpz/266/

You can vary :ref:`style of presentation <opt_style>` values ('tree', 'link')
and :ref:`format <opt_format>` values ('api', 'json', 'yaml', 'xml', 'html').
As an example :

  - '**tree**' style :

=================== =========================================================
'**api**' format    http://127.0.0.1:8000/db/rep/?style=tree&format=api
------------------- ---------------------------------------------------------
'**json**'' format  http://127.0.0.1:8000/db/rep/?style=tree&format=json
------------------- ---------------------------------------------------------
'**yaml**' format   http://127.0.0.1:8000/db/rep/?style=tree&format=yaml
------------------- ---------------------------------------------------------
'**xml**' format    http://127.0.0.1:8000/db/rep/?style=tree&format=xml
------------------- ---------------------------------------------------------
'**html**' format   http://127.0.0.1:8000/db/rep/?style=tree&format=html
=================== =========================================================

  - '**link**' style :

=================== =========================================================
'**api**' format    http://127.0.0.1:8000/db/rep/?style=link&format=api
------------------- ---------------------------------------------------------
'**json**'' format  http://127.0.0.1:8000/db/rep/?style=link&format=json
------------------- ---------------------------------------------------------
'**yaml**' format   http://127.0.0.1:8000/db/rep/?style=link&format=yaml
------------------- ---------------------------------------------------------
'**xml**' format    http://127.0.0.1:8000/db/rep/?style=link&format=xml
------------------- ---------------------------------------------------------
'**html**' format   http://127.0.0.1:8000/db/rep/?style=link&format=html
=================== =========================================================

