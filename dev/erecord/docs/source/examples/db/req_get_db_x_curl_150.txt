cURL command lines for all the vle versions :

curl -G http://127.0.0.1:8000/db/vle/ -d "style=tree"
curl -G http://127.0.0.1:8000/db/vle/ -d "style=link"

cURL command lines for the vle version with id=2 :

curl -G http://127.0.0.1:8000/db/vle/2/ -d "style=tree"
curl -G http://127.0.0.1:8000/db/vle/2/ -d "style=link"

cURL command lines for all the models repositories :

curl -G http://127.0.0.1:8000/db/rep/ -d "style=tree"
curl -G http://127.0.0.1:8000/db/rep/ -d "style=link"

cURL command lines for the models repository with id=2 ("recordb") :

curl -G http://127.0.0.1:8000/db/rep/2/ -d "style=tree"
curl -G http://127.0.0.1:8000/db/rep/2/ -d "style=link"

cURL command lines for all the models of the models repository with id=2 ("recordb") :

curl -G http://127.0.0.1:8000/db/pkg/ -d "style=tree&rep=2"
curl -G http://127.0.0.1:8000/db/pkg/ -d "style=link&rep=2"

cURL command lines for the model with id=19 ("2CV") :

curl -G http://127.0.0.1:8000/db/pkg/19/ -d "style=tree"
curl -G http://127.0.0.1:8000/db/pkg/19/ -d "style=link"

cURL command lines for all the simulators of the model with id=19 ("2CV") :

curl -G http://127.0.0.1:8000/db/vpz/ -d "style=tree&pkg=19"
curl -G http://127.0.0.1:8000/db/vpz/ -d "style=link&pkg=19"

cURL command lines for the model with id=41 ("wwdm") :

curl -G http://127.0.0.1:8000/db/pkg/41/ -d "style=tree"
curl -G http://127.0.0.1:8000/db/pkg/41/ -d "style=link"

cURL command lines for the simulator with id=266 ("wwdm.vpz") :

curl -G http://127.0.0.1:8000/db/vpz/266/ -d "style=link"
curl -G http://127.0.0.1:8000/db/vpz/266/ -d "style=tree"

