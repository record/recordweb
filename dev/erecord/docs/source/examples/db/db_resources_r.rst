.. _db_resources_r:

==================================================
Example of some 'db' resources calls in R language
==================================================

.. include:: intro.rst

Enter the R code/instructions in a R interpreter.

.. _db_call_350:

Calls
=====

  .. literalinclude:: ../wwdm/req_fn_content_rep_pkg_vpz_tree_300.txt
  .. literalinclude:: req_get_db_x_r_350.txt

