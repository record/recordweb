.. _db_resources_php:

====================================================
Example of some 'db' resources calls in PHP language
====================================================

.. include:: intro.rst

Use the PHP code into a php tag :

  .. literalinclude:: ../include/php_tag.txt

.. _db_call_450:

Calls
=====

  .. literalinclude:: req_get_db_x_php_450.txt

