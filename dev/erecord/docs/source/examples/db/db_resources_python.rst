.. _db_resources_python:

=======================================================
Example of some 'db' resources calls in Python language
=======================================================

.. include:: intro.rst

Enter the Python code/instructions in a Python interpreter.

.. _db_call_250:

Calls
=====

  .. literalinclude:: ../../../../erecord/apps/erecord_cmn/utils/using/send_get_and_receive.py

  .. literalinclude:: ../../../../erecord/apps/erecord_cmn/utils/using/content_rep_pkg_vpz_tree.py

  .. literalinclude:: req_get_db_x_python_250.txt

