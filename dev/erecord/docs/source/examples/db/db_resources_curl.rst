.. _db_resources_curl:

==============================================================
Example of some 'db' resources calls in command line with cURL
==============================================================

.. include:: intro.rst

Enter the cURL commands in a command line window.

.. _db_call_150:

Calls
=====

  .. literalinclude:: req_get_db_x_curl_150.txt

