1) Request GET vpz/input :

cURL command line :
curl "http://127.0.0.1:8000/vpz/input/?vpz=999&mode=compactlist&format=yaml&jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Ind3ZG10ZWFtIiwidXNlcl9pZCI6MiwiZW1haWwiOiIiLCJleHAiOjE0OTg4NjY2NjV9.kQhtSlfY0NWIXnig7p5J-wdq0_p81m5FwQc7gLze6vE"

=> Response :

begin: 2453980.0
duration: 222.0
cond_meteo.meteo_file: '["31035002.csv"]'
cond_wwdm.A: '[0.0065]'
cond_wwdm.B: '[0.00205]'
cond_wwdm.Eb: '[1.85]'
cond_wwdm.Eimax: '[0.94]'
cond_wwdm.K: '[0.7]'
cond_wwdm.Lmax: '[7.5]'
cond_wwdm.TI: '[900.0]'
[...]

2) Request POST vpz/output :

cURL command line :
curl -X POST -d "vpz=999&mode=compactlist&format=yaml&duration=10&cond_wwdm.Eb=1.86&outselect=all&jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Ind3ZG10ZWFtIiwidXNlcl9pZCI6MiwiZW1haWwiOiIiLCJleHAiOjE0OTg4NjY2NjV9.kQhtSlfY0NWIXnig7p5J-wdq0_p81m5FwQc7gLze6vE" http://127.0.0.1:8000/vpz/output/

=> Response :

res: '{"view.top:wwdm.LAI": [0.0, 0.002214909159036296, 0.005036363928261208, 0.008728879401612731, 0.013629810988742393, 0.01942796520463096, 0.026285056905304832, 0.03427568323615639, 0.044586961094757314, 0.05575120603485756, 0.06965116815464277], "view.top:wwdm.ST": [0.0, 21.2, 43.75, 68.2, 94.80000000000001, 120.60000000000001, 145.85000000000002, 170.40000000000003, 196.80000000000004, 220.75000000000006, 245.90000000000006], "view.time": [2453980.0, 2453981.0, 2453982.0, 2453983.0, 2453984.0, 2453985.0, 2453986.0, 2453987.0, 2453988.0, 2453989.0, 2453990.0], "view.top:wwdm.U": [0.0, 0.023633255918078876, 0.09067076273689131, 0.2058029827940016, 0.39606108383527067, 0.6448613989513584, 0.9065601369023396, 1.338469958707724, 1.8888959597983637, 2.5865214943340575, 3.3694330158985184]}'
plan: single
restype: dataframe

3) Request POST vpz/report, mode=todownload :

cURL command line :
curl -X POST -d "vpz=999&mode=todownload&duration=10&cond_wwdm.Eb=1.86&outselect=all&jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Ind3ZG10ZWFtIiwidXNlcl9pZCI6MiwiZW1haWwiOiIiLCJleHAiOjE0OTg4NjY2NjV9.kQhtSlfY0NWIXnig7p5J-wdq0_p81m5FwQc7gLze6vE" http://127.0.0.1:8000/vpz/report/

=> Response :

"The report can be downloaded at : http://127.0.0.1:8000/slm/download/?jwt=REPLACE_BY_YOUR_JWT_VALUE&key=20170630_141912_038a167c-a5b7-4064-a7c0-949c276cf92a *** The resource to download the result is 'GET slm/download' with 'key' option (key value : 20170630_141912_038a167c-a5b7-4064-a7c0-949c276cf92a) and 'jwt' option (jwt value : your own jwt). *** Don't forget to replace in your request jwt value by your own jwt value. *** Keep this key value in order to be able to download the result later on : key=20170630_141912_038a167c-a5b7-4064-a7c0-949c276cf92a"

