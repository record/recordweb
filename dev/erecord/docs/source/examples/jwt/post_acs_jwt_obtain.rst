.. _post_acs_jwt_obtain_curl:

===================================================================
Example of :ref:`req_post_acs_jwt_obtain` in command line with cURL
===================================================================

Example illustrating how to get a JWT value with a :ref:`req_post_acs_jwt_obtain` request.

  Enter the cURL commands in a command line window.

.. _jwt_call_190:

Call
====

  .. literalinclude:: req_post_acs_jwt_obtain_190.txt

