.. _jwt_authenticated_requests:

===========================================================
Examples of authenticated requests
===========================================================

Examples illustrating requests about a simulator in limited access, where an available JWT value is required, as a :ref:`opt_jwt` parameter of the requests.
The sent requests are :

  1) Request :ref:`req_get_vpz_input` to see parameters values.

  2) Request :ref:`req_post_vpz_output` to simulate and get simulation results.

  3) Request :ref:`req_post_vpz_report` whose report result (containing
     *parameters values and simulation results*) will be downloaded later
     on (in 4.) (see :ref:`option <opt_todownload>` mode=todownload).

  4) Request :ref:`req_get_slm_download` (with
     :ref:`opt_key` and :ref:`opt_jwt` parameters) to download the file
     report previously built (in 3.).

.. _jwt_call_191:

Requests 1,2,3. in command line with cURL
===========================================

  Enter the cURL commands in a command line window.

  .. literalinclude:: req_authenticated_191.txt

.. _jwt_call_092:

Request 4. in a webbrowser
==========================

  Enter in a webbrowser the following URL.

  .. literalinclude:: req_authenticated_092.txt

