.. _examples:

=================
Use case examples
=================

Here are some examples that aim to show how to call the :term:`erecord` web
services.

The following examples can be read as a *User Guide*, replacing the considered
model by the agronomic model you are interested in.

.. toctree::
   :maxdepth: 1

   jwt
   db
   wwdm
   Example of 'vpz/experiment' resources calls <experiment>


- :ref:`Example based on the recordschool model <recordschool>`
  (simulation, model analysis) 

- :ref:`Model Analysis <model_analysis>` (sensitivity analysis)

- :ref:`A html home page built for a simulator <wwdm_homepage>`

