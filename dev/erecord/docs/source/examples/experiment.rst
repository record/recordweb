.. _example_experiment:

===================================================
Example of vpz/experiment in command line with cURL
===================================================

Enter the cURL commands in a command line window.

Example illustrating how to build and run one's own experiment plan based on
a chosen simulator, by filling the experiment conditions into a xls file
and receiving the simulation results into a xls file.

First step : :ref:`req_get_vpz_experiment` request 
--------------------------------------------------

A :ref:`req_get_vpz_experiment` request is done to get the experiment conditions of the simulator in its original state.

.. literalinclude:: experiment/get/request.txt

The received :download:`experiment.xls<experiment/get/out/experiment.xls>` file
contains the experiment conditions of the simulator in its original state.

Second step : build one's own experiment plan into a xls file
-------------------------------------------------------------

Into the :download:`experiment.xls<experiment/get/out/experiment.xls>` file received
from the :ref:`req_get_vpz_experiment` request *(see 'first step' above)*,
or a copy of it, one can modify the experiment conditions as wanted : for
example, a modified
:download:`experiment.xls<experiment/post/in/experiment.xls>` file.

Third step : :ref:`req_post_vpz_experiment` request 
---------------------------------------------------

A :ref:`req_post_vpz_experiment` request is done to run the experiment plan
defined into the built
:download:`experiment.xls<experiment/post/in/experiment.xls>` file
*(see 'second step')*.

.. literalinclude:: experiment/post/request.txt

The returned :download:`experiment.xls<experiment/post/out/experiment.xls>`
file contains the simulation results and experiment conditions.

A variant
+++++++++

A :ref:`req_post_vpz_experiment` request to **modify the input datas folder**
(by using datafolder and datafoldercopy)
and run the experiment plan defined into the built
:download:`experiment.xls<experiment/post_3/in/experiment.xls>` file.

.. literalinclude:: include/intro_datafolder.txt
.. literalinclude:: experiment/post_3/request.txt

The returned file : :download:`experiment.xls<experiment/post_3/out/experiment.xls>`.

