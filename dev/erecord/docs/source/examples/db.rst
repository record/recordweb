.. _example_db:

====================================
Example of some 'db' resources calls
====================================

.. include:: db/intro.rst

Call
====

  :ref:`In a webbrowser <db_resources_webbrowser>` |
  :ref:`In command line with cURL <db_resources_curl>`

  :ref:`In Python language <db_resources_python>` |
  :ref:`In R language <db_resources_r>` |
  :ref:`In PHP language <db_resources_php>`

