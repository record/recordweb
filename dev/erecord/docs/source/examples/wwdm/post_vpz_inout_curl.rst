.. _post_vpz_inout_curl:

===============================================================
Examples of :ref:`req_post_vpz_inout` in command line with cURL
===============================================================

For the simulator 'wwdm.vpz' whose Id is  :ref:`wwdm_simulator_id`,
enter the cURL commands in a command line window.

*For more see* :ref:`req_post_vpz_inout`.

.. _wwdm_call_130:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - '**tree**' as style of presentation,
  - '**json**' as format,

  - values of type *cname.pname* as '**parselect**' :

    values 'cond_wwdm.A', 'cond_wwdm.B', 'cond_wwdm.Eb', 'cond_wwdm.TI'
    to select the parameters named 'A', 'B', 'Eb', 'TI'
    of the condition named 'cond_wwdm'.

  - value '**single**' as plan
  - value '**dataframe**' as restype

  - value of type *vname.oname* as '**outselect**' :

    values 'view.top:wwdm.LAI', 'view.top:wwdm.ST' to select the output datas
    named 'LAI', 'ST' of the view named 'view'.

  .. literalinclude:: req_post_vpz_inout_130.txt

A variant modifying the input datas folder by using datafolder and datafoldercopy :

  .. literalinclude:: ../include/intro_datafolder.txt
  .. literalinclude:: req_post_vpz_inout_130_3.txt

