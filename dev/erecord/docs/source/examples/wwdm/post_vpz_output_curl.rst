.. _post_vpz_output_curl:

================================================================
Examples of :ref:`req_post_vpz_output` in command line with cURL
================================================================

For the simulator 'wwdm.vpz' whose Id is  :ref:`wwdm_simulator_id`,
enter the cURL commands in a command line window.

*For more see* :ref:`req_post_vpz_output`.

.. _wwdm_call_135:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - '**compact**' as style of presentation,

  - value '**single**' as plan, or
    value '**linear**' in case of  multiple simulation
  - value '**dataframe**' as restype *(or 'matrix')*

  - value of type *vname.oname* as '**outselect**' :

    values 'view.top:wwdm.LAI', 'view.top:wwdm.ST' to select the output datas
    named 'LAI', 'ST' of the view named 'view'.

  .. literalinclude:: req_post_vpz_output_135.txt

Some variants modifying the input datas folder by using datafolder and datafoldercopy :


  .. literalinclude:: ../include/intro_datafolder.txt

  .. literalinclude:: req_post_vpz_output_135_3.txt

.. _wwdm_call_132:

Call
====

Example illustrating :


  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**pars**',

  - '**tree**' as style of presentation,

  - value '**single**' as plan, or
    value '**linear**' in case of  multiple simulation
  - value '**dataframe**' as restype *(or 'matrix')*

  - value '**all**' as '**outselect**',

  .. literalinclude:: req_post_vpz_output_132.txt

Call
====

See :ref:`jwt_call_191`

