.. _wwdm_modify_see_run:

====================================================
Seeing and running the modified 'wwdm.vpz' simulator
====================================================

*The erecord web services used in this part are some of the*
:ref:`vpz resources <webapi_vpz>`.

Seeing and running a -maybe modified- simulator consists in getting both its
:ref:`input information <dm_vpzinput>`, maybe after having modified it, and
its :ref:`output information <dm_vpzoutput>` resulting from the simulation.
It corresponds with the :ref:`req_post_vpz_inout` resource.

*Memo* : the Id of the ‘wwdm.vpz’ simulator is :ref:`wwdm_simulator_id`.

Call
====

  :ref:`In a webbrowser <post_vpz_inout_webbrowser>` |
  :ref:`In command line with cURL <post_vpz_inout_curl>`

  :ref:`In Python language <post_vpz_inout_python>` |
  :ref:`In R language <post_vpz_inout_r>` |
  :ref:`In PHP language <post_vpz_inout_php>`

More
====

See : :ref:`feat_simulation_running_mode` |
:ref:`feat_parameters` |
:ref:`feat_outputdatas` |

*Back to :* :ref:`example_wwdm`

