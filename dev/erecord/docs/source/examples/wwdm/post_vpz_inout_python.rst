.. _post_vpz_inout_python:

========================================================
Examples of :ref:`req_post_vpz_inout` in Python language
========================================================

For the simulator 'wwdm.vpz' whose Id is  :ref:`wwdm_simulator_id`,
enter the Python code/instructions in a Python interpreter.


*For more see* :ref:`req_post_vpz_inout`.

.. _wwdm_call_230:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - '**tree**' as style of presentation,
  - '**json**' as format,

  - values of type *cname.pname* as '**parselect**' :

    values 'cond_wwdm.A', 'cond_wwdm.B', 'cond_wwdm.Eb', 'cond_wwdm.TI'
    to select the parameters named 'A', 'B', 'Eb', 'TI'
    of the condition named 'cond_wwdm'.

  - value '**single**' as plan
  - value '**dataframe**' as restype

  - value of type *vname.oname* as '**outselect**' :

    values 'view.top:wwdm.LAI', 'view.top:wwdm.ST' to select the output datas
    named 'LAI', 'ST' of the view named 'view'.

  .. literalinclude:: ../../../../erecord/apps/erecord_cmn/utils/using/send_post_and_receive.py

  .. literalinclude:: ../../../../erecord/apps/erecord_cmn/utils/using/content_simulation_inputs_tree.py

  .. literalinclude:: ../../../../erecord/apps/erecord_cmn/utils/using/content_simulation_results_tree.py

  .. literalinclude:: req_post_vpz_inout_230.txt

