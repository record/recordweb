.. _post_vpz_output_php:

======================================================
Examples of :ref:`req_post_vpz_output` in PHP language
======================================================

For the simulator 'wwdm.vpz' whose Id is  :ref:`wwdm_simulator_id`,
use the PHP code into a php tag :

  .. literalinclude:: ../include/php_tag.txt

*For more see* :ref:`req_post_vpz_output`.

.. _wwdm_call_435:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - '**compact**' as style of presentation,
  - '**json**' as format,

  - value '**single**' as plan, or
    value '**linear**' in case of  multiple simulation
  - value '**dataframe**' as restype *(or 'matrix')*

  - value of type *vname.oname* as '**outselect**' :

    values 'view.top:wwdm.LAI', 'view.top:wwdm.ST' to select the output datas
    named 'LAI', 'ST' of the view named 'view'.

  .. literalinclude:: req_fn_send_post_and_receive_400.txt
  .. literalinclude:: req_post_vpz_output_435.txt

.. _wwdm_call_432:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters with '**pars**',

  - '**tree**' as style of presentation,
  - '**json**' as format,

  - value '**single**' as plan, or
    value '**linear**' in case of  multiple simulation
  - value '**dataframe**' as restype *(or 'matrix')*

  - value '**all**' as '**outselect**',

  .. literalinclude:: req_fn_send_post_and_receive_400.txt
  .. literalinclude:: req_post_vpz_output_432.txt

