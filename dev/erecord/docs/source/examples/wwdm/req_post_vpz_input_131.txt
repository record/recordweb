-------------------------------------------------------------------------------
* Case :
- Content-Type: Content-Type: application/json

cURL command line :
curl -L -H "Content-Type: application/json" -d '{"vpz":266, "duration":6, "begin":2453982.0, "cond_wwdm.A":0.0064, "cond_wwdm.Eb":1.86, "mode":"compactlist", "parselect":["cond_wwdm.A", "cond_wwdm.B", "cond_wwdm.Eb", "cond_wwdm.TI"]}' http://127.0.0.1:8000/vpz/input/

-------------------------------------------------------------------------------
* Case :
- Content-Type: application/x-www-form-urlencoded
- format yaml

cURL command line :
curl -H "Content-Type: application/x-www-form-urlencoded" -d 'vpz=266&duration=6&begin=2453982.0&cond_wwdm.A=0.0064&cond_wwdm.Eb=1.86&mode=compactlist&parselect=cond_wwdm.A&parselect=cond_wwdm.B&parselect=cond_wwdm.Eb&parselect=cond_wwdm.TI&format=yaml' http://127.0.0.1:8000/vpz/input/

-------------------------------------------------------------------------------

