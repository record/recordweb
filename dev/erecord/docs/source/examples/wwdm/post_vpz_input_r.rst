.. _post_vpz_input_r:

===================================================
Examples of :ref:`req_post_vpz_input` in R language
===================================================

For the simulator 'wwdm.vpz' whose Id is  :ref:`wwdm_simulator_id`,
enter the R code/instructions in a R interpreter.

*For more see* :ref:`req_post_vpz_input`.

.. _wwdm_call_331:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - '**compactlist**' as style of presentation,
  - '**json**' as format,

  - values of type *cname.pname* as '**parselect**' :

    values 'cond_wwdm.A', 'cond_wwdm.B', 'cond_wwdm.Eb', 'cond_wwdm.TI'
    to select the parameters named 'A', 'B', 'Eb', 'TI'
    of the condition named 'cond_wwdm'.

  .. literalinclude:: req_post_vpz_input_331.txt


.. _wwdm_call_334:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - value '**all**' as '**parselect**',

  - '**tree**' as style of presentation,
  - '**json**' as format,

  .. literalinclude:: req_fn_content_simulation_inputs_tree_300.txt
  .. literalinclude:: req_post_vpz_input_334.txt


.. _wwdm_call_336:

Call
====

Example illustrating :

  - '**compact**' as style of presentation,
  - '**json**' as format,

  - value '**all**' as '**parselect**'
    (to receive information of all the existing parameters and conditions)

  - value '**all**' as '**outselect**'
    (to receive information of all the existing output datas and views)

  .. literalinclude:: req_post_vpz_input_336.txt

