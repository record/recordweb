.. _post_vpz_input_php:

=====================================================
Examples of :ref:`req_post_vpz_input` in PHP language
=====================================================

For the simulator 'wwdm.vpz' whose Id is  :ref:`wwdm_simulator_id`,
use the PHP code into a php tag :

  .. literalinclude:: ../include/php_tag.txt

*For more see* :ref:`req_post_vpz_input`.


.. _wwdm_call_431:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - '**compactlist**' as style of presentation,
  - '**json**' as format,

  - values of type *cname.pname* as '**parselect**' :

    values 'cond_wwdm.A', 'cond_wwdm.B', 'cond_wwdm.Eb', 'cond_wwdm.TI'
    to select the parameters named 'A', 'B', 'Eb', 'TI'
    of the condition named 'cond_wwdm'.

  .. literalinclude:: req_fn_send_post_and_receive_400.txt

  .. literalinclude:: req_post_vpz_input_431.txt

.. _wwdm_call_434:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters with '**pars**',

  - value '**all**' as '**parselect**',

  - '**tree**' as style of presentation,
  - '**json**' as format,

  .. literalinclude:: req_fn_send_post_and_receive_400.txt

  .. literalinclude:: req_post_vpz_input_434.txt

.. _wwdm_call_436:

Call
====

Example illustrating :

  - '**compact**' as style of presentation,
  - '**json**' as format,

  - value '**all**' as 'parselect'
    (to receive information of all the existing parameters and conditions)

  - value '**all**' as 'outselect'
    (to receive information of all the existing output datas and views)

  .. literalinclude:: req_fn_send_post_and_receive_400.txt

  .. literalinclude:: req_post_vpz_input_436.txt

