.. _post_vpz_inout_webbrowser:

=====================================================
Examples of :ref:`req_post_vpz_inout` in a webbrowser
=====================================================

For the simulator 'wwdm.vpz' whose Id is  :ref:`wwdm_simulator_id` [2]_,
enter in a webbrowser the URL [1]_ and click
'POST' button after having entered your commands in the POST space
('Media type' and 'Content') [3]_.

.. [1] The URL is http://127.0.0.1:8000/vpz/inout.
.. [2] The simulator Id is specified in the POST space by 'vpz'.
.. [3] Some illustrations of POST requests in a webbrowser : 
       :download:`case <img_webbrowser_json.png>` of
       'application/json' as Media type,
       :download:`case n°1 <img_webbrowser_urlencoded.png>` and
       :download:`case n°2 <img_webbrowser_urlencoded_2.png>` of
       'x-www-form-urlencoded' as Media type.

*For more see* :ref:`req_post_vpz_inout`.

.. _wwdm_call_030:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - '**tree**' as style of presentation,
  - '**json**' as format,

  - values of type *cname.pname* as '**parselect**' :

    values 'cond_wwdm.A', 'cond_wwdm.B', 'cond_wwdm.Eb', 'cond_wwdm.TI'
    to select the parameters named 'A', 'B', 'Eb', 'TI'
    of the condition named 'cond_wwdm'.

  - value '**single**' as plan
  - value '**dataframe**' as restype

  - value of type *vname.oname* as '**outselect**' :

    values 'view.top:wwdm.LAI', 'view.top:wwdm.ST' to select the output datas
    named 'LAI', 'ST' of the view named 'view'.

  .. literalinclude:: req_post_vpz_inout_030.txt

