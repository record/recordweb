.. _wwdm_modify_run:

=========================================
Running the modified 'wwdm.vpz' simulator
=========================================

*The erecord web services used in this part are some of the*
:ref:`vpz resources <webapi_vpz>`.

Running a -maybe modified- simulator consists in getting its
:ref:`output information <dm_vpzoutput>` resulting from the simulation,
maybe after having modified the :ref:`input information <dm_vpzinput>`.
It corresponds with the :ref:`req_post_vpz_output` resource.

*Memo* : the Id of the ‘wwdm.vpz’ simulator is :ref:`wwdm_simulator_id`.

Call
====

  :ref:`In a webbrowser <post_vpz_output_webbrowser>` |
  :ref:`In command line with cURL <post_vpz_output_curl>`

  :ref:`In Python language <post_vpz_output_python>` |
  :ref:`In R language <post_vpz_output_r>` |
  :ref:`In PHP language <post_vpz_output_php>`

More
====

See : :ref:`feat_simulation_running_mode` |
:ref:`feat_parameters` |
:ref:`feat_outputdatas` |

*Back to :* :ref:`example_wwdm`

