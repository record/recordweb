.. _post_vpz_input_webbrowser:

=====================================================
Examples of :ref:`req_post_vpz_input` in a webbrowser
=====================================================

For the simulator 'wwdm.vpz' whose Id is  :ref:`wwdm_simulator_id` [2]_,
enter in a webbrowser the URL [1]_ and click
'POST' button after having entered your commands in the POST space
('Media type' and 'Content') [3]_.

.. [1] The URL is http://127.0.0.1:8000/vpz/input.
.. [2] The simulator Id is specified in the POST space by 'vpz'.
.. [3] Some illustrations of POST requests in a webbrowser : 
       :download:`case <img_webbrowser_json.png>` of
       'application/json' as Media type,
       :download:`case n°1 <img_webbrowser_urlencoded.png>` and
       :download:`case n°2 <img_webbrowser_urlencoded_2.png>` of
       'x-www-form-urlencoded' as Media type.

*For more see* :ref:`req_post_vpz_input`.


.. _wwdm_call_031:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - '**compactlist**' as style of presentation,

  - values of type *cname.pname* as '**parselect**' :

    values 'cond_wwdm.A', 'cond_wwdm.B', 'cond_wwdm.Eb', 'cond_wwdm.TI'
    to select the parameters named 'A', 'B', 'Eb', 'TI'
    of the condition named 'cond_wwdm'.

  .. literalinclude:: req_post_vpz_input_031.txt


.. _wwdm_call_034:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**pars**',

  - value '**all**' as '**parselect**',

  - '**tree**' as style of presentation,

  .. literalinclude:: req_post_vpz_input_034.txt

.. _wwdm_call_036:

Call
====

Example illustrating :

  - '**compact**' as style of presentation,

  - value '**all**' as '**parselect**'
    (to receive information of all the existing parameters and conditions)

  - value '**all**' as '**outselect**'
    (to receive information of all the existing output datas and views)

  .. literalinclude:: req_post_vpz_input_036.txt

.. _wwdm_call_001:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**'.

  .. literalinclude:: req_post_vpz_input_001.txt

.. _wwdm_call_002:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',
  - '**compactlist**' as style of presentation

  .. literalinclude:: req_post_vpz_input_002.txt

.. _wwdm_call_003:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  .. literalinclude:: req_post_vpz_input_003.txt

.. _wwdm_call_004:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',
  - '**compactlist**' as style of presentation

  .. literalinclude:: req_post_vpz_input_004.txt

.. _wwdm_call_006:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**pars**',
  - '**compactlist**' as style of presentation

  .. literalinclude:: req_post_vpz_input_006.txt

.. _wwdm_call_007:

Call
====

Example illustrating :

  - modifying some parameters by '**cname.pname**',
  - value '**all**' as '**parselect**',
    to select all parameters of all conditions.

  .. literalinclude:: req_post_vpz_input_007.txt

.. _wwdm_call_008:

Call
====

Example illustrating :

  - modifying some parameters by '**cname.pname**',
  - value of type *condname* as '**parselect**' :

    value 'cond_wwdm' to select all parameters of the condition named
    'cond_wwdm'.

  - value of type *cname.pname* as '**parselect**' :

    value 'cond_meteo.meteo_file' to select the parameter named 'meteo_file'
    of the condition named 'cond_meteo'.

  .. literalinclude:: req_post_vpz_input_008.txt

.. _wwdm_call_009:

Call
====

Example illustrating :

  - modifying some parameters by '**cname.pname**',
  - value of type *condname* as '**parselect**' :

    value 'cond_wwdm' to select all parameters of the condition named
    'cond_wwdm'.

  - value of type *cname.pname* as '**parselect**' :

    value 'cond_meteo.meteo_file' to select the parameter named 'meteo_file'
    of the condition named 'cond_meteo'.

  .. literalinclude:: req_post_vpz_input_009.txt

.. _wwdm_call_011:

Call
====

Example illustrating :

  - modifying some parameters by '**cname.pname**',
  - '**compactlist**' as style of presentation
  - values of type *cname.pname* as '**parselect**' :

    values 'cond_wwdm.A', 'cond_wwdm.B', 'cond_wwdm.Eb', 'cond_wwdm.TI'
    to select the parameters named 'A', 'B', 'Eb', 'TI'
    of the condition named 'cond_wwdm'.

  .. literalinclude:: req_post_vpz_input_011.txt

.. _wwdm_call_012:

Call
====

Example illustrating :

  - '**all**' as 'outselect'
  - '**compact**' as style of presentation

  .. literalinclude:: req_post_vpz_input_012.txt

