.. _post_vpz_input_curl:

===============================================================
Examples of :ref:`req_post_vpz_input` in command line with cURL
===============================================================

For the simulator 'wwdm.vpz' whose Id is  :ref:`wwdm_simulator_id`,
enter the cURL commands in a command line window.

*For more see* :ref:`req_post_vpz_input`.

.. _wwdm_call_131:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - '**compactlist**' as style of presentation,

  - values of type *cname.pname* as '**parselect**' :

    values 'cond_wwdm.A', 'cond_wwdm.B', 'cond_wwdm.Eb', 'cond_wwdm.TI'
    to select the parameters named 'A', 'B', 'Eb', 'TI'
    of the condition named 'cond_wwdm'.

  .. literalinclude:: req_post_vpz_input_131.txt


.. _wwdm_call_134:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**pars**',

  - value '**all**' as '**parselect**',

  - '**tree**' as style of presentation,

  .. literalinclude:: req_post_vpz_input_134.txt


.. _wwdm_call_136:

Call
====

Example illustrating :

  - '**compact**' as style of presentation,

  - value '**all**' as '**parselect**'
    (to receive information of all the existing parameters and conditions)

  - value '**all**' as '**outselect**'
    (to receive information of all the existing output datas and views)

  .. literalinclude:: req_post_vpz_input_136.txt

