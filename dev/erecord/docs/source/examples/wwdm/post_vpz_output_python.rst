.. _post_vpz_output_python:

=========================================================
Examples of :ref:`req_post_vpz_output` in Python language
=========================================================

For the simulator 'wwdm.vpz' whose Id is  :ref:`wwdm_simulator_id`,
enter the Python code/instructions in a Python interpreter.


*For more see* :ref:`req_post_vpz_output`.

.. _wwdm_call_235:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - '**compact**' as style of presentation,
  - '**json**' as format,

  - value '**single**' as plan, or
    value '**linear**' in case of  multiple simulation
  - value '**dataframe**' as restype *(or 'matrix')*

  - value of type *vname.oname* as '**outselect**' :

    values 'view.top:wwdm.LAI', 'view.top:wwdm.ST' to select the output datas
    named 'LAI', 'ST' of the view named 'view'.

    .. literalinclude:: ../../../../erecord/apps/erecord_cmn/utils/using/send_post_and_receive.py

    .. literalinclude:: ../../../../erecord/apps/erecord_cmn/utils/using/content_simulation_results_compact.py

  .. literalinclude:: req_post_vpz_output_235.txt

.. _wwdm_call_232:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters with '**pars**',

  - '**tree**' as style of presentation,
  - '**json**' as format,

  - value '**single**' as plan, or
    value '**linear**' in case of  multiple simulation
  - value '**dataframe**' as restype *(or 'matrix')*

  - value '**all**' as '**outselect**',

  .. literalinclude:: ../../../../erecord/apps/erecord_cmn/utils/using/send_post_and_receive.py

  .. literalinclude:: ../../../../erecord/apps/erecord_cmn/utils/using/content_simulation_results_tree.py

  .. literalinclude:: req_post_vpz_output_232.txt

