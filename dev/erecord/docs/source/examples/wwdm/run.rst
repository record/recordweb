.. _wwdm_run:

====================================
Running the 'wwdm.vpz' simulator
====================================

*The erecord web services used in this part are some of the*
:ref:`vpz resources <webapi_vpz>`.

Running a simulator, as is, consists in getting its
:ref:`output information <dm_vpzoutput>` resulting from the simulation.
It corresponds with the :ref:`req_get_vpz_output` resource.

Call
====

  *... under construction ...*

More
====

*Back to :* :ref:`example_wwdm`

