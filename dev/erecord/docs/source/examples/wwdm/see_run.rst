.. _wwdm_see_run:

===========================================
Seeing and running the 'wwdm.vpz' simulator
===========================================

*The erecord web services used in this part are some of the*
:ref:`vpz resources <webapi_vpz>`.

Seeing and running a simulator, as is, consists in getting both its 
:ref:`input information <dm_vpzinput>` and its
:ref:`output information <dm_vpzoutput>` resulting from the simulation.
It corresponds with the :ref:`req_get_vpz_inout` resource.

Call
====

  *... under construction ...*

More
====

*Back to :* :ref:`example_wwdm`

