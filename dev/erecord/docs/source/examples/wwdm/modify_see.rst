.. _wwdm_modify_see:

========================================
Seeing the modified 'wwdm.vpz' simulator
========================================

*The erecord web services used in this part are some of the*
:ref:`vpz resources <webapi_vpz>`.

Seeing a -may be modified- simulator consists in getting its
:ref:`input information <dm_vpzinput>` (begin, duration, parameters...) maybe
after having modified it.
It corresponds with the :ref:`req_post_vpz_input` resource.

*Memo* : the Id of the ‘wwdm.vpz’ simulator is :ref:`wwdm_simulator_id`.

Call
====

  :ref:`In a webbrowser <post_vpz_input_webbrowser>` |
  :ref:`In command line with cURL <post_vpz_input_curl>`

  :ref:`In Python language <post_vpz_input_python>` |
  :ref:`In R language <post_vpz_input_r>` |
  :ref:`In PHP language <post_vpz_input_php>`

More
====

See : :ref:`feat_parameters` |
:ref:`feat_outputdatas` |

*Back to :* :ref:`example_wwdm`

