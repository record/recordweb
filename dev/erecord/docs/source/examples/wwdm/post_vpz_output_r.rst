.. _post_vpz_output_r:

====================================================
Examples of :ref:`req_post_vpz_output` in R language
====================================================

For the simulator 'wwdm.vpz' whose Id is  :ref:`wwdm_simulator_id`,
enter the R code/instructions in a R interpreter.

*For more see* :ref:`req_post_vpz_output`.

.. _wwdm_call_335:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - '**compact**' as style of presentation,
  - '**json**' as format,

  - value '**single**' as plan, or
    value '**linear**' in case of  multiple simulation
  - value '**dataframe**' as restype *(or 'matrix')*

  - value of type *vname.oname* as '**outselect**' :

    values 'view.top:wwdm.LAI', 'view.top:wwdm.ST' to select the output datas
    named 'LAI', 'ST' of the view named 'view'.

  .. literalinclude:: req_fn_content_simulation_results_compact_300.txt
  .. literalinclude:: req_post_vpz_output_335.txt

.. _wwdm_call_332:

Call
====

Example illustrating :


  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - '**tree**' as style of presentation,
  - '**json**' as format,

  - value '**single**' as plan, or
    value '**linear**' in case of  multiple simulation
  - value '**dataframe**' as restype *(or 'matrix')*

  - value '**all**' as '**outselect**',

  .. literalinclude:: req_fn_content_simulation_results_tree_300.txt
  .. literalinclude:: req_post_vpz_output_332.txt

