.. _wwdm_ident:

====================================
Identifying the 'wwdm.vpz' simulator
====================================

Introduction
============

*The erecord web services used in this part are some of the*
:ref:`db resources <webapi_db>`.

Identifying the 'wwdm.vpz' simulator consists in identifying its Id into the erecord databases. Indeed it is necessary to know the simulator's Id when asking for seeing, modifying or simulating it.

In our case, the 'wwdm.vpz' simulator is recorded into the erecord databases as a :ref:`dm_vlevpz` (for more, see :ref:`wwdm_databases`).

Step by step 
============

- You can enter in a webbrowser the URL to list all the models repositories :

  http://127.0.0.1:8000/db/rep/

  You will see in particular the 'recordb' models repository that hosts our
  'wwdm.vpz' simulator. We see that the 'recordb' models repository's Id is 2.

  .. note:: The equivalent command line with cURL is :
     curl http://127.0.0.1:8000/db/rep/

- You can then enter in a webbrowser the URL to list all the models
  of the 'recordb' models repository *(whose Id is 2)* :

  http://127.0.0.1:8000/db/pkg/?rep=2

  You will see in particular the 'wwdm' model that contains our 'wwdm.vpz'
  simulator. We see that the 'wwdm' model's Id is 41.

- You can then enter in a webbrowser the URL to list all the simulators
  of the 'wwdm' model *(whose Id is 41)* :

  http://127.0.0.1:8000/db/vpz/?pkg=41
  
  You will see in particular our simulator 'wwdm.vpz' whose Id is 266.

  .. note:: Command line with cURL to control that the 'wwdm.vpz' simulator's
     Id is 266 :

     curl http://127.0.0.1:8000/db/vpz/266/

Other ways
==========

You can enter in a webbrowser some of the following URLs :

- **to list all the simulators** : http://127.0.0.1:8000/db/vpz/

  You will see in particular our simulator 'wwdm.vpz' whose Id is 266.

  *If you knew only the name of your simulator 'wwdm.vpz', or only the name of
  the model 'wwdm' (without knowing into which models repository they are),
  you could search for the words you know ('wwdm.vpz', 'wwdm'...) in the
  resulting list.*

  .. note:: The equivalent command line with cURL is :
     curl http://127.0.0.1:8000/db/vpz/

- **to list all the models repositories** :
  http://127.0.0.1:8000/db/rep/?mode=link

  *If you don't know anything about the models repositories, models,
  simulators of erecord, you can there browse among the links to discover
  them.*

Here are some other URLs that may be useful :

- to list all the models : http://127.0.0.1:8000/db/pkg/

  or http://127.0.0.1:8000/db/pkg/?mode=tree 
  to deploy information

- to list all the models repositories : http://127.0.0.1:8000/db/rep/

  or http://127.0.0.1:8000/db/rep/?mode=tree 
  to deploy information

- to control that the 'wwdm' model's Id is 41 : http://127.0.0.1:8000/db/pkg/41

  .. note:: The equivalent command line with cURL is :
     curl http://127.0.0.1:8000/db/pkg/41/

Highlights
==========

The 'wwdm.vpz' simulator is recorded into the erecord databases as the
:ref:`dm_vlevpz` whose Id is :ref:`wwdm_simulator_id`.

More
====

*Next step :* :ref:`wwdm_see` | :ref:`wwdm_run` | :ref:`wwdm_see_run`

*Back to :* :ref:`example_wwdm`

