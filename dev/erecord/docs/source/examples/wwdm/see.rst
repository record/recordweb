.. _wwdm_see:

====================================
Seeing the 'wwdm.vpz' simulator
====================================

*The erecord web services used in this part are some of the*
:ref:`vpz resources <webapi_vpz>`.

Seeing a simulator, as is, consists in getting its
:ref:`input information <dm_vpzinput>` (begin, duration, parameters...).
It corresponds with the :ref:`req_get_vpz_input` resource.

*Memo* : the Id of the ‘wwdm.vpz’ simulator is :ref:`wwdm_simulator_id`.

Call
====

In a webbrowser
---------------

Enter the URLs in a webbrowser.

    http://127.0.0.1:8000/vpz/input/?vpz=266

You can choose between different :ref:`styles <opt_style>` of presentation and different :ref:`formats <opt_format>`, for example :

    http://127.0.0.1:8000/vpz/input/?vpz=266&mode=compactlist&format=json

    http://127.0.0.1:8000/vpz/input/?vpz=266&mode=compact&format=json

    http://127.0.0.1:8000/vpz/input/?vpz=266&mode=link&format=api

    http://127.0.0.1:8000/vpz/input/?vpz=266&mode=tree&format=api

    http://127.0.0.1:8000/vpz/input/?vpz=266&mode=tree&format=html

    http://127.0.0.1:8000/vpz/input/?vpz=266&mode=compactlist&format=html

In command line with cURL
-------------------------

Enter the cURL command in a command line window.

    curl http://127.0.0.1:8000/vpz/input/?vpz=266

More
====

*Back to :* :ref:`example_wwdm`

