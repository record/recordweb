.. _post_vpz_input_python:

========================================================
Examples of :ref:`req_post_vpz_input` in Python language
========================================================

For the simulator 'wwdm.vpz' whose Id is  :ref:`wwdm_simulator_id`,
enter the Python code/instructions in a Python interpreter.


*For more see* :ref:`req_post_vpz_input`.


.. _wwdm_call_231:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - '**compactlist**' as style of presentation,
  - '**json**' as format,

  - values of type *cname.pname* as '**parselect**' :

    values 'cond_wwdm.A', 'cond_wwdm.B', 'cond_wwdm.Eb', 'cond_wwdm.TI'
    to select the parameters named 'A', 'B', 'Eb', 'TI'
    of the condition named 'cond_wwdm'.

  .. literalinclude:: ../../../../erecord/apps/erecord_cmn/utils/using/send_post_and_receive.py

  .. literalinclude:: req_post_vpz_input_231.txt

.. _wwdm_call_234:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters with '**pars**',

  - value '**all**' as '**parselect**',

  - '**tree**' as style of presentation,
  - '**json**' as format,


  .. literalinclude:: ../../../../erecord/apps/erecord_cmn/utils/using/send_post_and_receive.py

  .. literalinclude:: ../../../../erecord/apps/erecord_cmn/utils/using/content_simulation_inputs_tree.py

  .. literalinclude:: req_post_vpz_input_234.txt


.. _wwdm_call_236:

Call
====

Example illustrating :

  - '**compact**' as style of presentation,
  - '**json**' as format,

  - value '**all**' as 'parselect'
    (to receive information of all the existing parameters and conditions)

  - value '**all**' as 'outselect'
    (to receive information of all the existing output datas and views)

  .. literalinclude:: ../../../../erecord/apps/erecord_cmn/utils/using/send_post_and_receive.py

  .. literalinclude:: req_post_vpz_input_236.txt

