.. _post_vpz_output_webbrowser:

======================================================
Examples of :ref:`req_post_vpz_output` in a webbrowser
======================================================

For the simulator 'wwdm.vpz' whose Id is  :ref:`wwdm_simulator_id` [2]_,
enter in a webbrowser the URL [1]_ and click
'POST' button after having entered your commands in the POST space
('Media type' and 'Content') [3]_.

.. [1] The URL is http://127.0.0.1:8000/vpz/output.
.. [2] The simulator Id is specified in the POST space by 'vpz'.
.. [3] Some illustrations of POST requests in a webbrowser : 
       :download:`case <img_webbrowser_json.png>` of
       'application/json' as Media type,
       :download:`case n°1 <img_webbrowser_urlencoded.png>` and
       :download:`case n°2 <img_webbrowser_urlencoded_2.png>` of
       'x-www-form-urlencoded' as Media type.

*For more see* :ref:`req_post_vpz_output`.

.. _wwdm_call_035:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - '**compact**' as style of presentation,

  - value '**single**' as plan, or
    value '**linear**' in case of  multiple simulation
  - value '**dataframe**' as restype *(or 'matrix')*

  - value of type *vname.oname* as '**outselect**' :

    values 'view.top:wwdm.LAI', 'view.top:wwdm.ST' to select the output datas
    named 'LAI', 'ST' of the view named 'view'.

:download:`REQUEST in case of single plan (single simulation) <img_webbrowser_urlencoded.png>`

:download:`REQUEST in case of linear plan (multiple simulation) <img_webbrowser_urlencoded_2.png>`

  .. literalinclude:: req_post_vpz_output_035.txt

.. _wwdm_call_020:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',
  - value '**all**' as '**outselect**',
    to select all output datas of all views.

  .. literalinclude:: req_post_vpz_output_020.txt


.. _wwdm_call_021:

Call
====

Example illustrating :

  - modifying **duration**,
  - value '**single**' as plan
  - value '**dataframe**' as restype

  - value of type *viewname* as '**outselect**' :

    value 'view' to select all output datas of the view named 'view'.

  .. literalinclude:: req_post_vpz_output_021.txt


.. _wwdm_call_022:

Call
====

Example illustrating :

  - modifying **duration**,
  - value '**linear**' as plan
  - value '**dataframe**' as restype

  - value of type *vname.oname* as '**outselect**' :

    values 'view.top:wwdm.LAI', 'view.top:wwdm.ST' to select the output datas
    named 'LAI', 'ST' of the view named 'view'.

  .. literalinclude:: req_post_vpz_output_022.txt


.. _wwdm_call_023:

Call
====

Example illustrating :

  - modifying **duration**,
  - value '**single**' as plan
  - value '**matrix**' as restype
  - value '**all**' as '**outselect**',
    to select all output datas of all views.

  .. literalinclude:: req_post_vpz_output_023.txt

.. _wwdm_call_024:

Call
====

Example illustrating :

  - modifying **duration**,
  - value '**linear**' as plan 
  - value '**matrix**' as restype
  - value '**all**' as '**outselect**',
    to select all output datas of all views.

  .. literalinclude:: req_post_vpz_output_024.txt

.. _wwdm_call_025:

Call
====

Example illustrating :

  - modifying **duration**,
  - '**tree**' as style of presentation
  - value '**all**' as '**outselect**',
    to select all output datas of all views.

  .. literalinclude:: req_post_vpz_output_025.txt

.. _wwdm_call_026:

Call
====

Example illustrating :

  - modifying **duration**,
  - '**link**' as style of presentation
  - value '**all**' as '**outselect**',
    to select all output datas of all views.

  .. literalinclude:: req_post_vpz_output_026.txt

.. _wwdm_call_032:

Call
====

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters with '**pars**',

  - '**tree**' as style of presentation,

  - value '**single**' as plan, or
    value '**linear**' in case of  multiple simulation
  - value '**dataframe**' as restype *(or 'matrix')*

  - value '**all**' as '**outselect**',

  .. literalinclude:: req_post_vpz_output_032.txt

