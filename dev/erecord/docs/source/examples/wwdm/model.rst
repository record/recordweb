.. _wwdm_model:

==========
WWDM model
==========

WWDM as an agronomic model
==========================

WWDM (Winter Wheat Dry Matter) is a dynamical model with 2 equations and 2 state variables.

**Equations**

Above ground biomass :
U(t+1) = U(t) + Eb * Eimax * ( 1 - e^(K * LAI(t)) ) * PAR(t) 

Leaf area index :
LAI(t) = Lmax * ( 1 / ( 1 + e^(-A*(ST(t)-TI)) ) - e^(-B*(ST(t)-Tr)) )

with :
Tr = (1/B) * log(1+e^(A * TI)) , U_1 = 0 , LAI_1 = 0

**Input variables**

- PAR(t) : photosynthetically active radiation
- ST(t) : cumulative degree day

**Parameters**

====== ===================================  ========  ======= ==============
Eb     Radiation use efficiency             g/m^2     1.85    0.9-2.8 
------ -----------------------------------  --------  ------- --------------
Eimax  Max ratio of intercepted to                    0.94    0.9-0.99
       incident radiation
------ -----------------------------------  --------  ------- --------------
K      Coefficient of extinction                      0.7     0.6-0.8
------ -----------------------------------  --------  ------- --------------
Lmax   Maximal value of LAI                           7.5     3-12
------ -----------------------------------  --------  ------- --------------
TI     Temperature threshold                degre C   900     700-1100
------ -----------------------------------  --------  ------- --------------
A      Coefficient of LAI increase                    0.0065  0.0035-0.01
------ -----------------------------------  --------  ------- --------------
B      Coefficient of LAI decrease                    0.00205 0.0011-0.0025
====== ===================================  ========  ======= ==============

WWDM as a model of the Record platform
======================================

The WWDM model has been developed as an :term:`agronomic model` of the :term:`Record` platform, and so as a :term:`vle model`.

Simulator, model, models repository
+++++++++++++++++++++++++++++++++++

The WWDM model is the vle model named 'wwdm'. This :term:`model` (also called a :term:`vle package`) contains some :term:`simulator`\ s (also called :term:`vpz file`\ s). The simulator that will be used in this example is the 'wwdm.vpz' :term:`simulator` of the 'wwdm' :term:`model`.

In the 'wwdm.vpz' simulator, the crop model of the 'wwdm' model is coupled with a climate series reader model of another vle model, the 'meteo' model. The model for weather (*of 'meteo' model*) reads climatic datas contained in a file. It provides to the crop model (*of 'wwdm' model*) datas for RG (Global Radiation), Tmin (minimal daily temperature) and Tmax (maximal daily temperature), from which the crop model computes PAR and ST.

The 'wwdm.vpz' simulator is part of the 'wwdm' model. The 'wwdm' model (and its 'meteo' model dependency) are distributed by the Record platform, corresponding into erecord with the 'recordb' :term:`models repository`. This example takes into account the 'recordb' :term:`models repository` developed with vle-1.1.3 :term:`vle version`.

Thus the **'wwdm.vpz' simulator**, that belongs to the **'wwdm' model**, is going to be used by relying on the **'recordb' models repository** of erecord
developed with **'vle-1.1.3' vle version**.

.. _wwdm_databases:

WWDM into erecord databases
+++++++++++++++++++++++++++

*For more about the erecord databases*, see :ref:`databases`.

The erecord databases contain some available vle versions, some of their models repositories, their models, and their simulators. A :term:`simulator` can be recorded into the erecord databases as a :ref:`dm_vlevpz` that is attached to a :term:`model` recorded as a :ref:`dm_vlepkg` that is attached to a :term:`models repository` recorded as a :ref:`dm_vlerep` that is attached to a :term:`vle version` recorded as a :ref:`dm_vleversion`.

In our example case, the 'wwdm.vpz' simulator is attached to the 'wwdm' model that is attached to the 'recordb' models repository that is attached to the 'vle-1.1.3' vle version. The erecord databases contain in particular :

  - the 'vle-1.1.3' vle version (:ref:`dm_vleversion`, *vle version*)

    - the 'recordb' models repository (:ref:`dm_vlerep`, *vle rep*)

      - the 'wwdm' model (:ref:`dm_vlepkg`,
        *vle pkg, vle package, agronomic model*)

        - the 'wwdm.vpz' simulator (:ref:`dm_vlevpz`,
          *vle vpz, vpz file, simulator*)

Highlights
==========

The simulator of the example is 'wwdm.vpz'. It belongs to the 'wwdm' model that is hosted by the 'recordb' models repository developed with 'vle-1.1.3' vle version.

More
====

*Next step :* :ref:`wwdm_ident`

*Back to :* :ref:`example_wwdm`

