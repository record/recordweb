.. _documentation:

=============
Documentation
=============

Main documentation
==================

The **main documentation** of erecord project is produced with :term:`Sphinx` that (among other things) generates an API reference from source code.

- :ref:`Main documentation home <home_erecord>`.

- :ref:`Source code API reference <apiref_index>`.

*Production and generation :*

See "**Main documentation generation**" into :ref:`install_prod` or
:ref:`install_dev`.

Other external documentation
============================

There is some other external documentation, for example **html home pages produced for erecord simulators** *(with a specific python script)*.

*Production and generation :*

See :ref:`Installation of simulators HTML home pages <install_simulators_html_home_pages>`.

