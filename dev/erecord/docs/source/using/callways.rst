.. _callways:

=================================
Ways how to call the web services
=================================

Overview
========

As RESTful web services, the :term:`erecord` web services are based on the
**HTTP protocol** : they can be called from 'anything' that is able to send
an HTTP request 
(`an illustration <http://147.100.179.168/docs/erecord/erecord.pdf>`_ *(fr)*).

There are different ways how to call the :term:`erecord` web services :

  - from **software programs** written **in different languages** (any language supporting the HTTP protocol, such as for example Python, R, C++, Java, Php...).
  - from **command line tools** such as for example **cURL** that is  a command-line tool for transferring data using various protocols among which the HTTP one. 
  - from a **webbrowser**

In addition a **Web User Interface** (:ref:`access <webui>`) has been
developped to make easier calling some erecord web services.

Illustrations
=============

  - See :

    `Using an agronomic model developped with the Record platform and installed into the erecord web services <http://147.100.179.168/docs/erecord/model_user.pdf>`_ *(fr) (Utiliser un modèle agronomique développé sous la plateforme Record et disponible dans les services web erecord)*.

  - See :

    :ref:`Call in a webbrowser <post_vpz_output_webbrowser>` |
    :ref:`Call in command line with cURL <post_vpz_output_curl>`

    :ref:`Call in Python language <post_vpz_output_python>` |
    :ref:`Call in R language <post_vpz_output_r>` |
    :ref:`Call in PHP language <post_vpz_output_php>`

Use cases by webbrowser
=======================

- **Build request into a webbrowser** :

    You can write your request into a webbrowser (the request and its
    parameters) : :ref:`example <post_vpz_output_webbrowser>`.

- **Call the Web User Interface** :

    You can call the :ref:`webui`.

- **Customize html home pages** :

    You can build a html home page dedicated to the model you are interested
    in, and customize it : choose which parameters to show, add some
    information (units...), control the filled in values...

    Examples :
    `a WWDM home page <http://147.100.179.168/docs/models/wwdm/accueil_wwdm.html>`_ *(fr)* |
    `a recordschool home page <http://147.100.179.168/docs/models/recordschool/accueil_recordschool.html>`_ *(fr)*

    Such home pages may already exist for some models, for example if the
    model owner has given one when he has delivered its model to erecord.
  
    You can modify an existing home page as you want according to your own
    case, then keep it for yourself or share it, that is to say send it to
    some colleagues.

    For :ref:`more <wwdm_homepage>`.

