.. _index:

.. _home_erecord:

==================
Welcome to erecord
==================

Content
-------

The **erecord** project provides **web services** that allow to edit, modify and simulate some models developed with :term:`Vle`, such as the :term:`Record` platform ones.

Project
=======

    :ref:`erecord_ip` |
    :ref:`erecord_code_access` |
    :ref:`erecord_contacts`

  - Description :

    :ref:`software_content` |
    :ref:`erecord_package`

    :ref:`databases` |
    :ref:`data_model` |
    :ref:`repositories`

    :ref:`documentation` |
    :ref:`features`

  - See also :

    :ref:`faqs_owners` |
    :ref:`faqs_models`

For users
=========

    :ref:`webapi` |
    :ref:`webui` |
    :ref:`How to call the web services <callways>`

    :ref:`Examples <examples>`

  - See also :

    :ref:`To know more about some features <features>` |
    :ref:`faqs_users`

For developers
==============

    :ref:`documentation` |
    :ref:`Source code API reference <apiref_index>`

    :ref:`install` |
    :ref:`tests`

  - See also :

    :ref:`faqs_developers`

Frequently Asked Questions
==========================

    :ref:`For users <faqs_users>` |
    :ref:`For model owners <faqs_owners>` |
    :ref:`About agronomic models <faqs_models>` |
    :ref:`For developers <faqs_developers>`

Around erecord
==============

    :ref:`related` |
    :ref:`presentations` |
    :ref:`glossary`

Table of contents
-----------------

.. toctree::
   :maxdepth: 2

   home_project
   home_users
   home_developers
   faqs/index
   home_around

