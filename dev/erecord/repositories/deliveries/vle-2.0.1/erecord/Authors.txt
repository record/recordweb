			      erecord development team
			      ========================


Copyright
=========

- INRA
  - Copyright © 2019-2024
  - French National Institute for Agricultural Research
  - Institut National de la Recherche Agronomique
  - http://www.inra.fr



Authors
=======

Main programmer and founder
---------------------------

- Nathalie Rousse <nathalie.rousse@inra.fr>

