/*
 * @file erecord/RunVpz.cpp
 *
 * This file is part of erecord project
 *
 * Copyright (c) 2019-2024 INRA http://www.inra.fr
 *
 */

#include <iostream>
#include <fstream>

#include <vle/devs/Dynamics.hpp>

#include "erecord_vle.hpp"
#include "erecord_json.hpp"


class RunVpz : public vle::devs::Dynamics {

public:

    RunVpz(const vle::devs::DynamicsInit& mdl,
           const vle::devs::InitEventList& lst)
         : vle::devs::Dynamics(mdl, lst){}

    virtual ~RunVpz(){}

    /*
     * Runs the simulation and writes the simulation results into
     * "run_output.json" (json format)
     *
     * Note : The command parameters are read into "run_input.json"
     * (pkgname, vpzname, plan, restype...), that has been previously built
     * and copied into erecord/data (for example by erecord)
     *
     * Note :
     * - The simulation results are those of views in 'storage' plugin.
     * - Some other simulation results may exist, as files, in case of 
     *   views in 'file' plugin.
     */
    virtual vle::devs::Time init(vle::devs::Time /*time*/) override {

        bool cr_ok = true; // default

        // jinput
        std::stringstream ss;
        auto erecord_ctx = vle::utils::make_context();
        vle::utils::Package erecord_pack(erecord_ctx, "erecord");
        std::ifstream inputfile(erecord_pack.getDataFile("run_input.json",
                                                         utils::PKG_BINARY));
        ss << inputfile.rdbuf();
        inputfile.close();
        json jinput = json::parse(ss);

        // pkgname
        std::string pkgname; bool pkgname_found;
        json_get_pkgname(&pkgname, &pkgname_found, &jinput);

        // vpzname
        std::string vpzname; bool vpzname_found;
        json_get_vpzname(&vpzname, &vpzname_found, &jinput);

        // can be added : control that pkgname and vpzname exist

        if (pkgname_found && vpzname_found){

            // Vpz
            auto ctx = vle::utils::make_context();
            vle::utils::Package pack(ctx, pkgname);
            vpz::Vpz *file = new vpz::Vpz(pack.getExpFile(vpzname,
                                                          utils::PKG_BINARY));
            json object;

            // continues to read jinput and takes it into account

            /* plan (values : "linear" "single") */
            std::string plan;
            json_get_plan_or_default(&plan, &jinput);

            /* restype (values : "matrix" "dataframe") */
            std::string restype;
            json_get_restype_or_default(&restype, &jinput);

            json jres;
            if (plan == "linear"){
                value::Matrix* res;
                if (restype == "matrix"){
                    res = vle_run_manager_matrix(file, jres);
                } else { // restype == "dataframe"
                    res = vle_run_manager(file, jres);
                }
            } else { // plan == "single"
                value::Map* res;
                if (restype == "matrix"){
                    res = vle_run_matrix(file, jres);
                } else { // restype == "dataframe"
                    res = vle_run(file, jres);
                }
            }
            object["plan"] = plan;
            object["restype"] = restype;
            object["res"] = jres;

            std::ofstream outfile;
            outfile.open("run_output.json");
            outfile << object;
            outfile.close();

        } else {

            cr_ok = false;

            if (!pkgname_found){
                std::cerr << "-- " << pkgname;
                std::cerr << " not found into run_input.json --" << "\n";
            }
            if (!vpzname_found){
                std::cerr << "-- " << vpzname;
                std::cerr << " not found into run_input.json --" << "\n";
            }
        }

        if (cr_ok){ create_ok_file(); }
        return vle::devs::infinity;
    }

    virtual void output(vle::devs::Time /*time*/,
                 vle::devs::ExternalEventList& /*output*/) const override {}

    virtual vle::devs::Time timeAdvance() const override {}

    virtual void internalTransition(vle::devs::Time /*time*/) override {}

    virtual void externalTransition(
                            const vle::devs::ExternalEventList& /*event*/,
                            vle::devs::Time /*time*/) override {}

    virtual void confluentTransitions(vle::devs::Time /* time */,
               const vle::devs::ExternalEventList& /* events */) override {}

    virtual std::unique_ptr<vle::value::Value> observation(
            const vle::devs::ObservationEvent& /* event */) const override {}

    virtual void finish(){}
};

DECLARE_DYNAMICS(RunVpz)

