/*
 * @file erecord/ReadVpz.cpp
 *
 * This file is part of erecord project
 *
 * Copyright (c) 2019-2024 INRA http://www.inra.fr
 *
 */

#include <iostream>
#include <fstream>

#include <vle/devs/Dynamics.hpp>

#include "erecord_vle.hpp"
#include "erecord_json.hpp"


class ReadVpz : public vle::devs::Dynamics {

public:

    ReadVpz(const vle::devs::DynamicsInit& mdl,
            const vle::devs::InitEventList& lst)
          : vle::devs::Dynamics(mdl, lst){}

    virtual ~ReadVpz(){}

    /*
     * Reads the input information into the vpz file (parameters, begin...)
     * and writes it into "read_output.json" (json format)
     *
     * Note : The command parameters are read into "read_input.json"
     * (pkgname, vpzname), that has been previously built and copied into
     * erecord/data (for example by erecord)
     */
    virtual vle::devs::Time init(vle::devs::Time /*time*/) override {

        bool cr_ok = true; // default

        // jinput
        std::stringstream ss;
        auto erecord_ctx = vle::utils::make_context();
        vle::utils::Package erecord_pack(erecord_ctx, "erecord");
        std::ifstream inputfile(erecord_pack.getDataFile("read_input.json",
                                                     vle::utils::PKG_BINARY));
        ss << inputfile.rdbuf();
        inputfile.close();
        json jinput = json::parse(ss);

        // pkgname
        std::string pkgname; bool pkgname_found;
        json_get_pkgname(&pkgname, &pkgname_found, &jinput);

        // vpzname
        std::string vpzname; bool vpzname_found;
        json_get_vpzname(&vpzname, &vpzname_found, &jinput);

        // can be added : control that pkgname and vpzname exist

        if (pkgname_found && vpzname_found){

            auto ctx = vle::utils::make_context();
            vle::utils::Package pack(ctx, pkgname);
            vpz::Vpz *file = new vpz::Vpz(
                          pack.getExpFile(vpzname, vle::utils::PKG_BINARY));
            json object;
            vle_read_vpz_input(file, object);
            std::ofstream outfile;
            outfile.open("read_output.json");
            outfile << object;
            outfile.close();

        } else {
            cr_ok = false;

            if (!pkgname_found){
                std::cerr << "-- " << pkgname;
                std::cerr << " not found into read_input.json --" << "\n";
            }
            if (!vpzname_found){
                std::cerr << "-- " << vpzname;
                std::cerr << " not found into read_input.json --" << "\n";
            }
        }

        if (cr_ok){ create_ok_file(); }
        return vle::devs::infinity;
    }

    virtual void output(vle::devs::Time /*time*/,
                 vle::devs::ExternalEventList& /*output*/) const override {}

    virtual vle::devs::Time timeAdvance() const override {}

    virtual void internalTransition(vle::devs::Time /*time*/) override {}

    virtual void externalTransition(
                            const vle::devs::ExternalEventList& /*event*/,
                            vle::devs::Time /*time*/) override {}

    virtual void confluentTransitions(vle::devs::Time /* time */,
               const vle::devs::ExternalEventList& /* events */) override {}

    virtual std::unique_ptr<vle::value::Value> observation(
            const vle::devs::ObservationEvent& /* event */) const override {}

    virtual void finish(){}
};

DECLARE_DYNAMICS(ReadVpz)

