/*
 * @file erecord/erecord_vle.hpp
 *
 * This file is part of erecord project
 *
 * Copyright (c) 2019-2024 INRA http://www.inra.fr
 *
 */

#include <vle/vpz/Vpz.hpp>
#include <vle/manager/Manager.hpp>
#include <vle/manager/Simulation.hpp>
#include <vle/utils/Package.hpp>
#include <vle/value/Value.hpp>
#include <vle/value/Integer.hpp>
#include <vle/value/Set.hpp>
#include <vle/value/Table.hpp>
#include <vle/value/Tuple.hpp>
#include <vle/value/Matrix.hpp>
#include <vle/value/XML.hpp>
#include <vle/value/Null.hpp>
#include <vle/utils/Tools.hpp>
#include <vle/utils/Context.hpp>

using namespace vle;

#include "json.hpp"
using json = nlohmann::json;

/*****************************************************************************
 *
 * Conversions between vle::value::Value format and json format
 *
 *****************************************************************************/

/*************************************
 * Conversions into vle::value::Value
 *************************************/

value::Value* int_to_vlevalue(long i){
    return value::Integer::create(i).release();
}

value::Value* real_to_vlevalue(float i){
    return value::Double::create(i).release();
}

value::Value* str_to_vlevaluexml(std::string i){
    return value::Xml::create(i).release();
}

value::Value* string_to_vlevalue(std::string i){
    return value::String::create(i).release();
}

value::Value* bool_to_vlevalue(bool i){
    return value::Boolean::create(i).release();
}

value::Value* null_to_vlevalue(){
    return value::Null::create().release();
}

// vle::value::Set
value::Value* create_vlevalueset(){   
    return value::Set::create().release();
}
void add_value_to_vlevalueset(value::Value* set, value::Value* val){
    set->toSet().add(val->clone());
}

// vle::value::Map
value::Value* create_vlevaluemap(){   
    return value::Map::create().release();
}
void add_value_to_vlevaluemap(value::Value* map,
                              std::string key, value::Value* val){
    map->toMap().add(key, val->clone());
}

// vle::value::Table
value::Value* create_vlevaluetable(unsigned int width, unsigned int height){
    return value::Table::create(width, height).release();
}
void set_value_to_vlevaluetable(value::Value* table,
                                unsigned int i, unsigned int j, double v){
    table->toTable()(i,j) = v;
}

// vle::value::Matrix
value::Value* create_vlevaluematrix(unsigned int width, unsigned int height){
    return value::Matrix::create(width, height).release();
}
void set_value_to_vlevaluematrix(value::Value* mat,
                            unsigned int i, unsigned int j, value::Value* v){
    mat->toMatrix().set(i,j,v->clone());
}

// vle::value::Tuple
value::Value* create_vlevaluetuple(unsigned int size){
    return value::Tuple::create(size,0).release();
}
void set_value_to_vlevaluetuple(value::Value* tuple, unsigned int i, double v){
    tuple->toTuple().value()[i] = v;
}

/************************************************************
 * Conversion of json format to vle::value::Value format
 ************************************************************/

/*
 * Controls consistency between type_a_priori and type of json object
 * Returns type_to_be_returned (type of vle::value::Value) 
 * (valuing "error_case" in case of inconsistency/error)
 */
std::string get_type_to_be_returned(std::string type_a_priori, json& object){

    std::string type_to_be_returned = "error_case"; // default
    std::string jtype; // type of a json
    jtype = std::string(object.type_name());

    if (type_a_priori == "BOOLEAN"){
        if (jtype == "boolean"){ type_to_be_returned = type_a_priori; }
    } else if (type_a_priori == "INTEGER"){
        if (jtype == "number"){
            if ((object.is_number_integer()) ||
                (object.is_number_unsigned())){
                type_to_be_returned = type_a_priori;
            }
        }
    } else if (type_a_priori == "DOUBLE"){
        if (jtype == "number"){
            if ((object.is_number_float()) ||
                (object.is_number_integer()) ||
                (object.is_number_unsigned())){
                type_to_be_returned = type_a_priori;
            }
        }
    } else if (type_a_priori == "STRING"){
        if (jtype == "string"){ type_to_be_returned = type_a_priori; }
    } else if (type_a_priori == "XMLTYPE"){
        if (jtype == "string"){ type_to_be_returned = type_a_priori; }
    } else if (type_a_priori == "MAP"){
        if (jtype == "object"){ type_to_be_returned = type_a_priori; }
    } else if (type_a_priori == "SET"){
        if (jtype == "array"){ type_to_be_returned = type_a_priori; }
    } else if (type_a_priori == "TUPLE" ){
        if (jtype == "array"){ type_to_be_returned = type_a_priori; }
    } else if (type_a_priori == "TABLE"){
        if (jtype == "array"){ type_to_be_returned = type_a_priori; }
    } else if (type_a_priori == "MATRIX"){
        if (jtype == "array"){ type_to_be_returned = type_a_priori; }
    } else if (type_a_priori == "NIL"){
        if (jtype == "null"){ type_to_be_returned = type_a_priori; }
    } else { // type_a_priori == "", default
        if (jtype == "boolean"){
            type_to_be_returned = "BOOLEAN";
        } else if (jtype == "string"){
            type_to_be_returned = "STRING";
        } else if (jtype == "number"){
            if (object.is_number_float()){
                type_to_be_returned = "DOUBLE";
            } else if ((object.is_number_integer()) ||
                       (object.is_number_unsigned())){
                type_to_be_returned = "INTEGER";
            }
        } else if (jtype == "array"){
            type_to_be_returned = "SET";
        } else if (jtype == "object"){
            type_to_be_returned = "MAP";
        } else if (jtype == "null"){
            type_to_be_returned = "NIL";
        //} else { // default
        }
    }
    return type_to_be_returned;
}

/*
 * Returns vle::value::Value value, conversion of json object according to
 * type_a_priori if given (different from "")
 */
value::Value* json_to_vlevalue(json& object, std::string type_a_priori,
                               value::Value* Vref){
    /*
     * - object : objet json to be converted
     *   json type possible values :
     *   json j_boolean = true; true                          is a "boolean"
     *   json j_string = "Hello, world"; "Hello, world"       is a "string"
     *   json j_number_integer = -17; -17                     is a "number"
     *   json j_number_unsigned = 42u; 42                     is a "number"
     *   json j_number_float = 23.42;  23.42                  is a "number"
     *   json j_array = {1,2,4,8,16}; [1,2,4,8,16]            is an "array"
     *   json j_object = {{"a1",1},{"a2",2}}; {"a1":1,"a2":2} is an "object"
     *   json j_null; null                                    is a "null"
     * 
     * - type_a_priori : 
     *   possible values :
     *   values returned by vle_getTypeName(vle::value::Value::type) + ""
     *   => "" or "BOOLEAN" "INTEGER" "DOUBLE" "STRING" "XMLTYPE"
     *            "SET" "MAP" "TUPLE" "TABLE" "MATRIX" "NIL"
     * 
     * - Vref : vle::value::Value whose type could be compared with the
     *   converted value one's
     *
     * Note : Vref is not used
     */


    value::Value* val = NULL; // default
    int i, j;
    std::string jtype; // type of a json
    value::Value* nullVref = NULL;

    /* values of type_to_be_returned : values of type_a_priori + "error_case" */
    std::string type_to_be_returned = get_type_to_be_returned(type_a_priori,
                                                              object);
    if (type_to_be_returned == "BOOLEAN"){
        bool v = object; val = bool_to_vlevalue(v);
    } else if (type_to_be_returned == "INTEGER"){
        int v = object; val = int_to_vlevalue(v);
    } else if (type_to_be_returned == "DOUBLE"){
        double v = object; val = real_to_vlevalue(v);
    } else if (type_to_be_returned == "STRING"){
        std::string v = object; val = string_to_vlevalue(v);
    } else if (type_to_be_returned == "XMLTYPE"){
        std::string v = object; val = str_to_vlevaluexml(v);
    } else if (type_to_be_returned == "NIL"){
        val = null_to_vlevalue();
    } else if (type_to_be_returned == "SET"){
        val = create_vlevalueset();
        for (auto& v : object.items()){
            /* { // looks for 1st elt into SET Vref (if Vref is SET, NON EMPTY)
                value::Value* newVref = NULL; bool found = false;
                value::Set::const_iterator it = Vref->toSet().begin();
                newVref = *it;
                found = true;
            } */
            add_value_to_vlevalueset(val, json_to_vlevalue(v.value(),
                                     "", nullVref));
        }
    } else if (type_to_be_returned == "MAP"){
        val = create_vlevaluemap();
        for (auto& v : object.items()){
            /* { // looks for key into MAP Vref (if Vref is MAP)
                vle::value::Value* newVref = NULL; bool found = false;
                for (vle::value::Map::const_iterator it = Vref->toMap().begin();
                     (it != Vref->toMap().end()) && !found; ++it){
                    if (it->first.c_str() == v.key()){
                        newVref = it->second;
                        found = true;
                    }
                }
            } */
            add_value_to_vlevaluemap(val, v.key(), json_to_vlevalue(v.value(),
                                     "", nullVref));
        }
    } else if (type_to_be_returned == "TUPLE"){
        val = create_vlevaluetuple(object.size());
        i = 0;
        for (auto& v : object.items()){
            json jv = v.value();
            jtype = std::string(jv.type_name());
            if (jtype == "number"){ // float
                set_value_to_vlevaluetuple(val, i, jv);
                i = i + 1;
            } else { // cannot convert to float
                type_to_be_returned = "error_case"; 
            }
        }
    } else if (type_to_be_returned == "TABLE"){
        val = value::Null::create().release();
        i = 0;
        for (auto& v : object.items()){
            json jv = v.value();
            jtype = std::string(jv.type_name());
            if (jtype == "array"){ // list
                j = 0;
                for (auto& v1 : jv.items()){
                    json jv1 = v1.value();
                    jtype = std::string(jv1.type_name());
                    if (jtype == "number"){ // float
                        if (val->isNull()){
                            val = create_vlevaluetable(jv.size(),
                                                       object.size());
                        }
                        set_value_to_vlevaluetable(val, j, i, v1.value());
                        j = j + 1;
                    } else { // cannot convert to float
                        type_to_be_returned = "error_case"; 
                    }
                }
                i = i + 1;
            } else { // cannot convert to list
                type_to_be_returned = "error_case"; 
            }
        }
    } else if (type_to_be_returned == "MATRIX"){
        val = value::Null::create().release();
        /* { // looks for 1st elt into MATRIX Vref (if Vref is MATRIX,
             //                                                NON EMPTY)
            vle::value::Value* newVref = NULL; bool found = false;
            vle::value::Matrix& t = Vref->toMatrix();
            if ( (t.rows() > 0) && (t.columns() > 0) ){ // Vref NON EMPTY
                newVref = t.get(j,i);
                found = true;
            }
        } */
        i = 0;
        for (auto& v : object.items()){
            json jv = v.value();
            jtype = std::string(jv.type_name());
            if (jtype == "array"){ // list
                j = 0;
                for (auto& v1 : jv.items()){
                    json jv1 = v1.value();
                    jtype = std::string(jv1.type_name());
                    if (jtype == "number"){ // float
                        val = create_vlevaluematrix(jv.size(), object.size());
                    }
                    set_value_to_vlevaluematrix(val, j, i, 
                                  json_to_vlevalue(v1.value(), "", nullVref));
                    j = j + 1;
                }
                i = i + 1;
            } else { // cannot convert to list
                type_to_be_returned = "error_case"; 
            }
        }
    } else { // default
        type_to_be_returned = "error_case";
    }

    if (type_to_be_returned == "error_case"){
        val = NULL;
    }
    return val;
}

/************************************************************
 * Conversion of vle::value::Value (value) into json (object)
 ************************************************************/
void vlevalue_to_json(const value::Value& value, json& object){

    switch (value.getType()){
    case value::Value::BOOLEAN:{
        //json jtype = {"type", "BOOLEAN"};
        //json jvalue = {"value", vle::value::toBoolean(value)};
        //object = {jtype, jvalue};
        object = value::toBoolean(value);
        break;
    }
    case value::Value::INTEGER:{
        //json jtype = {"type", "INTEGER"};
        object = value::toInteger(value);
        break;
    }
    case value::Value::DOUBLE:{
        //json jtype = {"type", "DOUBLE"};
        object = value::toDouble(value);
        break;
    }
    case value::Value::STRING:{
        //json jtype = {"type", "STRING"};
        object = value::toString(value).c_str();
        break;
    }
    case value::Value::XMLTYPE:{
        //json jtype = {"type", "XMLTYPE"};
        object = value::toXml(value).c_str();
        break;
    }
    case value::Value::SET:{
        //json jtype = {"type", "SET"};
        json jset(json::value_t::array);
        for (value::Set::const_iterator it = value.toSet().begin();
            it != value.toSet().end(); ++it) {
            json v;
            vlevalue_to_json(**it, v);
            jset += v;
        }
        object = jset;
        break;
    }
    case value::Value::MAP:{
        //json jtype = {"type", "MAP"};
        json jmap(json::value_t::object);
        for (value::Map::const_iterator it = value.toMap().begin();
             it != value.toMap().end(); ++it) {
            json v;
            vlevalue_to_json(*(it->second), v);
            jmap[it->first.c_str()] = v;
        }
        object = jmap;
        break;
    }
    case value::Value::TUPLE:{
        //json jtype = {"type", "TUPLE"};
        json jval(json::value_t::array);
        std::vector<double>::const_iterator itb =
                                              value.toTuple().value().begin();
        std::vector<double>::const_iterator ite =
                                              value.toTuple().value().end();
        for(;itb!=ite;itb++){
            jval += *itb;
        }
        object = jval;
        break;
    }
    case value::Value::TABLE:{
        //json jtype = {"type", "TABLE"};
        json jval(json::value_t::array);
        const value::Table& t = value.toTable();
        for(unsigned int i=0; i<t.height(); i++){
            json jr;
            for(unsigned int j=0; j<t.width(); j++){
                jr += t.get(j,i);
            }
            jval += jr;
        }
        object = jval;
        break;
    }
    case value::Value::MATRIX:{
        //json jtype = {"type", "MATRIX"};
        json jval(json::value_t::array);
        const value::Matrix& t = value.toMatrix();
        for(unsigned int i=0; i<t.rows(); i++){
            json jr;
            for(unsigned int j=0; j<t.columns(); j++){
                json v;
                vlevalue_to_json(*t.get(j,i), v);
                jr += v;
            }
            jval += jr;
        }
        object = jval;
        break;
    }
    default:{
        // json jtype = {"type", "NONE"};
        json jnull;
        object = jnull;
        break;
    }}
}

/****************************************************************************
 *
 * vle
 *
 ****************************************************************************/

/*
 * Returns string corresponding with a vle::value::Value::type, or ""
 *
 * Note : returns "" for unknown vle::value::Value::type value or for
 * vle::value::Value::USER that is treated as unknown
 */
std::string vle_getTypeNameDefaultValue(){
    return "";
}
std::string vle_getTypeName(value::Value::type type){
    switch (type){
    case value::Value::BOOLEAN:
        return "BOOLEAN";
    case value::Value::INTEGER:
        return "INTEGER";
    case value::Value::DOUBLE:
        return "DOUBLE";
    case value::Value::STRING:
        return "STRING";
    case value::Value::XMLTYPE:
        return "XMLTYPE";
    case value::Value::SET:
        return "SET";
    case value::Value::MAP:
        return "MAP";
    case value::Value::TUPLE:
        return "TUPLE";
    case value::Value::TABLE:
        return "TABLE";
    case value::Value::MATRIX:
        return "MATRIX";
    case value::Value::NIL:
        return "NIL";
    default: // default or case vle::value::Value::USER:
        return vle_getTypeNameDefaultValue(); // value "" added 
    }
}

/*****************************************************************************
 * Conditions, parameters
 *****************************************************************************/

void vle_parameter_clear_values(vpz::Condition& c, std::string parname){
    c.clearValueOfPort(parname);
}

void vle_parameter_add_a_value(vpz::Condition& c, std::string parname,
                               value::Value* value){

    c.addValueToPort(parname, value->clone());         //TODO fuite memoire ?
                                // la condition devrait prendre l'ownership ?
}

/* Updates a parameter */
void vle_parameter_set_values(vpz::Vpz* file, std::string condname,
                              std::string parname, json* jvalues){
    /*
     * Updates a parameter values (one or list of vle::value::Value)
     * according to jvalues (json value).
     *
     * Note :
     * If jvalues is a json of "array" type, the parameter values must
     * always be given as multiple (multi-simulation case). The case where
     * it would be given as a singleton (one simulation case) is not treated.
     *
     * Note :
     * A json of "array" type corresponds with vle::value::Value types : 
     * vle::value::Value::SET,TUPLE,TABLE,MATRIX.
     */

    assert(file);
    vpz::Condition& c(file->project().experiment().conditions().get(condname));

    std::string type_vref = vle_getTypeNameDefaultValue(); // default
    value::Value* Vref = NULL; // default
    std::vector<std::shared_ptr<value::Value>>& inplace =
                                                      c.getSetValues(parname);
    if (inplace.size() > 0){
        Vref = inplace[0]->clone().get();
        type_vref = vle_getTypeName(inplace[0]->getType());
    }
    vle_parameter_clear_values(c, parname);

    if (std::string(jvalues->type_name()) == "array"){
        for (auto& values : jvalues->items()){
            vle_parameter_add_a_value(c, parname,
                           json_to_vlevalue(values.value(), type_vref, Vref));
        }
    } else { // singleton case
        vle_parameter_add_a_value(c, parname,
                                 json_to_vlevalue(*jvalues, type_vref, Vref));
    }
}


/*****************************************************************************
 * begin, duration
 *****************************************************************************/

/* Updates begin */
void vle_set_begin(vpz::Vpz* file, double* begin){
    assert(file);
    file->project().experiment().setBegin(*begin);
}

/* Updates duration */
void vle_set_duration(vpz::Vpz* file, double* duration){
    assert(file);
    file->project().experiment().setDuration(*duration);
}

/*****************************************************************************
 * Views, output datas
 *****************************************************************************/

/* Updates an output plugin */
void vle_output_set_plugin(vpz::Vpz* file, std::string outputname,
                           std::string plugin){
    assert(file);
    vpz::Output* p_output =
             &(file->project().experiment().views().outputs().get(outputname));
    std::string location = p_output->location();
    std::string package = p_output->package();
    std::shared_ptr< value::Value > data; // data copy
    if (p_output->data()) {
        data = p_output->data()->clone();
    } else {
        data = 0;
    }
    p_output->setStream(location, plugin, package);
    p_output->setData(data); // restaures data
}

/* Updates an output plugin of a view */
void vle_view_set_output_plugin(vpz::Vpz* file, std::string viewname,
                                std::string plugin){

    assert(file);
    vpz::View view = file->project().experiment().views().get(viewname);
    std::string output_name = view.output(); 
    vle_output_set_plugin(file, output_name, plugin);
}

/*
 * Modifies atom_name : last ',' is replaced by ':'
 *
 * Note : the atom_name input value is the atomic model name as defined by vle
 */
void vle_make_atomic_name_as_outside(std::string& atom_name){
    std::size_t last_found = atom_name.find_last_of(",");
    atom_name.replace(last_found, 1, ":");
}

/*
 * Modifies atom_name : last ':' is replaced by ','
 *
 * Note : the atom_name output value is the atomic model name as defined by vle
 */
void vle_make_atomic_name_as_in_vle(std::string& atom_name){
    std::size_t last_found = atom_name.find_last_of(":");
    atom_name.replace(last_found, 1, ",");
}

/*
 * builds an output data names :
 * - out_name (composed by atom_name and out_shortname).
 * - out_fullname (prefixation by view_name).
 *
 * Note : 
 * The atom_name input value is the atomic model name as defined by vle.
 * It is modified (by vle_make_atomic_name_as_outside) to compose out_name.
 */
void vle_build_out_names(std::string& view_name,
                         std::string& out_shortname, std::string& atom_name,
                         std::string& out_name, std::string& out_fullname){

    vle_make_atomic_name_as_outside(atom_name);
    std::stringstream ss;
    ss << atom_name << "." << out_shortname;
    out_name = ss.str();
    out_fullname = view_name + "." + out_name;
}

/*
 * decompose out_name as atom_name and out_shortname
 *
 * Note : 
 * The atom_name output value is the atomic model name as defined by vle.
 */
void vle_decompose_out_name(std::string& out_name,
                          std::string& out_shortname, std::string& atom_name){

    std::stringstream ss(out_name);
    std::getline(ss, atom_name, '.');
    std::getline(ss, out_shortname, '.');
    vle_make_atomic_name_as_in_vle(atom_name);
}

/* dels a view for all ObservablePort corresponding with out_name */
void vle_detach_view_from_observableport(vpz::Vpz *file,
                                         std::string& view_name,
                                         std::string& out_name){

    std::string atom_name; // as defined by vle
    std::string out_shortname;
    vle_decompose_out_name(out_name, out_shortname, atom_name);

    std::vector < vpz::AtomicModel* > list;
    file->project().model().getAtomicModelList(list);
    std::vector < vpz::AtomicModel* >::const_iterator itam_b = list.begin();
    std::vector < vpz::AtomicModel* >::const_iterator itam_e = list.end();
    vpz::Observables& observables=
                         file->project().experiment().views().observables();
    for(;itam_b!=itam_e;itam_b++){
        const vpz::AtomicModel& atom = **itam_b;
        std::string atomname = atom.getCompleteName();
        if (atomname == atom_name){
            const std::string& obs = atom.observables();
            if(obs != ""){
                const vpz::ObservablePortList& obsportlst =
                                   observables.get(obs).observableportlist();
                vpz::ObservablePortList::const_iterator ito_b =
                                                          obsportlst.begin();
                vpz::ObservablePortList::const_iterator ito_e =
                                                            obsportlst.end();
                for(;ito_e!=ito_b;ito_b++){
                    vpz::ObservablePort& port =
                                       observables.get(obs).get(ito_b->first);
                    std::string outshortname = ito_b->first;
                    if (outshortname == out_shortname){
                        // same out_name (ie atom_name and out_shortname)
                        vpz::ViewNameList list = port.viewnamelist();
                        vpz::ViewNameList::iterator itv_b = list.begin();
                        vpz::ViewNameList::iterator itv_e = list.end();
                        for(;itv_b!=itv_e;itv_b++){
                            std::string viewname = *itv_b;
                            if (viewname == view_name){
                                // same view_name, and out_name
                                if (port.exist(view_name)){
                                    port.del(view_name);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

/*****************************************************************************
 * all vpz input information
 *****************************************************************************/

/* reads a vpz input information */
void vle_read_vpz_input(vpz::Vpz *file, json& object){

    vpz::Experiment exp = file->project().experiment();

    object["experiment_name"] = exp.name();
    object["begin"]["value"] = exp.begin();
    object["duration"]["value"] = exp.duration();

    // conditions
    for (vpz::Conditions::const_iterator it = exp.conditions().begin();
         it != exp.conditions().end(); ++it){

        std::string condition_name = it->first; // as condition.name();
        vpz::Condition condition = it->second;

        std::vector < std::string > lst = condition.portnames();
        std::vector < std::string >::const_iterator itp;

        object["names"]["cond_names"] += condition_name;
        for (itp = lst.begin(); itp != lst.end(); ++itp){
            std::string port_name = itp->c_str();
            std::vector<std::shared_ptr<value::Value>>& inplace =
                                            condition.getSetValues(port_name);
            std::string full_name = condition_name+"."+port_name;
            object["names"]["par_names"] += full_name;

            json jvalues;
            int size = inplace.size();

            for (int i = 0; i < size; ++i){
                json newobject;
                vlevalue_to_json(*inplace[i], newobject);
                jvalues += newobject;
            }
            std::string type_0;
            if (size > 0){
	        type_0 = vle_getTypeName(inplace[0]->getType());
            } else {
                type_0 = vle_getTypeNameDefaultValue();
                std::cerr << "\nWarning in vle_read_vpz_input: ";
                std::cerr << "missing parameter value. Be careful while ";
                std::cerr << "using/parametrizing the simulator." << "\n";
            }

            json par_object;
	    par_object["type"] = type_0;
            par_object["value"] = jvalues;
            object["cond_list"][condition_name]["par_list"][port_name] =
                                                                    par_object;
        }
    }

    // views
    /* Note : only the output datas (VleOut) that exist at the initial
     * state will be taken into account (not the ones created during
     * the simulation).
     */
    vpz::ViewList& viewslst(exp.views().viewlist());
    vpz::ViewList::iterator it;
    int size = viewslst.size();
    if (size > 0) { // at least one view

        for (it = viewslst.begin(); it != viewslst.end(); ++it) {

            std::string view_name = it->first; // as view.name();
            vpz::View view = it->second;

            object["names"]["view_names"] += view_name;

            std::string view_type = view.streamtype();
            object["view_list"][view_name]["type"] = view_type;

            double view_timestep = view.timestep();
            object["view_list"][view_name]["timestep"] = view_timestep;

            /* "output" */
            json output_object;
            std::string output_name = view.output();
            output_object["name"] = output_name;
            vpz::Output* p_output = &(exp.views().outputs().get(output_name));
            output_object["plugin"] = p_output->plugin();
            //output_object["format"] = p_output->streamformat();
            output_object["location"] = p_output->location();
            //(+ fileformat (csv,text...), julianday... in file plugin case ?)
            object["view_list"][view_name]["output"] = output_object;
        }
        /* "out_list" */
        std::vector < vpz::AtomicModel* > list;
        vpz::BaseModel::getAtomicModelList(file->project().model().node(),
                                           list);
        std::vector < vpz::AtomicModel* >::const_iterator itam_b =
                                                                 list.begin();
        std::vector < vpz::AtomicModel* >::const_iterator itam_e = list.end();
        const vpz::Observables& observables=
                   file->project().experiment().views().observables();
        for(;itam_b!=itam_e;itam_b++){
            const vpz::AtomicModel& atom = **itam_b;
            const std::string& obs = atom.observables();
            if(obs != ""){
                const vpz::ObservablePortList& obsportlst =
                                    observables.get(obs).observableportlist();
                vpz::ObservablePortList::const_iterator ito_b =
                                                           obsportlst.begin();
                vpz::ObservablePortList::const_iterator ito_e =
                                                             obsportlst.end();
                for(;ito_e!=ito_b;ito_b++){
                    const vpz::ObservablePort& port =
                               observables.get(obs).get(ito_b->first);
                    vpz::ViewNameList list = port.viewnamelist();
                    vpz::ViewNameList::iterator itv_b = list.begin();
                    vpz::ViewNameList::iterator itv_e = list.end();
                    for(;itv_b!=itv_e;itv_b++){

                        std::string out_name;
                        std::string out_fullname;

                        std::string view_name = *itv_b;
                        std::string out_shortname = ito_b->first;
                        std::string atom_name = atom.getCompleteName();
                        vle_build_out_names(view_name, out_shortname, atom_name,
                                        out_name, out_fullname);

                        object["view_list"][view_name]["out_list"] += out_name;
                        object["names"]["out_names"] += out_fullname;
                    }
                }
            }
        }
    }
}

/********************************************************************
 * Simulates according to plan and restype
 *
 * Note :
 * - plan values : single, linear
 * - restype values : dataframe, matrix.
 ********************************************************************/

/*
 * Converts a view (value::Matrix) to json jdata where
 * lines represent time, columns represent observation ports.
 */
void vle_convert_view_matrix_to_json(const vle::value::Matrix& matrix,
                                     json& jdata){

    vle::value::Matrix::size_type i, j;
    vle::value::Matrix::size_type nbcols = matrix.columns();
    vle::value::Matrix::size_type nbrows = matrix.rows();

    for (i = 0; i < nbcols; ++i){
        json jcolumn;
        for (j = 0; j < nbrows; ++j){
            json jv;
            const std::unique_ptr<vle::value::Value>& v = matrix.get(i, j);
            if (v){
                vlevalue_to_json(*v, jv);
            }
            jcolumn[j] = jv;
        }
        jdata[i] = jcolumn;
    }
}

void vle_convert_matrix_to_json(const vle::value::Map& out, json& jmap){

    vle::value::Map::const_iterator itb = out.begin();
    vle::value::Map::const_iterator ite = out.end();
    for(; itb != ite; itb++){
        json jdata;
        vle_convert_view_matrix_to_json(itb->second->toMatrix(), jdata);
        jmap[itb->first.c_str()] = jdata;
    }
}

/*
 * Converts a view (value::Matrix) to json jdata where
 * lines represent time, columns represent observation ports.
 * Assumption : first line contains name of the columns.
 */
void vle_build_dataframe(const vle::value::Matrix& matrix, json& jdata){

    unsigned int nbcol = matrix.columns();
    unsigned int nbline = matrix.rows();

    for(unsigned int c = 0; c < nbcol; c++){ 
        json jcol;
        for (unsigned int i = 1; i < nbline; ++i) {
            json jv;
            const std::unique_ptr<vle::value::Value>& v = matrix.get(c,i);
            if (v) {
                vlevalue_to_json(*v, jv);
            }
            jcol[i-1] = jv;
        }
        jdata[matrix.getString(c,0).c_str()] = jcol;
    }
}

void vle_convert_dataframe_to_json(const vle::value::Map& out, json& jmap){

    vle::value::Map::const_iterator itb = out.begin();
    vle::value::Map::const_iterator ite = out.end();
    for(; itb != ite; itb++){
        json jdata;
        vle_build_dataframe(itb->second->toMatrix(), jdata);
        jmap[itb->first.c_str()] = jdata;
    }
}

void vle_convert_linear_matrix(const vle::value::Matrix& out, json& jarray){

    for (unsigned int j=0; j<out.columns();j++){
        json jline;
        for (unsigned int i=0; i<out.rows();i++){
            json jdata;
            if (out.get(j,i)){
                vle_convert_matrix_to_json(out.get(j,i)->toMap(), jdata);
            // else Matrix is empty
            }
            jline[i] = jdata;
        }
        jarray[j] = jline;
    }
}

void vle_convert_linear_dataframe(const vle::value::Matrix& out,
                                  json& jarray){

    for (unsigned int j=0; j<out.columns();j++){
        json jline;
        for (unsigned int i=0; i<out.rows();i++){
            json jdata;
            if (out.get(j,i)){
                vle_convert_dataframe_to_json(out.get(j,i)->toMap(), jdata);
            // else Matrix is empty
            }
            jline[i] = jdata;
        }
        jarray[j] = jline;
    }
}

value::Map* vle_run(vpz::Vpz* file, json& jres){ // value::Map : single
    assert(file);
    std::unique_ptr<vle::value::Map> res(nullptr);
    auto ctx = vle::utils::make_context();
    ctx.get()->set_log_priority(3); // VLE_LOG_ERROR

    try {
        manager::Error error;
        manager::Simulation sim(ctx, manager::LOG_NONE,
                                manager::SIMULATION_NONE,
                                std::chrono::milliseconds(0), NULL);
        // Configures output plugins for column names
        vpz::Outputs::iterator itb =
                      file->project().experiment().views().outputs().begin();
        vpz::Outputs::iterator ite =
                      file->project().experiment().views().outputs().end();
        for(;itb!=ite;itb++) {
            vpz::Output& output = itb->second;
            if((output.package() == "vle.output") &&
               (output.plugin() == "storage")){
                std::unique_ptr<value::Map> configOutput(new value::Map());
                configOutput->addString("header","top"); // dataframe
                output.setData(std::move(configOutput));
            }
        }
        res = sim.run(std::unique_ptr<vle::vpz::Vpz>(new vle::vpz::Vpz(*file)),
                &error);

        if (error.code != 0){
            std::cerr << "\nError in vle_run: " << error.message.c_str()
                      << "\n";
        }
        if (res == NULL){
            res.release(); // empty
            vle::value::Map* out = new vle::value::Map();
            res.reset(out); 
            std::cout << "\nWarning in vle_run: empty result\n";
        }
        vle_convert_dataframe_to_json(*res, jres);
        return res.get();
    } catch(const std::exception& e) {
        return NULL;
    }
    return NULL;
}

value::Map* vle_run_matrix(vpz::Vpz* file, json& jres){ // value::Map : single
    assert(file);
    std::unique_ptr<vle::value::Map> res(nullptr);
    auto ctx = vle::utils::make_context();
    ctx.get()->set_log_priority(3); // VLE_LOG_ERROR
    try {
        manager::Error error;
        manager::Simulation sim(ctx, manager::LOG_NONE,
                manager::SIMULATION_NONE, std::chrono::milliseconds(0),
                NULL);
        // Configures output plugins for column names
        vpz::Outputs::iterator itb =
                file->project().experiment().views().outputs().begin();
        vpz::Outputs::iterator ite =
                file->project().experiment().views().outputs().end();
        for(;itb!=ite;itb++) {
            vpz::Output& output = itb->second;
            if((output.package() == "vle.output") &&
               (output.plugin() == "storage")){
                std::unique_ptr<value::Map> configOutput(new value::Map());
                configOutput->addString("header","none"); // matrix
                output.setData(std::move(configOutput));
            }
        }
        res = sim.run(std::unique_ptr<vle::vpz::Vpz>(new vle::vpz::Vpz(*file)),
                      &error);

        if (error.code != 0){
            std::cerr << "\nError in vle_run_matrix: "
                      << error.message.c_str() << "\n";
        }
        if (res == NULL){
            res.release(); // empty
            vle::value::Map* out = new vle::value::Map();
            res.reset(out); 
            std::cout << "\nWarning in vle_run_matrix: empty result\n" ;
        }
        vle_convert_matrix_to_json(*res, jres);
        return res.get();
    } catch(const std::exception& e) {
        return NULL;
    }
    return NULL;
}

value::Matrix* vle_run_manager(vpz::Vpz* file, json& jres){
// value::Matrix : linear
    assert(file);
    std::unique_ptr<vle::value::Matrix> res(nullptr);
    auto ctx = vle::utils::make_context();
    ctx.get()->set_log_priority(3); // VLE_LOG_ERROR

    try {
        manager::Error error;
        manager::Manager sim(ctx, manager::LOG_NONE, manager::SIMULATION_NONE,
                             NULL);
        // Configures output plugins for column names
        vpz::Outputs::iterator itb =
                file->project().experiment().views().outputs().begin();
        vpz::Outputs::iterator ite =
                file->project().experiment().views().outputs().end();
        for(;itb!=ite;itb++) {
            vpz::Output& output = itb->second;
            if((output.package() == "vle.output") &&
               (output.plugin() == "storage")){
                std::unique_ptr<value::Map> configOutput(new value::Map());
                configOutput->addString("header","top"); // dataframe
                output.setData(std::move(configOutput));
            }
        }
        res = sim.run(std::unique_ptr<vle::vpz::Vpz>(new vle::vpz::Vpz(*file)),
                      1, 0, 1, &error);

        if (error.code != 0){
            std::cerr << "\nError in vle_run_manager: "
                      << error.message.c_str() << "\n";
        }
        if (res == NULL){
            res.release(); // empty
            vle::value::Matrix* out = new vle::value::Matrix();
            res.reset(out); 
            std::cout << "\nWarning in vle_run_manager: empty result\n";
        }
        json jarray;
        vle_convert_linear_dataframe(*res, jarray);

        // Added ('linear' case) : res dimension reduction to look like rvle
        for (auto& v : jarray.items()){
            jres += v.value().at(0);
        }
        return res.get();
    } catch(const std::exception& e) {
        return NULL;
    }
    return NULL;
}

value::Matrix* vle_run_manager_matrix(vpz::Vpz* file, json& jres){
// value::Matrix : linear

    assert(file);
    std::unique_ptr<vle::value::Matrix> res(nullptr);
    auto ctx = vle::utils::make_context();
    ctx.get()->set_log_priority(3); // VLE_LOG_ERROR
    try {
        manager::Error error;
        manager::Manager sim(ctx, manager::LOG_NONE, manager::SIMULATION_NONE,
                             NULL);
        //configure output plugins for column names
        vpz::Outputs::iterator itb =
                file->project().experiment().views().outputs().begin();
        vpz::Outputs::iterator ite =
                file->project().experiment().views().outputs().end();
        for(;itb!=ite;itb++) {
            vpz::Output& output = itb->second;
            if((output.package() == "vle.output") &&
               (output.plugin() == "storage")){
                std::unique_ptr<value::Map> configOutput(new value::Map());
                configOutput->addString("header","none"); // matrix
                output.setData(std::move(configOutput));
            }
        }
        res = sim.run(std::unique_ptr<vle::vpz::Vpz>(new vle::vpz::Vpz(*file)),
                      1, 0, 1, &error);

        if (error.code != 0){
            std::cerr << "\nError in vle_manager: "
                      << error.message.c_str() << "\n";
        }
        if (res == NULL){
            res.release(); // empty
            vle::value::Matrix* out = new vle::value::Matrix();
            res.reset(out); 
            std::cout << "\nWarning in vle_run_manager_matrix: empty result\n";
        }
        json jarray;
        vle_convert_linear_matrix(*res, jarray);

        // Added ('linear' case) : res dimension reduction to look like rvle
        for (auto& v : jarray.items()){
            jres += v.value().at(0);
        }
        return res.get();
    } catch(const std::exception& e){
        return NULL;
    }
    return NULL;
}

/********************************************************************
 * Manages error cases
 *
 * If  execution is OK, then creation of a "OK" file.
 *
 ********************************************************************/

/*
 * Creates "OK" file
 */
void create_ok_file(void){
    std::ofstream f;
    f.open("OK");
    f.close();
}

/********************************************************************/

