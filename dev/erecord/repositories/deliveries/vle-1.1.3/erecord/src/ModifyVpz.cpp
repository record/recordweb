/*
 * @file erecord/ModifyVpz.cpp
 *
 * This file is part of erecord project
 *
 * Copyright (c) 2019-2024 INRA http://www.inra.fr
 *
 */

#include <iostream>
#include <fstream>

#include <vle/devs/Dynamics.hpp>
#include <vle/manager/Simulation.hpp>
#include <vle/utils/Package.hpp>

#include "erecord_vle.hpp"
#include "erecord_json.hpp"

class ModifyVpz : public vle::devs::Dynamics {

public:

    ModifyVpz(const vle::devs::DynamicsInit& mdl,
              const vle::devs::InitEventList& lst)
            : vle::devs::Dynamics(mdl, lst){}

    virtual ~ModifyVpz(){}

    /*
     * Modifies conditions and parameters according to jconditions information
     *
     * Note : the information about parameters, given into jconditions, is
     * given as cname[pname] or cname.pname.
     * 
     * Note : if a cname[pname] value and at the same time a cname.pname value
     * were given into jconditions, there is no guarantee about which of both
     * values would be taken into account.
     */
    void modify_conditions(vpz::Vpz* file, json& jconditions){

        vpz::Experiment exp = file->project().experiment();

        for (vpz::Conditions::const_iterator it = exp.conditions().begin();
                                          it != exp.conditions().end(); ++it){

            std::string cname = it->first; // as condition.name();
            vpz::Condition condition = it->second;

            // looks for cname into jconditions
            if (jconditions.find(cname) != jconditions.end()){

                std::list < std::string > lst;
                condition.portnames(lst);
                std::list < std::string >::const_iterator itp;
                for (itp = lst.begin(); itp != lst.end(); ++itp){

                    std::string pname = itp->c_str();

                    // looks for pname into jconditions[cname]
                    auto it_pname = jconditions[cname].find(pname);
                    if (it_pname != jconditions[cname].end()){
                        json jvalues = *it_pname;
                        vle_parameter_set_values(file, cname, pname, &jvalues);
                    // else : pname not found into jconditions
                    }
                }
            // else : cname not found into jconditions
            }
        }

        /* Case of parameter given as cname.pname */
        for (vpz::Conditions::const_iterator it = exp.conditions().begin();
                                          it != exp.conditions().end(); ++it){
        
            std::string cname = it->first; // as condition.name();
            vpz::Condition condition = it->second;
        
            std::list < std::string > lst;
            condition.portnames(lst);
            std::list < std::string >::const_iterator itp;
            for (itp = lst.begin(); itp != lst.end(); ++itp){
        
                std::string pname = itp->c_str();
                std::string cpname = cname + "." + pname;
        
                // looks for cpname into jconditions
                auto it_cpname = jconditions.find(cpname);
                if (it_cpname != jconditions.end()){
                    json jvalues = *it_cpname;
                    vle_parameter_set_values(file, cname, pname, &jvalues);
                // else : cpname not found into jconditions
                }
            }
        }
    }

    /*
     * Modifies views and output datas according to jviews information
     * and by taking into account initial_state_object.
     *
     * The information given into jviews is, for the moment, a list
     * storaged_list :
     * - to define which view and output data is to be activated.
     * - containing values : "all", some vname, some vname.oname.
     *
     * Note : if this list is absent then no plugin of any view is 
     * modified.
     *
     * Note : A view is :
     * -    activated by setting its output plugin in "storage" value.
     * - desactivated by setting its output plugin in "dummy" value.
     *
     * Note : the activation/desactivation is also applied at level of each
     * output data (by remaining attached/detaching views to observable ports).
     *
     * Note : storaged_list corresponds with outselect in erecord
     */
    void modify_views(vpz::Vpz* file,
                      json& initial_state_object, json& jviews){

        json& jview_names = initial_state_object["names"]["view_names"];
        json& jout_names = initial_state_object["names"]["out_names"];

        if (json_found(&jviews, "storaged_list")){
            json jstoraged_list = jviews["storaged_list"];

            bool all_is_in_storaged;
            all_is_in_storaged = json_is_value_into_json_list("all",
                                                              jstoraged_list);
            if (all_is_in_storaged){
            
                for (auto& v : jview_names.items()){
                    std::string vname = v.value();
                    std::string plugin = "storage";
                    vle_view_set_output_plugin(file, vname, plugin);
                    // the out of vname (vname.oname) are not modified
                }

            } else {

                bool one_view_or_out_is_in_storaged;
                one_view_or_out_is_in_storaged =
                          json_is_one_value_of_first_list_into_another_one(
                                     jstoraged_list, jview_names, jout_names);
                if (one_view_or_out_is_in_storaged){
            
                    for (auto& v : jview_names.items()){
                        std::string vname = v.value();

                        if (json_is_value_into_json_list(vname,
                                                         jstoraged_list)){
                            std::string plugin = "storage";
                            vle_view_set_output_plugin(file, vname, plugin);
                            // out of view not modified

                        } else { 

                            bool one_out_of_view_is_in_storaged;
                            one_out_of_view_is_in_storaged =
                               json_is_one_out_of_view_into_json_list(
                                  vname, initial_state_object, jstoraged_list);

                            if (one_out_of_view_is_in_storaged){
                                std::string plugin = "storage";
                                vle_view_set_output_plugin(file, vname,
                                                           plugin);

                                for (auto& v : initial_state_object["view_list"][vname]["out_list"].items()){
                                    std::string oname = v.value();
                                    std::string out_fullname = vname + "."
                                                                     + oname;
                                    if (!json_is_value_into_json_list(
                                                out_fullname, jstoraged_list)){
                                        vle_detach_view_from_observableport(
                                                           file, vname, oname);
                                    }
                                }

                            } else {
                                std::string plugin = "dummy";
                                vle_view_set_output_plugin(file, vname,
                                                           plugin);
                            }
                        }
                    }
                //} else { // do nothing
                }
            }
        }
    }

    /*
     * Modifies and saves the vpz file according to the information read into
     * "modify_input.json" (begin, conditions, observables...)
     *
     * Note : The command parameters are read into "modify_input.json"
     * (pkgname, vpzname, duration, parameters...), that has been previously
     * built and copied into erecord/data (for example by erecord).
     *
     * Illustration of "modify_input.json" content :
     *
     *   {
     *     ...,
     *
     *     "begin":value,
     *     "duration":value,
     *
     *     "conditions" :
     *        {
     *        "cnameA" : { "pname_a":values1, "pname_b":values2, ...},
     *        "cnameB" : { "pnames_c":values3, "pname_d":values4, ...},
     *        ...,
     *        "cnameC.pname_e":values5,
     *        "cnameC.pname_f":values6,
     *        "cnameD.pname_g":values7,
     *        ...,
     *        },
     *
     *     "views" :
     *        {
     *        "storaged_list" :
     *           [
     *           "all",
     *           "vnameA", "vnameB", ...,
     *           "vnameC.oname_a", "vnameC.oname_b", "vnameD.oname_c", ...,
     *           ...,
     *           ],
     *        },
     *     ...,
     *   }
     *
     * Note : the simulation of the modified vpz file is then ready to be run
     * later on (for example by erecord).
     */
    vle::devs::Time init(const vle::devs::Time& /* time */){

        bool cr_ok = true; // default

        // jinput
        std::stringstream ss;
        vle::utils::Package erecord_pack("erecord");
        std::ifstream inputfile(erecord_pack.getDataFile("modify_input.json",
                                                      vle::utils::PKG_BINARY));
        ss << inputfile.rdbuf();
        inputfile.close();
        json jinput = json::parse(ss);

        // pkgname
        std::string pkgname; bool pkgname_found;
        json_get_pkgname(&pkgname, &pkgname_found, &jinput);

        // vpzname
        std::string vpzname; bool vpzname_found;
        json_get_vpzname(&vpzname, &vpzname_found, &jinput);

        // can be added : control that pkgname and vpzname exist

        if (pkgname_found && vpzname_found){

            // Vpz
            vle::utils::Package pack(pkgname);
            vpz::Vpz *file = new vpz::Vpz(
                            pack.getExpFile(vpzname, vle::utils::PKG_BINARY));

            // json initial state (values) of Vpz input information
            json initial_state_object;
            vle_read_vpz_input(file, initial_state_object);
            //=> usable to verify cond and par :
            //   initial_state_object["names"]["cond_names"]
            //   initial_state_object["names"]["par_names"]
            //=> usED to verify view et out :
            //   initial_state_object["names"]["view_names"]
            //   initial_state_object["names"]["out_names"]

            // continues to read jinput and modifies vpz file according to it
            // (and according to types read into vpz file)

            /* begin */
            double begin; bool begin_found;
            json_get_begin(&begin, &begin_found, &jinput);
            if (begin_found){
                vle_set_begin(file, &begin);
            // else : begin not found into jinput
            }

            /* duration */
            double duration; bool duration_found;
            json_get_duration(&duration, &duration_found, &jinput);
            if (duration_found){
                vle_set_duration(file, &duration);
            // else : duration not found into jinput
            }

            // conditions
            if (json_found(&jinput, "conditions")){
                json jconditions = jinput["conditions"];
                modify_conditions(file, jconditions);
            }

            // views
            if (json_found(&jinput, "views")){
                json jviews = jinput["views"];
                modify_views(file, initial_state_object, jviews);
            }

            file->write(); // saves, done once at the end

        } else {

            cr_ok = false;

            if (!pkgname_found){
                std::cerr << "-- " << pkgname;
                std::cerr << " not found into modify_input.json --" << "\n";
            }
            if (!vpzname_found){
                std::cerr << "-- " << vpzname;
                std::cerr << " not found into modify_input.json --" << "\n";
            }
        }

        if (cr_ok){ create_ok_file(); }
        return vle::devs::infinity;
    }

};

DECLARE_DYNAMICS(ModifyVpz)

