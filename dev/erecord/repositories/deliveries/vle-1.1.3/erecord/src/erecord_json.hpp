/*
 * @file erecord/erecord_json.hpp
 *
 * This file is part of erecord project
 *
 * Copyright (c) 2019-2024 INRA http://www.inra.fr
 *
 */

#include "json.hpp"
using json = nlohmann::json;

/*****************************************************************************
 *
 * json
 *
 * Pure json
 *
 * Note :
 * json & vle : see erecord_vle.hpp
 *
 *****************************************************************************/

bool json_found(json *object, std::string key){
    if (object->find(key) != object->end()){
        return true;
    } else {
        return false;
    }
}

void json_get_double(double *double_value, bool *found,
                     json *object, std::string key){
    *found = false; // default
    auto it_double = object->find(key);
    if (it_double != object->end()){
        *double_value = *it_double;
        *found = true;
    };
}

void json_get_string(std::string *string_value, bool *found,
                     json *object, std::string key){
    *found = false; // default
    auto it_string = object->find(key);
    if (it_string != object->end()){
        *string_value = *it_string;
        *found = true;
    };
}

void json_get_pkgname(std::string *pkgname, bool *found, json *object){
    json_get_string(pkgname, found, object, "pkgname");
}

void json_get_vpzname(std::string *vpzname, bool *found, json *object){
    json_get_string(vpzname, found, object, "vpzname");
}

void json_get_begin(double *begin, bool *found, json *object){
    json_get_double(begin, found, object, "begin");
}

void json_get_duration(double *duration, bool *found, json *object){
    json_get_double(duration, found, object, "duration");
}

void json_get_plan_or_default(std::string *plan, json *object){

    std::string found_value; bool found;
    json_get_string(&found_value, &found, object, "plan");
    *plan = "single"; // default value
    if (found){
        if ((found_value == "linear") ||
            (found_value == "single")){ // available values
            *plan = found_value;
        }
    }
}

void json_get_restype_or_default(std::string *restype, json *object){

    std::string found_value; bool found;
    json_get_string(&found_value, &found, object, "restype");
    *restype = "dataframe"; // default value
    if (found){
        if ((found_value == "matrix") ||
            (found_value == "dataframe")){ // available values
            *restype = found_value;
        }
    }
}

bool json_is_value_into_json_list(std::string value, json& jlist){

    for (auto& v : jlist.items()){
        if (v.value() == value){
            return true;
        }
    }
    return false;
}

bool json_is_one_value_of_first_list_into_another_one(json& jfirst_list,
                                   json& jsecond_list, json& jthird_list){

    for (auto& v : jfirst_list.items()){

        if (json_is_value_into_json_list(v.value(), jsecond_list)){
            return true;
        }
        if (json_is_value_into_json_list(v.value(), jthird_list)){
            return true;
        }
    }
    return false;
}

bool json_is_one_out_of_view_into_json_list(std::string vname, json& jobject,
                                            json& jlist){

    for (auto& v : jobject["view_list"][vname]["out_list"].items()){
        std::string oname = v.value();
        std::string out_fullname = vname + "." + oname;
        if (json_is_value_into_json_list(out_fullname, jlist)){
            return true;
        }
    }
    return false;
}




/*****************************************************************************/

