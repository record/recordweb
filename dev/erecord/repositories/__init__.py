"""repositories

*This package is part of erecord - web services for vle models*

:copyright: Copyright (C) 2019-2024 INRA http://www.inra.fr.
:license: GPLv3, see LICENSE file for more details.
:authors: see AUTHORS file.

Repositories of the erecord project

The repositories directory is dedicated to the vle repositories.  

The models repositories are delivered to erecord as containers files, stored
under the repositories directory according to the following conventions :

    The 'repname' models repository container file is installed into :
    vle-X/repname/rep.simg

Note : vle-X and the vle version that is contained/defined into the models 
repository container file must be relevant.

The repositories directory also contains into deliveries some required code 
(such as the erecord vle package).

"""

