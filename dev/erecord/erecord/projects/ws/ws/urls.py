"""erecord.projects.ws.ws.urls

ws URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see: \
    https://docs.djangoproject.com/en/2.2/topics/http/urls/

Examples:

Function views

    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')

Class-based views

    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')

Including another URLconf

    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import include, path

from django.views.generic import TemplateView

from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_jwt.views import verify_jwt_token

from erecord_cmn import views as cmn_views
from erecord_db.views import menus as db_views_menus


# admin
urlpatterns = [
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('admin/', admin.site.urls),
]

# JWT Authentication, urls kept whereas replaced by acs urls
# (see acs/jwt/obtain/, acs/jwt/verify/, acs/login)
urlpatterns += [
    path('api-token-auth/', obtain_jwt_token, name="api-token-auth"),
   #path('api-token-refresh/', refresh_jwt_token),
    path('api-token-verify/', verify_jwt_token, name="api-token-verify"),
]
urlpatterns += [
    path('api/login', obtain_jwt_token),
]

# index page (root)
urlpatterns += [
    path('', TemplateView.as_view(template_name='erecord_cmn/index.html'),
         name="erecord_cmn-index-page"),
]

# home page (idem erecord_db-menu-main-page-list,detail)
urlpatterns += [
    path('home/', db_views_menus.MenuMainPageList.as_view(),
         name='erecord-home-page'),
    path('menu/', db_views_menus.MenuMainPageList.as_view()),
    path('menu/<int:pk>/', db_views_menus.MenuMainPageDetail.as_view()),
]

# online documentation page
urlpatterns += [
    path('docs/', cmn_views.onlinedoc_page, name="erecord_cmn-onlinedoc-page"),
]

urlpatterns += [

    # database (main)
    path('db/', include('erecord_db.urls')),

    # vpz manipulation
    path('vpz/', include('erecord_vpz.urls')),

    # slm downloadable and uploaded files management, and media
    path('slm/', include('erecord_slm.urls')),
#   path('media/(?P<path>.*)$', django_views_static.serve,
#        {'document_root':settings.MEDIA_ROOT}, name="media"),

    # access (public, limited)
    path('acs/', include('erecord_acs.urls')),
]

