"""erecord.projects

*This package is part of erecord - web services for vle models*

:copyright: Copyright (C) 2019-2024 INRA http://www.inra.fr.
:license: GPLv3, see LICENSE file for more details.
:authors: see AUTHORS file.

django projects of the erecord package

See also python code in django applications (erecord.apps)
 
"""

