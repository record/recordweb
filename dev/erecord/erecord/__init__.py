"""erecord package

*This package is part of erecord - web services for vle models*

:copyright: Copyright (C) 2019-2024 INRA http://www.inra.fr.
:license: GPLv3, see LICENSE file for more details.
:authors: see AUTHORS file.

The erecord package is part of the erecord project providing some web services
for models developed with Vle (several Vle versions), such as the Record
platform ones.

The erecord package contains the production (source code, tests, etc).

The code is written in python language with the django web framework.

Content hierarchy : docs, install, projects, apps...

"""

