"""erecord_cmn.forms

Forms common to erecord applications

"""

from django import forms

# Some forms common to erecord applications

class ReadonlyForm(forms.ModelForm):
    """Form with all fields readonly and in textarea"""
    def __init__(self, *args, **kwargs):
        super(ReadonlyForm, self).__init__(*args, **kwargs)
        # read-only fields, text-area fields
        for (n,f) in self.fields.items() :
            f.widget=forms.Textarea(attrs={'rows':1, 'cols':60})
            f.widget.attrs['readonly'] = True

class TextAreasForm(forms.ModelForm):
    """Form with all fields in textarea"""

    def __init__(self, *args, **kwargs):
        super(TextAreasForm, self).__init__(*args, **kwargs)
        # text-area fields
        for (n,f) in self.fields.items() :
            f.widget=forms.Textarea(attrs={'rows':1, 'cols':100})

    def make_readonly(self, name_field):
        f = self.fields[name_field]
        f.widget.attrs['readonly'] = True

class ErrorForm(forms.Form):
    """Form dedicated to an error"""

    status = forms.IntegerField(label="status" )
    detail = forms.CharField(label="detail", max_length=100000)

    def __init__(self, *args, **kwargs):
        super(ErrorForm, self).__init__(*args, **kwargs)

        f = self.fields['detail']
        f.widget=forms.Textarea(attrs={'rows':2, 'cols':140})

        for (n,f) in self.fields.items() :
            f.widget.attrs['readonly'] = True

