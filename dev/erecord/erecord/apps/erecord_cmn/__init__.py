"""erecord_cmn

*This package is part of erecord - web services for vle models*

:copyright: Copyright (C) 2019-2024 INRA http://www.inra.fr.
:license: GPLv3, see LICENSE file for more details.
:authors: see AUTHORS file.

Common application
 
The erecord_cmn common application contains resources for the other
applications.

"""
