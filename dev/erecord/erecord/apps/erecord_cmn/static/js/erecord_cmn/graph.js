
var colors = ["#FF0000", "#008000", "#0000FF", "#B22222", "#808000", "#00FFFF",
              "#DC143C", "#66CDAA", "#1E90FF", "#CD5C5C", "#006400", "#800080",
              "#40E0D0", "#FF1493", "#32CD32", "#4682B4", "#C71585", "#00FF7F",
              "#9400D3", "#FF4500", "#90EE90", "#BA55D3"];
var icolor = 0; 

function draw_list(size, xy)
{ // draws points of xy in canvas 'graph'

  var width = size; var height = size;
  var sc=20; var delta0 = 20;
  var canvas = document.getElementById("graph");
  if (canvas!=null){
      canvas.width=width; canvas.height=height;
      var ctx = canvas.getContext("2d");

      var xmin; var xmax; var ymin; var ymax; var i = 0;
      xmin = Math.min.apply(null, xy[i]['X_values']);
      xmax = Math.max.apply(null, xy[i]['X_values']);
      ymin = Math.min.apply(null, xy[i]['Y_values']);
      ymax = Math.max.apply(null, xy[i]['Y_values']);
      for (i=0; i<xy.length; i++){
          one_xmin = Math.min.apply(null, xy[i]['X_values']);
          one_xmax = Math.max.apply(null, xy[i]['X_values']);
          one_ymin = Math.min.apply(null, xy[i]['Y_values']);
          one_ymax = Math.max.apply(null, xy[i]['Y_values']);
          xmin = Math.min(xmin, one_xmin);
          xmax = Math.max(xmax, one_xmax);
          ymin = Math.min(ymin, one_ymin);
          ymax = Math.max(ymax, one_ymax);
      }

      //alert(xmin);alert(xmax); alert(ymin);alert(ymax);
      //alert(xmax-xmin); alert(ymax-ymin);

      var deltax = (xmax-xmin)/delta0;
      if (deltax > 0){
          xmin = xmin - deltax;
          xmax = xmax + deltax;
          Kx = width / (xmax-xmin);
      } else if ( deltax == 0){
          xmin = xmin - 1;
          xmax = xmax + 1;
          Kx = width / (xmax-xmin);
      } else {
          alert("Error xmin(="+xmin+") >= xmax(="+xmax+")");
      }

      var deltay = (ymax-ymin)/delta0;
      if (deltay > 0){
          ymin = ymin - deltay;
          ymax = ymax + deltay;
          Ky = height / (ymax-ymin);
      } else if ( deltay == 0){
          ymin = ymin - 1;
          ymax = ymax + 1;
          Ky = height / (ymax-ymin);
      } else {
          alert("Error ymin(="+ymin+") >= ymax(="+ymax+")");
      }

      //alert(Kx);alert(Ky);

      // lines
      ctx.beginPath();
      ctx.strokeStyle = "#666";
      ctx.lineWidth=0.5;
      for(var i=0; i<=height/sc; i++) {
          ctx.moveTo(0, height-sc*i);
          ctx.lineTo(width, height-sc*i);
      }
      for(var i=0; i<=width/sc; i++) {
          ctx.moveTo(sc*i,height-0);
          ctx.lineTo(sc*i, height-height);
      }
      ctx.stroke();

      // outputs
      icolor = 0;
      for (i=0; i<xy.length; i++){
          ctx.beginPath();
          ctx.lineWidth=1.0;
          ctx.strokeStyle = colors[icolor++];
          // xy[i]['X_name'], xy[i]['Y_name']
          x = xy[i]['X_values'];
          y = xy[i]['Y_values'];
          var j = 0;
          var vx = Kx * ( x[j] - xmin);
          var vy = Ky * ( ymax - y[j]);
          ctx.moveTo(vx, vy)
          var length = Math.min(x.length, y.length);
          if (x.length != y.length){
              var txt = "Warning the result ";
              txt += xy[i]['Y_name']+"("+xy[i]['X_name']+")";
              txt += " has been truncated";
              alert(txt);
          }
          for(j=0; j<length; j++) {
              vx = Kx * ( x[j] - xmin);
              vy = Ky * ( ymax - y[j]);
              ctx.lineTo(vx, vy)
          ctx.stroke();
          }
      }
      //ctx.closePath();
  } else {
    alert("Error canvas 'graph' not found");
  }
}

