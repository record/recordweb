
function toggle_visibility(id)
{ // to show/hide element id

  var e = document.getElementById(id);
  if (e!=null){
      if (e.style.display == 'block'){ e.style.display = 'none';
      } else { e.style.display = 'block'; }
  }
}

function clear_id_content(id)
{ // The id element content is emptied
  var e = document.getElementById(id);
  if (e!=null){
    while (e.firstChild) { e.removeChild(e.firstChild); }
  }
}
function clear_element_content(e)
{ // The element content is emptied
  while (e.firstChild) { e.removeChild(e.firstChild); }
}

function add_option(name, element_select)
{ // add name option to element_select options
  var option = document.createElement('option');
  option.value = name;
  option.text = name;
  option.selected = 'selected';
  element_select.add(option);
}

