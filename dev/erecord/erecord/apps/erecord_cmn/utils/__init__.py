"""erecord_cmn.utils

*This package is part of erecord - web services for vle models*

:copyright: Copyright (C) 2019-2024 INRA http://www.inra.fr.
:license: GPLv3, see LICENSE file for more details.
:authors: see AUTHORS file.

Python code of the erecord package

Python code, some of it is independant of django applications code

"""

