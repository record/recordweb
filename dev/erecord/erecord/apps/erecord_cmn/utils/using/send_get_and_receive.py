"""erecord_cmn.utils.using.send_get_and_receive

Methods that may be used by a user calling the erecord web services from python

"""

import urllib.parse as urllib_local    # only in python 3   version case
#from __future__ import print_function   # only in python 2.7 version case
#import urllib as urllib_local           # only in python 2.7 version case

import pycurl
import io
import json

def send_get_and_receive(url, id=None, options=None):
    """Send GET request and return response datas"""
#
    # full_url composed by : url {id} /? options
    full_url = url
    if id is not None :
        full_url = full_url + "%i/" % id
    if options is not None :
        full_url = full_url + "?%s" % urllib_local.urlencode(options)
    #print("request GET ", full_url)
    buffer = io.BytesIO()
    c = pycurl.Curl()
    c.setopt(c.CUSTOMREQUEST, 'GET')
    c.setopt(c.URL, full_url)
    #c.setopt(c.FOLLOWLOCATION, True) # to follow redirect
    c.setopt(c.WRITEFUNCTION, buffer.write)
    c.perform()
    buffer_str = buffer.getvalue()
    buffer.close()
    buffer_str = buffer_str.decode("utf8")
    responsedata = json.loads(buffer_str)
    #responsedata
    #responsedata.keys()
    return responsedata
#

