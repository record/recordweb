"""erecord_cmn.utils.using.obtain_jwt

Methods that may be used by a user calling the erecord web services from python

"""

from erecord_cmn.utils.using.send_post_and_receive import send_post_and_receive

def obtain_jwt(username, password,
               base_url="http://erecord.toulouse.inra.fr:8000/"):
    """Returns a JWT value for username"""

    token_value = "unknown" # default

    url = base_url + "acs/jwt/obtain/"
    inputdata = {"username":username, "password":password}
    responsedata = send_post_and_receive(url=url, inputdata=inputdata)

    if "token" in responsedata.keys() :
        token_value = responsedata['token']

    return token_value

