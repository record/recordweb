"""erecord_cmn.utils.using.content_simulation_results_tree

Methods that may be used by a user calling the erecord web services from python

"""

#from __future__ import print_function # python 2.7 version case

def content_simulation_results_tree(res, plan, restype):
    """Content of simulation results (res) in 'tree' style of presentation,
    according to plan values ('single', 'linear') and
    restype values ('dataframe' or 'matrix')
    """
#
    res = json.loads(res)
    print("plan, restype :", plan, restype)
    print("res :", res)
    print("\nDetailing the results :")
    if restype=='dataframe' and plan=='single' :
        print("(view name, output data name, value)")
        for viewname,outputs in res.items() :
            for outputname,val in outputs.items() :
                print("- ", viewname, outputname, val)
    elif restype=='dataframe' and plan=='linear' :
        for (a,res_a) in enumerate(res) :
            print("*** simulation number ", a, ":")
            print("(view name, output data name, value)")
            for viewname,outputs in res_a.items() :
                for outputname,val in outputs.items() :
                    print("- ", viewname, outputname, val)
    elif restype=='matrix' and plan=='single' :
        print("(view name, value)")
        for viewname,v in res.items() :
            print("- ", viewname, v)
    elif restype=='matrix' and plan=='linear' :
        for (a,res_a) in enumerate(res) :
            print("*** simulation number ", a, ":")
            print("(view name, value)")
            for viewname,v in res_a.items() :
                print("- ", viewname, v)
    else : # error (unexpected)
        pass
#

