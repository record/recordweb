"""erecord_cmn.utils.using.content_rep_pkg_vpz_tree

Methods that may be used by a user calling the erecord web services from python

"""

#from __future__ import print_function # python 2.7 version case

def content_rep_tree(vlerep):
    """Content of the models repository (vlerep) in 'tree' style of
    presentation
    """
#
    #vlerep.keys()
    vlerep_id= vlerep['id']
    vlerep_name= vlerep['name']
    vlerep_verbose_name= vlerep['verbose_name']
    vlerep_text= vlerep['text']
    vlerep_path = vlerep['path']
    print("\nModels repository (id, name, verbose_name, text) : ", vlerep_id, vlerep_name, vlerep_verbose_name, vlerep_text)
    print("List of its models (id, name, verbose_name, text) :")
    vlerep_vlepkg_list = vlerep['vlepkg_list']
    for vlepkg in vlerep_vlepkg_list :
        # vlepkg.keys()
        vlepkg_id = vlepkg['id']
        vlepkg_name = vlepkg['name']
        vlepkg_verbose_name = vlepkg['verbose_name']
        vlepkg_text = vlepkg['text']
        vlepkg_vlerep = vlepkg['vlerep']
        print("- ", vlepkg_id, vlepkg_name, vlepkg_verbose_name, vlepkg_text)

def content_pkg_tree(vlepkg):
    """Content of the model (vlepkg) in 'tree' style of presentation"""
#
    #vlepkg.keys()
    vlepkg_id = vlepkg['id']
    vlepkg_name = vlepkg['name']
    vlepkg_verbose_name = vlepkg['verbose_name']
    vlepkg_text = vlepkg['text']
    vlepkg_vlerep = vlepkg['vlerep']
    print("\nModel (id, name, verbose_name, text, vlerep) : ", vlepkg_id, vlepkg_name, vlepkg_verbose_name, vlepkg_text, vlepkg_vlerep)
    print("List of its simulators (id, name, verbose_name, text, vlepkg) :")
    vlepkg_vlevpz_list = vlepkg['vlevpz_list']
    for vlevpz in vlepkg_vlevpz_list :
        # vlevpz.keys()
        vlevpz_id = vlevpz['id']
        vlevpz_name = vlevpz['name']
        vlevpz_verbose_name = vlevpz['verbose_name']
        vlevpz_text = vlevpz['text']
        vlevpz_vlepkg = vlevpz['vlepkg']
        print("- ", vlevpz_id, vlevpz_name, vlevpz_verbose_name, vlevpz_text, vlevpz_vlepkg)

def content_vpz_tree(vlevpz):
    """Content of the simulator (vlevpz) in 'tree' style of presentation"""
#
    #vlevpz.keys()
    vlevpz_id = vlevpz['id']
    vlevpz_name = vlevpz['name']
    vlevpz_verbose_name = vlevpz['verbose_name']
    vlevpz_text = vlevpz['text']
    vlevpz_vlepkg = vlevpz['vlepkg']
    print("\nSimulator (id, name, verbose_name, text, vlepkg) : ", vlevpz_id, vlevpz_name, vlevpz_verbose_name, vlevpz_text, vlevpz_vlepkg)
#

