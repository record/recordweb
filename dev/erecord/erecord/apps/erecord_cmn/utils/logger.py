"""erecord_cmn.utils.logger """

import logging
import os

# default values are IN_PRODUCTION False and LOGGING_FILE None
IN_PRODUCTION = False
LOGGING_FILE = None
try:
    from erecord_cmn.configs.config import IN_PRODUCTION
except Exception:
    pass
try:
    from erecord_cmn.configs.config import LOGGING_FILE
except Exception:
    pass
if (IN_PRODUCTION is not True) and (IN_PRODUCTION is not False) :
    IN_PRODUCTION = False
if LOGGING_FILE is not None :
    if not os.path.isdir(os.path.dirname(LOGGING_FILE)):
        LOGGING_FILE = None 


def get_logger(name):
    """Creates a logger

    Creates a logger with LOGGING_FILE file destination (if not None) and
    with console destination (if IN_PRODUCTION False).
    """

    # create logger
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter('%(asctime)s , %(name)s , \
                                   %(levelname)s >> %(message)s')

    if not IN_PRODUCTION :

        # create console handler and set level to debug
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)

        # add formatter to ch
        ch.setFormatter(formatter)

        # add ch to logger
        logger.addHandler(ch)

    if LOGGING_FILE is not None :

        # create file handler 
        fh = logging.FileHandler(filename=LOGGING_FILE)
        if not IN_PRODUCTION :
            level = logging.DEBUG
        else : # default, IN_PRODUCTION
            level = logging.INFO # .WARNING ?
        fh.setLevel(level)

        # add formatter to fh
        fh.setFormatter(formatter)

        # add fh to logger
        logger.addHandler(fh)

    return logger

import traceback
def report_error() :
    """ Reports an error """
    LOGGER = get_logger(__name__)
    LOGGER.error(u"Error report :\n %s ", traceback.format_exc())

