"""erecord_cmn.utils.container.location

Methods for the models repositories containers location/identification

Models repository containers are stored under the erecord repositories folder.

For more (conventions...) : see __init__.py file of erecord repositories folder

"""

# Prerequisite
# apps_home = "/opt/erecord/erecord/apps"
# if apps_home not in sys.path :
#     sys.path.insert(0, apps_home)

import os

from erecord_cmn.configs.config import REPOSITORIES_HOME

from erecord_cmn.utils.dir_and_file import is_a_file


def get_vlerep_path(version_name, rep_name):
    return os.path.join(REPOSITORIES_HOME, version_name, rep_name)

def get_vlecontainer_path(path):
    """Returns the full path name of a models repository container file

    path is the folder where the container file is located

    Convention : the container of a models repository is named rep.simg
    """

    return os.path.join(path, "rep.simg")

def contains_a_vlecontainer(path):
    """Controls if there is a models repository container file under path """

    return is_a_file(get_vlecontainer_path(path=path))

def is_structured_as_vle_version_name(version_name):
    """Controls if version_name is structured as vle-V.rev

    (for example vle-1.1.3, vle-2.0.0)
    """

    version_name_structure_ok = False # default
    a = version_name.split("-") # ["vle", V.rev]
    if len(a) > 1 :
        if a[0] == "vle" :
            b = a[1]
            c = b.split(".") # [V, rev..]
            if len(c) > 1 :
                version_name_structure_ok = True
    return version_name_structure_ok


# Concerning vle_actions_vpz (API to erecord app of container)

def get_vledatafolder_localpath() :
    """Returns the local path being the data folder for vle_actions_vpz """
    return "data"


def get_vlesavedvpzfile_localpath(vpzname) :
    """Returns the local path where vle_actions_vpz has put the vpz file """
    return os.path.join('exp', "__updated__"+vpzname)

