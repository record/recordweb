"""erecord_cmn.utils.container

*This package is part of erecord - web services for vle models*

:copyright: Copyright (C) 2019-2024 INRA http://www.inra.fr.
:license: GPLv3, see LICENSE file for more details.
:authors: see AUTHORS file.

Python code as API to models repositories containers

Some of it is independant of django applications code

Python code that may be used by a user calling the erecord web services
from python

"""

