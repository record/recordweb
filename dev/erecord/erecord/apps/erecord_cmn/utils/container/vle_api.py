"""erecord_cmn.utils.container.vle_api

Methods as API to models repositories containers -- vle.def part

A 'repname' models repository container 'rep.simg' has been built from the
succession of vle.def, erecord.def and rep.def recipe files application,
with some apps at each level : 

- vle.def : context, pkg_list, vpz_list, ...
- erecord.def : erecord

vle.def is dedicated to generalities about vle (vle version number, packages
names...).

"""

import os
import sys

# Prerequisite
# apps_home = "/opt/erecord/erecord/apps"
# if apps_home not in sys.path :
#     sys.path.insert(0, apps_home)

from erecord_cmn.utils.process import call_subprocess

#------------------------------------------------------------------------------
# General, common

def run_cmd(runvle_path, cmd):
    """Runs cmd under runvle_path

    Inputs :

    - runvle_path : folder from where the command is called
    - cmd         : command (text)

    Outputs :

    - retcode : returned by call_subprocess
    - msg_stdout : stdout message
    - msg_stderr : stderr message

    Note : some files are produced under runvle_path (cf cmd, stderr, stdout)
    """

    full_cmd = "cd " + runvle_path + ";"
    full_cmd = full_cmd + cmd + ";"

    stderr_filename = "stderr.txt"
    stdout_filename = "stdout.txt"
    stderr_path = os.path.join(runvle_path, stderr_filename)
    stdout_path = os.path.join(runvle_path, stdout_filename)

    retcode = call_subprocess(full_cmd, runvle_path, stderr_path, stdout_path)

    msg_stderr = ""
    msg_stdout = ""

    stderr_file = open(stderr_path)
    msg_stderr = stderr_file.read()
    stderr_file.close()

    stdout_file = open(stdout_path)
    msg_stdout = stdout_file.read()
    stdout_file.close()

    return (retcode, msg_stdout, msg_stderr)

def run_app(runvle_path, container_path, appname, appoptions=""):
    """Runs, under runvle_path, appname application of container_path container

    Runs command :

    "singularity run --bind /opt:/opt --app appname container_path appoptions"

    Note : /opt is bind mounted to the /opt directory inside the container.

    Inputs :

    - runvle_path    : folder from where the container is called
    - container_path : path of the models repository container
    - appname        : application name
    - appoptions     : application parameters (optional, depending on app).

    Outputs :

    - retcode : returned by call_subprocess
    - msg_stdout : stdout message
    - msg_stderr : stderr message

    Note : some files are produced under runvle_path (cf cmd, stderr, stdout)
    """

    cmd = "singularity run --bind /opt:/opt --app " +appname
    cmd = cmd + " " +container_path+ " " +appoptions
    (retcode, msg_stdout, msg_stderr) = run_cmd(runvle_path=runvle_path,
                                                cmd=cmd)
    return (retcode, msg_stdout, msg_stderr)


def control_out(msg_stdout, msg_stderr) :
    """Makes controls after run_app and returns cr_OK (True or False)

    Controls : stderr and stdout messages
    """

    if msg_stderr != "" :
        cr_OK = False
    else :
        cr_OK = True
    return cr_OK

def control_end(runvle_path) :
    """Controls the file indicating the end of an application

    Note : control_end is available only for the applications that produce
    an ok_filename file under runvle_path when ending, ie erecord.def
    applications such as erecord app.

    control_end verifies ok_filename file found under runvle_path, returns
    True if found and else False.
    """

    ok_filename = "OK"
    ok_path = os.path.join(runvle_path, ok_filename)
    OK_found = False
    if os.path.exists(ok_path) :
        if os.path.isfile(ok_path) :
            OK_found = True
    return OK_found


#------------------------------------------------------------------------------
# API to some applications (from vle.def)

def get_vle_version_number(runvle_path, container_path):
    """Returns the version number of the vle installed into the container

    Note : as API to vle_version_number app

    Inputs :

    - runvle_path    : folder from where the container is called
    - container_path : path of the models repository container

    Outputs :

    - in error case : error message (a str)
    - else : [ vle_version_number ] : version number (as str singleton)
    """

    vle_version_number = None # default
    try :
        res = run_app(runvle_path=runvle_path, container_path=container_path,
                      appname="vle_version_number")
        (retcode, msg_stdout, msg_stderr) = res
        cr_OK = control_out(msg_stdout, msg_stderr)
        if cr_OK :
            vle_version_number = msg_stdout.rstrip('\n').split(' ')
        else :
            raise Exception(msg_stderr)
    except :
        raise
    return vle_version_number

def get_appname_list(runvle_path, container_path):
    """Returns names of applications into the models repository container

    Note : as API to 'apps' option

    Inputs :

    - runvle_path    : folder from where the command is called
    - container_path : path of the models repository container

    Outputs :

    - list [appname,...]
    - raise exception in error case (bad parameters...)
    """

    appname_list = None # default
    try :
        cmd = "singularity apps " +container_path
        res = run_cmd(runvle_path=runvle_path, cmd=cmd)
        (retcode, msg_stdout, msg_stderr) = res
        cr_OK = control_out(msg_stdout, msg_stderr)
        if cr_OK :
            appname_list = msg_stdout.rstrip('\n').split('\n')
            if (appname_list == ['']) :
                appname_list = []
        else :
            raise Exception(msg_stderr)
    except :
        raise
    return appname_list


def get_pkg_list(runvle_path, container_path):
    """Returns names of packages into the models repository container

    Note : as API to pkg_list app

    Inputs :

    - runvle_path    : folder from where the container is called
    - container_path : path of the models repository container

    Outputs :

    - list [pkgname,...]
    - raise exception in error case (bad parameters...)
    """

    pkg_list = None # default
    try :
        res = run_app(runvle_path=runvle_path, container_path=container_path,
                      appname="pkg_list")
        (retcode, msg_stdout, msg_stderr) = res
        cr_OK = control_out(msg_stdout, msg_stderr)
        if cr_OK :
            pkg_list = msg_stdout.rstrip('\n').split(' ')
            if (pkg_list == ['']) :
                pkg_list = []
        else :
            raise Exception(msg_stderr)
    except :
        raise
    return pkg_list


def get_vpz_list(runvle_path, container_path, pkgname=""):
    """Returns names of vpz into the models repository container

    Note : as API to vpz_list app

    Inputs :

    - runvle_path : folder from where the container is called
    - container_path : path of the models repository container
    - pkgname : the package name, in case of one package; \
                in case of all packages, no pkgname given.

    Outputs :

    - in case of one package : list [vpz,...] of the pkgname package
    - in case of all packages : dict {} of key=pkgname, \
                                           value=list of its [vpz,...]
    - raise exception in error case (unavailable pkgname value...)

    Note : some error cases are accepted => ignored, such as :

    - if a package has no exp directory : that case error message is : \
      "Show package content error: Pkg list error: \
      '/VLE/vle_home/.../pkgname/exp' is not an experiments directory"
    """

    vpz_list = None # default
    try :
        if pkgname != "" :
            pkg_list = get_pkg_list(runvle_path, container_path)
            if pkgname not in pkg_list :
                errormsg = "ERROR : unavailable pkgname value"
                errormsg = errormsg + " : " + str(pkgname)
                raise Exception(errormsg)

        res = run_app(runvle_path=runvle_path, container_path=container_path,
                      appname="vpz_list", appoptions=pkgname)
        (retcode, msg_stdout, msg_stderr) = res
        cr_OK = control_out(msg_stdout, msg_stderr)

        # filters some (accepted) errors
        filtered_msg_stderr = ""
        for line in msg_stderr.splitlines() :
            if "is not an experiments directory" not in line :
                filtered_msg_stderr = filtered_msg_stderr + line + "\n"
        msg_stderr = filtered_msg_stderr
        if msg_stderr == "" :
            cr_OK = True

        if cr_OK :
            if (pkgname == "") : # all packages
                msg = msg_stdout.split('\n')
                vpz_list = dict()
                for m in msg :
                    c = m.split(' : ')
                    pkgname = c[0]
                    if len(c) > 1 :
                        v = c[1] 
                        if v == '' : 
                            vpz_list[pkgname] = []
                        else :
                            vpz_list[pkgname] = v.split(' ')
            else : # pkgname package
                vpz_list = msg_stdout.rstrip('\n').split(' ')
                if (vpz_list == ['']) :
                    vpz_list = []
        else : # not OK
            raise Exception(msg_stderr)
    except :
        raise
    return vpz_list


def get_data_list(runvle_path, container_path, pkgname) :
    """Returns data file names of pkgname package into the container

    Note : as API to data_list app

    Inputs :

    - runvle_path : folder from where the container is called
    - container_path : path of the models repository container
    - pkgname : the package name

    Outputs :

    - list [data file name,...] of the pkgname package
    - raise exception in error case (unavailable pkgname value...)
    """

    data_list = None # default
    try :
        if pkgname != "" :
            pkg_list = get_pkg_list(runvle_path, container_path)
            if pkgname not in pkg_list :
                errormsg = "ERROR : unavailable pkgname value"
                errormsg = errormsg + " : " + str(pkgname)
                raise Exception(errormsg)

        res = run_app(runvle_path=runvle_path, container_path=container_path,
                      appname="data_list", appoptions=pkgname)
        (retcode, msg_stdout, msg_stderr) = res
        cr_OK = control_out(msg_stdout, msg_stderr)
        if cr_OK :
            data_list = msg_stdout.rstrip('\n').split(' ')
            if (data_list == ['']) :
                data_list = []
        else : # not OK
            raise Exception(msg_stderr)
    except :
        raise
    return data_list

#------------------------------------------------------------------------------

