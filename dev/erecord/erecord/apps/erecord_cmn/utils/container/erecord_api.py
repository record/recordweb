"""erecord_cmn.utils.container.erecord_api

Methods as API to models repositories containers -- erecord.def part

A 'repname' models repository container 'rep.simg' has been built from the
succession of vle.def, erecord.def and rep.def recipe files application,
with some apps at each level : 

- vle.def : context, pkg_list, vpz_list...
- erecord.def : erecord

erecord.def is dedicated to erecord vle package, allowing to modify, read and
simulate some vpz.

"""

import os
import json
import shutil

from erecord_cmn.utils.container.vle_api import run_app
from erecord_cmn.utils.container.vle_api import control_out
from erecord_cmn.utils.container.vle_api import control_end
from erecord_cmn.utils.container.vle_api import get_vpz_list
from erecord_cmn.utils.container.location import get_vledatafolder_localpath
from erecord_cmn.utils.container.location import get_vlesavedvpzfile_localpath

#from erecord_cmn.utils.coding import byteify

#------------------------------------------------------------------------------
# json

def get_value_type(json_value_type):
    """Conversion of a value type coming from erecord pkg (json format)

    Note : json_value_type values are :

    - vle::value::Value::type values
    - "" for unknown vle::value::Value::type value \
      or for vle::value::Value::USER that is treated as unknown
    """

    if json_value_type == "BOOLEAN":
        return "boolean"
    elif json_value_type == "INTEGER":
        return "integer"
    elif json_value_type == "DOUBLE":
        return "double"
    elif json_value_type == "STRING":
        return "string"
    elif json_value_type == "XMLTYPE":
        return "xml"
    elif json_value_type == "SET":
        return "set"
    elif json_value_type == "MAP":
        return "map"
    elif json_value_type == "TUPLE":
        return "tuple"
    elif json_value_type == "TABLE":
        return "table"
    elif json_value_type == "MATRIX":
        return "matrix"
    elif json_value_type == "NIL":
        return "none"
    else : # "" and else
        return ""

def create_and_fill_jsonfile(content, folder_path, file_name) :
    jsonfile_tmp_path = os.path.join(folder_path, file_name)
    f = open(jsonfile_tmp_path, 'w')
    f.write(json.dumps(content)) ###f.write(json.dumps(byteify(content)))
    f.close()

def get_file_jsoncontent(folder_path, file_name) :
    output_file = os.path.join(folder_path, file_name)
    f = open(output_file, "r")
    res = f.read()
    f.close()
    r = json.loads(res)
    return r

#------------------------------------------------------------------------------
# General, common

def control_in(runvle_path, container_path, pkgname, vpzname) :
    """Makes first controls and returns a message about errors met

    Controls : pkgname is a package, vpzname is a vpz file of pkgname package

    Note : the returned message is "" if no error met.
    """

    errormsg = ""
    try :
        vpz_list = get_vpz_list(runvle_path=runvle_path,
                                container_path=container_path,
                                pkgname=pkgname)
        if vpzname not in vpz_list :
            errormsg = vpzname+ " is not a vpz file of " +pkgname+ " package"
    except Exception as e :
        errormsg = build_error_message(error=e)
    return errormsg

def run_erecord_app(runvle_path, container_path, appname, appoptions="") :
    """Runs an erecord appname application of container_path container

    Runs, under runvle_path, an appname application of container_path
    container that comes from erecord.def. Such an application produces an
    ending file under runvle_path, that is controlled there.
    """

    cr_ok = None # default
    errormsg = ""

    try :
        res = run_app(runvle_path=runvle_path, container_path=container_path,
                      appname=appname, appoptions=appoptions)
        (retcode, msg_stdout, msg_stderr) = res
        control_out_OK = control_out(msg_stdout=msg_stdout,
                                     msg_stderr=msg_stderr)
        control_end_OK = control_end(runvle_path=runvle_path)

        # Careful : stderr is not empty when OK in case vle-2.0.0
        if not control_out_OK :
            # msg_stderr modification !
            if msg_stderr != "" :
                filtered_stderr = ""
                msg = msg_stderr.split("\n")
                for line in msg :
                    if line is '' :
                        pass
                    elif "debug" in line or "Debug" in line or "DEBUG" in line :
                        pass
                    elif "warning" in line or "Warning" in line or "WARNING" in line :
                        pass
                    else :
                        filtered_stderr = filtered_stderr + line
                msg_stderr = filtered_stderr
            if msg_stderr == "" :
                control_out_OK = True # correction

        cr_ok = control_out_OK and control_end_OK
        if not control_out_OK :
            errormsg = errormsg + " -- " + msg_stderr
        if not control_end_OK :
            errormsg = errormsg + " -- OK signal absent (ending file) -- "

    except Exception as e :
        cr_ok = False
        msg = str(e)
        errormsg = errormsg + " -- " + msg

    # msg_stdout is not studied !
  
    return (cr_ok, errormsg) 

#------------------------------------------------------------------------------
# preparations

def prepare_vpz_reading(runvle_path, container_path, pkgname, vpzname):
    """Prepares reading the vpzname vpz of pkgname package

    Builds under runvle_path the "read_input.json" file,
    as input of read_vpz.vpz simulation of erecord package into container
    (done by erecord app of container)
    """

    errormsg = control_in(runvle_path=runvle_path,
                          container_path=container_path,
                          pkgname=pkgname, vpzname=vpzname)
    if errormsg != "" :
        raise Exception(errormsg)
    errormsg = control_in(runvle_path=runvle_path,
                          container_path=container_path,
                          pkgname="erecord", vpzname="read_vpz.vpz")
    if errormsg != "" :
        raise Exception(errormsg)

    read_parameters = dict()
    read_parameters["pkgname"] = pkgname 
    read_parameters["vpzname"] = vpzname
    create_and_fill_jsonfile(content=read_parameters, folder_path=runvle_path,
                             file_name="read_input.json")

def prepare_vpz_simulating(runvle_path, container_path, pkgname, vpzname,
                           plan, restype):
    """Prepares simulating the vpzname vpz of pkgname package

    Builds under runvle_path the "run_input.json" file,
    as input of run_vpz.vpz simulation of erecord package into container
    (done by erecord app of container)
    """

    errormsg = control_in(runvle_path=runvle_path,
                          container_path=container_path,
                          pkgname=pkgname, vpzname=vpzname)
    if errormsg != "" :
        raise Exception(errormsg)
    errormsg = control_in(runvle_path=runvle_path,
                          container_path=container_path,
                          pkgname="erecord", vpzname="run_vpz.vpz")
    if errormsg != "" :
        raise Exception(errormsg)

    run_parameters = dict()
    run_parameters["pkgname"] = pkgname 
    run_parameters["vpzname"] = vpzname
    run_parameters["plan"] = plan 
    run_parameters["restype"] = restype
    create_and_fill_jsonfile(content=run_parameters, folder_path=runvle_path,
                             file_name="run_input.json")

def prepare_vpz_modifying(runvle_path, container_path, pkgname, vpzname,
                          storaged_list, conditions, begin, duration):
    """Prepares modifying the vpzname vpz of pkgname package

    Builds under runvle_path the "modify_input.json" file,
    as input of modify_vpz.vpz simulation of erecord package into container
    (done by erecord app of container)
    """

    def update(modify_parameters, storaged_list, conditions, begin, duration) :
        """Updates modify_parameters according to input datas
        
        Input datas : storaged_list, conditions, begin, duration

        Note : The value of a condition parameter is supposed to always be
        given as multiple, if its type is 'list'. The case where a parameter
        value of 'list' type would be given as a singleton is not treated.
        """

        if begin is not None :
            modify_parameters["begin"] = begin
        if duration is not None :
            modify_parameters["duration"] = duration
        if modify_parameters is not None :
            modify_parameters["conditions"] = conditions
    
        views = dict()
        at_least_one = False
        if  storaged_list is not None :
            if (len(storaged_list) > 0) :
                at_least_one = True
                views["storaged_list"] = list()
                for v in storaged_list :
                    views["storaged_list"].append(v)
        if at_least_one :
            modify_parameters["views"] = views

    errormsg = control_in(runvle_path=runvle_path,
                          container_path=container_path,
                          pkgname=pkgname, vpzname=vpzname)
    if errormsg != "" :
        raise Exception(errormsg)
    errormsg = control_in(runvle_path=runvle_path,
                          container_path=container_path,
                          pkgname="erecord", vpzname="modify_vpz.vpz")
    if errormsg != "" :
        raise Exception(errormsg)

    modify_parameters = dict()
    modify_parameters["pkgname"] = pkgname 
    modify_parameters["vpzname"] = vpzname
    update(modify_parameters=modify_parameters,
           storaged_list=storaged_list, conditions=conditions,
           begin=begin, duration=duration)
    create_and_fill_jsonfile(content=modify_parameters,
                             folder_path=runvle_path,
                             file_name="modify_input.json")

def prepare_datafolder(runvle_path, data_src):
    """Prepares data folder for vle_actions_vpz

    Fills under runvle_path the data folder as required.

    Note : for data folder identity, see get_vledatafolder_localpath.

    Note : prepare_datafolder must be called before vle_actions_vpz (that
    launches erecord app) in action_copy_datafolder case
    """

    try :
        data_dest = os.path.join(runvle_path, get_vledatafolder_localpath())
        shutil.move(data_src, data_dest)
    except :
        errormsg = "has failed preparing the data folder (datafolder case)"
        raise Exception(errormsg)

def store_saved_vpz_file(runvle_path, vpzname, dest_file_path):
    """Stores as dest_file_path the saved vpz file produced by vle_actions_vpz

    Note : for identity of the saved vpz file, that has previously been
    produced by vle_actions_vpz, see get_vlesavedvpzfile_localpath.

    Note : saved vpz file has been produced by vle_actions_vpz (that
    launches erecord app) in action_copy_modified_vpz case
    """

    try :
        saved_vpz_file_path = os.path.join(runvle_path,
                                       get_vlesavedvpzfile_localpath(vpzname))
        shutil.copyfile(saved_vpz_file_path, dest_file_path)
    except :
        errormsg = "has failed preparing the vpz file into the report folder "
        errormsg = errormsg+ "(report=vpz case)"
        raise Exception(errormsg)

#------------------------------------------------------------------------------
# API to some applications (from erecord.def)

def vle_actions_vpz(runvle_path, container_path, pkgname, vpzname,
                    storaged_list, conditions, begin, duration, plan, restype,
                    action_modify_vpz, action_read_vpz, action_run_vpz,
                    action_copy_datafolder, datafoldercopy,
                    action_copy_modified_vpz):
    """Does the appropriate actions by launching erecord app

    Modifies and reads and simulates the vpzname vpz of pkgname package, by
    taking into account the given data folder, by saving the modified vpz file 

    Returns, as json data, the input information of vpzname and results of
    vpzname simulation

    Before calling vle_actions_vpz :
    In action_copy_datafolder case, there must be the data folder 
    (get_vledatafolder_localpath) under runvle_path => prepare_datafolder
    must have been called in order to create this data folder.

    vle_actions_vpz calls :

    - prepare_vpz_modifying in action_modify_vpz case
    - prepare_vpz_reading in action_read_vpz case
    - prepare_vpz_simulating in action_run_vpz case
    - run_erecord_app to launch erecord app

    After having called vle_actions_vpz :
    In action_copy_modified_vpz case, vle_actions_vpz has saved vpz file 
    (get_vlesavedvpzfile_localpath) under runvle_path
    => store_saved_vpz_file must be called in order to keep this vpz file.

    Note :

    - as API to erecord app of container (prepares and runs modify_vpz.vpz \
      and read_vpz.vpz and run_vpz.vpz simulations of erecord package)

    Notes :

    - also modifies (into container) the vpzname vpz file of pkgname package \
      and copies it (under runvle_path)
    - also produces (under runvle_path) the read_output.json file 
    - also produces (under runvle_path) the run_output.json file 
    """

    read_part = None # default
    run_part = None # default

    appoptions = "-p "+pkgname
    appoptions = appoptions+" "+ "-v "+vpzname
    if action_modify_vpz :
        appoptions = appoptions+" "+ "-a "+"modify"
    if action_read_vpz :
        appoptions = appoptions+" "+ "-a "+"read"
    if action_run_vpz :
        appoptions = appoptions+" "+ "-a "+"run"
    if action_copy_datafolder :
        appoptions = appoptions+" "+ "-d "+datafoldercopy
    if action_copy_modified_vpz :
        appoptions = appoptions+" "+ "-c "

    try :
        if action_modify_vpz :
            prepare_vpz_modifying(runvle_path=runvle_path,
                                  container_path=container_path,
                                  pkgname=pkgname, vpzname=vpzname,
                                  storaged_list=storaged_list,
                                  conditions=conditions,
                                  begin=begin, duration=duration)
        if action_read_vpz :
            prepare_vpz_reading(runvle_path=runvle_path,
                                  container_path=container_path,
                                  pkgname=pkgname, vpzname=vpzname)
        if action_run_vpz :
            prepare_vpz_simulating(runvle_path=runvle_path,
                                  container_path=container_path,
                                  pkgname=pkgname, vpzname=vpzname,
                                  plan=plan, restype=restype)
        (cr_ok, errormsg) = run_erecord_app(runvle_path=runvle_path,
                                  container_path=container_path,
                                  appname="erecord", appoptions=appoptions)
        if not cr_ok :
            raise Exception(" erecord -- " + errormsg)

        read_part = None
        run_part = None
        if action_read_vpz :
            read_part = get_file_jsoncontent(folder_path=runvle_path,
                                             file_name="read_output.json")
        if action_run_vpz :
            run_part = get_file_jsoncontent(folder_path=runvle_path,
                                            file_name="run_output.json")
    except :
        raise
    return (read_part, run_part)

#------------------------------------------------------------------------------
# prints

def print_vle_read_vpz_result(res):

    expected_keys = ["experiment_name", "begin", "duration",
                     "cond_list", "view_list", "names"]
    keys = res.keys()
    for key in keys :
        if key not in expected_keys :
            print("unexpected key : ", key)
            print("    (content : ", res[key], ")")
    key = "experiment_name"
    if key in keys :
        print(key, " : ", res[key])
    key = "begin"
    if key in keys :
        print(key, ": ", res[key])
        begin = res[key]
        print("         begin value : ", begin["value"])
        print("         begin value type: ", type(begin["value"]))
    key = "duration"
    if key in keys :
        print(key, ": ", res[key])
        duration = res[key]
        print("         duration value : ", duration["value"])
        print("         duration value type: ", type(duration["value"]))
    key = "cond_list"
    if key in keys :
        #print(key, " : ", res[key])
        conditions = res[key]
        print("Conditions")
        for cname in conditions.keys() :
            cond = conditions[cname]
            par_list = cond["par_list"]
            print("  - ", cname, " : ")
            for pname in par_list.keys() :
                par = par_list[pname]
                print("    - ", pname, " : ", par)
    key = "view_list"
    if key in keys :
        print(key, " : ", res[key])
        views = res[key]
        print("Views")
        for vname in views.keys() :
            view = views[vname]
            out_list = view["out_list"]
            print("  - ", vname, " : ")
            for oname in out_list :
                print("    - ", oname)
    key = "names"
    if key in keys :
        names = res[key]
        print("Names :")
        if "cond_names" in names.keys() :
            print("  - cond names :")
            for name in names["cond_names"] :
                print("    - ", name)
        if "par_names" in names.keys() :
            print("  - par names :")
            for name in names["par_names"] :
                print("    - ", name)
        if "view_names" in names.keys() :
            print("  - view names :")
            for name in names["view_names"] :
                print("    - ", name)
        if "out_names" in names.keys() :
            print("  - out names :")
            for name in names["out_names"] :
                print("    - ", name)

def print_vle_run_vpz_result(res):

    expected_keys = ["plan", "restype", "res"]
    keys = res.keys()
    for key in keys :
        if key not in expected_keys :
            print("unexpected key : ", key)
            print("    (content : ", res[key], ")")
    key = "plan"
    if key in keys :
        print(key, " : ", res[key])
    key = "restype"
    if key in keys :
        print(key, " : ", res[key])
    key = "res"
    if key in keys :
        print(key, ": ", res[key])

