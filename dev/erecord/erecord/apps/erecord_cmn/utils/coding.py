"""erecord_cmn.utils.coding

Coding methods

"""

import json

def byteify(input):
    """unicode to utf-8

    Convert any decoded JSON object from using unicode strings to
    UTF-8-encoded byte strings

    """
    
    if isinstance(input, dict):
        return {byteify(key):byteify(value) for key,value in input.items()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, str):
        return input
    #elif isinstance(input, unicode):
    #    return input.encode('utf-8')
    else:
        return input

def get_val(value):
    """JSON object to utf-8 """

    return byteify(json.loads(value))

