"""erecord_cmn.views """

from django.shortcuts import render

from erecord_cmn.configs.config import ONLINEDOC_URL
from erecord_cmn.configs.config import ONLINEDOC_WEBAPI_URL
from erecord_cmn.configs.config import ONLINEDOC_USECASE_URL
from erecord_cmn.configs.config import ONLINEDOC_FAQ_URL

#headsubtitle = "erecord_cmn application (common)"
headsubtitle = "(erecord_cmn)"

#------------------------------------------------------------------------------

def onlinedoc_page(request):
    """Online documentation page """

    context = { 'ONLINEDOC_URL': ONLINEDOC_URL,
                'ONLINEDOC_WEBAPI_URL': ONLINEDOC_WEBAPI_URL,
                'ONLINEDOC_USECASE_URL': ONLINEDOC_USECASE_URL,
                'ONLINEDOC_FAQ_URL': ONLINEDOC_FAQ_URL, }
    return render(request, 'erecord_cmn/onlinedoc.html', context)

#------------------------------------------------------------------------------

