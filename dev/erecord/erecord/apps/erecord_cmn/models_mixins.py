"""erecord_cmn.models_mixins

Mixins common to erecord applications models

"""

# Some mixins for erecord models (common to erecord applications models)

class DefaultVerboseNameMixin(object):
    """Additional to some models """

    def set_default_value_verbose_name(self, verbose_name=None) :
        """Sets a default value to verbose_name if empty """
        if (self.verbose_name is None) or (self.verbose_name is '') :
            if verbose_name is not None :
                self.verbose_name = verbose_name
            else :
                self.verbose_name = self.__str__()

class DefaultTextMixin(object):
    """Additional to some models """

    def set_undefined_value_text(self):
        self.text = "undefined"
    def is_undefined_value_text(self):
        return self.text == "undefined"

