"""erecord_db.models

Models of the erecord_db application

A (already physically installed) models repository is recorded into the
database through the following information : its own characteristics (VleRep),
the set of its vle packages { VlePkg } and their vpz files { VleVpz }.

Recording a models repository into the database :

- Input information : (i) folder 'path' where the models repository has been \
  physically installed.,(ii) 'name' given to the models repository into the \
  database.
- Result into the database : VleRep, the set of its vle packages { VlePkg } \
  and their vpz files { VleVpz }.
- Operations : (1) creation of VleRep, (2) creation of the { VlePkg } of \
  VleRep (definition of the list of the existing vle packages of VleRep ; \
  then for each pkg of this list, creation of VlePkg),(3) creation of the \
  { VleVpz } of the { VlePkg } of the VleRep (for each VlePkg of the VleRep, \
  definition of the list of its vpz files ; then for each vpz of this list, \
  creation of VleVpz).  

"""

from django.db import models
from django.core.exceptions import ValidationError

import os

from erecord_cmn.configs.config import REPOSITORIES_HOME

from erecord_acs.models_mixins import LockableMixin
from erecord_cmn.models_mixins import DefaultVerboseNameMixin
from erecord_cmn.models_mixins import DefaultTextMixin
from erecord_db.models_mixins import VleRepMixin
from erecord_db.models_mixins import VlePkgMixin
from erecord_db.models_mixins import VleVpzMixin
from erecord_vpz.models_mixins.workspace import InternalContainerWorkspace

from erecord_cmn.utils.container.location import contains_a_vlecontainer
from erecord_cmn.utils.container.location import is_structured_as_vle_version_name

# A vle version is recorded into the database through the following
# information : its own characteristics (VleVersion), the set of its
# vle models repositories { VleRep }.

ht_vleversion_name = \
    "Name identifying the vle version into the database (unique)."
ht_vleversion_verbose_name = \
    "Human-readable name of the vle version (optional, default value : name)."
ht_vleversion_text = "free text (describing vle version...)"


class VleVersion(DefaultVerboseNameMixin, DefaultTextMixin, models.Model):
    """Vle version

    - name : name given to the vle version for erecord (unique)
    - verbose_name : human-readable name
    - text : free text
    - vlerep_list : list of relative VleRep \
      (many VleRep to one VleVersion relationship)

    """

    name = models.CharField(max_length=200, unique=True, blank=False,
        help_text=ht_vleversion_name)
    verbose_name = models.CharField(max_length=200, blank=True,
        help_text=ht_vleversion_verbose_name)
    text = models.CharField(max_length=10000, blank=True, default="",
        help_text=ht_vleversion_text )

    # methods delete_tree, generate_tree, update_tree could be added

    def clean(self):
        """clean """

        if not is_structured_as_vle_version_name(self.name):
            msg = "The name '" + self.name + "' of vle version is not " + \
                  "well structured (as vle-V.rev)."
            raise ValidationError(msg)
        if self.text is None :
            self.set_undefined_value_text()

    def save(self, *args, **kwargs):
        """save """
        self.set_default_value_verbose_name()
        super(VleVersion, self).save(*args, **kwargs)

    def __str__(self):
        """str """
        return "'"+self.name+"'" + " (Id " +str(self.id)+ ")"

    def get_version_name(self):
        return self.name

    class Meta:
        verbose_name = "vle version"
        verbose_name_plural = "vle versions"


# A models repository is recorded into the database through the following
# information : its own characteristics (VleRep),
# the set of its vle packages { VlePkg } and their vpz files { VleVpz }.

ht_vlerep_name = \
    "Name identifying the models repository into the database (unique)."
ht_vlerep_verbose_name = \
    "Human-readable name of the models repository (optional, default " + \
    "value : name)."
ht_vlerep_text = "free text (describing the models repository...)"
ht_vlerep_path = \
    "Directory path where the models repository has been physically " + \
    "installed (more exactly its container file)."
ht_vlerep_vleversion = \
    "the vle version to which the models repository belongs."

class VleRep(DefaultVerboseNameMixin, DefaultTextMixin, VleRepMixin,
             models.Model):
    """Models repository

    - name : name given to the models repository for erecord (unique)
    - verbose_name : human-readable name
    - text : free text
    - path : directory path where the models repository has been physically \
      installed (in fact containing its container file).
    - vleversion : relative VleVersion (many VleRep to one VleVersion \
      relationship)
    - vlepkg_list : list of relative VlePkg \
      (many VlePkg to one VleRep relationship)

    """

    name = models.CharField(max_length=200, unique=True, blank=False,
        help_text=ht_vlerep_name)
    verbose_name = models.CharField(max_length=200, blank=True,
        help_text=ht_vlerep_verbose_name)
    text = models.CharField(max_length=10000, blank=True, default="",
        help_text=ht_vlerep_text )
    path = models.FilePathField(path=REPOSITORIES_HOME,
                                blank=False, help_text=ht_vlerep_path)

    vleversion = models.ForeignKey(VleVersion, on_delete=models.CASCADE,
        related_name="vlerep_list",
        verbose_name="related vle version", blank=False,
        help_text=ht_vlerep_vleversion)

    def delete_tree(self):
        """delete tree """
        for vlepkg in self.vlepkg_list.all():
            vlepkg.delete()

    def generate_tree(self):
        """generate tree """
        self.read_and_add_tree()

    def update_tree(self):
        """update tree """
        self.read_and_update_tree()

    def clean(self):
        """clean """

        if not contains_a_vlecontainer(path=self.path) :
           msg = "The '" + self.name + "' models repository path (" +  \
               self.path + ") does not contain any models repository " + \
               "container file."
           raise ValidationError(msg)

        vleversion_name = self.vleversion.name

        version_path = os.path.basename(os.path.dirname(self.path))
        if version_path != vleversion_name :
            msg = "The '" + self.name + "' models repository path (" +  \
                self.path + ") is not compatible with the vle version " + \
                "(" + self.vleversion.name + ")."
            raise ValidationError(msg)

        icw = InternalContainerWorkspace(path=self.path)
        vle_version_name_value = icw.CONT_get_vle_version_name()
        icw.clear()
        if vle_version_name_value != vleversion_name :
            msg = "The vle version of the '" + self.name + \
                  "' models repository container file (under " +  \
                  self.path + ") is not compatible with the vle version " + \
                  "(" + self.vleversion.name + ")."
            raise ValidationError(msg)
        if self.text is None :
            self.set_undefined_value_text()

    def save(self, *args, **kwargs):
        """save """
        self.set_default_value_verbose_name()
        super(VleRep, self).save(*args, **kwargs)

    def get_vle_version_name(self):
        return self.vleversion.name

    def __str__(self):
        """str """
        return "'"+self.name+"' (from '"+self.vleversion.name+"')" + \
               " (Id " +str(self.id)+ ")"

    class Meta:
        verbose_name = "models repository"
        verbose_name_plural = "models repositories"


ht_vlepkg_name = "Name of the vle package (same as in vle)."
ht_vlepkg_verbose_name = \
    "Human-readable name of the vle package (optional, default value : name)."
ht_vlepkg_text = "free text (describing the vle package...)"
ht_vlepkg_vlerep = \
    "the models repository to which the vle package belongs. The vle " + \
    "package 'name' must be contained into the models repository " + \
    "container file."


class VlePkg(DefaultVerboseNameMixin, DefaultTextMixin, VlePkgMixin,
             models.Model):
    """Vle package

    (vle notion of package, project)

    - name : vle package name (same as in vle)
    - verbose_name : human-readable name
    - text : free text
    - vlerep : relative VleRep (many VlePkg to one VleRep relationship)
    - vlevpz_list : list of relative VleVpz \
      (many VleVpz to one VlePkg relationship)

    """

    name = models.CharField(max_length=200, blank=False,
        help_text=ht_vlepkg_name)
    verbose_name = models.CharField(max_length=200, blank=True,
        help_text=ht_vlepkg_verbose_name)
    text = models.CharField(max_length=10000, blank=True, default="",
        help_text=ht_vlepkg_text )
    vlerep = models.ForeignKey(VleRep, on_delete=models.CASCADE,
        related_name="vlepkg_list",
        verbose_name="related models repository", blank=False,
        help_text=ht_vlepkg_vlerep)

    def clean(self):
        icw = InternalContainerWorkspace(path=self.vlerep.path)
        pkg_list = icw.CONT_get_pkg_list()
        icw.clear()
        pkgname=self.name
        if pkgname not in pkg_list :
            msg = "'" + pkgname + "' doesn't exist (as a vle package) " + \
                "into the '" + self.vlerep.name + "' models repository " + \
                "container file (under " + self.vlerep.path + ")"
            raise ValidationError(msg)
        if self.text is None :
            self.set_undefined_value_text()

    def save(self, *args, **kwargs):
        self.set_default_value_verbose_name()
        super(VlePkg, self).save(*args, **kwargs)

    def __str__(self):
        return "'"+self.name+"' (from '"+self.vlerep.name+"')" + \
               " (Id " +str(self.id)+ ")"

    class Meta:
        verbose_name = "vle package"
        verbose_name_plural = "vle packages"

ht_vlevpz_name = \
    "Name of the vpz file : path relative to 'exp' directory, with .vpz " + \
    "extension (same as in vle)."
ht_vlevpz_verbose_name = \
    "Human-readable name of the vpz file (optional, default value : name)."
ht_vlevpz_text = "free text (describing the vpz file...)"
ht_vlevpz_vlepkg = \
    "the vle package to which the vpz file belongs. The vpz file 'name' " + \
    "must be contained into the models repository container file under " + \
    "this vle package."

class VleVpz(LockableMixin, DefaultVerboseNameMixin, DefaultTextMixin,
             VleVpzMixin, models.Model):
    """Vpz file

    (vle notion of vpz file, also called simulator, simulation scenario)

    - name : vpz file name (path relative to 'exp' directory, with .vpz \
      extension) (same as in vle)
    - verbose_name : human-readable name
    - text : free text
    - vlepkg : relative VlePkg (many VleVpz to one VlePkg relationship)
    - vpzinput : relative VpzInput (one VleVpz to one VpzInput relationship)
    - vpzoutput : relative VpzOutput (one VleVpz to one VpzOutput relationship)

    """

    name = models.CharField(max_length=200, blank=False,
        help_text=ht_vlevpz_name)
    verbose_name = models.CharField(max_length=200, blank=True,
        help_text=ht_vlevpz_verbose_name)
    text = models.CharField(max_length=10000, blank=True, default="",
        help_text=ht_vlevpz_text )
    vlepkg = models.ForeignKey(VlePkg, on_delete=models.CASCADE,
        related_name="vlevpz_list",
        verbose_name="related vle package", blank=False,
        help_text=ht_vlevpz_vlepkg)

    def clean(self):

        vlereppath = self.vlepkg.vlerep.path
        repname = self.vlepkg.vlerep.name
        pkgname = self.vlepkg.name
        vpzname = self.name
        icw = InternalContainerWorkspace(path=vlereppath)
        all_vpz_list = icw.CONT_get_vpz_list()
        icw.clear()
        CR_OK = False # default
        if pkgname in all_vpz_list.keys() :
            if vpzname in all_vpz_list[pkgname] :
                CR_OK = True
        if not CR_OK :
            msg = "'" + vpzname + "' doesn't exist as a vpz file of " + \
                "'" + pkgname + "' vle package into the " + \
                "'" + repname + "' models repository container file " + \
                "(under " + vlereppath + ")"
            raise ValidationError(msg)
        if self.text is None :
            self.set_undefined_value_text()

    def save(self, *args, **kwargs):
        self.set_default_value_verbose_name()
        super(VleVpz, self).save(*args, **kwargs)

    def __str__(self):
        return "'" + self.name + "' (from '" + self.vlepkg.name + \
            "' (from '" + self.vlepkg.vlerep.name + "'))" + \
            " (Id " +str(self.id)+ ")"

    def get_vlerep(self):
        """returns the VleRep relative to the VleVpz """
        return self.vlepkg.vlerep
    get_vlerep.short_description = "Related models repository"

    class Meta:
        verbose_name = "vle vpz"
        verbose_name_plural = "vle vpz"

