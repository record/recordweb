from django.apps import AppConfig


class ErecordDbConfig(AppConfig):
    name = 'erecord_db'
