"""erecord_db.serializers """

from rest_framework import serializers

from erecord_db.models import VleVersion
from erecord_db.models import VleRep
from erecord_db.models import VlePkg
from erecord_db.models import VleVpz


#------------------------------------------------------------------------------

class VleOptionSerializer(serializers.Serializer):
    vle = serializers.IntegerField( required = False )
    def validate(self, attrs):
        if 'vle' not in attrs.keys() :
            attrs['vle']=None
        return attrs

class RepOptionSerializer(serializers.Serializer):
    rep = serializers.IntegerField( required = False )
    def validate(self, attrs):
        if 'rep' not in attrs.keys() :
            attrs['rep']=None
        return attrs

class PkgOptionSerializer(serializers.Serializer):
    pkg = serializers.IntegerField( required = False )
    def validate(self, attrs):
        if 'pkg' not in attrs.keys() :
            attrs['pkg']=None
        return attrs

class VpzOptionSerializer(serializers.Serializer):
    vpz = serializers.IntegerField( required = False )
    def validate(self, attrs):
        if 'vpz' not in attrs.keys() :
            attrs['vpz']=None
        return attrs

#------------------------------------------------------------------------------

class VleVersionSerializerLink(serializers.ModelSerializer):
    vlerep_list = serializers.HyperlinkedRelatedField(many=True,
            read_only=True, # queryset = VleRep.objects.all(),
            view_name='erecord_db-rep-detail')

    class Meta:
        model = VleVersion
        fields= ( 'id', 'name', 'verbose_name', 'text', 'vlerep_list',)

class VleRepSerializerLink(serializers.ModelSerializer):
    vlepkg_list = serializers.HyperlinkedRelatedField(many=True,
            read_only=True, # queryset = VlePkg.objects.all(),
            view_name='erecord_db-pkg-detail')
    vleversion = serializers.HyperlinkedRelatedField(
            read_only=True, # queryset = VleVersion.objects.all(),
            view_name='erecord_db-vle-detail')

    class Meta:
        model = VleRep
        fields= ( 'id', 'name', 'verbose_name', 'text', 'path', 'vleversion',
                  'vlepkg_list',)

class VlePkgSerializerLink( serializers.ModelSerializer ):
    vlevpz_list = serializers.HyperlinkedRelatedField(many=True,
            read_only=True, # queryset = VleVpz.objects.all(),
            view_name='erecord_db-vpz-detail')
    vlerep = serializers.HyperlinkedRelatedField(
            read_only=True, # queryset = VleRep.objects.all(),
            view_name='erecord_db-rep-detail')
    class Meta:
        model = VlePkg
        fields= ( 'id', 'verbose_name', 'text', 'name', 'vlerep',
                  'vlevpz_list',)

class VleVpzSerializerLink( serializers.ModelSerializer ):
    vlepkg = serializers.HyperlinkedRelatedField(
            read_only=True, # queryset = VlePkg.objects.all(),
            view_name='erecord_db-pkg-detail')
    class Meta:
        model = VleVpz
        fields= ( 'id', 'verbose_name', 'text', 'name', 'vlepkg',)

#------------------------------------------------------------------------------

class VleVersionSerializerTree(serializers.ModelSerializer):
    class Meta:
        model = VleVersion
        fields= ( 'id', 'name', 'verbose_name', 'text', 'vlerep_list',)
        depth = 3

class VleRepSerializerTree(serializers.ModelSerializer):
    class Meta:
        model = VleRep
        fields= ( 'id', 'name', 'verbose_name', 'text', 'path',
                  'vlepkg_list',)
        depth = 3

class VlePkgSerializerTree( serializers.ModelSerializer ):
    class Meta:
        model = VlePkg
        fields= ( 'id', 'verbose_name', 'text', 'name', 'vlerep',
                  'vlevpz_list',)
        depth = 3

class VleVpzSerializerTree( serializers.ModelSerializer ):
    class Meta:
        model = VleVpz
        fields= ( 'id', 'verbose_name', 'text', 'name', 'vlepkg',)
        depth = 3

#------------------------------------------------------------------------------

