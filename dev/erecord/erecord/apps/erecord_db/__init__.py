"""erecord_db

*This package is part of erecord - web services for vle models*

:copyright: Copyright (C) 2019-2024 INRA http://www.inra.fr.
:license: GPLv3, see LICENSE file for more details.
:authors: see AUTHORS file.

Database application
 
The db application is dedicated to the main erecord database.

The erecord database contains models repositories and their tree (vle packages,
vpz files).

A models repository is a self independant and consistent group of some vle
packages.

'vle package' is the vle term for a model (also called 'project' in vle).
Each vle package has some vpz files.

'vpz file' is the vle term for a simulator, or simulation scenario.

The erecord web services allow to manipulate vpz files of vle packages of
models repositories (edit, modify, simulate).

"""

