"""erecord_db.urls

Urls of the erecord_db application

"""

from django.urls import path

from django.views.generic import TemplateView

from erecord_db.views import menus as db_views_menus
from erecord_db.views import objects as db_views_objects
from erecord_db.views import views as db_views_views

urlpatterns = [

    # root page
    path('', TemplateView.as_view(template_name='erecord_cmn/index.html'),
        name="erecord_db-index-page"),

    # menu pages

    path('menu/misc/',
         TemplateView.as_view(template_name='erecord_db/menu_misc.html'),
         name='erecord_db-menu-misc-page'),
    path('menu/', db_views_menus.MenuMainPageList.as_view(),
         name='erecord_db-menu-main-page-list'),
    path('menu/input/', db_views_menus.MenuVpzInputPageList.as_view(),
         name='erecord_db-menu-vpzinput-page-list'),
    path('menu/output/', db_views_menus.MenuVpzOutputPageList.as_view(),
         name='erecord_db-menu-vpzoutput-page-list'),
    path('menu/inout/', db_views_menus.MenuVpzInoutPageList.as_view(),
         name='erecord_db-menu-vpzinout-page-list'),
    path('menu/experiment/',
         db_views_menus.MenuVpzInoutModifyPageList.as_view(),
         name='erecord_db-menu-vpzinoutmodify-page-list'),

    path('menu/<int:pk>/', db_views_menus.MenuMainPageDetail.as_view(),
         name="erecord_db-menu-main-page-detail"),
    path('menu/input/<int:pk>/',
         db_views_menus.MenuVpzInputPageDetail.as_view(),
         name="erecord_db-menu-vpzinput-page-detail"),
    path('menu/output/<int:pk>/',
         db_views_menus.MenuVpzOutputPageDetail.as_view(),
         name="erecord_db-menu-vpzoutput-page-detail"),
    path('menu/inout/<int:pk>/',
         db_views_menus.MenuVpzInoutPageDetail.as_view(),
         name="erecord_db-menu-vpzinout-page-detail"),
    path('menu/experiment/<int:pk>/',
         db_views_menus.MenuVpzInoutModifyPageDetail.as_view(),
         name="erecord_db-menu-vpzinoutmodify-page-detail"),

    # home menus

    path('home/', db_views_menus.home, name="erecord_db-home-menu"),

    path('home/lists/', db_views_menus.home_lists,
         name="erecord_db-home-menu-lists"),
    path('home/detail/<int:pk>/', db_views_menus.home_details,
         name="erecord_db-home-menu-detail"),

    # lists and details
    # options : mode=tree,link ; format=api,json,html

    path('vle/', db_views_objects.VleVersionList.as_view(),
         name='erecord_db-vle-list' ),
    path('rep/', db_views_objects.VleRepList.as_view(),
         name='erecord_db-rep-list' ),
    path('pkg/', db_views_objects.VlePkgList.as_view(),
         name='erecord_db-pkg-list' ),
    path('vpz/', db_views_objects.VleVpzList.as_view(),
         name='erecord_db-vpz-list' ),

    path('vle/<int:pk>/', db_views_objects.VleVersionDetail.as_view(),
         name='erecord_db-vle-detail' ),
    path('rep/<int:pk>/', db_views_objects.VleRepDetail.as_view(),
         name='erecord_db-rep-detail' ),
    path('pkg/<int:pk>/', db_views_objects.VlePkgDetail.as_view(),
         name='erecord_db-pkg-detail' ),
    path('vpz/<int:pk>/', db_views_objects.VleVpzDetail.as_view(),
         name='erecord_db-vpz-detail' ),

    # name, datalist, access

    path('rep/<int:pk>/name/', db_views_views.VleRepNameView.as_view(),
         name='erecord_db-rep-name' ),
    path('pkg/<int:pk>/name/', db_views_views.VlePkgNameView.as_view(),
         name='erecord_db-pkg-name' ),
    path('vpz/<int:pk>/name/', db_views_views.VleVpzNameView.as_view(),
         name='erecord_db-vpz-name' ),

    path('pkg/<int:pk>/datalist/',
         db_views_views.VlePkgDatalistView.as_view(),
         name='erecord_db-pkg-datalist' ),

    # access : see acs
]

