"""erecord_db.forms"""

from django import forms

from erecord_cmn.forms import ReadonlyForm

from erecord_db.models import VleVersion
from erecord_db.models import VleRep
from erecord_db.models import VlePkg
from erecord_db.models import VleVpz

from erecord_cmn.configs.config import REPOSITORIES_HOME

from erecord_vpz.models_mixins.workspace import InternalContainerWorkspace
from erecord_cmn.utils.container.location import contains_a_vlecontainer
from erecord_cmn.utils.container.location import is_structured_as_vle_version_name

from erecord_db.models import ht_vleversion_name as mht_vleversion_name
from erecord_db.models import ht_vlerep_path as mht_vlerep_path
from erecord_db.models import ht_vlepkg_name as mht_vlepkg_name
from erecord_db.models import ht_vlevpz_name as mht_vlevpz_name

empty_choice = ("","---------")

def get_vleversionname_choices() :
    """Returns the list to choose a vle version name.
    
    The vle versions taken into account are those found under the
    REPOSITORIES_HOME path.
    """

    choices_list = [ empty_choice ]
    tmp = forms.FilePathField(path=REPOSITORIES_HOME, recursive=False,
            match="vle.*$", # filtering vle-X subfolders
            allow_files=False, allow_folders=True)
    for (path,c) in tmp.choices :
        if is_structured_as_vle_version_name(c):
            choices_list.append( (c,c) )
    return choices_list

ht_vleversion_name = mht_vleversion_name+ \
        " Choose a vle version among the existing ones"

class VleVersionForm(forms.ModelForm):

    name = forms.ChoiceField( choices=get_vleversionname_choices(), 
            required=True, help_text=ht_vleversion_name)

    def __init__(self, *args, **kwargs):
        super(VleVersionForm, self).__init__(*args, **kwargs)
        for (n,f) in self.fields.items() :
            if n is "text" :
                f.widget=forms.Textarea(attrs={'rows':1, 'cols':100})

    class Meta:
        model = VleVersion
        fields = '__all__'

def get_vlereppath_choices() :
    """Returns the list to choose a models repository path.
    
    The path taken into account are those found under the REPOSITORIES_HOME
    path, that contains a models repository container file.

    Their vle packages are also shown in order to help choosing.
    """

    choices_list = list()
    tmp = forms.FilePathField(path=REPOSITORIES_HOME, recursive=True,
            allow_files=False, allow_folders=True)
    for (path,c) in tmp.choices :
        if contains_a_vlecontainer(path=path) :
            choices_list.append( (path,c) )

    text = "Don't choose one of the following values, \
        (existing vle packages shown only for help) :"
    plus = [ empty_choice, ("",text) ]
    for (path,c) in choices_list :
        icw = InternalContainerWorkspace(path=path)
        pkg_list = icw.CONT_get_pkg_list()
        icw.clear()
        for pkgname in pkg_list :
            text = c+": "+pkgname
            plus.append( ("",text) )
    for p in plus :
        choices_list.append(p)

    choices_list.insert(0, empty_choice)

    return choices_list

ht_vlerep_path = mht_vlerep_path+ \
        " Choose a directory among those existing ones under "+ \
        REPOSITORIES_HOME+"."

class VleRepForm(forms.ModelForm):

    path = forms.ChoiceField( choices=get_vlereppath_choices(), 
           required=True, help_text=ht_vlerep_path)

    def __init__(self, *args, **kwargs):
        super(VleRepForm, self).__init__(*args, **kwargs)
        for (n,f) in self.fields.items() :
            if n is "text" :
                f.widget=forms.Textarea(attrs={'rows':1, 'cols':100})

    class Meta:
        model = VleRep
        fields = '__all__'

def get_vlepkgname_choices() :
    """Returns the list to choose a vle package name.
    
    The vle packages taken into account are those found into the container
    file of one of the database models repositories.
    """

    choices_list = [ empty_choice ]
    try :
        for vlerep in VleRep.objects.all() :
            icw = InternalContainerWorkspace(path=vlerep.path)
            pkg_list = icw.CONT_get_pkg_list()
            icw.clear()
            for vlepkgname in pkg_list :
                choice_info = "rep: "+vlerep.name+" ; pkg: "+vlepkgname 
                choices_list.append( (vlepkgname, choice_info) )
    except :
        pass
    return choices_list

ht_vlepkg_name = mht_vlepkg_name+ \
        " Choose a vle package among the existing (models repository name, \
        vle package name)"

class VlePkgForm(forms.ModelForm):

    name = forms.ChoiceField( choices=get_vlepkgname_choices(), 
            required=True,
            help_text=ht_vlepkg_name)

    def __init__(self, *args, **kwargs):
        super(VlePkgForm, self).__init__(*args, **kwargs)
        for (n,f) in self.fields.items() :
            if n is "text" :
                f.widget=forms.Textarea(attrs={'rows':1, 'cols':100})

    class Meta:
        model = VlePkg
        fields = '__all__'

def get_vlevpzname_choices() :
    """Returns the list to choose a vpz file name.
    
    The vpz files taken into account are those found into the container
    file of one of the database models repositories.
    """

    choices_list = [ empty_choice ]
    try :
        for vlerep in VleRep.objects.all() :
            icw = InternalContainerWorkspace(path=vlerep.path)
            all_vpz_list = icw.CONT_get_vpz_list()
            icw.clear()
            for pkgname,vpz_list in all_vpz_list.items() :
                for vpzname in vpz_list :
                    choice_info = "rep: "+vlerep.name+" ; pkg: "+ \
                                  pkgname+" ; vpz: "+vpzname
                    choices_list.append( (vpzname, choice_info) )
    except :
        pass
    return choices_list


ht_vlevpz_name = mht_vlevpz_name+ \
        " Choose a vpz file among the existing (models repository name, \
        vle package name, vpz file name)"

class VleVpzForm(forms.ModelForm):

    name = forms.ChoiceField( choices=get_vlevpzname_choices(), 
            required=True, help_text=ht_vlevpz_name)

    def __init__(self, *args, **kwargs):
        super(VleVpzForm, self).__init__(*args, **kwargs)
        for (n,f) in self.fields.items() :
            if n is "text" :
                f.widget=forms.Textarea(attrs={'rows':1, 'cols':100})

    class Meta:
        model = VleVpz
        fields = '__all__'

###############################################################################

class VleVersionUserForm(ReadonlyForm):
    class Meta:
        model = VleVersion
        fields = '__all__'

class VleRepUserForm(ReadonlyForm):
    class Meta:
        model = VleRep
        fields = '__all__'

class VlePkgUserForm(ReadonlyForm):
    class Meta:
        model = VlePkg
        fields = '__all__'

class VleVpzUserForm(ReadonlyForm):
    class Meta:
        model = VleVpz
        fields = '__all__'

###############################################################################

class VleVersionNameForm(ReadonlyForm):
    class Meta:
        model = VleVersion
        fields = ['name',]

class VleRepNameForm(ReadonlyForm):
    class Meta:
        model = VleRep
        fields = ['name',]

class VlePkgNameForm(ReadonlyForm):
    class Meta:
        model = VlePkg
        fields = ['name',]

class VleVpzNameForm(ReadonlyForm):
    class Meta:
        model = VleVpz
        fields = ['name',]

###############################################################################
# Forms for menus

# default forms

class FormatForm(forms.Form):
    HTML = "html"
    JSON = "json"
    API = "api"
    YAML = "yaml"
    XML = "xml"
    CHOICES = [ (HTML, "html"), (JSON, "json"), (API, "api"),
                (YAML, "yaml"), (XML, "xml"),]
    LABEL = "format"
    HELP_TEXT = "response format"
    format = forms.ChoiceField(required=True, label=LABEL, choices=CHOICES,
                               help_text=HELP_TEXT, initial=HTML)
class StyleForm(forms.Form):
    COMPACTLIST = "compactlist"
    COMPACT = "compact"
    TREE = "tree"
    LINK = "link"
    CHOICES = [ (TREE, "tree"), (LINK, "link"),]
    LABEL = "style"
    HELP_TEXT = "style of presentation"
    mode = forms.ChoiceField(required=True, label=LABEL, choices=CHOICES,
                             help_text=HELP_TEXT, initial=TREE)
class PlanForm(forms.Form):
    SINGLE = "single"
    LINEAR = "linear"
    CHOICES = [ (SINGLE, "single"), (LINEAR, "linear"),]
    LABEL = "plan"
    HELP_TEXT = "mono or multi-simulation"
    mode = forms.ChoiceField(required=True, label=LABEL, choices=CHOICES,
                             help_text=HELP_TEXT, initial=SINGLE)
class RestypeForm(forms.Form):
    DATAFRAME = "dataframe"
    MATRIX = "matrix"
    CHOICES = [ (DATAFRAME, "dataframe"), (MATRIX, "matrix"),]
    LABEL = "restype"
    HELP_TEXT = "type of result"
    mode = forms.ChoiceField(required=True, label=LABEL, choices=CHOICES,
                             help_text=HELP_TEXT, initial=DATAFRAME)
class StorageForm(forms.Form):
    STORAGE = "storage"
    EMPTY_VALUE = ""
    CHOICES = [ (STORAGE, "yes"), (EMPTY_VALUE, "no"),]
    LABEL = "storage"
    HELP_TEXT = "web storage activation"
    mode = forms.ChoiceField(required=True, label=LABEL, choices=CHOICES,
                             help_text=HELP_TEXT, initial=STORAGE)
class OutselectForm(forms.Form):
    ALL = "all"
    EMPTY_VALUE = ""
    CHOICES = [ (ALL, "yes"), (EMPTY_VALUE, "no"),]
    LABEL = "forcing the outputs"
    HELP_TEXT = "outputs selection"
    outselect = forms.ChoiceField(required=True, label=LABEL, choices=CHOICES,
                                  help_text=HELP_TEXT, initial=ALL)
class TodownloadForm(forms.Form):
    TODOWNLOAD = "todownload"
    EMPTY_VALUE = ""
    CHOICES = [ (TODOWNLOAD, "yes"), (EMPTY_VALUE, "no"),]
    LABEL = "to download"
    HELP_TEXT = "downloading the result later on"
    mode = forms.ChoiceField(required=True, label=LABEL, choices=CHOICES,
                             help_text=HELP_TEXT, initial=EMPTY_VALUE)

class DatafoldercopyForm(forms.Form):
    OVERWRITE = "overwrite"
    REPLACE = "replace"
    CHOICES = [ (OVERWRITE, "overwrite"), (REPLACE, "replace"),]
    LABEL = "datafolder copy"
    HELP_TEXT = "how to take into account input datas folder"
    mode = forms.ChoiceField(required=True, label=LABEL, choices=CHOICES,
                             help_text=HELP_TEXT, initial=OVERWRITE)

# vpz/input forms

class VpzInputFormatForm(FormatForm):
    pass
class VpzInputStyleForm(StyleForm):
    mode = forms.ChoiceField(required=True, label=StyleForm.LABEL,
               choices=[ (StyleForm.COMPACTLIST, "compactlist"),
                         (StyleForm.COMPACT, "compact"),
                         (StyleForm.TREE, "tree"),
                         (StyleForm.LINK, "link"),],
               help_text=StyleForm.HELP_TEXT, initial=StyleForm.COMPACTLIST)
class VpzInputOutselectForm(OutselectForm):
    pass

# vpz/output forms

class VpzOutputFormatForm(FormatForm):
    pass
class VpzOutputStyleForm(StyleForm):
    mode = forms.ChoiceField(required=True, label=StyleForm.LABEL,
               choices=[ (StyleForm.COMPACT, "compact"),
                         (StyleForm.TREE, "tree"),
                         (StyleForm.LINK, "link"),],
               help_text=StyleForm.HELP_TEXT, initial=StyleForm.COMPACT)
class VpzOutputPlanForm(PlanForm):
    pass
class VpzOutputRestypeForm(RestypeForm):
    pass
class VpzOutputStorageForm(StorageForm):
    pass
class VpzOutputOutselectForm(OutselectForm):
    pass
class VpzOutputDatafoldercopyForm(DatafoldercopyForm):
    pass

# vpz/inout forms

class VpzInoutFormatForm(FormatForm):
    format = forms.ChoiceField(required=True, label=FormatForm.LABEL,
                 choices=FormatForm.CHOICES, help_text=FormatForm.HELP_TEXT,
                 initial=FormatForm.API)
class VpzInoutStyleForm(StyleForm):
    mode = forms.ChoiceField(required=True, label=StyleForm.LABEL,
               choices=StyleForm.CHOICES,
               help_text=StyleForm.HELP_TEXT, initial=StyleForm.TREE)
class VpzInoutPlanForm(PlanForm):
    pass
class VpzInoutRestypeForm(RestypeForm):
    pass
class VpzInoutOutselectForm(OutselectForm):
    pass
class VpzInoutDatafoldercopyForm(DatafoldercopyForm):
    pass

# vpz/experiment forms

class VpzInoutModifyTodownloadForm(TodownloadForm):
    pass
class VpzInoutModifyDatafoldercopyForm(DatafoldercopyForm):
    pass

# identity forms

class IdentityFormatForm(FormatForm):
    pass
class IdentityStyleForm(StyleForm):
    mode = forms.ChoiceField(required=True, label=StyleForm.LABEL,
               choices=StyleForm.CHOICES, initial=StyleForm.TREE)

