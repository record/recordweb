"""erecord_db.models_mixins

Mixins for models of the erecord_db application

"""

from erecord_vpz.models_mixins.workspace import InternalContainerWorkspace

import datetime

# Some mixins for the erecord_db models

class VleRepMixin(object):
    """Additional methods for VleRep """

    def read_and_add_tree(self) :
        """Reads a VleRep tree (its vle packages and their vpz files) in its
        path and add the relevant VlePkg and VleVpz into the database """

        icw = InternalContainerWorkspace(path=self.path)
        all_vpz_list = icw.CONT_get_vpz_list()
        icw.clear()
        for vlepkgname,vpz_list in all_vpz_list.items() :
            vlepkg = self.vlepkg_list.create(name=vlepkgname)
            for vlevpzname in vpz_list :
                vlepkg.vlevpz_list.create(name=vlevpzname)

    def read_and_update_tree(self) :
        """Reads a VleRep tree (its vle packages and their vpz files) in its
        path and add or remove the relevant VlePkg and VleVpz into the
        database """

        icw = InternalContainerWorkspace(path=self.path)
        all_vpz_list = icw.CONT_get_vpz_list()
        icw.clear()

        # delete into the database what is not (anymore) in all_vpz_list
        for vlepkg in self.vlepkg_list.all():
            if vlepkg.name not in all_vpz_list.keys():
                vlepkg.delete()
            else :
                for vlevpz in vlepkg.vlevpz_list.all():
                    if vlevpz.name not in all_vpz_list[vlepkg.name]:
                        vlevpz.delete()
        # add into the database what is new in path
        vlepkg_names = [vlepkg.name for vlepkg in self.vlepkg_list.all()]
        for vlepkgname in all_vpz_list.keys():
            if vlepkgname not in vlepkg_names: # create vlepkg and its vlevpz
                vlepkg = self.vlepkg_list.create(name=vlepkgname)
                for vlevpzname in all_vpz_list[vlepkgname]:
                    vlepkg.vlevpz_list.create(name=vlevpzname)
            else : # may be create some vlevpz
                for vlepkg in self.vlepkg_list.all().filter(name=vlepkgname):
                    vlevpz_names = [vlevpz.name
                                    for vlevpz in vlepkg.vlevpz_list.all()]
                    for vlevpzname in all_vpz_list[vlepkgname]:
                        if vlevpzname not in vlevpz_names:
                            vlepkg.vlevpz_list.create(name=vlevpzname)

class VlePkgMixin(object):
    """Additional methods for VlePkg """

    def get_datafilename_list(self):
        """Defines and returns the name list of data folder files of VlePkg

        The data folder is the default one : 'data' subdirectory of the
        vle package.

        'CMakeLists.txt' is excluded.
        """

        icw = InternalContainerWorkspace(path=self.vlerep.path)
        data_list = icw.CONT_get_data_list(pkgname=self.name)
        icw.clear()
        n = "CMakeLists.txt"
        if n in data_list:
            data_list.remove(n)
        return data_list
 
class VleVpzMixin(object):
    """Additional methods for VleVpz """

    def for_vpzact_creation(self):
        """Prepares the creation of a VpzAct from a VleVpz

        VpzAct is part of erecord_vpz application models 
        """

        now = datetime.datetime.now()
        res = None
        text = "Vpz activity  (Vpzact) created from VleVpz. "
        text = text+ "VleVpz id : " + str(self.id) + ", "
        text = text+ "verbose_name : " + self.verbose_name + ", "
        text = text+ "name : " + self.name + ". "
        text = text+ "VlePkg id : " + str(self.vlepkg.id) + ", "
        text = text+ "verbose_name : " + self.vlepkg.verbose_name + ", "
        text = text+ "name : " + self.vlepkg.name + ". "
        text = text+ "VleRep id : " + str(self.vlepkg.vlerep.id) + ", "
        text = text+ "verbose_name : " + self.vlepkg.vlerep.verbose_name + "."
        text = text+ "name : " + self.vlepkg.vlerep.name + ", "
        text = text+ "path : " + self.vlepkg.vlerep.path + ". "
        text = text+ "VleVersion id : "
        text = text+ str(self.vlepkg.vlerep.vleversion.id) + ", "
        text = text+ "verbose_name : "
        text = text+ self.vlepkg.vlerep.vleversion.verbose_name + "."
        text = text+ "name : " + self.vlepkg.vlerep.vleversion.name + "."
        text = text+ "Time:" + str(now) + "."

        res = { 'vlepath': self.vlepkg.vlerep.path,
                'pkgname': self.vlepkg.name, 
                'vpzname': self.name,
                'vleversion': self.vlepkg.vlerep.vleversion.name,
                'textorigin': text, }
        return res

