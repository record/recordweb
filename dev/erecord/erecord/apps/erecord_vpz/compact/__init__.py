"""erecord_vpz.compact

*This package is part of erecord - web services for vle models*

:copyright: Copyright (C) 2019-2024 INRA http://www.inra.fr.
:license: GPLv3, see LICENSE file for more details.
:authors: see AUTHORS file.

Vpz activities application, code specific to the compact style presentation

In the Vpz activities application, it may be possible to select a compact \
style presentation (see mode=compact or compactlist).

This subpackage is dedicated to code about compact style management.

"""

