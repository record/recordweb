"""erecord_vpz.compact.serializers"""

from rest_framework import serializers

from erecord_vpz.compact.models import VleParCompact
from erecord_vpz.compact.models import VleCondCompact
from erecord_vpz.compact.models import VleViewCompact
from erecord_vpz.compact.models import VleOutCompact

class VpzInputCompact(object):
    """VpzInput relative to compact style (minimum information around values)

    This presentation should help user preparing its request (see "pars", \
    "outselect"...)

    Only selected VlePar (and their VleCond) of VpzInput are kept.
    Only selected VleOut (and their VleView) of VpzInput are kept.
    """

    def __init__(self, begin=None, duration=None, pars=None, conds=None, 
                 views=None, outs=None):
        self.begin = begin
        self.duration = duration
        self.pars = list()
        if isinstance(pars, list) :
            for par in pars :
                self.pars.append(par)
        self.conds = list()
        if isinstance(conds, list) :
            for cond in conds :
                self.conds.append(cond)
        self.views = list()
        if isinstance(views, list) :
            for view in views :
                self.views.append(view)
        self.outs = list()
        if isinstance(outs, list) :
            for out in outs :
                self.outs.append(out)

    def init_from_vpzinput(self, vpzinput) :
        """Initializes VpzInputCompact values from a VpzInput values

        Only selected VlePar and selected VleOut are kept.
        A VleCond without any selected VlePar is not kept.
        A VleView without any selected VleOut is kept (cf output datas created 
        dynamically).
        """

        self.begin = vpzinput.vlebegin.value
        self.duration = vpzinput.vleduration.value
        for c in vpzinput.vlecond_list.all() :
            at_least_one_vlepar_selected = False
            for p in c.vlepar_list.all() :
                if p.is_selected():
                    self.pars.append( VleParCompact(cname=p.cname,
                        pname=p.pname, value=p.value,
                        selection_name=p.build_selection_name()) )
                    at_least_one_vlepar_selected = True
            if at_least_one_vlepar_selected:
                self.conds.append( 
                        VleCondCompact(
                            selection_name=c.build_selection_name()) )
        for v in vpzinput.vleview_list.all() :
            at_least_one_vleout_selected = False
            for o in v.vleout_list.all() :
                if o.is_selected():
                    self.outs.append( 
                            VleOutCompact(
                                selection_name=o.build_selection_name()) )
                    at_least_one_vleout_selected = True
            self.views.append( 
                    VleViewCompact(
                        selection_name=v.build_selection_name()) )

class VleParCompactSerializer(serializers.ModelSerializer):
    class Meta:
        model = VleParCompact
        fields= [ 'selection_name', 'cname', 'pname', 'value',]

class VleCondCompactSerializer(serializers.ModelSerializer):
    class Meta:
        model = VleCondCompact
        fields= [ 'selection_name']

class VleViewCompactSerializer(serializers.ModelSerializer):
    class Meta:
        model = VleViewCompact
        fields= [ 'selection_name']

class VleOutCompactSerializer(serializers.ModelSerializer):
    class Meta:
        model = VleOutCompact
        fields= [ 'selection_name']

class VpzInputCompactSerializer(serializers.Serializer):
    """Some options to modify VpzInput """

    begin = serializers.FloatField(required=False)
    duration = serializers.FloatField(required=False)
    pars = VleParCompactSerializer(many=True, required=False, read_only=False)
    conds = VleCondCompactSerializer(many=True, required=False,
                                     read_only=False)
    views = VleViewCompactSerializer(many=True, required=False,
                                     read_only=False)
    outs = VleOutCompactSerializer(many=True, required=False, read_only=False)

    def create(self, validated_data):
        return VpzInputCompact(**validated_data)

    def update(self, instance, validated_data):
        """Updates an existing VpzInputCompact instance from a dictionary of \
           deserialized field values"""

        if 'begin' in validated_data.keys() :
            instance.begin = validated_data['begin']
        if 'duration' in validated_data.keys() :
            instance.duration = validated_data['duration']
        if 'pars' in validated_data.keys() :
            pars = validated_data['pars']
            for par in pars :
                instance.pars.append(VleParCompact(cname=par['cname'],
                    pname=par['pname'], value=par['value'],
                    selection_name=par['selection_name']))
        return instance

    def save_obj(self, vpzinputcompact, vpzinput):
        """Restores and saves VpzInput according to VpzInputCompact

        Updates values of VpzInput 'attached' objects (VleBegin, VleDuration,
        all the VlePar given into 'pars') and saves into the database.
        """

        if vpzinputcompact.begin is not None :
            vpzinput.vlebegin.value = vpzinputcompact.begin
            vpzinput.vlebegin.save()
        if vpzinputcompact.duration is not None :
            vpzinput.vleduration.value = vpzinputcompact.duration
            vpzinput.vleduration.save()
        vpzinput.update_vlepar_values_and_save(
                vleparcompact_list=vpzinputcompact.pars)

