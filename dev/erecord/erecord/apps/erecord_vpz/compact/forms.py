"""erecord_vpz.compact.forms

vpz forms relative to compact style presentation

"""

from django import forms

from erecord_cmn.forms import ReadonlyForm

from erecord_vpz.compact.models import VleParCompact
from erecord_vpz.compact.models import VleCondCompact
from erecord_vpz.compact.models import VleViewCompact
from erecord_vpz.compact.models import VleOutCompact


class VleParCompactUserForm(ReadonlyForm):
    """VleParCompact form, with all fields readonly"""

    class Meta:
        model = VleParCompact
        fields = ['selection_name', 'pname', 'cname', 'value']


class VpzInputCompactUserForm(forms.Form):
    """VpzInputCompact form, with all fields readonly"""

    begin = forms.FloatField()
    duration = forms.FloatField()
    #pars

    def __init__(self, *args, **kwargs):
        super(VpzInputCompactUserForm, self).__init__(*args, **kwargs)
        # read-only fields
        for (n,f) in self.fields.items() :
            f.widget=forms.Textarea(attrs={'rows':1, 'cols':30})
            f.widget.attrs['readonly'] = True


class VleCondCompactUserForm(ReadonlyForm):
    """VleCondCompact form, with all fields readonly"""

    class Meta:
        model = VleCondCompact
        fields = ['selection_name',]

class VleViewCompactUserForm(ReadonlyForm):
    """VleViewCompact form, with all fields readonly"""

    class Meta:
        model = VleViewCompact
        fields = ['selection_name',]

class VleOutCompactUserForm(ReadonlyForm):
    """VleOutCompact form, with all fields readonly"""

    class Meta:
        model = VleOutCompact
        fields = ['selection_name',]

