"""erecord_vpz.serializers """

from rest_framework import serializers

from erecord_vpz.models import VpzAct
from erecord_vpz.models import VpzOrigin
from erecord_vpz.models import VpzInput
from erecord_vpz.models import VpzOutput
from erecord_vpz.models import VpzPath
from erecord_vpz.models import VleBegin
from erecord_vpz.models import VleDuration
from erecord_vpz.models import VleCond
from erecord_vpz.models import VlePar
from erecord_vpz.models import VleView
from erecord_vpz.models import VleOut
from erecord_vpz.models import VpzWorkspace

#------------------------------------------------------------------------------

class VpzSelectionOptionsSerializer(serializers.Serializer):
    """Id options from which to create a VpzAct
    
    The options information to select a VpzPath (option 'vpzpath') or a
    VleVpz (option 'vpz'). One and only one option must remain (priority to
    'vpzpath').
    """
    vpzpath = serializers.IntegerField( required=False )
    vpz = serializers.IntegerField( required=False )

    def validate(self, attrs):
        for k in ('vpzpath', 'vpz') :
            if k not in attrs.keys() :
                attrs[k]=None
        if attrs['vpzpath'] is not None :
            if attrs['vpz'] is not None :
                attrs['vpz']=None
        return attrs

#------------------------------------------------------------------------------

class VleBeginSerializerLink( serializers.ModelSerializer ):
    vpzinput = serializers.HyperlinkedRelatedField(read_only=True,
                                  view_name='erecord_vpz-vpzinput-detail')
    class Meta:
        model = VleBegin
        fields= ['id', 'verbose_name', 'value', 'vpzinput',]

class VleDurationSerializerLink( serializers.ModelSerializer ):
    vpzinput = serializers.HyperlinkedRelatedField(read_only=True,
                                  view_name='erecord_vpz-vpzinput-detail')
    class Meta:
        model = VleDuration
        fields= ['id', 'verbose_name', 'value', 'vpzinput',]

class VleParSerializerLink( serializers.ModelSerializer ):
    vlecond = serializers.HyperlinkedRelatedField(read_only=True,
                                      view_name='erecord_vpz-cond-detail')
    class Meta:
        model = VlePar
        fields= ['id', 'verbose_name', 'value', 'type', 'pname', 'cname',
                 'selected', 'vlecond',]

class VleCondSerializerLink( serializers.ModelSerializer ):
    """Among all the VlePar, only the selected ones are serialized """

    vpzinput = serializers.HyperlinkedRelatedField(read_only=True,
                                  view_name='erecord_vpz-vpzinput-detail')
    vlepar_list = serializers.HyperlinkedRelatedField(
                       source='get_vlepar_selected', many=True,
                       read_only=True, view_name='erecord_vpz-par-detail')
    class Meta:
        model = VleCond
        fields= ['id', 'name', 'verbose_name', 'vlepar_list', 'vpzinput',]

class VleOutSerializerLink( serializers.ModelSerializer ):
    vleview = serializers.HyperlinkedRelatedField(read_only=True,
                                      view_name='erecord_vpz-view-detail')
    class Meta:
        model = VleOut
        fields= ['id', 'verbose_name', 'shortname', 'oname', 'vname', 
                 'selected', 'vleview',]

class VleViewSerializerLink( serializers.ModelSerializer ):
    """
    Among all the VleOut, only the selected ones are serialized
    """

    vpzinput = serializers.HyperlinkedRelatedField(read_only=True,
                                  view_name='erecord_vpz-vpzinput-detail')
    vleout_list = serializers.HyperlinkedRelatedField(
                       source='get_vleout_selected', many=True,
                       read_only=True, view_name='erecord_vpz-out-detail')
    class Meta:
        model = VleView
        fields= ['id', 'name', 'verbose_name', 'type', 'timestep', 
                'output_name', 'output_plugin', 'output_format', 
                'output_location', 'vleout_list', 'vpzinput',]

class VpzWorkspaceSerializerLink( serializers.ModelSerializer ):
    vpzact = serializers.HyperlinkedRelatedField(read_only=True,
                                     view_name='erecord_vpz-vpzact-detail')
    class Meta:
        model = VpzWorkspace
        fields= ['id', 'vpzact', 'verbose_name', 'homepath',]

class VpzOriginSerializerLink( serializers.ModelSerializer ):
    vpzact = serializers.HyperlinkedRelatedField(read_only=True,
                                    view_name='erecord_vpz-vpzact-detail')
    class Meta:
        model = VpzOrigin
        fields= ['id', 'text', 'vpzact',]

class VpzInputSerializerLink( serializers.ModelSerializer ):
    """Among all the VleCond, only the selected ones are serialized (ie \
    the ones having at least one selected VlePar).
    Among all the VleView, only the selected ones are serialized (ie \
    the ones having at least one selected VleOut)
    """

    vpzact = serializers.HyperlinkedRelatedField(read_only=True,
                                   view_name='erecord_vpz-vpzact-detail')
    vlebegin = serializers.HyperlinkedRelatedField(read_only = True,
                                   view_name='erecord_vpz-begin-detail')
    vleduration = serializers.HyperlinkedRelatedField(read_only = True,
                                   view_name='erecord_vpz-duration-detail')
    vlecond_list = serializers.HyperlinkedRelatedField(
                          source='get_vlecond_selected', many=True,
                          read_only=True, view_name='erecord_vpz-cond-detail')
    vleview_list = serializers.HyperlinkedRelatedField(
                          source='get_vleview_selected', many=True,
                          read_only=True, view_name='erecord_vpz-view-detail')
    class Meta:
        model = VpzInput
        fields= ['id', 'vpzact', 'vlebegin', 'vleduration', 'vlecond_list', 
                 'vleview_list',]

class VpzOutputSerializerLink( serializers.ModelSerializer ):
    vpzact = serializers.HyperlinkedRelatedField(
            read_only = True, # queryset = VpzAct.objects.all(),
            view_name='erecord_vpz-vpzact-detail')
    class Meta:
        model = VpzOutput
        fields= [ 'id', 'vpzact', 'res', 'plan', 'restype',]

class VpzPathSerializerLink( serializers.ModelSerializer ):
    class Meta:
        model = VpzPath
        fields= [ 'id', 'verbose_name', 'text', 'vpzpath',]

class VpzActSerializerLink( serializers.ModelSerializer ):
    vpzinput = serializers.HyperlinkedRelatedField(
            read_only = True, # queryset = VpzInput.objects.all(),
            view_name='erecord_vpz-vpzinput-detail')
    vpzoutput = serializers.HyperlinkedRelatedField(
            read_only = True, # queryset = VpzOutput.objects.all(),
            view_name='erecord_vpz-vpzoutput-detail')
    vpzorigin = serializers.HyperlinkedRelatedField(
            read_only = True, # queryset = VpzOrigin.objects.all(),
            view_name='erecord_vpz-vpzorigin-detail')
    vpzworkspace = serializers.HyperlinkedRelatedField(
            read_only = True, # queryset = VpzWorkspace.objects.all(),
            view_name='erecord_vpz-vpzworkspace-detail')
    class Meta:
        model = VpzAct
        fields= [ 'id', 'verbose_name', 'vpzname', 'pkgname', 'vlepath',
                'vleversion',
                'vpzinput', 'vpzoutput', 'vpzorigin', 'vpzworkspace',]

#------------------------------------------------------------------------------

class VleBeginSerializerTree( serializers.ModelSerializer ):
    class Meta:
        model = VleBegin
        fields= [ 'id', 'verbose_name', 'value', ] #'vpzinput',]
        depth = 3

class VleDurationSerializerTree( serializers.ModelSerializer ):
    class Meta:
        model = VleDuration
        fields= [ 'id', 'verbose_name', 'value',] # 'vpzinput',]
        depth = 3

class VleParSerializerTree( serializers.ModelSerializer ):

    class Meta:
        model = VlePar
        fields= [ 'id', 'verbose_name', 'value', 'type', 'pname', 'cname',
                'selected',] # 'vlecond',]
        depth = 3

class VleCondSerializerTree( serializers.ModelSerializer ):
    """Among all the VlePar, only the selected ones are serialized """

    vlepar_list = VleParSerializerTree(source='get_vlepar_selected', many=True)

    class Meta:
        model = VleCond
        fields= [ 'id', 'name', 'verbose_name', 'vlepar_list',] # 'vpzinput',]
        depth = 3

class VleOutSerializerTree( serializers.ModelSerializer ):
    class Meta:
        model = VleOut
        fields= ['id', 'verbose_name', 'shortname', 'oname', 'vname',
                'selected',] # 'vleview',]
        depth = 3

class VleViewSerializerTree( serializers.ModelSerializer ):
    """Among all the VleOut, only the selected ones are serialized """

    vleout_list = VleOutSerializerTree(source='get_vleout_selected', many=True)
    class Meta:
        model = VleView
        fields= ['id', 'name', 'verbose_name', 'type', 'timestep', 
                'output_name', 'output_plugin', 'output_format', 
                'output_location', 'vleout_list',]
        depth = 3

class VpzWorkspaceSerializerTree( serializers.ModelSerializer ):
    class Meta:
        model = VpzWorkspace
        fields= [ 'id', 'verbose_name', 'homepath', 'reporthome', 'datahome',]
        depth = 3

class VpzOriginSerializerTree( serializers.ModelSerializer ):
    class Meta:
        model = VpzOrigin
        fields= [ 'id', 'text',]
        depth = 3

class VpzInputSerializerTree( serializers.ModelSerializer ):
    """
    Among all the VleCond, only the selected ones are serialized (that is to \
    say the ones having at least one selected VlePar)
    Among all the VleView, only the selected ones are serialized (that is to \
    say the ones having at least one selected VleOut)
    """

    vlebegin = VleBeginSerializerTree()
    vleduration = VleDurationSerializerTree()
    vlecond_list = VleCondSerializerTree(source='get_vlecond_selected',
                                         many=True)
    vleview_list = VleViewSerializerTree(source='get_vleview_selected',
                                         many=True)

    class Meta:
        model = VpzInput
        fields= ['id', 'vlebegin', 'vleduration', 'vlecond_list', 
                 'vleview_list',]
        depth = 3

class VpzOutputSerializerTree( serializers.ModelSerializer ):
    class Meta:
        model = VpzOutput
        fields= ['id', 'res', 'plan', 'restype',]
        depth = 3

class VpzPathSerializerTree( serializers.ModelSerializer ):
    class Meta:
        model = VpzPath
        fields= ['id', 'verbose_name', 'text', 'vpzpath',]
        depth = 3

class VpzActSerializerTree( serializers.ModelSerializer ):
    vpzinput = VpzInputSerializerTree()
    vpzoutput = VpzOutputSerializerTree()
    vpzorigin = VpzOriginSerializerTree()
    vpzworkspace = VpzWorkspaceSerializerTree()
    class Meta:
        model = VpzAct
        fields= ['id', 'verbose_name', 'vpzname', 'pkgname', 'vlepath',
                 'vleversion', 'vpzinput', 'vpzoutput', 'vpzorigin',
                 'vpzworkspace',]
        depth = 3

#------------------------------------------------------------------------------

