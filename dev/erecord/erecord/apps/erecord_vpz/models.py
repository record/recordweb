"""erecord_vpz.models

Models of the erecord_vpz application

"""

import os
import json

from django.db import models
from django.core.exceptions import ValidationError
from django.db.models.signals import pre_delete

from erecord_cmn.configs.config import REPOSITORIES_HOME

from erecord_acs.models_mixins import LockableMixin
from erecord_acs.models_mixins import LockableVpzActMixin
from erecord_acs.models_mixins import LockableVpzInputMixin
from erecord_acs.models_mixins import LockableVleCondMixin
from erecord_acs.models_mixins import LockableVleViewMixin

from erecord_cmn.models_mixins import DefaultVerboseNameMixin
from erecord_cmn.models_mixins import DefaultTextMixin

from erecord_vpz.models_mixins.models_mixins import VpzPathMixin
from erecord_vpz.models_mixins.models_mixins import AttachedToVpzActMixin
from erecord_vpz.models_mixins.models_mixins import VpzActMixin
from erecord_vpz.models_mixins.models_mixins import VpzWorkspaceMixin
from erecord_vpz.models_mixins.models_mixins import VpzOriginMixin
from erecord_vpz.models_mixins.models_mixins import VpzInputMixin
from erecord_vpz.models_mixins.models_mixins import VpzOutputMixin
from erecord_vpz.models_mixins.transform import VpzInputTransformMixin
from erecord_vpz.models_mixins.transform import VpzOutputTransformMixin
from erecord_vpz.models_mixins.transform import VpzActTransformMixin

from erecord_vpz.models_mixins.workspace import InternalContainerWorkspace

from erecord_cmn.utils.dir_and_file import is_a_directory
from erecord_cmn.utils.container.location import contains_a_vlecontainer
from erecord_cmn.utils.container.location import get_vlerep_path
from erecord_cmn.utils.container.location import is_structured_as_vle_version_name
from erecord_cmn.utils.container.erecord_api import get_value_type

from erecord_cmn.utils.coding import byteify

ht_vpzact_vpzname = \
        "Name of the vpz file : path relative to 'exp' directory, with .vpz \
        extension (same as in vle)"
ht_vpzact_pkgname = \
        "the name of the vle package including the vpz file 'name'. The vpz \
        file must be physically installed under the vle package (under 'path' \
        directory)"
ht_vpzact_vlepath = \
        "Directory path where the vle package 'pkg' including the vpz file \
        'name' has been physically installed (directory structured as a \
        VLE_HOME directory)"
ht_vpzact_verbose_name = "Human-readable name of the vpz"

ht_vpzact_vleversion = "vle version name"

class VpzAct(LockableVpzActMixin, VpzActTransformMixin, VpzActMixin,
             DefaultVerboseNameMixin, models.Model):
    """Vpz activity

    An activity about/on a vpz file is one such as :

    - GET  vpz/input  : see (unmodified) input,
    - POST vpz/input  : modify input and see modified input,
    - GET  vpz/output : run (unmodified) input and see output,
    - POST vpz/output : modify input and run modified input and see output,
    - GET  vpz/inout  : idem GET vpz/output + see (unmodified) input,
    - POST vpz/inout  : idem POST vpz/output + see modified input,

    Attributes :

    - vpzname : vpz file name
    - pkgname : vle package name
    - vlepath : vle home path (models repository path)

    - vleversion : vle version name

    - verbose_name : human-readable name...

    - vpzorigin : relative VpzOrigin (1 VpzAct to 1 VpzOrigin relationship)
    - vpzinput : relative VpzInput (1 VpzAct to 1 VpzInput relationship) 
    - vpzoutput : relative VpzOutput (1 VpzAct to 1 VpzOutput relationship)

    - created_at, updated_at : date information for admin management

    """

    vpzname = models.CharField(max_length=200, blank=False,
                               help_text=ht_vpzact_vpzname)
    pkgname = models.CharField(max_length=200, blank=False,
                               help_text=ht_vpzact_pkgname)

    vlepath = models.FilePathField(path=REPOSITORIES_HOME, blank=False,
                                   help_text=ht_vpzact_vlepath)

    vleversion = models.CharField(max_length=200, blank=False,
                                  help_text=ht_vpzact_vleversion)

    verbose_name = models.CharField(max_length=200, blank=True,
                                    help_text=ht_vpzact_verbose_name)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def delete_if_old(self):
        """Deletes vpzact and its dependencies, if too old """

        if self.is_old() :
            self.delete()

    def clean(self):

        if not is_structured_as_vle_version_name(self.vleversion) :
            msg = "The vle version name ("+self.vleversion+") is not "
            msg = msg + "structured as vle-V.rev."
            raise ValidationError(msg)

        if not contains_a_vlecontainer(path=self.vlepath) :
            msg = "The path ("+self.vlepath+") does not contain any "
            msg = msg + "models repository container file."
            raise ValidationError(msg)

        icw = InternalContainerWorkspace(path=self.vlepath)
        all_vpz_list = icw.CONT_get_vpz_list()
        icw.clear()
        pkgname = self.pkgname
        vpzname = self.vpzname
        if pkgname not in all_vpz_list.keys() :
            msg = "'"+pkgname+ "' doesn't exist (as a vle package) "
            msg = msg + "into the models repository container file "
            msg = msg + "(under " + self.vlepath + ")"
            raise ValidationError(msg)
        elif vpzname not in all_vpz_list[pkgname] :
            msg = "'"+vpzname+"' doesn't exist as a vpz file of the "
            msg = msg + "'"+pkgname+"' vle package into the models repository "
            msg = msg + "container file (under " + self.vlepath + ")"
            raise ValidationError(msg)

        icw = InternalContainerWorkspace(path=self.vlepath)
        vle_version_name_value = icw.CONT_get_vle_version_name()
        icw.clear()
        if vle_version_name_value != self.vleversion :
            msg = "The vle version of the '" + self.name + \
                  "' models repository container file (under " +  \
                  self.vlepath + ") is not compatible with the vle " + \
                  "version (" + self.vleversion + ")."
            raise ValidationError(msg)

    def save(self, *args, **kwargs):
        vn = "An activity on vpz file '"+self.vpzname+"' (of '"+ \
                self.pkgname+"' of '"+ self.vlepath+"')"
        self.set_default_value_verbose_name(vn)
        super(VpzAct, self).save(*args, **kwargs)

    def __str__(self):
        return "Vpz activity id "+str(self.id)+", on vpz file '"+ \
                self.vpzname+"' (of '"+self.pkgname+"' of '"+ \
                self.vlepath+"' of '"+ self.vleversion+"')"

    class Meta:
        verbose_name = "vpz activity"
        verbose_name_plural = "vpz activities"


ht_vpzorigin_text = "text describing vpz origin"
class VpzOrigin(LockableMixin, DefaultTextMixin, VpzOriginMixin,
                AttachedToVpzActMixin, models.Model):
    """Vpz origin

    - vpzact : relative VpzAct (1 VpzAct to 1 VpzOrigin relationship) 
    - date (copy)

    - vlerep : relative VleRep (1 VpzOrigin to 1 VleRep relationship)
    - vlepkg : relative VlePkg (1 VpzOrigin to 1 VlePkg relationship)
    - vlevpz : relative VleVpz (1 VpzOrigin to 1 VleVpz relationship)

    """

    vpzact = models.OneToOneField(VpzAct, on_delete=models.CASCADE,
                                  related_name="vpzorigin", blank=False)

    text = models.CharField(max_length=2000, blank=True, default=None,
                            help_text=ht_vpzorigin_text)

    def clean(self):
        if self.text is None :
            self.set_undefined_value_text()

    def __str__(self):
        return "Vpz origin id "+str(self.id)

    class Meta:
        verbose_name = "vpz origin"
        verbose_name_plural = "vpz origins"

class VpzInput(LockableVpzInputMixin, VpzInputTransformMixin, VpzInputMixin,
               AttachedToVpzActMixin, models.Model):
    """Vpz input

    - vpzact : relative VpzAct (1 VpzAct to 1 VpzInput relationship) 

    - vlebegin : relative VleBegin (1 VpzInput to 1 VleBegin relationship)
    - vleduration : relative VleDuration \
      (1 VpzInput to 1 VleDuration relationship)
    - vlecond_list : list of relative VleCond \
      (many VleCond to 1 VpzInput relationship)
    - vleview_list : list of relative VleView \
      (many VleView to 1 VpzInput relationship)
    """

    vpzact = models.OneToOneField(VpzAct, on_delete=models.CASCADE,
                                  related_name="vpzinput", blank=False)

    def __str__(self):
        return "Vpz input id "+str(self.id)

    def add_content(self, jres, parselect=None) :
        """Reads VpzInput content (initial state) into jres according to \
           parselect and adds it into the database.

        Reads into jres the vle input information, taking into account 
        parselect and adds the relevant objects (VleBegin, VleDuration,
        VleCond..., VleView, VleOut) into the database.

        The VpzInput is attached to a VpzAct.

        output_plugin_choice that is used for VleView always values 'storage'.
        """

        output_plugin_choice='storage'

        keys = jres.keys()

        if "begin" in keys :
            begin = jres["begin"]
            if "value" in begin.keys() :
                vlebegin = VleBegin(vpzinput=self, value=begin["value"])
                vlebegin.save()

        if "duration" in keys :
            duration = jres["duration"]
            if "value" in duration.keys() :
                vleduration = VleDuration(vpzinput=self,
                                          value=duration["value"])
                vleduration.save()

        if "cond_list" in keys :
            cond_list = jres["cond_list"]
            cond_list = self.add_par_selected_values(cond_list, parselect)
            for cname in cond_list.keys() :
                vlecond = self.vlecond_list.create(name=cname)
                vlecond.save()
                vlecond.read_and_add_content(jcondition=cond_list[cname])

        if "view_list" in keys :
            view_list = jres["view_list"]
            for vname in view_list.keys() :

                jview = view_list[vname]
                type = jview["type"]                   #'type': 'timed'
                timestep = jview["timestep"]           #'timestep': 1.0
                output = jview["output"]
                output_name = output["name"]           #'name': 'view'
                output_plugin = output["plugin"]       #'plugin': 'file'
                #old output_format = output["format"]  #'format': 'local'
                output_format = "obsolete"
                output_location = output["location"]   #'location': ''

                vleview = self.vleview_list.create(name=vname, type=type, 
                    timestep=timestep, output_name=output_name, 
                    output_plugin=output_plugin, output_format=output_format, 
                    output_location=output_location)
                vleview.save()
                vleview.read_and_add_content(jview=jview)

        self.update_vleout_and_save(output_plugin_choice=output_plugin_choice)

    def update_vleout_and_save(self, output_plugin_choice='storage') :
        """Updates into the database the VleOut that are attached to \
           VpzInput, according to output_plugin_choice.

        Initialisation of all VleOut selected according to
        output_plugin_choice.

        A view activation operation depends on output_plugin_choice value.
        """

        for v in self.vleview_list.all() :
            if v.is_activated(output_plugin_choice) :
                for o in v.vleout_list.all() :
                    o.set_selected()
                    o.save()

    def update_vlepar_values_and_save(self, vleparcompact_list) :
        """Updates into the database VlePar values according to \
           vleparcompact_list (list of VleParCompact).
        """

        for par in vleparcompact_list :
            for c in self.vlecond_list.all() :
                for p in c.vlepar_list.all() :
                    if par.selection_name==p.build_selection_name():
                        p.value = par.value
                        p.save()

    def set_selected_all_vlepars(self):
        for c in self.vlecond_list.all() :
            for p in c.vlepar_list.all() :
                p.set_selected()
                p.save()

    def unset_selected_all_vlepars(self):
        for c in self.vlecond_list.all() :
            for p in c.vlepar_list.all() :
                p.unset_selected()
                p.save()

    @classmethod
    def add_par_selected_values(cls, cond_list, parselect) :
        """Adds into cond_list the 'selected' value, for each parameter, \
           according to parselect, and return the modified cond_list.

        (for more about parselect values, see ParselectOptionViewMixin)

        Rules to define selected values (True or False) : 

        - if parselect=None or \
          if "all" among the parselect list (priority given to this case) : \
          selected=True for all parameters of all conditions.

        For each condition condname :

        - if condname in parselect, then selected=True for all its parameters.
        - if at least one parameter relative to condname in parselect, then \
          selected=True for all its parameters in that case and \
          selected=False for all its other parameters.
        - else then selected=False for all its parameters.

        Note :
        If a condition name and at the same time (at least) one of its
        parameter name was given in parselect, there is no guarantee about
        which of both rules would be applied.
        """

        selected_value = False # default value
        if not parselect:
            selected_value = True
        elif "all" in parselect :
            selected_value = True

        # final or initial values 
        for condname in cond_list.keys() :
            par_list = cond_list[condname]['par_list']
            for pname in par_list.keys() :
                par_list[pname]['selected'] = selected_value

        if not selected_value :
            for condname in cond_list.keys() :
                par_list = cond_list[condname]['par_list']
                if condname in parselect :
                    for pname in par_list.keys() :
                        par_list[pname]['selected'] = True
                else :
                    cond_with_one_par_selected = False
                    for pname in par_list.keys() :
                        parname = VlePar.build_parameter_selection_name(
                                                cname=condname, pname=pname)
                        if parname in parselect :
                            cond_with_one_par_selected = True
                    if cond_with_one_par_selected :
                        for pname in par_list.keys() :
                            parname = VlePar.build_parameter_selection_name(
                                                cname=condname, pname=pname)
                            if parname in parselect :
                                par_list[pname]['selected'] = True
        return cond_list

    def update_vleout_and_save(self, output_plugin_choice='storage') :
        """Updates into the database the VleOut that are attached to \
           VpzInput, according to output_plugin_choice.

        Initialisation of all VleOut selected according to
        output_plugin_choice.

        A view activation operation depends on output_plugin_choice value.
        """

        # VleOut selected initialisation
        for v in self.vleview_list.all() :
            if v.is_activated(output_plugin_choice) :
                for o in v.vleout_list.all() :
                    o.set_selected()
                    o.save()

    def get_vlecond_selected(self):
        """Filter consisting in keeping only the selected VleCond
        
        A VleCond is selected if at least one of its VlePar is selected
        """

        id_list = [ c.id for c in self.vlecond_list.all() 
                if c.get_vlepar_selected() ]
        vlecond_selected_list = self.vlecond_list.filter(id__in=id_list)
        return vlecond_selected_list

    def get_vleview_selected(self): # n'est plus valable ???
        """Filter consisting in keeping only the selected VleView
        
        A VleView is selected if at least one of its VleOut is selected
        """

        id_list = [ v.id for v in self.vleview_list.all() 
                if v.get_vleout_selected() ]
        vleview_selected_list = self.vleview_list.filter(id__in=id_list)
        return vleview_selected_list

    def is_without_any_view_in_storage_mode(self):
        """Returns if it has or not any VleView in storage mode """

        for v in self.vleview_list.all() :
            if v.is_storage_mode() :
                return False
        return True

    class Meta:
        verbose_name = "vpz input"
        verbose_name_plural = "vpz inputs"


ht_vpzoutput_res = "Simulation numerical result"
ht_vpzoutput_plan = "Kind of plan of the simulation"
ht_vpzoutput_restype = "Kind of restype of the simulation"
class VpzOutput(LockableMixin, VpzOutputTransformMixin,
                VpzOutputMixin, AttachedToVpzActMixin, models.Model):
    """Vpz output

    - vpzact : relative VpzAct (1 VpzAct to 1 VpzOutput relationship) 

    - res : simulation numerical result
    - plan : kind of plan of the simulation (single or linear)
    - restype : kind of restype of the simulation (dataframe or matrix)

    """

    vpzact = models.OneToOneField(VpzAct, on_delete=models.CASCADE,
                                  related_name="vpzoutput")

    res = models.CharField(max_length=1000000000, blank=True, default="",
            help_text=ht_vpzoutput_res )
    plan = models.CharField(max_length=10, blank=True, default="",
            help_text=ht_vpzoutput_plan )
    restype = models.CharField(max_length=10, blank=True, default="",
            help_text=ht_vpzoutput_restype )

    def add_content(self, jres) :
        """Reads VpzOutput content and adds it into the database.

        Reads into jres the vle output information and adds the relevant 
        objects (VleView...) into the database.

        The VpzOutput is attached to a VpzAct.
        """

        self.res = None # default
        res = None

        keys = jres.keys()
        if "plan" in keys :
            plan = jres["plan"]
        if "restype" in keys :
            restype = jres["restype"]
        if "res" in keys :
            res = jres["res"]

        if type(res) is str : # error case
            msg = "Simulation running error ... " + res
            raise Exception(msg)

        # empty cases
        if res is None : # single case
            res = dict()
        if type(res) is list : # linear case
            if res == [None] :
                res = list()

        self.res = res
        self.plan = plan
        self.restype = restype
        self.res = json.dumps(self.res)
        self.save()

    def __str__(self):
        return "Vpz output id "+str(self.id)

    class Meta:
        verbose_name = "vpz output"
        verbose_name_plural = "vpz outputs"

ht_vpzpath_vpzpath = "Vpz file pseudo-absolute path"
ht_vpzpath_verbose_name = "Human-readable name of the vpz path"
ht_vpzpath_text = "free text (describing the vpz path...)"

class VpzPath(LockableMixin, DefaultVerboseNameMixin, DefaultTextMixin,
              VpzPathMixin, models.Model):
    """Vpz path

    A VpzPath corresponds with a vpz file pseudo absolute path (that gives 
    information about the vpz file name, its vle package, its models
    repository container file).

    Attributes :

    - verbose_name : human-readable name
    - text : free text
    - vpzpath : vpz file pseudo absolute path as \
                vleversionname/repname/pkgname/vpzname
    """

    verbose_name = models.CharField(max_length=200, blank=True,
                                    help_text=ht_vpzpath_verbose_name)

    text = models.CharField(max_length=10000, blank=True, default="",
                            help_text=ht_vpzpath_text )

    vpzpath = models.FilePathField(path=REPOSITORIES_HOME, blank=False,
                                   help_text=ht_vpzpath_vpzpath)

    def clean(self):

        (vleversion, repname, pkgname, vpzname) = self.decompose()
        vlepath = get_vlerep_path(vleversion, repname)

        if not is_a_directory(path=vlepath) :
            msg = "The path ("+vlepath+") does not exist as a directory."
            raise ValidationError(msg)

        if not contains_a_vlecontainer(path=vlepath) :
            msg = "The path ("+vlepath+") does not contain any "
            msg = msg + "models repository container file."
            raise ValidationError(msg)

        if not is_structured_as_vle_version_name(vleversion) :
            msg = "The vle version name ("+vleversion+") is not "
            msg = msg + "structured as vle-V.rev."
            raise ValidationError(msg)

        icw = InternalContainerWorkspace(path=vlepath)
        vle_version_name_value = icw.CONT_get_vle_version_name()
        icw.clear()
        if vle_version_name_value != vleversion :
            msg = "The vle version of the '" + repname + \
                  "' models repository container file (under " +  \
                  vlepath + ") is not compatible with the vle version " + \
                  "name (" + vleversion + ")."
            raise ValidationError(msg)

        icw = InternalContainerWorkspace(path=vlepath)
        all_vpz_list = icw.CONT_get_vpz_list()
        icw.clear()
        if pkgname not in all_vpz_list.keys() :
            msg = "'"+pkgname+ "' doesn't exist (as a vle package) "
            msg = msg + "into the models repository container file "
            msg = msg + "(under " + vlepath + ")"
            raise ValidationError(msg)
        elif vpzname not in all_vpz_list[pkgname] :
            msg = "'"+vpzname+"' doesn't exist as a vpz file of the "
            msg = msg + "'"+pkgname+"' vle package into the models repository "
            msg = msg + "container file (under " + vlepath + ")"
            raise ValidationError(msg)

        if self.text is None :
            self.set_undefined_value_text()

    def save(self, *args, **kwargs):
        vn = "A VpzPath ('"+str(self.vpzpath)+"')"
        self.set_default_value_verbose_name(vn)
        super(VpzPath, self).save(*args, **kwargs)

    def __str__(self):
        return "Vpz path id "+str(self.id)

    class Meta:
        verbose_name = "vpz path"
        verbose_name_plural = "vpz paths"


ht_vlebegin_verbose_name = "Human-readable name of the begin " + \
        "(optional, default value : 'begin')"
ht_vlebegin_value = "Begin value"
class VleBegin(LockableMixin, DefaultVerboseNameMixin, models.Model):
    """Vle begin

    Attributes :

    - verbose_name : human-readable name
    - value : value (in vle, begin value)
    - vpzinput : relative VpzInput (1 VleBegin to 1 VpzInput relationship) 
    """

    verbose_name = models.CharField(max_length=200, blank=True,
                          default='begin', help_text=ht_vlebegin_verbose_name)
    value = models.FloatField( blank=False, help_text=ht_vlebegin_value)

    vpzinput = models.OneToOneField(VpzInput, on_delete=models.CASCADE,
                                    related_name="vlebegin",
                                    verbose_name="related vpz input",
                                    blank=False)

    def save(self, *args, **kwargs):
        self.set_default_value_verbose_name()
        super(VleBegin, self).save(*args, **kwargs)

    def __str__(self):
        return "Begin id "+str(self.id)
                
    class Meta:
        verbose_name = "vle begin"
        verbose_name_plural = "vle begins"


ht_vleduration_verbose_name = "Human-readable name of the duration " + \
        "(optional, default value : 'duration')"
ht_vleduration_value = "Duration value"
class VleDuration(LockableMixin, DefaultVerboseNameMixin,
                  models.Model):
    """Vle duration

    Attributes :

    - verbose_name : human-readable name
    - value : value (in vle, duration value)
    - vpzinput : relative VpzInput (1 VleDuration to 1 VpzInput relationship) 
    """

    verbose_name = models.CharField(max_length=200, blank=True,
                   default='duration', help_text=ht_vleduration_verbose_name)
    value = models.FloatField( blank=False, help_text=ht_vleduration_value)

    vpzinput = models.OneToOneField(VpzInput, on_delete=models.CASCADE,
                                    related_name="vleduration",
                                    verbose_name="related vpz input",
                                    blank=False )

    def save(self, *args, **kwargs):
        self.set_default_value_verbose_name()
        super(VleDuration, self).save(*args, **kwargs)

    def __str__(self):
        return "Duration id "+str(self.id)
                
    class Meta:
        verbose_name = "vle duration"
        verbose_name_plural = "vle durations"


ht_vlecond_name = "Condition name (same as in vle)"
ht_vlecond_verbose_name = "Human-readable name of the condition " + \
        "(optional, default value : name)"
ht_vlecond_vpzinput = "the vpz input to which the vle condition belongs"
class VleCond(LockableVleCondMixin, DefaultVerboseNameMixin, models.Model):
    """Vle condition

    - verbose_name : human-readable name
    - name : name (in vle, condition name)
    - vlepar_list : list of relative VlePar \
      (many VlePar to 1 VleCond relationship)
    - vpzinput : relative VpzInput (many VleCond to 1 VpzInput relationship) 
    """

    name = models.CharField(max_length=200, blank=False,
                            help_text=ht_vlecond_name)
    verbose_name = models.CharField(max_length=200, blank=True,
                                    help_text=ht_vlecond_verbose_name)

    vpzinput = models.ForeignKey(VpzInput, on_delete=models.CASCADE,
            related_name="vlecond_list", verbose_name="related vpz input",
            blank=False, help_text=ht_vlecond_vpzinput)

    def get_vlepar_selected(self):
        """Filter consisting in keeping only the selected VlePar """

        vlepar_selected_list = self.vlepar_list.all().filter(selected=True)
        return vlepar_selected_list

    def save(self, *args, **kwargs):
        vn = "Condition '"+self.name+"'" + \
               " of vpz file '"+self.vpzinput.vpzact.vpzname+"'"
        self.set_default_value_verbose_name(vn)
        super(VleCond, self).save(*args, **kwargs)

    def __str__(self):
        return "Condition id "+str(self.id)+", '"+self.name+"'" + \
               " of vpz file '"+self.vpzinput.vpzact.vpzname+"'"

    def read_and_add_content(self, jcondition=None) :
        """Reads VleCond content and adds it into the database

        Reads into jcondition the vle condition information and adds the
        relevant objects (VlePar) into the database.
        The VleCond is attached to a VpzInput that is attached to a VpzAct.

        Note :
        Uses json result based on read_vpz simulator of erecord package.
        More exactly : jcondition is the condition (from json result)
        excepted that its contains selected value for each parameter of
        the condition
        """

        if jcondition is not None :
            if "par_list" in jcondition.keys() :
                par_list = jcondition["par_list"]
                for pname in par_list.keys() :
                    parameter = par_list[pname]
                    parameter_type = get_value_type(parameter["type"])
                    parameter_value = parameter["value"]
                    parameter_selected = parameter["selected"]
                    self.vlepar_list.create(pname=pname, cname=self.name, 
                       type=parameter_type, value=json.dumps(parameter_value),
                       selected=parameter_selected)

    def build_selection_name(self) :
        return (self.name)

    class Meta:
        verbose_name = "vle condition"
        verbose_name_plural = "vle conditions"


ht_vlepar_pname = "Name of the vle parameter (same as in vle)"
ht_vlepar_cname = "Name of the vle parameter condition (same as in vle)"
ht_vlepar_verbose_name = "Human-readable name of the vle parameter " + \
        "(optional, default value : name)"
ht_vlepar_type = "Type of the vle parameter (...of the first value)"
ht_vlepar_value = "Value of the vle parameter (...charfield for the moment)"
ht_vlepar_selected = "Selected or not for restitution"
ht_vlepar_vlecond = "the vle condition to which the vle parameter belongs"
class VlePar(LockableMixin, DefaultVerboseNameMixin, models.Model):
    """Vle parameter (of a VleCond)

    Attributes :

    - verbose_name : human-readable name
    - pname : parameter name (in vle, port name)
    - cname : condition name (in vle, condition name)
    - type : value type (in vle, port value type)
    - value : value (in vle, port value)
    - selected : selection or not of the parameter for restitution
    - vlecond : relative VleCond (many VlePar to 1 VleCond relationship)
    """

    pname = models.CharField(max_length=200, blank=False,
                             help_text=ht_vlepar_pname)
    cname = models.CharField(max_length=200, blank=False,
                             help_text=ht_vlepar_cname)
    verbose_name = models.CharField(max_length=200, blank=True,
                                    help_text=ht_vlepar_verbose_name)
    type = models.CharField(max_length=200, blank=False,
                            help_text=ht_vlepar_type)
    value = models.CharField(max_length=1000000, blank=False,
                             help_text=ht_vlepar_value)
    selected = models.BooleanField(blank=False, help_text=ht_vlepar_selected,
                                   default=False)
    vlecond = models.ForeignKey(VleCond, on_delete=models.CASCADE,
            related_name="vlepar_list", verbose_name="related vle condition",
            blank=False, help_text=ht_vlepar_vlecond)

    def save(self, *args, **kwargs):
        vn = "Parameter '"+self.pname+"' of condition '"+ \
                self.cname+"'" + " of vpz file '"+ \
                self.vlecond.vpzinput.vpzact.vpzname+"'" 
        self.set_default_value_verbose_name(vn)
        super(VlePar, self).save(*args, **kwargs)

    def __str__(self):
        return "parameter id "+str(self.id)+", '"+self.pname+ \
                "' of condition '"+self.cname+"'" + " of vpz file '"+ \
                self.vlecond.vpzinput.vpzact.vpzname+"'" 

    def is_selected(self) :
        return self.selected
    def set_selected(self) :
        self.selected = True
    def unset_selected(self) :
        self.selected = False

    @classmethod
    def build_parameter_selection_name(cls, cname, pname):
        """Defines and returns the selection name of a parameter.
        
        Attention : if the selection name did not value cname.pname anymore, 
        then get_vleparcompact_option_values code should be modified/adapted
        """
        return (cname+"."+pname)

    def build_selection_name(self) :
        """Defines and returns the selection name of a parameter.

        For consistency, must call build_parameter_selection_name, as
        get_vleparcompact_option_values
        """

        return self.build_parameter_selection_name(cname=self.cname,
                pname=self.pname)

    def get_val(self):
        return byteify(json.loads(self.value))

    class Meta:
        verbose_name = "vle parameter"
        verbose_name_plural = "vle parameters"

ht_vleview_name = "View name (same as in vle)"
ht_vleview_type = "View type (same as in vle)"
ht_vleview_timestep = "View timestep (same as in vle)"
ht_vleview_output_name = "View output name (same as in vle)"
ht_vleview_output_plugin = "View output plugin (same as in vle)"
ht_vleview_output_format = "View output format (same as in vle)"
ht_vleview_output_location = "View output location (same as in vle)"
ht_vleview_verbose_name = "Human-readable name of the view " + \
        "(optional, default value : name)"
ht_vleview_vpzinput = "the vpz input to which the vle view belongs"
class VleView(LockableVleViewMixin, DefaultVerboseNameMixin, models.Model):
    """Vle view

    Attributes :

    - verbose_name : human-readable name
    - name : name (in vle, view name)
    - type : type (in vle, view type)
    - timestep : time step (in vle, view time step)
    - output_name : output name (in vle, view output name)
    - output_plugin : output plugin (in vle, view output plugin)
    - output_format : output format (in vle, view output format)
    - output_location : output location (in vle, view output location)
    - vleout_list : list of relative VleOut \
      (many VleOut to 1 VleView relationship)
    - vpzinput : relative VpzInput (many VleView to 1 VpzInput relationship) 

    Only the Vle output datas (VleOut) that exist at the initial state will
    be taken into account (not the ones created during the simulation).
    """

    name = models.CharField(max_length=200, blank=False,
                            help_text=ht_vleview_name)
    type = models.CharField(max_length=200, blank=False,
                            help_text=ht_vleview_type)
    timestep = models.CharField(max_length=200, blank=False,
                                help_text=ht_vleview_timestep)
    output_name = models.CharField(max_length=200, blank=False,
                                   help_text=ht_vleview_output_name)
    output_plugin = models.CharField(max_length=200, blank=False,
                                     help_text=ht_vleview_output_plugin)
    output_format = models.CharField(max_length=200, blank=False,
                                     help_text=ht_vleview_output_format)
    output_location = models.CharField(max_length=200, blank=False,
                                       help_text=ht_vleview_output_location)
    verbose_name = models.CharField(max_length=200, blank=True,
                                    help_text=ht_vleview_verbose_name)
    vpzinput = models.ForeignKey(VpzInput, on_delete=models.CASCADE,
            related_name="vleview_list", verbose_name="related vpz input",
            blank=False, help_text=ht_vleview_vpzinput)

    def get_vleout_selected(self):
        """Filter consisting in keeping only the selected VleOut """

        vleout_selected_list = self.vleout_list.all().filter(selected=True)
        return vleout_selected_list

    def save(self, *args, **kwargs):
        vn = "View '"+self.name+"'" + \
             " of vpz file '"+self.vpzinput.vpzact.vpzname+"'"
        self.set_default_value_verbose_name(vn)
        super(VleView, self).save(*args, **kwargs)

    def __str__(self):
        return "View id "+str(self.id)+", '"+self.name+"'" + \
               " of vpz file '"+self.vpzinput.vpzact.vpzname+"'"

    def read_and_add_content(self, jview=None) :
        """Reads VleView content (initial state) and adds it into the database

        Reads into the vpz file (cf VpzAct) the vle view information
        (initial state) and adds the relevant objects (VleOut) into
        the database.
        The VleView is attached to a VpzInput that is attached to a VpzAct.

        Uses json result based on read_vpz simulator of erecord package.
        More exactly : jview is the view (from json result)
        """

        outputdata_name_list = [] # default
        if "out_list" in jview.keys() :
            outputdata_name_list = jview["out_list"]
        for oname in outputdata_name_list :
            l = oname.split(".")
            if len(l) == 2 :
                modelname = l[0]
                shortname = l[1]
            nameinres = oname  # nameinres=nameinvle now
            self.vleout_list.create(oname=nameinres, vname=self.name, 
                    shortname=shortname, selected=False)

    def is_storage_mode(self) :
        return (self.output_plugin == "storage")
    def is_file_mode(self) :
        return (self.output_plugin == "file")
    def is_activated(self, output_plugin_choice) :
        return (self.output_plugin == output_plugin_choice)
    def unactivate(self) :
        self.output_plugin = "dummy"
    def activate(self, output_plugin_choice):
        self.output_plugin = output_plugin_choice

    def build_selection_name(self) :
        return (self.name)

    class Meta:
        verbose_name = "vle view"
        verbose_name_plural = "vle views"


ht_vleout_oname = "Name of the vle output data (same as in vle)"
ht_vleout_vname = "Name of the vle output data view (same as in vle)"
ht_vleout_verbose_name = "Human-readable name of the vle output data " + \
        "(optional, default value : name)"
ht_vleout_shortname = "Shortname of the vle output data"
ht_vleout_selected = "Selected or not for retitution"
ht_vleout_vleview = "the vle view to which the vle output data belongs"
class VleOut(LockableMixin, DefaultVerboseNameMixin, models.Model):
    """Vle output data (of a VleView)

    Attributes :

    - verbose_name : human-readable name
    - oname : output data name (in vle, port name)
    - vname : view name (in vle, view name)
    - shortname : short name
    - selected : selection or not of the output data for restitution
    - vleview : relative VleView (many VleOut to 1 VleView relationship)
    """

    oname = models.CharField(max_length=200, blank=False,
                             help_text=ht_vleout_oname)
    vname = models.CharField(max_length=200, blank=False,
                             help_text=ht_vleout_vname)
    verbose_name = models.CharField(max_length=200, blank=True,
                                    help_text=ht_vleout_verbose_name)
    shortname = models.CharField(max_length=200, blank=False,
                                 help_text=ht_vleout_shortname)
    selected = models.BooleanField(blank=False, help_text=ht_vleout_selected,
                                   default=False)
    vleview = models.ForeignKey(VleView, on_delete=models.CASCADE,
            related_name="vleout_list", verbose_name="related vle view",
            blank=False, help_text=ht_vleout_vleview)

    def save(self, *args, **kwargs):
        vn = "Output data '"+self.oname+"' of view '"+ \
                self.vname+"'" + " of vpz file '"+ \
                self.vleview.vpzinput.vpzact.vpzname+"'" 
        self.set_default_value_verbose_name(vn)
        super(VleOut, self).save(*args, **kwargs)

    def __str__(self):
        return "output data id "+str(self.id)+", '"+self.oname+ \
                "' of view '"+self.vname+"'" + " of vpz file '"+ \
                self.vleview.vpzinput.vpzact.vpzname+"'" 

    def is_selected(self) :
        return self.selected
    def set_selected(self) :
        self.selected = True
    def unset_selected(self) :
        self.selected = False

    @classmethod
    def build_output_selection_name(cls, vname, oname):
        """Defines and returns the selection name of an output data """

        return (vname+"."+oname)

    def build_selection_name(self) :
        """Defines and returns the selection name of an output data """

        return self.build_output_selection_name(vname=self.vname,
                oname=self.oname)

    class Meta:
        verbose_name = "vle output data"
        verbose_name_plural = "vle output datas"

def vpzworkspace_clear(sender, instance, *args, **kwargs):
    """Deletes VpzWorkspace associated folders"""

    instance.clear_dirs()

ht_vpzworkspace_verbose_name = "Human-readable name of the vpz workspace"
ht_vpzworkspace_homepath = "absolute path of the workspace directory"
class VpzWorkspace(LockableMixin, DefaultVerboseNameMixin, VpzWorkspaceMixin,
                   models.Model):
    """Vpz workspace

    A VpzWorkspace is attached to a VpzAct. It defines a workspace
    directory dedicated to the Vpz activity/manipulation.

    Attributes :

    - verbose_name : human-readable name
    - vpzact : relative VpzAct (1 VpzAct to 1 VpzWorkspace relationship) 
    - homepath : absolute path
    - vlecontainerpath : path of the vle models repository container file
    - reporthome : report home path (defined if needed/used)
    - datahome : (sent) data home path (defined if needed/used)
    - runhome : run home path (defined if needed/used)
    """

    verbose_name = models.CharField(max_length=200, blank=True,
                                    help_text=ht_vpzworkspace_verbose_name)
    homepath = models.CharField(max_length=200, blank=False,
                                help_text=ht_vpzworkspace_homepath)
    vlecontainerpath = models.CharField(max_length=200, blank=True,
                                        default=None)
    reporthome = models.CharField(max_length=200, blank=True, default=None)
    datahome = models.CharField(max_length=200, blank=True, default=None)
    runhome = models.CharField(max_length=200, blank=True, default=None)
    vpzact = models.OneToOneField(VpzAct, on_delete=models.CASCADE,
                                  related_name="vpzworkspace", blank=False)

    @classmethod
    def create(cls, vpzact, as_reporthome=False, as_datahome=False,
               as_runhome=False):
        """Creates a VpzWorkspace attached to vpzact """

        vpzworkspace = cls(vpzact=vpzact)
        vpzworkspace.define_and_build(as_reporthome=as_reporthome,
                             as_datahome=as_datahome, as_runhome=as_runhome)
        return vpzworkspace

    def clean(self):
        """Verifies that path directories exist"""

        if not os.path.isdir(self.homepath) :
            msg = "unavailable homepath directory '"+self.homepath+"'"
            raise ValidationError(msg)

    def save(self, *args, **kwargs):
        self.set_default_value_verbose_name()
        super(VpzWorkspace, self).save(*args, **kwargs)

    def __str__(self):
        return "Vpz workspace id "+str(self.id)

    class Meta:
        verbose_name = "vpz workspace"
        verbose_name_plural = "vpz workspaces"

# done before a VpzWorkspace deletion
pre_delete.connect(vpzworkspace_clear, sender=VpzWorkspace)

