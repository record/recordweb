"""erecord_vpz.forms

Forms of the erecord_vpz application

"""

from django import forms

from erecord_cmn.forms import ReadonlyForm
from erecord_cmn.forms import TextAreasForm

from erecord_vpz.models import VpzAct
from erecord_vpz.models import VpzOrigin
from erecord_vpz.models import VpzInput
from erecord_vpz.models import VpzOutput
from erecord_vpz.models import VpzPath
from erecord_vpz.models import VleBegin
from erecord_vpz.models import VleDuration
from erecord_vpz.models import VleCond
from erecord_vpz.models import VlePar
from erecord_vpz.models import VleView
from erecord_vpz.models import VleOut
from erecord_vpz.models import VpzWorkspace

import os

from erecord_cmn.configs.config import REPOSITORIES_HOME

from erecord_cmn.utils.container.location import contains_a_vlecontainer
from erecord_vpz.models_mixins.workspace import InternalContainerWorkspace

from erecord_vpz.models import ht_vpzpath_vpzpath as mht_vpzpath_vpzpath


class VpzActForm(TextAreasForm):
    def __init__(self, *args, **kwargs):
        super(VpzActForm, self).__init__(*args, **kwargs)
        self.make_readonly('vpzname')
        self.make_readonly('pkgname')
        self.make_readonly('vlepath')
        self.make_readonly('vleversion')
    class Meta:
        model = VpzAct
        fields = '__all__'

#class VpzInputForm(TextAreasForm):

class VpzOutputForm(TextAreasForm):
    def __init__(self, *args, **kwargs):
        super(VpzOutputForm, self).__init__(*args, **kwargs)
        self.make_readonly('res')
        self.make_readonly('plan')
        self.make_readonly('restype')
    class Meta:
        model = VpzOutput
        fields = '__all__'

class VpzOriginForm(TextAreasForm):
    class Meta:
        model = VpzOrigin
        fields = '__all__'


ht_vpzpath_vpzpath = mht_vpzpath_vpzpath+ \
    " Choose a vpz file among the existing ones (under "+REPOSITORIES_HOME+")"

empty_choice = ("","---------")

def get_vpzpath_choices() :
    """Returns the list to choose a vpz file name.
    
    The vpz files taken into account are those found into one of the
    existing (installed) models repositories containers
    """

    choices_list = [ empty_choice ]

    vlecontainerpath_list = list()
    tmp = forms.FilePathField(path=REPOSITORIES_HOME, recursive=True,
            allow_files=False, allow_folders=True)
    for (path,c) in tmp.choices :
        if contains_a_vlecontainer(path=path) :
            icw = InternalContainerWorkspace(path=path)
            all_vpz_list = icw.CONT_get_vpz_list()
            icw.clear()
            for pkgname,vpz_list in all_vpz_list.items() :
                for vpzname in vpz_list :
                    vpzpathchoice = os.path.join(c, pkgname, vpzname)
                    choices_list.append( (vpzpathchoice, vpzpathchoice) )
    return choices_list

class VpzPathForm(forms.ModelForm):

    vpzpath = forms.ChoiceField(choices=get_vpzpath_choices(), 
                                required=True, help_text=ht_vpzpath_vpzpath)

    def __init__(self, *args, **kwargs):
        super(VpzPathForm, self).__init__(*args, **kwargs)
        for (n,f) in self.fields.items() :
            if n is "text" :
                f.widget=forms.Textarea(attrs={'rows':1, 'cols':100})

    class Meta:
        model = VpzPath
        fields = '__all__'

class VleBeginForm(TextAreasForm):
    def __init__(self, *args, **kwargs):
        super(VleBeginForm, self).__init__(*args, **kwargs)
        self.make_readonly('value')
    class Meta:
        model = VleBegin
        fields = '__all__'

class VleDurationForm(TextAreasForm):
    def __init__(self, *args, **kwargs):
        super(VleDurationForm, self).__init__(*args, **kwargs)
        self.make_readonly('value')
    class Meta:
        model = VleDuration
        fields = '__all__'

class VleCondForm(TextAreasForm):
    def __init__(self, *args, **kwargs):
        super(VleCondForm, self).__init__(*args, **kwargs)
        self.make_readonly('name')
    class Meta:
        model = VleCond
        fields = '__all__'

class VleParForm(TextAreasForm):
    def __init__(self, *args, **kwargs):
        super(VleParForm, self).__init__(*args, **kwargs)
        self.make_readonly('pname')
        self.make_readonly('cname')
        self.make_readonly('type')
        self.make_readonly('value')
        self.make_readonly('selected')
    class Meta:
        model = VlePar
        fields = '__all__'

class VleViewForm(TextAreasForm):
    def __init__(self, *args, **kwargs):
        super(VleViewForm, self).__init__(*args, **kwargs)
        self.make_readonly('name')
        self.make_readonly('type')
        self.make_readonly('timestep')
        self.make_readonly('output_name')
        self.make_readonly('output_plugin')
        self.make_readonly('output_format')
        self.make_readonly('output_location')
    class Meta:
        model = VleView
        fields = '__all__'

class VleOutForm(TextAreasForm):
    def __init__(self, *args, **kwargs):
        super(VleOutForm, self).__init__(*args, **kwargs)
        self.make_readonly('oname')
        self.make_readonly('vname')
        self.make_readonly('shortname')
        self.make_readonly('selected')
    class Meta:
        model = VleOut
        fields = '__all__'

class VpzWorkspaceForm(TextAreasForm):
    def __init__(self, *args, **kwargs):
        super(VpzWorkspaceForm, self).__init__(*args, **kwargs)
        self.make_readonly('homepath')
        self.make_readonly('vlecontainerpath')
        self.make_readonly('reporthome')
        self.make_readonly('datahome')
    class Meta:
        model = VpzWorkspace
        fields = '__all__'

#------------------------------------------------------------------------------

class VpzActUserForm(ReadonlyForm):
    class Meta:
        model = VpzAct
        fields = '__all__'

class VpzInputUserForm(ReadonlyForm):
    class Meta:
        model = VpzInput
        fields = '__all__'

class VpzOutputUserForm(ReadonlyForm):
    class Meta:
        model = VpzOutput
        fields = '__all__'

class VpzOriginUserForm(ReadonlyForm):
    class Meta:
        model = VpzOrigin
        fields = '__all__'

class VpzPathUserForm(ReadonlyForm):
    class Meta:
        model = VpzPath
        fields = '__all__'

class VleBeginUserForm(ReadonlyForm):
    class Meta:
        model = VleBegin
        fields= [ 'verbose_name', 'value', ]

class VleDurationUserForm(ReadonlyForm):
    class Meta:
        model = VleDuration
        fields= [ 'verbose_name', 'value', ]

class VleCondUserForm(ReadonlyForm):
    class Meta:
        model = VleCond
        fields= [ 'verbose_name', 'name',]

class VleParUserForm(ReadonlyForm):
    class Meta:
        model = VlePar
        fields= [ 'verbose_name', 'value', 'pname', 'cname',]

class VleViewUserForm(ReadonlyForm):
    class Meta:
        model = VleView
        fields= ['verbose_name', 'name', 'type', 'timestep', 
                'output_name', 'output_plugin', 'output_format', 
                'output_location',]

class VleOutUserForm(ReadonlyForm):
    class Meta:
        model = VleOut
        fields= ['verbose_name', 'shortname', 'oname', 'vname',]

class VpzWorkspaceUserForm(ReadonlyForm):
    class Meta:
        model = VpzWorkspace
        fields = '__all__'

