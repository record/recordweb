"""erecord_vpz.models_mixins.models_mixins

Mixins for erecord_vpz model

"""

import os
import json
import datetime

from erecord_cmn.configs.config import REQUESTS_HOME_APP_VPZ

from erecord_cmn.utils.container.location import get_vlerep_path

from erecord_vpz.models_mixins.workspace import ContainerWorkspaceMixin
from erecord_vpz.models_mixins.workspace import RunhomeWorkspaceMixin
from erecord_vpz.models_mixins.workspace import DatahomeWorkspaceMixin
from erecord_vpz.models_mixins.workspace import ReporthomeWorkspaceMixin
from erecord_vpz.models_mixins.workspace import WorkspaceMixin

from erecord_cmn.utils.dir_and_file import delete_path_if_present
from erecord_cmn.utils.dir_and_file import clean_dir
from erecord_cmn.utils.dir_and_file import get_available_pathname
from erecord_cmn.configs.config import ACTIVITY_LIFETIME

from erecord_cmn.utils.logger import get_logger
LOGGER = get_logger(__name__)

# Some mixins for the erecord_vpz models

class VpzPathMixin(object):
    """Additional methods for VpzPath """

    def decompose(self) :
        """Extracts vleversion, repname, pkgname, vpzname from vpzpath """

        vpzname = os.path.basename(self.vpzpath)
        path = os.path.dirname(self.vpzpath)
        pkgname = os.path.basename(path)
        path = os.path.dirname(path)
        repname = os.path.basename(path)
        path = os.path.dirname(path)
        vleversion = os.path.basename(path)
        return (vleversion, repname, pkgname, vpzname)

    def for_vpzact_creation(self):
        """Prepares the creation of a VpzAct from a VpzPath """

        res = None

        try :
            (vleversion, repname, pkgname, vpzname) = self.decompose()
            vlepath = get_vlerep_path(vleversion, repname)
        except :
            msg = "unavailable vpzpath '"+self.vpzpath+"'"
            raise Exception(msg)

        now = datetime.datetime.now()

        text = "Vpz activity (VpzAct) created from VpzPath. "
        text = text+ "VpzPath id : " + str(self.id) + ", "
        text = text+ "vpzpath : " + self.vpzpath + ". "
        text = text+ "vle version : " + vleversion + ". "
        text = text+ "Time:" + str(now) + "."

        res = { 'vlepath':vlepath, 'pkgname': pkgname, 'vpzname': vpzname,
                'vleversion': vleversion, 'textorigin': text, }
        return res

class AttachedToVpzActMixin(object):
    """Additional methods for a model attached to a VpzAct """

    @classmethod
    def create(cls, vpzact, **kwargs):

        """Creates the object attached to vpzact """
        obj = cls(vpzact=vpzact, **kwargs)
        return obj

class VpzOriginMixin(object):
    """Additional methods for VpzOrigin """
    pass

class VpzWorkspaceMixin(RunhomeWorkspaceMixin, DatahomeWorkspaceMixin,
           ReporthomeWorkspaceMixin, ContainerWorkspaceMixin, WorkspaceMixin):
    """Additional methods for VpzWorkspace """

    def define_and_build(self, as_reporthome=False, as_datahome=False,
                         as_runhome=False):
        """ Defines and builds a VpzWorkspace
        
        Chooses/defines fields values (homepath, vlecontainerpath, 
        reporthome, datahome, runhome) and creates the required directories
        (at least homepath).

        A workspace concerns vlecontainerpath.

        A workspace may concern reporthome, datahome, runhome.
        Those subdirectories will there be created/defined or not depending
        on as_reporthome, as_datahome, as_runhome.
        """

        self.homepath = self.get_undefined_value()
        self.vlecontainerpath = self.get_undefined_value()
        self.reporthome = self.get_undefined_value()
        self.datahome = self.get_undefined_value()
        self.runhome = self.get_undefined_value()

        homepath = get_available_pathname(rootpath=REQUESTS_HOME_APP_VPZ,
                rootname="user_")
        (deletion_done, creation_done) = clean_dir(dirpath=homepath)
        if deletion_done :
            LOGGER.error("%s has been deleted before to be recreated, \
                          whereas it was supposed not to exist", homepath)
        self.homepath = homepath

        self.datahome = self.init_datahome(LOGGER=LOGGER, 
                                           as_datahome=as_datahome)
        self.reporthome = self.init_reporthome(LOGGER=LOGGER, 
                                               as_reporthome=as_reporthome)
        self.runhome = self.init_runhome(LOGGER=LOGGER, as_runhome=as_runhome)
        self.vlecontainerpath = self.define_vlecontainerpath()

    def get_homepath(self):
        return self.homepath

    def clean_workspace(self):
        clean_dir(dirpath=self.homepath)

    def clear_workspace(self):
        delete_path_if_present(path=self.homepath)

    def clear_dirs(self):
        self.clear_reporthome()
        self.clear_datahome()
        self.clear_runhome()
        self.clear_workspace()

class VpzActMixin(object):
    """Additional methods for VpzAct """

    def is_old(self):
        """vpzact is old if created_at 'older' than ACTIVITY_LIFETIME """

        d = self.created_at + datetime.timedelta(days=ACTIVITY_LIFETIME)
        now = datetime.datetime.now()
        if d.toordinal() < now.toordinal() :
           return True
        else :
           return False

class VpzInputMixin(object):
    """Additional methods for VpzInput """
    pass 

class VpzOutputMixin(object):
    """additional methods for VpzOutput """

    @classmethod
    def build_output_outname(cls, oname):
        """Builds and returns the outname value of an output data from its
        oname value.

        This method is used to interpret the res content of VpzOutput in \
        relation with the VleOut denominations (vname, oname, selection_name).

        oname is the oname value of the output data as a VleOut.

        outname comes from the result produced by a vle simulation (cf res \
        of VpzOutput).

        Identity method since oname equals outname.
        """

        return oname

    @classmethod
    def build_output_oname(cls, outname):
        """Builds and returns the oname value of an output data from its
        outname value.

        This method is used to interpret the res content of VpzOutput in \
        relation with the VleOut denominations (vname, oname, selection_name).

        oname is the oname value of the output data as a VleOut.

        outname comes from the result produced by a vle simulation (cf res \
        of VpzOutput).

        Identity method since oname equals outname.
        """

        return outname

    def format_res_ok(self):
        """ Verifies the format of res according to plan and restype.

        Returns True or False.
        """

        def format_ok_dataframe_single(res):
            format_ok = True
            if not isinstance(res, dict) :
                format_ok = False
            else :
                for view in res.values():
                    if not isinstance(view, dict) :
                        format_ok = False
                    else :
                        for out in view.values() :
                            if not isinstance(out, list) :
                                format_ok = False
            return format_ok

        def format_ok_matrix_single(res):
            format_ok = True
            if not isinstance(res, dict) :
                format_ok = False
            else :
                for view in res.values():
                    if not isinstance(view, list) :
                        format_ok = False
                    else :
                        for out in view :
                            if not isinstance(out, list) :
                                format_ok = False
            return format_ok

        res = json.loads(self.res)
        if self.restype == 'dataframe' :
            if self.plan == 'single' :
                format_ok = format_ok_dataframe_single(res)
            elif self.plan == 'linear' :
                format_ok = True
                if not isinstance(res, list) :
                    format_ok = False
                else :
                    for r in res:
                        if not format_ok_dataframe_single(r):
                            format_ok = False
        elif self.restype == 'matrix' :
            if self.plan == 'single' :
                format_ok = format_ok_matrix_single(res)
            elif self.plan == 'linear' :
                format_ok = True
                if not isinstance(res, list) :
                    format_ok = False
                else :
                    for r in res:
                        if not format_ok_matrix_single(r):
                            format_ok = False
        return format_ok

    def print_res(self):

        print("--- self.res : ")
        if (self.plan=='single') and (self.restype=='dataframe'):
            for viewname,viewvalue in self.res.items() :
                print(viewname, " : ")
                for outname,outvalue in viewvalue.items() :
                    print("   ", outname, " : ", outvalue)
        elif (self.plan=='linear') and (self.restype=='dataframe'):
            for m,mv in enumerate(list(self.res)) :
                print("(", m, ") :")
                for viewname,viewvalue in mv.items() :
                    print(viewname, " : ")
                    for outname,outvalue in viewvalue.items() :
                        print("   ", outname, " : ", outvalue)
        if (self.plan=='single') and (self.restype=='matrix'):
            for viewname,viewvalue in self.res.items() :
                print(viewname, " : ")
                #print(viewvalue)
                for out in viewvalue :
                    print("   ", out)
        if (self.plan=='linear') and (self.restype=='matrix'):
            for m,mv in enumerate(list(self.res)) :
                print("(", m, ") :")
                for viewname,viewvalue in mv.items() :
                    print(viewname, " : ")
                    #print(viewvalue)
                    for out in viewvalue :
                        print("   ", out)
        print("---")

