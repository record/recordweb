"""erecord_vpz.models_mixins

*This package is part of erecord - web services for vle models*

:copyright: Copyright (C) 2019-2024 INRA http://www.inra.fr.
:license: GPLv3, see LICENSE file for more details.
:authors: see AUTHORS file.

Mixins common to models of erecord_vpz application

"""

