""" erecord_vpz.models_mixins.transform

Mixins for erecord_vpz model, about transformations

"""

import os
import json
import copy

from erecord_cmn.utils.dir_and_file import make_csv_file

import shutil

import xlwt
from erecord_cmn.utils.dir_and_file import WritableWorksheet as WWS
import xlrd
from erecord_cmn.utils.dir_and_file import ReadableWorksheet as RWS

from erecord_cmn.utils.logger import get_logger
LOGGER = get_logger(__name__)

from erecord_cmn.utils.errors import logger_report_error
from erecord_cmn.utils.errors import build_error_message

# Some mixins for the erecord_vpz models, about transformations

class TransformMixin(object):

    @classmethod
    def transpose(cls, array):
        tarray = list()
        lengths = [len(r) for r in array]
        if lengths :
            maxlength = max(lengths)
            for r in array :
                while len(r) < maxlength:
                    r.append(None)
            for i in range(1,maxlength):
                tarray.append([])
            tarray.append([])
            for i,r_i in enumerate(array):
                for j,m_ij in enumerate(r_i):
                    tarray[j].append(m_ij)
        return tarray

    @classmethod
    def make_list_title(cls, title):
        """Builds and returns a list for title (by row)"""

        r = list()
        r.append([title,])
        r.append([])
        return r

    @classmethod
    def make_list_line_feed(cls):

        """Builds and returns a list for a line feed (by row)"""
        return ([[]])

class VpzInputTransformMixin(TransformMixin):
    """Additional methods for some VpzInput transformations"""

    def make_vpz_file(self, filename, dirpath):
        """ Builds the vpz file relative to VpzInput and saves it as
        filename under dirpath """

        src = self.vpzact.vpzworkspace.get_saved_vpz_file_path()
        dest = os.path.join(dirpath, filename)
        shutil.copyfile(src, dest)

    def make_txt_file(self, filename, dirpath):
        """ Builds the txt file relative to VpzInput and saves it as
        filename under dirpath """

        txtfile = os.path.join(dirpath, filename)
        file = open(txtfile, "a") # "w")
        file.write("\n")
        file.write("*** Input information report***\n")
        file.write("... under construction ...\n")
        file.write("\n")
        file.close()

    def make_list_general(self):
        """Builds and returns general information list of input information """

        list_general = list()
        list_general.append(["begin", self.vlebegin.value,])
        list_general.append(["duration", self.vleduration.value,])
        return list_general 

    def make_list_par_values(self, nicknames, types):
        """Builds and returns list of selected parameters values

        Some names and the type of the parameter are given before its values.
        """

        list_par_values = list()
        for vlecond in self.vlecond_list.all() :
            for vlepar in vlecond.vlepar_list.all() :
                if vlepar.is_selected() :
                    selection_name = vlepar.build_selection_name()
                    nickname = nicknames[selection_name]
                    type = types[selection_name]
                    row = [selection_name, type, nickname]
                    try :
                        val = vlepar.get_val()
                    except Exception as e :
                        msg = "parameter " + str(vlepar.cname)
                        msg += "," + str(vlepar.pname)
                        msg += ":" + str(vlepar.value)
                        errormsg = build_error_message(error=e, msg=msg)
                        logger_report_error(LOGGER)
                        raise Exception(errormsg)
                    if isinstance(val, list) :
                        for i,v in enumerate(val) :
                            row.append(json.dumps(v))
                    else : # singleton case
                        row.append(json.dumps(val))
                    list_par_values.append(row)
        return list_par_values 

    def make_list_par_ident(self):
        """Builds and returns identification information of selected parameters

        - list_ident : parameters identification information,
        - nicknames : nickname (pname) according to selection name
        - types : type according to selection name
        """

        nicknames = dict()
        types = dict()
        list_ident = list()
        list_ident.append(["name (selection name)", "condition name",
                           "parameter name", "type (of first value)",])
                          #"id (in database)"])
        for vlecond in self.vlecond_list.all() :
            for vlepar in vlecond.vlepar_list.all() :
                if vlepar.is_selected() :
                    selection_name = vlepar.build_selection_name()
                    row = [selection_name, vlepar.cname, vlepar.pname,
                           vlepar.type,]
                    #row.append(vlepar.id)
                    list_ident.append(row)
                    nicknames[selection_name] = vlepar.pname
                    types[selection_name] = vlepar.type
        return (list_ident, nicknames, types)

    def make_list_out_ident(self):
        """Builds and returns identification information of selected output
        datas

        - list_ident : output datas identification information,
        - nicknames : nickname (shortname) according to oname
        """

        nicknames = dict()
        nicknames['time'] = 'time'
        list_ident = list()
        list_ident.append(["name (selection name)", "view name",
                           "output data name", "short name",])
                          #"id (in database)"])
        for vleview in self.vpzact.vpzinput.vleview_list.all() :
            for vleout in vleview.vleout_list.all() :
                if vleout.is_selected() :
                    row = [vleout.build_selection_name(), vleout.vname,
                           vleout.oname, vleout.shortname,]
                    #row.append(vleout.id)
                    list_ident.append(row)
                    nicknames[vleout.oname] = vleout.shortname
        return (list_ident, nicknames)

class VpzOutputTransformMixin(TransformMixin):
    """Additional methods for some VpzOutput transformations"""

    def make_res_compact(self):
        """Builds and returns res_compact where the output datas are
        identified by their selection_name (as VleOut).

        Has an effect only in 'dataframe' restype case, returns unmodified
        res in 'matrix' restype case.

        If 'dataframe' restype case, an output data is identified into res
        with a key relative to its name (not exactly oname), under the key
        vname.

        res (of VpzOutput) is encoded.
        res_compact will be encoded.
        """

        from erecord_vpz.models import VleOut
        def build_res_compact_dataframe_single(res):
            res_compact = dict()
            for vname,view in res.items():
                for outname in view.keys():
                    oname = self.build_output_oname(outname)
                    selection_name = VleOut.build_output_selection_name(
                        vname=vname, oname=oname)
                    #res_compact[selection_name] = view.pop(outname)
                    res_compact[selection_name] = copy.deepcopy(view[outname])
            return res_compact

        compacted = False # default
        if self.format_res_ok():
            if self.restype == 'dataframe' :
                if self.plan == 'single' :
                    res = json.loads(self.res)
                    res_compact = build_res_compact_dataframe_single(res)
                    res_compact = json.dumps(res_compact)
                    compacted = True
                elif self.plan == 'linear' :
                    res_a = json.loads(self.res)
                    res_compact_a = list()
                    for res in res_a :
                        res_compact = build_res_compact_dataframe_single(res)
                        res_compact_a.append(res_compact)
                    res_compact = json.dumps(res_compact_a)
                    compacted = True
        if not compacted :
            res_compact = self.res
        return res_compact

    @classmethod
    def make_default_list_general(cls):
        """Builds and returns general information list with default values"""

        list_general = list()
        list_general.append(["restype", "dataframe",])
        list_general.append(["plan", "single",])
        return list_general 

    def make_list_general(self):
        """Builds and returns general information list of output information"""

        list_general = list()
        list_general.append(["restype", self.restype,])
        list_general.append(["plan", self.plan,])
        return list_general 

    @classmethod
    def get_suffixed_name(cls, name, simunumber=None):
        """Returns name suffixed by simunumber (name_simunumber)"""

        suffixed_name = name
        if simunumber is not None :
            suffixed_name = suffixed_name + "_" +str(simunumber)
        return suffixed_name

    @classmethod
    def get_indexed_name(cls, name, simunumber=None):
        """Returns name indexed by simunumber (name[simunumber])"""

        indexed_name = name
        if simunumber is not None :
            indexed_name = indexed_name + "[" +str(simunumber)+ "]"
        return indexed_name

    def write_csv_out_values(self, dirpath, res):
        """Builds under dirpath the csv reports about output datas values

        res is decoded res (of VpzOutput).

        Creates and fills the required csv files : one by view if 'single'
        plan, or as many as simulations if 'linear' plan.

        csv files have headers or not according to restype case :

        - no header if 'matrix' restype.
        - header if 'dataframe' restype (same as those of vle simulation in \
          'file' mode).

        (output information)
        """

        def get_filename(viewname, simunumber=None):
            filename = self.get_suffixed_name(viewname, simunumber) + '.csv'
            return filename

        def write_csv_view(dirpath, viewname, outputs, simunumber=None): 
            filename = get_filename(viewname, simunumber)
            r = list()
            for outputname,val in outputs.items() :
                row = list()
                row.append(outputname)
                for i,v in enumerate(val):
                    row.append(v)
                r.append(row)
            t_r = self.transpose(r)
            make_csv_file(row_list=t_r, filename=filename, dirpath=dirpath,
                          delimiter=";")

        if self.restype=='dataframe' and self.plan=='single' :
            for viewname,outputs in res.items() :
                write_csv_view(dirpath=dirpath, viewname=viewname,
                               outputs=outputs) 

        elif self.restype=='dataframe' and self.plan=='linear' :
            for (a,res_a) in enumerate(res) :
                for viewname,outputs in res_a.items() :
                    write_csv_view(dirpath=dirpath, viewname=viewname,
                                   outputs=outputs, simunumber=a)

        elif self.restype=='matrix' and self.plan=='single' :
            for viewname,v in res.items() :
                filename = get_filename(viewname)
                t_v = self.transpose(v)
                make_csv_file(row_list=t_v, filename=filename,
                              dirpath=dirpath, delimiter=";")
        elif self.restype=='matrix' and self.plan=='linear' :
            for (a,res_a) in enumerate(res) :
                for viewname,v in res_a.items() :
                    filename = get_filename(viewname, a)
                    t_v = self.transpose(v)
                    make_csv_file(row_list=t_v, filename=filename,
                                  dirpath=dirpath, delimiter=";")

    def write_xls_out_values(self, wb, res, nicknames, sheetname="output"):
        """Writes the output datas values into xlwt.Workbook

        res is decoded res (of VpzOutput).

        Creates and fills the required xlwt.Worksheet :

        - if 'dataframe' restype : only one worksheet for all the views \
          together (whichever 'plan' case). \
        - if 'matrix' restype : one worksheet by view if 'single' plan, or \
          as many as simulations if 'linear' plan.

        Worksheets have headers or not according to restype case :

        - no header if 'matrix' restype.
        - header if 'dataframe' restype, where several names are given to \
          identify each output data.

        (output information)
        """

        def get_sheetname_byview(rootname, viewname, simunumber=None):
            """Returns the word joining rootname, viewname and simnumber"""

            sheetname = rootname + " " + viewname
            if simunumber is not None :
                sheetname = sheetname + " " + str(simunumber)
            return sheetname

        def get_sheetname(rootname, simunumber=None):
            """Returns the word joining rootname and simnumber"""

            sheetname = rootname
            if simunumber is not None :
                sheetname = sheetname + " " + str(simunumber)
            return sheetname

        from erecord_vpz.models import VleOut
        def get_fullname(outputname, viewname, simunumber=None):
            """Returns the output data selection name suffixed by simunumber"""

            selection_name = VleOut.build_output_selection_name(vname=viewname,
                                                              oname=outputname)
            fullname = self.get_indexed_name(name=selection_name,
                                             simunumber=simunumber)
            return fullname

        def write_output(ws, row, col, viewname, outputname, values,
                         simunumber=None): 
            """Writes in the col column of the ws worksheet the output data
               header and values
            """

            fullname = get_fullname(outputname=outputname, viewname=viewname,
                                    simunumber=simunumber)
            WWS.write(ws, row, col, fullname)
            row = row + 1
            WWS.write(ws, row, col,
                      self.get_indexed_name(outputname, simunumber))
            row = row + 1
            if outputname in nicknames.keys() :
                nickname = nicknames[outputname]
            else : # has to build nickname
                l = outputname.split(".")
                if len(l) == 2 :
                    nickname = l[1]
                else : # unexpected
                    nickname = outputname

            WWS.write(ws, row, col,
                      self.get_indexed_name(nickname, simunumber),
                      style=WWS.head_style)
            row_ini = row + 1
            for i,v in enumerate(val):
                row = i + row_ini
                WWS.write(ws, row, col, v)
            col = col + 1
            row = row + 1
            return (row, col)

        if self.restype=='dataframe' :
            if self.plan=='single' :
                ws = wb.add_sheet(sheetname)
                col = 0
                for viewname,outputs in res.items() :
                    for outputname,val in outputs.items() :
                        (row, col) = write_output(ws=ws, row=0, col=col,
                                         viewname=viewname,
                                         outputname=outputname, values=val)
            elif self.plan=='linear' :
                for (a,res_a) in enumerate(res) :
                    ws = wb.add_sheet(get_sheetname(rootname=sheetname,
                                          simunumber=a))
                    col = 0
                    for viewname,outputs in res_a.items() :
                        for outputname,val in outputs.items() :
                            (row, col) = write_output(ws=ws, row=0, col=col,
                                             viewname=viewname,
                                             outputname=outputname,
                                             values=val, simunumber=a)
        elif self.restype=='matrix' and self.plan=='single' :
            for viewname,v in res.items() :
                ws = wb.add_sheet(get_sheetname_byview(rootname=sheetname,
                                                viewname=viewname))
                last_row = WWS.write_lines_bycol(ws=ws, row_ini=0, lines=v)
        elif self.restype=='matrix' and self.plan=='linear' :
            for (a,res_a) in enumerate(res) :
                for viewname,v in res_a.items() :
                    ws = wb.add_sheet(get_sheetname_byview(rootname=sheetname,
                                      viewname=viewname, simunumber=a))
                    last_row = WWS.write_lines_bycol(ws=ws, row_ini=0, lines=v)

    def make_txt_file(self, filename, dirpath):
        """ Builds the txt file relative to VpzOutput and saves it as
        filename under dirpath """

        txtfile = os.path.join(dirpath, filename)
        file = open(txtfile, "a") # "w")
        file.write("\n")
        file.write("*** Output information report***\n")
        file.write("... under construction ...\n")
        file.write("\n")
        file.close()

class VpzActTransformMixin(TransformMixin):
    """Additional methods for some VpzAct transformations"""

    def prepare_reports(self, with_general_output=True,
                        default_general_output=False,
                        with_out_ident_and_nicknames=True,
                        with_res=True,
                        with_bycol=True):
        """Prepares information (lists) that will be used for reports building

        Information comes from input and output information, but from only
        input information in with_general_output and with_res False case.
        """
        
        list_general_input = self.vpzinput.make_list_general()
        if with_general_output :
            if default_general_output :
                list_general_output = VpzOutputTransformMixin.make_default_list_general()
            else :
                list_general_output = self.vpzoutput.make_list_general()
            list_general = list_general_output + list_general_input
        else :
            list_general = list_general_input
        self.list_general_byrow = list_general
        if with_bycol :
            self.list_general_bycol = self.transpose(list_general)
        else :
            self.list_general_bycol = None

        (list_par_ident,
         nicknames_par, types_par) = self.vpzinput.make_list_par_ident()
        self.list_par_ident_byrow = list_par_ident
        self.nicknames_par = nicknames_par
        self.types_par = types_par

        if with_out_ident_and_nicknames :
            (list_out_ident,
             nicknames_out) = self.vpzinput.make_list_out_ident()
            self.list_out_ident_byrow = list_out_ident
            self.nicknames_out = nicknames_out
        else :
            self.list_out_ident_byrow = None
            self.nicknames_out = None

        list_par_values =  self.vpzinput.make_list_par_values(
                                             nicknames=self.nicknames_par,
                                             types=self.types_par)
        self.list_par_values_byrow =  list_par_values
        if with_bycol :
            self.list_par_values_bycol = self.transpose(list_par_values)
        else :
            self.list_par_values_bycol = None

        if with_res :
            decoded_res = json.loads(self.vpzoutput.res)
            self.decoded_res = decoded_res
        else :
            self.decoded_res = None

    def make_folder_vpz(self, dirpath):
        """ Builds under dirpath the vpz report """

        newvpzname = "__updated__" + self.vpzname
        self.vpzinput.make_vpz_file(filename=newvpzname, dirpath=dirpath)

    def make_folder_txt(self, dirpath):
        """Builds under dirpath the txt report
        
        (input and output information)
        """

        filename='report.txt'
        self.vpzinput.make_txt_file(filename=filename, dirpath=dirpath)
        self.vpzoutput.make_txt_file(filename=filename, dirpath=dirpath)

    def make_folder_csv_ident(self, dirpath):
        """ Builds under dirpath the csv report about identification

        (input information)
        """

        filename='ident.csv'
        title="*** Parameters identification ***"
        r = self.make_list_title(title)
        r = r + self.list_par_ident_byrow
        r = r + self.make_list_line_feed()
        title="*** Output datas identification ***"
        r = r + self.make_list_title(title)
        r = r + self.list_out_ident_byrow
        make_csv_file(row_list=r, filename=filename, dirpath=dirpath,
                      delimiter=";")

    def make_folder_csv_cond(self, dirpath, bycol):
        """Builds under dirpath the csv reports about simulation conditions

        (input and output information)
        """

        if bycol :
            filename='conditions.csv' # a col for each data
            title="*** General information ***"
            r = self.make_list_title(title)
            r = r + self.list_general_bycol
            r = r + self.make_list_line_feed()
            title="*** Parameters values ***"
            r = r + self.make_list_title(title)
            r = r + self.list_par_values_bycol
            make_csv_file(row_list=r, filename=filename, dirpath=dirpath,
                          delimiter=";")
        else :
            filename='conditions.csv' # a row for each data
            title="*** General information ***"
            r = self.make_list_title(title)
            r = r + self.list_general_byrow
            r = r + self.make_list_line_feed()
            title="*** Parameters values ***"
            r = r + self.make_list_title(title)
            r = r + self.list_par_values_byrow
            make_csv_file(row_list=r, filename=filename, dirpath=dirpath,
                          delimiter=";")

    def make_folder_csv_output(self, dirpath):
        """Builds under dirpath the csv reports about output datas values

        (output information)
        """

        self.vpzoutput.write_csv_out_values(dirpath=dirpath,
                                            res=self.decoded_res)

    def make_folder_xls(self, dirpath, bycol):
        """Builds under dirpath the xls report

        A worksheet by theme (ident, conditions...).

        (input and output information)
        """

        filename='report.xls'
        xlsfile = os.path.join(dirpath, filename)
        wb = xlwt.Workbook()

        ws_ident = wb.add_sheet("ident")
        last_row = WWS.write_group_byrow(ws=ws_ident,
                   info_list=self.list_par_ident_byrow, nb_head=1, col_main=[2],
                   row_ini=0, title="*** Parameters identification ***")
        last_row = WWS.write_line_feed(ws=ws_ident, row_ini=last_row+1)
        last_row = WWS.write_group_byrow(ws=ws_ident,
                   info_list=self.list_out_ident_byrow, nb_head=1, col_main=[3],
                   row_ini=last_row+1,
                   title="*** Output datas identification ***")

        if bycol :
            ws = wb.add_sheet("conditions")
            last_row = WWS.write_group_bycol(ws=ws,
                       info_list=self.list_general_byrow, row_main=[0],
                       row_ini=0, title="*** General information ***")
            last_row = WWS.write_line_feed(ws=ws, row_ini=last_row+1)
            last_row = WWS.write_group_bycol(ws=ws,
                       info_list=self.list_par_values_byrow, row_main=[2],
                       row_ini=last_row+1, title="*** Parameters values ***")
        else :
            ws = wb.add_sheet("conditions")
            last_row = WWS.write_group_byrow(ws=ws,
                       info_list=self.list_general_byrow, col_main=[0],
                       row_ini=0, title="*** General information ***")
            last_row = WWS.write_line_feed(ws=ws, row_ini=last_row+1)
            last_row = WWS.write_group_byrow(ws=ws,
                       info_list=self.list_par_values_byrow, col_main=[2],
                       row_ini=last_row+1, title="*** Parameters values ***")

        self.vpzoutput.write_xls_out_values(wb=wb, res=self.decoded_res,
                                            nicknames=self.nicknames_out)

        wb.save(xlsfile)

    def make_conditions_folder_xls(self, dirpath, bycol):
        """Builds under dirpath the xls reports about simulation conditions

        A file by row or by column, depending on bycol.

        (input information)
        """

        if bycol :
            filename='conditions.xls'
            xlsfile = os.path.join(dirpath, filename)
            wb = xlwt.Workbook()
            ws = wb.add_sheet("conditions")
            last_row = WWS.write_group_bycol(ws=ws,
                       info_list=self.list_general_byrow, row_main=[0],
                       row_ini=0, title="*** General information ***")
            last_row = WWS.write_line_feed(ws=ws, row_ini=last_row+1)
            last_row = WWS.write_group_bycol(ws=ws,
                       info_list=self.list_par_values_byrow, row_main=[2],
                       row_ini=last_row+1, title="*** Parameters values ***")
            wb.save(xlsfile)
        else :
            filename='conditions.xls'
            xlsfile = os.path.join(dirpath, filename)
            wb = xlwt.Workbook()
            ws = wb.add_sheet("conditions")
            last_row = WWS.write_group_byrow(ws=ws,
                       info_list=self.list_general_byrow, col_main=[0],
                       row_ini=0, title="*** General information ***")
            last_row = WWS.write_line_feed(ws=ws, row_ini=last_row+1)
            last_row = WWS.write_group_byrow(ws=ws,
                       info_list=self.list_par_values_byrow, col_main=[2],
                       row_ini=last_row+1, title="*** Parameters values ***")
            wb.save(xlsfile)

    def make_experiment_conditions_sheet(self, ws, with_help=True):
        """ Builds experiment conditions sheet """

        if with_help :
            text = "You can base yourself on this file to build the file "
            text+= "expected in a POST vpz/experiment request"
            last_row = WWS.write_help(ws=ws, row_ini=0, text=text)
            text = "Don't change anything else than what is said in blue text "
            text+= "(values to modify, rows to remove)"
            last_row = WWS.write_help(ws=ws, row_ini=last_row+1, text=text)
            text = "DON'T TOUCH THE SHEET NAME ('conditions') "
            text+= "NEITHER THE TITLES ROWS (beginning with '***')"
            last_row = WWS.write_help(ws=ws, row_ini=last_row+1, text=text)
            last_row = WWS.write_line_feed(ws=ws, row_ini=last_row+1)
        else :
            last_row = WWS.write_line_feed(ws=ws, row_ini=0)

        if with_help :
            text = "Modify below values as wanted"
            last_row = WWS.write_help(ws=ws, row_ini=last_row+1, text=text)
            text= "restype available values : dataframe, matrix."
            last_row = WWS.write_help(ws=ws, row_ini=last_row+1, text=text)
            text = "plan available values : single, linear."
            last_row = WWS.write_help(ws=ws, row_ini=last_row+1, text=text)
        last_row = WWS.write_group_byrow(ws=ws,
                   info_list=self.list_general_byrow, col_main=[0],
                   row_ini=last_row+1, title="*** General information ***")
        last_row = WWS.write_line_feed(ws=ws, row_ini=last_row+1)
        if with_help :
            text = "Modify below values as wanted "
            last_row = WWS.write_help(ws=ws, row_ini=last_row+1, text=text)
            text = "In case of N simulations (each parameter has one or N "
            text+= "values) : put one value by column."
            last_row = WWS.write_help(ws=ws, row_ini=last_row+1, text=text)
            text = "You can keep only the modified parameters (remove some "
            text+= "rows of unchanged parameters for faster process)"
            last_row = WWS.write_help(ws=ws, row_ini=last_row+1, text=text)

        last_row = WWS.write_group_byrow(ws=ws,
                   info_list=self.list_par_values_byrow, col_main=[2],
                   row_ini=last_row+1, title="*** Parameters values ***")

        last_row = WWS.write_line_feed(ws=ws, row_ini=last_row+1)
        if with_help :
            text = "Keep below only the parameters you want to be returned in "
            text+= "POST vpz/experiment response (remove the unwanted rows)"
            last_row = WWS.write_help(ws=ws, row_ini=last_row+1, text=text)
        last_row = WWS.write_group_byrow(ws=ws,
                   info_list=self.list_par_ident_byrow, nb_head=1, col_main=[2],
                   row_ini=last_row+1,
                   title="*** Parameters identification ***")
        last_row = WWS.write_line_feed(ws=ws, row_ini=last_row+1)
        if with_help :
            text = "Keep below only the output datas you want to be returned "
            text+= "in POST vpz/experiment response (remove the unwanted rows)"
            last_row = WWS.write_help(ws=ws, row_ini=last_row+1, text=text)
        last_row = WWS.write_group_byrow(ws=ws,
                   info_list=self.list_out_ident_byrow, nb_head=1, col_main=[3],
                   row_ini=last_row+1,
                   title="*** Output datas identification ***")

    def read_experiment_file_xls(self, filepath):
        """ Reads the xls filepath file about experiment

        The information is read into the "conditions" sheet.
        The information is structured like into the file returned by a
        GET vpz/experiment request.

        Returns (cr_ok, restype_value, plan_value,
        begin_value, duration_value, parameters as a dict (key,value being
        parameter selection_name,value), parselect_values, outselect_values.

        (input information)
        """

        cr_ok = False # default
        restype_value = None
        plan_value = None
        begin_value = None
        duration_value = None
        parameters = dict()
        parselect_values = list()
        outselect_values = list()

        sh = RWS.get_sheet(filepath=filepath, sheetname="conditions")
        firstcol = None # default
        if sh is not None :
            firstcol = RWS.read_sheet_column(sh, 0)
        if firstcol is not None :
            rows = RWS.read_sheet_rows(sh)
            def get_value(rows, name) :
                """Returns value (in 2nd col) of a data with name in 1st col"""
                for row in rows :
                    if len(row) > 1 :
                        if row[0] == name :
                            value = row[1]
                            return value
                return None
            restype_value = get_value(rows, "restype")
            plan_value = get_value(rows, "plan")
            begin_value = float(get_value(rows, "begin"))
            duration_value = float(get_value(rows, "duration"))
    
            def get_ifirst(col, title) :
                """Returns the (first) indice of col column whose value
                begins with title"""
                for i,c in enumerate(col) :
                    if c.startswith(title) :
                        return i
                return None
            def get_ilast(col, ifirst) :
                """Returns the (first) indice of col column after ifirst
                and with a title value (beginning with '***')"""
                length = len(col)
                if length > 1 :
                    ibegin = ifirst + 1 
                    if ibegin < length :
                        for i in range(ibegin, length) :
                            if col[i].startswith("***") :
                                return i
                    return length-1
                else :
                    return None
            ifirst_par = get_ifirst(firstcol, "*** Parameters values")
            if ifirst_par is not None :
                ilast_par = get_ilast(firstcol, ifirst_par)
                if ilast_par is not None :
                    end = ilast_par+1
                    for row in rows[ifirst_par:end] :
                        if len(row) > 3 and row[1] != '' :
                                selection_name = row[0]
                                values = [json.loads(v)
                                          for v in row[3:len(row)]]
                                parameters[selection_name] = json.dumps(values)
            ifirst_parselect = get_ifirst(firstcol,
                                          "*** Parameters identification")
            if ifirst_parselect is not None :
                ilast_parselect = get_ilast(firstcol, ifirst_parselect)
                if ilast_parselect is not None :
                    end = ilast_parselect+1
                    for row in rows[ifirst_parselect:end] :
                        if len(row) > 1 and row[1] != '' \
                           and not row[0].startswith("name (selection") :
                            parselect_values.append(row[0])

            ifirst_outselect = get_ifirst(firstcol,
                                          "*** Output datas identification")
            if ifirst_outselect is not None :
                ilast_outselect = get_ilast(firstcol, ifirst_outselect)
                if ilast_outselect is not None :
                    end = ilast_outselect+1
                    for row in rows[ifirst_outselect:end] :
                        if len(row) > 1 and row[1] != '' \
                           and not row[0].startswith("name (selection") :
                            outselect_values.append(row[0])
            cr_ok = True
        return (cr_ok, restype_value, plan_value, begin_value, duration_value,
                parameters, parselect_values, outselect_values)

    def make_experiment_in_file_xls(self, dirpath):
        """Builds under dirpath the xls file about experiment

        (input information)
        """

        filename='experiment.xls'
        xlsfile = os.path.join(dirpath, filename)
        wb = xlwt.Workbook()
        ws = wb.add_sheet("conditions")
        self.make_experiment_conditions_sheet(ws=ws, with_help=True)
        wb.save(xlsfile)

    def make_experiment_out_file_xls(self, dirpath):
        """Builds under dirpath the xls file about experiment

        A worksheet for conditions. A worksheet for simulation results
        (or several in some multi-simulation cases).

        (input and output information)
        """

        filename='experiment.xls'
        xlsfile = os.path.join(dirpath, filename)
        wb = xlwt.Workbook()
        ws = wb.add_sheet("conditions")
        self.make_experiment_conditions_sheet(ws=ws, with_help=False)
        self.vpzoutput.write_xls_out_values(wb=wb, res=self.decoded_res,
                           nicknames=self.nicknames_out, sheetname="results")
        wb.save(xlsfile)

