"""erecord_vpz.models_mixins.workspace

Mixins for erecord_vpz model, about workspace management

"""

import os
import shutil

from erecord_cmn.configs.config import REQUESTS_HOME_APP_VPZ

from erecord_cmn.utils.dir_and_file import delete_path_if_present
from erecord_cmn.utils.dir_and_file import clean_dir
from erecord_cmn.utils.dir_and_file import create_dir_if_absent
from erecord_cmn.utils.dir_and_file import is_a_directory
from erecord_cmn.utils.dir_and_file import get_available_pathname

from erecord_cmn.utils.container.location import get_vlecontainer_path

from erecord_vpz.models_mixins.container import VleContainerMixin

from erecord_cmn.utils.logger import get_logger
LOGGER = get_logger(__name__)

# Some mixins for the erecord_vpz models, about workspace management

class WorkspaceMixin(object):

    @classmethod
    def get_undefined_value(cls):
        return "undefined"

    @classmethod
    def is_undefined_value(cls, v):
        return (v == cls.get_undefined_value())

    def get_experiment_file_path(self):
        return os.path.join(self.homepath, "user_experiment.xls")

class ReporthomeWorkspaceMixin(WorkspaceMixin):

    def init_reporthome(self, LOGGER, as_reporthome=False):
        reporthome = os.path.join(self.homepath, "report_home")
        if as_reporthome :
            (deletion_done, creation_done) = clean_dir(dirpath=reporthome)
            if deletion_done :
                LOGGER.warning(u"%s has been deleted before to be recreated", 
                        reporthome)
        return reporthome

    def get_reporthome(self):
        return self.reporthome

    def get_report_folder_path(self, name) :
        """Defines and return a folder path name as a reporthome subdirectory"""
        return os.path.join(self.reporthome, name+'_folder')

    def clean_reporthome(self):
        clean_dir(dirpath=self.reporthome)

    def clear_reporthome(self):
        delete_path_if_present(path=self.reporthome)

class DatahomeWorkspaceMixin(WorkspaceMixin):
    """datahome workspace 

    The datahome workspace is dedicated to modify the 'data' directory of
    the main vle package according to a data folder sent into the request.
    It prepares things for actions_vpz.

    Implied folders and files :

      datahome            = homepath / data_home 

      datafolder_zip_file = homepath / data_home / datafolder.zip

      datafolder_src      = homepath / data_home / data

    datafolder_zip_file, datafolder_src : the datafolder_zip_file zip file
    contains the data folder sent into a request. It will be unzipped to get
    datafolder_src. The content of the 'data' directory of the main vle
    package will (later) be replaced or overwritten by the datafolder_src
    content.
    """

    def init_datahome(self, LOGGER, as_datahome=False):
        datahome = os.path.join(self.homepath, "data_home")
        if as_datahome :
            (deletion_done, creation_done) = clean_dir(dirpath=datahome)
            if deletion_done :
                LOGGER.warning(u"%s has been deleted before to be recreated", 
                               datahome)
        return datahome

    def get_datafolder_zip_file_path(self):
        return os.path.join(self.datahome, "datafolder.zip")

    def get_datafolder_src_path(self):
        return os.path.join(self.datahome, 'data')

    def get_and_verify_datafolder_src_path(self):
        datafolder_src_path = self.get_datafolder_src_path()
        data_name = os.path.basename(datafolder_src_path)
        cr_ok = is_a_directory(datafolder_src_path)
        return (cr_ok, datafolder_src_path, data_name)

    def get_datahome(self):
        return self.datahome

    def clean_datahome(self):
        """creates datahome after having deleted it before if necessary"""

        (deletion_done, creation_done) = clean_dir(dirpath=self.datahome)
        if deletion_done :
            LOGGER.warning(u"%s has been deleted before to be recreated", 
                           self.datahome)

    def clear_datahome(self):
        delete_path_if_present(path=self.datahome)

class RunhomeWorkspaceMixin(WorkspaceMixin):

    def init_runhome(self, LOGGER, as_runhome=False):
        runhome = os.path.join(self.homepath, "run_home")
        if as_runhome :
            (deletion_done, creation_done) = clean_dir(dirpath=runhome)
            if deletion_done :
                LOGGER.warning(u"%s has been deleted before to be recreated", 
                        runhome)
        return runhome

    def get_runhome(self):
        return self.runhome

    def clean_runhome(self):
        clean_dir(dirpath=self.runhome)

    def clear_runhome(self):
        delete_path_if_present(path=self.runhome)

    def define_and_build_runhome_subdirectory(self, rootname="s_"):

        runhome_subdirectory = get_available_pathname(rootpath=self.runhome,
                                                      rootname=rootname)
        create_dir_if_absent(runhome_subdirectory)
        return runhome_subdirectory

    def get_saved_vpz_file_path(self) :
        """File path where the saved vpz file has been temporary stored

        Note : coming from a run_home subdirectory and going to report_home
        """
        
        return os.path.join(self.runhome, "__updated__wwdm.vpz")

class ContainerWorkspaceMixin(VleContainerMixin):
    """Additional methods for VpzWorkspace about calling a container """

    def define_vlecontainerpath(self):
        """Returns the full path name of the models repository container file

        Convention :
        The container of a models repository is located under the vlepath
        (models repository path) of VpzAct attached to VpzWorkspace.
        """

        return get_vlecontainer_path(path=self.vpzact.vlepath)

    def actions_vpz(self, storaged_list=None, conditions=None,
                    begin=None, duration=None, plan=None, restype=None,
                    action_modify_vpz=False,
                    action_read_vpz=False,
                    action_run_vpz=False,
                    action_copy_datafolder=False,
                    datafoldercopy='overwrite',
                    action_copy_modified_vpz=False,
                    runvle_path=None) :
        """Calls CONT_actions_vpz according to VpzWorkspace and its VpzAct

        Calls CONT_actions_vpz (see VleContainerMixin) in the case of :

        - the models repository container file of VpzWorkspace (see \
          self.vlecontainerpath)
        - its vpz file (pkgname, vpzname) defined by VpzAct attached to \
          VpzWorkspace.

        Default values are defined there for calling CONT_actions_vpz in 
        case corresponding with a VpzAct.
        """

        return self.CONT_actions_vpz(storaged_list=storaged_list,
                     conditions=conditions, begin=begin, duration=duration,
                     plan=plan, restype=restype,
                     pkgname=self.vpzact.pkgname, vpzname=self.vpzact.vpzname,
                     action_modify_vpz=action_modify_vpz,
                     action_read_vpz=action_read_vpz,
                     action_run_vpz=action_run_vpz,
                     action_copy_datafolder=action_copy_datafolder,
                     datafoldercopy=datafoldercopy,
                     action_copy_modified_vpz=action_copy_modified_vpz,
                     runvle_path=runvle_path)

class InternalContainerWorkspace(RunhomeWorkspaceMixin, VleContainerMixin):
    """Internal workspace to use a container

    An InternalContainerWorkspace defines a workspace directory dedicated to a
    models repository container manipulation independantly from any requests.
    The models repository container is identified by vlecontainerpath,
    independantly from any vlerep, vpzact.

    - homepath : absolute path
    - runhome : run home path
    - vlecontainerpath : path of the vle models repository container file

    Note : After having created, and then used, an InternalContainerWorkspace
    object, its own folders must be deleted (see clear method)

    """

    def __init__(self, path):
        """Init from path

        path is the folder containing the models repository container file
        """
        self.homepath = get_available_pathname(rootpath=REQUESTS_HOME_APP_VPZ,
                rootname="internal_")
        create_dir_if_absent(self.homepath)
        self.runhome = self.init_runhome(LOGGER=LOGGER, as_runhome=True)
        self.vlecontainerpath = get_vlecontainer_path(path=path)

    def clear(self):
        """Deletes associated folder"""
        delete_path_if_present(path=self.homepath)

