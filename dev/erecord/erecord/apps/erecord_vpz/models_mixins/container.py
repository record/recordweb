"""erecord_vpz.models_mixins.container

Mixins about calling/using a models repository container

"""

from erecord_cmn.utils.container.vle_api import get_vle_version_number
from erecord_cmn.utils.container.vle_api import get_pkg_list
from erecord_cmn.utils.container.vle_api import get_vpz_list
from erecord_cmn.utils.container.vle_api import get_data_list
from erecord_cmn.utils.container.erecord_api import vle_actions_vpz
from erecord_cmn.utils.container.erecord_api import prepare_datafolder
from erecord_cmn.utils.container.erecord_api import store_saved_vpz_file

class VleContainerMixin(object):
    """Methods about a models repository container

    Careful : self.runhome and self.vlecontainerpath must have been defined
    """

    def CONT_get_vle_version_name(self, runvle_path=None) :
        """Defines and returns the vle version name

        Gets into the models repository container the vle version number,
        defines from it the vle version name and returns this vle version name. 

        Convention :
        The vle_version_name is the vle version number prefixed by "vle-"

        It is possible to give one's own runvle_path where to run, or else
        it will be defined (from self.runhome) and created.
        """

        vle_version_name = None # default
        if runvle_path is None :
            runvle_path = self.define_and_build_runhome_subdirectory(
                                                     rootname = "vleversion_")
        try :
            ret = get_vle_version_number(runvle_path=runvle_path,
                                         container_path=self.vlecontainerpath)
            if type(ret) is list :
                vle_version_number = ret[0]
                vle_version_name = "vle-" + vle_version_number
            else :
                raise Exception("CONT_get_vle_version_name -- error")
        except :
            raise
        return vle_version_name


    def CONT_get_pkg_list(self, runvle_path=None) :
        """Gets and returns list of package names of the models repository \
           container

        It is possible to give one's own runvle_path where to run, or else
        it will be defined and created.
        """

        pkg_list = None #default
        if runvle_path is None :
            runvle_path = self.define_and_build_runhome_subdirectory(
                                                  rootname="pkg_list_")
        try :
            pkg_list = get_pkg_list(runvle_path=runvle_path,
                                    container_path=self.vlecontainerpath)
        except :
            raise
        return pkg_list


    def CONT_get_vpz_list(self, pkgname="", runvle_path=None) :
        """Gets and returns names of vpz into the models repository container

        - if pkgname given :  \
          returns list of vpz names of the pkgname package
        - else (case of all packages) :  \
          returns dict of key=pkgname, value=list of its vpz names

        It is possible to give one's own runvle_path where to run, or else
        it will be defined and created.
        """

        vpz_list = None #default
        if runvle_path is None :
            runvle_path = self.define_and_build_runhome_subdirectory(
                                                  rootname="vpz_list_")
        try :
            vpz_list = get_vpz_list(runvle_path=runvle_path,
                                    container_path=self.vlecontainerpath,
                                    pkgname=pkgname)
        except Exception as e :
            raise
        return vpz_list


    def CONT_get_data_list(self, pkgname, runvle_path=None) :
        """Gets and returns data files names of pkgname package into the \
           models repository container

        Returns list of data files names of the pkgname package

        It is possible to give one's own runvle_path where to run, or else
        it will be defined and created.
        """

        data_list = None #default
        if runvle_path is None :
            runvle_path = self.define_and_build_runhome_subdirectory(
                                                  rootname="data_list_")
        try :
            data_list = get_data_list(runvle_path=runvle_path,
                                      container_path=self.vlecontainerpath,
                                      pkgname=pkgname)
        except Exception as e :
            raise
        return data_list


    def CONT_actions_vpz(self, pkgname, vpzname, storaged_list, conditions,
                   begin, duration, plan, restype,
                   action_modify_vpz=False,        # True or False
                   action_read_vpz=False,          # True or False
                   action_run_vpz=False,           # True or False
                   action_copy_datafolder=False,   # True or False
                   datafoldercopy='overwrite',     # 'replace' or 'overwrite'
                   action_copy_modified_vpz=False, # True or False
                   runvle_path=None) :
        """Applies the appropriate combination of actions to the vpz file.

        Does :

        - prepare_datafolder in action_copy_datafolder case.
        - Modifies and reads and simulates the vpzname vpz of pkgname package \
          by taking into account the given data folder and by saving the \
          modified vpz file, as described into the called vle_actions_vpz.
        - store_saved_vpz_file in action_copy_modified_vpz case.

        Returns, as json data, the input information of vpzname and results of
        vpzname simulation.

        It is possible to give one's own runvle_path where to run, or else
        it will be defined and created.
        """

        jres_read_part = None #default
        jres_run_part = None  #default
        if runvle_path is None :
            runvle_path = self.define_and_build_runhome_subdirectory(
                                                  rootname="actions_")
        try :

            if action_copy_datafolder :
                prepare_datafolder(runvle_path=runvle_path,
                                   data_src=self.get_datafolder_src_path())

            (jres_read_part, jres_run_part) = vle_actions_vpz(
                     runvle_path=runvle_path,
                     container_path=self.vlecontainerpath,
                     pkgname=pkgname, vpzname=vpzname,
                     plan=plan, restype=restype,
                     storaged_list=storaged_list, conditions=conditions,
                     begin=begin, duration=duration,
                     action_modify_vpz=action_modify_vpz,
                     action_read_vpz=action_read_vpz,
                     action_run_vpz=action_run_vpz,
                     action_copy_datafolder=action_copy_datafolder,
                     datafoldercopy=datafoldercopy,
                     action_copy_modified_vpz=action_copy_modified_vpz)

            if action_copy_modified_vpz :
                dest_file_path = self.get_saved_vpz_file_path()
                store_saved_vpz_file(runvle_path=runvle_path, vpzname=vpzname,
                                     dest_file_path= dest_file_path)
        except :
            raise

        return (jres_read_part, jres_run_part)

