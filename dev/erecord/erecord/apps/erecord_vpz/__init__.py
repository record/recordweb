"""erecord_vpz

*This package is part of erecord - web services for vle models*

:copyright: Copyright (C) 2019-2024 INRA http://www.inra.fr.
:license: GPLv3, see LICENSE file for more details.
:authors: see AUTHORS file.

Vpz activities application

The vpz application is dedicated to activities on vpz (edition, modification,
simulation...).

The manipulation object is VpzAct. It has some characteristics that determine which vpz file is manipulated and all along the manipulation, some other information will be attached to it.

"""

