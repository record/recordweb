"""erecord_vpz.views.activities

erecord_vpz application views (activities)

"""

from rest_framework.response import Response
from rest_framework import generics

from erecord_vpz.models import VpzAct
from erecord_acs.models import ActivityLock

from erecord_cmn.views_mixins import DetailViewMixin
from erecord_cmn.views_mixins import StyleViewMixin
from erecord_cmn.views_mixins import FormatViewMixin
from erecord_cmn.views_mixins import DataViewMixin
from erecord_vpz.views.activities_mixins import ActivityViewMixin
from erecord_vpz.views.activities_mixins import InputViewMixin
from erecord_vpz.views.activities_mixins import OutputViewMixin
from erecord_vpz.views.activities_mixins import InOutputViewMixin
from erecord_vpz.views.views_mixins import ReportViewMixin
from erecord_vpz.views.views_mixins import ExperimentViewMixin

from erecord_vpz.views.views_mixins import PersistenceViewMixin
from erecord_vpz.views.objects_mixins import VpzInputDetailViewMixin
from erecord_vpz.views.objects_mixins import VpzOutputDetailViewMixin
from erecord_vpz.views.objects_mixins import VpzActDetailViewMixin

from erecord_vpz.forms import VpzOutputUserForm
from erecord_vpz.forms import VpzActUserForm

from erecord_cmn.configs.config import DOWNLOAD_LIFETIME
from erecord_slm.forms import TodownloadUrlForm

from erecord_cmn.utils.logger import get_logger
LOGGER = get_logger(__name__)
from erecord_cmn.utils.errors import logger_report_error
from erecord_cmn.utils.errors import build_error_message
from erecord_cmn.utils.errors import get_error_status

from rest_framework.negotiation import DefaultContentNegotiation

headsubtitle = "(erecord_vpz)"

class FormatContentNegotiation(FormatViewMixin, DataViewMixin,
                               DefaultContentNegotiation):
    """ContentNegotiation taking into account format as POST parameter """

    def select_parser(self, request, parsers):
        return super(FormatContentNegotiation, self).select_parser(request,
                                                                   parsers)

    def select_renderer(self, request, renderers, format_suffix):
        data = self.data_from_request(request)
        format = self.get_format_value(data=data)
        if format is not None :
            format_suffix = format
        return super(FormatContentNegotiation, self).select_renderer(request,
                                                    renderers, format_suffix)

class InputView(VpzInputDetailViewMixin, PersistenceViewMixin,
                InputViewMixin,
                StyleViewMixin, FormatViewMixin, DataViewMixin,
                DetailViewMixin, generics.RetrieveAPIView):
    """Input information of a vpz
    
    Returns VpzInput information of a VpzAct after having created it and \
    maybe, in POST command case, has modified before the vpz file relative \
    to VpzAct.

    The VpzAct is created from a VpzPath or from a VleVpz.

    After restitution, VpzAct (and its 'dependencies/tree') may be deleted \
    or kept into the database depending on persistence management.
    """

    content_negotiation_class = FormatContentNegotiation

    def get_object(self):
        data = self.data_from_request(self.request)
        self.vpzactid = None # default
        if self.request.method == 'POST' :
            try :
                vpzact = self.action_input_post(LOGGER=LOGGER, data=data)
                self.vpzactid = vpzact.id
                vpzinput = vpzact.vpzinput
                return vpzinput
            except :
                raise
        else : # 'GET'
            try :
                vpzact = self.action_input_get(LOGGER=LOGGER, data=data)
                self.vpzactid = vpzact.id
                vpzinput = vpzact.vpzinput
                return vpzinput
            except :
                raise

    def get(self, request, format=None, **kwargs):
        """Returns VpzInput"""

        data = self.request_query_params(request)
        if format is None :
            format = self.get_format_value(data=data)
        style = self.get_style_value()

        response = self.numeric_response(request=request, data=data,
                                         format=format, style=style, **kwargs)

        if self.vpzactid :
            vpzact = VpzAct.objects.get(pk=self.vpzactid)
            self.manages_persistence(vpzact=vpzact)
            ActivityLock.propagate_lock(vpzact)
        return response

    def post(self, request, format=None, **kwargs):
        """Modifies vpz file relative to vpzact according to post data \
           and returns VpzInput.
        """

        data = self.request_data(request)
        if format is None :
            format = self.get_format_value(data=data)
        style = self.get_style_value()

        response = self.numeric_response(request=request, data=data,
                                         format=format, style=style, **kwargs)

        if self.vpzactid :
            vpzact = VpzAct.objects.get(pk=self.vpzactid)
            self.manages_persistence(vpzact=vpzact)
            ActivityLock.propagate_lock(vpzact)
        return response

    def numeric_response(self, request, format, style, **kwargs):
        """Response about VpzInput in numeric case

        Looks like VpzInputDetail
        """

        if format=='html' :
            try :
                vpzinput = self.get_object()
                if style=='compact' or style=='compactlist' :
                    context = self.html_compact_context(vpzinput=vpzinput,
                                       is_compactlist=(style=='compactlist'),
                                       headsubtitle=headsubtitle)
                    response = Response(context)
                else : # style == 'link' or 'tree'
                    context = self.html_nocompact_context(vpzinput=vpzinput,
                                       headsubtitle=headsubtitle)
                    response = Response(context)
            except Exception as e :
                s = get_error_status(e)
                errormsg = build_error_message(error=e)
                logger_report_error(LOGGER)
                context = self.html_error_context(status=s, detail=errormsg)
                response = Response(context)

        else : # format other than html ('api' 'json' 'yaml' 'xml')

            if style=='compact' or style=='compactlist' :
                try :
                    vpzinput = self.get_object()
                    if style=='compact' :
                        context = self.nohtml_compact_context(
                                                          vpzinput=vpzinput)
                        response = Response(data=context)
                    elif style=='compactlist' :
                        context = self.nohtml_compactlist_context(
                                                          vpzinput=vpzinput)
                        response = Response(data=context)
                except Exception as e :
                    s = get_error_status(e)
                    errormsg = build_error_message(error=e)
                    logger_report_error(LOGGER)
                    response = Response(data={'detail':errormsg,}, status=s)
            else : # style == 'link' or 'tree'
                try :
                    context = super(InputView, self).get(request, **kwargs)
                    response = context
                except Exception as e :
                    s = get_error_status(e)
                    errormsg = build_error_message(error=e)
                    logger_report_error(LOGGER)
                    response = Response(data={'detail':errormsg,}, status=s)
        return response

class OutputView(VpzOutputDetailViewMixin, PersistenceViewMixin,
                 OutputViewMixin,
                 StyleViewMixin, FormatViewMixin, DataViewMixin,
                 DetailViewMixin, generics.RetrieveAPIView):
    """Output information of a vpz
    
    Returns VpzOutput information of a VpzAct after having created it and \
    done the required operations : maybe has modified its vpz file \
    (only in POST command case), has run simulation.

    The VpzAct is created from a VpzPath or from a VleVpz.

    After restitution, VpzAct (and its 'dependencies/tree') may be deleted \
    or kept into the database depending on persistence management.
    """

    content_negotiation_class = FormatContentNegotiation

    def get_object(self):
        data = self.data_from_request(self.request)
        self.vpzactid = None # default
        if self.request.method == 'POST' :
            try :
                vpzact = self.action_output_post(LOGGER=LOGGER, data=data)
                self.vpzactid = vpzact.id
                vpzoutput = vpzact.vpzoutput
                return vpzoutput
            except :
                raise
        else : # 'GET'
            try :
                vpzact = self.action_output_get(LOGGER=LOGGER, data=data)
                self.vpzactid = vpzact.id
                vpzoutput = vpzact.vpzoutput
                return vpzoutput
            except :
                raise

    def get(self, request, format=None, **kwargs):
        """Simulates vpz file relative to vpzact and returns VpzOutput """

        data = self.request_query_params(request)
        if format is None :
            format = self.get_format_value(data=data)
        style = self.get_style_value() # 'tree' or 'link' or 'compact'
        storage = self.get_storage_value(data=data)

        response = self.numeric_response(request=request, data=data,
                        format=format, style=style, storage=storage, **kwargs)

        if self.vpzactid :
            vpzact = VpzAct.objects.get(pk=self.vpzactid)
            self.manages_persistence(vpzact=vpzact)
            ActivityLock.propagate_lock(vpzact)
        return response

    def post(self, request, format=None, **kwargs):
        """Modifies vpz file relative to vpzact according to post data, \
           simulates it and returns VpzOutput.
        """

        data = self.request_data(request)
        if format is None :
            format = self.get_format_value(data=data)
        style = self.get_style_value()
        storage = self.get_storage_value(data=data)

        response = self.numeric_response(request=request, data=data,
                        format=format, style=style, storage=storage, **kwargs)
        if self.vpzactid :
            vpzact = VpzAct.objects.get(pk=self.vpzactid)
            self.manages_persistence(vpzact=vpzact)
            ActivityLock.propagate_lock(vpzact)
        return response

    def numeric_response(self, request, format, style, storage, **kwargs):
        """Response about VpzOutput in numeric case

        Looks like VpzOutputDetail

        """

        if format=='html' :
            try :
                vpzoutput = self.get_object()
                context = dict()
                if (style=='compact') :
                    title = 'The simulation results (in compact style)'
                    context['res'] = vpzoutput.make_res_compact()
                    context['plan'] = vpzoutput.plan
                    context['restype'] = vpzoutput.restype
                else :
                    title = 'The simulation results'
                    form = VpzOutputUserForm(instance=vpzoutput)
                    context['form'] = form
                context['title'] = title
                context['headsubtitle'] = headsubtitle
                context['style'] = style
                if storage :
                    context['storage'] = 'storage'
                response = Response(context)
            except Exception as e :
                s = get_error_status(e)
                errormsg = build_error_message(error=e)
                logger_report_error(LOGGER)
                context = self.html_error_context(status=s, detail=errormsg)
                response = Response(context)

        else : # format other than html ('api' 'json' 'yaml' 'xml')

            if (style=='compact') :
                try :
                    vpzoutput = self.get_object()
                    context = dict()
                    context['res'] = vpzoutput.make_res_compact()
                    context['plan'] = vpzoutput.plan
                    context['restype'] = vpzoutput.restype
                    response = Response(data=context)
                except Exception as e :
                    s = get_error_status(e)
                    errormsg = build_error_message(error=e)
                    logger_report_error(LOGGER)
                    response = Response(data={'detail':errormsg,}, status=s)
            else :
                try :
                    context = super(OutputView, self).get(request, **kwargs)
                    response = context
                except Exception as e :
                    s = get_error_status(e)
                    errormsg = build_error_message(error=e)
                    logger_report_error(LOGGER)
                    response = Response(data={'detail':errormsg,}, status=s)
        return response

class InOutView(VpzActDetailViewMixin, PersistenceViewMixin,
                InOutputViewMixin,
                StyleViewMixin, FormatViewMixin, DataViewMixin,
                DetailViewMixin, generics.RetrieveAPIView):
    """Output information of a vpz and input information of a vpz
    
    Returns VpzInput and VpzOutput information of a VpzAct after having \
    created it and done the required operations : maybe has modified its \
    vpz file (only in POST command case), has read its VpzInput information, \
    has run simulation.

    In fact in the current version the returned information contains more \
    than VpzOutput and VpzInput, it contains all VpzAct information \
    (VpzOrigin...).

    The VpzAct is created from a VpzPath or from a VleVpz.

    After restitution, VpzAct (and its 'dependencies/tree') may be deleted \
    or kept into the database depending on persistence management.
    """

    content_negotiation_class = FormatContentNegotiation

    def get_object(self):
        data = self.data_from_request(self.request)
        self.vpzactid = None # default
        if self.request.method == 'POST' :
            try :
                vpzact = self.action_inoutput_post(LOGGER=LOGGER, data=data,
                                                   with_saving_vpz_file=False)
                self.vpzactid = vpzact.id
                return vpzact
            except :
                raise
        else : # 'GET'
            try :
                vpzact = self.action_inoutput_get(LOGGER=LOGGER, data=data,
                                                  with_saving_vpz_file=False)
                self.vpzactid = vpzact.id
                return vpzact
            except :
                raise

    def get(self, request, format=None, **kwargs):
        """Simulates vpz file relative to vpzact, returns \
           VpzInput and VpzOutput.
        """

        data = self.request_query_params(request)
        if format is None :
            format = self.get_format_value(data=data)

        response = self.numeric_response(request=request, data=data,
                                         format=format, **kwargs)
        if self.vpzactid :
            vpzact = VpzAct.objects.get(pk=self.vpzactid)
            self.manages_persistence(vpzact=vpzact)
            ActivityLock.propagate_lock(vpzact)
        return response

    def post(self, request, format=None, **kwargs):
        """Modifies vpz file relative to vpzact according to post data, \
           simulates it and returns VpzInput and VpzOutput.
        """

        data = self.request_data(request)
        if format is None :
            format = self.get_format_value(data=data)

        response = self.numeric_response(request=request, data=data,
                                         format=format, **kwargs)
        if self.vpzactid :
            vpzact = VpzAct.objects.get(pk=self.vpzactid)
            self.manages_persistence(vpzact=vpzact)
            ActivityLock.propagate_lock(vpzact)
        return response

    def numeric_response(self, request, format, **kwargs):
        """Response about VpzAct in numeric case

        Looks like VpzActDetail
        """

        if format=='html' :
            try :
                vpzact = self.get_object()
                context = dict()
                context['title'] = 'Vpz activity'
                context['headsubtitle'] = headsubtitle
                context['form'] = VpzActUserForm(instance=vpzact)
                response = Response(context )
            except Exception as e :
                s = get_error_status(e)
                errormsg = build_error_message(error=e)
                logger_report_error(LOGGER)
                context = self.html_error_context(status=s, detail=errormsg)
                response = Response(context)
        else :
            try :
                context = super(InOutView, self).get(request, **kwargs)
                response = context
            except Exception as e :
                s = get_error_status(e)
                errormsg = build_error_message(error=e)
                logger_report_error(LOGGER)
                response = Response(data={'detail':errormsg,}, status=s)
        return response

class ReportView(ReportViewMixin, VpzActDetailViewMixin, PersistenceViewMixin,
                 InOutputViewMixin, 
                 StyleViewMixin, FormatViewMixin, DataViewMixin,
                 DetailViewMixin, generics.RetrieveAPIView):
    """Output and input information of a vpz as report(s)

    Creates VpzInput and VpzOutput information of a VpzAct by doing the \
    required operations : maybe has modified its vpz file (only in POST \
    command case), has run simulation.

    Produces and returns the required report folder.

    The VpzAct is created from a VpzPath or from a VleVpz.
    
    The 'report' option is used to choose what kind of report is \
    produced/returned about vpzact.
 
    The 'bycol' mode option is used to choose to have some arrays by columns \
    (by default : by rows).

    The 'todownload' mode option is used to choose to download the result.

    After restitution, VpzAct (and its 'dependencies/tree') may be deleted \
    or kept into the database depending on persistence management.
    """

    content_negotiation_class = FormatContentNegotiation

    def get_object(self):
        data = self.data_from_request(self.request)
        self.vpzactid = None # default

        with_saving_vpz_file = self.is_vpz_report(data=data)

        if self.request.method == 'POST' :
            try :
                vpzact = self.action_inoutput_post(LOGGER=LOGGER, data=data,
                                    with_saving_vpz_file=with_saving_vpz_file)
                self.vpzactid = vpzact.id
                return vpzact
            except :
                raise
        else : # 'GET'
            try :
                vpzact = self.action_inoutput_get(LOGGER=LOGGER, data=data,
                                    with_saving_vpz_file=with_saving_vpz_file)
                self.vpzactid = vpzact.id
                return vpzact
            except :
                raise

    def common(self, request, format=None, **kwargs):
        """Generic method for get and post """

        data = self.data_from_request(self.request)
        if format is None :
            format = self.get_format_value(data=data)
        reports = self.get_report_values(data=data)
        bycol = self.get_bycol_value(data=data)
        todownload = self.get_todownload_value(data=data)

        response = self.report_response(request=request, reports=reports,
                                        bycol=bycol, todownload=todownload,
                                        format=format, **kwargs)
        if self.vpzactid :
            vpzact = VpzAct.objects.get(pk=self.vpzactid)
            self.manages_persistence(vpzact=vpzact)
            ActivityLock.propagate_lock(vpzact)
        return response

    def get(self, request, format=None, **kwargs):
        """Simulates vpz file relative to vpzact, returns VpzOutput \
           and VpzInput into a report folder.
        """

        return self.common(request, format, **kwargs)

    def post(self, request, format=None, **kwargs):
        """Modifies vpz file relative to vpzact according to post data, \
           simulates it, returns VpzInput and VpzOutput into a report folder.
        """

        return self.common(request, format, **kwargs)

    def report_response(self, request, reports, bycol, todownload,
                        format, **kwargs):
        """Response about VpzAct"""

        try :
            vpzact = self.get_object()
            vpzact.vpzworkspace.clean_reporthome()
            self.build_folder(vpzact=vpzact, reports=reports, bycol=bycol)
            zip_folder_path = self.build_zip_folder(vpzact=vpzact)
    
            if todownload :

                download_document = self.build_download_folder(
                                                    file_path=zip_folder_path)

                text ="lock dedicated to DownloadVpzDocument "
                text = text+ "(id " + str(download_document.id) + ")"
                ActivityLock.forward_lock(vpzact=vpzact,
                                   load_document=download_document, text=text)

                label = "The report can be downloaded at"
                (download_url, more_text) = self.get_download_url(
                         request=request, download_document=download_document)

                if format=='html' :
                    context = dict()
                    context['title'] = 'Report (input and output simulation)'
                    context['headsubtitle'] = headsubtitle
                    form = TodownloadUrlForm({'url':download_url,
                                              'more':more_text})
                    form.set(label=label, lifetime=DOWNLOAD_LIFETIME)
                    context['form'] = form
                    response = Response(context)
                else :
                    response = self.download_response(url=download_url,
                                             label=label, more_text=more_text)
            else :
                response = self.zip_response(zip_path=zip_folder_path)
    
        except Exception as e :
            s = get_error_status(e)
            errormsg = build_error_message(error=e)
            logger_report_error(LOGGER)
            if format=='html' :
                context = self.html_error_context(status=s, detail=errormsg)
                response = Response(context)
            else :
                response = Response(data={'detail':errormsg,}, status=s)

        return response

class ReportConditionsView(ReportViewMixin, VpzActDetailViewMixin,
                           PersistenceViewMixin, InputViewMixin,
                           StyleViewMixin, FormatViewMixin, DataViewMixin,
                           DetailViewMixin, generics.RetrieveAPIView):
    """Input information of a vpz as report(s)

    Creates VpzInput information of a VpzAct by doing the required \
    operations : maybe has modified the vpz file relative to VpzAct (only in \
    POST command case), reads VpzInput. Produces and returns the required \
    report folder.

    The VpzAct is created from a VpzPath or from a VleVpz.
    
    The report produced/returned is xls files, about simulation conditions.

    The 'bycol' mode option is used to choose to have some arrays by columns \
    (by default : by rows).

    The 'todownload' mode option is used to choose to download the result.
 
    After restitution, VpzAct (and its 'dependencies/tree') may be deleted \
    or kept into the database depending on persistence management.
    """

    content_negotiation_class = FormatContentNegotiation

    def get_object(self):
        data = self.data_from_request(self.request)
        self.vpzactid = None # default
        if self.request.method == 'POST' :
            try :
                vpzact = self.action_input_post(LOGGER=LOGGER, data=data)
                self.vpzactid = vpzact.id
                return vpzact
            except :
                raise
        else : # 'GET'
            try :
                vpzact = self.action_input_get(LOGGER=LOGGER, data=data)
                self.vpzactid = vpzact.id
                return vpzact
            except :
                raise

    def common(self, request, format=None, **kwargs):
        """Generic method for get and post """

        data = self.data_from_request(self.request)
        if format is None :
            format = self.get_format_value(data=data)
        bycol = self.get_bycol_value(data=data)
        todownload = self.get_todownload_value(data=data)

        response = self.report_response(request=request, bycol=bycol,
                            todownload=todownload, format=format, **kwargs)
        if self.vpzactid :
            vpzact = VpzAct.objects.get(pk=self.vpzactid)
            self.manages_persistence(vpzact=vpzact)
            ActivityLock.propagate_lock(vpzact)
        return response

    def get(self, request, format=None, **kwargs):
        """Reads VpzInput, returns simulation conditions (from VpzInput) \
           into a report folder.
        """

        return self.common(request, format, **kwargs)

    def post(self, request, format=None, **kwargs):
        """Modifies the vpz file relative to vpzact according to post data, \
           reads VpzInput, returns simulation conditions (from VpzInput) \
           into a report folder.
        """

        return self.common(request, format, **kwargs)

    def report_response(self, request, bycol, todownload, format, **kwargs):
        """Response about simulation conditions (from VpzInput)"""

        try :
            vpzact = self.get_object()
            vpzact.vpzworkspace.clean_reporthome()
            self.build_conditions_folder(vpzact=vpzact, bycol=bycol)
            zip_folder_path = self.build_zip_folder(vpzact=vpzact)

            if todownload :
                download_document = self.build_download_folder(
                                                    file_path=zip_folder_path)

                text ="lock dedicated to DownloadVpzDocument "
                text = text+ "(id " + str(download_document.id) + ")"
                ActivityLock.forward_lock(vpzact = vpzact,
                                 load_document = download_document, text=text)

                label = "The report can be downloaded at"
                (download_url, more_text) = self.get_download_url(
                         request=request, download_document=download_document)

                if format=='html' :
                    context = dict()
                    context['title'] = 'Report (input simulation)'
                    context['headsubtitle'] = headsubtitle
                    form = TodownloadUrlForm({'url':download_url,
                                              'more':more_text})
                    form.set(label=label, lifetime=DOWNLOAD_LIFETIME)
                    context['form'] = form
                    response = Response(context)
                else :
                    response = self.download_response(url=download_url,
                                             label=label, more_text=more_text)
            else :
                response = self.zip_response(zip_path=zip_folder_path)
    
        except Exception as e :
            s = get_error_status(e)
            errormsg = build_error_message(error=e)
            logger_report_error(LOGGER)
            if format=='html' :
                context = self.html_error_context(status=s, detail=errormsg)
                response = Response(context)
            else :
                response = Response(data={'detail':errormsg,}, status=s)

        return response

class ExperimentView(ExperimentViewMixin, VpzActDetailViewMixin,
                     PersistenceViewMixin, ActivityViewMixin,
                     StyleViewMixin, FormatViewMixin, DataViewMixin,
                     DetailViewMixin, generics.RetrieveAPIView):
    """Experiment of a vpz, into a xls file

    In GET case, the returned xls file contains the experiment conditions \
    in their original state.  

    In POST case, the returned xls file contains the simulation results and \
    the experiment conditions that have been modified according to the xls \
    file given by the user into the POST request.
    
    Important : \
    The posted xls file (POST vpz/experiment request) has the same format \
    and structure than the xls file returned by GET vpz/experiment request.

    The VpzAct is created from a VpzPath or from a VleVpz.

    The 'todownload' mode option is used to choose to download the result.

    After restitution, VpzAct (and its 'dependencies/tree') may be deleted \
    or kept into the database depending on persistence management.
    """

    content_negotiation_class = FormatContentNegotiation

    def get_object(self):
        data = self.data_from_request(self.request)
        self.vpzactid = None # default
        if self.request.method == 'POST' :
            try :
                vpzact = self.action_experiment_post(LOGGER=LOGGER,
                                                     data=self.request.data)
                self.vpzactid = vpzact.id
                return vpzact
            except :
                raise
        else : # 'GET'
            try :
                vpzact = self.action_experiment_get(LOGGER=LOGGER, data=data)
                self.vpzactid = vpzact.id
                return vpzact
            except :
                raise

    def common(self, request, format=None, **kwargs):
        """Generic method for get and post """

        data = self.data_from_request(self.request)
        if format is None :
            format = self.get_format_value(data=data)
        todownload = self.get_todownload_value(data=data)
        response = self.experiment_response(request=request,
                            todownload=todownload, format=format, **kwargs)

        if self.vpzactid :
            vpzact = VpzAct.objects.get(pk=self.vpzactid)
            self.manages_persistence(vpzact=vpzact)
            ActivityLock.propagate_lock(vpzact)
        return response

    def get(self, request, format=None, **kwargs):
        """Experiment conditions of a vpz, into a xls file

        The returned xls file contains the experiment conditions (input \
        information) : begin, duration and parameters values in their \
        original state, information to identify all the existing vlecond, \
        vlepar, vleview, vleout (forcing all the parameters and output datas \
        identification : outselect=all, parselect=all).  

        Note : the returned file will be useful to build POST requests.
        """

        return self.common(request, format, **kwargs)

    def post(self, request, format=None, **kwargs):
        """Experiment conditions and results of a vpz, into a xls file

        Modifies VpzInput according to POST FILE data (content of the xls \
        file given by the user into the POST request), simulates it and \
        returns input and output information (from VpzInput, VpzOutput) into \
        a xls file.

        The returned xls file contains the simulation results (output \
        information) and the experiment conditions (input information) : \
        begin, duration and parameters values modified according to POST FILE \
        data. Only the selected vlecond, vlepar, vleview, vleout are returned. 

        Note :
        The xls file given by the user into the POST request has the same \
        format and structure than the xls file returned by a \
        GET vpz/experiment request. To build the POSTed file, \
        GET vpz/experiment may help.
        """

        return self.common(request, format, **kwargs)

    def experiment_response(self, request, todownload, format, **kwargs):
        """Response about experiment (from VpzAct)"""

        try :
            vpzact = self.get_object()
            vpzact.vpzworkspace.clean_reporthome()

            if self.request.method == 'POST' :
                self.build_experiment_out_folder(vpzact=vpzact)
                title = 'Experiment (conditions and results)'
            else : # 'GET'
                self.build_experiment_in_folder(vpzact=vpzact)
                title = 'Experiment (conditions)'

            zip_folder_path = self.build_zip_folder(vpzact=vpzact)
    
            if todownload :
                download_document = self.build_download_folder(
                                                    file_path=zip_folder_path)

                text ="lock dedicated to DownloadVpzDocument "
                text = text+ "(id " + str(download_document.id) + ")"
                ActivityLock.forward_lock(vpzact=vpzact,
                                   load_document=download_document, text=text)

                label = "The experiment report can be downloaded at"
                (download_url, more_text) = self.get_download_url(
                         request=request, download_document=download_document)

                if format=='html' :
                    context = dict()
                    context['title'] = title
                    context['headsubtitle'] = headsubtitle
                    form = TodownloadUrlForm({'url':download_url,
                                              'more':more_text})
                    form.set(label=label, lifetime=DOWNLOAD_LIFETIME)
                    context['form'] = form
                    response = Response(context)
                else :
                    response = self.download_response(url=download_url,
                                             label=label, more_text=more_text)
            else :
                response = self.zip_response(zip_path=zip_folder_path)

        except Exception as e :
            s = get_error_status(e)
            errormsg = build_error_message(error=e)
            logger_report_error(LOGGER)
            if format=='html' :
                context = self.html_error_context(status=s, detail=errormsg)
                response = Response(context)
            else :
                response = Response(data={'detail':errormsg,}, status=s)

        return response

