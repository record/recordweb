"""erecord_vpz.views.objects_mixins

Mixins common to erecord_vpz application views (objects)

"""

from erecord_vpz.serializers import VpzActSerializerLink
from erecord_vpz.serializers import VpzActSerializerTree
from erecord_vpz.serializers import VpzInputSerializerLink
from erecord_vpz.serializers import VpzInputSerializerTree
from erecord_vpz.serializers import VpzOutputSerializerLink
from erecord_vpz.serializers import VpzOutputSerializerTree

from erecord_vpz.forms import VpzInputUserForm
from erecord_vpz.compact.serializers import VpzInputCompact
from erecord_vpz.compact.serializers import VpzInputCompactSerializer

from erecord_vpz.compact.forms import VpzInputCompactUserForm
from erecord_vpz.compact.forms import VleParCompactUserForm
from erecord_vpz.compact.forms import VleCondCompactUserForm
from erecord_vpz.compact.forms import VleViewCompactUserForm
from erecord_vpz.compact.forms import VleOutCompactUserForm

from erecord_vpz.forms import VleBeginUserForm
from erecord_vpz.forms import VleDurationUserForm
from erecord_vpz.forms import VleCondUserForm
from erecord_vpz.forms import VleParUserForm
from erecord_vpz.forms import VleViewUserForm
from erecord_vpz.forms import VleOutUserForm


class VpzActDetailViewMixin(object):
    """Additional methods for views about VpzAct"""

    def get_style_value( self ):
        """Returns the style value according to data

        style = tree or link. Default value : tree.
        """

        data = self.data_from_request(self.request)
        mode_options = self.get_mode_option_values(data=data)
        style = 'tree' # default
        if mode_options is not None :
            if 'tree' not in mode_options :
                if 'link' in mode_options :
                    style = 'link'
        return style

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                        serializer_tree_class=VpzActSerializerTree,
                        serializer_link_class=VpzActSerializerLink)

class VpzOutputDetailViewMixin(object):
    """Additional methods for views about VpzOutput"""

    def get_style_value( self ):
        """Returns the style value according to data

        style = tree, link, compact or compactlist. Default value : tree.
        """

        data = self.data_from_request(self.request)
        mode_options = self.get_mode_option_values(data=data)
        style = 'tree' # default
        if mode_options is not None :
            if 'tree' not in mode_options :
                if ('compactlist' in mode_options) \
                or ('compact' in mode_options) :
                    style = 'compact'
                if 'link' in mode_options :
                    style = 'link'
        return style

    def get_renderers( self ):
        from erecord_cmn.renderers import ZIPRenderer
        template_name='erecord_vpz/headedform_vpzoutput_detail.html'
        r = self.get_specific_renderers(template_name)
        r.append( ZIPRenderer() )
        return r

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                        serializer_tree_class=VpzOutputSerializerTree,
                        serializer_link_class=VpzOutputSerializerLink)

class VpzInputDetailViewMixin(object):
    """Additional methods for views about VpzInput"""

    def get_style_value( self ):
        """Returns the style value according to data

        style = tree, link, compact or compactlist. Default value : compact.
        """

        data = self.data_from_request(self.request)
        mode_options = self.get_mode_option_values(data=data)
        style = 'compact' # default
        if mode_options is not None :
            if 'compact' not in mode_options :
                if 'compactlist' in mode_options :
                    style = 'compactlist'
                if 'tree' in mode_options :
                    style = 'tree'
                if 'link' in mode_options :
                    style = 'link'
        return style

    def get_renderers( self ):
        style = self.get_style_value()
        if style == 'compact' :
            template_name='erecord_vpz/headedform_vpzinput_compact_detail.html'
        elif style == 'compactlist' :
            template_name='erecord_vpz/headedform_vpzinput_compactlist_detail.html'
        else :
            template_name='erecord_vpz/headedform_vpzinput_detail.html'
        r = self.get_specific_renderers(template_name)
        return r

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                        serializer_tree_class=VpzInputSerializerTree,
                        serializer_link_class=VpzInputSerializerLink)

    def html_compact_context(self, vpzinput, is_compactlist=False,
                             headsubtitle=""):
        """Builds and returns the html context relative to the Vpzinput, \
           in compact style case.

        More precisely, style may be 'compact' (is_compactlist False) or \
        'compactlist' (is_compactlist True).

        Only the selected VlePar and selected VleOut (and their VleView) of \
        VpzInput are kept.
        """

        vpzinputcompact = VpzInputCompact()
        vpzinputcompact.init_from_vpzinput(vpzinput)
        context = dict()

        if is_compactlist : # 'compactlist' style
            context['title'] = 'Vpz input (compact list)'
        else : # 'compact' style
            context['title'] = 'Vpz input (compact)'

        context['headsubtitle'] = headsubtitle
    
        context['form'] = VpzInputCompactUserForm(initial={
                                        'begin':vpzinputcompact.begin, 
                                        'duration':vpzinputcompact.duration})

        form_par_list = list()
        for par in vpzinputcompact.pars :
            form_par_list.append(VleParCompactUserForm(instance=par))
        context['form_pars'] = form_par_list

        form_cond_list = list()
        for cond in vpzinputcompact.conds :
            form_cond_list.append(VleCondCompactUserForm(instance=cond))
        context['form_conds'] = form_cond_list

        form_out_list = list()
        for out in vpzinputcompact.outs :
            form_out_list.append(VleOutCompactUserForm(instance=out))
        context['form_outs'] = form_out_list

        form_view_list = list()
        for view in vpzinputcompact.views :
            form_view_list.append(VleViewCompactUserForm(instance=view))
        context['form_views'] = form_view_list
        return context

    def html_nocompact_context(self, vpzinput, headsubtitle=""):
        """Builds and returns the html context relative to the Vpzinput, \
           in no compact style case.

        More precisely, style does not value 'compact' neither 'compactlist'. \
        style may be 'link' or 'tree'.

        Only the selected VlePar and selected VleOut (and their VleView) of \
        VpzInput are kept.
        """

        context = dict()
        context['title'] = 'Vpz input'
        context['headsubtitle'] = headsubtitle
    
        context['form'] = VpzInputUserForm(instance=vpzinput)
        context['form_vlebegin'] = VleBeginUserForm(
                                            instance=vpzinput.vlebegin)
        context['form_vleduration'] = VleDurationUserForm(
                                            instance=vpzinput.vleduration)
    
        vlecond_list = list()
        for vlecond in vpzinput.vlecond_list.all() :
            form_vlepar_list = [VleParUserForm(instance=vlepar) 
                                    for vlepar in vlecond.vlepar_list.all() 
                                    if vlepar.is_selected()]
            if form_vlepar_list:
                form_vlecond = VleCondUserForm(instance=vlecond) 
                vlecond_list.append( (form_vlecond, form_vlepar_list) )
        context['vlecond_list'] = vlecond_list

        vleview_list = list()
        for vleview in vpzinput.vleview_list.all() :
            form_vleout_list = [VleOutUserForm(instance=vleout) 
                                    for vleout in vleview.vleout_list.all() 
                                    if vleout.is_selected()]
            if form_vleout_list:
                form_vleview = VleViewUserForm(instance=vleview) 
                vleview_list.append( (form_vleview, form_vleout_list) )
        context['vleview_list'] = vleview_list
        return context

    def nohtml_compact_context(self, vpzinput):
        """Builds and returns the context relative to the Vpzinput, \
           in case of no html format and 'compact' style.

        More precisely, format does not value 'html'. format may value \
        'json', 'api', etc. style values 'compact'.

        Only the selected VlePar and selected VleOut (and their VleView) of \
        VpzInput are kept.
        """

        vpzinputcompact = VpzInputCompact()
        vpzinputcompact.init_from_vpzinput(vpzinput)
        serializer = VpzInputCompactSerializer(vpzinputcompact)
        return serializer.data

    def nohtml_compactlist_context(self, vpzinput):
        """Builds and returns the context relative to the Vpzinput, \
           in case of no html format and 'compactlist' style.

        More precisely, format does not value 'html'. format may value \
        'json', 'api', etc. style values 'compactlist'.

        Only the selected VlePar and selected VleOut (and their VleView) of \
        VpzInput are kept.
        """

        data = dict()
        data['begin'] = vpzinput.vlebegin.value
        data['duration'] = vpzinput.vleduration.value
        vlecondcompact_list = list()
        for c in vpzinput.vlecond_list.all() :
            at_least_one_vlepar_selected = False
            for p in c.vlepar_list.all() :
                if p.is_selected():
                    data[p.build_selection_name()] = p.value
                    at_least_one_vlepar_selected = True
            if at_least_one_vlepar_selected:
                vlecondcompact_list.append(c.build_selection_name())
        data['conds'] = vlecondcompact_list
        vleviewcompact_list = list()
        vleoutcompact_list = list()
        for v in vpzinput.vleview_list.all() :
            at_least_one_vleout_selected = False
            for o in v.vleout_list.all() :
                if o.is_selected():
                    vleoutcompact_list.append( o.build_selection_name() )
                    at_least_one_vleout_selected = True
            if at_least_one_vleout_selected:
                vleviewcompact_list.append(v.build_selection_name())
        data['views'] = vleviewcompact_list
        data['outs'] = vleoutcompact_list
        return data

