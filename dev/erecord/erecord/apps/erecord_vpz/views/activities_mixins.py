"""erecord_vpz.views.activities_mixins

Mixins common to erecord_vpz application views (activities)

"""

from django.http import Http404

from erecord_vpz.models import VpzAct
from erecord_vpz.models import VpzOrigin
from erecord_vpz.models import VpzInput
from erecord_vpz.models import VpzOutput
from erecord_vpz.models import VpzWorkspace
from erecord_vpz.models import VpzPath

from erecord_acs.models import ActivityLock

from erecord_db.models import VleVpz

from erecord_cmn.views_mixins import ErrorViewMixin
from erecord_cmn.views_mixins import ModeOptionViewMixin
from erecord_acs.views_mixins import LimitedAccessViewMixin
from erecord_acs.views_mixins import LimitedAccessVpzInputViewMixin

from erecord_cmn.views_mixins import RenderViewMixin
from rest_framework.renderers import TemplateHTMLRenderer

from erecord_vpz.compact.views_mixins import VleParCompactOptionViewMixin

from erecord_vpz.serializers import VpzSelectionOptionsSerializer
from erecord_cmn.serializers import getKeyOptionValues

from erecord_vpz.compact.serializers import VpzInputCompact
from erecord_vpz.compact.serializers import VpzInputCompactSerializer

from erecord_cmn.utils.dir_and_file import save_posted_file
from erecord_cmn.utils.dir_and_file import unzip_file

from erecord_cmn.utils.coding import get_val

from erecord_cmn.utils.errors import logger_report_error
from erecord_cmn.utils.errors import build_error_message
from erecord_cmn.utils.errors import Http400

#------------------------------------------------------------------------------

class OutselectOptionViewMixin(object):
    """Additional methods for views having outselect option
    
    The 'outselect' option is used to choose restituted output datas.
 
    - Value "all" : to select all output datas of all views
    - Value viewname : to select all output datas of the view named viewname
    - Value vname.oname : to select the ouput data oname of the view named \
      vname (and unselect the out datas of this view that are not selected \
      like this)

    Attention :
    The selection of type vname.oname is available in 'dataframe' restype \
    case. It won't be completely applied in 'matrix' restype case, even if \
    it has an impact on the view named vname.
    """

    def get_outselect_option_values(self, data):
        """Option 'outselect' (several values are possible)
    
        Returns the list of outselect option values.
    
        The 'outselect' option is used to choose restituted output datas.
        'outsel' is also accepted
        """

        s = list(set(getKeyOptionValues(data=data, key='outselect')) | 
                 set(getKeyOptionValues(data=data, key='outsel')))
        outselect_option = None
        if len(s) > 0:
            outselect_option = s
        return outselect_option

class ParselectOptionViewMixin(object):
    """Additional methods for views having parselect option
    
    The 'parselect' option is used to choose restituted parameters.
 
    - Value "all" : to select all parameters of all conditions
    - Value condname : to select all parameters of the condition named condname
    - Value cname.pname : to select the parameter pname of the condition \
      named cname (and unselect the parameters of this condition that are \
      not selected like this)
    """

    def get_parselect_option_values(self, data):
        """Option 'parselect' (several values are possible)
    
        Returns the list of parselect option values.
    
        The 'parselect' option is used to choose restituted parameters.
        'parsel' is also accepted
        """

        s = list(set(getKeyOptionValues(data=data, key='parselect')) | 
                set(getKeyOptionValues(data=data, key='parsel')))
        parselect_option = None
        if len(s) > 0:
            parselect_option = s
        return parselect_option

class VpzInputCompactViewMixin(OutselectOptionViewMixin,
                     ParselectOptionViewMixin, VleParCompactOptionViewMixin):
    """Additional methods for views having options to modify VpzInput """

    def getVpzInputCompactOptionValues(self, data):
        """Options to modify vpz file (indirectly future read VpzInput)
        
        Returns (cr_ok, vpzinputcompact, outselect_option, pars)
        that are relative to VpzInputCompactSerializer,
        get_outselect_option_values and get_vleparcompact_option_values).

        Note :
        parselect_option (relative to get_parselect_option_values) is not 
        treated here ; it modifies VpzInput directly, not through
        modification of vpz file.
        """

        vpzinputcompact = VpzInputCompact()
        serializer = VpzInputCompactSerializer(instance=vpzinputcompact,
                                               data=data)
        cr_ok = serializer.is_valid()
        if cr_ok :
            serializer.save()
            outselect_option = self.get_outselect_option_values(data=data)
            vleparcompact_option = self.get_vleparcompact_option_values(
                    data=data)
        else :
            begin_value = None
            duration_value = None
            outselect_option = None
            vleparcompact_option = None
        return (cr_ok, serializer, vpzinputcompact, outselect_option,
                vleparcompact_option)

    def get_input_vpz_modifications(self, data):
        """Builds and returns information to modify vpz file

        Returns (storaged_list, conditions, begin, duration) or an error
        message (str) in error case.

        data information can correspond to VpzInputCompactSerializer format,
        where parameters are given in 'pars'. Each parameter 
        ('cname', 'pname', 'value') can also be given as the data 
        'pname'.'cname' with value 'value'. If a parameter value was given 
        in both ways, there is no guarantee about which one would be kept.

        data information can contain the 'outselect' option that is used
        to select the restituted output datas (see OutselectOptionViewMixin :
        all, view, output data...). This 'outselect' option is applied to the
        vpz file.

        data information can contain the 'parselect' option that is used
        to select the restituted parameters (see ParselectOptionViewMixin :
        all, cond, parameter...). This 'parselect' option is NOT taken into 
        account here (it will then be applied directly on VpzInput).

        """

        ret = None # default
        (cr_tmp, serializer, vpzinputcompact, outselect_option,
         vleparcompact_option) = self.getVpzInputCompactOptionValues(data=data)
        if cr_tmp :
            storaged_list = outselect_option
            conditions = dict()
            for par in vleparcompact_option :
                cname = par.cname
                pname = par.pname
                value = get_val(par.value)
                if cname not in conditions.keys():
                    conditions[cname] = dict()
                conditions[cname][pname] = value
            for par in vpzinputcompact.pars :
                cname = par.cname
                pname = par.pname
                value = get_val(par.value)
                if cname not in conditions.keys():
                    conditions[cname] = dict()
                conditions[cname][pname] = value
            begin = vpzinputcompact.begin
            duration = vpzinputcompact.duration
            ret = (storaged_list, conditions, begin, duration)
        else :
            errormsg = "Bad request : " + str(serializer.errors)
            ret = errormsg
        return ret

class DataFolderViewMixin(object):
    """Additional methods for views where 'data' folder can be modified
    
    The 'data' folder of the main vle package can be modified.

    Option datafolder : zip file containing the new 'data' folder to be taken
    into account. Something else than the 'data' folder that may be contained
    into the datafolder zip file won't be considered. 

    Option datafoldercopy : the way how to take into account datafolder.

    - Value replace : the content of (the 'data' folder contained into) the \
      datafolder zip file replaces the original 'data' folder.
    - Value overwrite : the content of (the 'data' folder contained into) \
      the datafolder zip file is added to the original 'data' folder (by \
      overwriting).

    Note : At this level, the new 'data' folder to be taken into account is
    just prepared. It will be used into actions_vpz.
    """

    def may_be_datafolder_case(self, data) :
        """Verifies if datafolder option given

        datafolder values ignored : "", None.

        Note : the zip file content is not taken into account
        """

        if 'datafolder' in data.keys() :
            ret = True
            if data['datafolder'] == "":
                ret = False
            if data['datafolder'] is None :
                ret = False
        else : 
            ret = False
        return ret

    def is_ready_datafolder_case(self, data, vpzact) :
        """Verifies datafolder is ready to be treated 

        Conditions to verify are :

        - datafolder option given
        - data folder has been found and extracted from zip file
        """

        ret = False # default
        if self.may_be_datafolder_case(data) :

            # verifies 'data' folder found under datahome
            w = vpzact.vpzworkspace
            (cr_ok, datafolder_src_path, data_name) = \
                                       w.get_and_verify_datafolder_src_path()
            if cr_ok :
                ret = True
        return ret

    def prepare_datafolder(self, LOGGER, data, vpzact) :
        """Updates the 'data' folder

        Prepares the 'data' folder (dedicated to the main vle package)
        from to the datafolder option (zip file containing 'data' folder).

        datafolder is supposed to contain a zip file.
        else : raise error
        """

        if self.may_be_datafolder_case(data) :

            cr_ok = False # default
            w = vpzact.vpzworkspace
            w.clean_datahome()
            datafolder_zip_file_path = w.get_datafolder_zip_file_path()
            try :
                cr_ok = save_posted_file(data=data, key='datafolder',
                                         filepath=datafolder_zip_file_path)
            except :
                raise
            if not cr_ok :
                errormsg = "Unable to satisfy the request"
                raise Http400(errormsg)
            try :
                cr_ok = unzip_file(zip_file_path=datafolder_zip_file_path,
                                   path=w.get_datahome())
            except :
                raise

            # verifies 'data' folder found under datahome
            (cr_ok, datafolder_src_path, data_name) = \
                                       w.get_and_verify_datafolder_src_path()
            if not cr_ok :
                errormsg = "Bad request : folder named '"+data_name+"' not "
                errormsg += "found into the zip file received by the "
                errormsg += "datafolder option. This zip file must contain a "
                errormsg += "folder named '"+data_name+"' that contains all "
                errormsg += "the data files to be taken into account."
                raise Http400(errormsg)

class ActivityViewMixin(LimitedAccessViewMixin, RenderViewMixin,
               ErrorViewMixin, VpzInputCompactViewMixin, ModeOptionViewMixin):
    """Additional methods for activities views"""

    def get_renderers( self ):
        r = super(ActivityViewMixin, self).get_renderers()
        renderer = TemplateHTMLRenderer()
        renderer.template_name='erecord_cmn/headedform_under_construction.html'
        r.append( renderer )
        return r

    def init_activity(self, LOGGER, data):
        """Initial build
        
        Builds VpzAct, VpzOrigin, VpzWorspace

        Receives and controls token in case of locked vpz (vpzpath or vlevpz).

        Returns VpzAct if no error, and raises exception else.

        Note : VpzInput and VpzOutput are not built there
        """

        p = VpzSelectionOptionsSerializer(data=data)
        p.is_valid()
        vpzpath_id = p.data['vpzpath']
        vlevpz_id = p.data['vpz']
        if vpzpath_id is not None :
            try:
                source = VpzPath.objects.get(pk=vpzpath_id)
            except VpzPath.DoesNotExist:
                errormsg = "Vpz path not found : VpzPath with id value = "+ \
                        str(vpzpath_id)+" does not exist " + \
                        "(option vpzpath="+str(vpzpath_id)+")"
                raise Http404(errormsg)
        elif vlevpz_id is not None :
            try:
                source = VleVpz.objects.get(pk=vlevpz_id)
            except VleVpz.DoesNotExist:
                errormsg = "Vpz file not found : VleVpz with id value = "+ \
                        str(vlevpz_id)+" does not exist " + \
                        "(option vpz="+str(vlevpz_id)+")"
                raise Http404(errormsg)
        else :
            errormsg = "Bad request : option vpz or vpzpath required"
            raise Http400(errormsg)

        # Important :
        # since all the activities call init_activity (even Experiment),
        # jwt control is done there where there is all the needed information  
        has_lock = source.has_lock()
        access_ok = True # default
        user_id = None
        if has_lock: # limited source case
            access_ok = False # before control
            try : # try to unlock source
                user_id = self.successful_unlocking(model=source, data=data,
                                                    LOGGER=LOGGER)
                access_ok = True # since no exception sent
            except : # in particular if unsuccessful unlocking
                raise

        if access_ok :
            try:
                d = source.for_vpzact_creation()
                vpzact = VpzAct(vpzname=d['vpzname'], pkgname=d['pkgname'], 
                                vlepath=d['vlepath'],
                                vleversion=d['vleversion'])
                vpzact.full_clean()
                vpzact.save()
                if has_lock:
                    ActivityLock.create(vpzact, user_id)

            except Exception as e :
                errormsg = build_error_message(error=e,
                                         msg="Unable to satisfy the request")
                logger_report_error(LOGGER)
                raise Exception(errormsg)
    
            vpzorigin = VpzOrigin.create(vpzact=vpzact, text=d['textorigin'])
            vpzorigin.full_clean()
            vpzorigin.save()
        
            # reporthome, datahome, runhome subworkspaces are created
            try :
                vpzworkspace = VpzWorkspace.create(vpzact=vpzact,
                        as_reporthome=True, as_datahome=True, as_runhome=True)
                vpzworkspace.full_clean()
                vpzworkspace.save()
            except Exception as e :
                msg = "Unable to satisfy the request"
                errormsg = build_error_message(error=e, msg=msg)
                logger_report_error(LOGGER)
                raise Exception(errormsg)
    
            return vpzact

    def build_input_information(self, LOGGER, data, vpzact,
                                with_modification):
        """Builds input information used for VpzInput

        Builds input information (jres) from vpz file (relative to vpzact),
        having before taken into account data information if with_modification.

        Returns jres if no error, and raises exception else.

        Attention : the parselect option (among data options) is NOT taken
        into account here.

        Note : data options, other than parselect, are taken into account 
        directly into vpz file (modified before to be read).
        """

        try :
            w = vpzact.vpzworkspace
            if with_modification :

                r = self.get_input_vpz_modifications(data=data)
                if type(r) is str : # error case
                    errormsg = r
                    raise Exception(errormsg)
                else :
                    (storaged_list, conditions, begin, duration) = r
                    (jres_read_part, jres_run_part) = w.actions_vpz(
                                              storaged_list=storaged_list,
                                              conditions=conditions,
		                              begin=begin, duration=duration,
                                              action_modify_vpz=True,
                                              action_read_vpz=True)
		                              #plan=None, restype=None,
                                              #action_run_vpz=False,
                                              #action_copy_datafolder=False,
                                              #datafoldercopy=None,
                                              #action_copy_modified_vpz=False,
                    jres = jres_read_part
            else :
                (jres_read_part, jres_run_part) = w.actions_vpz(
                                              action_read_vpz=True)
                                              #storaged_list=None,
                                              #conditions=None,
		                              #begin=None, duration=None,
		                              #plan=None, restype=None,
                                              #action_modify_vpz=False,
                                              #action_run_vpz=False,
                                              #action_copy_datafolder=False,
                                              #datafoldercopy=None,
                                              #action_copy_modified_vpz=False,
                jres = jres_read_part
        except Exception as e :
            msg = "Unable to satisfy the request"
            errormsg = build_error_message(error=e, msg=msg)
            logger_report_error(LOGGER)
            raise Exception(errormsg)

        return jres


    def build_output_information(self, LOGGER, data, vpzact,
                                 with_modification):
        """Builds output information used for VpzOutput
        
        Runs the vpz file (relative to vpzact) simulation according to plan
        and restype, having before taken into account data information if
        with_modification. Builds output information (jres).

        Returns jres if no error, and raises exception else.

        Note : data options, other than plan and restype are taken into
        account directly into vpz file (modified before to be run).
        """

        try :
            w = vpzact.vpzworkspace
            plan = self.get_plan_value(data)
            restype = self.get_restype_value(data)

            if with_modification :
                r = self.get_input_vpz_modifications(data=data)
                if type(r) is str : # error case
                    errormsg = r
                    raise Exception(errormsg)
                else :
                    (storaged_list, conditions, begin, duration) = r

                    if self.is_ready_datafolder_case(data=data,
                                                     vpzact=vpzact) :
                        action_copy_datafolder = True
                        datafoldercopy = self.get_datafoldercopy_value(
                                                                   data=data)
                    else :
                        action_copy_datafolder = False
                        datafoldercopy = None

                    (jres_read_part, jres_run_part) = w.actions_vpz(
                              storaged_list=storaged_list,
                              conditions=conditions,
                              begin=begin, duration=duration,
                              plan=plan, restype=restype,
                              action_modify_vpz=True,
                              action_run_vpz=True,
                              action_copy_datafolder=action_copy_datafolder,
                              datafoldercopy=datafoldercopy)
                              #action_read_vpz=False,
                              #action_copy_modified_vpz=False,
                    jres = jres_run_part
            else :
                (jres_read_part, jres_run_part) = w.actions_vpz(
                                          plan=plan, restype=restype,
                                          action_run_vpz=True)
                                          #storaged_list=None,
                                          #conditions=None,
                                          #begin=None, duration=None,
                                          #action_modify_vpz=False,
                                          #action_read_vpz=False,
                                          #action_copy_datafolder=False,
                                          #datafoldercopy=None,
                                          #action_copy_modified_vpz=False,
                jres = jres_run_part

        except Exception as e :
            msg = "Unable to satisfy the request"
            errormsg = build_error_message(error=e, msg=msg)
            logger_report_error(LOGGER)
            raise Exception(errormsg)

        return jres


    def build_inoutput_information(self, LOGGER, data, vpzact,
                                   with_modification,
                                   with_saving_vpz_file=False):
        """Builds input and output information used for VpzInput and VpzOutput
        
        Builds input information from vpz file (relative to vpzact),
        having before taken into account data information if with_modification.

        Runs the vpz file (relative to vpzact) simulation according to
        previous modifications, plan and restype. Builds output information.

        Saves the maybe modified vpz file if with_saving_vpz_file.
       
        Returns jres as (jres_read_part, jres_run_part) if no error, and
        raises exception else.

        Attention : the parselect option (among data options) is NOT taken
        into account here. Other data options, other than plan and restype,
        are taken into account directly into vpz file (modified before to be
        read and run).
        """

        try :
            w = vpzact.vpzworkspace
            plan = self.get_plan_value(data)
            restype = self.get_restype_value(data)

            if with_modification :
                r = self.get_input_vpz_modifications(data=data)
                if type(r) is str : # error case
                    errormsg = r
                    raise Exception(errormsg)
                else :
                    (storaged_list, conditions, begin, duration) = r

                    if self.is_ready_datafolder_case(data=data,
                                                     vpzact=vpzact) :
                        action_copy_datafolder = True
                        datafoldercopy = self.get_datafoldercopy_value(
                                                                   data=data)
                    else :
                        action_copy_datafolder = False
                        datafoldercopy = None

                    (jres_read_part, jres_run_part) = w.actions_vpz(
                                storaged_list=storaged_list,
                                conditions=conditions,
                                begin=begin, duration=duration,
                                plan=plan, restype=restype,
                                action_modify_vpz=True,
                                action_read_vpz=True,
                                action_run_vpz=True,
                                action_copy_datafolder=action_copy_datafolder,
                                datafoldercopy=datafoldercopy,
                                action_copy_modified_vpz=with_saving_vpz_file)
                    jres = (jres_read_part, jres_run_part)
            else :
                (jres_read_part, jres_run_part) = w.actions_vpz(
                                plan=plan, restype=restype,
                                action_read_vpz=True,
                                action_run_vpz=True,
                                action_copy_modified_vpz=with_saving_vpz_file)
                                #storaged_list=None,
                                #conditions=None,
                                #begin=None, duration=None,
                                #action_modify_vpz=False,
                                #action_copy_datafolder=False,
                                #datafoldercopy=None,
                jres = (jres_read_part, jres_run_part)

        except Exception as e :
            msg = "Unable to satisfy the request"
            errormsg = build_error_message(error=e, msg=msg)
            logger_report_error(LOGGER)
            raise Exception(errormsg)

        return jres # as (jres_read_part, jres_run_part) 

class InputViewMixin(ActivityViewMixin, LimitedAccessVpzInputViewMixin):
    """Additional methods for activities views about input of a vpz"""

    def action_input_get(self, LOGGER, data):
        """Action done for a GET request about input of a vpz"""

        try :
            res = self.init_activity(LOGGER=LOGGER, data=data)
        except :
            raise
        vpzact = res
        try :
            vpzinput = VpzInput.create(vpzact=vpzact)
            vpzinput.full_clean()
            vpzinput.save()
            jres = self.build_input_information(LOGGER=LOGGER, data=data,
                                                vpzact=vpzact,
                                                with_modification=False)
            parselect = self.get_parselect_option_values(data=data)
            vpzinput.add_content(jres, parselect)
        except :
            raise
        vpzact = res
        return vpzact

    def action_input_post(self, LOGGER, data):
        """Action done for a POST request about input of a vpz"""

        try :
            res = self.init_activity(LOGGER=LOGGER, data=data)
        except :
            raise
        vpzact = res
        try :
            vpzinput = VpzInput.create(vpzact=vpzact)
            vpzinput.full_clean()
            vpzinput.save()
            jres = self.build_input_information(LOGGER=LOGGER, data=data,
                                                vpzact=vpzact,
                                                with_modification=True)
            parselect = self.get_parselect_option_values(data=data)
            vpzinput.add_content(jres, parselect)
        except :
            raise
        vpzact = res
        return vpzact

class OutputViewMixin(DataFolderViewMixin, ActivityViewMixin):
    """Additional methods for activities views about output of a vpz"""

    def action_output_get(self, LOGGER, data):
        """Action done for a GET request about output of a vpz

        The simulation running depends on the 'mode' option that data
        information may contain, including plan and restype information.
        """

        try : 
            res = self.init_activity(LOGGER=LOGGER, data=data)
            vpzact = res

            vpzoutput = VpzOutput.create(vpzact=vpzact)
            vpzoutput.full_clean()
            vpzoutput.save()
            jres = self.build_output_information(LOGGER=LOGGER, data=data,
                                       vpzact=vpzact, with_modification=False)
            vpzoutput.add_content(jres)
            vpzoutput.full_clean()
            vpzoutput.save()

        except :
            raise
        return vpzact

    def action_output_post(self, LOGGER, data):
        """Action done for a POST request about output of a vpz

        The simulation running depends on the 'mode' option that data
        information may contain, including plan and restype information.
        """

        try :
            res = self.init_activity(LOGGER=LOGGER, data=data)
            vpzact = res

            self.prepare_datafolder(LOGGER=LOGGER, data=data, vpzact=vpzact)

            vpzoutput = VpzOutput.create(vpzact=vpzact)
            vpzoutput.full_clean()
            vpzoutput.save()
            jres = self.build_output_information(LOGGER=LOGGER, data=data,
                                        vpzact=vpzact, with_modification=True)
            vpzoutput.add_content(jres)
            vpzoutput.full_clean()
            vpzoutput.save()
        except :
            raise
        return vpzact


class InOutputViewMixin(DataFolderViewMixin, ActivityViewMixin):
    """Additional methods for activities views about input and output of a vpz
    """

    def action_inoutput_get(self, LOGGER, data, with_saving_vpz_file=False):
        """Action done for a GET request about input and output of a vpz

        The simulation running depends on the 'mode' option that data
        information may contain, including plan and restype information.

        The vpz file will be saved or not, depending on with_saving_vpz_file.
        """

        try : 
            res = self.init_activity(LOGGER=LOGGER, data=data)
            vpzact = res

            vpzinput = VpzInput.create(vpzact=vpzact)
            vpzinput.full_clean()
            vpzinput.save()

            vpzoutput = VpzOutput.create(vpzact=vpzact)
            vpzoutput.full_clean()
            vpzoutput.save()

            (jres_read_part, jres_run_part) = self.build_inoutput_information(
                                    LOGGER=LOGGER, data=data, vpzact=vpzact,
                                    with_modification=False,
                                    with_saving_vpz_file=with_saving_vpz_file)

            parselect = self.get_parselect_option_values(data=data)
            vpzinput.add_content(jres=jres_read_part, parselect=parselect)

            vpzoutput.add_content(jres_run_part)
            vpzoutput.full_clean()
            vpzoutput.save()

        except :
            raise
        return vpzact

    def action_inoutput_post(self, LOGGER, data, with_saving_vpz_file=False):
        """Action done for a POST request about input and output of a vpz

        The simulation running depends on the 'mode' option that data
        information may contain, including plan and restype information.

        The vpz file will be saved or not, depending on with_saving_vpz_file.
        """

        try :
            res = self.init_activity(LOGGER=LOGGER, data=data)
            vpzact = res

            vpzinput = VpzInput.create(vpzact=vpzact)
            vpzinput.full_clean()
            vpzinput.save()

            vpzoutput = VpzOutput.create(vpzact=vpzact)
            vpzoutput.full_clean()
            vpzoutput.save()

            self.prepare_datafolder(LOGGER=LOGGER, data=data, vpzact=vpzact)

            (jres_read_part, jres_run_part) = self.build_inoutput_information(
                                    LOGGER=LOGGER, data=data, vpzact=vpzact,
                                    with_modification=True,
                                    with_saving_vpz_file=with_saving_vpz_file)

            parselect = self.get_parselect_option_values(data=data)
            vpzinput.add_content(jres=jres_read_part, parselect=parselect)

            vpzoutput.add_content(jres_run_part)
            vpzoutput.full_clean()
            vpzoutput.save()
        except :
            raise
        return vpzact

