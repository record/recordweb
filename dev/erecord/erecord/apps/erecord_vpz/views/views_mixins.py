"""erecord_vpz.views.views_mixins

Mixins common to erecord_vpz application views

"""

from erecord_cmn.utils.errors import Http400

from erecord_vpz.models import VpzInput
from erecord_vpz.models import VpzOutput

from erecord_cmn.views_mixins import RenderViewMixin
from erecord_vpz.views.activities_mixins import DataFolderViewMixin

from erecord_cmn.serializers import getKeyOptionValues

from erecord_cmn.configs.config import ACTIVITY_PERSISTENCE

import os

from django.http import QueryDict

from rest_framework.renderers import TemplateHTMLRenderer

from erecord_cmn.utils.dir_and_file import make_zip_folder
from erecord_cmn.utils.dir_and_file import create_dirs_if_absent
from erecord_cmn.utils.dir_and_file import save_posted_file
from erecord_slm.views_mixins import VpzDownloadViewMixin

from erecord_cmn.utils.coding import get_val

from erecord_cmn.utils.errors import build_error_message

class PersistenceViewMixin(object):
    """Additional methods for persistence management
    
    The view must be in relation with a VpzAct.

    A VzpAct (and its dependencies/tree) may be deleted after restitution,
    depending on persistence management.

    """
    @classmethod
    def get_persistence_mode(cls):
        return ACTIVITY_PERSISTENCE

    def manages_persistence(self, vpzact):
        if not self.get_persistence_mode() :
            vpzact.delete()

class ReportViewMixin(VpzDownloadViewMixin, RenderViewMixin):
    """Additional methods for views producing and returning reports
    
    The 'report' option is used to choose what kind of report is \
    produced/returned about vpzact.
 
    """

    def get_renderers( self ):
        r = RenderViewMixin.get_renderers(self)
        renderer = TemplateHTMLRenderer()
        renderer.template_name='erecord_slm/headedform_downloading.html'
        r.append( renderer )
        return r

    def get_report_values(self, data):
        """Defines the list of report values deduced from 'report' option \
        (several values are possible).

        The resulting reports may content several values for the kinds of \
        report to be built and returned into the folder.

        Priority to 'all' value. Default values ['vpz', 'xls',]
        """

        available_values = ['vpz', 'csv', 'txt', 'xls',] # special case : 'all'
        
        s = getKeyOptionValues(data=data, key='report')
        report_option = list()
        if len(s) > 0:
            report_option = s

        report_option = list(set(report_option)) # unicity

        if 'all' in report_option :
            reports = [o for o in available_values] # all
        else :
            reports = list()
            if report_option :
                reports = [o for o in report_option if o in available_values]
            if not reports : # default
                reports = ['vpz', 'xls',]
        return reports

    def is_vpz_report(self, data) :
        """True if 'vpz' report is into data request """

        return ('vpz' in self.get_report_values(data=data))

    def build_zip_folder(self, vpzact):
        """Builds the zip file of the report folder, and returns its path. """

        zip_path = make_zip_folder(self.get_folder_path(vpzact=vpzact))
        return zip_path

    def get_folder_path(self, vpzact) :
        """Returns the report folder path name"""

        name = '__' + str(vpzact.id) + '__input__'
        return vpzact.vpzworkspace.get_report_folder_path(name)

    def build_folder(self, vpzact, reports, bycol) :
        """Builds content of the report folder depending on reports

        The content is relative to input and output information.
        """

        folderpath = self.get_folder_path(vpzact)

        if 'vpz' in reports:
            path = os.path.join(folderpath, 'exp')
            create_dirs_if_absent([folderpath, path,])
            vpzact.make_folder_vpz(dirpath=path)

        if 'csv' in reports or 'xls' in reports or 'txt' in reports:

            vpzact.prepare_reports(with_general_output=True,
                       default_general_output=False,
                       with_out_ident_and_nicknames=True, with_res=True,
                       with_bycol=bycol)

            if 'csv' in reports:
                path = os.path.join(folderpath, 'output')
                create_dirs_if_absent([folderpath, path,])
                vpzact.make_folder_csv_ident(dirpath=folderpath)
                vpzact.make_folder_csv_cond(dirpath=folderpath, bycol=bycol)
                vpzact.make_folder_csv_output(dirpath=path)
            if 'xls' in reports:
                path = folderpath
                create_dirs_if_absent([folderpath, path,])
                vpzact.make_folder_xls(dirpath=path, bycol=bycol)
            if 'txt' in reports:
                path = folderpath
                create_dirs_if_absent([folderpath, path,])
                vpzact.make_folder_txt(dirpath=path)

    def build_conditions_folder(self, vpzact, bycol) :
        """Builds content of the simulation conditions report folder

        The content is relative to input information.
        """

        folderpath = self.get_folder_path(vpzact)
        vpzact.prepare_reports(with_general_output=False,
                   default_general_output=False,
                   with_out_ident_and_nicknames=False, with_res=False,
                   with_bycol=bycol)

        path = folderpath
        create_dirs_if_absent([folderpath, path,])
        vpzact.make_conditions_folder_xls(dirpath=path, bycol=bycol)

class ExperimentViewMixin(DataFolderViewMixin, ReportViewMixin):
    """Additional methods for views about experiment

    The experiment information is into a xls file.
    """

    def getExperimentValues(self, data, vpzact) :
        """Experiment values (conditions)

        The experiment information comes from the xls experiment file that \
        is sent in POST data as key 'experimentfile'. It is saved (into \
        workspace) as svg_experiment_file file, that is then read to extract \
        the returned information (see read_experiment_file_xls).
        """

        svg_experiment_file = vpzact.vpzworkspace.get_experiment_file_path()
        cr_ok = save_posted_file(data=data, key='experimentfile',
                                 filepath=svg_experiment_file)
        if cr_ok :
            e = vpzact.read_experiment_file_xls(filepath=svg_experiment_file)
        else :
            e = (False, None, None, None, None, dict(), list(), list())
        return e

    def get_experiment_modifications(self, begin_value=None,
                duration_value=None, parameters=None, outselect_values=None) :
        """Builds and returns information to modify vpz file

        Builds information to modify the vpz file (relative to vpzact)
        according to inputs.

        parameters is a dict (selection_name as key, value as value).

        outselect_values : list of the values of the 'outselect' option that
        is used to select the restituted output datas. 

        Returns (storaged_list, conditions, begin, duration) or an error
        message (str) in error case.
        """

        ret = None # default
        try :
            storaged_list = outselect_values
            conditions = dict()
            if parameters is not None :
                vleparcompact_option = self.get_vleparcompact_option_values(
                            data=parameters)
                for par in vleparcompact_option :
                    cname = par.cname
                    pname = par.pname
                    value = get_val(par.value)
                    if cname not in conditions.keys():
                        conditions[cname] = dict()
                    conditions[cname][pname] = value
            begin = begin_value
            duration = duration_value
            ret = (storaged_list, conditions, begin, duration)

        except Exception as e :
            msg = "Unable to satisfy the request"
            errormsg = build_error_message(error=e, msg=msg)
            ret = errormsg
        return ret

    def action_experiment_post(self, LOGGER, data):
        """Action done for a POST request about experiment of a vpz

        Builds VpzInput from experiment of a vpz file (relative to vpzact),
        having before taken into account data information, and then
        having taken into account parselect option.

        Runs the vpz file (relative to vpzact, built VpzInput) simulation
        according to plan and restype. Builds VpzOutput.

        Returns VpzAct if no error, and raises exception else.

        Note : data options, other than parselect, are taken into account 
        directly into vpz file (modified before to be read and simulated).
        """

        try :
            res = self.init_activity(LOGGER=LOGGER, data=data)
            vpzact = res

            # modifications data
            (cr_tmp, restype_value, plan_value, begin_value, duration_value,
             parameters, parselect_values, outselect_values) = \
            self.getExperimentValues(data=data, vpzact=vpzact)
            if not cr_tmp :
                errormsg = "Unable to satisfy the request"
                raise Http400(errormsg)

            # parselect
            DATA = QueryDict('', mutable=True)
            key = 'parselect'
            for value in parselect_values :
                DATA.update({key:value})
            r = self.get_experiment_modifications(begin_value=begin_value,
                        duration_value=duration_value, parameters=parameters,
                        outselect_values=outselect_values)
            if type(r) is str : # error case
                errormsg = r
                raise Exception(errormsg)
            (storaged_list, conditions, begin, duration) = r

            vpzinput = VpzInput.create(vpzact=vpzact)
            vpzinput.full_clean()
            vpzinput.save()

            vpzoutput = VpzOutput.create(vpzact=vpzact)
            vpzoutput.full_clean()
            vpzoutput.save()

            self.prepare_datafolder(LOGGER=LOGGER, data=data, vpzact=vpzact)

            if self.is_ready_datafolder_case(data=data, vpzact=vpzact) :
                action_copy_datafolder = True
                datafoldercopy = self.get_datafoldercopy_value(data=data)
            else :
                action_copy_datafolder = False
                datafoldercopy = None

            w = vpzact.vpzworkspace
            (jres_read_part, jres_run_part) = w.actions_vpz(
                               storaged_list=storaged_list,
                               conditions=conditions,
                               begin=begin, duration=duration,
                               plan=plan_value, restype=restype_value,
                               action_modify_vpz=True,
                               action_read_vpz=True,
                               action_run_vpz=True,
                               action_copy_datafolder=action_copy_datafolder,
                               datafoldercopy=datafoldercopy)
                               #action_copy_modified_vpz=False,
            jres = (jres_read_part, jres_run_part)

            parselect = self.get_parselect_option_values(data=DATA)
            vpzinput.add_content(jres=jres_read_part, parselect=parselect)

            vpzoutput.add_content(jres_run_part)
            vpzoutput.full_clean()
            vpzoutput.save()

        except :
            raise
        return vpzact


    def action_experiment_get(self, LOGGER, data):
        """Action done for a GET request about experiment of a vpz

        Builds VpzInput from experiment of a vpz file (relative to vpzact),
        having before enforced and taken into account outselect:'all',
        and then having enforced and taken into account parselect:'all'.

        Returns VpzAct if no error, and raises exception else.

        Note : outselect option is taken into account directly into vpz file
        (modified before to be read).

        Note : parselect is not an option of this GET vpz/experiment request
        for the moment (could change).
        """

        try :
            res = self.init_activity(LOGGER=LOGGER, data=data)
            vpzact = res

            # "outselect"="all" and "parselect"="all"
            DATA = QueryDict('', mutable=True)
            DATA.update( {"parselect":"all"} )
            r = self.get_experiment_modifications(outselect_values=["all"])
            if type(r) is str : # error case
                errormsg = r
                raise Exception(errormsg)
            (storaged_list, conditions, begin, duration) = r

            vpzinput = VpzInput.create(vpzact=vpzact)
            vpzinput.full_clean()
            vpzinput.save()

            w = vpzact.vpzworkspace
            (jres_read_part, jres_run_part) = w.actions_vpz(
                                      storaged_list=storaged_list,
                                      conditions=conditions,
                                      begin=begin, duration=duration,
                                      action_modify_vpz=True,
                                      action_read_vpz=True)
                                      #plan=None, restype=None,
                                      #action_run_vpz=False,
                                      #action_copy_datafolder=False,
                                      #datafoldercopy=None,
                                      #action_copy_modified_vpz=False,
            jres = jres_read_part

            parselect = self.get_parselect_option_values(data=DATA)
            vpzinput.add_content(jres=jres, parselect=parselect)

        except :
            raise
        return vpzact

    def build_experiment_out_folder(self, vpzact) :
        """Builds content of the report file (into a folder) about experiment

        The content is relative to input and output information.
        """

        folderpath = self.get_folder_path(vpzact)
        vpzact.prepare_reports(with_general_output=True,
                   default_general_output=False,
                   with_out_ident_and_nicknames=True, with_res=True,
                   with_bycol=False)

        path = folderpath
        create_dirs_if_absent([folderpath, path,])
        vpzact.make_experiment_out_file_xls(dirpath=path)

    def build_experiment_in_folder(self, vpzact) :
        """Builds content of the report file (into a folder) about experiment

        The content is relative to input information.
        """

        folderpath = self.get_folder_path(vpzact)
        vpzact.prepare_reports(with_general_output=True,
                   default_general_output=True,
                   with_out_ident_and_nicknames=True, with_res=False,
                   with_bycol=False)
        path = folderpath
        create_dirs_if_absent([folderpath, path,])
        vpzact.make_experiment_in_file_xls(dirpath=path)

