"""erecord_vpz.views.menus """

import copy

from django.shortcuts import render

from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse

from erecord_cmn.utils.urls import url_add_options

from erecord_vpz.models import VpzAct

from erecord_cmn.configs.config import ONLINEDOC_URL

#headsubtitle = "erecord_vpz application (activity on vpz)"
headsubtitle = "(erecord_vpz)"

#------------------------------------------------------------------------------

# common definitions

cases_format_style = ['api_link', 'api_tree', 'json_link', 'json_tree', 
                      'html_link', 'html_tree'] # default

options_format_style = {
    'api_link' : { 'format':'api', 'mode':'link' },
    'json_link' : { 'format':'json', 'mode':'link' },
    'html_link' : { 'format':'html', 'mode':'link' },
    'api_tree' : { 'format':'api', 'mode':'tree' },
    'json_tree' : { 'format':'json', 'mode':'tree' },
    'html_tree' : { 'format':'html', 'mode':'tree' },
    'api_compact' : {'format':'api', 'mode':'compact'},
    'json_compact' : {'format':'json', 'mode':'compact'},
    'html_compact' : {'format':'html', 'mode':'compact'},
    'api_compactlist' : {'format':'api', 'mode':'compactlist'},
    'json_compactlist' : {'format':'json', 'mode':'compactlist'},
    'html_compactlist' : {'format':'html', 'mode':'compactlist'},
}

titles = {
    'api_link' : "(api format, link style) : ",
    'json_link' : "(json format, link style) : ",
    'html_link' : "(html format, link style) : ",
    'api_tree' : "(api format, tree style) : ",
    'json_tree': "(json format, tree style) : ",
    'html_tree': "(html format, tree style) : ",
    'api_compact' : "(api format, compact style) : ",
    'json_compact' : "(json format, compact style) : ",
    'html_compact' : "(html format, compact style) : ",
    'api_compactlist' : "(api format, compactlist style) : ",
    'json_compactlist' : "(json format, compactlist style) : ",
    'html_compactlist' : "(html format, compactlist style) : ",
}

objectcases = ('vpzact', 'vpzorigin', 'vpzinput', 'vpzoutput', 'vpzpath')

#------------------------------------------------------------------------------

@api_view(('GET',))
def home( request, format=None ):
    """erecord_vpz application

    The erecord_vpz application is dedicated to activities on vpz (edition,
    modification, simulation).

    Main menu
    """

    generated_admindocs = 'generated admindocs :   ' + \
        reverse('django-admindocs-docroot', request=request, format=format)
    online_documentation = 'online documentation (erecord web site) : ' + \
        ONLINEDOC_URL

    docs = [generated_admindocs, online_documentation,]

    main_menus = [
        "Home menu (api format) :  " + \
            url_add_options(reverse('erecord_vpz-home-menu', request=request, 
                            format=format), {'format':'api'}),
        "Home menu (json format) : " + \
            url_add_options(reverse('erecord_vpz-home-menu', request=request, 
                            format=format), {'format':'json'}),
        "Page menu (html) :  " + \
            reverse('erecord_vpz-menu-page', request=request, format=format),
        ]

    admin_pages = ['Administration : (vpz/admin)',]

    home_page = [
        "MENUS :" , main_menus,

        "LISTS, a submenu to see all the existing objects into the " + \
        "database (vpzpath, vpzact...)", 
        ["LISTS : " + \
         reverse('erecord_vpz-home-menu-lists', request=request,
                 format=format),
        ],

        "DETAIL, a submenu to see one existing object of the database " + \
        "(vpzpath, vpzact...).", 
        "The object is selected by its id. To know an object id, " + \
        "go to the LISTS submenu.", 
        ["DETAIL (the '1' value must be changed by the id value) : " + \
         reverse('erecord_vpz-home-menu-detail', args=(1,), request=request),
        ],

        "SERVICES, the submenu of vpz activities/manipulations.",
        ["SERVICES : "  + \
         reverse('erecord_vpz-home-menu-services', request=request, 
                 format=format),
        ],

        "Some docs :" , docs,
        "Only for admin :" , admin_pages,
    ]

    return Response( home_page )


@api_view(('GET',))
def home_lists( request, format=None ):
    """erecord_vpz application

    The erecord_vpz application is dedicated to activities on vpz (edition,
    modification, simulation).

    Lists menu
    """

    titles['vpzact'] = "All the vpz activities."
    titles['vpzorigin'] = "All the vpzorigin."
    titles['vpzinput'] = "All the vpzinput."
    titles['vpzoutput'] = "All the vpzoutput."
    titles['vpzpath'] = "All the vpzpath."

    viewnames = {
        'vpzact': 'erecord_vpz-vpzact-list',
        'vpzorigin': 'erecord_vpz-vpzorigin-list',
        'vpzinput': 'erecord_vpz-vpzinput-list',
        'vpzoutput': 'erecord_vpz-vpzoutput-list',
        'vpzpath': 'erecord_vpz-vpzpath-list',
    }

    home_list = dict()
    for case in objectcases :
        home_list[case] = [ titles[case] ]
        for f in cases_format_style :
            for k,v in options_format_style[f].items() :
                line = titles[f] + url_add_options(reverse(viewnames[case], 
                    request=request), options_format_style[f])
            home_list[case].append(line)

    home_lists = [
        "VpzAct.", home_list['vpzact'],
        "VpzOrigin.", home_list['vpzorigin'] ,
        "VpzInput.", home_list['vpzinput'] ,
        "VpzOutput.", home_list['vpzoutput'] ,
        "VpzPath.", home_list['vpzpath'] ,
    ]

    text = [ ".... VpzAct .... VpzPath ...", ]

    home_page = [ 
        'Introduction', text,

        "To see all the existing object of the database (vpzpath, vpzact...).", 
        home_lists,
    ]
    return Response( home_page )


@api_view(('GET',))
def home_details( request, pk, format=None ):
    """erecord_vpz application

    The erecord_vpz application is dedicated to activities on vpz (edition,
    modification, simulation).

    Detail menu
    """

    titles['vpzact'] = "The vpz activity whose id is "+str(pk)+ \
        " (value to be replaced by the VpzAct id value)."
    titles['vpzorigin'] = "The vpzorigin whose id is "+str(pk)+ \
        " (value to be replaced by the VpzOrigin id value)."
    titles['vpzinput'] = "The vpzinput  whose id is "+str(pk)+ \
        " (value to be replaced by the VpzInput id value)."
    titles['vpzoutput'] = "The vpzoutput whose id is "+str(pk)+ \
        " (value to be replaced by the VpzOutput id value)."
    titles['vpzpath'] = "The vpzpath whose id is "+str(pk)+ \
        " (value to be replaced by the VpzPath id value)."

    viewnames = {
        'vpzact': 'erecord_vpz-vpzact-detail',
        'vpzorigin': 'erecord_vpz-vpzorigin-detail',
        'vpzinput': 'erecord_vpz-vpzinput-detail',
        'vpzoutput': 'erecord_vpz-vpzoutput-detail',
        'vpzpath': 'erecord_vpz-vpzpath-detail',
    }

    home_detail = dict()
    for case in objectcases :
        home_detail[case] = [ titles[case] ]
        for f in cases_format_style :
            for k,v in options_format_style[f].items() :
                line = titles[f] + url_add_options(reverse(viewnames[case], 
                    args=(pk,), request=request), options_format_style[f])
            home_detail[case].append(line)

    home_details = [

        "VpzAct.",
        "The VpzAct is selected by its id. To know a VpzAct id, " + \
        "go to the LISTS submenu.",
        home_detail['vpzact'],

        "VpzOrigin.",
        "The VpzOrigin is selected by its id. To know a VpzOrigin id, " + \
        "go to the LISTS submenu.",
        home_detail['vpzorigin'] ,

        "VpzInput.",
        "The VpzInput is selected by its id. To know a VpzInput id, " + \
        "go to the LISTS submenu.",
        home_detail['vpzinput'] ,

        "VpzOutput.",
        "The VpzOutput is selected by its id. To know a VpzOutput id, " + \
        "go to the LISTS submenu.",
        home_detail['vpzoutput'] ,

        "VpzPath.",
        "The VpzPath is selected by its id. To know a VpzPath id, " + \
        "go to the LISTS submenu.",
        home_detail['vpzpath'] ,
    ]

    text = [".... VpzAct .... VpzPath ...",]

    home_page = [ 
        'Introduction', text,

        "To see one existing object of the database (vpzpath, vpzact...).", 
        "The object is selected by its id. To know an object id, " + \
        "go to the LISTS submenu.", 
        home_details,
    ]
    return Response( home_page )


@api_view(('GET',))
def home_services( request, pk=None, format=None ):
    """erecord_vpz application

    The erecord_vpz application is dedicated to activities on vpz (edition,
    modification, simulation).

    Services (vpz activities/manipulations) 
    """

    if pk is None :
        pk = 1 # default value for urls examples

    go_to_db_lists_menu = "go to the LISTS submenu of db application " + \
        reverse('erecord_db-home-menu-lists', request=request, format=format)
    go_to_lists_menu = "go to the LISTS submenu " + \
        reverse('erecord_vpz-home-menu-lists', request=request, format=format)

    def make_home(request, pk, service="input" ):
        """Makes home_service menu for service = "input", "output", "inout" """

        cases_format_style_service = copy.deepcopy(cases_format_style)

        if service == "input" :
            capital = "Input"
            classname = "VpzInput"
            detailclassname = "VpzAct...VpzInput"
            url = "input"
            urlobject = "input"

            # adding 'compact' style (for 'api', 'json' and 'html' formats)
            cases_format_style_service.append('api_compact')
            cases_format_style_service.append('json_compact')
            cases_format_style_service.append('html_compact')
            # adding 'compactlist' style (for 'api', 'json' and 'html' formats)
            cases_format_style_service.append('api_compactlist')
            cases_format_style_service.append('json_compactlist')
            cases_format_style_service.append('html_compactlist')

        elif service == "output" :
            capital = "Output"
            classname = "VpzOutput"
            detailclassname = "VpzAct...VpzOutput"
            url = "output"
            urlobject = "output"

        elif service == "inout" :
            capital = "InOut (Input and Output)"
            classname = "VpzAct (including VpzInput and VpzOutput)"
            detailclassname = "VpzAct (including VpzInput and VpzOutput)"
            url = "inout"
            urlobject = "act"

        titles['vpz'] = capital + " of a VpzAct defined by the VleVpz whose " \
            "id is vpz="+str(pk)+ " (value to be replaced by the VleVpz " + \
            "id value)."
        
        titles['vpzpath'] = capital + " of a VpzAct defined by the VpzPath " \
            "whose id is vpzpath="+str(pk)+ " (value to be replaced by " + \
            "the VpzPath id value)."
        
        titles['id'] = capital + " of the existing "+classname+" whose id " + \
            "is "+str(pk)+" (value to be replaced by the "+classname+ \
            " id value)."
       
        home_service = dict()
        for case in ('vpz', 'vpzpath') :
            home_service[case] = [ titles[case] ]
            for f in cases_format_style_service :
                caseoptions = { case:pk }
                for k,v in options_format_style[f].items() :
                    caseoptions[k] = v
                line = titles[f] + url_add_options(reverse('erecord_vpz-'+url, 
                        request=request), caseoptions)
                home_service[case].append(line)

        case = 'id'
        home_service[case] = [ titles[case] ]
        for f in cases_format_style_service :
            line = titles[f] + \
               url_add_options(reverse('erecord_vpz-vpz'+urlobject+'-detail', 
                        args=(pk,), request=request), options_format_style[f])
            home_service[case].append(line)
    
        home_services = [
    
            "Case of a VpzAct defined by a VleVpz (the Vpz is first " + \
            "created ("+detailclassname+")).",
            "The VleVpz is selected by its id, given into 'vpz' option. ",
            "To know a VleVpz id, " + go_to_db_lists_menu,
            home_service['vpz'],
    
            "Case of a VpzAct defined by a VpzPath (the Vpz is first " + \
            "created ("+detailclassname+")).",
            "The VpzPath is selected by its id, given into 'vpzpath' option.",
            "To know a VpzPath id, " + go_to_lists_menu, 
            home_service['vpzpath'],
    
            "Case of an existing "+classname+".",
            "The "+classname+" is selected by its id.",
            "To know a "+classname+ " id, " + go_to_lists_menu,
            home_service['id'],
        ] 
        return home_services

    text = [
        "The manipulated VpzAct 'comes from' a VpzPath or a VleVpz.",
        "In VleVpz case, the VleVpz is selected by its id, given into " + \
        "'vpz' option.", 
        "To know a VleVpz id, " + go_to_db_lists_menu,
        "In VpzPath case, the VpzPath is selected by its id, given into " + \
        "'vpzpath' option.",
        "To know a VpzPath id, " + go_to_lists_menu,
    ]

    home_page = [ 
        'Introduction', text,

        "INPUT services, to see input data of a vpz file :",
        "(for more description : 'vpz/input' resource in 'Web API' " +
                "at " + ONLINEDOC_URL + \
                " (parameters modification, output datas selection...))",
        make_home(request=request, pk=pk, service="input"),

        "OUTPUT services, to see output data of a vpz file (simulation " + \
        "result) :",
        "(for more description : 'vpz/output' resource in 'Web API' " +
                "at " + ONLINEDOC_URL + \
                " (parameters modification, output datas selection...))",
        make_home(request=request, pk=pk, service="output"),

        "INOUT services, to see input data of a vpz file and its output " + \
        "data (simulation result) :",
        "(for more description : 'vpz/inout' resource in 'Web API' " +
                "at " + ONLINEDOC_URL + \
                " (parameters modification, output datas selection...))",
        make_home(request=request, pk=pk, service="inout"),

    ]
    return Response( home_page )

#------------------------------------------------------------------------------

def menu_page(request):
    """Menu page """

    vpzact_list = VpzAct.objects.all()
    context = { 'vpzact_list': vpzact_list, }
    return render(request, 'erecord_vpz/menu.html', context)

#------------------------------------------------------------------------------

