"""erecord_vpz.urls

Urls of the erecord_vpz application

"""

from django.urls import path

from django.views.generic import TemplateView

from erecord_vpz.views import activities as vpz_views_activities
from erecord_vpz.views import menus as vpz_views_menus
from erecord_vpz.views import objects as vpz_views_objects
from erecord_vpz.views import views as vpz_views_views

# root, menu, home pages
urlpatterns = [

    path('', TemplateView.as_view(template_name='erecord_cmn/index.html'),
         name="erecord_vpz-index-page"),

    path('menu/', vpz_views_menus.menu_page, name='erecord_vpz-menu-page'),

    path('home/', vpz_views_menus.home, name="erecord_vpz-home-menu"),

    path('home/lists/', vpz_views_menus.home_lists,
         name="erecord_vpz-home-menu-lists"),
    path('home/detail/<int:pk>/', vpz_views_menus.home_details,
         name="erecord_vpz-home-menu-detail"),

    path('home/services/', vpz_views_menus.home_services,
         name="erecord_vpz-home-menu-services"),
    path('home/services/<int:pk>/', vpz_views_menus.home_services,
         name="erecord_vpz-home-menu-services"),
]

# lists
urlpatterns += [

    path('vpzpath/', vpz_views_objects.VpzPathList.as_view(),
         name='erecord_vpz-vpzpath-list' ),

    # lists of results parts
    # Note : in False ACTIVITY_PERSISTENCE case, results are removed at the 
    # end ; in True ACTIVITY_PERSISTENCE case, results should be in access
    # only for the user who asked for them.

    #path('vpzact/', vpz_views_objects.VpzActList.as_view(),
    #     name='erecord_vpz-vpzact-list' ),
    #path('vpzorigin/', vpz_views_objects.VpzOriginList.as_view(),
    #     name='erecord_vpz-vpzorigin-list' ),
    #path('vpzworkspace/', vpz_views_objects.VpzWorkspaceList.as_view(),
    #     name='erecord_vpz-vpzworkspace-list' ),
    #path('vpzinput/', vpz_views_objects.VpzInputList.as_view(),
    #     name='erecord_vpz-vpzinput-list' ),
    #path('vpzoutput/', vpz_views_objects.VpzOutputList.as_view(),
    #     name='erecord_vpz-vpzoutput-list' ),
    #path('begin/', vpz_views_objects.VleBeginList.as_view(),
    #     name='erecord_vpz-begin-list' ),
    #path('duration/', vpz_views_objects.VleDurationList.as_view(),
    #     name='erecord_vpz-duration-list' ),
    #path('cond/', vpz_views_objects.VleCondList.as_view(),
    #     name='erecord_vpz-cond-list' ),
    #path('par/', vpz_views_objects.VleParList.as_view(),
    #     name='erecord_vpz-par-list' ),
    #path('view/', vpz_views_objects.VleViewList.as_view(),
    #     name='erecord_vpz-view-list' ),
    #path('out/', vpz_views_objects.VleOutList.as_view(),
    #     name='erecord_vpz-out-list' ),

]

# details
urlpatterns += [

    path('vpzpath/<int:pk>/', vpz_views_objects.VpzPathDetail.as_view(),
         name='erecord_vpz-vpzpath-detail' ),

    # details of results parts
    # Note : False ACTIVITY_PERSISTENCE case : results removed at the end.
    # True ACTIVITY_PERSISTENCE case : results should be in access only for
    # the user who asked for them.

    #path('vpzact/<int:pk>/', vpz_views_objects.VpzActDetail.as_view(),
    #     name='erecord_vpz-vpzact-detail' ),
    #path('vpzorigin/<int:pk>/', vpz_views_objects.VpzOriginDetail.as_view(),
    #     name='erecord_vpz-vpzorigin-detail' ),
    #path('vpzworkspace/<int:pk>/',
    #     vpz_views_objects.VpzWorkspaceDetail.as_view(),
    #     name='erecord_vpz-vpzworkspace-detail' ),
    #path('vpzinput/<int:pk>/', vpz_views_objects.VpzInputDetail.as_view(),
    #     name='erecord_vpz-vpzinput-detail' ),
    #path('vpzoutput/<int:pk>/', vpz_views_objects.VpzOutputDetail.as_view(),
    #     name='erecord_vpz-vpzoutput-detail' ),
    #path('begin/<int:pk>/', vpz_views_objects.VleBeginDetail.as_view(),
    #     name='erecord_vpz-begin-detail' ),
    #path('duration/<int:pk>/', vpz_views_objects.VleDurationDetail.as_view(),
    #     name='erecord_vpz-duration-detail' ),
    #path('cond/<int:pk>/', vpz_views_objects.VleCondDetail.as_view(),
    #     name='erecord_vpz-cond-detail' ),
    #path('par/<int:pk>/', vpz_views_objects.VleParDetail.as_view(),
    #     name='erecord_vpz-par-detail' ),
    #path('view/<int:pk>/', vpz_views_objects.VleViewDetail.as_view(),
    #     name='erecord_vpz-view-detail' ),
    #path('out/<int:pk>/', vpz_views_objects.VleOutDetail.as_view(),
    #     name='erecord_vpz-out-detail' ),
]

# access
urlpatterns += [

    path('vpzpath/<int:pk>[0-9]+)/access/',
         vpz_views_views.VpzPathAccessView.as_view(),
         name='erecord_vpz-vpzpath-access' ),

    path('vpzact/<int:pk>/access/',
         vpz_views_views.VpzActAccessView.as_view(),
         name='erecord_vpz-vpzact-access' ),
    path('vpzorigin/<int:pk>/access/',
         vpz_views_views.VpzOriginAccessView.as_view(),
         name='erecord_vpz-vpzorigin-access' ),
    path('vpzinput/<int:pk>/access/',
         vpz_views_views.VpzInputAccessView.as_view(),
         name='erecord_vpz-vpzinput-access' ),
    path('vpzoutput/<int:pk>/access/',
         vpz_views_views.VpzOutputAccessView.as_view(),
         name='erecord_vpz-vpzoutput-access' ),
    path('vlebegin/<int:pk>/access/',
         vpz_views_views.VleBeginAccessView.as_view(),
         name='erecord_vpz-vlebegin-access' ),
    path('vleduration/<int:pk>/access/',
         vpz_views_views.VleDurationAccessView.as_view(),
         name='erecord_vpz-vleduration-access' ),
    path('vlecond/<int:pk>/access/',
         vpz_views_views.VleCondAccessView.as_view(),
         name='erecord_vpz-vlecond-access' ),
    path('vlepar/<int:pk>/access/',
         vpz_views_views.VleParAccessView.as_view(),
         name='erecord_vpz-vlepar-access' ),
    path('vleview/<int:pk>/access/',
         vpz_views_views.VleViewAccessView.as_view(),
         name='erecord_vpz-vleview-access' ),
    path('vleout/<int:pk>/access/',
         vpz_views_views.VleOutAccessView.as_view(),
         name='erecord_vpz-vleout-access' ),
    path('vpzworkspace/<int:pk>/access/',
         vpz_views_views.VpzWorkspaceAccessView.as_view(),
         name='erecord_vpz-vpzworkspace-access' ),
]

# vpz/input,output,inout,reports,experiment
# Note : 1 (and only 1) of both following options is needed (information from 
# which the manipulated Vpz is going to be created) :
# - vpzpath=id of a VpzPath
# - vpz=id of a VleVpz (into 'db' database)

urlpatterns += [

    path('input/', vpz_views_activities.InputView.as_view(),
         name='erecord_vpz-input' ),
        
    path('output/', vpz_views_activities.OutputView.as_view(),
         name='erecord_vpz-output' ),
        
    path('inout/', vpz_views_activities.InOutView.as_view(),
         name='erecord_vpz-inout' ),
        
    path('report/', vpz_views_activities.ReportView.as_view(),
         name='erecord_vpz-report' ),
    path('report/conditions/',  
         vpz_views_activities.ReportConditionsView.as_view(),
         name='erecord_vpz-report-conditions' ),
        
    path('experiment/', vpz_views_activities.ExperimentView.as_view(),
         name='erecord_vpz-experiment' ),

]

