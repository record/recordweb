"""erecord.apps

*This package is part of erecord - web services for vle models*

:copyright: Copyright (C) 2019-2024 INRA http://www.inra.fr.
:license: GPLv3, see LICENSE file for more details.
:authors: see AUTHORS file.

django applications of the erecord package

The django applications are subdirectories of apps.

The 'apps' path must be included into PYTHONPATH.

Documentation of each 'xxx' django application : see its 'xxx' package.

See also python code in django projects (erecord.projects)
 
"""

