"""erecord_acs.views """

from rest_framework.response import Response
from rest_framework import generics

from erecord_db.models import VleVpz
from erecord_vpz.models import VpzPath

from erecord_cmn.views_mixins import FormatViewMixin
from erecord_cmn.views_mixins import DetailViewMixin
from erecord_cmn.views_mixins import DataListViewMixin

from erecord_acs.views_mixins import LimitedAccessViewMixin
from erecord_acs.views_mixins import AccessViewMixin
from erecord_acs.views_mixins import DataListLimitedCaseViewMixin


from erecord_cmn.utils.logger import get_logger
LOGGER = get_logger(__name__)

headsubtitle = "(erecord_acs)"


class VleVpzAccessView(AccessViewMixin, DetailViewMixin,
                       generics.RetrieveAPIView):
    """Returns access type of a simulator (VleVpz)

    Values : public, limited
    """

    def get(self, request, format=None, **kwargs):
        obj = VleVpz.objects.get(pk=kwargs['pk'])
        title = 'Access of (vpz) simulator (Id ' + str(obj.id) + ')'
        return self.access_response(request=request, format=format,
                                    obj=obj, title=title)

class VleVpzAccessibleIdView(LimitedAccessViewMixin, DataListViewMixin, 
                             FormatViewMixin, generics.RetrieveAPIView):
    """List of ids of the accessible simulators (vpz) 

    Options : jwt and access.

    access option (values : limited, public) to filter simulators list.

    jwt option to filter simulators in limited access.
    """

    def get(self, request, format=None, **kwargs):

        data = self.request_query_params(request)
        if format is None :
            format = self.get_format_value(data=data)
        jwt = self.get_jwt_value(data=data)
        access = self.get_access_option_values(data=data)

        vpz_all = VleVpz.objects.all()
        vpz_public = []
        vpz_limited = []
        for vpz in vpz_all :
            if vpz.is_public() : # public
                vpz_public.append(vpz)
            else : # limited
                vpz_limited.append(vpz)
        id_list = [] # default

        if "public" in access : # which ever jwt (None or else)
            for vpz in vpz_public :
                id_list.append(vpz.id)

        if ("limited" in access) and (jwt is not None) :
            for vpz in vpz_limited :
                if self.access_ok(model=vpz, data=data) :
                    id_list.append(vpz.id)

        context = dict()
        context['list'] = id_list
        if format=='html' :
            title = 'Ids of accessible simulators (vpz)'
            text = "The simulators (vpz) that are accessible, "
            text += "according to "+ str(access) +" access status"
            if jwt is not None :
                text += " and the given JWT"
            text += ", have the following Ids :"
            context['title'] = title 
            context['headsubtitle'] = headsubtitle
            context['text'] = text
            response = Response(context)
        else :
            response = Response(data=context)
        return response

class VleVpzUserIdView(DataListLimitedCaseViewMixin, generics.RetrieveAPIView):
    """List of ids of the authorized users of a simulator VleVpz

    Available only in limited access : only simulators in limited access have
    users.

    Returns an error in case of a public simulator (having no user)
    """

    def get(self, request, format=None, **kwargs):

        vpz = VleVpz.objects.get(pk=kwargs['pk'])
        useridlist = vpz.get_authorized_users_ids()
        title = 'Ids of authorized users'
        text = "Users who are authorized "
        text += "for the (vpz) simulator (Id "+str(vpz.id)+") "
        text += "have the following Ids :"
        errormsg = "Unable to satisfy the request, because public simulator "
        errormsg += "(only simulators in limited access have users)"

        return self.list_limited_case_response(request=request,
                       format=format, obj=vpz, data_list=useridlist,
                       title=title, headsubtitle=headsubtitle, text=text,
                       error_msg=errormsg, LOGGER=LOGGER)

class VleVpzUserNameView(DataListLimitedCaseViewMixin,
                         generics.RetrieveAPIView):
    """List of names of the authorized users of a simulator VleVpz

    Available only in limited access : only simulators in limited access have
    users.

    Returns an error in case of a public simulator (having no user)
    """

    def get(self, request, format=None, **kwargs):

        vpz = VleVpz.objects.get(pk=kwargs['pk'])
        users_list = vpz.get_authorized_users()
        usernamelist = [user.username for user in users_list]
        title = 'Names of authorized users'
        text = "Users who are authorized "
        text += "for the (vpz) simulator (Id "+str(vpz.id)+") "
        text += "have the following names :"
        errormsg = "Unable to satisfy the request, because public simulator "
        errormsg += "(only simulators in limited access have users)"

        return self.list_limited_case_response(request=request,
                       format=format, obj=vpz, data_list=usernamelist,
                       title=title, headsubtitle=headsubtitle, text=text,
                       error_msg=errormsg, LOGGER=LOGGER)

class VpzPathAccessView(AccessViewMixin, DetailViewMixin,
                       generics.RetrieveAPIView):
    """Returns access type of a simulator (VpzPath)

    Values : public, limited
    """

    def get(self, request, format=None, **kwargs):
        obj = VpzPath.objects.get(pk=kwargs['pk'])
        title = 'Access of (vpzpath) simulator (Id ' + str(obj.id) + ')'
        return self.access_response(request=request, format=format,
                                    obj=obj, title=title)

class VpzPathAccessibleIdView(LimitedAccessViewMixin, DataListViewMixin, 
                             FormatViewMixin, generics.RetrieveAPIView):
    """List of ids of the accessible simulators (vpzpath) 

    Options : jwt and access.

    access option (values : limited, public) to filter simulators list.

    jwt option to filter simulators in limited access.
    """

    def get(self, request, format=None, **kwargs):

        data = self.request_query_params(request)
        if format is None :
            format = self.get_format_value(data=data)
        jwt = self.get_jwt_value(data=data)
        access = self.get_access_option_values(data=data)

        vpzpath_all = VpzPath.objects.all()
        vpzpath_public = []
        vpzpath_limited = []
        for vpzpath in vpzpath_all :
            if vpzpath.is_public() : # public
                vpzpath_public.append(vpzpath)
            else : # limited
                vpzpath_limited.append(vpzpath)
        id_list = [] # default

        if "public" in access : # which ever jwt (None or else)
            for vpzpath in vpzpath_public :
                id_list.append(vpzpath.id)

        if ("limited" in access) and (jwt is not None) :
            for vpzpath in vpzpath_limited :
                if self.access_ok(model=vpzpath, data=data) :
                    id_list.append(vpzpath.id)

        context = dict()
        context['list'] = id_list
        if format=='html' :
            title = 'Ids of accessible simulators (vpzpath)'
            text = "The simulators (vpzpath) that are accessible, "
            text += "according to "+ str(access) +" access status"
            if jwt is not None :
                text += " and the given JWT"
            text += ", have the following Ids :"
            context['title'] = title 
            context['headsubtitle'] = headsubtitle
            context['text'] = text
            response = Response(context)
        else :
            response = Response(data=context)
        return response

class VpzPathUserIdView(DataListLimitedCaseViewMixin, generics.RetrieveAPIView):
    """List of ids of the authorized users of a simulator VpzPath

    Available only in limited access : only simulators in limited access have
    users.

    Returns an error in case of a public simulator (having no user)
    """

    def get(self, request, format=None, **kwargs):

        vpzpath = VpzPath.objects.get(pk=kwargs['pk'])
        useridlist = vpzpath.get_authorized_users_ids()
        title = 'Ids of authorized users'
        text = "Users who are authorized "
        text += "for the (vpzpath) simulator (Id "+str(vpzpath.id)+") "
        text += "have the following Ids :"
        errormsg = "Unable to satisfy the request, because public simulator "
        errormsg += "(only simulators in limited access have users)"

        return self.list_limited_case_response(request=request,
                       format=format, obj=vpzpath, data_list=useridlist,
                       title=title, headsubtitle=headsubtitle, text=text,
                       error_msg=errormsg, LOGGER=LOGGER)

class VpzPathUserNameView(DataListLimitedCaseViewMixin,
                         generics.RetrieveAPIView):
    """List of names of the authorized users of a simulator VpzPath

    Available only in limited access : only simulators in limited access have
    users.

    Returns an error in case of a public simulator (having no user)
    """

    def get(self, request, format=None, **kwargs):

        vpzpath = VpzPath.objects.get(pk=kwargs['pk'])
        users_list = vpzpath.get_authorized_users()
        usernamelist = [user.username for user in users_list]
        title = 'Names of authorized users'
        text = "Users who are authorized "
        text += "for the (vpzpath) simulator (Id "+str(vpzpath.id)+") "
        text += "have the following names :"
        errormsg = "Unable to satisfy the request, because public simulator "
        errormsg += "(only simulators in limited access have users)"

        return self.list_limited_case_response(request=request,
                       format=format, obj=vpzpath, data_list=usernamelist,
                       title=title, headsubtitle=headsubtitle, text=text,
                       error_msg=errormsg, LOGGER=LOGGER)

