"""erecord_acs.views_mixins

Mixins common to views using Lock provided by erecord_acs application

"""

from rest_framework_jwt.authentication import BaseJSONWebTokenAuthentication
from rest_framework.exceptions import AuthenticationFailed
from rest_framework import permissions

from rest_framework.response import Response

from erecord_cmn.views_mixins import RenderViewMixin
from rest_framework.renderers import TemplateHTMLRenderer

from erecord_cmn.views_mixins import FormatViewMixin
from erecord_cmn.views_mixins import DataViewMixin
from erecord_cmn.views_mixins import ErrorViewMixin
from erecord_cmn.views_mixins import DataListViewMixin

from erecord_cmn.serializers import JwtOptionSerializer
from erecord_cmn.serializers import getKeyOptionValues

from erecord_vpz.models import VpzInput

#from erecord_cmn.utils.logger import get_logger
#LOGGER = get_logger(__name__)
from erecord_cmn.utils.errors import logger_report_error
from erecord_cmn.utils.errors import build_error_message
from erecord_cmn.utils.errors import get_error_status
from erecord_cmn.utils.errors import Http403
from erecord_cmn.utils.errors import Http400

class AccessPermission(permissions.BasePermission):
    """Methods about access permission rules """

    def has_object_permission(self, request, view, obj):
        data = view.data_from_request(request)
        access_ok = view.access_ok(model=obj, data=data)
        return access_ok


class LimitedAccessViewMixin(BaseJSONWebTokenAuthentication, DataViewMixin):
    """Additional methods for views maybe having limited access

    JSON Web Token Authentication scheme.
    The JWT (JSON Web Token) comes in the querystring. 
    """

    permission_classes = (AccessPermission,)

    def get_jwt_value(self, data):
        """Returns the JWT value found in data and else, None """

        p = JwtOptionSerializer(data=data)
        p.is_valid()
        return p.data['jwt']

    def get_access_option_values(self, data):
        """Option 'access' (several values are possible)
    
        Returns the list of access option values.
        """

        s_access = getKeyOptionValues(data=data, key='access')
        access_options = []
        for v in ['public', 'limited']:
            if v in s_access :
                access_options.append(v)
        return access_options

    def get_id_of_authenticated_user(self, data):
        """Returns the id of the authenticated user

        Returns user id if token OK, None if no token found,
        else fails (exception) : if token not OK...
        """

        user_id = None # default
        res = self.authenticate(data)
        if res is not None :
            (user, jwt_value) = res
            user_id = user.id
        return user_id

    def successful_unlocking(self, model, data, LOGGER=None):
        """Returns user_id if model unlocked by user authenticated into data

        If a user has been authenticated and is authorized for the model \
        then returns user_id and else sends an exception.

        model has to be locked (ie linked to some Lock)
        """

        user_id = None # default

        try :
            user_id = self.get_id_of_authenticated_user(data=data)

        except Exception as e :
            msg = "Error while limited access"
            if isinstance(e, AuthenticationFailed) :
                msg = msg + " (AuthenticationFailed)"
            errormsg = build_error_message(error=e, msg=msg)
            if LOGGER is not None :
                logger_report_error(LOGGER)
            raise Http403(errormsg)

        if user_id is None :
            errormsg = "Error while limited access : "
            errormsg = errormsg + "a token is required (option jwt=...)"
            raise Http403(errormsg)

        if user_id not in model.get_authorized_users_ids() :
            errormsg = "Error while limited access : "
            errormsg = errormsg + "bad token, "
            errormsg = errormsg + "unauthorized user (id " +str(user_id)+ ")"
            raise Http403(errormsg)

        return user_id

    def access_ok(self, model, data):
        """Returns True if the model is accessible and else False.

        The model is accessible if it has no lock, or \
        if it has lock and has been unlocked by user authenticated into data.
        """

        access_ok = True # default
        if model.has_lock() : # limited model case
            access_ok = False # before control
            try : # try to unlock model
                self.successful_unlocking(model=model, data=data)
                access_ok = True # since no exception sent
            except : # exception raised in particular if unsuccessful unlocking
                pass 
        return access_ok

    def filter_access_ok(self, model_list, data):
        """Returns models list containing the accessible models of model_list.

        A model is accessible if it has no lock, or \
        if it has lock and has been unlocked by user authenticated into data
        (see access_ok method).
        """

        to_be_excluded = []
        for model in model_list :
            try :
                access_ok = self.control_access(model, data)
            except :
                access_ok = False
            if not access_ok :
                to_be_excluded.append(model.id)
        return model_list.exclude(id__in=to_be_excluded)

    def filter_with_propagation_access_ok(self, model_list, data):
        """Returns models list containing the accessible models of model_list.

        The model is accessible if it has no lock, or \
        if it has lock and has been unlocked by user authenticated into data.

        It is assumed that if the model has no lock, then neither its parts.

        model has to have propagate_control_access_user method
        """

        to_be_excluded = []
        for model in model_list :
            try :
                access_ok = self.control_access_with_propagation(model, data)
            except :
                access_ok = False
            if not access_ok :
                to_be_excluded.append(model.id)
        return model_list.exclude(id__in=to_be_excluded)

    def control_access(self, model, data) :
        """Controls if model is accessible regarding user into data

        returns True if model is accessible and else sends an exception.

        The model is accessible if it has no lock, or \
        if it has lock and has been unlocked by user authenticated into data.

        only first level control
        """

        if model.has_lock():
            try :
                user_id = self.successful_unlocking(model=model, data=data)
            except :
                raise
        return True

    def control_access_with_propagation(self, model, data) :
        """Controls if model and its parts accessible regarding user into data

        returns True if the model and all its parts are accessible
        and else sends an exception.

        The (part) model is accessible if it has no lock, or \
        if it has lock and has been unlocked by user authenticated into data.

        It is assumed that if the model has no lock, then neither its parts.

        model has to have propagate_control_access_user method
        """

        if model.has_lock():
            try :
                user_id = self.successful_unlocking(model=model, data=data)
                model.propagate_control_access_user(user_id=user_id)
            except :
                raise
        return True


class AccessPermissionVpzInputDetail(permissions.BasePermission):
    """Methods about access permission rules """

    def has_object_permission(self, request, view, obj):
        data = view.data_from_request(request)
        if isinstance(obj, VpzInput) :
            access_ok = view.access_ok(model=obj, data=data)
        else : # VpzInputCompact
            access_ok = True # default (in fact unknown, not controlled)
        return access_ok


class LimitedAccessVpzInputViewMixin(LimitedAccessViewMixin):
    """additional methods for views about VpzInput as maybe having limited \
       access """
    permission_classes = (AccessPermissionVpzInputDetail,)

class AccessViewMixin(FormatViewMixin, DataViewMixin):
    """Additional methods for views showing access"""

    def get_renderers( self ):
        r = RenderViewMixin.get_renderers(self)
        renderer = TemplateHTMLRenderer()
        renderer.template_name='erecord_acs/headedform_access.html'
        r.append( renderer )
        return r

    def access_response(self, request, format, obj, title, headsubtitle=None) :
        data = self.request_query_params(request)
        if format is None :
            format = self.get_format_value(data=data)
        access = obj.get_access_value()
        if format=='html' :
            context = dict()
            context['title'] = title
            context['headsubtitle'] = headsubtitle
            context['access'] = access
            context['text'] = "Access is " + access
            response = Response(context)
        else :
            response = Response(data={'access':access })
        return response

class DataListLimitedCaseViewMixin(DataListViewMixin, FormatViewMixin,
                                   DataViewMixin, ErrorViewMixin):
    """Additional methods for views returning a list of datas with access \
       conditions """

    def list_limited_case_response(self, request, format, obj, data_list,
                      title, headsubtitle, text, error_msg, LOGGER=None):
        """Returns data_list if obj is in limited access and else error_msg """

        data = self.request_query_params(request)
        if format is None :
            format = self.get_format_value(data=data)

        if obj.is_public() :
            e = Exception(Http400)
            s = get_error_status(e)
            errormsg = build_error_message(msg=error_msg)
            logger_report_error(LOGGER)
            if format=='html' :
                context = self.html_error_context(status=s, detail=errormsg)
                response = Response(context)
            else :
                response = Response(data={'detail':errormsg,}, status=s)
        else :
            context = dict()
            context['list'] = data_list
            if format=='html' :
                context['title'] = title
                context['headsubtitle'] = headsubtitle
                context['text'] = text
                response = Response(context)
            else :
                response = Response(data=context)
        return response

