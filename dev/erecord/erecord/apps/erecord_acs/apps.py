from django.apps import AppConfig


class ErecordAcsConfig(AppConfig):
    name = 'erecord_acs'
