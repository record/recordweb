"""erecord_acs.urls

Urls of the erecord_acs application

"""

from django.urls import path

from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_jwt.views import verify_jwt_token

from erecord_acs import views as acs_views

urlpatterns = [

    # JWT Authentication
    path('jwt/obtain/', obtain_jwt_token, name="token-auth"),
   #path('jwt/refresh/', refresh_jwt_token),
    path('jwt/verify/', verify_jwt_token, name="token-verify"),
    path('login', obtain_jwt_token),

    # simulator (VleVpz) access
    path('vpz/<int:pk>/access/', acs_views.VleVpzAccessView.as_view(),
         name='erecord_acs-vpz-access' ),

    # accessible simulators (VleVpz)
    path('vpz/accessible/id/', acs_views.VleVpzAccessibleIdView.as_view(),
        name='erecord_acs-vpz-accessible-id' ),

    # id of authorized users of a simulator (VleVpz) in limited access
    path('vpz/<int:pk>/user/id/', acs_views.VleVpzUserIdView.as_view(),
         name='erecord_acs-vpz-user-id' ),

    # name of authorized users of a simulator (VleVpz) in limited access
    path('vpz/<int:pk>/user/name/', acs_views.VleVpzUserNameView.as_view(),
         name='erecord_acs-vpz-user-name' ),

    # simulator (VpzPath) access
    path('vpzpath/<int:pk>/access/', acs_views.VpzPathAccessView.as_view(),
         name='erecord_acs-vpzpath-access' ),

    # accessible simulators (VpzPath)
    path('vpzpath/accessible/id/',
         acs_views.VpzPathAccessibleIdView.as_view(),
         name='erecord_acs-vpzpath-accessible-id' ),

    # id of authorized users of a simulator (VpzPath) in limited access
    path('vpzpath/<int:pk>/user/id/', acs_views.VpzPathUserIdView.as_view(),
         name='erecord_acs-vpzpath-user-id' ),

    # name of authorized users of a simulator (VpzPath) in limited access
    path('vpzpath/<int:pk>/user/name/',
         acs_views.VpzPathUserNameView.as_view(),
         name='erecord_acs-vpzpath-user-name' ),
]

