"""erecord_acs

*This package is part of erecord - web services for vle models*

:copyright: Copyright (C) 2019-2024 INRA http://www.inra.fr.
:license: GPLv3, see LICENSE file for more details.
:authors: see AUTHORS file.

Access control application
 
The acs application is dedicated to access control management, based on
JWT (JSON Web Token).

"""

