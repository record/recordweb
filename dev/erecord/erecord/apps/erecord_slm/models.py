"""erecord_slm.models

Models of the erecord_slm application

"""

import os

from django.db.models.signals import pre_delete

from django.core.files import File
from django.core.files.storage import FileSystemStorage
from django.conf import settings

from django.db import models
from erecord_slm.models_mixins import LoadVpzDocumentMixin
from erecord_acs.models_mixins import LockableMixin

from erecord_cmn.configs.config import DOWNLOADS_HOME_APP_VPZ
from erecord_cmn.configs.config import DOWNLOAD_LIFETIME

from django.utils.deconstruct import deconstructible

@deconstructible
class getUploadToPath(object):
    def __call__(self, instance, filename):
        return instance.get_available_filepath(
                       root=os.path.join(settings.CLOSEDMEDIA_ROOT,
                                         DOWNLOADS_HOME_APP_VPZ),
                       sub="download", date=instance.date, filename=filename)

get_upload_to_path = getUploadToPath()

class UploadVpzDocument(LockableMixin, LoadVpzDocumentMixin, models.Model):
    """Uploaded document relative to erecord_vpz application"""

    date = models.DateTimeField(auto_now_add=True)

    fs = FileSystemStorage(location=settings.CLOSEDMEDIA_ROOT,
                           base_url=settings.CLOSEDMEDIA_URL)

    docfile = models.FileField(upload_to=get_upload_to_path, storage=fs,
                               max_length=600)

    key = models.CharField(max_length=60, blank=True,
                           help_text="upload file key")

    def is_old(self):
        """UploadVpzDocument is old if date 'older' than UPLOAD_LIFETIME"""
        return self.is_old_indays(days=UPLOAD_LIFETIME)

    def delete_if_old(self):
        """Deletes UploadVpzDocument and its associated folder, if too old """

        if self.is_old() :
            self.delete()

    def __str__(self):
        return "Upload Vpz Document id "+str(self.id)

    @classmethod
    def get_id(cls, key):
        """Finds the id of the objet with key and returns it, or else None """

        for document in UploadVpzDocument.objects.all() :
            if document.key == key :
                return document.id
        return None

def uploadvpzdocument_clear(sender, instance, *args, **kwargs):
    """Deletes UploadVpzDocument associated folder"""

    instance.clear_dir()

# done before a UploadVpzDocument deletion
pre_delete.connect(uploadvpzdocument_clear, sender=UploadVpzDocument)

@deconstructible
class getDownloadToPath(object):
    def __call__(self, instance, filename):
        return instance.get_available_filepath(
                        root=os.path.join(settings.CLOSEDMEDIA_ROOT,
                                          DOWNLOADS_HOME_APP_VPZ),
                        sub="download", date=instance.date, filename=filename)

get_download_to_path = getDownloadToPath()

class DownloadVpzDocument(LockableMixin, LoadVpzDocumentMixin, models.Model):
    """Downloadable document relative to erecord_vpz application"""

    date = models.DateTimeField(auto_now_add=True)

    fs = FileSystemStorage(location=settings.CLOSEDMEDIA_ROOT,
                           base_url=settings.CLOSEDMEDIA_URL)

    docfile = models.FileField(upload_to=get_download_to_path, storage=fs,
                               max_length=600)

    key = models.CharField(max_length=60, blank=True,
                           help_text="download file key")

    def is_old(self):
        """DownloadVpzDocument is old if date 'older' than DOWNLOAD_LIFETIME"""

        return self.is_old_indays(days=DOWNLOAD_LIFETIME)

    def delete_if_old(self):
        """Deletes DownloadVpzDocument and its associated folder, if too old """

        if self.is_old() :
            self.delete()

    @classmethod
    def create(cls, file_path):
        """Creates a DownloadDocument for file file_path"""

        f = File(open(file_path,"rb")) # f = File(open(file_path))
        document = cls()
        document.save()
        document.docfile.save(name=os.path.basename(f.name), content=f,
                              save=True) # save=False)
        document.key = document.def_key_value()
        document.save()
        return document

    def __str__(self):
        return "Download Vpz Document id "+str(self.id)

    @classmethod
    def get_id(cls, key):
        """Finds the id of the objet with key and returns it, or else None """

        for document in DownloadVpzDocument.objects.all() :
            if document.key == key :
                return document.id
        return None

def downloadvpzdocument_clear(sender, instance, *args, **kwargs):
    """Deletes DownloadVpzDocument associated folder"""

    instance.clear_dir()

# done before a DownloadVpzDocument deletion
pre_delete.connect(downloadvpzdocument_clear, sender=DownloadVpzDocument)

