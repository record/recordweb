"""erecord_slm

*This package is part of erecord - web services for vle models*

:copyright: Copyright (C) 2019-2024 INRA http://www.inra.fr.
:license: GPLv3, see LICENSE file for more details.
:authors: see AUTHORS file.

Downloadable and uploaded files management application
 
The slm application is dedicated to downloadable and uploaded files management.

The applications that have such files call for erecord_slm.

"""

