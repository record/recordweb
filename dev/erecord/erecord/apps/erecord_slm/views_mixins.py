"""erecord_slm.views_mixins

Mixins common to views using management provided by erecord_slm application

"""

from rest_framework.reverse import reverse

from erecord_cmn.utils.urls import url_add_options

from rest_framework.response import Response
from django.http import HttpResponse
from wsgiref.util import FileWrapper

from erecord_slm.serializers import KeyOptionSerializer
from erecord_slm.models import DownloadVpzDocument

class VpzDownloadViewMixin(object):
    """Additional methods for views relative to erecord_vpz application, with
    downloadable results
    """

    def build_download_folder(self, file_path):
        """Builds and returns a downloadable folder DownloadVpzDocument
        containing the file found at file_path.
        """

        download_document = DownloadVpzDocument.create(file_path=file_path)
        return download_document

    def get_download_url(self, request, download_document):
        """Returns the url to download the download_document content """

        key = download_document.key

        url = reverse('erecord_slm-download', request=request)
        if "?" in url : # remaining option(s)
            url = url.split("?")[0]

        options = {'key':key}
        
        more_text = "The resource to download the result is 'GET slm/download'"
        more_text += " with 'key' option (key value : " + key + ")"

        if download_document.has_lock() :
            options['jwt'] = "REPLACE_BY_YOUR_JWT_VALUE"
            more_text += " and 'jwt' option (jwt value : your own jwt).\n"
            more_text += "Don't forget to replace in your request jwt value "
            more_text += "by your own jwt value."

        download_url = url_add_options(url, options)
        more_text += "\n"
        more_text += "Keep this key value in order to be able to download"
        more_text += " the result later on : key=" + key

        return (download_url, more_text)

    def zip_response(self, zip_path):
        response = HttpResponse(FileWrapper(open(zip_path,'rb')),
                                content_type='application/zip')
        content_disposition = "attachment; filename="+zip_path+"'"
        response['Content-Disposition'] = content_disposition
        return response

    def download_response(self, url, label=None, more_text=None):
        if url:
            if not label :
                label = "The downloading url"
            context = label + " : " + url
            if more_text :
                text = more_text.replace('\n', ' *** ')
                context = context + " *** " + text
        else :
            context = "No downloadable result"
        return Response(context)


class KeyViewMixin(object):
    """Additional methods for views having key option """

    def get_key_value(self, data):
        """Returns the key value found in data and else, None """

        p = KeyOptionSerializer(data=data)
        p.is_valid()
        return p.data['key']

