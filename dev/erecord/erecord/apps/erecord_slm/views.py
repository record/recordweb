"""erecord_slm.views """

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.urls import reverse

from rest_framework.response import Response
from rest_framework import generics

from erecord_slm.models import UploadVpzDocument
from erecord_slm.forms import UploadDocumentForm
from erecord_slm.models import DownloadVpzDocument

from erecord_slm.views_mixins import VpzDownloadViewMixin
from erecord_slm.views_mixins import KeyViewMixin
from erecord_acs.views_mixins import LimitedAccessViewMixin
from erecord_cmn.views_mixins import FormatViewMixin
from erecord_cmn.views_mixins import DataViewMixin
from erecord_cmn.views_mixins import DetailViewMixin
from erecord_acs.views_mixins import AccessViewMixin

from erecord_cmn.utils.logger import get_logger
LOGGER = get_logger(__name__)
from erecord_cmn.utils.errors import logger_report_error
from erecord_cmn.utils.errors import build_error_message
from erecord_cmn.utils.errors import get_error_status
from erecord_cmn.utils.errors import Http400

#headsubtitle = "erecord_slm application ((down)(up)loaded files management)"
headsubtitle = "(erecord_slm)"


#def handle_uploaded_file(f):
#    with open('some/file/name.txt', 'wb+') as destination:
#        for chunk in f.chunks():
#            destination.write(chunk)

def erecord_vpz_management(request):
    """Management by erecord_slm of the erecord_vpz downloadable files.

    The erecord_slm application is dedicated to downloadable (and maybe later
    uploaded) files management.

    The erecord_vpz application is dedicated to activities on vpz, where
    some results may be downloadable (cf todownload mode case).

    Attention : this view should be in access only for administrator !

    The admin site can be used (as admin user) instead of this view.

    """

    # Handle file upload
    if request.method == 'POST':
        form = UploadDocumentForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = UploadVpzDocument(docfile = request.FILES['docfile'])
            newdoc.save()

            return HttpResponseRedirect(reverse(
                                      'erecord_slm-erecord_vpz_management'))
    else:
        form = UploadDocumentForm() # a empty, unbound form

    upload_documents = UploadVpzDocument.objects.all()
    download_documents = DownloadVpzDocument.objects.all()

    # Render list page with the documents and the form
    context = {'upload_documents': upload_documents, 'form': form,
               'download_documents': download_documents,}
    return render_to_response('erecord_slm/erecord_vpz_management.html',
                              context,
                              context_instance=RequestContext(request))

#------------------------------------------------------------------------------

#class UploadVpzDocumentDetail( ...

class DownloadVpzDocumentDetail(LimitedAccessViewMixin, KeyViewMixin,
                             VpzDownloadViewMixin, generics.RetrieveAPIView):
    """One Download Vpz document """

    def get(self, request, format=None, **kwargs):
        """Returns DownloadVpzDocument docfile content

        DownloadVpzDocument is identified by 'key' option, not by id

        DownloadVpzDocument is accessible by who knows its key (ie the user
        who made the request to build it).

        """

        try :
            data = request.query_params
            key = self.get_key_value(data=data)
            if key is None :
                errormsg = "Bad request : option key required"
                raise Http400(errormsg)

            download_documents = DownloadVpzDocument.objects.all().filter(key=key)
            number_documents = len(download_documents)
            if number_documents < 1:
                errormsg = "Bad request : nothing to download with that key="
                errormsg = errormsg + key
                raise Http400(errormsg)
            elif number_documents > 1:
                errormsg = "Bad request : problem with that key="
                errormsg = errormsg + key
                errormsg = errormsg + " too many candidates for that key"
                raise Http400(errormsg)
            download_document = download_documents[0] # the only one

            try:
                self.control_access(model=download_document, data=data)
            except :
                raise

            path = download_document.get_url()
            response = self.zip_response(zip_path=path)

        except Exception as e :
            s = get_error_status(e)
            errormsg = build_error_message(error=e)
            logger_report_error(LOGGER)
            response = Response(data={'detail':errormsg,}, status=s)

        return response

#------------------------------------------------------------------------------

class UploadVpzDocumentAccessView(AccessViewMixin, FormatViewMixin,
                                  DataViewMixin, DetailViewMixin,
                                  generics.RetrieveAPIView):

    def get(self, request, format=None, **kwargs):
        obj = UploadVpzDocument.objects.get(pk=kwargs['pk'])
        title = 'Access of UploadVpzDocument (Id ' + str(obj.id) + ')'
        return self.access_response(request=request, format=format,
                                    obj=obj, title=title)

class DownloadVpzDocumentAccessView(AccessViewMixin, FormatViewMixin,
                                    DataViewMixin, DetailViewMixin,
                                    generics.RetrieveAPIView):

    def get(self, request, format=None, **kwargs):
        obj = DownloadVpzDocument.objects.get(pk=kwargs['pk'])
        title = 'Access of downloadable document DownloadVpzDocument (Id ' + str(obj.id) + ')'
        return self.access_response(request=request, format=format,
                                    obj=obj, title=title)

#------------------------------------------------------------------------------

