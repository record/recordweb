"""erecord_slm.urls

Urls of the erecord_slm application

"""

from django.urls import path

from erecord_slm import views as slm_views

urlpatterns = [

    # Management of downloadable and uploaded files of erecord_vpz
    # Not online/activated because should be in access only for administrator
    # => Use the admin site (as admin user) instead of this url.
    ##path('vpz/', 'erecord_slm.views.erecord_vpz_management',
    ##     name="erecord_slm-erecord_vpz_management"),

    # download
    # path('download/<int:pk>/',
    path('download/', slm_views.DownloadVpzDocumentDetail.as_view(),
         name='erecord_slm-download' ),

    # access
    path('uploadvpzdocument/<int:pk>/access/',
         slm_views.UploadVpzDocumentAccessView.as_view(),
         name='erecord_vpz-uploadvpzdocument-access' ),
    path('downloadvpzdocument/<int:pk>/access/',
         slm_views.DownloadVpzDocumentAccessView.as_view(),
         name='erecord_vpz-downloadvpzdocument-access' ),

]

