"""erecord_slm.forms

Forms of the erecord_slm application

"""

from django import forms

import datetime 

class UploadDocumentForm(forms.Form):
    docfile = forms.FileField(label="Select a file :")

class TodownloadUrlForm(forms.Form):
    """Form dedicated to an url to download"""

    url = forms.URLField(label="The downloading url", max_length=1000)
    more = forms.CharField(max_length=100000)

    @classmethod
    def lifetime_text(cls, lifetime):
        end = datetime.datetime.now() + datetime.timedelta(days=lifetime)
        text = "Available " + str(lifetime) + " days, until " + str(end)
        return text

    def set(self, label=None, lifetime=None, help=None):

        url = self.fields['url']
        if label :
            url.label = label
        if lifetime :
            url.help_text = self.lifetime_text(lifetime)
        if help :
            url.help_text = url.help_text + " *** " + help

        more = self.fields['more']
        more.widget = forms.Textarea(attrs={'rows':8, 'cols':140})
        more.widget.attrs['readonly'] = True

