*******************************************************************************
                            Install erecord

            Case of production under debian with apache http server

              Versions : debian 9.5 (stretch), apache2.4

*******************************************************************************

"Rename" erecord path as /opt/erecord, if erecord is installed somewhere else
(ln -s 'erecord path' /opt/erecord).

================
Some basic tools
================

- sudo installation
  apt-get install sudo

- python 3 installation
  apt-get install python3

- pip installation
  apt-get install python-pip

- virtualenv tool installation 
  pip install virtualenv

- Apache HTTP Server

  - Apache2.4 installation
    apt-get install apache2

  - mod_wsgi installation      
    apt-get install libapache2-mod-wsgi

=======================
Miscellaneous libraries
=======================

libcurl installation
--------------------
Those libraries are needed by pycurl.

    sudo apt-get install libcurl4-openssl-dev
    sudo apt-get install libssl-dev

===========================
Copy of erecord source code
===========================

  - get erecord source code :
    git clone https://forgemia.inra.fr/record/recordweb.git
    => erecord source code is recordweb/erecord

  - copy erecord as /opt/erecord

Notes :
- /opt/erecord/factory and /opt/erecord/repositories are empty at this stage.
- /opt/erecord/factory will contain things generated later on.
- /opt/erecord/repositories will contain some models repositories installed
  later on.  

=====================
erecordenv virtualenv
=====================
Python modules are installed in a virtualenv.

- erecordenv virtualenv creation :
  virtualenv /opt/erecord/factory/install/erecordenv -p /usr/bin/python3 --no-site-packages

- erecordenv virtualenv activation :
  - in bash or sh case :
    source /opt/erecord/factory/install/erecordenv/bin/activate
  - in csh or tcsh case :
    source /opt/erecord/factory/install/erecordenv/bin/activate.csh

- just to control the activation of erecordenv virtualenv : which python

- installation (into erecordenv) from requirement.txt :
  pip install -r /opt/erecord/erecord/install/requirement.txt

==============================
erecord software configuration
==============================

- /opt/erecord/erecord/apps/erecord_cmn/configs/config.py file modifications :

      IN_PRODUCTION = True

- /opt/erecord/erecord/projects/ws/ws/settings.py file modifications :

      DEBUG = False

- URLs :
  Some URL values directly appear in source code (.py, .rst). They have to be
  in conformity with the erecord_ws file and with the documentation
  installation (see below) :

    If needed, replace erecord.toulouse.inra.fr by the appropriate IP address :

    - into /opt/erecord/docs/server/index.html

    - into /opt/erecord/erecord/apps/erecord_cmn/configs/config.py
      for ONLINEDOC_* 

    - into /opt/erecord/erecord/apps/erecord_cmn/templates/erecord_cmn/index.html

    - into /opt/erecord/docs/source/*

    Note : commands, if '127.0.0.1' had to be replaced by
           'erecord.toulouse.inra.fr' into foo.rst files :
           cd "foo path" ;
           sed -i 's/127.0.0.1/erecord.toulouse.inra.fr/g' foo.rst ;

====================
erecord static files
====================

Static files creation (css, js, images) :

  - Activate erecordenv virtualenv (if not yet done) :
    source /opt/erecord/factory/install/erecordenv/bin/activate
    or source /opt/erecord/factory/install/erecordenv/bin/activate.csh

  - generate :
    cd /opt/erecord/erecord/projects/ws
    python manage.py collectstatic

=========================
erecord database creation
=========================

Creation of the database (/opt/erecord/databases/erecord_default.sqlite3) :

  - Activate erecordenv virtualenv (if not yet done) :
    source /opt/erecord/factory/install/erecordenv/bin/activate
    or source /opt/erecord/factory/install/erecordenv/bin/activate.csh

  cd /opt/erecord/erecord/projects/ws

  - create database :
    python manage.py makemigrations erecord_db
    python manage.py makemigrations erecord_acs
    python manage.py makemigrations erecord_vpz
    python manage.py makemigrations erecord_slm
    python manage.py migrate

  - create admin user into database :
    python manage.py createsuperuser

==========================
erecord main documentation
==========================

- Main documentation generation :

  - Activate erecordenv virtualenv (if not yet done) :
    source /opt/erecord/factory/install/erecordenv/bin/activate
    or source /opt/erecord/factory/install/erecordenv/bin/activate.csh

  - generate :
    cd /opt/erecord/docs
    make clean
    make cleanref
    make ref
    make html

  Note : The resulting documentation is produced under /opt/erecord/factory/docs

- Main documentation installation :

  - add server entry :
    sudo cp /opt/erecord/docs/server/docs.html /opt/erecord/factory/docs/docs.html
    sudo cp /opt/erecord/docs/server/index.html /opt/erecord/factory/docs/index.html

  - save maybe existing /var/www/docs.html,index.html

  - install under /var/www :
    sudo ln -s /opt/erecord/factory/docs /var/www/erecord
    sudo ln -s /var/www/erecord/docs.html /var/www/docs.html
    sudo ln -s /var/www/erecord/index.html /var/www/index.html

====================
Apache configuration
====================

/etc/apache2/ports.conf file modifications
------------------------------------------
Modify the apache2 ports.conf file so that the Apache2 server listens to the
port 8000 dedicated to the erecord_ws site
(see /opt/erecord/erecord/projects/ws/ws/erecord_ws file) and the port 80
dedicated to the online documentation :

      Listen 8000
      Listen 80
      ...
      <IfModule ssl_module>
          Listen 443
      </IfModule>

/etc/apache2/apache2.conf file modifications
--------------------------------------------
Modify into the apache2.conf file the Timeout value (number of seconds
before receives and sends time out, initial value 300)

      Timeout 600

erecord_ws file modifications
-----------------------------
Give/control into the erecord_ws file the appropriate values (ServerName,
erecord paths), then install it in the appropriate location :

  - get the appropriate version of erecord_ws file :
    cp /opt/erecord/erecord/projects/ws/ws/debian/apache2.4/erecord_ws /opt/erecord/erecord/projects/ws/ws/erecord_ws 

  - /opt/erecord/erecord/projects/ws/ws/erecord_ws file modifications :

        ServerName : if needed, replace erecord.toulouse.inra.fr by the appropriate IP address

        erecord paths : /opt/erecord/...

  - install erecord_ws file :
    sudo cp /opt/erecord/erecord/projects/ws/ws/erecord_ws /etc/apache2/sites-available/erecord_ws.conf

/etc/apache2/sites-available/000-default.conf modifications
-----------------------------------------------------------
Modify the apache2 000-default.conf file so that it corresponds with the
erecord main documentation installation.

  - /etc/apache2/sites-available/000-default.conf file modifications :

        #DocumentRoot /var/www/html
	DocumentRoot /var/www

===
Run
===

- Apache2 user as owner :
  sudo chown -R www-data:www-data /opt
  sudo chown -R www-data:www-data /var/www

- Activate erecord_ws site :
  sudo a2ensite erecord_ws.conf

- Activate 000-default.conf site : is supposed to be already done.

- Apache run :
  sudo systemctl reload apache2
  (or sudo service apache2 reload)

=====
Notes
=====

- erecord online at :
  - erecord web site     : http://erecord.toulouse.inra.fr .
  - erecord web services : http://erecord.toulouse.inra.fr:8000 .

- Installation remaining to be done :
  - models repositories containers : see
    /opt/erecord/erecord/install/containers/install_singularity.txt
    /opt/erecord/erecord/install/containers/install_containers.txt             
  - simulators HTML home pages : see
    /opt/erecord/erecord/install/prod/install_vpz_html_home_pages.txt

- log files :
  - log files are into /opt/erecord/factory/log
  - original apache log files : /var/log/apache2/error.log
                                /var/log/apache2/access.log...
  - see more at /opt/erecord/erecord/install/prod/install_log.txt.

- See also /opt/erecord/erecord/install/help.txt.

*******************************************************************************

