
****************************************************
*                                                  *
*            generation des vpzpages               *
*         depuis n'importe quel endroit            *
*            repertoire_de_production              *
*                                                  *
*   => le resultat : 'gen_vpzpages' et 'private'   *
*                                                  *
****************************************************

# repertoire de production
mkdir repertoire_de_production
cd repertoire_de_production

# erecordroot : maj value !!!
export erecordroot="/home/nrousse/workspace_svn/DEVELOPPEMENT_WEB/ERECORD/trunk"

# verification emplacement code erecord :
echo ${erecordroot}

# environnement
source ${erecordroot}/erecord/factory/erecordenv/bin/activate
cp -f ${erecordroot}/erecord/docs/vpzpages/get_token.py .
cp -f ${erecordroot}/erecord/docs/vpzpages/build_vpzpages.py .

# token file : avec username_value et password_value !!!
python get_token.py username_value password_value > token_value

# generation des pages
python build_vpzpages.py

=> le resultat : 'gen_vpzpages' et 'private'
   - 'gen_vpzpages' (a copier en tant que '/var/www/docs/vpzpages/gen')
   - 'private' (a garder hors deploiement erecord)


****************************************************
*                                                  *
*        nettoyage (productions et copies)         *
*                                                  *
****************************************************

source clean.sh

