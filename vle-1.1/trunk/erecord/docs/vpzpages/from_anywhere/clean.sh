#!/bin/bash
# 

#*****************************
#*        nettoyage          *
#*****************************

# pages generees
rm -fr gen_vpzpages
rm -fr private

# copies
rm -f get_token.py
rm -f build_vpzpages.py

# token file
rm -f token_value

