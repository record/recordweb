.. _faqs_users:

==========
Users FAQs
==========

How to call/use the web services ?
==================================

    See :ref:`webapi` | :ref:`using`


Restitutions
============

    See :ref:`webapi` | :ref:`some examples <erecord_examples>`


Can I use any model of the erecord web services ?
=================================================

.. _faq_can_anyone_use_anymodel:

Can anyone use any model ?
--------------------------

    - No. A model may be public or in limited access. Authentication is
      required to use a model that is in limited access.
      :ref:`More <webapi_feat_access>`.

What are public models and models in limited access ?
-----------------------------------------------------

    - See :ref:`webapi_feat_access`

How to authenticate into erecord ?
----------------------------------

    - See :ref:`erecord_authentication`

What is JSON Web Token authentication ?
---------------------------------------

    - See :ref:`erecord_authentication`

How to get a token (JSON Web Token) ?
-------------------------------------

    - See :ref:`howto_get_jwt`

I would like to use a simulator that is in limited access. Do I have to register myself into erecord ?
------------------------------------------------------------------------------

    - No.

      .. include:: ../../devel/design/features/no_declaration.rst

Can I use my simulation results later on ?
==========================================

    - When calling the erecord web services from a web browser, it is possible
      to **store some simulation results into the web browser**, in order to
      use them later on.

      See :ref:`webapi_opt_storage` | :ref:`webapi_feat_storage`

      See also the page "Stockage de résultats de simulation dans le navigateur web" of document : `Using an agronomic model developped with the Record platform and installed into the erecord web services <http://147.100.164.34/docs/erecord/model_user.pdf>`_ *(fr) (Utiliser un modèle agronomique développé sous la plateforme Record et disponible dans les services web erecord)*.

    - Some resources return **results as files** (simulation results,
      parameters values) :

      :ref:`get_vpz_report` | :ref:`get_vpz_report_conditions` | :ref:`get_vpz_experiment`

      :ref:`post_vpz_report` | :ref:`post_vpz_report_conditions` | :ref:`post_vpz_experiment`

        - In case of a resource returning result files, you can keep your
          received result files as long as you want and use them when you want.

        - In case of a resource returning result files, it is possible to ask
          for downloading the result files later on (instead of receiving
          them directly).

          See :ref:`webapi_opt_todownload` | :ref:`get_slm_download`

How can I share my simulation results with someone else ?
=========================================================

    - Some resources return **results as files** (simulation results,
      parameters values) :

      :ref:`get_vpz_report` | :ref:`get_vpz_report_conditions` | :ref:`get_vpz_experiment`

      :ref:`post_vpz_report` | :ref:`post_vpz_report_conditions` | :ref:`post_vpz_experiment`

        - In case of a resource returning result files, you can **give** your
          received **result files to who you want**.

        - In case of a resource returning result files that you have asked
          for downloading later on, you can **allow** who you want **to
          download the result files** by **giving** the **required key**.

          See :ref:`get_slm_download` | :ref:`webapi_opt_todownload`

Can I run a simulation with my own input datas files ?
======================================================

    See :ref:`webapi_feat_datafolder`

