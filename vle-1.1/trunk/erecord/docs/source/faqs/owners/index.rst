.. _faqs_owners:

=================
Model owners FAQs
=================

erecord and my agronomic model project
======================================

I intend to develop an agronomic model into the :term:`Record` platform. What about the erecord web services ?
------------------------------------------------------------------------------

    See `Modelling into the Record platform and the erecord web services <http://147.100.164.34/docs/erecord/model_owner.pdf>`_ *(fr) (Modélisation sous la plateforme RECORD et services web erecord)*

I am developing an agronomic model into the :term:`Record` platform. What about the erecord web services ?
------------------------------------------------------------------------------

    See `Modelling into the Record platform and the erecord web services <http://147.100.164.34/docs/erecord/model_owner.pdf>`_ *(fr) (Modélisation sous la plateforme RECORD et services web erecord)*

My agronomic model, developed into the :term:`Record` platform, has now reached some stability state. What about the erecord web services ?
------------------------------------------------------------------------------

    See `Modelling into the Record platform and the erecord web services <http://147.100.164.34/docs/erecord/model_owner.pdf>`_ *(fr) (Modélisation sous la plateforme RECORD et services web erecord)*

My agronomic model, developed into the :term:`Record` platform, is now finalized, operational. What about the erecord web services ?
------------------------------------------------------------------------------

    See `Modelling into the Record platform and the erecord web services <http://147.100.164.34/docs/erecord/model_owner.pdf>`_ *(fr) (Modélisation sous la plateforme RECORD et services web erecord)*


Can my agronomic model remain private under erecord ?
=====================================================

Is everybody allowed to use my agronomic model if I deliver it to erecord ?
---------------------------------------------------------------------------

    - It depends on what you want. When you deliver your model to erecord,
      you choose between declaring it as a public model or as a model in
      limited access. In that last case, you will also select who will be
      allowed to use it.
      :ref:`More <webapi_feat_access>`.

If I deliver my agronomic model to erecord, can visitors see or load its source code ?
------------------------------------------------------------------------------

    - Not at all. The erecord web services never allow anyone to see -and
      even less load- the source code of delivered agronomic models.

    - Delivering your model to erecord gives you the opportunity of using it
      by web services and if you want, to share this opportunity with someone
      else. By 'using the model', one means simulating it and seeing its
      agronomic parameters. This doesn't concern the source code of your
      agronomic model.

    - Delivering your model to erecord implies to send its source code to
      erecord project. Indeed the model software has to be installed into
      erecord in order to be able to run simulations. But the installed 
      source code of delivered agronomic models is not accessible to 
      erecord visitors.

