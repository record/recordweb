.. _faqs_models:

===========================
FAQs about agronomic models
===========================

Can erecord web services be used for any agronomic model ?
----------------------------------------------------------

    - The agronomic model has to be an operational model developed with
      **vle-1.1** version of vle.

    - The agronomic model software will have to be previously installed on the
      erecord server (see an
      `illustration <http://147.100.164.34/docs/erecord/erecord.pdf>`_ *(fr)* ), that
      is to say (i) physically stored into one of the models repositories and
      (ii) recorded into the databases of the erecord project. For more
      information, :ref:`contact us <erecord_contacts>`.

    - The agronomic model software has to follow some rules :


          - The current running directory must not be used for
            anything else than storing the simulation results, and those
            simulation running productions must not be written anywhere else
            than into this 'output' directory.
          - The model must not get its input datas anywhere else than into the
            data directory of its main vle package.
          - The simulation shouldn't display anything on screen while running.
          - The erecord web services doesn't accept any types of parameters
            that can be chosen while building a simulator into the
            :term:`Record` platform. For a simulator to be able to be called
            by erecord web services, its parameters must have types among :
            Boolean, Double, Integer, String, Map, Set, Matrix, Table, Tuple
            (at 1st level, and in other levels : Boolean, Double, Integer,
            String, Map, Set).

