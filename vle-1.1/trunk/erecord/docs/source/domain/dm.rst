.. _domain_dm:

==========
Data model
==========

.. toctree::
   :maxdepth: 2

   dm_erecord_db
   dm_erecord_vpz
   dm_erecord_acs

