.. _related:

================
Related projects
================

Web solutions for the Record platform (past project)
====================================================

Before developping the erecord web services, some work had already been done
about web solutions for models of the :term:`Record` platform.

- :ref:`The webrecord project <webrecord_project>` (2011-2014)

- :ref:`Means platform as client of Record platform web services <meansrecord_trainingcourse>` (2014)

AnaEE-France project as client of erecord web services
======================================================

- :ref:`AnaEE-France project as client of Record platform web services <anaeefrance_project>` (since 2015)

Recordschool model as a use case
================================

- :ref:`recordschool` (since 2017)

Model analysis
==============

The erecord web services can be used for models analysis, by providing
some simulations for example to a R program that manages analysis.

- See : :ref:`model analysis <model_analysis>` based on the WWDM model.

- See : `model analysis <http://147.100.164.34/docs/models/recordschool/sequence_8_3_hackaton_3.pdf>`_ *(fr)* based on the :ref:`recordschool model <recordschool>`.

