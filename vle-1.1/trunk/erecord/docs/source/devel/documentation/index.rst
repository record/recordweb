.. _devel_documentation:

=============
Documentation
=============

Introduction
============

The **main documentation** of erecord project is produced with Sphinx.

Sphinx is a documentation generator which converts reStructuredText files into
HTML websites and other formats including PDF, EPub and man).

There is some **other external documentation**, for example html home and pattern pages produced for erecord simulators (with a specific python script).

Production and generation
=========================

Prerequisite
------------

    Activate erecordenv virtualenv (if not yet done) :

    :ref:`cmd_activate_erecordenv_virtualenv`


Main documentation
------------------

    Generation :

    The generation commands are done under :ref:`erecord_docs_path
    <deploy_erecord_docs_path>`.

    .. literalinclude:: ../deployment/include/cmd_make_doc.rst


Some other external documentation
---------------------------------

    Generation :

    The generation commands are done under :ref:`erecord_docs_path
    <deploy_erecord_docs_path>`.

    .. literalinclude:: ../deployment/include/cmd_make_vpzpages.rst

For more
--------

For more, see :ref:`documentation generation and installation <geninstall_documentation>`

