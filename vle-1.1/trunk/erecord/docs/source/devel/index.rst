.. _devel:

============================
Development process overview
============================

Design
======

.. toctree::
   :maxdepth: 1

   Web API <design/webapi/index>
   Features <design/features/index>

Source code API reference
=========================

- :ref:`apiref_index`

Documentation
=============

.. toctree::
   :maxdepth: 2

   documentation/index

Test
====

.. toctree::
   :maxdepth: 2

   test/index
   benchmark/index

Deployment
==========

.. toctree::
   :maxdepth: 2

   deployment/index

erecord project content
=======================

.. toctree::
   :maxdepth: 2

   erecord/index

More
====

.. include:: ../include/more.rst

