.. _webapi_feat_datafolder:

================================
Modifying the input datas folder
================================

Introduction
------------

For some resources, it is possible to **modify the input datas folder** of
the simulator according to a posted file (optional).

How to modify the input datas folder
------------------------------------

.. include:: ../webapi/include/vpz_modify_datafolder.rst

Parameters : :ref:`webapi_opt_datafolder` | :ref:`webapi_opt_datafoldercopy`

Concerned resources
-------------------

Some resources for which the input datas folder can be modified :

    :ref:`post_vpz_output` | :ref:`post_vpz_experiment` |
    :ref:`post_vpz_report` | :ref:`post_vpz_inout`

Some examples
-------------

Examples in command line with cURL illustrating how to modify the input datas folder by using datafolder and datafoldercopy.

    .. include:: ../../../using/examples/include/wwdm_curl_intro.rst

    .. literalinclude:: ../../../using/examples/include/memo_request_datafolder.rst

- :ref:`post_vpz_output` :

    .. literalinclude:: ../../../using/examples/include/memo_request_135_3.rst

- :ref:`post_vpz_experiment` :

    .. literalinclude:: ../../../using/examples/include/memo_request_171_3.rst

- :ref:`post_vpz_report` :

    .. literalinclude:: ../../../using/examples/include/memo_request_160_3.rst

- :ref:`post_vpz_inout` :

    .. literalinclude:: ../../../using/examples/include/memo_request_130_3.rst

