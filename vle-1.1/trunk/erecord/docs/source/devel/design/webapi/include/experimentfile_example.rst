
For example the :download:`posted xls file<../../../using/examples/include/experiment_post_in/experiment.xls>` of a :ref:`post_vpz_experiment` request has been written from the :download:`xls file<../../../using/examples/include/experiment_get/experiment.xls>` returned by a :ref:`get_vpz_experiment` request.

