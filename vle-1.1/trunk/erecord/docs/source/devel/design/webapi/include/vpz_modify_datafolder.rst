
The file (optional) posted to **modify the input datas folder** is a zip file given by the user into the POST request (HTTP multipart request) as 'datafolder' parameter.
The way how this zip file content is taken into account depends on the 'datafoldercopy' parameter value.

