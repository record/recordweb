.. _webapi_opt_bycol:

==============
mode for bycol
==============

The :ref:`'mode' option <webapi_opt_mode>` is used to choose to have some
arrays presented by colums instead of, by default, by rows.

Value 'bycol' (mode=bycol) for some arrays to be by columns.

