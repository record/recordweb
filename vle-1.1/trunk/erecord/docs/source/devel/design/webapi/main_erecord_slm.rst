.. _webapi_erecord_slm:

===============================
Web API erecord_slm application
===============================

Overview
========
.. include:: include/slm_intro.rst

Main resources
==============
.. include:: slm_resources.rst

Options
=======

* :doc:`opt_key`

More
====
.. include:: ../../../include/more.rst

