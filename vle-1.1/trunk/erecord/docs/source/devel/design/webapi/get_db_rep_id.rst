.. _get_db_rep_id:

===============
GET db/rep/{Id}
===============

Resource URL
============
:ref:`online_url`\ **/db/rep/{Id}**

Description
===========

Returns the identification information of the :term:`models repository` having
the Id value (as :ref:`dm_vlerep`).

Details
-------

*style*
    .. include:: include/db_style.rst

Request parameters
==================

* *Id* : id value of the models repository as :ref:`dm_vlerep`

* :ref:`webapi_opt_style` 

* :ref:`webapi_opt_format` 

Response result
===============

The returned result is about :ref:`dm_vlerep`.

Example
=======

An overview example that you can adapt to what you need :

    .. include:: ../../../using/examples/include/db_quickoverview.rst

Some use case examples in : :ref:`wwdm_ident` (from
:ref:`examples <erecord_examples>`).

More
====

This resource is part of :ref:`webapi_db`

Go to :doc:`include/db_rep_id_more`

