.. _webapi_erecord_cmn:

===============================
Web API erecord_cmn application
===============================

Options
=======

* :doc:`opt_mode`

  :doc:`opt_style` ( :doc:`more <include/opt_style_more>` )

  :doc:`opt_plan_restype`

  :doc:`opt_todownload`

  :doc:`opt_storage` ( :doc:`more <include/opt_storage_more>` )
 
* :doc:`opt_jwt`

* :doc:`opt_format`

* :doc:`opt_datafoldercopy` ( :doc:`more <include/opt_datafoldercopy_more>` )

More
====
.. include:: ../../../include/more.rst
