.. _get_slm_download:

================
GET slm/download
================

Resource URL
============
:ref:`online_url`\ **/slm/download**

Description
===========

Returns the downloadable file that is identified by 'key' value.

Request parameters
==================

**Required** : key must be provided.

**Required** : jwt in limited access case.

* :ref:`webapi_opt_key`

* :ref:`webapi_opt_jwt`


Response result
===============

The returned result is the requested downloadable file.

More
====

This resource is part of :ref:`webapi_slm`

