.. _webapi_opt_mode:

====
mode
====

The 'mode' option is used for different choices that are available or not 
according to the calling case. Some may be incompatible each other.

- Style :
      see :ref:`webapi_opt_style`

- Simulation running mode (plan and restype) :
      see :ref:`webapi_opt_plan_restype`

- Storage of previous simulations :
      see :ref:`webapi_opt_storage`

- Downloadable results :
      see :ref:`webapi_opt_todownload`

