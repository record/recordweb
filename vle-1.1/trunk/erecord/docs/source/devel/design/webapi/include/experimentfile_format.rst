
The posted xls file of the :ref:`post_vpz_experiment` request has the same
format and structure than the xls file returned by the
:ref:`get_vpz_experiment` request.

