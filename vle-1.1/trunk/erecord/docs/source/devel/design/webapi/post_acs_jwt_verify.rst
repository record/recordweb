.. _post_acs_jwt_verify:

===================
POST acs/jwt/verify
===================

Resource URL
============
:ref:`online_url`\ **/acs/jwt/verify**

:ref:`Try it <online_url_jwt_verify>`

--------------------------------------

Description
===========

Checks the veracity of a token (JSON Web Token) value, returning the token
value if it is valid.

Help
----

A JWT value is available for a limited duration. For more, see
:ref:`JWT in practice <erecord_authentication>`

Request parameters
==================

**Required** : token.

* *token* : JWT value to be verified.


Response result
===============

"token" : the returned result is the JWT value if it is valid.

Example
=======

*under construction*

More
====

This resource is part of :ref:`webapi_acs`

