
The 'acs' web services are provided by the :term:`erecord_acs` application
that is dedicated to access management (authentication...).

A model can be public or in limited access.

See :ref:`webapi_feat_access` | :ref:`erecord_authentication`

The 'acs' web services allow to **get and verify a token (JSON Web Token)**,
to **get information about simulators access** (simulators as :ref:`dm_vlevpz`
or as :ref:`dm_vpzpath`).

