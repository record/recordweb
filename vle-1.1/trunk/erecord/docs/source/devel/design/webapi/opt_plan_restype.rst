.. _webapi_opt_plan_restype:

=========================
mode for plan and restype
=========================

The :ref:`'mode' option <webapi_opt_mode>` is used to choose the simulation
running mode, through values for plan and restype.

Values 'single' or 'linear' : plan value. Default value : 'single'.

    - mode=single for plan=single
    - mode=linear for plan=linear

Values 'dataframe' or 'matrix' : restype value. Default value : 'dataframe'.

    - mode=dataframe for restype=dataframe
    - mode=matrix    for restype=matrix

.. note:: 'plan' and 'restype' can also be used as option names (like 'mode') :

   plan=single, plan=linear.

   restype=dataframe, restype=matrix.

