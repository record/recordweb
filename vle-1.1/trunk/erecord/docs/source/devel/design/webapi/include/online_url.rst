.. _online_url:

====================================
http://erecord.toulouse.inra.fr:8000
====================================

Access to the erecord web services
==================================

**The erecord web services are online at** http://erecord.toulouse.inra.fr:8000.

You have to use the complete URL of the required resource, for example http://erecord.toulouse.inra.fr:8000/db/vpz as URL for the *'GET db/vpz'* resource.

More
====

To know the URL of each resource : :ref:`webapi`.

You can call the web services by :ref:`different ways <erecord_callways>`.

A Web User Interface to call more easily the web services : :ref:`webui`.

Help
====

:ref:`HELP <using>` |
:ref:`webapi` |
:ref:`webui` |
:ref:`An example readable as a tutorial <erecord_examples>`

