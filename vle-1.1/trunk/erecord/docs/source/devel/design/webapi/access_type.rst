.. _webapi_access_type:

===========
access type
===========

An access can be 'public' for a public access case or 'limited' for a limited access case.


For more see :ref:`webapi_feat_access` | :ref:`erecord_authentication`.

