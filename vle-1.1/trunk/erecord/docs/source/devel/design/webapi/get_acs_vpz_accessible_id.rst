.. _get_acs_vpz_accessible_id:

=========================
GET acs/vpz/accessible/id
=========================

Resource URL
============
:ref:`online_url`\ **/acs/vpz/accessible/id**

Description
===========

Returns the list of id of the accessible :term:`simulator`\ s (as :ref:`dm_vlevpz`), according to the parameters values.

Details
-------

The accessibility depends on the parameters values (*access*, *jwt*) used to filter the list.

*jwt*
    .. include:: include/acs_jwt.rst


Request parameters
==================

* :ref:`webapi_opt_access`

* :ref:`webapi_opt_jwt`

* :ref:`webapi_opt_format` 

Response result
===============

"list" : list of id of the accessible :term:`simulator`\ s (as :ref:`dm_vlevpz`).

Example
=======

*under construction*

More
====

This resource is part of :ref:`webapi_acs`

