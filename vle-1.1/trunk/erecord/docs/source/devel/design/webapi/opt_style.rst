.. _webapi_opt_style:

==============
mode for style
==============

The :ref:`'mode' option <webapi_opt_mode>` is used to select the style
presentation.

* mode 

    - value 'link' (mode=link) for presentation by links.
    - value 'tree' (mode=tree) for presentation by opening the tree.
    - value 'compact' (mode=compact).
    - value 'compactlist' (mode=compactlist).

.. note:: 'style' can also be used as option name (like 'mode') :
   style=link, style=tree, style=compact, style=compactlist.

The values 'link' and 'tree' are the main style values.

In 'link' value case, the information given as links is accessible only if the
erecord software has been set to make it possible.

The values 'compact' and 'compactlist' are two other style values, that are
not always available.

.. note::
   'compact' and 'compactlist' values can be helpful to prepare a following 
   command (where parameters modification, output datas selection...), can be
   handy to then exploit the returned datas (output datas, parameters...). 

.. note::
   see also :ref:`webapi_opt_format`
