
====================
More about vpz/inout
====================

The 'vpz/inout' resource is one of the activities on a vpz (see :ref:`webapi_vpz`).

Code
====

The corresponding class is InOutView.

For more about InOutView, see :

    .. autoclass:: erecord_vpz.views.activities.InOutView
       :members:
