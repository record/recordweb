
The 'vpz' web services are provided by the :term:`erecord_vpz` application.
They allow to **modify and run simulators**.

The **activities** on :term:`vpz (as VpzAct)` consist in : seeing and may be
modifying :term:`vpz input information`, running simulation and seeing the
resulting :term:`vpz output information`.

With **reports** on :term:`vpz (as VpzAct)`, the returned results are some
file reports.

With **experiment**, you build your own experiment plan from a
:term:`vpz (as VpzAct)` and get the simulation results into xls files.

Some other 'vpz' web services : **lists and details**, **menus**.

