.. _webapi_db_resources:

- **Lists and details**

    - :ref:`get_db_rep`    : to identify some models repositories
    - :ref:`get_db_rep_id` : to identify a models repository
    - :ref:`get_db_pkg`    : to identify some models
    - :ref:`get_db_pkg_id` : to identify a model
    - :ref:`get_db_vpz`    : to identify some simulators
    - :ref:`get_db_vpz_id` : to identify a simulator

- **List of data files**

    - :ref:`get_db_pkg_id_datalist` : list of names of data files of a model

- **name**

    - :ref:`get_db_rep_id_name` : name of a models repository
    - :ref:`get_db_pkg_id_name` : name of a model
    - :ref:`get_db_vpz_id_name` : name of a simulator

You will find help on how the 'db' web services are used in the
:ref:`examples <erecord_examples>`, especially in :ref:`wwdm_ident`.

