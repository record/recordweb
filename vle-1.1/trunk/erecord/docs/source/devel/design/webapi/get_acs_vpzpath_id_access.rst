.. _get_acs_vpzpath_id_access:

===========================
GET acs/vpzpath/{Id}/access
===========================

Resource URL
============
:ref:`online_url`\ **/acs/vpzpath/{Id}/access**

Description
===========

Returns the access type of the :term:`simulator` (as :ref:`dm_vpzpath`)
having the Id value (as :ref:`dm_vpzpath`).

Request parameters
==================

* *Id* : id value of the simulator as :ref:`dm_vpzpath`

* :ref:`webapi_opt_format` 

Response result
===============

* :ref:`access <webapi_access_type>`

Example
=======

*under construction*

More
====

This resource is part of :ref:`webapi_acs`

