.. _webapi_erecord_vpz:

===============================
Web API erecord_vpz application
===============================

Overview
========
.. include:: include/vpz_intro.rst

Main resources
==============
.. include:: vpz_resources.rst

Other resources
===============
.. include:: vpz_others.rst

Options
=======

* :doc:`opt_report` ( :doc:`more <include/opt_report_more>` )
* :doc:`opt_outselect` ( :doc:`more <include/opt_outselect_more>` )
* :doc:`opt_parselect` ( :doc:`more <include/opt_parselect_more>` )
* :doc:`opt_begin` ( :doc:`more <include/opt_begin_more>` )
* :doc:`opt_duration` ( :doc:`more <include/opt_duration_more>` )
* :doc:`opt_pars` ( :doc:`more <include/opt_pars_more>` )
* *experimentfile*
* *datafolder*

More
====
.. include:: ../../../include/more.rst
