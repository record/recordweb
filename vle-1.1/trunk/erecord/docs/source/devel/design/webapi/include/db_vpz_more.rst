
=================
More about db/vpz
=================

The 'db/vpz' resource is one of the resources of :ref:`webapi_db`.

Code
====

The corresponding class is VleVpzList.

For more about VleVpzList, see :

    .. autoclass:: erecord_db.views.objects.VleVpzList
       :members:
