.. _webapi_db_vpz_filter:

===
vpz
===

The 'vpz' option is used to specify which :term:`simulator` to keep (filtering all the other ones). The simulator under consideration is a :ref:`dm_vlevpz`.

- vpz

    - value : id of the relevant :ref:`dm_vlevpz`

.. note::

   To know the existing simulators : :ref:`get_db_vpz` resource (see 
   :ref:`the 'db' web services <webapi_db_resources>`)

   You will find help in the :ref:`examples <erecord_examples>`, especially
   in :ref:`wwdm_ident`.

