====================
More about vpz/output
====================

The 'vpz/output' resource is one of the activities on a vpz (see
:ref:`webapi_vpz`).

Code
====

The corresponding class is OutputView.

For more about OutputView, see :

    .. autoclass:: erecord_vpz.views.activities.OutputView
       :members:

Particular case
===============

The :ref:`parselect <webapi_opt_parselect>` option, enabling to choose the
restituted parameters is available in POST command case, but has no
immediate effect since :ref:`dm_vpzinput` is not returned by vpz/output.
However, it is recorded into the database as defined by the option.

