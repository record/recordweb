.. _post_acs_jwt_obtain:

===================
POST acs/jwt/obtain
===================

Resource URL
============
:ref:`online_url`\ **/acs/jwt/obtain**

:ref:`Try it <online_url_jwt_obtain>`

--------------------------------------

Description
===========

Returns a JWT (JSON Web Token) value.

For more see :ref:`erecord_authentication` | :ref:`howto_get_jwt`.

Details
-------

*username, password* : :ref:`how to get them <ask_for_access_authorization>`

Help
----

The JWT value can then be used as a :ref:`webapi_opt_jwt` parameter of requests in limited access case (see :ref:`erecord_authentication`).

Request parameters
==================

**Required** : username and password.

* *username*

* *password*

Response result
===============

"token" : the returned result is an available JWT value.

Example
=======

.. include:: ../../../using/examples/get_jwt.rst

More
====

This resource is part of :ref:`webapi_acs`


