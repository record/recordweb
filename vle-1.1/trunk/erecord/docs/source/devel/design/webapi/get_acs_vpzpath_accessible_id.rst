.. _get_acs_vpzpath_accessible_id:

=============================
GET acs/vpzpath/accessible/id
=============================

Resource URL
============
:ref:`online_url`\ **/acs/vpzpath/accessible/id**

Description
===========

Returns the list of id of the accessible :term:`simulator`\ s (as :ref:`dm_vpzpath`), according to the parameters values.

Details
-------

The accessibility depends on the parameters values (*access*, *jwt*) used to filter the list.

*jwt*
    .. include:: include/acs_jwt.rst


Request parameters
==================

* :ref:`webapi_opt_access`

* :ref:`webapi_opt_jwt`

* :ref:`webapi_opt_format` 

Response result
===============

"list" : list of id of the accessible :term:`simulator`\ s (as :ref:`dm_vpzpath`).

Example
=======

*under construction*

More
====

This resource is part of :ref:`webapi_acs`

