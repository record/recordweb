.. _get_vpz_output:

==============
GET vpz/output
==============

Resource URL
============
:ref:`online_url`\ **/vpz/output**

Description
===========

Returns the **simulation results** of a simulator (see :term:`output information of a vpz`) after having run simulation.

Details
-------

*jwt*
    .. include:: include/acs_jwt.rst

*style*
    .. include:: include/vpz_style_output.rst

*style, restype*
    .. include:: include/vpz_style_and_restype.rst

Help
----

See :ref:`an example as help on how to know the simulators that can be chosen
with the 'vpz' option <wwdm_ident>`.

Request parameters
==================

**Required** : either vpz or vpzpath must be provided.

**Required** : jwt in limited access case.

* :ref:`webapi_activity_vpz_choice`

* :ref:`webapi_opt_plan_restype`

* :ref:`webapi_opt_style` 

* :ref:`webapi_opt_format` 

* :ref:`webapi_opt_jwt`

Response result
===============

The returned result is about :ref:`dm_vpzoutput`.


Example
=======

    See :ref:`wwdm_runasis`.

More
====

This resource is part of :ref:`webapi_vpz`

Go to :doc:`include/vpz_output_more`

