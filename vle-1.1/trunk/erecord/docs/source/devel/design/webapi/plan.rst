
============
Web API plan
============
*Web API documentation, including the hidden (not inline) resources*

.. toctree::
   :maxdepth: 1

   main

.. toctree::
   :maxdepth: 1

   main_erecord_db
   main_erecord_vpz
   main_erecord_cmn
