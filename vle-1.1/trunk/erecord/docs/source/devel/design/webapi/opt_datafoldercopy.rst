.. _webapi_opt_datafoldercopy:

==============
datafoldercopy
==============

The 'datafoldercopy' option is used to choose the way how to take into account the new input datas folder posted by the user (see :ref:`webapi_opt_datafolder` parameter).

* datafoldercopy

    - value 'replace' : the new input datas folder replaces the original one.

    - value 'overwrite' : the content of the new input datas folder is added
      to the original one (by overwriting).

