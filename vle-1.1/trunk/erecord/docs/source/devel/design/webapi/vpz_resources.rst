.. _webapi_vpz_resources:

- **Activities on a vpz**

    - :ref:`get_vpz_input`   : to see a simulator
    - :ref:`post_vpz_input`  : to see a modified simulator
    - :ref:`get_vpz_output`  : to run a simulator
    - :ref:`post_vpz_output` : to run a modified simulator
    - :ref:`get_vpz_inout`   : both to see and run a simulator
    - :ref:`post_vpz_inout`  : both to see and run a modified simulator

- **Reports on a vpz**

    - :ref:`get_vpz_report`  : for reports about a simulator conditions and
      results.
    - :ref:`post_vpz_report` : for reports about a modified simulator
      conditions and results.
      
    - :ref:`get_vpz_report_conditions`  : for reports about a simulator
      conditions
    - :ref:`post_vpz_report_conditions` : for reports about a modified
      simulator conditions

- **Experiment plan by xls files**

    - :ref:`get_vpz_experiment`  : to get and see the experiment plan.
    - :ref:`post_vpz_experiment` : to send and run the modified experiment plan.

- **Lists and details**

    - *GET vpz/vpzpath* : list of :ref:`dm_vpzpath`.
    - *GET vpz/vpzpath/{Id}* : :ref:`dm_vpzpath` with Id as id.

