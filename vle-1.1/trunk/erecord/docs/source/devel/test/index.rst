.. _devel_test:

====
Test
====

Introduction
============

The erecord project software tests have been made with some testing tools from
django framework and the django application django-rest-framework *(based on
python libraries)*.

The software tests can be automatically run. They should be run after
software modifications to verify non regression.

Tests description
=================

The written tests imply GET vpz/input, POST vpz/input, and POST vpz/output
resources. Tests are written for 2 cases :

    - case of a simulator based on a VpzPath : cf test_vpz_from_vpzpath.
    - case of a simulator defined by a VleVpz (of a VlePkg of a VleRep) : cf
      test_vpz_from_vlevpz *(.. under construction ..)*.

Running tests
=============

Tests can be run using the test command of the django project’s manage.py
utility :

    python manage.py test

This command runs all the TestCase (subclasses) found into all the test*.py
files.
 
.. note::
   *To run tests, ACTIVITY_PERSISTENCE should value True (see the configuration
   file config.py of the erecord_cmn application).*

