.. _citing_erecord:

=============
Citing Record
=============

If :term:`erecord <erecord project>` contributes to a project that leads to a
scientific publication, please acknowledge this fact by **citing the**
:term:`Record` **platform reference publication** :

    *J.-E. Bergez, P. Chabrier, C. Gary, M.H. Jeuffroy, D. Makowski, G. Quesnel, E. Ramat, H. Raynal, N. Rousse, D. Wallach, P. Debaeke, P. Durand, M. Duru, J. Dury, P. Faverdin, C. Gascuel-Odoux, F. Garcia, An open platform to build, evaluate and simulate integrated models of farming and agro-ecosystems, Environmental Modelling & Software, Volume 39, January 2013, Pages 39-49, ISSN 1364-8152, 10.1016/j.envsoft.2012.03.011.*

