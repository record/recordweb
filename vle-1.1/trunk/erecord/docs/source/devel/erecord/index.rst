
.. _erecord_project:

================
erecord project
================

Introduction
============

:term:`Record` is a simulation platform for agro-ecosystems developped at
:term:`INRA`.

The erecord project is dedicated to the Record platform web development for
vle-1.1 models.

To access the erecord source code see : :ref:`erecord_software_project`.

The erecord directory is the root project.

Content hierarchy
=================

The erecord project *(erecord root directory)* contains :

  - erecord : the production package (source code...)
  - databases : the required databases,
  - repositories : the models repositories (recordb...),
  - docs : the documentation (some of it is automatically generated),
  - factory : workspace (buildings : VLE_HOME...)
  - etc. 

Content general description
===========================

erecord
-------

    The erecord subdirectory (python package) contains the software
    production : source code...

    For more see : :ref:`erecord_package`

databases
---------

    The software (erecord package) relies on information that is stored on the
    databases subdirectory. This information is dispatched into several
    databases.

    For more see : :ref:`databases`

repositories
------------

    The software (erecord package) relies on models that are physically
    installed on the repositories subdirectory, that contains the models
    repositories *(several, as many as wanted)*. 

    In order to be used, a model must have been :
      (i)  physically stored into a models repository of the repositories
           directory.
      (ii) recorded into the appropriate database of the databases directory.

    For more see : :ref:`repositories`

docs
----

    The docs subdirectory contains the erecord project documentation.

    The documentation is produced with Sphinx.

    For more see : :ref:`devel_documentation`

factory
-------

    The factory subdirectory is the workspace dedicated to the management of
    the information produced for and during processing.

    Before installation and running, it is empty.

    It is used for several purposes :

        - erecordenv : the python virtualenv
        - static
        - docs : generated online documentation
        - log
        - run
        - media

More
====

.. include:: ../../include/more.rst

