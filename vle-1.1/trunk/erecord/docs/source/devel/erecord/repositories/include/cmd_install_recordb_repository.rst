
Install the recordb repository
==============================

cd /opt/erecord/repositories

* to create recordb (/opt/erecord/repositories/recordb) as a 'vle_home'
  environment :

  mkdir recordb
  export VLE_HOME=/opt/erecord/repositories/recordb
  vle -R update
                                                                       result :
                                    -------------------------------------------
                                    /opt/erecord/repositories/recordb/vle.conf
                                                                     /pkgs-1.1
                                    -------------------------------------------

* Add the recordb distribution to vle.remote.url into the vle.conf file :

  Into /opt/erecord/repositories/recordb/vle.conf :
  vle.remote.url=http://www.vle-project.org/pub/1.1,http://recordb.toulouse.inra.fr/distributions/1.1
  
* To generate remote.pkg :

  vle -R update
                                                                       result :
                   ------------------------------------------------------------
                   /opt/erecord/repositories/recordb/.conf/pkgs-1.1/remote.pkg
                                                           /pkgs-1.1/local.pkg
                   ------------------------------------------------------------

* To know/define packages_list, the list of the existing erecordb repository
  remote packages :

  (the command 'grep Package' applied to pkgs-1.1/remote.pkg helps)

  grep Package /opt/erecord/repositories/recordb/pkgs-1.1/remote.pkg

* to install packages of packages_list :

  Command 'vle -R install pkg'
  for pkg in packages_list (for all of them or else select some of them) :

                                                                       result :
           --------------------------------------------------------------------
           /opt/erecord/repositories/recordb/vle.conf
                                            /pkgs-1.1/remote.pkg,local.pkg
                                            /pkgs-1.1/pkg/data,exp,plugins,lib
           --------------------------------------------------------------------

  vle -R install ext.muparser
  vle -R install vle.output
  vle -R install vle.extension.decision
  vle -R install vle.forrester
  vle -R install vle.extension.fsa
  vle -R install vle.extension.petrinet
  vle -R install vle.extension.difference-equation
  vle -R install vle.extension.differential-equation
  vle -R install vle.extension.cellqss
  vle -R install vle.extension.celldevs
  vle -R install vle.extension.dsdevs
  vle -R install vle.examples
  vle -R install tester
  vle -R install meteo
  vle -R install wwdm
  vle -R install 2CV
  vle -R install ext.Eigen
  vle -R install DEtimeStep
  vle -R install gluePhysic
  vle -R install LotkaVolterra
  vle -R install DateTime
  vle -R install RinsideGVLE
  vle -R install WACSgen
  vle -R install Multiformalism
  vle -R install glue
  vle -R install CallForOptim
  vle -R install GenCSVcan

  vle -R install tp5_2
  vle -R install tp4_1
  vle -R install tp5_4
  vle -R install tp2_4
  vle -R install tp5_1
  vle -R install tp3_2
  vle -R install tp3_2_correction
  vle -R install tp2_3_correction
  vle -R install tp5_4_correction
  vle -R install tp5_2_correction
  vle -R install tp5_1_correction
  vle -R install tp2_4_correction
  vle -R install tp4_1_correction
  vle -R install tpForrester
  vle -R install tpForrester_correction

  vle -R install GenGIScan

