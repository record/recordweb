==================================
vle-1.1 and pyvle-1.1 installation
==================================

vle-1.1.3 and pyvle-1.1.3
Installation on Debian Jessie (production server).

Install the dependencies in Debian Jessie case
==============================================

* vle dependencies :
sudo aptitude install cmake g++ libgtkmm-2.4-dev libglademm-2.4-dev \
   libgtksourceview2.0-dev \
   libboost-dev libboost-serialization-dev libboost-date-time-dev \
   libboost-filesystem-dev  \
   libboost-test-dev  libboost-regex-dev libboost-program-options-dev \
   libboost-thread-dev \
   libboost-chrono-dev libarchive-dev libasio-dev libopenmpi-dev openmpi-bin

* some more dependencies for pyvle :
sudo aptitude install libboost-python-dev

Install vle-1.1.3
=================

* get the source code :
git clone https://github.com/vle-forge/vle.git
cd vle
git checkout -b v1.1.3 v1.1.3

* build and install :
mkdir build
cd build
cmake -DWITH_GTKSOURCEVIEW=OFF -DWITH_GTK=ON -DWITH_CAIRO=ON -DWITH_MPI=OFF \
      -DBUILD_SHARED_LIBS=ON -DCMAKE_BUILD_TYPE=RelWithDebInfo              \
      -DCMAKE_INSTALL_PREFIX=/usr ..
make
sudo make install

Install pyvle-1.1.3
===================

* get the source code :
git clone git://github.com/vle-forge/pyvle.git
cd pyvle
git checkout -b v1.1.3 v1.1.3

* build and install into erecordenv virtualenv :
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/opt/erecord/factory/erecordenv -DCMAKE_BUILD_TYPE=RelWithDebInfo ..
make
sudo make install

==========================================================================
==================================  OLD ==================================
==========================================================================

==================================
vle-1.1 and pyvle-1.1 installation
==================================

vle-1.1.2 and pyvle-1.1 master branch (more recent than pyvle-1.1.2).
Installation on Debian Wheezy (production server).
Installation on Ubuntu 14.04 (development environement).

Install the dependencies in Debian Wheezy case
==============================================

* vle dependencies :
sudo aptitude install cmake g++ libgtkmm-2.4-dev libglademm-2.4-dev \
   libgtksourceview2.0-dev \
   libboost-dev libboost-serialization-dev libboost-date-time-dev \
   libboost-filesystem-dev  \
   libboost-test-dev  libboost-regex-dev libboost-program-options-dev \
   libboost-thread-dev \
   libboost-chrono-dev libarchive-dev libasio-dev libopenmpi-dev openmpi-bin

* some more dependencies for pyvle :
sudo aptitude install libboost-python-dev

Install the dependencies in Ubuntu 14.04 case
=============================================

* vle dependencies :
sudo apt-get install cmake g++ libgtkmm-2.4-dev libglademm-2.4-dev \
    libgtksourceview2.0-dev \
    libboost1.55-dev libboost-serialization1.55-dev \
    libboost-date-time1.55-dev libboost-filesystem1.55-dev \
    libboost-test1.55-dev libboost-regex1.55-dev \
    libboost-program-options1.55-dev libboost-thread1.55-dev \
    libboost-chrono1.55-dev libarchive-dev

* some more dependencies for pyvle :
sudo apt-get install libboost-python1.55.0
sudo apt-get install libboost-python1.55-dev

Install vle-1.1.2
=================

* get the source code :
git clone https://github.com/vle-forge/vle.git
cd vle
git checkout -b v1.1.2 v1.1.2

* build and install :
mkdir build
cd build
cmake -DWITH_GTKSOURCEVIEW=OFF -DWITH_GTK=ON -DWITH_CAIRO=ON -DWITH_MPI=OFF \
      -DBUILD_SHARED_LIBS=ON -DCMAKE_BUILD_TYPE=RelWithDebInfo              \
      -DCMAKE_INSTALL_PREFIX=/usr ..
make
sudo make install

Install pyvle-1.1 master branch (more recent than pyvle-1.1.2)
==============================================================

* get the source code :
git clone git://github.com/vle-forge/pyvle.git
cd pyvle
git checkout -b v1.1 origin/master1.1
   (git checkout -b v1.1.2 v1.1.2)

* build and install into erecordenv virtualenv :
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=/opt/erecord/factory/erecordenv -DCMAKE_BUILD_TYPE=RelWithDebInfo ..
make
sudo make install

