
* /etc/apache2/apache2.conf file modifications :

Timeout 600
(to be adapted)

KeepAlive Off
(not KeepAlive On)

* /etc/apache2/mods-available/mpm_prefork.conf modifications :

ServerLimit 150
(same value as MaxRequestWorkers)

MaxConnectionsPerChild 1
(not MaxConnectionsPerChild 0)

