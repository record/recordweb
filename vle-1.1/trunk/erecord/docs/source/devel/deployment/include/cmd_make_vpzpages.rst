
-------------------------------------------------------------------------------
First solution (into erecord/docs) :

The generation must be done somewhere else than on the machine where erecord
webservices run (generation not available in deployment situation) :

Under erecord_docs_path, cd erecord/docs

* clean :
make cleanvpzpages

* generate token :
  for a user (username, password) who is authorized for every simulator 
  in limited access, generate a token :

make username=... password=... token_value

* generate pages :
make genvpzpages

* The resulting documentation is produced under erecord_docs_path,
  under erecord/factory/docs/vpzpages (gen and private)

=> In deployment case, copy this generated erecord/factory/docs/vpzpages/gen
   as /opt/erecord/factory/docs/vpzpages/gen

-------------------------------------------------------------------------------
Second solution (into erecord/docs/vpzpages/from_anywhere) :

The generation must be done somewhere else than on the machine where erecord
webservices run (generation not available in deployment situation) :

Under erecord_docs_path, cd erecord/docs/vpzpages/from_anywhere
or from a copy of from_anywhere somewhere else :

* clean :
source clean.sh

* generate :
follow the instructions of README.txt file (erecord/docs/vpzpages/from_anywhere/README.txt)

* The resulting documentation is produced
  under from_anywhere/gen_vpzpages and from_anywhere/private

=> In deployment case, copy this generated from_anywhere/gen_vpzpages
   as /opt/erecord/factory/docs/vpzpages/gen

-------------------------------------------------------------------------------

