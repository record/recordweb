
* /opt/erecord/erecord/apps/erecord_cmn/configs/config.py file modifications :

ONLINEDOC_URL = 'http://erecord.toulouse.inra.fr/docs.html'
ONLINEDOC_WEBAPI_URL = 'http://erecord.toulouse.inra.fr/erecord/html/webapi/index.html'
ONLINEDOC_USECASE_URL = 'http://erecord.toulouse.inra.fr/erecord/html/using/examples/index.html'
ONLINEDOC_FAQ_URL = 'http://erecord.toulouse.inra.fr/erecord/html/faqs/index.html'

* /opt/erecord/docs/source/devel/design/webapi/include/online_url.rst and
  /opt/erecord/docs/source/devel/design/webapi/include/online_*.rst files modifications :

Put the appropriate url value : http://erecord.toulouse.inra.fr:8000 

    (commands, if '127.0.0.1' has to be replaced by 'erecord.toulouse.inra.fr' into online_*.rst files :
     cd /opt/erecord/docs/source/devel/design/webapi/include ;
     sed -i 's/127.0.0.1/erecord.toulouse.inra.fr/g' online_url*.rst ; )

