erecordenv virtualenv activation
================================

* in bash or sh case :
source /opt/erecord/factory/erecordenv/bin/activate

* in csh or tcsh case :
source /opt/erecord/factory/erecordenv/bin/activate.csh

* just to control the activation on the erecordenv virtualenv :
which python

