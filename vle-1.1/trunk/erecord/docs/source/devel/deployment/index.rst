.. _devel_deployment:

==========
Deployment
==========

Introduction
============

The erecord project software, once developed, can be deployed to a production
server in order to be used by web users.

This implies a deployment environment, in addition to the tools and libraries
already used for development.

Source software
---------------

The source software includes :

    - The erecord project software

    - Some models software, stored as elements of models repositories installed
      into the 'repositories' subdirectory of the erecord project.

Once the installation finished, the resulting hierarchy is such as :

    .. literalinclude:: include/hierarchy_source.rst

Development environment
-----------------------

The erecord project software is developed with :

    - python language (python 2.7 version),
    - django framework,
    - some django applications, like for example django-rest-framework. 
    - SQLite for databases.
    - Sphinx for documentation.

    See the requirement.txt file :

    .. literalinclude:: ../../../../erecord/install/requirement.txt

The stored models software :

    - have previously been developed with vle (vle-1.1 version), a C++ platform.
    - are interfaced with erecord software by pyvle (pyvle-1.1 version).

Deployment environment
----------------------

The software is deployed on a virtual machine with **Debian 8** (*Jessie*),
**Apache2.4** server and **mod_wsgi**.

    *Django’s primary deployment platform is WSGI, the Python standard for web
    servers and applications. mod_wsgi is an Apache module which can host any
    Python WSGI application, including Django.*

Installation procedure in 'from scratch' case
=============================================

    See :ref:`devel_deployment_fromscratch_case`.

Running instructions
====================

    See :ref:`deploy_run_server`.

Installation procedure in updating case
=======================================

    See :ref:`devel_deployment_updating_case`.

