.. _devel_deployment_updating_case:

=======================================
Installation procedure in updating case
=======================================

1. Development environment updating
===================================

See the concerned part in :ref:`Development environment installation <deploy_development_environment_install>`.

2. erecord project software updating
====================================

If the **source code** or **documentation** code of the erecord project
software has changed, then the corresponding directories are to be updated
under :ref:`deploy_root_path`.

The concerned directories are 'erecord/erecord' and 'erecord/docs'.

    .. literalinclude:: include/cmd_update_erecord.rst

    .. note::
       Be careful not to lose some **models repositories** by overwriting
       'erecord/repositories' under :ref:`deploy_root_path`.

       Be careful not to inadvertently overwrite the **databases** in
       'erecord/databases' under :ref:`deploy_root_path`.

Then apply on the erecord project software the appropriate modifications and
generations :

    See :ref:`deploy_erecord_modify_and_generate`.

3. Models software updating
===========================

Updating an existing model, adding a model...

See :ref:`deploy_repositories`.

