.. _deploy_repositories:

=========================================
Installation guide of models repositories
=========================================

Introduction
============

Once the erecord project software has been initially installed (from scratch)
under :ref:`erecord_path <deploy_erecord_path>`, the :ref:`erecord_repositories_path <deploy_erecord_repositories_path>` is empty.

Then at any time, some models repositories *(as many as wanted)* can be
installed into
:ref:`erecord_repositories_path <deploy_erecord_repositories_path>`. In order
to be operational, their content has to be built (compilation...).

At any time, some models *(as many as wanted)* can be
installed into an existing models repository of 
:ref:`erecord_repositories_path <deploy_erecord_repositories_path>`.

In order to be accessible to web users, the models repositories (and their
models and their simulators) will have to be :

    - installed under
      :ref:`erecord_repositories_path <deploy_erecord_repositories_path>`
      (for more, see :ref:`repositories`).
    - recorded into the appropriate database (for more, see :ref:`databases`). 

*For more details*, see :ref:`repositories`.

Example relying on the vle repository
=====================================

The creation is done under
:ref:`erecord_repositories_path <deploy_erecord_repositories_path>`.

See :ref:`cmd_install_vle_repository`

Example relying on the recordb repository
=========================================

The creation is done under
:ref:`erecord_repositories_path <deploy_erecord_repositories_path>`.

See :ref:`cmd_install_recordb_repository`

