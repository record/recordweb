.. _devel_deployment_fromscratch_case:

=============================================
Installation procedure in 'from scratch' case
=============================================

Prerequisites
=============

Some basic tools, installed once and for all : python 2.7, sudo, virtualenv...

    .. literalinclude:: include/cmd_setup.rst


1. Deployment environment installation
======================================

The deployment environment installation is done once and for all.

    See :ref:`deploy_deployment_environment_install`.

.. _deploy_beginning_of_erecord_project_software_install:

2. Beginning of erecord project software installation
=====================================================

The erecord project software installation begins with the code copying. The
following operations will be done later on (see below).

Software copy
+++++++++++++

    The whole erecord project software is copied as
    :ref:`deploy_erecord_path`.

    .. literalinclude:: include/cmd_copy_erecord.rst

    .. note::
       'factory' and 'repositories' under :ref:`deploy_erecord_path` (ie
       '/opt/erecord/factory' and '/opt/erecord/repositories') are empty at
       this stage.
    
       *'/opt/erecord/factory' will contain things once they have been
       generated later on (see the rest of the 'erecord project software
       installation').*
    
       *'/opt/erecord/repositories' will contain some models repositories once
       they have been installed later on.*

3. Development environment installation
=======================================

Prerequisites :

    The :ref:`deploy_beginning_of_erecord_project_software_install` must have
    been done before.

Installation :

    See :ref:`deploy_development_environment_install`.

4. Rest of the erecord project software installation
====================================================

Now that the erecord project software has been copied (see
:ref:`Beginning of erecord project software installation
<deploy_beginning_of_erecord_project_software_install>`), this consists
in doing the appropriate modifications and generations.

    See :ref:`deploy_erecord_modify_and_generate`.

5. Models software installation
===============================

See :ref:`deploy_repositories`.

