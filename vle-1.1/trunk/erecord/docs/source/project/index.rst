.. _erecord_software_project:

========================
erecord software project
========================

Intellectual property
---------------------

   See : :ref:`erecord_ip`
   (:ref:`AUTHORS` | :ref:`LICENSE` | :ref:`citing_erecord`)

Source code repository
----------------------

    The erecord **source code repository** is hosted on the recordweb
    project of the Mulcyber software forge
    https://mulcyber.toulouse.inra.fr/projects/recordweb, where its path is
    **vle-1.1/trunk** under Subversion (*see 'Dépôt de sources'*).

.. _erecord_contacts:

Contacts
--------

    You are invited to use the erecord tracking tools :

        The erecord tracking tools are those of the recordweb project of the
        Mulcyber software forge, under
        https://mulcyber.toulouse.inra.fr/projects/recordweb :

        - ' **Bug Report** ' to notice a bug.
        - ' **Feature Request** ' to ask for new functions.

    You may find answers to your own questions in :ref:`faqs`.

    You can send mail to Nathalie Rousse (INRA, MIAT, RECORD Team Member)
    at nathalie.rousse@inra.fr.

More
====

.. include:: ../include/more.rst

