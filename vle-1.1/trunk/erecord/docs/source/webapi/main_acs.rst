.. _webapi_acs:

=============
Web API 'acs'
=============

Overview
========
.. include:: ../devel/design/webapi/include/acs_intro.rst

Resources
=========

.. include:: ../devel/design/webapi/acs_resources.rst

More
====

.. include:: ../include/more.rst

