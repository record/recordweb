.. _webapi_db:

============
Web API 'db'
============

Overview
========
.. include:: ../devel/design/webapi/include/db_intro.rst

Resources
=========

.. include:: ../devel/design/webapi/db_resources.rst

More
====

.. include:: ../include/more.rst

