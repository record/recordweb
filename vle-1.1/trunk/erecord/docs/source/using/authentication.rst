.. _erecord_authentication:

=============================
JSON Web Token authentication
=============================

Overview
========

Authentication is required for the **models** that are in **limited access**. Authentication is managed into erecord by JWT (**JSON Web Token**). See :ref:`more information <webapi_feat_access>`.

If you want to use a model that is in limited access, you have to identify yourself.

In practice
===========

To send a request about a simulator that is in limited access (:ref:`get_vpz_input`, :ref:`post_vpz_output`, :ref:`post_vpz_report`, :ref:`get_slm_download`...), you must identify yourself by giving an available JWT value as a :ref:`webapi_opt_jwt` parameter of the request.

So you must previously have acquired an available JWT value : :ref:`howto_get_jwt`.

A JWT value is available only for a limited duration so some days after having acquired it, you will have to ask for a new one.

Illustration
============

**In this fictional example** :

    The user (*username="alphateam"*, *password="ahpla2017"*) has access authorization for some simulators that are in limited access, among which there is the simulator whose Id is *999*.

Get an available JWT value
--------------------------

    .. include:: examples/get_jwt.rst

**As a result** :

    An available JWT value, corresponding with user *"alphateam"*, is :

    eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Ind3ZG10ZWFtIiwidXNlcl9pZCI6MiwiZW1haWwiOiIiLCJleHAiOjE0OTg4NjY2NjV9.kQhtSlfY0NWIXnig7p5J-wdq0_p81m5FwQc7gLze6vE

    This JWT value is available for all the simulators that are authorized for user *"alphateam"*, in particular for the simulator whose Id is *999*.

Use JWT value to access the simulator in limited access
-----------------------------------------------------

With the JWT value previously got, one can send requests about the simulator whose Id is *999*, in order to access its parameters values and/or simulation results.

In the following example, the sent requests are :

    1) Request :ref:`get_vpz_input` to see parameters values.

    2) Request :ref:`post_vpz_output` to simulate and get simulation results.

    3) Request :ref:`post_vpz_report` whose report result (containing
       *parameters values and simulation results*) will be downloaded later
       on (in 4.) (see :ref:`option <webapi_opt_todownload>` mode=todownload).

    4) Request :ref:`get_slm_download` to download the file report previously
       built (in 3.).

    *-> Example (1. 2. 3.)* :
    :ref:`In command line with cURL <erecord_use_capture_191>`

    *-> Example (4.)* :
    :ref:`In a webbrowser <erecord_use_capture_092>`

See also 
========

    :ref:`webapi_feat_access` | :ref:`howto_get_jwt`


