.. _wwdm_run:

=====================
Running the simulator
=====================

*The erecord web services used in this part are some of the ‘vpz’ web
services* (see :ref:`webapi_vpz`).

Running a simulator consists in getting its output information resulting from
the simulation (see :ref:`dm_vpzoutput`) maybe after having modified the input
information (see :ref:`dm_vpzinput`).
 
As a reminder, the Id of the simulator ‘wwdm.vpz’ is :ref:`wwdm_vpz_id`
(:ref:`more <wwdm_vpz_id>`).

.. toctree::
   :maxdepth: 2

   wwdm_runasis
   wwdm_runmodif

More
====

*Back to :* :ref:`erecord_examples`

