# PHP code :

# content of simulation results (res) in 'tree' style of presentation,
# according to plan values ('single', 'linear') and
# restype values ('dataframe' or 'matrix')
function content_simulation_results_tree($res, $plan, $restype){
    #
    print ("plan, restype :".$plan.' '.$restype."<br />");
    print ("res :".$res."<br />");
    print ("\nDetailing the results :"."<br />");
    $res = json_decode($res, TRUE);

    if ($restype=='dataframe' and $plan=='single'){
        print ("(view name, output data name, value)"."<br />");
        foreach ($res as $viewname=>$outputs){
            foreach ($outputs as $outputname=>$val){
                print ("- ".$viewname.' '.$outputname.' '.$val);
            }
        }
    }else if ($restype=='dataframe' and $plan=='linear'){
        foreach ($res as $a => $res_a){
            print ("*** simulation number ".$a." :"."<br />");
            print ("(view name, output data name, value)"."<br />");
            foreach ($res_a as $viewname=>$outputs){
                foreach ($outputs as $outputname=>$val){
                    $val = json_encode($val);
                    print_r ("- ".$viewname.' '.$outputname.' '.$val."<br />");
                }
            }
        }
    }else if ($restype=='matrix' and $plan=='single'){
        print ("(view name, value)");
        foreach ($res as $viewname=>$v){
            print ("- ".$viewname.' '.$v."<br />");
        }
    }else if ($restype=='matrix' and $plan=='linear'){
        foreach ($res as $a => $res_a){
            print ("*** simulation number ".$a." :"."<br />");
            print ("(view name, value)"."<br />");
            foreach ($res_a as $viewname=>$v){
                $v = json_encode($v);
                print ("- ".$viewname.' '.$v."<br />");
            }
        }
    }else{ # error (unexpected)
        ;
    }
}

