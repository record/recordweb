# PHP code :

#######################
# request and response
#######################

$inputdata = ["vpz"=>266, "mode"=>"tree", "format"=>"json"];
$inputdata["duration"] = 6;
$inputdata["begin"] = 2453982.0;
$inputdata["parselect"] = "all";

# some parameters modification with 'pars'
$parameter_A = ["selection_name"=>"cond_wwdm.A", "cname"=>"cond_wwdm", "pname"=>"A", "value"=>0.0064];
$parameter_Eb = ["selection_name"=>"cond_wwdm.Eb", "cname"=>"cond_wwdm", "pname"=>"Eb", "value"=>1.86];

$pars = array($parameter_A, $parameter_Eb);
$inputdata["pars"] = $pars;

$responsedata = send_post_and_receive($url="http://erecord.toulouse.inra.fr:8000/vpz/input/", $inputdata=$inputdata);


#######################################################
# responsedata in case of 'tree' style of presentation
#######################################################

# id as VpzInput
if (array_key_exists('id', $responsedata)){
    $id = $responsedata['id'];
    content_simulation_inputs_tree($vpzinput=$responsedata);
}

