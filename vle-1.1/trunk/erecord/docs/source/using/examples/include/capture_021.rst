.. _erecord_use_capture_021:

=================================================
Example of :ref:`post_vpz_output` in a webbrowser
=================================================

    .. include:: wwdm_wb_intro_postoutput.rst

Example illustrating :

  - modifying **duration**,
  - value '**single**' as plan
  - value '**dataframe**' as restype

  - value of type *viewname* as '**outselect**' :

    value 'view' to select all output datas of the view named 'view'.

  - with 'application/x-www-form-**urlencoded**' as 'Media type'

    *Memo*

    .. literalinclude:: memo_request_021.rst

