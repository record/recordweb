# R code :

library('rjson')

# content of simulation results (res) in 'compact' style of presentation,
# according to plan values ('single', 'linear') and
# restype values ('dataframe' or 'matrix')
content_simulation_results_compact <- function(res, plan, restype){

    res = fromJSON(res)
    cat( "plan, restype :", plan, restype, "\n")
    #cat( "res :", "\n")
    #print(res)
    cat("\nDetailing the results :", "\n")
    if (restype=='dataframe' && plan=='single'){
        cat("(Output data name (ie selection_name) and value)", "\n")
        for (compact_outputname in names(res)){
            val = res[[compact_outputname]]
            cat("\n- ", compact_outputname, "\n")
            print(val)
        }
    } else if (restype=='dataframe' && plan=='linear'){
        for (res_a in res){
            cat("\n*** simulation :", "\n")
            cat("(Output data name (ie selection_name) and value)", "\n")
            for (compact_outputname in names(res_a)){
                val = res_a[[compact_outputname]]
                cat("\n- ", compact_outputname, "\n")
                print(val)
            }
        }
    } else if (restype=='matrix' && plan=='single'){
        cat("(View name (ie selection_name) and value)", "\n")
        for (viewname in names(res)){
            v = res[[viewname]]
            cat("\n- ", viewname, "\n")
            print(v)
        }
    } else if (restype=='matrix' && plan=='linear'){
        for (res_a in res){
            cat("\n*** simulation :", "\n")
            cat("(View name (ie selection_name) and value)", "\n")
            for (viewname in names(res_a)){
                v = res_a[[viewname]]
                cat("\n- ", viewname, "\n")
                print(v)
            }
        }
    } else {
        cat("error (unexpected)", "\n")
    }
}

