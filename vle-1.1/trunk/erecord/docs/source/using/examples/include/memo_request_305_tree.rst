# R code :

# content of the models repository (vlerep) in 'tree' style of presentation
content_rep_tree <- function(vlerep){
    #print(vlerep)
    vlerep_id= vlerep$id
    vlerep_name= vlerep$name
    vlerep_verbose_name= vlerep$verbose_name
    vlerep_path = vlerep$path
    cat("\nModels repository (id, name, verbose_name) : ", vlerep_id, vlerep_name, vlerep_verbose_name, "\n")
    cat("List of its models (id, name, verbose_name) :", "\n")
    vlerep_vlepkg_list = vlerep$vlepkg_list
    for (vlepkg in vlerep_vlepkg_list){
        #print(vlepkg)
        vlepkg_id = vlepkg$id
        vlepkg_name = vlepkg$name
        vlepkg_verbose_name = vlepkg$verbose_name
        vlepkg_vlerep = vlepkg$vlerep
        cat("- ", vlepkg_id, vlepkg_name, vlepkg_verbose_name, "\n")
    }
}

# content of the model (vlepkg) in 'tree' style of presentation
content_pkg_tree <- function(vlepkg){
    #print(vlepkg)
    vlepkg_id = vlepkg$id
    vlepkg_name = vlepkg$name
    vlepkg_verbose_name = vlepkg$verbose_name
    vlepkg_vlerep = vlepkg$vlerep
    cat("\nModel (id, name, verbose_name, name of vlerep, id of vlerep) : ", vlepkg_id, vlepkg_name, vlepkg_verbose_name, vlepkg_vlerep$name, vlepkg_vlerep$id, "\n")
    cat("List of its simulators (id, name, verbose_name, name of vlepkg, id of vlepkg) :", "\n")
    vlepkg_vlevpz_list = vlepkg$vlevpz_list
    for (vlevpz in vlepkg_vlevpz_list){
        #print(vlevpz)
        vlevpz_id = vlevpz$id
        vlevpz_name = vlevpz$name
        vlevpz_verbose_name = vlevpz$verbose_name
        vlevpz_vlepkg = vlevpz$vlepkg
        cat("- ", vlevpz_id, vlevpz_name, vlevpz_verbose_name, vlevpz_vlepkg$name, vlevpz_vlepkg$id, "\n")
    }
}

# content of the simulator (vlevpz) in 'tree' style of presentation
content_vpz_tree <- function(vlevpz){
    #print(vlevpz)
    vlevpz_id = vlevpz$id
    vlevpz_name = vlevpz$name
    vlevpz_verbose_name = vlevpz$verbose_name
    vlevpz_vlepkg = vlevpz$vlepkg
    cat("\nSimulator (id, name, verbose_name, name of vlepkg, id of vlepkg) : ", vlevpz_id, vlevpz_name, vlevpz_verbose_name, vlevpz_vlepkg$name, vlevpz_vlepkg$id, "\n")
}

