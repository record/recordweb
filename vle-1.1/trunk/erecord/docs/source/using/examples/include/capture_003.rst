.. _erecord_use_capture_003:

================================================
Example of :ref:`post_vpz_input` in a webbrowser
================================================

    .. include:: wwdm_wb_intro_postinput.rst

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',
  - with 'application/**json**' as 'Media type'

:download:`REQUEST <capture_request_003.png>`

    *Memo*

    .. literalinclude:: memo_request_003.rst

