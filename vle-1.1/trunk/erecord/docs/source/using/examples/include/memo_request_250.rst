# Python code :

from __future__ import print_function # python 2.7 version case

##############################################
# All the models repositories in 'tree' style
##############################################
url = "http://erecord.toulouse.inra.fr:8000/db/rep/"
options = {'mode':'tree'}
vlerep_list = send_get_and_receive(url=url,options=options)
for vlerep in vlerep_list :
    content_rep_tree(vlerep)

pass
################################################################################
# Among all the models repositories, looking for the model with name model_name
################################################################################
url = "http://erecord.toulouse.inra.fr:8000/db/rep/"
options = {'mode':'tree'}
vlerep_list = send_get_and_receive(url=url,options=options)
for model_name in ["2CV", "wwdm"]:
    for vlerep in vlerep_list :
        vlerep_vlepkg_list = vlerep['vlepkg_list']
        for vlepkg in vlerep_vlepkg_list :
            vlepkg_id = vlepkg['id']
            vlepkg_name = vlepkg['name']
            if (vlepkg_name == model_name) :
            #################################################################
            # New request for the model with name model_name in 'tree' style
            #################################################################
                print("\n\nMore about the model with name ", model_name)
                url = "http://erecord.toulouse.inra.fr:8000/db/pkg/"
                options = {'mode':'tree'}
                vlepkg = send_get_and_receive(url=url,id=vlepkg_id, options=options)
                content_pkg_tree(vlepkg)

pass
#####################################################################
# All the simulators of the model with id=19 ("2CV") in 'tree' style
#####################################################################
vlepkg_id = 19
url = "http://erecord.toulouse.inra.fr:8000/db/vpz/"
options = {'mode':'tree', 'pkg':vlepkg_id}
vlevpz_list = send_get_and_receive(url=url,options=options)
print("\n\nSimulators of the model with id=", vlepkg_id, " :")
for vlevpz in vlevpz_list :
    content_vpz_tree(vlevpz)

pass
##################################
# domino requests in 'link' style
##################################
# (models repositories => theirs models => their simulators)
url = "http://erecord.toulouse.inra.fr:8000/db/rep/"
options = {'mode':'link'}
vlerep_list = send_get_and_receive(url=url,options=options)
for vlerep in vlerep_list :
    vlerep_id = vlerep['id']
    vlerep_name = vlerep['name']
    vlerep_verbose_name = vlerep['verbose_name']
    vlerep_path = vlerep['path']
    vlepkg_url_list = vlerep['vlepkg_list']
    print("=> models repository ", vlerep_id, vlerep_name, vlerep_verbose_name, vlerep_path, vlepkg_url_list)
    for vlepkg_url in vlepkg_url_list :
        vlepkg = send_get_and_receive(url=vlepkg_url,options=options)
        vlepkg_id = vlepkg['id']
        vlepkg_name = vlepkg['name']
        vlepkg_verbose_name = vlepkg['verbose_name']
        vlerep_url = vlepkg['vlerep']
        vlevpz_url_list = vlepkg['vlevpz_list']
        print("   => model ",  vlepkg_id, vlepkg_name, vlepkg_verbose_name, vlerep_url, vlevpz_url_list)
        for vlevpz_url in vlevpz_url_list :
            vlevpz = send_get_and_receive(url=vlevpz_url,options=options)
            vlevpz_id = vlevpz['id']
            vlevpz_name = vlevpz['name']
            vlevpz_verbose_name = vlevpz['verbose_name']
            vlevpz_vlepkg_url = vlevpz['vlepkg']
            print("      => simulator ", vlevpz_id, vlevpz_name, vlevpz_verbose_name, vlevpz_vlepkg_url)

pass

