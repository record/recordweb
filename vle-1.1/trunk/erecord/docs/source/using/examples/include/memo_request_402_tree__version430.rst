# PHP code :

# content of simulation input information in 'tree' style of presentation
# (vpzinput)
function content_simulation_inputs_tree($vpzinput){
    #
    # duration
    if (array_key_exists('vleduration', $vpzinput)){
        $vleduration = $vpzinput['vleduration'];
        $duration_value = $vleduration['value'];
        #duration_verbose_name = $vleduration['verbose_name'];
        #$duration_id = $vleduration['id'];
        print ("duration value : ".$duration_value."<br />");
    }
    #
    # begin
    if (array_key_exists('vlebegin', $vpzinput)){
        $vlebegin = $vpzinput['vlebegin'];
        $begin_value = $vlebegin['value'];
        #$begin_verbose_name = $vlebegin['verbose_name'];
        #$begin_id = $vlebegin['id'];
        print ("begin value : ".$begin_value."<br />");
    }
    #
    # conditions and parameters
    if (array_key_exists('vlecond_list', $vpzinput)){
        $conds = $vpzinput['vlecond_list'];
        foreach ($conds as $cond){
            $cond_verbose_name = $cond['verbose_name'];
            $cond_id = $cond['id'];
            $cond_name = $cond['name'];
            print ("\nCondition name ".$cond_name."<br />");
            print ("List of its parameters (id, cname, pname, type, value, selected, verbose_name) :"."<br />");
            $pars = $cond['vlepar_list'];
            foreach ($pars as $par){
                $par_verbose_name = $par['verbose_name'];
                $par_id = $par['id'];
                $par_cname = $par['cname'];
                $par_pname = $par['pname'];
                $par_type = $par['type'];
                $par_value = $par['value'];
                $par_selected = $par['selected'];
                print ("- ".$par_id." ".$par_cname." ".$par_pname." ".$par_type." ".$par_value." ".$par_selected." ".$par_verbose_name."<br />");
            }
        }
    }
    #
    # views and output datas identity
    if (array_key_exists('vleview_list', $vpzinput)){
        $views = $vpzinput['vleview_list'];
        foreach ($views as $view){
            $view_verbose_name = $view['verbose_name'];
            $view_id = $view['id'];
            $view_name = $view['name'];
            $view_type = $view['type'];
            $view_timestep = $view['timestep'];
            $view_output_format = $view['output_format'];
            $view_output_location = $view['output_location'];
            $view_output_name = $view['output_name'];
            $view_output_plugin = $view['output_plugin'];
            print ("\nView name ".$view_name."<br />");
            print ("List of its output datas (id, vname, oname, shortname, selected, verbose_name) :<br />");
            $outs = $view['vleout_list'];
            foreach ($outs as $out){
                $out_verbose_name = $out['verbose_name'];
                $out_id = $out['id'];
                $out_vname = $out['vname'];
                $out_oname = $out['oname'];
                $out_shortname = $out['shortname'];
                $out_selected = $out['selected'];
                print ("- ".$out_id." ".$out_vname." ".$out_oname." ".$out_shortname." ".$out_selected." ".$out_verbose_name."<br />");
            }
        }
    }
}

