.. _erecord_use_capture_031:

================================================
Example of :ref:`post_vpz_input` in a webbrowser
================================================

    .. include:: wwdm_wb_intro_postinput.rst

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - '**compactlist**' as style of presentation,

  - values of type *cname.pname* as '**parselect**' :

    value 'cond_wwdm.A' to select the parameter named 'A' of the condition
    named 'cond_wwdm'.

    value 'cond_wwdm.B' to select the parameter named 'B' of the condition
    named 'cond_wwdm'.

    value 'cond_wwdm.Eb' to select the parameter named 'Eb' of the condition
    named 'cond_wwdm'.

    value 'cond_wwdm.TI' to select the parameter named 'TI' of the condition
    named 'cond_wwdm'.

  - with 'application/x-www-form-**urlencoded**' as 'Media type'

:download:`REQUEST <capture_request_031.png>` |
:download:`RESPONSE <capture_response_031.png>`

    *Memo*

    .. literalinclude:: memo_request_031.rst

