
-> *Example* : 
:ref:`In a webbrowser <erecord_use_capture_050>` |
:ref:`In command line with cURL <erecord_use_capture_150>` |
:ref:`In Python language <erecord_use_capture_250>` |
:ref:`In R language <erecord_use_capture_350>` |
:ref:`In PHP language <erecord_use_capture_450>` |
:ref:`In C++ language <erecord_use_capture_550>`

