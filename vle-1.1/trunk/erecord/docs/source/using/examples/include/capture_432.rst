.. _erecord_use_capture_432:

=================================================
Example of :ref:`post_vpz_output` in PHP language
=================================================

    .. include:: wwdm_php_intro.rst

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters with '**pars**',

  - '**tree**' as style of presentation,
  - '**json**' as format,

  - value '**single**' as plan, or
    value '**linear**' in case of  multiple simulation
  - value '**dataframe**' as restype *(or 'matrix')*

  - value '**all**' as '**outselect**',

  - with 'application/**json**' as 'Content-Type'

    *Memo*

    .. literalinclude:: memo_request_400.rst
    .. literalinclude:: memo_request_401_tree__version432.rst
    .. literalinclude:: memo_request_432.rst


.. NB: memo_request_401_tree.rst replaced (temporary) by xxx__version432.rst

