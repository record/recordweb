
-------------------------------------------------------------------------------
* Case :
- wwdm_data.zip as datafolder
- replace datafoldercopy
- cond_meteo.meteo_file="31035002_ter.csv"
- format yaml

* cURL command line :
curl -F 'vpz=266' -F 'datafolder=@wwdm_data.zip' -F 'datafoldercopy=replace' -F 'cond_meteo.meteo_file="31035002_ter.csv"' -F 'duration=6' -F 'begin=2453982.0' -F 'cond_wwdm.A=0.0064' -F 'cond_wwdm.Eb=1.86' -F 'parselect=cond_meteo.meteo_file' -F 'parselect=cond_wwdm.A' -F 'parselect=cond_wwdm.B' -F 'parselect=cond_wwdm.Eb' -F 'parselect=cond_wwdm.TI' -F 'mode=tree' -F 'mode=single' -F 'mode=dataframe' -F 'outselect=view.top:wwdm.LAI' -F 'outselect=view.top:wwdm.ST' -F 'format=yaml' http://erecord.toulouse.inra.fr:8000/vpz/inout/

* Comments :

The modified input datas folder : data/31035002_bis.csv
                                      /31035002_ter.csv

-------------------------------------------------------------------------------

