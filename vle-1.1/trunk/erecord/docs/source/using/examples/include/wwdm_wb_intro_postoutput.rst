
For the simulator 'wwdm.vpz' whose Id is  :ref:`wwdm_vpz_id` [2]_, enter in a
webbrowser the URL [1]_ and click 'POST' button after having entered your
commands in the POST space.

.. [1] The URL is http://erecord.toulouse.inra.fr:8000/vpz/output.
.. [2] The simulator Id is specified in the POST space by 'vpz'.

