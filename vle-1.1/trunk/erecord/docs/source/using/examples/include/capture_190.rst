.. _erecord_use_capture_190:

===============================================================
Example of :ref:`post_acs_jwt_obtain` in command line with cURL
===============================================================

    Enter the cURL commands in a command line window.

Example illustrating how to get a JWT value with a :ref:`post_acs_jwt_obtain` request.

    *Memo*

    .. literalinclude:: memo_request_190.rst

