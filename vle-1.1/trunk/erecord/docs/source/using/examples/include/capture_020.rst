.. _erecord_use_capture_020:

=================================================
Example of :ref:`post_vpz_output` in a webbrowser
=================================================

    .. include:: wwdm_wb_intro_postoutput.rst

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',
  - value '**all**' as '**outselect**',
    to select all output datas of all views.
  - with 'application/x-www-form-**urlencoded**' as 'Media type'

:download:`REQUEST <capture_request_020.png>`

    *Memo*

    .. literalinclude:: memo_request_020.rst

