.. _erecord_use_capture_036:

================================================
Example of :ref:`post_vpz_input` in a webbrowser
================================================

    .. include:: wwdm_wb_intro_postinput.rst

Example illustrating :

  - '**compact**' as style of presentation,

  - value '**all**' as '**parselect**'
    (to receive information of all the existing parameters and conditions)

  - value '**all**' as '**outselect**'
    (to receive information of all the existing output datas and views)

  - with 'application/**json**' as 'Media type'

:download:`REQUEST <capture_request_036.png>`

    *Memo*

    .. literalinclude:: memo_request_036.rst

