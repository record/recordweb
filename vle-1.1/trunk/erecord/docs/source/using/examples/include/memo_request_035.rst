URL :
http://erecord.toulouse.inra.fr:8000/vpz/output/

Content (case of single plan (single simulation)) :
vpz=266&duration=6&begin=2453982.0&cond_wwdm.A=0.0064&cond_wwdm.Eb=1.86&mode=compact&mode=single&mode=dataframe&outselect=view.top:wwdm.LAI&outselect=view.top:wwdm.ST

Content (case of linear plan (multiple simulation)) :
vpz=266&duration=6&begin=2453982.0&cond_wwdm.A=[0.0064,0.0065,0.0066]&cond_wwdm.Eb=[1.84,1.85,1.86]&mode=compact&mode=linear&mode=dataframe&outselect=view.top:wwdm.LAI&outselect=view.top:wwdm.ST

(rq : or with matrix instead of dataframe)

