.. _erecord_use_capture_092:

=================================================
Example of authenticated requests in a webbrowser
=================================================

Example illustrating requests about a simulator in limited access, where an available JWT value is required, as a :ref:`webapi_opt_jwt` parameter of the requests.

    - Request :ref:`get_slm_download` (with 'key' and 'jwt' parameters) to download a file built from a simulator in limited access.

      Enter in a webbrowser the URL :

      http://erecord.toulouse.inra.fr:8000/slm/download/?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Ind3ZG10ZWFtIiwidXNlcl9pZCI6MiwiZW1haWwiOiIiLCJleHAiOjE0OTg4NjY2NjV9.kQhtSlfY0NWIXnig7p5J-wdq0_p81m5FwQc7gLze6vE&key=20170630_141912_038a167c-a5b7-4064-a7c0-949c276cf92a

    *Memo*

    .. literalinclude:: memo_request_092.rst

