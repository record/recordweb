# PHP code :

# content of the models repository (vlerep) in 'tree' style of presentation
function content_rep_tree($vlerep){
    $vlerep_id= $vlerep['id'];
    $vlerep_name= $vlerep['name'];
    $vlerep_verbose_name= $vlerep['verbose_name'];
    $vlerep_path = $vlerep['path'];
    print ("<br />Models repository (id, name, verbose_name) : ".$vlerep_id." ".$vlerep_name." ".$vlerep_verbose_name);
    print ("<br />List of its models (id, name, verbose_name) :");
    $vlerep_vlepkg_list = $vlerep['vlepkg_list'];
    foreach ($vlerep_vlepkg_list as $vlepkg){
        $vlepkg_id = $vlepkg['id'];
        $vlepkg_name = $vlepkg['name'];
        $vlepkg_verbose_name = $vlepkg['verbose_name'];
        $vlepkg_vlerep = $vlepkg['vlerep'];
        print ("<br />- ".$vlepkg_id." ".$vlepkg_name." ".$vlepkg_verbose_name);
    }
    print ("<br />");
}

# content of the model (vlepkg) in 'tree' style of presentation
function content_pkg_tree($vlepkg){
    $vlepkg_id = $vlepkg['id'];
    $vlepkg_name = $vlepkg['name'];
    $vlepkg_verbose_name = $vlepkg['verbose_name'];
    $vlepkg_vlerep = json_encode($vlepkg['vlerep'], JSON_UNESCAPED_SLASHES);
    print ("<br />Model (id, name, verbose_name, vlerep) : ".$vlepkg_id." ".$vlepkg_name." ".$vlepkg_verbose_name." ".$vlepkg_vlerep);
    print ("<br />List of its simulators (id, name, verbose_name, vlepkg) :");
    $vlepkg_vlevpz_list = $vlepkg['vlevpz_list'];
    foreach ($vlepkg_vlevpz_list as $vlevpz){
        $vlevpz_id = $vlevpz['id'];
        $vlevpz_name = $vlevpz['name'];
        $vlevpz_verbose_name = $vlevpz['verbose_name'];
        $vlevpz_vlepkg = json_encode($vlevpz['vlepkg'], JSON_UNESCAPED_SLASHES);
        print_r ("<br />- ".$vlevpz_id." ".$vlevpz_name." ".$vlevpz_verbose_name." ".$vlevpz_vlepkg);
    }
    print ("<br />");
}

# content of the simulator (vlevpz) in 'tree' style of presentation
function content_vpz_tree($vlevpz){
    $vlevpz_id = $vlevpz['id'];
    $vlevpz_name = $vlevpz['name'];
    $vlevpz_verbose_name = $vlevpz['verbose_name'];
    $vlevpz_vlepkg = json_encode($vlevpz['vlepkg'], JSON_UNESCAPED_SLASHES);
    print_r ("<br />Simulator (id, name, verbose_name, vlepkg) : ".$vlevpz_id." ".$vlevpz_name." ".$vlevpz_verbose_name." ".$vlevpz_vlepkg);
    print ("<br />");
}

##############################################
# All the models repositories in 'tree' style
##############################################

$url = "http://erecord.toulouse.inra.fr:8000/db/rep/";
$options = ['mode'=>'tree'];
$vlerep_list = send_get_and_receive($url=$url,$options=$options);
foreach ($vlerep_list as $vlerep){
    content_rep_tree($vlerep);
}
print ("<br />");

################################################################################
# Among all the models repositories, looking for the model with name model_name
################################################################################
$url = "http://erecord.toulouse.inra.fr:8000/db/rep/";
$options = ['mode'=>'tree'];
$vlerep_list = send_get_and_receive($url=$url,$options=$options);
foreach (["2CV", "wwdm"] as $model_name){
    foreach ($vlerep_list as $vlerep){
        $vlerep_vlepkg_list = $vlerep['vlepkg_list'];
        foreach ($vlerep_vlepkg_list as $vlepkg){
            $vlepkg_id = $vlepkg['id'];
            $vlepkg_name = $vlepkg['name'];
            if ($vlepkg_name == $model_name){
            #################################################################
            # New request for the model with name model_name in 'tree' style
            #################################################################
                print ("<br /><br />More about the model with name ".$model_name);
                $url = "http://erecord.toulouse.inra.fr:8000/db/pkg/";
                $options = ['mode'=>'tree'];
                $vlepkg = send_get_and_receive($url=$url, $options=$options, $id=$vlepkg_id);
                content_pkg_tree($vlepkg);
            }
        }
    }
}
print ("<br />");

#####################################################################
# All the simulators of the model with id=19 ("2CV") in 'tree' style
#####################################################################
$vlepkg_id = 19;
$url = "http://erecord.toulouse.inra.fr:8000/db/vpz/";
$options = ['mode'=>'tree', 'pkg'=>$vlepkg_id];
$vlevpz_list = send_get_and_receive($url=$url,$options=$options);
print ("<br /><br />Simulators of the model with id=".$vlepkg_id." :");
foreach ($vlevpz_list as $vlevpz){
    content_vpz_tree($vlevpz);
}

##################################
# domino requests in 'link' style
##################################
# (models repositories => theirs models => their simulators)
$url = "http://erecord.toulouse.inra.fr:8000/db/rep/";
$options = ['mode'=>'link'];
$vlerep_list = send_get_and_receive($url=$url,$options=$options);
foreach ($vlerep_list as $vlerep){
    $vlerep_id = $vlerep['id'];
    $vlerep_name = $vlerep['name'];
    $vlerep_verbose_name = $vlerep['verbose_name'];
    $vlerep_path = $vlerep['path'];
    $vlepkg_url_list = $vlerep['vlepkg_list'];
    print ("<br />=> models repository ".$vlerep_id." ".$vlerep_name." ".$vlerep_verbose_name." ".$vlerep_path." ".json_encode($vlepkg_url_list, JSON_UNESCAPED_SLASHES));
    foreach ($vlepkg_url_list as $vlepkg_url){
        $vlepkg = send_get_and_receive($url=$vlepkg_url,$options=$options);
        $vlepkg_id = $vlepkg['id'];
        $vlepkg_name = $vlepkg['name'];
        $vlepkg_verbose_name = $vlepkg['verbose_name'];
        $vlerep_url = $vlepkg['vlerep'];
        $vlevpz_url_list = $vlepkg['vlevpz_list'];
        print (". . . => model ".$vlepkg_id." ".$vlepkg_name." ".$vlepkg_verbose_name." ".$vlerep_url." ".json_encode($vlevpz_url_list, JSON_UNESCAPED_SLASHES));
        foreach ($vlevpz_url_list as $vlevpz_url){
            $vlevpz = send_get_and_receive($url=$vlevpz_url,$options=$options);
            $vlevpz_id = $vlevpz['id'];
            $vlevpz_name = $vlevpz['name'];
            $vlevpz_verbose_name = $vlevpz['verbose_name'];
            $vlevpz_vlepkg_url = $vlevpz['vlepkg'];
            print (". . . => simulator ".$vlevpz_id." ".$vlevpz_name." ".$vlevpz_verbose_name." ".$vlevpz_vlepkg_url);
        }
    }
}

