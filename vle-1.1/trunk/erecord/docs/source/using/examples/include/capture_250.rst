.. _erecord_use_capture_250:

====================================================
Example of some :ref:`'db' resources <webapi_db>` calls in Python language
====================================================

The following resources are directly used in this example or could be used in a similar way :
  - :ref:`get_db_rep`, :ref:`get_db_rep_id`
  - :ref:`get_db_pkg`, :ref:`get_db_pkg_id`
  - :ref:`get_db_vpz`, :ref:`get_db_vpz_id`

Enter the Python code/instructions in a Python interpreter.

    *Memo*

    .. literalinclude:: ../../../../../erecord/apps/erecord_cmn/utils/using/send_get_and_receive.py

    .. literalinclude:: ../../../../../erecord/apps/erecord_cmn/utils/using/content_rep_pkg_vpz_tree.py

    .. literalinclude:: memo_request_250.rst

