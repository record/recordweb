
-> *Example* :
:ref:`In a webbrowser <erecord_use_capture_034>` |
:ref:`In command line with cURL <erecord_use_capture_134>` |
:ref:`In Python language <erecord_use_capture_234>` |
:ref:`In R language <erecord_use_capture_334>` |
:ref:`In PHP language <erecord_use_capture_434>` |
:ref:`In C++ language <erecord_use_capture_534>`

