-------------------------------------------------------------------------------
* Case :
- wwdm_data.zip as datafolder
- overwrite datafoldercopy
- cond_meteo.meteo_file unchanged (cond_meteo.meteo_file="31035002.csv")

* cURL command line :
curl -F 'vpz=266' -F 'datafolder=@wwdm_data.zip' -F 'datafoldercopy=overwrite' -F 'duration=6' -F 'cond_wwdm.A=0.0064' -F 'cond_wwdm.Eb=1.86' -F 'mode=compact' -F 'plan=single' -F 'restype=dataframe' -F 'outselect=view.top:wwdm.LAI' -F 'outselect=view.top:wwdm.ST' -F 'format=yaml' http://erecord.toulouse.inra.fr:8000/vpz/output/

* Response :
res: '{"view.top:wwdm.LAI": [0.0, 0.0023647344518109726, 0.0053704330932816385, 0.009295027359586974,
  0.014491204594073003, 0.020623387827212118, 0.027858027831614042], "view.top:wwdm.ST":
  [0.0, 21.2, 43.75, 68.2, 94.80000000000001, 120.60000000000001, 145.85000000000002],
  "view.time": [2453980.0, 2453981.0, 2453982.0, 2453983.0, 2453984.0, 2453985.0,
  2453986.0]}'
plan: single
restype: dataframe

* Comments :

The modified input datas folder : data/31035002.csv
                                      /serieclim.dat
                                      /31035002_bis.csv
                                      /31035002_ter.csv

The simulation has found the required meteo file (31035002.csv) into the input datas folder.

-------------------------------------------------------------------------------
* Case :
- wwdm_data.zip as datafolder
- replace datafoldercopy
- cond_meteo.meteo_file unchanged (cond_meteo.meteo_file="31035002.csv")

* cURL command line :
curl -F 'vpz=266' -F 'datafolder=@wwdm_data.zip' -F 'datafoldercopy=replace' -F 'duration=6' -F 'cond_wwdm.A=0.0064' -F 'cond_wwdm.Eb=1.86' -F 'mode=compact' -F 'plan=single' -F 'restype=dataframe' -F 'outselect=view.top:wwdm.LAI' -F 'outselect=view.top:wwdm.ST' -F 'format=yaml' http://erecord.toulouse.inra.fr:8000/vpz/output/

* Response :
detail: "Unable to satisfy the request -- Simulation running error ... \n/!\\ vle\
  \ error reported: vle::utils::FileError\n[meteo] Meteo: invalid date 2006-Sep-01\
  \ 00:00:00 or invalid data"

* Comments :

The modified input datas folder : data/31035002_bis.csv
                                      /31035002_ter.csv

The simulation failed because it has not found the required meteo file (31035002.csv) into the input datas folder.

-------------------------------------------------------------------------------
* Case :
- wwdm_data.zip as datafolder
- replace datafoldercopy
- cond_meteo.meteo_file="31035002_ter.csv"

* cURL command line :
curl -F 'vpz=266' -F 'datafolder=@wwdm_data.zip' -F 'datafoldercopy=replace' -F 'cond_meteo.meteo_file="31035002_ter.csv"' -F 'duration=6' -F 'cond_wwdm.A=0.0064' -F 'cond_wwdm.Eb=1.86' -F 'mode=compact' -F 'plan=single' -F 'restype=dataframe' -F 'outselect=view.top:wwdm.LAI' -F 'outselect=view.top:wwdm.ST' -F 'format=yaml' http://erecord.toulouse.inra.fr:8000/vpz/output/

* Response :
res: '{"view.top:wwdm.LAI": [0.0, 0.0023647344518109726, 0.0053704330932816385, 0.009295027359586974,
  0.014491204594073003, 0.020623387827212118, 0.027858027831614042], "view.top:wwdm.ST":
  [0.0, 21.2, 43.75, 68.2, 94.80000000000001, 120.60000000000001, 145.85000000000002],
  "view.time": [2453980.0, 2453981.0, 2453982.0, 2453983.0, 2453984.0, 2453985.0,
  2453986.0]}'
plan: single
restype: dataframe

* Comments :

The modified input datas folder : data/31035002_bis.csv
                                      /31035002_ter.csv

The simulation has found the required meteo file (31035002_ter.csv) into the input datas folder.

-------------------------------------------------------------------------------

