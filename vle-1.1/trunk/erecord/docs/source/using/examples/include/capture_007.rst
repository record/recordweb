.. _erecord_use_capture_007:

================================================
Example of :ref:`post_vpz_input` in a webbrowser
================================================

    .. include:: wwdm_wb_intro_postinput.rst

Example illustrating :

  - modifying some parameters by '**cname.pname**',
  - value '**all**' as '**parselect**',
    to select all parameters of all conditions.
  - with 'application/x-www-form-**urlencoded**' as 'Media type'

:download:`REQUEST <capture_request_007.png>`

    *Memo*

    .. literalinclude:: memo_request_007.rst

