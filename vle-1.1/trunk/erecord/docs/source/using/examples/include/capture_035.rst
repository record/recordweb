.. _erecord_use_capture_035:

=================================================
Example of :ref:`post_vpz_output` in a webbrowser
=================================================

    .. include:: wwdm_wb_intro_postoutput.rst

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - '**compact**' as style of presentation,

  - value '**single**' as plan, or
    value '**linear**' in case of  multiple simulation
  - value '**dataframe**' as restype *(or 'matrix')*

  - value of type *vname.oname* as '**outselect**' :

    value 'view.top:wwdm.LAI' to select the 'LAI' output data of the view
    named 'view'.

    value 'view.top:wwdm.ST' to select the 'ST' output data of the view
    named 'view'.

  - with 'application/x-www-form-**urlencoded**' as 'Media type'

:download:`REQUEST in case of single plan (single simulation) <capture_request_035.png>`

:download:`REQUEST in case of linear plan (multiple simulation) <capture_request2_035.png>`

    *Memo*

    .. literalinclude:: memo_request_035.rst

