# Python code :

#######################
# request and response
#######################

inputdata = {"vpz":266, "format":"json"}
inputdata["duration"] = 6
inputdata["begin"] = 2453982.0

inputdata["mode"] = ["tree", "single", "dataframe"]

# some parameters modification with 'cname.pname'
inputdata["cond_wwdm.A"] = 0.0064
inputdata["cond_wwdm.Eb"] = 1.86

inputdata["parselect"] = ["cond_wwdm.A", "cond_wwdm.B", "cond_wwdm.Eb",
                          "cond_wwdm.TI"]

inputdata["outselect"] = ["view.top:wwdm.LAI", "view.top:wwdm.ST"]

responsedata = send_post_and_receive(
    url="http://erecord.toulouse.inra.fr:8000/vpz/inout/",
    inputdata=inputdata)

responsedata
keys = responsedata.keys()
keys

#######################################################
# responsedata in case of 'tree' style of presentation 
#######################################################

# id as VpzAct
if "id" in keys :
    id = responsedata['id']

if "verbose_name" in keys :
    verbose_name = responsedata['verbose_name']

#----------------------
# input information of simulation

if "vpzinput" in keys :
    vpzinput = responsedata['vpzinput']
    vpzinput_keys = vpzinput.keys()
    #
    # id as VpzInput
    if "id" in vpzinput_keys :
        id = vpzinput['id']
    #
    content_simulation_inputs_tree(vpzinput=vpzinput)

pass

#----------------------
# output information of simulation

if "vpzoutput" in keys :
    vpzoutput = responsedata['vpzoutput']
    vpzoutput_keys = vpzoutput.keys()
    #
    # id as VpzOutput
    if "id" in vpzoutput_keys :
        id = vpzoutput['id']
    #
    if 'res' in vpzoutput_keys and 'plan' in vpzoutput_keys and 'restype' in vpzoutput_keys : 
        content_simulation_results_tree(res=vpzoutput['res'],
                                        plan=vpzoutput['plan'],
                                        restype=vpzoutput['restype'])

pass

#----------------------
# others

if "vpzorigin" in keys :
    vpzorigin = responsedata['vpzorigin']

if "pkgname" in keys :
    pkgname = responsedata['pkgname']

if "vlepath" in keys :
    vlepath = responsedata['vlepath']

if "vpzname" in keys :
    vpzname = responsedata['vpzname']

if "vpzworkspace" in keys :
    vpzworkspace = responsedata['vpzworkspace']

