.. _erecord_use_capture_131:

==========================================================
Example of :ref:`post_vpz_input` in command line with cURL
==========================================================

    .. include:: wwdm_curl_intro.rst

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - '**compactlist**' as style of presentation,

  - values of type *cname.pname* as '**parselect**' :

    value 'cond_wwdm.A' to select the parameter named 'A' of the condition
    named 'cond_wwdm'.

    value 'cond_wwdm.B' to select the parameter named 'B' of the condition
    named 'cond_wwdm'.

    value 'cond_wwdm.Eb' to select the parameter named 'Eb' of the condition
    named 'cond_wwdm'.

    value 'cond_wwdm.TI' to select the parameter named 'TI' of the condition
    named 'cond_wwdm'.

  - with 'application/**json**' as 'Content-Type'

:download:`REQUEST and RESPONSE<capture_request_131.png>`

    *Memo*

    .. literalinclude:: memo_request_131.rst

Some variants :

    .. literalinclude:: memo_request_131_2.rst

