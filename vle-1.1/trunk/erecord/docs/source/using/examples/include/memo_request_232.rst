# Python code :

#######################
# request and response
#######################

inputdata = {"vpz":266, "format":"json"}
inputdata["duration"] = 6
inputdata["begin"] = 2453982.0

#----------------------
# parameters and mode in case of single plan (single simulation)

# some parameters modification with 'pars'
parameter_A = {"selection_name":"cond_wwdm.A",
               "cname":"cond_wwdm","pname":"A",
               "value":0.0064}
parameter_Eb = {"selection_name":"cond_wwdm.Eb",
                "cname":"cond_wwdm","pname":"Eb",
                "value":1.86}
pars = list()
pars.append(parameter_A)
pars.append(parameter_Eb)
inputdata["pars"] = pars

inputdata["mode"]      = ["tree", "single", "dataframe"]
# or inputdata["mode"] = ["tree", "single", "matrix"]

#----------------------
# parameters and mode in case of linear plan (multiple simulation)

# some parameters modification with 'pars' (multiple values)
parameter_A = {"selection_name":"cond_wwdm.A",
               "cname":"cond_wwdm","pname":"A",
               "value":[0.0064,0.0065,0.0066]}
parameter_Eb = {"selection_name":"cond_wwdm.Eb",
                "cname":"cond_wwdm","pname":"Eb",
                "value":[1.84,1.85,1.86]}
pars = list()
pars.append(parameter_A)
pars.append(parameter_Eb)
inputdata["pars"] = pars

inputdata["mode"] = ["tree", "linear", "dataframe"]
# or inputdata["mode"] = ["tree", "linear", "matrix"]

#----------------------

inputdata["outselect"] = "all"

responsedata = send_post_and_receive(
    url="http://erecord.toulouse.inra.fr:8000/vpz/output/",
    inputdata=inputdata)

responsedata
keys = responsedata.keys()
keys

#######################################################
# responsedata in case of 'tree' style of presentation
#######################################################

# id as VpzOutput
if "id" in keys :
    id = responsedata['id']

if 'res' in keys and 'plan' in keys and 'restype' in keys : 
    content_simulation_results_tree( res=responsedata['res'],
                                     plan=responsedata['plan'],
                                     restype=responsedata['restype'])

pass

