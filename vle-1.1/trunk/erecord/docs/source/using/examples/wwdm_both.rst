.. _wwdm_both:

================================
Seeing and running the simulator
================================

*The erecord web services used in this part are some of the ‘vpz’ web
services* (see :ref:`webapi_vpz`).

Seeing and running a simulator consists in getting both its input information
(see :ref:`dm_vpzinput`) - maybe after having modified it - and its output
information resulting from the simulation (see :ref:`dm_vpzoutput`).
 
As a reminder, the Id of the simulator ‘wwdm.vpz’ is :ref:`wwdm_vpz_id`
(:ref:`more <wwdm_vpz_id>`).

.. toctree::
   :maxdepth: 2

   wwdm_bothasis
   wwdm_bothmodif

More
====

*Back to :* :ref:`erecord_examples`

