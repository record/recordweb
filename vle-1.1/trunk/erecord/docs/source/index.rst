.. _index:

.. _erecord_home:

=======
erecord
=======

The erecord project is dedicated to the :term:`Record` platform web 
development, concerning more specifically its models developed with the
vle-1.1 version of :term:`Vle`. The web services provided by erecord allow
to edit, modify and simulate some vle models, such as the Record platform
ones.

Quick overview
==============

:ref:`Access to erecord web services <online_url>` |
:ref:`webui` |
:ref:`webapi` |
:ref:`An example as a tutorial <erecord_examples>`

Project
=======

.. toctree::
   :maxdepth: 1

   General presentation <general_presentation/index>

.. toctree::
   :maxdepth: 2

   About erecord <project/index>

* :ref:`Focus on some features <features_erecord>`

Users
=====

.. toctree::
   :maxdepth: 1

   Web API <webapi/index>

.. toctree::
   :maxdepth: 1

   Web User Interface <webui/index>

.. toctree::
   :maxdepth: 2

   Users <using/index>

Developers
==========

.. toctree::
   :maxdepth: 2

   Developers <devel/developers/index>

How to
======

    * :ref:`How to call/use the web services <webapi>`

    * :ref:`How to install/deploy the erecord software <devel_deployment>`

    * :ref:`How to install/declare a model <repositories>`

    * :ref:`How to build documentation <devel_documentation>`

    * :ref:`Testing the erecord software <devel_test>`

FAQs
====

.. toctree::
   :maxdepth: 2

   faqs/index

Related projects
================

.. toctree::
   :maxdepth: 2

   related/index

More
====

.. include:: include/more.rst

