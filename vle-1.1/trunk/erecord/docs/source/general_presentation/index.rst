.. _general_presentation:

====================
General presentation
====================

.. toctree::
   :maxdepth: 1

   Domain area <../domain/index>
   Models repositories <../devel/erecord/repositories/index>
   Databases <../devel/erecord/databases/index>
   The software content <../devel/erecord/index>

The erecord web services are online :
:ref:`Access to the erecord web services <online_url>`.

Some presentations
------------------

* **An illustration of erecord web services :**

    `Illustration <http://147.100.164.34/docs/erecord/erecord.pdf>`_ *(fr)*

* **A general presentation of erecord web services :**

    `erecord web services, general presentation <http://147.100.164.34/docs/erecord/erecord_presentation_generale.pdf>`_

    *(fr) (Services web erecord, présentation générale)*.

* **Using a model through internet with erecord web services :**

    `Lesson : Using a model through the web <http://147.100.164.34/docs/erecord/cours_2.5.pdf>`_

    *(fr) (Cours : Utiliser un modèle à travers le web)*.

* **In details - the erecord web services for a model of the Record platform :**

    `Using an agronomic model developped with the Record platform and installed into the erecord web services <http://147.100.164.34/docs/erecord/model_user.pdf>`_

    *(fr) (Utiliser un modèle agronomique développé sous la plateforme Record et disponible dans les services web erecord)*.

* **The erecord web services while modelling into the Record platform :**

    `Modelling into the Record platform and the erecord web services <http://147.100.164.34/docs/erecord/model_owner.pdf>`_

    *(fr) (Modélisation sous la plateforme Record et services web erecord)*.

