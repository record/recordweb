===========================================================
erecord, record platform web development for vle-1.1 models
===========================================================

The erecord project is dedicated to the Record platform web development for
vle-1.1 models.

Content hierarchy : docs, repositories, erecord (the erecord package),
databases, factory...

See __init__.py files, docs directory.

