# -*- coding: utf-8 -*-
"""factory.erecord

Built information relative to erecord development

Contains some built information relative to erecord development. Information may be generated for example during installations. It could be useful to build the production space.

See :ref:`index`

"""
