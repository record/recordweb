# -*- coding: utf-8 -*-
"""erecord.projects.ws.ws.settings

Django settings for ws project.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.11/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

import sys

# project_home, apps_home (erecord project, apps) to add to pythonpath
project_home = os.path.normpath(os.path.join(BASE_DIR, "..", "..", ".."))
if project_home not in sys.path :
    sys.path.insert(0, project_home)
apps_home = os.path.normpath(os.path.join(BASE_DIR, "..", "..", "apps"))
if apps_home not in sys.path :
    sys.path.insert(0, apps_home)

#print "project_home : ", project_home, " , ", "apps_home : ", apps_home

from erecord_cmn.utils.logger import get_logger
LOGGER = get_logger(__name__)

LOGGER.debug(u"sys.path : %s (must contain %s)", sys.path, project_home)
LOGGER.debug(u"sys.path : %s (must contain %s)", sys.path, apps_home)

from erecord_cmn.configs.config import DB_NAME_DEFAULT
from erecord_cmn.configs.config import DB_PATH_DEFAULT

from erecord_cmn.configs.config import CLOSEDMEDIA_DESTINATION_HOME
from erecord_cmn.configs.config import MEDIA_DESTINATION_HOME
from erecord_cmn.configs.config import STATIC_DESTINATION_HOME
from erecord_cmn.configs.config import STATIC_APP_CMN
from erecord_cmn.configs.config import TEMPLATE_APP_CMN
from erecord_cmn.configs.config import TEMPLATE_APP_DB
from erecord_cmn.configs.config import TEMPLATE_APP_VPZ
from erecord_cmn.configs.config import TEMPLATE_APP_SLM
from erecord_cmn.configs.config import TOKEN_LIFETIME

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'neyq=+mtept6yji+ut7c6j(eu+^14ik$ni-ay)_m_@j(+)8qqb'

SITE_ID = 1 # SITE_ID is required by admindocs # a laisser en 1.11 ?

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True # True # False

#ALLOWED_HOSTS = []
ALLOWED_HOSTS = ['localhost', '127.0.0.1', '[::1]', 'erecord.toulouse.inra.fr']

# Application definition

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites', # a laisser en 1.11 ?
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'django.contrib.admin',  # to enable the admin
    'django.contrib.admindocs', # a laisser en 1.11 ?

    #'request', # django-request statistics module

    'rest_framework',  # web services
    'rest_framework_jwt', # djangorestframework-jwt

    'erecord_db',
    'erecord_vpz',
    'erecord_slm',
    'erecord_acs',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

REST_FRAMEWORK = {
    #'DEFAULT_PERMISSION_CLASSES': (
    #    'rest_framework.permissions.IsAuthenticated',
    #),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
    ),
}

# Token, JWT configuration
JWT_AUTH = {
    'JWT_EXPIRATION_DELTA': TOKEN_LIFETIME,
}

ROOT_URLCONF = 'ws.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',

        # Templates locations (old TEMPLATE_DIRS in 1.7.8)
        'DIRS': [ TEMPLATE_APP_DB, TEMPLATE_APP_SLM, TEMPLATE_APP_VPZ,
                  TEMPLATE_APP_CMN, # last
                ],

        'APP_DIRS': True,

        'OPTIONS': {

            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                #???'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                #???'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],

            'debug': True, # old TEMPLATE_DEBUG in 1.7.8
        },
    },
]

WSGI_APPLICATION = 'ws.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

#DATABASES = {
#    'default': {
#        'ENGINE': 'django.db.backends.sqlite3',
#        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#    }
#}
DATABASES = {
    DB_NAME_DEFAULT: {
        'ENGINE': 'django.db.backends.sqlite3', 'NAME': DB_PATH_DEFAULT,
        'OPTIONS': {
                       'timeout': 6000, # ms
                   },
    },
}

# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

#AUTH_PASSWORD_VALIDATORS = [
#    {
#    'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
#    },
#    {
#    'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
#    },
#    {
#    'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
#    },
#    {
#    'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
#    },
#]

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Closed media files,  whose access is controlled
# (used for downloadable (and later uploaded ?) files)

CLOSEDMEDIA_ROOT = os.path.join(CLOSEDMEDIA_DESTINATION_HOME, 'ws')
CLOSEDMEDIA_URL = ''

# Media files (for public files, empty for the moment)

MEDIA_ROOT = os.path.join(MEDIA_DESTINATION_HOME, 'ws')
MEDIA_URL = '/media/'

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_ROOT = os.path.join(STATIC_DESTINATION_HOME, 'ws')
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = [
#   '/var/www/static/',
    STATIC_APP_CMN, # last
]


# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

