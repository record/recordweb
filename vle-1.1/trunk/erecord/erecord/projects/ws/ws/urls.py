# -*- coding: utf-8 -*-
"""erecord.projects.ws.ws.urls

Urls of the ws project

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))

"""

from django.conf.urls import include, url

from django.conf import settings
from django.conf.urls.static import static

from django.views import static as django_views_static

from django.views.generic import TemplateView

from rest_framework_jwt.views import obtain_jwt_token
#from rest_framework_jwt.views import refresh_jwt_token
from rest_framework_jwt.views import verify_jwt_token

from erecord_cmn import views as cmn_views
from erecord_db.views import menus as db_views_menus

# to enable the admin:
from django.contrib import admin
admin.autodiscover()

# internationalisation, translations
urlpatterns = [
    url(r'^i18n/', include('django.conf.urls.i18n')),
]

urlpatterns += [

    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', admin.site.urls),
]

# JWT Authentication
# Those urls have not been removed whereas replaced by acs urls
# (see acs/jwt/obtain/, acs/jwt/verify/, acs/login)
urlpatterns += [
    url(r'^api-token-auth/', obtain_jwt_token, name="api-token-auth"),
    #url(r'^api-token-refresh/', refresh_jwt_token),
    url(r'^api-token-verify/', verify_jwt_token, name="api-token-verify"),
]
urlpatterns += [
    url(r'^api/login', obtain_jwt_token),
]

# index.html to be written ! root page :
#  - presentation,
#  - access to menu,
#  - html and web services aspects...
# index page (root)
urlpatterns += [
    url(r'^$', TemplateView.as_view(template_name='erecord_cmn/index.html'),
        name="erecord_cmn-index-page"),
]

# home page (web user interface : idem erecord_db-menu-main-page-list,erecord_db-menu-main-page-detail)
urlpatterns += [
    url(r'^home/$', db_views_menus.MenuMainPageList.as_view(),
        name='erecord-home-page'),
    url(r'^menu/$', db_views_menus.MenuMainPageList.as_view()),
    url(r'^menu/(?P<pk>[0-9]+)/$', db_views_menus.MenuMainPageDetail.as_view()),
]

# online documentation page
urlpatterns += [
    url(r'^docs/', cmn_views.onlinedoc_page, name="erecord_cmn-onlinedoc-page"),
]

# database (main)
urlpatterns += [
    url(r'^db/', include('erecord_db.urls')),
]

# vpz manipulation
urlpatterns += [
    url(r'^vpz/', include('erecord_vpz.urls')),
]

# slm downloadable and uploaded files management, and media

urlpatterns += [
    url(r'^slm/', include('erecord_slm.urls')),
]

urlpatterns += [
    url(r'^media/(?P<path>.*)$', django_views_static.serve,
     {'document_root':settings.MEDIA_ROOT}, name="media"),
]

# access (public, limited)
urlpatterns += [
    url(r'^acs/', include('erecord_acs.urls')),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

