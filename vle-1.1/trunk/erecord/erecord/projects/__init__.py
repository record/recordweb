"""erecord.projects

*This package is part of erecord - Record platform web development*

:copyright: Copyright (C) 2014-2015 INRA http://www.inra.fr.
:license: GPLv3, see LICENSE for more details.
:authors: see AUTHORS.

django projects of the erecord package

See also python code in django applications (erecord.apps)

See :ref:`index`
 
"""
