# -*- coding: utf-8 -*-
"""erecord_db.admin"""

from django.contrib import admin

from erecord_db.models import VleRep
from erecord_db.models import VlePkg
from erecord_db.models import VleVpz

from erecord_db.forms import VleRepForm
from erecord_db.forms import VlePkgForm
from erecord_db.forms import VleVpzForm

from erecord_acs.admin import VpzLockingVleVpzInline

from erecord_acs.models import VpzLock
from django.contrib.admin.helpers import ActionForm
from django import forms
from django.contrib import messages

# To show/hide some admin parts

from django.contrib.auth.models import User
from django.contrib.auth.models import Group
#admin.site.unregister(User)
#admin.site.unregister(Group)

group_general_description = \
        "This is my group description ...."
vlepkg_identification_description = \
        "'models repository' and/or 'name' should be consistent with the models repository directory path content : the vle package 'name' must be physically installed under the models repository path."
vlerep_general_description = \
        "Actions to generate -or update- into the database -or delete from the database- the models repository tree (its vle packages and their vpz files) are available in the 'Action' menu of the models repositories list page ('generate tree','update tree','delete tree'). Once a models repository installation modified (new version...) : (i) the models repository should be updated or (ii) the models repository tree should be generated after having deleted the previous one."
vlerep_identification_description = \
        "The models repository tree should be consistent with the models repository directory path content. Once 'path' modified : (i) the models repository should be updated or (ii) the models repository tree should be generated, after having deleted the previous one."

def delete_tree(modeladmin, request, queryset):
    for vlerep in queryset:
        vlerep.delete_tree()
delete_tree.short_description = "Delete tree (vle packages and vpz files)"

def generate_tree(modeladmin, request, queryset):
    for vlerep in queryset:
        vlerep.generate_tree()
generate_tree.short_description = "Generate tree (vle packages and vpz files)"

def update_tree(modeladmin, request, queryset):
    for vlerep in queryset:
        vlerep.update_tree()
update_tree.short_description = "Update tree (vle packages and vpz files)"

class VlePkgInline(admin.TabularInline):
    model = VlePkg

class VleRepAdmin(admin.ModelAdmin):
    form = VleRepForm
    list_display = ('id', 'name', 'verbose_name', 'path',)
    actions = [generate_tree, update_tree, delete_tree]
    inlines = [VlePkgInline]
    search_fields = ['name']

    fieldsets = (
        (None, {
            'description': vlerep_general_description,
            'fields': (),
        }),
        (None, {
            'fields': ('verbose_name',),
        }),
        ('Identification', {
            'description' : vlerep_identification_description,
            'fields': ('name', 'path',),
        }),
    )

admin.site.register(VleRep, VleRepAdmin)


vlepkg_general_description = \
        "The vle packages are automatically generated -updated,deleted- into the database with their models repository tree generation -updating,deletion- (see the 'generate tree' -'update tree','delete tree'- actions in the 'Action' menu of the models repositories list page). Manual operations (add, change) of vle packages should be carefully done and consistent with the models repositories physical installations."
vlepkg_identification_description = \
        "'models repository' and/or 'name' should be consistent with the models repository directory path content : the vle package 'name' must be physically installed under the models repository path."

class VleVpzInline(admin.TabularInline):
    model = VleVpz

class VlePkgAdmin(admin.ModelAdmin):
    form = VlePkgForm
    list_display = ('id', 'name', 'verbose_name', 'vlerep',)
    inlines = [VleVpzInline]
    list_filter = ['vlerep']
    search_fields = ['name']

    fieldsets = (
        (None, {
            'description': vlepkg_general_description,
            'fields': (),
        }),
        (None, {
            'fields': ('verbose_name',),
        }),
        ('Identification', {
            'description': vlepkg_identification_description,
            'fields': ('name', 'vlerep',),
        }),
    )

admin.site.register(VlePkg, VlePkgAdmin)

vlevpz_general_description = \
        "The vpz files are automatically generated -updated,deleted- into the database with the tree generation -updating,deletion- of their vle package models repository (see the 'generate tree' -'update tree','delete tree'- actions in the 'Action' menu of the models repositories list page). Manual operations (add, change) of vpz files should be carefully done and consistent with the models repositories physical installations."
vlevpz_identification_description = \
        "'vle package' and/or 'name' should be consistent with the models repository directory path content : the vpz file 'name' must be physically installed under the vle package (under the models repository path)."

error_help = "you have to select a 'Lock' before 'Go'"

# lock the selected vlevpz by the lock selected in choice list
def lock_by(modeladmin, request, queryset):
    lock_id = request.POST['lock']
    if lock_id == '' :
        messages.error(request,"Lock failed : " + error_help)
    else :
        lock_id = int(lock_id)
        lock = VpzLock.objects.get(pk=lock_id)
        for vlevpz in queryset:
            lock.lock(vlevpz)
lock_by.short_description = "Lock by ... choose the lock just there ->"

# unlock the selected vlevpz by the lock selected in choice list
def unlock_by(modeladmin, request, queryset):
    lock_id = request.POST['lock']
    if lock_id == '' :
        messages.error(request,"Unlock failed : " + error_help)
    else :
        lock_id = int(lock_id)
        lock = VpzLock.objects.get(pk=lock_id)
        for vlevpz in queryset:
            lock.unlock(vlevpz)
unlock_by.short_description = "Unlock by ... choose the lock just there ->"

# locks choice list
class LockActionForm(ActionForm):
    lock = forms.ModelChoiceField(queryset=VpzLock.objects.all(),
                                  required=False, help_text="Lock")

class VleVpzAdmin(admin.ModelAdmin):
    form = VleVpzForm
    list_display = ('id', 'name', 'verbose_name', 'vlepkg',) # 'get_vlerep')
    actions = [lock_by, unlock_by]
    action_form = LockActionForm
    inlines = [VpzLockingVleVpzInline]
    list_filter = ['vlepkg']
    search_fields = ['name']

    fieldsets = (
        (None, {
            'description': vlevpz_general_description,
            'fields': (),
        }),
        (None, {
            'fields': ('verbose_name',),
        }),
        ('Identification', {
            'description': vlevpz_identification_description,
            'fields': ('name', 'vlepkg',),
        }),

    )
admin.site.register(VleVpz, VleVpzAdmin)

