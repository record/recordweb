# -*- coding: utf-8 -*-
"""erecord_db.models_mixins

Mixins for models of the erecord_db application

"""

from django.db import models

from django.core.exceptions import ValidationError

from django import forms

import os
from erecord_cmn.utils.vle import get_rep_pkgname_list
from erecord_cmn.utils.vle import get_rep_pkg_exp_path
from erecord_cmn.utils.vle import get_rep_pkg_data_path

import datetime

# Some mixins for the erecord_db models

class VleRepMixin(object):
    """additional methods for VleRep """

    def read_and_add_tree(self) :
        """reads a VleRep tree (its vle packages and their vpz files) in its
        path and add the relevant VlePkg and VleVpz into the database """

        vlepkgname_list = get_rep_pkgname_list(self.path)
        for vlepkgname in vlepkgname_list :

            vlepkg = self.vlepkg_list.create(name=vlepkgname)
            vlepkg_exp_path = get_rep_pkg_exp_path(rep_path=self.path,
                    pkgname=vlepkgname )

            for root, dirs, files in os.walk( vlepkg_exp_path, topdown=False):
                for name in files:
                    vlevpzname = os.path.relpath( os.path.join(root, name),
                            vlepkg_exp_path )
                    if vlevpzname != "CMakeLists.txt" :
                        vlepkg.vlevpz_list.create(name=vlevpzname)

    def read_and_update_tree(self) :
        pass
        """reads a VleRep tree (its vle packages and their vpz files) in its
        path and add or remove the relevant VlePkg and VleVpz into the
        database """

        path_content = dict() # vlepkgname as key, list of vlevpzname as value
        vlepkgname_list = get_rep_pkgname_list(self.path)
        for vlepkgname in vlepkgname_list :
            path_content[vlepkgname] = list()
            vlepkg_exp_path = get_rep_pkg_exp_path(rep_path=self.path,
                    pkgname=vlepkgname )
            for root, dirs, files in os.walk( vlepkg_exp_path, topdown=False):
                for name in files:
                    vlevpzname = os.path.relpath( os.path.join(root, name),
                            vlepkg_exp_path )
                    if vlevpzname != "CMakeLists.txt" :
                        path_content[vlepkgname].append(vlevpzname)
        # delete into the database what is not (anymore) in path
        for vlepkg in self.vlepkg_list.all():
            if vlepkg.name not in path_content.keys():
                vlepkg.delete()
            else :
                for vlevpz in vlepkg.vlevpz_list.all():
                    if vlevpz.name not in path_content[vlepkg.name]:
                        vlevpz.delete()
        # add into the database what is new in path
        vlepkg_names = [vlepkg.name for vlepkg in self.vlepkg_list.all()]
        for vlepkgname in path_content.keys():
            if vlepkgname not in vlepkg_names: # create vlepkg and its vlevpz
                vlepkg = self.vlepkg_list.create(name=vlepkgname)
                for vlevpzname in path_content[vlepkgname]:
                    vlepkg.vlevpz_list.create(name=vlevpzname)
            else : # may be create some vlevpz
                for vlepkg in self.vlepkg_list.all().filter(name=vlepkgname):
                    for vlevpzname in path_content[vlepkgname]:
                        vlevpz_names = [vlevpz.name
                                        for vlevpz in vlepkg.vlevpz_list.all()]
                        if vlevpzname not in vlevpz_names:
                            vlepkg.vlevpz_list.create(name=vlevpzname)

class VlePkgMixin(object):
    """additional methods for VlePkg """

    def get_datafilename_list(self):
        """Defines and returns the name list of data folder files of VlePkg

        The data folder is the default one : 'data' subdirectory of the
        vle package.

        'CMakeLists.txt' is excluded.
        """

        data_path = get_rep_pkg_data_path(rep_path=self.vlerep.path,
                                          pkgname=self.name )
        tmp = forms.FilePathField(path=data_path, recursive=True,
            allow_files=True, allow_folders=False)
        data_list = [ os.path.relpath(v, data_path) for (v,c) in tmp.choices ]
        n = "CMakeLists.txt"
        if n in data_list:
            data_list.remove(n)
        return data_list
 
class VleVpzMixin(object):
    """additional methods for VleVpz """

    def for_vpzact_creation(self):
        """ Prepares the creation of a VpzAct from a VleVpz

        VpzAct is part of erecord_vpz application models 
        """

        now = datetime.datetime.now()
        res = None
        text = "Vpz activity  (Vpzact) created from VleVpz. "
        text = text+ "VleVpz id : " + str(self.id) + ", "
        text = text+ "verbose_name : " + self.verbose_name + ", "
        text = text+ "name : " + self.name + ". "
        text = text+ "VlePkg id : " + str(self.vlepkg.id) + ", "
        text = text+ "verbose_name : " + self.vlepkg.verbose_name + ", "
        text = text+ "name : " + self.vlepkg.name + ". "
        text = text+ "VleRep id : " + str(self.vlepkg.vlerep.id) + ", "
        text = text+ "verbose_name : " + self.vlepkg.vlerep.verbose_name + "."
        text = text+ "name : " + self.vlepkg.vlerep.name + ", "
        text = text+ "path : " + self.vlepkg.vlerep.path + ". "
        text = text+ "Time:" + str(now) + "."

        res = { 'vlepath': self.vlepkg.vlerep.path,
                'pkgname': self.vlepkg.name, 
                'vpzname': self.name,
                'textorigin': text, }
        return res

