# -*- coding: utf-8 -*-
"""erecord_db

*This package is part of erecord - Record platform web development*

:copyright: Copyright (C) 2014-2015 INRA http://www.inra.fr.
:license: GPLv3, see :ref:`LICENSE` for more details.
:authors: see :ref:`AUTHORS`.


Database application
 
The db application is dedicated to the main erecord database.

The erecord database contains models repositories and their tree (vle packages,
vpz files).

The vle models into the database : @include doc/vlemodels_in_database.txt

A models repository is a self independant and consistent group of some vle
packages.

'vle package' is the vle term for a model (also called 'project' in vle).
Each vle package has some vpz files.

'vpz file' is the vle term for a simulator, or simulation scenario.

The erecord development would allow to manipulate vpz files of vle packages
of models repositories (edit, modify, simulate).

See :ref:`index`

"""


