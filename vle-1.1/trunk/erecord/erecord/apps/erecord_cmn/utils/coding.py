# -*- coding: utf-8 -*-
"""erecord_cmn.utils.coding

Coding methods

"""

def byteify(input):
    """unicode to utf-8

    Convert any decoded JSON object from using unicode strings to
    UTF-8-encoded byte strings

    """
    
    if isinstance(input, dict):
        return {byteify(key):byteify(value) for key,value in input.iteritems()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

