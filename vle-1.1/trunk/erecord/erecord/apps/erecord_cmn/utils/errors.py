# -*- coding: UTF-8 -*-
"""erecord_cmn.utils.errors

Utils to manage errors

"""

import traceback

from rest_framework import status
from django.http import Http404

def logger_report_error(LOGGER) :
    """ Reports an error to log """
    LOGGER.error(u"Error report :\n %s ", traceback.format_exc())

def build_error_message(error=None, msg=None) :
    """builds a message error 
    
    The message is composed by msg (optional) and error message(s) (optional) 
    """
    txt = ""
    if msg is not None:
        txt = msg + " -- "
    if error is not None:
        if hasattr(error, 'message'):
            txt = txt + str(error.message)
        if hasattr(error, 'messages'):
            txt = txt + "; ".join(error.messages)
        if hasattr(error, 'status_code'):
            txt = txt + "; " + str( error.status_code)
        if hasattr(error, 'detail'):
            txt = txt + "; " + error.detail
    return txt

class Http400(Exception):
    pass
class Http403(Exception):
    pass
class Http401(Exception):
    pass

def get_error_status(e) :
    if hasattr(e, 'status_code'):
        return e.status_code
    else :
        s = status.HTTP_500_INTERNAL_SERVER_ERROR # default
        if isinstance(e, Http404):
            s = status.HTTP_404_NOT_FOUND
        elif isinstance(e, Http400):
            s = status.HTTP_400_BAD_REQUEST
        elif isinstance(e, Http403):
            s = status.HTTP_403_FORBIDDEN
        elif isinstance(e, Http401):
            s = status.HTTP_401_UNAUTHORIZED
        else :
            s = status.HTTP_500_INTERNAL_SERVER_ERROR
        return s

