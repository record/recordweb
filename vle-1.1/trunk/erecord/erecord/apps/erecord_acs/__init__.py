# -*- coding: utf-8 -*-
"""erecord_acs

*This package is part of erecord - Record platform web development*

:copyright: Copyright (C) 2014-2017 INRA http://www.inra.fr.
:license: GPLv3, see :ref:`LICENSE` for more details.
:authors: see :ref:`AUTHORS`.

Access control application
 
The acs application is dedicated to access control management, based on
JWT (JSON Web Token).

See :ref:`index`

"""

