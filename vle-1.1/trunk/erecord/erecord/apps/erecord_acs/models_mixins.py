# -*- coding: utf-8 -*-
"""erecord_acs.models_mixins

Mixins for models able to be locked (ie linked to some Lock)

"""

from django.contrib.auth.models import User
from erecord_cmn.utils.errors import Http403


class LockableMixin(object):
    """methods for models able to be locked (ie linked to some Lock)"""

    def get_authorized_users(self):
        """creates and returns the list of model authorized users """

        users = []
        for lock in self.lock_list.all() :
            users.extend([user for user in lock.user_list.all()])
        users = list(set(users))
        return users

    def get_authorized_users_ids(self):
        """creates and returns the list of model authorized users ids """

        users_ids = []
        for lock in self.lock_list.all() :
            users_ids.extend([user.id for user in lock.user_list.all()])
        users_ids = list(set(users_ids))
        return users_ids

    def has_lock(self):
        """defines if model has lock or not """

        from erecord_acs.models import Lock
        has_lock = False # default
        for lock in self.lock_list.all() :
            if isinstance(lock, Lock) :
                has_lock = True
        return has_lock

    def get_access_value(self):
        """defines and returns model access value ('limited' or 'public') """

        if self.has_lock() :
            access = "limited"
        else :
            access = "public"
        return access

    def is_public(self):
        return (self.get_access_value() == "public")

    def is_limited(self):
        return (self.get_access_value() == "limited")

    def control_access_user(self, user_id, for_message):
        """controls if model is accessible regarding user_id

        returns True if model is accessible
        and else sends an exception containing for_message.

        model is accessible if it has no lock or \
        if it is locked and user_id is authorized for it.

        only first level control
        """

        if self.has_lock():
            if user_id not in self.get_authorized_users_ids() :
                errormsg = "Error while limited access : unauthorized user "
                errormsg = errormsg + "(id "+str(user_id)+") " + for_message
                raise Http403(errormsg)
        return True

class LockableVpzActMixin(LockableMixin):
    """additional methods for VpzAct as able to be locked

    able to be locked : ie linked to some Lock
    """

    def propagate_control_access_user(self, user_id):
        """controls if the parts of vpzact are accessible regarding user_id

        returns True if all the parts are accessible
        and else sends an exception.

        vpzact must be locked and accessible by user_id
        """

        if hasattr(self, "vpzinput"):
            try :
                txt=" for vpz input information (id "+str(self.vpzinput.id)+")"
                self.vpzinput.control_access_user(user_id=user_id,
                                                  for_message=txt)
                self.vpzinput.propagate_control_access_user(user_id=user_id)
            except :
                raise

        if hasattr(self, "vpzoutput"):
            try :
                txt=" for vpz output information (id "+str(self.vpzoutput.id)+")"
                self.vpzoutput.control_access_user(user_id=user_id,
                                                     for_message=txt)
            except :
                raise

        if hasattr(self, "vpzorigin"):
            try :
                txt=" for vpz origin information (id "+str(self.vpzorigin.id)+")"
                self.vpzorigin.control_access_user(user_id=user_id,
                                                     for_message=txt)
            except :
                raise

        if hasattr(self, "vpzworkspace"):
            try :
                txt=" for vpz workspace information (id "+str(self.vpzworkspace.id)+")"
                self.vpzworkspace.control_access_user(user_id=user_id,
                                                     for_message=txt)
            except :
                raise

        return True


class LockableVpzInputMixin(LockableMixin):
    """additional methods for VpzInput as able to be locked

    able to be locked : ie linked to some Lock
    """


    def propagate_control_access_user(self, user_id):
        """controls if the parts of vpzinput are accessible regarding user_id

        returns True if all the parts are accessible
        and else sends an exception.

        vpzinput must be locked and accessible by user_id
        """

        if hasattr(self, "vlebegin"):
            try :
                txt=" for vle begin information (id "+str(self.vlebegin.id)+")"
                self.vlebegin.control_access_user(user_id=user_id,
                                                  for_message=txt)
            except :
                raise

        if hasattr(self, "vleduration"):
            try :
                txt=" for vle duration information (id "+str(self.vleduration.id)+")"
                self.vleduration.control_access_user(user_id=user_id,
                                                     for_message=txt)
            except :
                raise

        if hasattr(self, "vlecond_list"):
            for vlecond in self.vlecond_list.all() :
                try :
                    txt=" for vle condition information (id "+str(vlecond.id)+")"
                    vlecond.control_access_user(user_id=user_id,
                                                for_message=txt)
                    vlecond.propagate_control_access_user(user_id=user_id)
                except :
                    raise


        if hasattr(self, "vleview_list"):
            for vleview in self.vleview_list.all() :
                try :
                    txt=" for vle view information (id "+str(vleview.id)+")"
                    vleview.control_access_user(user_id=user_id,
                                                for_message=txt)
                    vleview.propagate_control_access_user(user_id=user_id)
                except :
                    raise
        return True


class LockableVleCondMixin(LockableMixin):
    """additional methods for VleCond as able to be locked (ie linked to some Lock)"""

    def propagate_control_access_user(self, user_id):
        """controls if the parts of vlecond are accessible regarding user_id

        returns True if all the parts are accessible
        and else sends an exception.

        vlecond must be locked and accessible by user_id
        """

        for vlepar in self.vlepar_list.all() :
            try :
                txt=" for vle parameter information (id "+str(vlepar.id)+")"
                vlepar.control_access_user(user_id=user_id, for_message=txt)
            except :
                raise
        return True

class LockableVleViewMixin(LockableMixin):
    """additional methods for VleView as able to be locked (ie linked to some Lock)"""

    def propagate_control_access_user(self, user_id):
        """controls if the parts of vleview are accessible regarding user_id

        returns True if all the parts are accessible
        and else sends an exception.

        vleview must be locked and accessible by user_id
        """

        for vleout in self.vleout_list.all() :
            try :
                txt=" for vle out information (id "+str(vleout.id)+")"
                vleout.control_access_user(user_id=user_id, for_message=txt)
            except :
                raise
        return True

