# -*- coding: utf-8 -*-
"""erecord_slm.admin"""

from django.contrib import admin

from erecord_slm.models import UploadVpzDocument
from erecord_slm.models import DownloadVpzDocument

from erecord_acs.admin import LoadLockingUploadVpzDocumentInline
from erecord_acs.admin import LoadLockingDownloadVpzDocumentInline

# To show/hide some admin parts

from django.contrib.auth.models import User
from django.contrib.auth.models import Group
#admin.site.unregister(User)
#admin.site.unregister(Group)


uploadvpzdocument_general_description = \
        "UploadVpzDocument."

def delete_too_old_uploadvpzdocument(modeladmin, request, queryset):
    for uploadvpzdocument in queryset:
        uploadvpzdocument.delete_if_old()
delete_too_old_uploadvpzdocument.short_description =\
        "Delete selected uploadvpzdocuments if too old"

class UploadVpzDocumentAdmin(admin.ModelAdmin):
    list_display = ('id', 'key', )
    actions = [delete_too_old_uploadvpzdocument]
    inlines = [LoadLockingUploadVpzDocumentInline]
    fieldsets = (
        (None, {
            'description': uploadvpzdocument_general_description,
            'fields': ('docfile', 'key',), # not 'date'
        }),
    )

admin.site.register(UploadVpzDocument, UploadVpzDocumentAdmin)


downloadvpzdocument_general_description = \
        "DownloadVpzDocument containing reports (cf vpz/report resource)."

def delete_too_old_downloadvpzdocument(modeladmin, request, queryset):
    for downloadvpzdocument in queryset:
        downloadvpzdocument.delete_if_old()
delete_too_old_downloadvpzdocument.short_description = \
        "Delete selected downloadvpzdocuments if too old"

class DownloadVpzDocumentAdmin(admin.ModelAdmin):
    list_display = ('id', 'key', )
    actions = [delete_too_old_downloadvpzdocument]
    inlines = [LoadLockingDownloadVpzDocumentInline]
    fieldsets = (
        (None, {
            'description': downloadvpzdocument_general_description,
            'fields': ('docfile', 'key',), # not 'date'
        }),
    )

admin.site.register(DownloadVpzDocument, DownloadVpzDocumentAdmin)

