# -*- coding: utf-8 -*-
"""erecord_slm.serializers """

from rest_framework import serializers


class KeyOptionSerializer(serializers.Serializer):
    """Option 'key' (for identification in download...) """
    key = serializers.CharField( required=False )
    def validate(self, attrs):
        if 'key' not in attrs.keys() :
            attrs['key']=None
        return attrs

