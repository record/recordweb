# -*- coding: utf-8 -*-
"""erecord_slm.router"""

from erecord_cmn.router import DatabaseRouter
from erecord_cmn.configs.config import DB_NAME_SLM


class SlmRouter(DatabaseRouter):
    """
    The application database router.
    """
    db_name = DB_NAME_SLM
    app_name = 'erecord_slm'

