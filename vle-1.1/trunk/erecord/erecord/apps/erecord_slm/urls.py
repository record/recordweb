# -*- coding: utf-8 -*-
"""erecord_slm.urls

Urls of the erecord_slm application

"""

from django.conf.urls import include, url

from django.conf import settings
from django.conf.urls.static import static

from erecord_slm import views as slm_views

# to enable the admin:
#from django.contrib import admin
#admin.autodiscover()

urlpatterns = [

    #url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    #url(r'^admin/', admin.site.urls),

    # management of downloadable and uploaded files of erecord_vpz
    #
    # Not online/activated because should be in access only for administrator
    #
    # => Use the admin site (as admin user) instead of this url.
    #
    ##url(r'^vpz/$', 'erecord_slm.views.erecord_vpz_management',
    ##    name="erecord_slm-erecord_vpz_management"),

    # download
    #url(r'^download/(?P<pk>[0-9]+)/$',
    url(r'^download/$', slm_views.DownloadVpzDocumentDetail.as_view(),
        name='erecord_slm-download' ),

    # access
    url(r'^uploadvpzdocument/(?P<pk>[0-9]+)/access/$',
            slm_views.UploadVpzDocumentAccessView.as_view(),
        name='erecord_vpz-uploadvpzdocument-access' ),
    url(r'^downloadvpzdocument/(?P<pk>[0-9]+)/access/$',
            slm_views.DownloadVpzDocumentAccessView.as_view(),
        name='erecord_vpz-downloadvpzdocument-access' ),
]

