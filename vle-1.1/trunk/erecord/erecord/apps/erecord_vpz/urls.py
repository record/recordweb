# -*- coding: utf-8 -*-
"""erecord_vpz.urls

Urls of the erecord_vpz application

"""

from django.conf.urls import include, url

from django.conf import settings
from django.conf.urls.static import static

from django.views.generic import TemplateView
from django.views.generic.base import RedirectView

from rest_framework.urlpatterns import format_suffix_patterns

from erecord_vpz.views import activities as vpz_views_activities
from erecord_vpz.views import menus as vpz_views_menus
from erecord_vpz.views import objects as vpz_views_objects
from erecord_vpz.views import views as vpz_views_views

# to enable the admin:
#from django.contrib import admin
#admin.autodiscover()


urlpatterns = [
        
    #url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    #url(r'^admin/', admin.site.urls),

    # root page
    url(r'^$', TemplateView.as_view(template_name='erecord_cmn/index.html'),
        name="erecord_vpz-index-page"),

    # menu page
    url(r'^menu/$', vpz_views_menus.menu_page, name='erecord_vpz-menu-page'),

    # home menus

    url(r'^home/$', vpz_views_menus.home, name="erecord_vpz-home-menu"),

    url(r'^home/lists/$', vpz_views_menus.home_lists,
        name="erecord_vpz-home-menu-lists"),
    url(r'^home/detail/(?P<pk>[0-9]+)/$', vpz_views_menus.home_details,
        name="erecord_vpz-home-menu-detail"),

    url(r'^home/services/$', vpz_views_menus.home_services,
        name="erecord_vpz-home-menu-services"),
    url(r'^home/services/(?P<pk>[0-9]+)/$', vpz_views_menus.home_services,
        name="erecord_vpz-home-menu-services"),

    # lists

    url(r'^vpzpath/$', vpz_views_objects.VpzPathList.as_view(),
        name='erecord_vpz-vpzpath-list' ),

    # lists
    # the following urls are public for the moment. Should stay like this ?
    # If False ACTIVITY_PERSISTENCE case : no problem since empty responses.
    url(r'^vpzact/$', vpz_views_objects.VpzActList.as_view(),
        name='erecord_vpz-vpzact-list' ),
    url(r'^vpzorigin/$', vpz_views_objects.VpzOriginList.as_view(),
        name='erecord_vpz-vpzorigin-list' ),
    url(r'^vpzworkspace/$', vpz_views_objects.VpzWorkspaceList.as_view(),
        name='erecord_vpz-vpzworkspace-list' ),
    url(r'^vpzinput/$', vpz_views_objects.VpzInputList.as_view(),
        name='erecord_vpz-vpzinput-list' ),
    url(r'^vpzoutput/$', vpz_views_objects.VpzOutputList.as_view(),
        name='erecord_vpz-vpzoutput-list' ),
    url(r'^begin/$', vpz_views_objects.VleBeginList.as_view(),
        name='erecord_vpz-begin-list' ),
    url(r'^duration/$', vpz_views_objects.VleDurationList.as_view(),
        name='erecord_vpz-duration-list' ),
    url(r'^cond/$', vpz_views_objects.VleCondList.as_view(),
        name='erecord_vpz-cond-list' ),
    url(r'^par/$', vpz_views_objects.VleParList.as_view(),
        name='erecord_vpz-par-list' ),
    url(r'^view/$', vpz_views_objects.VleViewList.as_view(),
        name='erecord_vpz-view-list' ),
    url(r'^out/$', vpz_views_objects.VleOutList.as_view(),
        name='erecord_vpz-out-list' ),

    # details

    url(r'^vpzpath/(?P<pk>[0-9]+)/$', 
            vpz_views_objects.VpzPathDetail.as_view(),
        name='erecord_vpz-vpzpath-detail' ),

    # details
    # the following urls are public for the moment. Should stay like this ?
    # If False ACTIVITY_PERSISTENCE case : no problem since empty responses.
    # If True ACTIVITY_PERSISTENCE case : should be in access only for the
    # user who 'created' it ?
    url(r'^vpzact/(?P<pk>[0-9]+)/$', vpz_views_objects.VpzActDetail.as_view(),
        name='erecord_vpz-vpzact-detail' ),
    url(r'^vpzorigin/(?P<pk>[0-9]+)/$', 
        vpz_views_objects.VpzOriginDetail.as_view(),
        name='erecord_vpz-vpzorigin-detail' ),
    url(r'^vpzworkspace/(?P<pk>[0-9]+)/$', 
            vpz_views_objects.VpzWorkspaceDetail.as_view(),
        name='erecord_vpz-vpzworkspace-detail' ),
    url(r'^vpzinput/(?P<pk>[0-9]+)/$', 
            vpz_views_objects.VpzInputDetail.as_view(),
        name='erecord_vpz-vpzinput-detail' ),
    url(r'^vpzoutput/(?P<pk>[0-9]+)/$', 
            vpz_views_objects.VpzOutputDetail.as_view(),
        name='erecord_vpz-vpzoutput-detail' ),
    url(r'^begin/(?P<pk>[0-9]+)/$', 
            vpz_views_objects.VleBeginDetail.as_view(),
        name='erecord_vpz-begin-detail' ),
    url(r'^duration/(?P<pk>[0-9]+)/$', 
            vpz_views_objects.VleDurationDetail.as_view(),
        name='erecord_vpz-duration-detail' ),
    url(r'^cond/(?P<pk>[0-9]+)/$', 
            vpz_views_objects.VleCondDetail.as_view(),
        name='erecord_vpz-cond-detail' ),
    url(r'^par/(?P<pk>[0-9]+)/$', 
            vpz_views_objects.VleParDetail.as_view(),
        name='erecord_vpz-par-detail' ),
    url(r'^view/(?P<pk>[0-9]+)/$', 
            vpz_views_objects.VleViewDetail.as_view(),
        name='erecord_vpz-view-detail' ),
    url(r'^out/(?P<pk>[0-9]+)/$', 
            vpz_views_objects.VleOutDetail.as_view(),
        name='erecord_vpz-out-detail' ),

    # access

    url(r'^vpzpath/(?P<pk>[0-9]+)/access/$', 
            vpz_views_views.VpzPathAccessView.as_view(),
        name='erecord_vpz-vpzpath-access' ),

    url(r'^vpzact/(?P<pk>[0-9]+)/access/$', 
            vpz_views_views.VpzActAccessView.as_view(),
        name='erecord_vpz-vpzact-access' ),
    url(r'^vpzorigin/(?P<pk>[0-9]+)/access/$', 
            vpz_views_views.VpzOriginAccessView.as_view(),
        name='erecord_vpz-vpzorigin-access' ),
    url(r'^vpzinput/(?P<pk>[0-9]+)/access/$', 
            vpz_views_views.VpzInputAccessView.as_view(),
        name='erecord_vpz-vpzinput-access' ),
    url(r'^vpzoutput/(?P<pk>[0-9]+)/access/$', 
            vpz_views_views.VpzOutputAccessView.as_view(),
        name='erecord_vpz-vpzoutput-access' ),
    url(r'^vlebegin/(?P<pk>[0-9]+)/access/$', 
            vpz_views_views.VleBeginAccessView.as_view(),
        name='erecord_vpz-vlebegin-access' ),
    url(r'^vleduration/(?P<pk>[0-9]+)/access/$', 
            vpz_views_views.VleDurationAccessView.as_view(),
        name='erecord_vpz-vleduration-access' ),
    url(r'^vlecond/(?P<pk>[0-9]+)/access/$', 
            vpz_views_views.VleCondAccessView.as_view(),
        name='erecord_vpz-vlecond-access' ),
    url(r'^vlepar/(?P<pk>[0-9]+)/access/$', 
            vpz_views_views.VleParAccessView.as_view(),
        name='erecord_vpz-vlepar-access' ),
    url(r'^vleview/(?P<pk>[0-9]+)/access/$', 
            vpz_views_views.VleViewAccessView.as_view(),
        name='erecord_vpz-vleview-access' ),
    url(r'^vleout/(?P<pk>[0-9]+)/access/$', 
            vpz_views_views.VleOutAccessView.as_view(),
        name='erecord_vpz-vleout-access' ),
    url(r'^vpzworkspace/(?P<pk>[0-9]+)/access/$', 
            vpz_views_views.VpzWorkspaceAccessView.as_view(),
        name='erecord_vpz-vpzworkspace-access' ),


    # vpz/input

    # One (and only one) of both following options is needed (information from
    # which the Vpz that will be manipulated is going to be created) :
    # - vpzpath=id of a VpzPath
    # - vpz=id of a VleVpz (into 'db' database)
    url(r'^input/$', vpz_views_activities.InputView.as_view(),
        name='erecord_vpz-input' ),

    # vpz/output 

    # One (and only one) of both following options is needed (information from
    # which the Vpz that will be manipulated is going to be created) :
    # - vpzpath=id of a VpzPath
    # - vpz=id of a VleVpz (into 'db' database)
    url(r'^output/$', vpz_views_activities.OutputView.as_view(),
        name='erecord_vpz-output' ),

    # vpz/inout :

    # One (and only one) of both following options is needed (information from
    # which the Vpz that will be manipulated is going to be created) :
    # - vpzpath=id of a VpzPath
    # - vpz=id of a VleVpz (into 'db' database)
    url(r'^inout/$', vpz_views_activities.InOutView.as_view(),
        name='erecord_vpz-inout' ),

    # reports
    # One (and only one) of both following options is needed (information from
    # which the Vpz that will be manipulated is going to be created) :
    # - vpzpath=id of a VpzPath
    # - vpz=id of a VleVpz (into 'db' database)
    url(r'^report/$', 
        vpz_views_activities.ReportView.as_view(),
        name='erecord_vpz-report' ),
    url(r'^report/conditions/$', 
        vpz_views_activities.ReportConditionsView.as_view(),
        name='erecord_vpz-report-conditions' ),

    # experiment
    # One (and only one) of both following options is needed (information from
    # which the Vpz that will be manipulated is going to be created) :
    # - vpzpath=id of a VpzPath
    # - vpz=id of a VleVpz (into 'db' database)
    url(r'^experiment/$', 
        vpz_views_activities.ExperimentView.as_view(),
        name='erecord_vpz-experiment' ),

]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns = format_suffix_patterns(urlpatterns, 
                  allowed=['json', 'api', 'html', 'yaml', 'xml',])

