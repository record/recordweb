# -*- coding: utf-8 -*-
"""erecord_vpz

*This package is part of erecord - Record platform web development*

:copyright: Copyright (C) 2014-2015 INRA http://www.inra.fr.
:license: GPLv3, see :ref:`LICENSE` for more details.
:authors: see :ref:`AUTHORS`.

Vpz activities application

The vpz application is dedicated to activities on vpz (edition, modification,
simulation...).

The manipulation object is VpzAct. It has some characteristics that determine which vpz file is manipulated and all along the manipulation, some other information wil be attached to it.

See also : @include docs/vpz.txt

See :ref:`index`

"""

