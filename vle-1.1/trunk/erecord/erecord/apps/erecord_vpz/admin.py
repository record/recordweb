# -*- coding: utf-8 -*-
"""erecord_vpz.admin"""

from django.contrib import admin

from erecord_vpz.models import VpzAct
from erecord_vpz.models import VpzOrigin
from erecord_vpz.models import VpzInput
from erecord_vpz.models import VpzOutput
from erecord_vpz.models import VpzWorkspace
from erecord_vpz.models import VpzPath
from erecord_vpz.models import VleBegin
from erecord_vpz.models import VleDuration
from erecord_vpz.models import VleCond
from erecord_vpz.models import VlePar
from erecord_vpz.models import VleView
from erecord_vpz.models import VleOut

from erecord_vpz.forms import VpzActForm
from erecord_vpz.forms import VpzOriginForm
#from erecord_vpz.forms import VpzInputForm
from erecord_vpz.forms import VpzOutputForm
from erecord_vpz.forms import VpzWorkspaceForm
from erecord_vpz.forms import VpzPathForm
from erecord_vpz.forms import VleBeginForm
from erecord_vpz.forms import VleDurationForm
from erecord_vpz.forms import VleCondForm
from erecord_vpz.forms import VleParForm
from erecord_vpz.forms import VleViewForm
from erecord_vpz.forms import VleOutForm

from erecord_acs.admin import VpzLockingVpzPathInline
from erecord_acs.admin import ActivityLockingVpzActInline
from erecord_acs.admin import ActivityLockingVpzOriginInline
from erecord_acs.admin import ActivityLockingVpzInputInline
from erecord_acs.admin import ActivityLockingVpzOutputInline
from erecord_acs.admin import ActivityLockingVleBeginInline
from erecord_acs.admin import ActivityLockingVleDurationInline
from erecord_acs.admin import ActivityLockingVleCondInline
from erecord_acs.admin import ActivityLockingVleParInline
from erecord_acs.admin import ActivityLockingVleViewInline
from erecord_acs.admin import ActivityLockingVleOutInline
from erecord_acs.admin import ActivityLockingVpzWorkspaceInline

# To show/hide some admin parts

from django.contrib.auth.models import User
from django.contrib.auth.models import Group
#admin.site.unregister(User)
#admin.site.unregister(Group)

class VpzActInline(admin.TabularInline):
    model = VpzAct
    readonly_fields = ('vpzname', 'pkgname', 'vlepath',)

class VpzOriginInline(admin.TabularInline):
    model = VpzOrigin

class VpzInputInline(admin.TabularInline):
    model = VpzInput

class VpzOutputInline(admin.TabularInline):
    model = VpzOutput
    readonly_fields = ('res', 'plan', 'restype',)

class VpzWorkspaceInline(admin.TabularInline):
    model = VpzWorkspace
    readonly_fields = ('homepath', 'vlehome', 'reporthome', 'datahome')

class VleBeginInline(admin.TabularInline):
    model = VleBegin
    readonly_fields = ('value',)

class VleDurationInline(admin.TabularInline):
    model = VleDuration
    readonly_fields = ('value',)

class VleCondInline(admin.TabularInline):
    model = VleCond
    readonly_fields = ('name',)

class VleParInline(admin.TabularInline):
    model = VlePar
    readonly_fields = ('pname', 'cname', 'type', 'value', 'selected',)

class VleViewInline(admin.TabularInline):
    model = VleView
    readonly_fields = ('name', 'type', 'timestep', 'output_name',
                       'output_plugin', 'output_format', 'output_location',)

class VleOutInline(admin.TabularInline):
    model = VleOut
    readonly_fields = ('oname', 'vname', 'shortname', 'selected',)


vpzact_general_description = \
    "An activity VpzAct can be created from a VpzPath (of this 'vpz' database) \
    or from a VleVpz (of the 'db' database). \
    It is made possible to change a vpz but it should be carefully done, \
    keeping it consistent with its related objects (VpzOrigin, VpzInput, \
    VpzOutput...)."
vpzact_identification_description = \
    "This information is automatically calculated from the original \
    information (VpzOrigin relative to a VpzPath or to a VleVpz)."

def delete_too_old_activity(modeladmin, request, queryset):
    for vpzact in queryset:
        vpzact.delete_if_old()
delete_too_old_activity.short_description = "Delete selected vpz activities if too old"

class VpzActAdmin(admin.ModelAdmin):
    form = VpzActForm
    list_display = ('id', 'vpzname', 'pkgname', 'vlepath', 'verbose_name', )
    actions = [delete_too_old_activity]
    inlines = [VpzInputInline, VpzOutputInline, VpzOriginInline,
               VpzWorkspaceInline, ActivityLockingVpzActInline,]
    search_fields = ['vpzname']
    fieldsets = (
        (None, {
            'description': vpzact_general_description,
            'fields': (),
        }),
        (None, {
            'fields': ('verbose_name',),
        }),
        ('Identification', {
            'description' : vpzact_identification_description,
            'fields': ('vpzname', 'pkgname', 'vlepath',),
        }),
    )

admin.site.register(VpzAct, VpzActAdmin)


vpzorigin_general_description = \
    "A Vpz origin VpzOrigin is created for and linked to a VpzAct."

class VpzOriginAdmin(admin.ModelAdmin):
    form = VpzOriginForm
    list_display = ('id', 'text', )
    inlines = [ActivityLockingVpzOriginInline,] #VpzActInline]
            # memo
            # django 1.7.6, error :
            # <class 'erecord_vpz.admin.VpzActInline'>:
            # (admin.E202) 'erecord_vpz.VpzAct' has
            # no ForeignKey to 'erecord_vpz.VpzOrigin'.
    search_fields = ['id']
    fieldsets = (
        (None, {
            'description': vpzorigin_general_description,
            'fields': (),
        }),
        ('Vpz origin', {
            'fields': ( 'text',), 
        }),
    )

admin.site.register(VpzOrigin, VpzOriginAdmin)


vpzinput_general_description = \
    "a Vpz input VpzInput is created in relation with a VpzAct."

class VpzInputAdmin(admin.ModelAdmin):
    #list_display = ('id',)
    inlines = [ActivityLockingVpzInputInline,
               VleBeginInline, VleDurationInline,
               VleCondInline, VleViewInline,] #VpzActInline]
    search_fields = ['id']
    fieldsets = (
        (None, {
            'description': vpzinput_general_description,
            'fields': (),
        }),
    )

admin.site.register(VpzInput, VpzInputAdmin)


vpzoutput_general_description = \
    "a Vpz output VpzOutput is created in relation with a VpzAct."

class VpzOutputAdmin(admin.ModelAdmin):
    form = VpzOutputForm
    list_display = ('id', 'plan', 'restype', )
    inlines = [ActivityLockingVpzOutputInline,] #VpzActInline]
    search_fields = ['id']
    fieldsets = (
        (None, {
            'description': vpzoutput_general_description,
            'fields': (),
        }),
        ('Vpz output', {
            'fields': ('plan', 'restype', 'res',),
        }),
    )

admin.site.register(VpzOutput, VpzOutputAdmin)


vpzworkspace_general_description = \
    "a Vpz workspace VpzWorkspace is created for and linked to a VpzAct."

class VpzWorkspaceAdmin(admin.ModelAdmin):
    form = VpzWorkspaceForm
    list_display = ('id', 'verbose_name', 'homepath',)
    search_fields = ['verbose_name']
    inlines = [ActivityLockingVpzWorkspaceInline,] #VpzActInline]
    fieldsets = (
        (None, {
            'description': vpzworkspace_general_description,
            'fields': (),
        }),
        ('Vpz workspace', {
            'fields': ( 'verbose_name', 'homepath',
                        'vlehome', 'reporthome', 'datahome',), 
        }),
    )

admin.site.register(VpzWorkspace, VpzWorkspaceAdmin)


vpzpath_general_description = \
    "This information will be able to be used to 'choose' a Vpz."

class VpzPathAdmin(admin.ModelAdmin):
    form = VpzPathForm
    list_display = ('id', 'verbose_name', 'vpzpath',)
    search_fields = ['verbose_name']
    inlines = [VpzLockingVpzPathInline]

    fieldsets = (
        (None, {
            'description': vpzpath_general_description,
            'fields': (),
        }),
        ('Vpz file absolute path', {
            'fields': ('verbose_name', 'vpzpath',),
        }),
    )

admin.site.register(VpzPath, VpzPathAdmin)


vlebegin_general_description = \
    "a Vle begin VleBegin is created in relation with a VpzInput"

class VleBeginAdmin(admin.ModelAdmin):
    form = VleBeginForm
    list_display = ('id', 'verbose_name', 'value',)
    inlines = [ActivityLockingVleBeginInline,] #VpzInputInline]
    search_fields = ['id']
    fieldsets = (
        (None, {
            'description': vlebegin_general_description,
            'fields': (),
        }),
        ('Vle begin', {
            'fields': ('verbose_name', 'value',), 
        }),
    )

admin.site.register(VleBegin, VleBeginAdmin)


vledurationgeneral_description = \
    "a Vle duration VleDuration is created in relation with a VpzInput"

class VleDurationAdmin(admin.ModelAdmin):
    form = VleDurationForm
    list_display = ('id', 'verbose_name', 'value',)
    inlines = [ActivityLockingVleDurationInline,] #VpzInputInline]
    fieldsets = (
        (None, {
            'description': vledurationgeneral_description,
            'fields': (),
        }),
        ('Vle duration', {
            'fields': ('verbose_name', 'value',), 
        }),
    )

admin.site.register(VleDuration, VleDurationAdmin)


vlecond_general_description = \
    "a Vle condition VleCond is created in relation with a VpzInput"

class VleCondAdmin(admin.ModelAdmin):
    form = VleCondForm
    list_display = ('id', 'name', 'verbose_name', )
    inlines = [ActivityLockingVleCondInline, VleParInline, ] #VpzInputInline]
    search_fields = ['id']
    fieldsets = (
        (None, {
            'description': vlecond_general_description,
            'fields': (),
        }),
        ('Vle condition', {
            'fields': ('name', 'verbose_name',),
        }),
    )

admin.site.register(VleCond, VleCondAdmin)


vlepar_general_description = \
    "a Vle parameter VlePar is created in relation with a VleCond"

class VleParAdmin(admin.ModelAdmin):
    form = VleParForm
    list_display = ('id', 'pname', 'cname', 'verbose_name', 'type', 'value',
                    'selected',)
    inlines = [ActivityLockingVleParInline,] #VleCondInline]
    search_fields = ['id']
    fieldsets = (
        (None, {
            'description': vlepar_general_description,
            'fields': (),
        }),
        ('Vle parameter', {
            'fields': ('pname', 'cname', 'verbose_name', 'type', 'value',
                       'selected',),
        }),
    )

admin.site.register(VlePar, VleParAdmin)


vleview_general_description = \
    "a Vle view VleView is created in relation with a VpzInput"

class VleViewAdmin(admin.ModelAdmin):
    form = VleViewForm
    list_display = ('id', 'name', 'verbose_name', 'type', 'timestep',
                    'output_name', 'output_plugin',
                    'output_format', 'output_location',)
    inlines = [ActivityLockingVleViewInline, VleOutInline,] #VpzInputInline]
    search_fields = ['id']
    fieldsets = (
        (None, {
            'description': vleview_general_description,
            'fields': (),
        }),
        ('Vle condition', {
            'fields': ('name', 'verbose_name', 'type', 'timestep',
                       'output_name', 'output_plugin', 'output_format',
                       'output_location',),
        }),
    )

admin.site.register(VleView, VleViewAdmin)


vleout_general_description = \
    "a Vle output VleOut is created in relation with a VleView"

class VleOutAdmin(admin.ModelAdmin):
    form = VleOutForm
    list_display = ('oname', 'vname', 'shortname', 'verbose_name', 'selected',)
    inlines = [ActivityLockingVleOutInline,] #VleViewInline]
    search_fields = ['id']
    fieldsets = (
        (None, {
            'description': vleout_general_description,
            'fields': (),
        }),
        ('Vle output', {
            'fields': ('oname', 'vname', 'shortname', 'verbose_name',
                       'selected',),
        }),
    )

admin.site.register(VleOut, VleOutAdmin)


#from erecord_vpz import models as erecord_vpz_models
#from django.db.models.base import ModelBase
#othersite = admin.AdminSite('othersite')
#for name, var in erecord_vpz_models.__dict__.items():
#    if type(var) is ModelBase:
#        othersite.register(var)

