# -*- coding: UTF-8 -*-
"""erecord_vpz.views.objects """

from rest_framework.response import Response
###from rest_framework import status
from rest_framework import generics

from erecord_vpz.models import VpzAct
from erecord_vpz.models import VpzOrigin
from erecord_vpz.models import VpzInput
from erecord_vpz.models import VpzOutput
from erecord_vpz.models import VpzPath
from erecord_vpz.models import VleBegin
from erecord_vpz.models import VleDuration
from erecord_vpz.models import VleCond
from erecord_vpz.models import VlePar
from erecord_vpz.models import VleView
from erecord_vpz.models import VleOut
from erecord_vpz.models import VpzWorkspace

from erecord_vpz.forms import VpzActUserForm
from erecord_vpz.forms import VpzOriginUserForm
from erecord_vpz.forms import VpzInputUserForm
from erecord_vpz.forms import VpzOutputUserForm
from erecord_vpz.forms import VpzPathUserForm
from erecord_vpz.forms import VleBeginUserForm
from erecord_vpz.forms import VleDurationUserForm
from erecord_vpz.forms import VleCondUserForm
from erecord_vpz.forms import VleParUserForm
from erecord_vpz.forms import VleViewUserForm
from erecord_vpz.forms import VleOutUserForm
from erecord_vpz.forms import VpzWorkspaceUserForm

from erecord_vpz.serializers import VpzActSerializerLink
from erecord_vpz.serializers import VpzActSerializerTree
from erecord_vpz.serializers import VpzOriginSerializerLink
from erecord_vpz.serializers import VpzOriginSerializerTree
from erecord_vpz.serializers import VpzInputSerializerLink
from erecord_vpz.serializers import VpzInputSerializerTree
from erecord_vpz.serializers import VpzOutputSerializerLink
from erecord_vpz.serializers import VpzOutputSerializerTree
from erecord_vpz.serializers import VpzPathSerializerLink
from erecord_vpz.serializers import VpzPathSerializerTree
from erecord_vpz.serializers import VleBeginSerializerLink
from erecord_vpz.serializers import VleBeginSerializerTree
from erecord_vpz.serializers import VleDurationSerializerLink
from erecord_vpz.serializers import VleDurationSerializerTree
from erecord_vpz.serializers import VleCondSerializerLink
from erecord_vpz.serializers import VleCondSerializerTree
from erecord_vpz.serializers import VleParSerializerLink
from erecord_vpz.serializers import VleParSerializerTree
from erecord_vpz.serializers import VleViewSerializerLink
from erecord_vpz.serializers import VleViewSerializerTree
from erecord_vpz.serializers import VleOutSerializerLink
from erecord_vpz.serializers import VleOutSerializerTree
from erecord_vpz.serializers import VpzWorkspaceSerializerLink
from erecord_vpz.serializers import VpzWorkspaceSerializerTree

from erecord_cmn.views_mixins import ListViewMixin
from erecord_cmn.views_mixins import DetailViewMixin
from erecord_cmn.views_mixins import ModeOptionViewMixin
from erecord_cmn.views_mixins import StyleViewMixin
from erecord_cmn.views_mixins import FormatViewMixin
from erecord_cmn.views_mixins import DataViewMixin
from erecord_vpz.views.objects_mixins import VpzInputDetailViewMixin
from erecord_vpz.views.objects_mixins import VpzOutputDetailViewMixin
from erecord_vpz.views.objects_mixins import VpzActDetailViewMixin
from erecord_acs.views_mixins import LimitedAccessViewMixin
from erecord_acs.views_mixins import LimitedAccessVpzInputViewMixin
from erecord_cmn.views_mixins import ErrorViewMixin

from erecord_cmn.utils.logger import get_logger
LOGGER = get_logger(__name__)
from erecord_cmn.utils.errors import logger_report_error
from erecord_cmn.utils.errors import build_error_message
from erecord_cmn.utils.errors import get_error_status

#headsubtitle = "erecord_vpz application (vpz manipulation)"
headsubtitle = "(erecord_vpz)"

#------------------------------------------------------------------------------

#
#
#   """The vpz activities
#   
#   formats : .api, .json, .html
#
#   options :
#   - mode for style : to select the style presentation. Value 'link' for \
#     presentation by links, value 'tree' for presentation by opening the \
#     tree. Default value 'link'.
#
#   examples : /vpzact/ /vpzact/?mode=tree
#   /vpzact/.api?mode=link , /vpzact/?format=api&mode=link
#   /vpzact/.api?mode=tree , /vpzact/?format=api&mode=tree
#   /vpzact/.html , /vpzact/?format=html
#   /vpzact/.json , /vpzact/?format=json
#   
#   """
#
#
class VpzActList(LimitedAccessViewMixin, StyleViewMixin,
         FormatViewMixin, ListViewMixin, generics.ListAPIView):
    """The vpz activities

       ... documentation under construction ...
    """

    model = VpzAct

    def get_queryset(self):
        return self.filter_with_propagation_access_ok(
                                     model_list=VpzAct.objects.all(),
                                     data=self.data_from_request(self.request))

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VpzActSerializerTree, 
                serializer_link_class=VpzActSerializerLink) 

    def get(self, request, format=None, **kwargs):
        if format is None :
            format = self.get_format_value(data=request.query_params)
        if format=='html' :
            context = dict()
            context['title'] = 'Vpz activities'
            context['headsubtitle'] = headsubtitle
            context['form_list'] = [ VpzActUserForm(instance=o) 
                    for o in self.get_queryset() ]
            return Response(context )
        else :
            context = super(VpzActList, self).get(request, format, **kwargs)
            return context

#
#
#   """The vpz origins
#   
#   formats : .api, .json, .html
#
#   options :
#   - mode for style : to select the style presentation. Value 'link' for \
#     presentation by links, value 'tree' for presentation by opening the \
#     tree. Default value 'link'.
#
#   examples : /vpzorigin/ /vpzorigin/?mode=tree
#   /vpzorigin/.api?mode=link , /vpzorigin/?format=api&mode=link
#   /vpzorigin/.api?mode=tree , /vpzorigin/?format=api&mode=tree
#   /vpzorigin/.html , /vpzorigin/?format=html
#   /vpzorigin/.json , /vpzorigin/?format=json
#
#   """
#
#
class VpzOriginList(LimitedAccessViewMixin, StyleViewMixin,
         FormatViewMixin, ListViewMixin, generics.ListAPIView):
    """The vpz origins

       ... documentation under construction ...
    """
    model = VpzOrigin

    def get_queryset(self):
        return self.filter_access_ok(model_list=VpzOrigin.objects.all(),
                                     data=self.data_from_request(self.request))

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VpzOriginSerializerTree, 
                serializer_link_class=VpzOriginSerializerLink) 

    def get(self, request, format=None, **kwargs):
        if format is None :
            format = self.get_format_value(data=request.query_params)
        if format=='html' :
            context = dict()
            context['title'] = 'Vpz origins'
            context['headsubtitle'] = headsubtitle
            context['form_list'] = [ VpzOriginUserForm(instance=o) 
                    for o in self.get_queryset() ]
            return Response(context )
        else :
            context = super(VpzOriginList, self).get(request, format, **kwargs)
            return context

#
#
#   """The vpz inputs
#   
#   formats : .api, .json, .html
#
#   options :
#   - mode for style : to select the style presentation. Value 'link' for \
#     presentation by links, value 'tree' for presentation by opening the \
#     tree. Default value 'link'.
#
#   examples : /vpzinput/ /vpzinput/?mode=tree
#   /vpzinput/.api?mode=link , /vpzinput/?format=api&mode=link
#   /vpzinput/.api?mode=tree , /vpzinput/?format=api&mode=tree
#   /vpzinput/.html , /vpzinput/?format=html
#   /vpzinput/.json , /vpzinput/?format=json
#
#   """
#
#
class VpzInputList(LimitedAccessViewMixin, StyleViewMixin,
         FormatViewMixin, ListViewMixin, generics.ListAPIView):
    """The vpz inputs

       ... documentation under construction ...
    """

    model = VpzInput

    def get_queryset(self):
        return self.filter_with_propagation_access_ok(
                                     model_list=VpzInput.objects.all(),
                                     data=self.data_from_request(self.request))

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VpzInputSerializerTree, 
                serializer_link_class=VpzInputSerializerLink) 

    def get(self, request, format=None, **kwargs):
        if format is None :
            format = self.get_format_value(data=request.query_params)
        if format=='html' :
            context = dict()
            context['title'] = 'Vpz inputs'
            context['headsubtitle'] = headsubtitle
            context['form_list'] = [ VpzInputUserForm(instance=o) 
                    for o in self.get_queryset() ]
            return Response(context )
        else :
            context = super(VpzInputList, self).get(request, format, **kwargs)
            return context

#
#
#   """The vpz outputs
#   
#   formats : .api, .json, .html
#
#   options :
#   - mode for style : to select the style presentation. Value 'link' for \
#     presentation by links, value 'tree' for presentation by opening the \
#     tree. Default value 'link'.
#
#   examples : /vpzoutput/ /vpzoutput/?mode=tree
#   /vpzoutput/.api?mode=link , /vpzoutput/?format=api&mode=link
#   /vpzoutput/.api?mode=tree , /vpzoutput/?format=api&mode=tree
#   /vpzoutput/.html , /vpzoutput/?format=html
#   /vpzoutput/.json , /vpzoutput/?format=json
#
#   """
#
#
class VpzOutputList(LimitedAccessViewMixin, StyleViewMixin,
         FormatViewMixin, ListViewMixin, generics.ListAPIView):
    """The vpz outputs

       ... documentation under construction ...
    """

    model = VpzOutput

    def get_queryset(self):
        return self.filter_access_ok(model_list=VpzOutput.objects.all(),
                                     data=self.data_from_request(self.request))

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VpzOutputSerializerTree, 
                serializer_link_class=VpzOutputSerializerLink) 

    def get(self, request, format=None, **kwargs):
        if format is None :
            format = self.get_format_value(data=request.query_params)
        if format=='html' :
            context = dict()
            context['title'] = 'Vpz outputs'
            context['headsubtitle'] = headsubtitle
            context['form_list'] = [ VpzOutputUserForm(instance=o) 
                    for o in self.get_queryset() ]
            return Response(context )
        else :
            context = super(VpzOutputList, self).get(request, format, **kwargs)
            return context

#
#
#   """The vpz paths
#   
#   formats : .api, .json, .html
#
#   options :
#   - mode for style : to select the style presentation. Value 'link' for \
#     presentation by links, value 'tree' for presentation by opening the \
#     tree. Default value 'link'.
#
#   examples : /vpzpath/ /vpzpath/?mode=tree
#   /vpzpath/.api?mode=link , /vpzpath/?format=api&mode=link
#   /vpzpath/.api?mode=tree , /vpzpath/?format=api&mode=tree
#   /vpzpath/.html , /vpzpath/?format=html
#   /vpzpath/.json , /vpzpath/?format=json
#
#   """
#
#
class VpzPathList(StyleViewMixin, FormatViewMixin, ListViewMixin, 
        generics.ListAPIView):
    """The vpz paths

       ... documentation under construction ...
    """

    model = VpzPath

    def get_queryset(self):
        return VpzPath.objects.all()

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VpzPathSerializerTree, 
                serializer_link_class=VpzPathSerializerLink) 

    def get(self, request, format=None, **kwargs):
        if format is None :
            format = self.get_format_value(data=request.query_params)
        if format=='html' :
            context = dict()
            context['title'] = 'Vpz paths'
            context['headsubtitle'] = headsubtitle
            context['form_list'] = [ VpzPathUserForm(instance=o) 
                    for o in self.get_queryset() ]
            return Response(context )
        else :
            context = super(VpzPathList, self).get(request, format, **kwargs)
            return context


class VleBeginList(LimitedAccessViewMixin, StyleViewMixin,
         FormatViewMixin, ListViewMixin, generics.ListAPIView):
    """The vle begins """
    
    model = VleBegin

    def get_queryset(self):
        return self.filter_access_ok(model_list=VleBegin.objects.all(),
                                     data=self.data_from_request(self.request))

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VleBeginSerializerTree, 
                serializer_link_class=VleBeginSerializerLink) 

    def get(self, request, format=None, **kwargs):
        if format is None :
            format = self.get_format_value(data=request.query_params)
        if format=='html' :
            context = dict()
            context['title'] = 'Vpz begins'
            context['headsubtitle'] = headsubtitle
            context['form_list'] = [ VleBeginUserForm(instance=o) 
                    for o in self.get_queryset() ]
            return Response(context )
        else :
            context = super(VleBeginList, self).get(request, format, **kwargs)
            return context


class VleDurationList(LimitedAccessViewMixin, StyleViewMixin,
         FormatViewMixin, ListViewMixin, generics.ListAPIView):
    """The vle durations """
    
    model = VleDuration

    def get_queryset(self):
        return self.filter_access_ok(model_list=VleDuration.objects.all(),
                                     data=self.data_from_request(self.request))

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VleDurationSerializerTree, 
                serializer_link_class=VleDurationSerializerLink) 

    def get(self, request, format=None, **kwargs):
        if format is None :
            format = self.get_format_value(data=request.query_params)
        if format=='html' :
            context = dict()
            context['title'] = 'Vle durations'
            context['headsubtitle'] = headsubtitle
            context['form_list'] = [ VleDurationUserForm(instance=o) 
                    for o in self.get_queryset() ]
            return Response(context )
        else :
            context = super(VleDurationList, self).get(request, format,
                                                       **kwargs)
            return context

class VleCondList(LimitedAccessViewMixin, StyleViewMixin,
         FormatViewMixin, ListViewMixin, generics.ListAPIView):
    """The vle conditions """
    
    model = VleCond

    def get_queryset(self):
        return self.filter_with_propagation_access_ok(
                                     model_list=VleCond.objects.all(),
                                     data=self.data_from_request(self.request))

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VleCondSerializerTree, 
                serializer_link_class=VleCondSerializerLink) 

    def get(self, request, format=None, **kwargs):
        if format is None :
            format = self.get_format_value(data=request.query_params)
        if format=='html' :
            context = dict()
            context['title'] = 'Vle conditions'
            context['headsubtitle'] = headsubtitle
            context['form_list'] = [ VleCondUserForm(instance=o) 
                    for o in self.get_queryset() ]
            return Response(context )
        else :
            context = super(VleCondList, self).get(request, format, **kwargs)
            return context


class VleParList(LimitedAccessViewMixin, StyleViewMixin,
         FormatViewMixin, ListViewMixin, generics.ListAPIView):
    """The vle parameters """
    
    model = VlePar

    def get_queryset(self):
        return self.filter_access_ok(model_list=VlePar.objects.all(),
                                     data=self.data_from_request(self.request))

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VleParSerializerTree, 
                serializer_link_class=VleParSerializerLink) 

    def get(self, request, format=None, **kwargs):
        if format is None :
            format = self.get_format_value(data=request.query_params)
        if format=='html' :
            context = dict()
            context['title'] = 'Vle parameters'
            context['headsubtitle'] = headsubtitle
            context['form_list'] = [ VleParUserForm(instance=o) 
                    for o in self.get_queryset() ]
            return Response(context )
        else :
            context = super(VleParList, self).get(request, format, **kwargs)
            return context

class VleViewList(LimitedAccessViewMixin, StyleViewMixin,
         FormatViewMixin, ListViewMixin, generics.ListAPIView):
    """The vle views """
    
    model = VleView

    def get_queryset(self):
        return self.filter_with_propagation_access_ok(
                                     model_list=VleView.objects.all(),
                                     data=self.data_from_request(self.request))

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VleViewSerializerTree, 
                serializer_link_class=VleViewSerializerLink) 

    def get(self, request, format=None, **kwargs):
        if format is None :
            format = self.get_format_value(data=request.query_params)
        if format=='html' :
            context = dict()
            context['title'] = 'Vle views'
            context['headsubtitle'] = headsubtitle
            context['form_list'] = [ VleViewUserForm(instance=o) 
                    for o in self.get_queryset() ]
            return Response(context )
        else :
            context = super(VleViewList, self).get(request, format, **kwargs)
            return context

class VleOutList(LimitedAccessViewMixin, StyleViewMixin,
         FormatViewMixin, ListViewMixin, generics.ListAPIView):
    """The vle output datas """
    
    model = VleOut

    def get_queryset(self):
        return self.filter_access_ok(model_list=VleOut.objects.all(),
                                     data=self.data_from_request(self.request))

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VleOutSerializerTree, 
                serializer_link_class=VleOutSerializerLink) 

    def get(self, request, format=None, **kwargs):
        if format is None :
            format = self.get_format_value(data=request.query_params)
        if format=='html' :
            context = dict()
            context['title'] = 'Vle output datas'
            context['headsubtitle'] = headsubtitle
            context['form_list'] = [ VleOutUserForm(instance=o) 
                    for o in self.get_queryset() ]
            return Response(context )
        else :
            context = super(VleOutList, self).get(request, format, **kwargs)
            return context

#
#
#   """The vpz workspaces
#   
#   formats : .api, .json, .html
#
#   options :
#   - mode for style : to select the style presentation. Value 'link' for \
#     presentation by links, value 'tree' for presentation by opening the \
#     tree. Default value 'link'.
#
#   examples : /vpzworkspace/ /vpzworkspace/?mode=tree
#   /vpzworkspace/.api?mode=link , /vpzworkspace/?format=api&mode=link
#   /vpzworkspace/.api?mode=tree , /vpzworkspace/?format=api&mode=tree
#   /vpzworkspace/.html , /vpzworkspace/?format=html
#   /vpzworkspace/.json , /vpzworkspace/?format=json
#
#   """
#
#
class VpzWorkspaceList(LimitedAccessViewMixin, StyleViewMixin,
         FormatViewMixin, ListViewMixin, generics.ListAPIView):
    """The vpz workspaces

       ... documentation under construction ...
    """

    model = VpzWorkspace

    def get_queryset(self):
        return self.filter_access_ok(model_list=VpzWorkspace.objects.all(),
                                     data=self.data_from_request(self.request))

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VpzWorkspaceSerializerTree, 
                serializer_link_class=VpzWorkspaceSerializerLink) 

    def get(self, request, format=None, **kwargs):
        if format is None :
            format = self.get_format_value(data=request.query_params)
        if format=='html' :
            context = dict()
            context['title'] = 'Vpz workspaces'
            context['headsubtitle'] = headsubtitle
            context['form_list'] = [ VpzWorkspaceUserForm(instance=o) 
                    for o in self.get_queryset() ]
            return Response(context )
        else :
            context = super(VpzWorkspaceList, self).get(request, format,
                                                        **kwargs)
            return context

#
#
#   """One vpz activity
#   
#   formats : .api, .json, .html
#
#   attribute :
#   - id : the vpz activity id (VpzAct object)
#
#   options :
#   - mode for style : to select the style presentation. Value 'link' for \
#     presentation by links, value 'tree' for presentation by opening the \
#     tree. Default value 'link'.
#
#   examples : /vpzact/8/ /vpzact/8/?mode=tree
#   /vpzact/8/.api?mode=link , /vpzact/8/?format=api&mode=link
#   /vpzact/8/.api /vpzact/8/.json , /vpzact/8/?format=json
#   /vpzact/8/.html , /vpzact/8/?format=html
#
#   """
#
#
class VpzActDetail(LimitedAccessViewMixin, ErrorViewMixin,
         VpzActDetailViewMixin,
         StyleViewMixin, FormatViewMixin, DataViewMixin, DetailViewMixin, 
         generics.RetrieveAPIView):
    """One vpz activity

       ... documentation under construction ...
    """

    model = VpzAct

    def get_queryset(self):
        return VpzAct.objects.all()

    def get(self, request, format=None, **kwargs):
        vpzact = VpzAct.objects.get(pk=kwargs['pk'])
        data = request.query_params
        if format is None :
            format = self.get_format_value(data=data)

        try:
            self.control_access_with_propagation(model=vpzact, data=data)
        except Exception as e :
            s = get_error_status(e)
            errormsg = build_error_message(error=e)
            logger_report_error(LOGGER)
            if format=='html' :
                context = self.html_error_context(status=s, detail=errormsg)
                response = Response(context)
            else :
                response = Response(data={'detail':errormsg,}, status=s)
            return response

        if format=='html' :
            context = dict()
            context['title'] = 'Vpz activity'
            context['headsubtitle'] = headsubtitle
            context['form'] = VpzActUserForm(instance=vpzact)
            response = Response(context )
        else :
            context = super(VpzActDetail, self).get(request, format, **kwargs)
            response = context
        return response

#
#
#   """One vpz origin 
#   
#   formats : .api, .json, .html
#
#   attribute :
#   - id : the vpz origin id (VpzOrigin object)
#
#   options :
#   - mode for style : to select the style presentation. Value 'link' for \
#     presentation by links, value 'tree' for presentation by opening the \
#     tree. Default value 'link'.
#
#   examples : /vpzorigin/8/ /vpzorigin/8/?mode=tree
#   /vpzorigin/8/.api?mode=link , /vpzorigin/8/?format=api&mode=link
#   /vpzorigin/8/.api /vpzorigin/8/.json , /vpzorigin/8/?format=json
#   /vpzorigin/8/.html , /vpzorigin/8/?format=html
#
#   """
#
#
class VpzOriginDetail(LimitedAccessViewMixin, ErrorViewMixin, StyleViewMixin,
         FormatViewMixin, DetailViewMixin, generics.RetrieveAPIView):
    """One vpz origin 

       ... documentation under construction ...
    """

    model = VpzOrigin

    def get_queryset(self):
        return VpzOrigin.objects.all()

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VpzOriginSerializerTree, 
                serializer_link_class=VpzOriginSerializerLink) 

    def get(self, request, format=None, **kwargs):
        vpzorigin = VpzOrigin.objects.get(pk=kwargs['pk'])
        data = request.query_params
        if format is None :
            format = self.get_format_value(data=data)

        try:
            self.control_access(model=vpzorigin, data=data)
        except Exception as e :
            s = get_error_status(e)
            errormsg = build_error_message(error=e)
            logger_report_error(LOGGER)
            if format=='html' :
                context = self.html_error_context(status=s, detail=errormsg)
                response = Response(context)
            else :
                response = Response(data={'detail':errormsg,}, status=s)
            return response

        if format=='html' :
            context = dict()
            context['title'] = 'Vpz origin'
            context['headsubtitle'] = headsubtitle
            context['form'] = VpzOriginUserForm(instance=vpzorigin)
            return Response(context )
        else :
            context = super(VpzOriginDetail, self).get(request, format,
                                                       **kwargs)
            return context

# ModeOptionViewMixin is still here with StyleViewMixin.
#
#
#   """One vpz input 
#   
#   formats : .api, .json, .html, yaml, xml
#
#   attribute :
#   - id : the vpz input id (VpzInput object)
#
#   options :
#   - mode for style : to select the style presentation. Value 'link' for \
#     presentation by links, value 'tree' for presentation by opening the tree.
#     Two other style values 'compact' and 'compactlist' are available (also \
#     available in html format). Looking like the post data format, the \
#     'compact' and 'compactlist' styles can help the user to prepare its \
#     modifications (parameters values...). Default style value 'compact'.
#
#   examples : /vpzinput/8/ /vpzinput/8/?mode=tree
#   /vpzinput/8/.api?mode=link , /vpzinput/8/?format=api&mode=link
#   /vpzinput/8/.api?mode=compact , /vpzinput/8/?format=api&mode=compactlist
#   /vpzinput/8/.api /vpzinput/8/.json , /vpzinput/8/?format=json
#   /vpzinput/8/.html , /vpzinput/8/?format=html
#   /vpzinput/8/.html?mode=compactlist , /vpzinput/8/?format=html&mode=compact
#
#   """
#
#
class VpzInputDetail(LimitedAccessVpzInputViewMixin, ErrorViewMixin,
         VpzInputDetailViewMixin,
         StyleViewMixin, FormatViewMixin, DataViewMixin, DetailViewMixin,
         generics.RetrieveAPIView):
    """One vpz input 

       ... documentation under construction ...
    """
    model = VpzInput

    def get_queryset(self):
        return VpzInput.objects.all()

    def get(self, request, format=None, **kwargs):
        data = request.query_params
        if format is None :
            format = self.get_format_value(data=data)
        style = self.get_style_value()
        vpzinput = VpzInput.objects.get(pk=kwargs['pk'])

        try:
            self.control_access_with_propagation(model=vpzinput, data=data)
        except Exception as e :
            s = get_error_status(e)
            errormsg = build_error_message(error=e)
            logger_report_error(LOGGER)
            if format=='html' :
                context = self.html_error_context(status=s, detail=errormsg)
                response = Response(context)
            else :
                response = Response(data={'detail':errormsg,}, status=s)
            return response

        if format=='html' :

            if style=='compact' or style=='compactlist' :
                context = self.html_compact_context(vpzinput=vpzinput,
                        is_compactlist=(style=='compactlist'),
                        headsubtitle=headsubtitle)
                response = Response(context)

            else : # style == 'link' or 'tree'
                context = self.html_nocompact_context(vpzinput=vpzinput,
                        headsubtitle=headsubtitle)
                response = Response(context)

        else : # format other than html ('api' 'json' 'yaml' 'xml')

            if style=='compact' or style=='compactlist' :

                if style=='compact' :
                    context = self.nohtml_compact_context(
                            vpzinput=vpzinput)
                    response = Response(data=context)
                elif style=='compactlist' :
                    context = self.nohtml_compactlist_context(
                            vpzinput=vpzinput)
                    response = Response(data=context)

            else : # style == 'link' or 'tree'
                context = super(VpzInputDetail, self).get(request, format,
                                                          **kwargs)
                response = context
        return response

#
#
#   """One vpz output 
#   
#   formats : .api, .json, .html, yaml, xml
#
#   attribute :
#   - id : the vpz output id (VpzOutput object)
#
#   options :
#   - mode for style : to select the style presentation. Value 'link' for \
#     presentation by links, value 'tree' for presentation by opening the \
#     tree.
#     Another style value 'compact' is available (also available in html \
#     format) for the output datas to be identified by their selection_name.
#     The style value 'compactlist' is also available, having here \
#     the same meaning as 'compact' value.
#     Default style value 'tree'.
#
#   The 'compact' (and 'compactlist') style value has an effect only in
#   'dataframe' restype case (no effect in 'matrix' restype case).
#   In the original self.res, if 'dataframe' restype case : an output data \
#   is identified with a key relative to its name (not exactly oname), under \
#   the key vname.
#
#   examples : /vpzoutput/8/ /vpzoutput/8/?mode=tree
#   /vpzoutput/8/.api?mode=link , /vpzoutput/8/?format=api&mode=link
#   /vpzoutput/8/?format=api&mode=compact
#   /vpzoutput/8/.api /vpzoutput/8/.json , /vpzoutput/8/?format=json
#   /vpzoutput/8/.html , /vpzoutput/8/?format=html
#
#   """
#
#
class VpzOutputDetail(LimitedAccessViewMixin, ErrorViewMixin,
         VpzOutputDetailViewMixin,
         StyleViewMixin, FormatViewMixin, DataViewMixin, DetailViewMixin,
         generics.RetrieveAPIView):
    """One vpz output 

       ... documentation under construction ...
    """

    model = VpzOutput

    def get_queryset(self):
        return VpzOutput.objects.all()

    def get(self, request, format=None, **kwargs):
        data = request.query_params
        if format is None :
            format = self.get_format_value(data=data)
        style = self.get_style_value() # 'tree' or 'link' or 'compact'
        storage = self.get_storage_value(data=data)
        vpzoutput = VpzOutput.objects.get(pk=kwargs['pk'])

        try:
            self.control_access(model=vpzoutput, data=data)
        except Exception as e :
            s = get_error_status(e)
            errormsg = build_error_message(error=e)
            logger_report_error(LOGGER)
            if format=='html' :
                context = self.html_error_context(status=s, detail=errormsg)
                response = Response(context)
            else :
                response = Response(data={'detail':errormsg,}, status=s)
            return response

        if format=='html' :

            context = dict()
            if (style=='compact') :
                #title = 'Vpz output (compact)'
                title = 'The simulation results (in compact style)'
                context['res'] = vpzoutput.make_res_compact()
                context['plan'] = vpzoutput.plan
                context['restype'] = vpzoutput.restype
            else :
                #title = 'Vpz output'
                title = 'The simulation results'
                form = VpzOutputUserForm(instance=vpzoutput)
                context['form'] = form

            context['title'] = title
            context['headsubtitle'] = headsubtitle
            context['style'] = style
            if storage :
                context['storage'] = 'storage'
            response = Response(context)

        else : # format other than html ('api' 'json' 'yaml' 'xml')

            if (style=='compact') :
                context = dict()
                context['res'] = vpzoutput.make_res_compact()
                context['plan'] = vpzoutput.plan
                context['restype'] = vpzoutput.restype
                response = Response(data=context)
            else :
                context = super(VpzOutputDetail, self).get(request, format,
                                                           **kwargs)
                response = context
        return response

#
#
#   """One vpz path 
#   
#   formats : .api, .json, .html
#
#   attribute :
#   - id : the vpz path id (VpzPath object)
#
#   options :
#   - mode for style : to select the style presentation. Value 'link' for \
#     presentation by links, value 'tree' for presentation by opening the \
#     tree. Default value 'link'.
#
#   examples : /vpzpath/8/ /vpzpath/8/?mode=tree
#   /vpzpath/8/.api?mode=link , /vpzpath/8/?format=api&mode=link
#   /vpzpath/8/.api /vpzpath/8/.json , /vpzpath/8/?format=json
#   /vpzpath/8/.html , /vpzpath/8/?format=html
#
#   """
#
#
class VpzPathDetail(StyleViewMixin, FormatViewMixin, DetailViewMixin,
        generics.RetrieveAPIView):
    """One vpz path 

       ... documentation under construction ...
    """

    model = VpzPath

    def get_queryset(self):
        return VpzPath.objects.all()

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VpzPathSerializerTree, 
                serializer_link_class=VpzPathSerializerLink) 

    def get(self, request, format=None, **kwargs):
        data = request.query_params
        vpzpath = VpzPath.objects.get(pk=kwargs['pk'])
        if format is None :
            format = self.get_format_value(data=data)
        if format=='html' :
            context = dict()
            context['title'] = 'Vpz path'
            context['headsubtitle'] = headsubtitle
            context['form'] = VpzPathUserForm(instance=vpzpath)
            return Response(context )
        else :
            context = super(VpzPathDetail, self).get(request, format, **kwargs)
            return context


class VleBeginDetail(LimitedAccessViewMixin, ErrorViewMixin, StyleViewMixin,
         FormatViewMixin, DetailViewMixin, generics.RetrieveAPIView):
    """One vle begin """

    model = VleBegin

    def get_queryset(self):
        return VleBegin.objects.all()

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VleBeginSerializerTree, 
                serializer_link_class=VleBeginSerializerLink) 

    def get(self, request, format=None, **kwargs):
        data = request.query_params
        vlebegin = VleBegin.objects.get(pk=kwargs['pk'])
        if format is None :
            format = self.get_format_value(data=data)

        try:
            self.control_access(model=vlebegin, data=data)
        except Exception as e :
            s = get_error_status(e)
            errormsg = build_error_message(error=e)
            logger_report_error(LOGGER)
            if format=='html' :
                context = self.html_error_context(status=s, detail=errormsg)
                response = Response(context)
            else :
                response = Response(data={'detail':errormsg,}, status=s)
            return response

        if format=='html' :
            context = dict()
            context['title'] = 'Vle begin'
            context['headsubtitle'] = headsubtitle
            context['form'] = VleBeginUserForm(instance=vlebegin)
            return Response(context )
        else :
            context = super(VleBeginDetail, self).get(request, format, **kwargs)
            return context


class VleDurationDetail(LimitedAccessViewMixin, ErrorViewMixin, StyleViewMixin,
         FormatViewMixin, DetailViewMixin, generics.RetrieveAPIView):
    """One vle duration """

    model = VleDuration

    def get_queryset(self):
        return VleDuration.objects.all()

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VleDurationSerializerTree, 
                serializer_link_class=VleDurationSerializerLink)

    def get(self, request, format=None, **kwargs):
        data = request.query_params
        vleduration = VleDuration.objects.get(pk=kwargs['pk'])
        if format is None :
            format = self.get_format_value(data=data)

        try:
            self.control_access(model=vleduration, data=data)
        except Exception as e :
            s = get_error_status(e)
            errormsg = build_error_message(error=e)
            logger_report_error(LOGGER)
            if format=='html' :
                context = self.html_error_context(status=s, detail=errormsg)
                response = Response(context)
            else :
                response = Response(data={'detail':errormsg,}, status=s)
            return response

        if format=='html' :
            context = dict()
            context['title'] = 'Vle duration'
            context['headsubtitle'] = headsubtitle
            context['form'] = VleDurationUserForm(instance=vleduration)
            return Response(context )
        else :
            context = super(VleDurationDetail, self).get(request, format,
                                                         **kwargs)
            return context

class VleCondDetail(LimitedAccessViewMixin, ErrorViewMixin, StyleViewMixin,
         FormatViewMixin, DetailViewMixin, generics.RetrieveAPIView):
    """One vle condition """

    model = VleCond

    def get_queryset(self):
        return VleCond.objects.all()

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VleCondSerializerTree, 
                serializer_link_class=VleCondSerializerLink) 

    def get(self, request, format=None, **kwargs):
        data = request.query_params
        vlecond = VleCond.objects.get(pk=kwargs['pk'])
        if format is None :
            format = self.get_format_value(data=data)

        try:
            self.control_access_with_propagation(model=vlecond, data=data)
        except Exception as e :
            s = get_error_status(e)
            errormsg = build_error_message(error=e)
            logger_report_error(LOGGER)
            if format=='html' :
                context = self.html_error_context(status=s, detail=errormsg)
                response = Response(context)
            else :
                response = Response(data={'detail':errormsg,}, status=s)
            return response

        if format=='html' :
            context = dict()
            context['title'] = 'Vle condition'
            context['headsubtitle'] = headsubtitle
            context['form'] = VleCondUserForm(instance=vlecond)
            return Response(context )
        else :
            context = super(VleCondDetail, self).get(request, format, **kwargs)
            return context


class VleParDetail(LimitedAccessViewMixin, ErrorViewMixin, StyleViewMixin,
         FormatViewMixin, DetailViewMixin, generics.RetrieveAPIView):
    """One vle parameter """

    model = VlePar

    def get_queryset(self):
        return VlePar.objects.all()

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VleParSerializerTree, 
                serializer_link_class=VleParSerializerLink) 

    def get(self, request, format=None, **kwargs):
        data = request.query_params
        vlepar = VlePar.objects.get(pk=kwargs['pk'])
        if format is None :
            format = self.get_format_value(data=data)

        try:
            self.control_access(model=vlepar, data=data)
        except Exception as e :
            s = get_error_status(e)
            errormsg = build_error_message(error=e)
            logger_report_error(LOGGER)
            if format=='html' :
                context = self.html_error_context(status=s, detail=errormsg)
                response = Response(context)
            else :
                response = Response(data={'detail':errormsg,}, status=s)
            return response

        if format=='html' :
            context = dict()
            context['title'] = 'Vle parameter'
            context['headsubtitle'] = headsubtitle
            context['form'] = VleParUserForm(instance=vlepar)
            return Response(context )
        else :
            context = super(VleParDetail, self).get(request, format, **kwargs)
            return context


class VleViewDetail(LimitedAccessViewMixin, ErrorViewMixin, StyleViewMixin,
         FormatViewMixin, DetailViewMixin, generics.RetrieveAPIView):
    """One vle view """

    model = VleView

    def get_queryset(self):
        return VleView.objects.all()

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VleViewSerializerTree, 
                serializer_link_class=VleViewSerializerLink) 

    def get(self, request, format=None, **kwargs):
        data = request.query_params
        vleview = VleView.objects.get(pk=kwargs['pk'])
        if format is None :
            format = self.get_format_value(data=data)

        try:
            self.control_access_with_propagation(model=vleview, data=data)
        except Exception as e :
            s = get_error_status(e)
            errormsg = build_error_message(error=e)
            logger_report_error(LOGGER)
            if format=='html' :
                context = self.html_error_context(status=s, detail=errormsg)
                response = Response(context)
            else :
                response = Response(data={'detail':errormsg,}, status=s)
            return response

        if format=='html' :
            context = dict()
            context['title'] = 'Vle view'
            context['headsubtitle'] = headsubtitle
            context['form'] = VleViewUserForm(instance=vleview)
            return Response(context )
        else :
            context = super(VleViewDetail, self).get(request, format, **kwargs)
            return context


class VleOutDetail(LimitedAccessViewMixin, ErrorViewMixin, StyleViewMixin,
         FormatViewMixin, DetailViewMixin, generics.RetrieveAPIView):
    """One vle output data """

    model = VleOut

    def get_queryset(self):
        return VleOut.objects.all()

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VleOutSerializerTree, 
                serializer_link_class=VleOutSerializerLink) 

    def get(self, request, format=None, **kwargs):
        data = request.query_params
        vleout = VleOut.objects.get(pk=kwargs['pk'])
        if format is None :
            format = self.get_format_value(data=data)

        try:
            self.control_access(model=vleout, data=data)
        except Exception as e :
            s = get_error_status(e)
            errormsg = build_error_message(error=e)
            logger_report_error(LOGGER)
            if format=='html' :
                context = self.html_error_context(status=s, detail=errormsg)
                response = Response(context)
            else :
                response = Response(data={'detail':errormsg,}, status=s)
            return response

        if format=='html' :
            context = dict()
            context['title'] = 'Vle output data'
            context['headsubtitle'] = headsubtitle
            context['form'] = VleOutUserForm(instance=vleout)
            return Response(context )
        else :
            context = super(VleOutDetail, self).get(request, format, **kwargs)
            return context

#
#
#   """One vpz workspace 
#   
#   formats : .api, .json, .html
#
#   attribute :
#   - id : the vpz workspace id (VpzWorkspace object)
#
#   options :
#   - mode for style : to select the style presentation. Value 'link' for \
#     presentation by links, value 'tree' for presentation by opening the \
#     tree. Default value 'link'.
#
#   examples : /vpzworkspace/8/ /vpzworkspace/8/?mode=tree
#   /vpzworkspace/8/.api?mode=link , /vpzworkspace/8/?format=api&mode=link
#   /vpzworkspace/8/.api /vpzworkspace/8/.json , /vpzworkspace/8/?format=json
#   /vpzworkspace/8/.html , /vpzworkspace/8/?format=html
#
#   """
#
#
class VpzWorkspaceDetail(LimitedAccessViewMixin, ErrorViewMixin,
         StyleViewMixin,
         FormatViewMixin, DetailViewMixin, generics.RetrieveAPIView):
    """One vpz workspace 

       ... documentation under construction ...
    """

    model = VpzWorkspace

    def get_queryset(self):
        return VpzWorkspace.objects.all()

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VpzWorkspaceSerializerTree, 
                serializer_link_class=VpzWorkspaceSerializerLink) 

    def get(self, request, format=None, **kwargs):
        data = request.query_params
        vpzworkspace = VpzWorkspace.objects.get(pk=kwargs['pk'])
        if format is None :
            format = self.get_format_value(data=data)

        try:
            self.control_access(model=vpzworkspace, data=data)
        except Exception as e :
            s = get_error_status(e)
            errormsg = build_error_message(error=e)
            logger_report_error(LOGGER)
            if format=='html' :
                context = self.html_error_context(status=s, detail=errormsg)
                response = Response(context)
            else :
                response = Response(data={'detail':errormsg,}, status=s)
            return response

        if format=='html' :
            context = dict()
            context['title'] = 'Vpz workspace'
            context['headsubtitle'] = headsubtitle
            context['form'] = VpzWorkspaceUserForm(instance=vpzworkspace)
            return Response(context )
        else :
            context = super(VpzWorkspaceDetail, self).get(request, format,
                                                          **kwargs)
            return context

#------------------------------------------------------------------------------

