# -*- coding: UTF-8 -*-
"""erecord_vpz.views.activities_mixins

Mixins common to erecord_vpz application views (activities)

"""

from rest_framework.response import Response
from django.http import Http404
from rest_framework.reverse import reverse

from erecord_vpz.models import VpzAct
from erecord_vpz.models import VpzOrigin
from erecord_vpz.models import VpzInput
from erecord_vpz.models import VpzOutput
from erecord_vpz.models import VpzWorkspace
from erecord_vpz.models import VpzPath

from erecord_acs.models import ActivityLock

#from erecord_cmn.configs.config import DB_NAME_DB
from erecord_db.models import VleVpz

from erecord_cmn.views_mixins import ErrorViewMixin
from erecord_cmn.views_mixins import ModeOptionViewMixin
from erecord_cmn.views_mixins import DataFolderCopyViewMixin
from erecord_acs.views_mixins import LimitedAccessViewMixin
from erecord_acs.views_mixins import LimitedAccessVpzInputViewMixin

from erecord_cmn.views_mixins import RenderViewMixin
from rest_framework.renderers import TemplateHTMLRenderer

from erecord_vpz.compact.views_mixins import VleParCompactOptionViewMixin

from erecord_vpz.serializers import VpzSelectionOptionsSerializer
from erecord_vpz.serializers import VpzActivityDetailSerializer
from erecord_cmn.serializers import getKeyOptionValues

from erecord_vpz.compact.serializers import VpzInputCompact
from erecord_vpz.compact.serializers import VpzInputCompactSerializer

from erecord_cmn.utils.dir_and_file import save_posted_file
from erecord_cmn.utils.dir_and_file import unzip_file

from erecord_cmn.utils.urls import url_add_options

#from erecord_cmn.utils.logger import get_logger
#LOGGER = get_logger(__name__)
from erecord_cmn.utils.errors import logger_report_error
from erecord_cmn.utils.errors import build_error_message
from erecord_cmn.utils.errors import get_error_status
from erecord_cmn.utils.errors import Http400

#------------------------------------------------------------------------------

class OutselectOptionViewMixin(object):
    """additional methods for views having outselect option
    
    The 'outselect' option is used to choose restituted output datas.
 
    Value "all" : to select all output datas of all views
    Value viewname : to select all output datas of the view named viewname
    Value vname.oname : to select the ouput data oname of the view named \
    vname (and unselect the out datas of this view that are not selected \
    like this)

    Attention :
    The selection of type vname.oname is available in 'dataframe' restype \
    case. It won't be completely applied in 'matrix' restype case, even if it \
    has an impact on the view named vname.
    """

    def get_outselect_option_values(self, data):
        """Option 'outselect' (several values are possible)
    
        Returns the list of outselect option values.
    
        The 'outselect' option is used to choose restituted output datas.
        'outsel' is also accepted
        """

        s = list(set(getKeyOptionValues(data=data, key='outselect')) | 
                set(getKeyOptionValues(data=data, key='outsel')))
        outselect_option = None
        if len(s) > 0:
            outselect_option = s
        return outselect_option

class ParselectOptionViewMixin(object):
    """additional methods for views having parselect option
    
    The 'parselect' option is used to choose restituted parameters.
 
    Value "all" : to select all parameters of all conditions
    Value condname : to select all parameters of the condition named condname
    Value cname.pname : to select the parameter pname of the condition named \
    cname (and unselect the parameters of this condition that are not \
    selected like this)
    """

    def get_parselect_option_values(self, data):
        """Option 'parselect' (several values are possible)
    
        Returns the list of parselect option values.
    
        The 'parselect' option is used to choose restituted parameters.
        'parsel' is also accepted
        """

        s = list(set(getKeyOptionValues(data=data, key='parselect')) | 
                set(getKeyOptionValues(data=data, key='parsel')))
        parselect_option = None
        if len(s) > 0:
            parselect_option = s
        return parselect_option

class VpzInputCompactViewMixin(OutselectOptionViewMixin,
         ParselectOptionViewMixin, VleParCompactOptionViewMixin):
    """additional methods for views having options to modify VpzInput """

    def getVpzInputCompactOptionValues(self, data):
        """Options to modify VpzInput
        
        Returns (cr_ok, vpzinputcompact, \
        outselect_option, pars, parselect_option) \
        that are relative to VpzInputCompactSerializer, \
        get_outselect_option_values, get_vleparcompact_option_values and \
        get_parselect_option_values.
        """

        vpzinputcompact = VpzInputCompact()
        serializer = VpzInputCompactSerializer(instance=vpzinputcompact,
                                               data=data)
        cr_ok = serializer.is_valid()
        if cr_ok :
            serializer.save()
            outselect_option = self.get_outselect_option_values(data=data)
            vleparcompact_option = self.get_vleparcompact_option_values(
                    data=data)
            parselect_option = self.get_parselect_option_values(data=data)
        else :
            begin_value = None
            duration_value = None
            outselect_option = None
            vleparcompact_option = None
            parselect_option = None
        return (cr_ok, serializer, vpzinputcompact,
                outselect_option, vleparcompact_option, parselect_option)

class DataFolderViewMixin(DataFolderCopyViewMixin):
    """additional methods for views where 'data' folder can be modified
    
    The 'data' folder of the main vle package can be modified.

    Option datafolder : zip file containing the new 'data' folder to be taken
    into account. Something else than the 'data' folder that may be contained
    into the datafolder zip file won't be considered. 

    Option datafoldercopy : the way how to take into account datafolder.
    - Value replace : the content of (the 'data' folder contained into) the
      datafolder zip file replaces the original 'data' folder.
    - Value overwrite : the content of (the 'data' folder contained into) the
      datafolder zip file is added to the original 'data' folder (by
      overwriting).
    """

    def update_datafolder(self, LOGGER, data, vpzact) :
        """updates the 'data' folder

        Updates the 'data' folder of the main vle package according to the
        datafolder option (zip file containing 'data' folder) and the
        datafoldercopy option (replace or overwrite).

        datafolder is supposed to contain a zip file.
        datafolder values ignored : "", None.
        else : raise error
        """

        if 'datafolder' in data.keys() :

            if data['datafolder'] == "":
                return
            if data['datafolder'] is None :
                return

            cr_ok = False # default
            w = vpzact.vpzworkspace
            w.clean_datahome()
            datafolder_zip_file_path = w.get_datafolder_zip_file_path()
            try :
                cr_ok = save_posted_file(data=data, key='datafolder',
                                         filepath=datafolder_zip_file_path)
            except :
                raise
            if not cr_ok :
                errormsg = "Unable to satisfy the request"
                raise Http400(errormsg)
            try :
                cr_ok = unzip_file(zip_file_path=datafolder_zip_file_path,
                                   path=w.get_datahome())
            except :
                raise
            try :
                datafoldercopy = self.get_datafoldercopy_value(data=data)
                self.modify_datafolder(datafoldercopy=datafoldercopy,
                                       vpzact=vpzact)
            except :
                raise

    def modify_datafolder(self, datafoldercopy, vpzact):
        """updates the 'data' folder

        Updates the 'data' folder of the main vle package according to the
        datafoldercopy option and the 'data' folder found under datahome.

        Raise error if no 'data' folder found under datahome.
        """

        w = vpzact.vpzworkspace
        (cr_ok, datafolder_src_path, data_name) = \
                                       w.get_and_verify_datafolder_src_path()
        if not cr_ok :
            errormsg = "Bad request : folder named '"+data_name+"' not found "
            errormsg += "into the zip file received by the datafolder option. "
            errormsg += "This zip file must contain a folder "
            errormsg += "named '"+data_name+"' that contains all the data "
            errormsg += "files to be taken into account."
            raise Http400(errormsg)
        try :
            if datafoldercopy == 'overwrite' :
                w.overwrite_pkg_data(data_src=datafolder_src_path,
                                     pkgname=vpzact.pkgname)
            else : # 'replace', default
                w.replace_pkg_data(data_src=datafolder_src_path,
                                   pkgname=vpzact.pkgname)
        except :
            raise

class ActivityViewMixin(LimitedAccessViewMixin, RenderViewMixin,
         ErrorViewMixin, VpzInputCompactViewMixin, ModeOptionViewMixin):
    """additional methods for activities views"""

    def get_renderers( self ):
        r = super(ActivityViewMixin, self).get_renderers()
        renderer = TemplateHTMLRenderer()
        renderer.template_name='erecord_cmn/headedform_under_construction.html'
        r.append( renderer )
        return r

    def getVpzActivityDetailOptionValues(self, data):
        """Options to call a vpz activity (relative to a VpzAct)
    
        returns a dict"""
    
        #... et 'outselect' 'parselect' ?

        options = dict()
        p = VpzActivityDetailSerializer(data=data)
        p.is_valid()
        for k,v in p.data.iteritems() :
            if v is not None :
                options[k] = v
        mode_options = self.get_mode_option_values(data=data)
        if mode_options is not None :
            options['mode'] = mode_options
        report_option = self.get_report_option_values(data=data)
        if report_option is not None :
            options['report'] = report_option
        return options


    def init_activity(self, LOGGER, data):
        """initial build
        
        Builds VpzAct, VpzOrigin, VpzWorspace, VpzInput (initial state)

        output_plugin_choice that is used for VleView always values 'storage'.

        Receives and controls token in case of locked vpz (vpz_path or vlevpz).

        Returns VpzAct if no error, and raises exception else.
        """

        output_plugin_choice='storage'

        p = VpzSelectionOptionsSerializer(data=data)
        p.is_valid()
        vpzpath_id = p.data['vpzpath']
        vlevpz_id = p.data['vpz']
        if vpzpath_id is not None :
            try:
                source = VpzPath.objects.get(pk=vpzpath_id)
            except VpzPath.DoesNotExist:
                errormsg = "Vpz path not found : VpzPath with id value = "+ \
                        str(vpzpath_id)+" does not exist " + \
                        "(option vpzpath="+str(vpzpath_id)+")"
                raise Http404(errormsg)
        elif vlevpz_id is not None :
            try:
                source = VleVpz.objects.get(pk=vlevpz_id)
            except VleVpz.DoesNotExist:
                errormsg = "Vpz file not found : VleVpz with id value = "+ \
                        str(vlevpz_id)+" does not exist " + \
                        "(option vpz="+str(vlevpz_id)+")"
                raise Http404(errormsg)
        else :
            errormsg = "Bad request : option vpz or vpzpath required"
            raise Http400(errormsg)

        # since all the activities call init_activity (even Experiment),
        # jwt control is done there where there is all the needed information  
        has_lock = source.has_lock()
        access_ok = True # default
        user_id = None
        if has_lock: # limited source case
            access_ok = False # before control
            try : # try to unlock source
                user_id = self.successful_unlocking(model=source, data=data,
                                                    LOGGER=LOGGER)
                access_ok = True # since no exception sent
            except : # exception raised in particular if unsuccessful unlocking
                raise

        if access_ok :
            try:
                d = source.for_vpzact_creation()
                vpzact = VpzAct(vpzname=d['vpzname'], pkgname=d['pkgname'], 
                        vlepath=d['vlepath'])
                vpzact.full_clean()
                vpzact.save()
                if has_lock:
                    ActivityLock.create(vpzact, user_id) #lock = ...

            except Exception as e :
                errormsg = build_error_message(error=e,
                               msg="Unable to satisfy the request")
                logger_report_error(LOGGER)
                raise Exception(errormsg)
    
            vpzorigin = VpzOrigin.create(vpzact=vpzact, text=d['textorigin'])
            vpzorigin.full_clean()
            vpzorigin.save()
        
            # vlehome, reporthome and datahome subworkspaces are created
            try :
                vpzworkspace = VpzWorkspace.create(vpzact=vpzact,
                        as_vlehome=True, as_reporthome=True, as_datahome=True)
                vlepackage_list = vpzact.get_ordered_vlepackage_list()
                vpzworkspace.build_pkg_and_its_dependencies(vpzname=vpzact.vpzname,
                    vlepackage_list=vlepackage_list)
                vpzworkspace.full_clean()
                vpzworkspace.save()
            except Exception as e :
                msg = "Unable to satisfy the request"
                errormsg = build_error_message(error=e, msg=msg)
                logger_report_error(LOGGER)
                raise Exception(errormsg)
    
            try :
                vpzinput = VpzInput.create(vpzact=vpzact)
                vpzinput.full_clean()
                vpzinput.save()
                vpzinput.read_and_add_content()
                vpzinput.update_vleview_vleout_and_save(
                            output_plugin_choice=output_plugin_choice)
            except Exception as e :
                msg = "Unable to satisfy the request"
                errormsg = build_error_message(error=e, msg=msg)
                logger_report_error(LOGGER)
                raise Exception(errormsg)

            return vpzact

    def modify_input(self, LOGGER, data, vpzact):
        """Modifies vpzact.vpzinput according to data.

        Updates VpzInput according to data.

        data information can correspond to VpzInputCompactSerializer format,
        where parameters are given in 'pars'. Each parameter 
        ('cname', 'pname', 'value') can also be given as the data 
        'pname'.'cname' with value 'value'. If a parameter value was given 
        in both ways, there is no guarantee about which one would be kept.

        data information can also contain the 'outselect' option that is used
        to select the restituted output datas (see OutselectOptionViewMixin :
        all, view, output data...).

        data information can also contain the 'parselect' option that is used
        to select the restituted parameters (see ParselectOptionViewMixin :
        all, cond, parameter...).

        output_plugin_choice that is used for VleView always values 'storage'.

        Returns VpzAct if no error, and raises exception else.
        """

        output_plugin_choice = 'storage'

        vpzinput = vpzact.vpzinput

        (cr_tmp, serializer, vpzinputcompact,
         outselect_option, vleparcompact_option,
         parselect_option) = self.getVpzInputCompactOptionValues(data=data)
        if cr_tmp :
            serializer.save_obj(vpzinputcompact=vpzinputcompact,
                                vpzinput=vpzinput)
            vpzinput.update_vleview_vleout_and_save(
                    outselect=outselect_option,
                    output_plugin_choice=output_plugin_choice)
            vpzinput.update_vlepar_values_and_save(
                    vleparcompact_list=vleparcompact_option)
            vpzinput.update_vlepar_selected_and_save(
                    parselect=parselect_option)

            try :
                vpzinput.vle_format_control()
            except Exception as e :
                msg = "Unable to satisfy the request"
                errormsg = build_error_message(error=e, msg=msg)
                logger_report_error(LOGGER)
                raise Exception(errormsg)
        else :
            errormsg = "Bad request : " + str(serializer.errors)
            raise Http400(errormsg)

        return vpzact

    def simulate(self, LOGGER, plan, restype, vpzact):
        """Runs simulation according to plan and restype.
        
        Runs vpzact.vpzinput and builds vpzact.vpzoutput.

        Returns VpzAct if no error, and raises exception else.
        """

        vpzoutput = VpzOutput.create(vpzact=vpzact)
        vpzoutput.full_clean()
        vpzoutput.save()
        try :
            vpzoutput.run_and_add_content(plan=plan, restype=restype)
            vpzoutput.full_clean()
            vpzoutput.save()
        except Exception as e :
            msg = "Unable to satisfy the request"
            errormsg = build_error_message(error=e, msg=msg)
            logger_report_error(LOGGER)
            raise Exception(errormsg)
        return vpzact

    def url_pk_options_redirection(self, request, data, url_pk, pk):
        """defines and returns the redirection url to url_pk
        
        With attribute pk.
        Options are kept (see getVpzActivityDetailOptionValues).
        """

        # old calling example :
        # pk = vpzact.vpzoutput.id
        # url_pk = 'erecord_vpz-vpzoutput-detail'
        # new_url = self.url_pk_options_redirection(request, data, url_pk, pk)
        # return redirect(new_url)

        options = request.resolver_match.kwargs
        options.update(self.getVpzActivityDetailOptionValues(data=data))
        new_url = reverse(url_pk, args=(pk,))
        if options :
            new_url = url_add_options(new_url, options)
        return new_url

class InputViewMixin(ActivityViewMixin, LimitedAccessVpzInputViewMixin):
    """additional methods for activities views about input of a vpz"""

    def action_input_get(self, LOGGER, data):
        """action done for a GET request about input of a vpz"""

        try :
            res = self.init_activity(LOGGER=LOGGER, data=data)
        except :
            raise
        vpzact = res
        return vpzact

    def action_input_post(self, LOGGER, data):
        """action done for a POST request about input of a vpz"""

        try :
            res = self.init_activity(LOGGER=LOGGER, data=data)
        except :
            raise
        vpzact = res
        try :
            res = self.modify_input(LOGGER=LOGGER, data=data, vpzact=vpzact)
        except :
            raise
        vpzact = res
        return vpzact

class OutputViewMixin(DataFolderViewMixin, ActivityViewMixin):
    """additional methods for activities views about output of a vpz"""

    def action_output_get(self, LOGGER, data):
        """action done for a GET request about output of a vpz

        The simulation running depends on the 'mode' option that data \
        information may contain, including plan and restype information.

        """

        try : 
            res = self.init_activity(LOGGER=LOGGER, data=data)
        except :
            raise
        vpzact = res
        try : 
            plan = self.get_plan_value(data)
            restype = self.get_restype_value(data)
            res = self.simulate(LOGGER=LOGGER, plan=plan, restype=restype,
                                vpzact=vpzact)
        except :
            raise
        vpzact = res
        return vpzact

    def action_output_post(self, LOGGER, data):
        """action done for a POST request about output of a vpz

        The simulation running depends on the 'mode' option that data \
        information may contain, including plan and restype information.

        """

        try :
            res = self.init_activity(LOGGER=LOGGER, data=data)
        except :
            raise
        vpzact = res
        try :
            res = self.modify_input(LOGGER=LOGGER, data=data, vpzact=vpzact)
        except :
            raise
        vpzact = res
        try :
            self.update_datafolder(LOGGER=LOGGER, data=data, vpzact=vpzact)
        except :
            raise
        try :
            plan = self.get_plan_value(data)
            restype = self.get_restype_value(data)
            res = self.simulate(LOGGER=LOGGER, plan=plan, restype=restype,
                                vpzact=vpzact)
        except :
            raise
        vpzact = res
        return vpzact

