# -*- coding: UTF-8 -*-
"""erecord_vpz.views.views_mixins

Mixins common to erecord_vpz application views

"""

from erecord_cmn.utils.errors import Http400

from erecord_cmn.views_mixins import ModeOptionViewMixin
from erecord_cmn.views_mixins import RenderViewMixin
from erecord_vpz.views.activities_mixins import DataFolderViewMixin

from erecord_cmn.serializers import getKeyOptionValues

from erecord_cmn.configs.config import ACTIVITY_PERSISTENCE

import os

from rest_framework.response import Response

from rest_framework.renderers import TemplateHTMLRenderer
#from erecord_cmn.renderers import ZIPRenderer
#from erecord_cmn.renderers import PlainTextRenderer

from erecord_cmn.utils.dir_and_file import make_zip_folder
from erecord_cmn.utils.dir_and_file import create_dirs_if_absent
from erecord_cmn.utils.dir_and_file import save_posted_file
from erecord_slm.views_mixins import VpzDownloadViewMixin

#from erecord_cmn.utils.logger import get_logger
#LOGGER = get_logger(__name__)
from erecord_cmn.utils.errors import logger_report_error
from erecord_cmn.utils.errors import build_error_message

class PersistenceViewMixin(object):
    """additional methods for persistence management
    
    The view must be in relation with a VpzAct.

    A VzpAct (and its dependencies/tree) may be deleted after restitution,
    depending on persistence management.

    """
    @classmethod
    def get_persistence_mode(cls):
        return ACTIVITY_PERSISTENCE

    def manages_persistence(self, vpzact):
        if not self.get_persistence_mode() :
            vpzact.delete()

class ReportViewMixin(VpzDownloadViewMixin, RenderViewMixin):
    """additional methods for views producing and returning reports
    
    The 'report' option is used to choose what kind of report is \
    produced/returned about vpzact.
 
    @include erecord_vpz/docs/report_option.txt
    (for more information, see the online documentation, main page) 
    """

    def get_renderers( self ):
        r = RenderViewMixin.get_renderers(self)
        renderer = TemplateHTMLRenderer()
        renderer.template_name='erecord_slm/headedform_downloading.html'
        r.append( renderer )
        return r

    def get_report_values(self, data):
        """Defines the list of report values deduced from 'report' option \
        (several values are possible).

        The resulting reports may content several values for the kinds of \
        report to be built and returned into the folder.

        Priority to 'all' value.
        Default values ['vpz', 'xls',]
        """

        available_values = ['vpz', 'csv', 'txt', 'xls',] # + special case 'all'
        
        s = getKeyOptionValues(data=data, key='report')
        report_option = list()
        if len(s) > 0:
            report_option = s

        report_option = list(set(report_option)) # unicity

        if 'all' in report_option :
            reports = [o for o in available_values] # all
        else :
            reports = list()
            if report_option :
                reports = [o for o in report_option if o in available_values]
            if not reports : # default
                reports = ['vpz', 'xls',]
        return reports

    def build_zip_folder(self, vpzact):
        """builds the zip file of the report folder, and returns its path. """

        zip_path = make_zip_folder(self.get_folder_path(vpzact=vpzact))
        return zip_path

    def get_folder_path(self, vpzact) :
        """returns the report folder path name"""
        name = '__' + str(vpzact.id) + '__input__'
        return vpzact.vpzworkspace.get_report_folder_path(name)

    def build_folder(self, vpzact, reports, bycol) :
        """builds content of the report folder depending on reports

        The content is relative to input and output information.
        """

        folderpath = self.get_folder_path(vpzact)

        if 'vpz' in reports:
            path = os.path.join(folderpath, 'exp')
            create_dirs_if_absent([folderpath, path,])
            vpzact.make_folder_vpz(dirpath=path)

        if 'csv' in reports or 'xls' in reports or 'txt' in reports:

            vpzact.prepare_reports(with_general_output=True,
                       default_general_output=False,
                       with_out_ident_and_nicknames=True, with_res=True,
                       with_bycol=bycol)

            if 'csv' in reports:
                path = os.path.join(folderpath, 'output')
                create_dirs_if_absent([folderpath, path,])
                vpzact.make_folder_csv_ident(dirpath=folderpath)
                vpzact.make_folder_csv_cond(dirpath=folderpath, bycol=bycol)
                vpzact.make_folder_csv_output(dirpath=path)
            if 'xls' in reports:
                path = folderpath
                create_dirs_if_absent([folderpath, path,])
                vpzact.make_folder_xls(dirpath=path, bycol=bycol)
            if 'txt' in reports:
                path = folderpath
                create_dirs_if_absent([folderpath, path,])
                vpzact.make_folder_txt(dirpath=path)

    def build_conditions_folder(self, vpzact, bycol) :
        """builds content of the simulation conditions report folder

        The content is relative to input information.
        """

        folderpath = self.get_folder_path(vpzact)
        vpzact.prepare_reports(with_general_output=False,
                   default_general_output=False,
                   with_out_ident_and_nicknames=False, with_res=False,
                   with_bycol=bycol)

        path = folderpath
        create_dirs_if_absent([folderpath, path,])
        vpzact.make_conditions_folder_xls(dirpath=path, bycol=bycol)

class ExperimentViewMixin(DataFolderViewMixin, ReportViewMixin):
    """additional methods for views about experiment
    
    The experiment information is into a xls file.
    """

    def getExperimentValues(self, data, vpzact) :
        """experiment values (conditions)

        The experiment information comes from the xls experiment file that \
        is sent in POST data as key 'experimentfile'. It is saved (into \
        workspace) as svg_experiment_file file, that is then read to extract \
        the returned information (see read_experiment_file_xls).
        """

        svg_experiment_file = vpzact.vpzworkspace.get_experiment_file_path()
        cr_ok = save_posted_file(data=data, key='experimentfile',
                                 filepath=svg_experiment_file)
        if cr_ok :
            e = vpzact.read_experiment_file_xls(filepath=svg_experiment_file)
        else :
            e = (False, None, None, None, None, dict(), list(), list())
        return e

    def modify_experiment(self, LOGGER, vpzact,
                           begin_value=None, duration_value=None,
                           parameters=None,
                           parselect_values=None, outselect_values=None) :
        """Modifies vpzact.vpzinput according to inputs.

        parameters is a dict (selection_name as key, value as value).

        outselect_values : list of the values of the 'outselect' option that \
        is used to select the restituted output datas. 

        parselect_values : list of the values of the 'parselect' option that \
        is used to select the restituted parameters.

        output_plugin_choice that is used for VleView always values 'storage'.

        Returns VpzAct if no error, and raises exception else.
        """

        output_plugin_choice = 'storage'
        vpzinput = vpzact.vpzinput

        vpzinput.update_vlebegin_vleduration_and_save(begin_value,
                                                      duration_value)
        if parameters is not None :
            vleparcompact_option = self.get_vleparcompact_option_values(
                        data=parameters)
            vpzinput.update_vlepar_values_and_save(
                         vleparcompact_list=vleparcompact_option)
        vpzinput.update_vlepar_selected_and_save(parselect=parselect_values)
        vpzinput.update_vleview_vleout_and_save(outselect=outselect_values,
                     output_plugin_choice=output_plugin_choice)
        try :
            vpzinput.vle_format_control()
        except Exception as e :
            msg = "Unable to satisfy the request"
            errormsg = build_error_message(error=e, msg=msg)
            logger_report_error(LOGGER)
            raise Exception(errormsg)
        return vpzact

    def action_experiment_post(self, LOGGER, data):
        """action done for a POST request about experiment of a vpz"""
        
        try :
            res = self.init_activity(LOGGER=LOGGER, data=data)
        except :
            raise
        vpzact = res
        try :
            (cr_tmp, restype_value, plan_value, begin_value, duration_value,
             parameters, parselect_values, outselect_values) = \
            self.getExperimentValues(data=data, vpzact=vpzact)
        except :
            raise
        if not cr_tmp :
            errormsg = "Unable to satisfy the request"
            raise Http400(errormsg)
        try :
            res = self.modify_experiment(LOGGER=LOGGER, vpzact=vpzact,
                           begin_value=begin_value,
                           duration_value=duration_value,
                           parameters=parameters,
                           parselect_values=parselect_values,
                           outselect_values=outselect_values)
        except :
            raise
        vpzact = res
        try :
            self.update_datafolder(LOGGER=LOGGER, data=data, vpzact=vpzact)
        except :
            raise
        try :
            res = self.simulate(LOGGER=LOGGER, plan=plan_value,
                                restype=restype_value, vpzact=vpzact)
        except :
            raise
        vpzact = res
        return vpzact

    def action_experiment_get(self, LOGGER, data):
        """action done for a GET request about experiment of a vpz"""

        try :
            res = self.init_activity(LOGGER=LOGGER, data=data)
        except :
            raise
        vpzact = res
        try :
            res = self.modify_experiment(LOGGER=LOGGER, vpzact=vpzact,
                           parselect_values="all", outselect_values="all")
        except :
            raise
        vpzact = res
        return vpzact

    def build_experiment_out_folder(self, vpzact) :
        """builds content of the report file (into a folder) about experiment

        The content is relative to input and output information.
        """

        folderpath = self.get_folder_path(vpzact)
        vpzact.prepare_reports(with_general_output=True,
                   default_general_output=False,
                   with_out_ident_and_nicknames=True, with_res=True,
                   with_bycol=False)

        path = folderpath
        create_dirs_if_absent([folderpath, path,])
        vpzact.make_experiment_out_file_xls(dirpath=path)

    def build_experiment_in_folder(self, vpzact) :
        """builds content of the report file (into a folder) about experiment

        The content is relative to input information.
        """

        folderpath = self.get_folder_path(vpzact)
        vpzact.prepare_reports(with_general_output=True,
                   default_general_output=True,
                   with_out_ident_and_nicknames=True, with_res=False,
                   with_bycol=False)
        path = folderpath
        create_dirs_if_absent([folderpath, path,])
        vpzact.make_experiment_in_file_xls(dirpath=path)

