# -*- coding: utf-8 -*-
"""repositories

*This package is part of erecord - Record platform web development*

:copyright: Copyright (C) 2014-2015 INRA http://www.inra.fr.
:license: GPLv3, see LICENSE for more details.
:authors: see AUTHORS.

Repositories of the erecord project

The repositories directory is dedicated to the vle repositories.  

See :ref:`index`

"""
