# -*- coding: utf-8 -*-
"""databases

*This package is part of erecord - Record platform web development*

:copyright: Copyright (C) 2014-2015 INRA http://www.inra.fr.
:license: GPLv3, see LICENSE for more details.
:authors: see AUTHORS.

Databases of the erecord project

See :ref:`index`

"""
