# -*- coding: utf-8 -*-
"""erecord_vpz.forms

Forms of the erecord_vpz application

"""

from django import forms
from django.core.exceptions import ValidationError

from erecord_cmn.forms import ReadonlyForm

from erecord_vpz.models import VpzAct
from erecord_vpz.models import VpzOrigin
from erecord_vpz.models import VpzInput
from erecord_vpz.models import VpzOutput
from erecord_vpz.models import VpzPath
from erecord_vpz.models import VleBegin
from erecord_vpz.models import VleDuration
from erecord_vpz.models import VleCond
from erecord_vpz.models import VlePar
from erecord_vpz.models import VleView
from erecord_vpz.models import VleOut
from erecord_vpz.models import VpzWorkspace

import os

from erecord_cmn.configs.config import REPOSITORIES_HOME
from erecord_cmn.utils.vle import is_structured_as_vle_home
from erecord_cmn.utils.vle import is_pkg_of_rep
from erecord_cmn.utils.vle import is_vpz_of_pkg_of_rep
from erecord_cmn.utils.vle import get_vlepath_pkg_name

from erecord_vpz.models import ht_vpzpath_vpzpath as mht_vpzpath_vpzpath


class VpzActForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(VpzActForm, self).__init__(*args, **kwargs)
        for (n,f) in self.fields.iteritems() :
            f.widget=forms.Textarea(attrs={'rows':1, 'cols':100})
    class Meta:
        model = VpzAct
        fields = '__all__'

ht_vpzpath_vpzpath = mht_vpzpath_vpzpath+ \
    " Choose a vpz file among the existing ones (under "+REPOSITORIES_HOME+")"

empty_choice = ("","---------")

def get_vpzpath_choices() :
    """returns the list to choose a vpz file name.
    
    The vpz files taken into account are those found under one of the
    existing (installed) models repositories 
    """

    choices_list = [ empty_choice ]
    tmp = forms.FilePathField(path=REPOSITORIES_HOME, recursive=True,
            allow_files=True, allow_folders=False, match=".*\.vpz$")

    for (v,c) in tmp.choices :
        e = get_vlepath_pkg_name(vpzpath=v, limit=REPOSITORIES_HOME )
        if e is not None :
            (vlepath,pkg,name) = e
            if is_structured_as_vle_home(vlepath) :
                if is_pkg_of_rep(rep_path=vlepath, pkgname=pkg) :
                    if is_vpz_of_pkg_of_rep(rep_path=vlepath,
                            pkgname=pkg, vpzname=name) :
                        #choices_list.append( (v,v) )
                        choices_list.append( (v,c) )
    return choices_list


class VpzPathForm(forms.ModelForm):

    vpzpath = forms.ChoiceField( choices=get_vpzpath_choices(), 
            required=True,
            help_text=ht_vpzpath_vpzpath)

    class Meta:
        model = VpzPath
        fields = '__all__'

#------------------------------------------------------------------------------

class VpzActUserForm(ReadonlyForm):
    class Meta:
        model = VpzAct
        fields = '__all__'

class VpzInputUserForm(ReadonlyForm):
    class Meta:
        model = VpzInput
        fields = '__all__'

class VpzOutputUserForm(ReadonlyForm):
    class Meta:
        model = VpzOutput
        fields = '__all__'

class VpzOriginUserForm(ReadonlyForm):
    class Meta:
        model = VpzOrigin
        fields = '__all__'

class VpzPathUserForm(ReadonlyForm):
    class Meta:
        model = VpzPath
        fields = '__all__'


class VleBeginUserForm(ReadonlyForm):
    class Meta:
        model = VleBegin
        fields= [ 'verbose_name', 'value', ] #'vpzinput', 'id',]

class VleDurationUserForm(ReadonlyForm):
    class Meta:
        model = VleDuration
        fields= [ 'verbose_name', 'value', ] #'vpzinput', 'id',]

class VleCondUserForm(ReadonlyForm):
    class Meta:
        model = VleCond
        fields= [ 'verbose_name', 'name',] # 'vpzinput', 'id',]

class VleParUserForm(ReadonlyForm):
    class Meta:
        model = VlePar
        fields= [ 'verbose_name', 'value', 'pname', 'cname',] # 'vlecond', 'id',]

class VleViewUserForm(ReadonlyForm):
    class Meta:
        model = VleView
        fields= ['verbose_name', 'name', 'type', 'timestep', 
                'output_name', 'output_plugin', 'output_format', 
                'output_location',] # 'vpzinput', 'id',]

class VleOutUserForm(ReadonlyForm):
    class Meta:
        model = VleOut
        fields= ['verbose_name', 'shortname', 'oname', 'vname',] # 'vleview', 'id',]

class VpzWorkspaceUserForm(ReadonlyForm):
    class Meta:
        model = VpzWorkspace
        fields = '__all__'

