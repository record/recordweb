# -*- coding: UTF-8 -*-
"""erecord_vpz.views.views_mixins

Mixins common to erecord_vpz application views

"""

from erecord_cmn.views_mixins import ModeOptionViewMixin
from erecord_cmn.views_mixins import RenderViewMixin

from erecord_cmn.serializers import getKeyOptionValues

from erecord_cmn.configs.config import ACTIVITY_PERSISTENCE

import os

from rest_framework.response import Response

from rest_framework.renderers import TemplateHTMLRenderer
from erecord_cmn.renderers import ZIPRenderer
from erecord_cmn.renderers import PlainTextRenderer

from django.http import HttpResponse
from django.core.servers.basehttp import FileWrapper
from erecord_cmn.utils.dir_and_file import make_zip_folder
from erecord_cmn.utils.dir_and_file import create_dirs_if_absent
from erecord_slm.views_mixins import VpzDownloadViewMixin

#from erecord_cmn.utils.logger import get_logger
#LOGGER = get_logger(__name__)
from erecord_cmn.utils.errors import logger_report_error
from erecord_cmn.utils.errors import build_error_message

class PersistenceViewMixin(object):
    """additional methods for persistence management
    
    The view must be in relation with a VpzAct.

    A VzpAct (and its dependencies/tree) may be deleted after restitution,
    depending on persistence management.

    """
    @classmethod
    def get_persistence_mode(cls):
        return ACTIVITY_PERSISTENCE

    def manages_persistence(self, vpzact):
        if not self.get_persistence_mode() :
            vpzact.delete()

class ReportViewMixin(VpzDownloadViewMixin, RenderViewMixin):
    """additional methods for views producing and returning reports
    
    The 'report' option is used to choose what kind of report is \
    produced/returned about vpzact.
 
    @include erecord_vpz/docs/report_option.txt
    (for more information, see the online documentation, main page) 
    """

    def get_renderers( self ):
        r = RenderViewMixin.get_renderers(self)
        renderer = TemplateHTMLRenderer()
        renderer.template_name='erecord_slm/headedform_downloading.html'
        r.append( renderer )
        return r

    def get_report_values(self, data):
        """Defines the list of report values deduced from 'report' option \
        (several values are possible).

        The resulting reports may content several values for the kinds of \
        report to be built and returned into the folder.

        Priority to 'all' value.
        Default values ['vpz', 'xls',]
        """

        available_values = ['vpz', 'csv', 'txt', 'xls',] # + special case 'all'
        
        s = getKeyOptionValues(data=data, key='report')
        report_option = list()
        if len(s) > 0:
            report_option = s

        report_option = list(set(report_option)) # unicity

        if 'all' in report_option :
            reports = [o for o in available_values] # all
        else :
            reports = list()
            if report_option :
                reports = [o for o in report_option if o in available_values]
            if not reports : # default
                reports = ['vpz', 'xls',]
        return reports

    def zip_response(self, zip_path):
        response = HttpResponse(FileWrapper(file(zip_path,'rb')),
                         content_type='application/zip')
        content_disposition = "attachment; filename="+zip_path+"'"
        response['Content-Disposition'] = content_disposition
        return response

    def download_response(self, url, label=None):
        if url:
            if not label :
                label = "The downloading url"
            context = label + " : " + url
        else :
            context = "No downloadable result"
        return Response(context)

    def build_zip_folder(self, vpzact):
        """builds the zip file of the report folder, and returns its path. """

        zip_path = make_zip_folder(self.get_folder_path(vpzact=vpzact))
        return zip_path

    def get_folder_path(self, vpzact) :
        """returns the report folder path name"""
        name = '__' + str(vpzact.id) + '__input__'
        return vpzact.vpzworkspace.get_report_folder_path(name)

    def build_folder(self, vpzact, reports) :
        """builds content of the report folder depending on reports

        The content is relative to input and output information.
        """

        folderpath = self.get_folder_path(vpzact)

        if 'vpz' in reports:
            path = os.path.join(folderpath, 'exp')
            create_dirs_if_absent([folderpath, path,])
            vpzact.make_folder_vpz(dirpath=path)

        if 'csv' in reports or 'xls' in reports or 'txt' in reports:

            vpzact.prepare_reports()
            if 'csv' in reports:
                path = os.path.join(folderpath, 'output')
                create_dirs_if_absent([folderpath, path,])
                vpzact.make_folder_csv_ident(dirpath=folderpath)
                vpzact.make_folder_csv_cond(dirpath=folderpath)
                vpzact.make_folder_csv_output(dirpath=path)
            if 'xls' in reports:
                path = folderpath
                create_dirs_if_absent([folderpath, path,])
                vpzact.make_folder_xls(dirpath=path)
            if 'txt' in reports:
                path = folderpath
                create_dirs_if_absent([folderpath, path,])
                vpzact.make_folder_txt(dirpath=path)

    def build_conditions_folder(self, vpzact) :
        """builds content of the simulation conditions report folder

        The content is relative to input information.
        """

        folderpath = self.get_folder_path(vpzact)
        vpzact.prepare_reports(only_conditions=True)
        path = folderpath
        create_dirs_if_absent([folderpath, path,])
        vpzact.make_conditions_folder_xls(dirpath=path)

