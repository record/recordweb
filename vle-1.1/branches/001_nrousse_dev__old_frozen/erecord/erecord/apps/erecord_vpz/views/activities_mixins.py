# -*- coding: UTF-8 -*-
"""erecord_vpz.views.activities_mixins

Mixins common to erecord_vpz application views (activities)

"""

from rest_framework.response import Response
from django.http import Http404
from rest_framework.reverse import reverse

from erecord_vpz.models import VpzAct
from erecord_vpz.models import VpzOrigin
from erecord_vpz.models import VpzInput
from erecord_vpz.models import VpzOutput
from erecord_vpz.models import VpzWorkspace
from erecord_vpz.models import VpzPath

#from erecord_cmn.configs.config import DB_NAME_DB
from erecord_db.models import VleVpz

from erecord_cmn.views_mixins import ModeOptionViewMixin
###from erecord_vpz.views.views_mixins import ReportOptionViewMixin

from erecord_cmn.views_mixins import RenderViewMixin
from rest_framework.renderers import TemplateHTMLRenderer

from erecord_vpz.compact.views_mixins import VleParCompactOptionViewMixin

from erecord_vpz.serializers import VpzSelectionOptionsSerializer
from erecord_vpz.serializers import VpzActivityDetailSerializer
from erecord_cmn.serializers import getKeyOptionValues

from erecord_vpz.compact.serializers import VpzInputCompact
from erecord_vpz.compact.serializers import VpzInputCompactSerializer

from erecord_cmn.utils.urls import url_add_options

#from erecord_cmn.utils.logger import get_logger
#LOGGER = get_logger(__name__)
from erecord_cmn.utils.errors import logger_report_error
from erecord_cmn.utils.errors import build_error_message
from erecord_cmn.utils.errors import get_error_status
from erecord_cmn.utils.errors import Http400

#------------------------------------------------------------------------------

class OutselectOptionViewMixin(object):
    """additional methods for views having outselect option
    
    The 'outselect' option is used to choose restituted output datas.
 
    Value "all" : to select all output datas of all views
    Value viewname : to select all output datas of the view named viewname
    Value vname.oname : to select the ouput data oname of the view named \
    vname (and unselect the out datas of this view that are not selected \
    like this)

    Attention :
    The selection of type vname.oname is available in 'dataframe' restype \
    case. It won't be completely applied in 'matrix' restype case, even if it \
    has an impact on the view named vname.
    """

    def get_outselect_option_values(self, data):
        """Option 'outselect' (several values are possible)
    
        Returns the list of outselect option values.
    
        The 'outselect' option is used to choose restituted output datas.
        'outsel' is also accepted
        """

        s = list(set(getKeyOptionValues(data=data, key='outselect')) | 
                set(getKeyOptionValues(data=data, key='outsel')))
        outselect_option = None
        if len(s) > 0:
            outselect_option = s
        return outselect_option

class ParselectOptionViewMixin(object):
    """additional methods for views having parselect option
    
    The 'parselect' option is used to choose restituted parameters.
 
    Value "all" : to select all parameters of all conditions
    Value condname : to select all parameters of the condition named condname
    Value cname.pname : to select the parameter pname of the condition named \
    cname (and unselect the parameters of this condition that are not \
    selected like this)
    """

    def get_parselect_option_values(self, data):
        """Option 'parselect' (several values are possible)
    
        Returns the list of parselect option values.
    
        The 'parselect' option is used to choose restituted parameters.
        'parsel' is also accepted
        """

        s = list(set(getKeyOptionValues(data=data, key='parselect')) | 
                set(getKeyOptionValues(data=data, key='parsel')))
        parselect_option = None
        if len(s) > 0:
            parselect_option = s
        return parselect_option

class VpzInputCompactViewMixin(OutselectOptionViewMixin,
        ParselectOptionViewMixin, VleParCompactOptionViewMixin):
    """additional methods for views having options to modify VpzInput """

    def getVpzInputCompactOptionValues(self, data):
        """Options to modify VpzInput
        
        Returns (cr_ok, vpzinputcompact, \
        outselect_option, pars, parselect_option) \
        that are relative to VpzInputCompactSerializer, \
        get_outselect_option_values, get_vleparcompact_option_values and \
        get_parselect_option_values.
        """

        vpzinputcompact = VpzInputCompact()
        serializer = VpzInputCompactSerializer(instance=vpzinputcompact,
                                               data=data)
        cr_ok = serializer.is_valid()
        if cr_ok :
            serializer.save()
            outselect_option = self.get_outselect_option_values(data=data)
            vleparcompact_option = self.get_vleparcompact_option_values(
                    data=data)
            parselect_option = self.get_parselect_option_values(data=data)
        else :
            begin_value = None
            duration_value = None
            outselect_option = None
            vleparcompact_option = None
            parselect_option = None
        return (cr_ok, serializer, vpzinputcompact,
                outselect_option, vleparcompact_option, parselect_option)

class ActivityViewMixin(RenderViewMixin, VpzInputCompactViewMixin,
        ModeOptionViewMixin):
        #ReportOptionViewMixin, ModeOptionViewMixin):
    """additional methods for activities views"""

    def get_renderers( self ):
        r = super(ActivityViewMixin, self).get_renderers()
        renderer = TemplateHTMLRenderer()
        renderer.template_name='erecord_cmn/headedform_under_construction.html'
        r.append( renderer )
        return r

    def getVpzActivityDetailOptionValues(self, data):
        """Options to call a vpz activity (relative to a VpzAct)
    
        returns a dict"""
    
        #... et 'outselect' 'parselect' ?

        options = dict()
        p = VpzActivityDetailSerializer(data=data)
        p.is_valid()
        for k,v in p.data.iteritems() :
            if v is not None :
                options[k] = v
        mode_options = self.get_mode_option_values(data=data)
        if mode_options is not None :
            options['mode'] = mode_options
        report_option = self.get_report_option_values(data=data)
        if report_option is not None :
            options['report'] = report_option
        return options


    def init_activity(self, LOGGER, data):
        """initial build
        
        Builds VpzAct, VpzOrigin, VpzWorspace, VpzInput (initial state)

        output_plugin_choice that is used for VleView always values 'storage'.

        Returns VpzAct if no error, and raises exception else.
        """

        output_plugin_choice='storage'

        p = VpzSelectionOptionsSerializer(data=data)
        p.is_valid()
        vpzpath_id = p.data['vpzpath']
        vlevpz_id = p.data['vpz']
        if vpzpath_id is not None :
            try:
                source = VpzPath.objects.get(pk=vpzpath_id)
            except VpzPath.DoesNotExist:
                errormsg = "Vpz path not found : VpzPath with id value = "+ \
                        str(vpzpath_id)+" does not exist " + \
                        "(option vpzpath="+str(vpzpath_id)+")"
                raise Http404(errormsg)
        elif vlevpz_id is not None :
            try:
                #source = VleVpz.objects.using(DB_NAME_DB).get(pk=vlevpz_id)
                # ???????
                source = VleVpz.objects.get(pk=vlevpz_id)
            except VleVpz.DoesNotExist:
                errormsg = "Vpz file not found : VleVpz with id value = "+ \
                        str(vlevpz_id)+" does not exist " + \
                        "(option vpz="+str(vlevpz_id)+")"
                raise Http404(errormsg)
        else :
            errormsg = "Bad request : option vpz or vpzpath required"
            raise Http400(errormsg)

        try:
            d = source.for_vpzact_creation()
            vpzact = VpzAct(vpzname=d['vpzname'], pkgname=d['pkgname'], 
                    vlepath=d['vlepath'])
            vpzact.full_clean()
            vpzact.save()
        except Exception as e :
            errormsg = build_error_message(error=e,
                           msg="Unable to satisfy the request")
            logger_report_error(LOGGER)
            raise Exception(errormsg)

        vpzorigin = VpzOrigin.create(vpzact=vpzact, text=d['textorigin'])
        vpzorigin.full_clean()
        vpzorigin.save()

        # vlehome and reporthome subworkspaces are created
        try :
            vpzworkspace = VpzWorkspace.create(vpzact=vpzact,
                    as_vlehome=True, as_reporthome=True)
            vlepackage_list = vpzact.get_ordered_vlepackage_list() #!!! a deplacer ?
            vpzworkspace.build_pkg_and_its_dependencies(vpzname=vpzact.vpzname,
                vlepackage_list=vlepackage_list) #!!! a deplacer ?
            vpzworkspace.full_clean()
            vpzworkspace.save()
        except Exception as e :
            msg = "Unable to satisfy the request"
            errormsg = build_error_message(error=e, msg=msg)
            logger_report_error(LOGGER)
            raise Exception(errormsg)

        try :
            vpzinput = VpzInput.create(vpzact=vpzact)
            vpzinput.full_clean()
            vpzinput.save()
            vpzinput.read_and_add_content()
            vpzinput.update_vleview_vleout_and_save(
                        output_plugin_choice=output_plugin_choice)
        except Exception as e :
            msg = "Unable to satisfy the request"
            errormsg = build_error_message(error=e, msg=msg)
            logger_report_error(LOGGER)
            raise Exception(errormsg)

        return vpzact

    def modify_input(self, LOGGER, data, vpzact):
        """Modifies vpzact.vpzinput according to data.

        Updates VpzInput according to data.

        data information can correspond to VpzInputCompactSerializer format,
        where parameters are given in 'pars'. Each parameter 
        ('cname', 'pname', 'value') can also be given as the data 
        'pname'.'cname' with value 'value'. If a parameter value was given 
        in both ways, there is no guarantee about which one would be kept.

        data information can also contain the 'outselect' option that is used
        to select the restituted output datas (see OutselectOptionViewMixin :
        all, view, output data...).

        data information can also contain the 'parselect' option that is used
        to select the restituted parameters (see ParselectOptionViewMixin :
        all, cond, parameter...).

        output_plugin_choice that is used for VleView always values 'storage'.

        Returns VpzAct if no error, and raises exception else.
        """

        output_plugin_choice = 'storage'

        vpzinput = vpzact.vpzinput

        (cr_tmp, serializer, vpzinputcompact,
         outselect_option, vleparcompact_option,
         parselect_option) = self.getVpzInputCompactOptionValues(data=data)
        if cr_tmp :
            serializer.save_obj(vpzinputcompact=vpzinputcompact,
                                vpzinput=vpzinput)
            vpzinput.update_vleview_vleout_and_save(
                    outselect=outselect_option,
                    output_plugin_choice=output_plugin_choice)
            vpzinput.update_vlepar_values_and_save(
                    vleparcompact_list=vleparcompact_option)
            vpzinput.update_vlepar_selected_and_save(
                    parselect=parselect_option)

            try :
                vpzinput.vle_format_control()
            except Exception as e :
                msg = "Unable to satisfy the request"
                errormsg = build_error_message(error=e, msg=msg)
                logger_report_error(LOGGER)
                raise Exception(errormsg)
        else :
            errormsg = "Bad request : " + str(serializer.errors)
            raise Http400(errormsg)

        return vpzact

    def simulate(self, LOGGER, data, vpzact):
        """Runs simulation according to data.
        
        Runs vpzact.vpzinput and builds vpzact.vpzoutput.

        The simulation running method depends on the 'mode' option that data \
        information may contain, including plan and restype information.
        
        Returns VpzAct if no error, and raises exception else.
        """

        vpzoutput = VpzOutput.create(vpzact=vpzact)
        vpzoutput.full_clean()
        vpzoutput.save()
        try :
            vpzoutput.run_and_add_content(plan=self.get_plan_value(data),
                    restype=self.get_restype_value(data))
            vpzoutput.full_clean()
            vpzoutput.save()
        except Exception as e :
            msg = "Unable to satisfy the request"
            errormsg = build_error_message(error=e, msg=msg)
            logger_report_error(LOGGER)
            raise Exception(errormsg)
        return vpzact

    def url_pk_options_redirection(self, request, data, url_pk, pk):
        """defines and returns the redirection url to url_pk
        
        With attribute pk.
        Options are kept (see getVpzActivityDetailOptionValues).
        """

        # old calling example :
        # pk = vpzact.vpzoutput.id
        # url_pk = 'erecord_vpz-vpzoutput-detail'
        # new_url = self.url_pk_options_redirection(request, data, url_pk, pk)
        # return redirect(new_url)

        options = request.resolver_match.kwargs
        options.update(self.getVpzActivityDetailOptionValues(data=data))
        new_url = reverse(url_pk, args=(pk,))
        if options :
            new_url = url_add_options(new_url, options)
        return new_url

class InputViewMixin(ActivityViewMixin):
    """additional methods for activities views about input of a vpz"""

    def action_input_get(self, LOGGER, data):
        """action done for a GET request about input of a vpz"""

        try :
            res = self.init_activity(LOGGER=LOGGER, data=data)
        except :
            raise
        vpzact = res
        return vpzact

    def action_input_post(self, LOGGER, data):
        """action done for a POST request about input of a vpz"""

        try :
            res = self.init_activity(LOGGER=LOGGER, data=data)
        except :
            raise
        vpzact = res
        try :
            res = self.modify_input(LOGGER=LOGGER, data=data, vpzact=vpzact)
        except :
            raise
        vpzact = res
        return vpzact

class OutputViewMixin(ActivityViewMixin):
    """additional methods for activities views about output of a vpz"""

    def action_output_get(self, LOGGER, data):
        """action done for a GET request about output of a vpz """

        try : 
            res = self.init_activity(LOGGER=LOGGER, data=data)
        except :
            raise
        vpzact = res
        try : 
            res = self.simulate(LOGGER=LOGGER, data=data, vpzact=vpzact)
        except :
            raise
        vpzact = res
        return vpzact

    def action_output_post(self, LOGGER, data):
        """action done for a POST request about output of a vpz """

        try :
            res = self.init_activity(LOGGER=LOGGER, data=data)
        except :
            raise
        vpzact = res
        try :
            res = self.modify_input(LOGGER=LOGGER, data=data, vpzact=vpzact)
        except :
            raise
        vpzact = res
        try :
            res = self.simulate(LOGGER=LOGGER, data=data, vpzact=vpzact)
        except :
            raise
        vpzact = res
        return vpzact

