# -*- coding: UTF-8 -*-
"""erecord_vpz.views.activities """

from django.shortcuts import redirect

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics

from erecord_vpz.models import VpzAct
from erecord_vpz.models import VpzOrigin
from erecord_vpz.models import VpzInput
from erecord_vpz.models import VpzOutput
from erecord_vpz.models import VpzWorkspace
from erecord_vpz.models import VpzPath
from erecord_vpz.models import VleBegin
from erecord_vpz.models import VleDuration
from erecord_vpz.models import VleCond
from erecord_vpz.models import VlePar

from erecord_cmn.configs.config import DB_NAME_DB
from erecord_db.models import VleVpz

from erecord_cmn.views_mixins import DetailViewMixin
from erecord_cmn.views_mixins import StyleViewMixin
from erecord_cmn.views_mixins import FormatViewMixin
from erecord_cmn.views_mixins import DataViewMixin
from erecord_vpz.views.activities_mixins import InputViewMixin
from erecord_vpz.views.activities_mixins import OutputViewMixin
from erecord_vpz.views.views_mixins import ReportViewMixin

from erecord_vpz.views.views_mixins import PersistenceViewMixin
from erecord_vpz.views.objects_mixins import VpzInputDetailViewMixin
from erecord_vpz.views.objects_mixins import VpzOutputDetailViewMixin
from erecord_vpz.views.objects_mixins import VpzActDetailViewMixin

from erecord_vpz.forms import VpzInputUserForm
from erecord_vpz.forms import VpzOutputUserForm
from erecord_vpz.forms import VpzActUserForm

from erecord_cmn.configs.config import DOWNLOAD_LIFETIME
from erecord_slm.forms import TodownloadUrlForm

from erecord_cmn.utils.logger import get_logger
LOGGER = get_logger(__name__)
from erecord_cmn.utils.errors import logger_report_error
from erecord_cmn.utils.errors import build_error_message
from erecord_cmn.utils.errors import get_error_status

from rest_framework.negotiation import DefaultContentNegotiation

headsubtitle = "(erecord_vpz)"

class FormatContentNegotiation(FormatViewMixin, DataViewMixin,
                               DefaultContentNegotiation):
    """ ContentNegotiation taking into account format as POST parameter """

    def select_parser(self, request, parsers):
        return super(FormatContentNegotiation, self).select_parser(request,
                                                                   parsers)

    def select_renderer(self, request, renderers, format_suffix):
        data = self.data_from_request(request)
        format = self.get_format_value(data=data)
        if format is not None :
            format_suffix = format
        return super(FormatContentNegotiation, self).select_renderer(request,
                   renderers, format_suffix)

class InputView(VpzInputDetailViewMixin, PersistenceViewMixin,
                InputViewMixin,
                StyleViewMixin, FormatViewMixin, DataViewMixin,
                DetailViewMixin, generics.RetrieveAPIView):
    """input information of a vpz
    
    For more information, see the online documentation
    (:ref:`get_vpz_input`, :ref:`post_vpz_input`).
    
    Returns VpzInput information of a VpzAct after having created it and, in \
    POST command case maybe have modified some of its vpz input information.

    The VpzAct is created from a VpzPath or from a VleVpz.

    After restitution, VpzAct (and its 'dependencies/tree') may be deleted or \
    kept into the database depending on persistence management.
    """

    content_negotiation_class = FormatContentNegotiation

    def get_object(self):
        data = self.data_from_request(self.request)
        self.vpzactid = None # default
        if self.request.method == 'POST' :
            try :
                vpzact = self.action_input_post(LOGGER=LOGGER, data=data)
                self.vpzactid = vpzact.id
                vpzinput = vpzact.vpzinput
                return vpzinput
            except :
                raise
        else : # 'GET'
            try :
                vpzact = self.action_input_get(LOGGER=LOGGER, data=data)
                self.vpzactid = vpzact.id
                vpzinput = vpzact.vpzinput
                return vpzinput
            except :
                raise

    def get(self, request, format=None, **kwargs):

        data = self.request_query_params(request)
        if format is None :
            format = self.get_format_value(data=data)
        style = self.get_style_value()

        response = self.numeric_response(request=request, data=data,
                            format=format, style=style, **kwargs)

        if self.vpzactid :
            vpzact = VpzAct.objects.get(pk=self.vpzactid)
            #print "GET vpz/input, vlehome: ", vpzact.vpzworkspace.vlehome
            self.manages_persistence(vpzact=vpzact)
        return response

    def post(self, request, format=None, **kwargs):
        """Modifies VpzInput according to post data and returns it. """

        data = self.request_data(request)
        if format is None :
            format = self.get_format_value(data=data)
        style = self.get_style_value()

        response = self.numeric_response(request=request, data=data,
                            format=format, style=style, **kwargs)

        if self.vpzactid :
            vpzact = VpzAct.objects.get(pk=self.vpzactid)
            #print "POST vpz/input, vlehome: ", vpzact.vpzworkspace.vlehome
            self.manages_persistence(vpzact=vpzact)
        return response

    def numeric_response(self, request, format, style, **kwargs):
        """response about VpzInput in numeric case

        Looks like VpzInputDetail

        """

        if format=='html' :
            try :
                vpzinput = self.get_object()
                if style=='compact' or style=='compactlist' :
                    context = self.html_compact_context(vpzinput=vpzinput,
                            is_compactlist=(style=='compactlist'),
                            headsubtitle=headsubtitle)
                    response = Response(context)
                else : # style == 'link' or 'tree'
                    context = self.html_nocompact_context(vpzinput=vpzinput,
                            headsubtitle=headsubtitle)
                    response = Response(context)
            except Exception as e :
                s = get_error_status(e)
                errormsg = build_error_message(error=e)
                logger_report_error(LOGGER)
                response = Response(data={'detail':errormsg,}, status=s)

        else : # format other than html ('api' 'json' 'yaml' 'xml')

            if style=='compact' or style=='compactlist' :
                try :
                    vpzinput = self.get_object()
                    if style=='compact' :
                        context = self.nohtml_compact_context(
                                vpzinput=vpzinput)
                        response = Response(data=context)
                    elif style=='compactlist' :
                        context = self.nohtml_compactlist_context(
                                vpzinput=vpzinput)
                        response = Response(data=context)
                except Exception as e :
                    s = get_error_status(e)
                    errormsg = build_error_message(error=e)
                    logger_report_error(LOGGER)
                    response = Response(data={'detail':errormsg,}, status=s)
            else : # style == 'link' or 'tree'
                try :
                    context = super(InputView, self).get(request, **kwargs)
                    response = context
                except Exception as e :
                    s = get_error_status(e)
                    errormsg = build_error_message(error=e)
                    logger_report_error(LOGGER)
                    response = Response(data={'detail':errormsg,}, status=s)
        return response

class OutputView(VpzOutputDetailViewMixin, PersistenceViewMixin,
                OutputViewMixin,
                StyleViewMixin, FormatViewMixin, DataViewMixin,
                DetailViewMixin, generics.RetrieveAPIView):

    """output information of a vpz
    
    For more information, see the online documentation
    (:ref:`get_vpz_output`, :ref:`post_vpz_output`).
    
    Returns VpzOutput information of a VpzAct after having created it and \
    done the required operations : maybe have modified some of its vpz input \
    information (only in POST command case), have runned simulation.

    The VpzAct is created from a VpzPath or from a VleVpz.

    After restitution, VpzAct (and its 'dependencies/tree') may be deleted or \
    kept into the database depending on persistence management.
    """

    content_negotiation_class = FormatContentNegotiation

    def get_object(self):
        data = self.data_from_request(self.request)
        self.vpzactid = None # default
        if self.request.method == 'POST' :
            try :
                vpzact = self.action_output_post(LOGGER=LOGGER, data=data)
                self.vpzactid = vpzact.id
                vpzoutput = vpzact.vpzoutput
                return vpzoutput
            except :
                raise
        else : # 'GET'
            try :
                vpzact = self.action_output_get(LOGGER=LOGGER, data=data)
                self.vpzactid = vpzact.id
                vpzoutput = vpzact.vpzoutput
                return vpzoutput
            except :
                raise

    def get(self, request, format=None, **kwargs):

        """Simulates VpzInput and returns VpzOutput. """

        data = self.request_query_params(request)
        if format is None :
            format = self.get_format_value(data=data)
        style = self.get_style_value() # 'tree' or 'link' or 'compact'
        storage = self.get_storage_value(data=data)

        response = self.numeric_response(request=request, data=data,
                        format=format, style=style, storage=storage, **kwargs)

        if self.vpzactid :
            vpzact = VpzAct.objects.get(pk=self.vpzactid)
            #print "GET vpz/output, vlehome: ", vpzact.vpzworkspace.vlehome
            self.manages_persistence(vpzact=vpzact)
        return response

    def post(self, request, format=None, **kwargs):
        """
        Modifies VpzInput according to post data, simulates it and returns \
        VpzOutput.
        """

        data = self.request_data(request)
        if format is None :
            format = self.get_format_value(data=data)
        style = self.get_style_value()
        storage = self.get_storage_value(data=data)

        response = self.numeric_response(request=request, data=data,
                        format=format, style=style, storage=storage, **kwargs)
        if self.vpzactid :
            vpzact = VpzAct.objects.get(pk=self.vpzactid)
            #print "POST vpz/output, vlehome: ", vpzact.vpzworkspace.vlehome
            self.manages_persistence(vpzact=vpzact)
        return response

    def numeric_response(self, request, format, style, storage, **kwargs):
        """response about VpzOutput in numeric case

        Looks like VpzOutputDetail

        """

        if format=='html' :
            try :
                vpzoutput = self.get_object()
                context = dict()
                if (style=='compact') :
                    #title = 'Vpz output (compact)'
                    title = 'The simulation results (in compact style)'
                    context['res'] = vpzoutput.make_res_compact()
                    context['plan'] = vpzoutput.plan
                    context['restype'] = vpzoutput.restype
                else :
                    #title = 'Vpz output'
                    title = 'The simulation results'
                    form = VpzOutputUserForm(instance=vpzoutput)
                    context['form'] = form
                context['title'] = title
                context['headsubtitle'] = headsubtitle
                context['style'] = style
                if storage :
                    context['storage'] = 'storage'
                response = Response(context)
            except Exception as e :
                s = get_error_status(e)
                errormsg = build_error_message(error=e)
                logger_report_error(LOGGER)
                response = Response(data={'detail':errormsg,}, status=s)

        else : # format other than html ('api' 'json' 'yaml' 'xml')

            if (style=='compact') :
                try :
                    vpzoutput = self.get_object()
                    context = dict()
                    context['res'] = vpzoutput.make_res_compact()
                    context['plan'] = vpzoutput.plan
                    context['restype'] = vpzoutput.restype
                    response = Response(data=context)
                except Exception as e :
                    s = get_error_status(e)
                    errormsg = build_error_message(error=e)
                    logger_report_error(LOGGER)
                    response = Response(data={'detail':errormsg,}, status=s)
            else :
                try :
                    context = super(OutputView, self).get(request, **kwargs)
                    response = context
                except Exception as e :
                    s = get_error_status(e)
                    errormsg = build_error_message(error=e)
                    logger_report_error(LOGGER)
                    response = Response(data={'detail':errormsg,}, status=s)
        return response

class InOutView(VpzActDetailViewMixin, PersistenceViewMixin,
                OutputViewMixin,
                StyleViewMixin, FormatViewMixin, DataViewMixin,
                DetailViewMixin, generics.RetrieveAPIView):
    """output information of a vpz and input information of a vpz
    
    For more information, see the online documentation
    (:ref:`get_vpz_inout`, :ref:`post_vpz_inout`).
    
    Returns VpzInput and VpzOutput information of a VpzAct after having \
    created it and done the required operations : maybe have modified some of \
    its VpzInput information (only in POST command case), have runned \
    simulation.

    In fact in the current version the returned information contains more \
    than VpzOutput and VpzInput, it contains all VpzAct information \
    (VpzOrigin...).

    The VpzAct is created from a VpzPath or from a VleVpz.

    After restitution, VpzAct (and its 'dependencies/tree') may be deleted or \
    kept into the database depending on persistence management.
    """

    content_negotiation_class = FormatContentNegotiation

    def get_object(self):
        data = self.data_from_request(self.request)
        self.vpzactid = None # default
        if self.request.method == 'POST' :
            try :
                vpzact = self.action_output_post(LOGGER=LOGGER, data=data)
                self.vpzactid = vpzact.id
                return vpzact
            except :
                raise
        else : # 'GET'
            try :
                vpzact = self.action_output_get(LOGGER=LOGGER, data=data)
                self.vpzactid = vpzact.id
                return vpzact
            except :
                raise

    def get(self, request, format=None, **kwargs):
        """Simulates VpzInput and returns VpzOutput and VpzInput. """

        data = self.request_query_params(request)
        if format is None :
            format = self.get_format_value(data=data)

        response = self.numeric_response(request=request, data=data,
                                         format=format, **kwargs)
        if self.vpzactid :
            vpzact = VpzAct.objects.get(pk=self.vpzactid)
            self.manages_persistence(vpzact=vpzact)
        return response

    def post(self, request, format=None, **kwargs):
        """
        Modifies VpzInput according to post data, simulates it and returns \
        VpzOutput and VpzInput.
        """

        data = self.request_data(request)
        if format is None :
            format = self.get_format_value(data=data)

        response = self.numeric_response(request=request, data=data,
                                         format=format, **kwargs)
        if self.vpzactid :
            vpzact = VpzAct.objects.get(pk=self.vpzactid)
            self.manages_persistence(vpzact=vpzact)
        return response

    def numeric_response(self, request, format, **kwargs):
        """response about VpzAct in numeric case

        Looks like VpzActDetail
        """

        if format=='html' :
            try :
                vpzact = self.get_object()
                context = dict()
                context['title'] = 'Vpz activity'
                context['headsubtitle'] = headsubtitle
                context['form'] = VpzActUserForm(instance=vpzact)
                response = Response(context )
            except Exception as e :
                s = get_error_status(e)
                errormsg = build_error_message(error=e)
                logger_report_error(LOGGER)
                response = Response(data={'detail':errormsg,}, status=s)
        else :
            try :
                context = super(InOutView, self).get(request, **kwargs)
                response = context
            except Exception as e :
                s = get_error_status(e)
                errormsg = build_error_message(error=e)
                logger_report_error(LOGGER)
                response = Response(data={'detail':errormsg,}, status=s)
        return response

class ReportView(ReportViewMixin, VpzActDetailViewMixin, PersistenceViewMixin,
                 OutputViewMixin, 
                 StyleViewMixin, FormatViewMixin, DataViewMixin,
                 DetailViewMixin, generics.RetrieveAPIView):
    """output and input information of a vpz as report(s)

    For more information, see the online documentation
    (:ref:`get_vpz_report`, :ref:`post_vpz_report`).
    
    Creates VpzInput and VpzOutput information of a VpzAct by doing the \
    required operations : maybe have modified some of its VpzInput \
    information (only in POST command case), have runned simulation. \
    Produces and returns the required report folder.

    The VpzAct is created from a VpzPath or from a VleVpz.
    
    The 'report' option is used to choose what kind of report is \
    produced/returned about vpzact.
 
    The 'todownload' mode option is used to choose to download the result.

    @include erecord_vpz/docs/report_option.txt
    (for more information, see the online documentation, main page) 

    After restitution, VpzAct (and its 'dependencies/tree') may be deleted or \
    kept into the database depending on persistence management.
    """

    content_negotiation_class = FormatContentNegotiation

    def get_object(self):
        data = self.data_from_request(self.request)
        self.vpzactid = None # default
        if self.request.method == 'POST' :
            try :
                vpzact = self.action_output_post(LOGGER=LOGGER, data=data)
                self.vpzactid = vpzact.id
                return vpzact
            except :
                raise
        else : # 'GET'
            try :
                vpzact = self.action_output_get(LOGGER=LOGGER, data=data)
                self.vpzactid = vpzact.id
                return vpzact
            except :
                raise

    def common(self, request, format=None, **kwargs):
        """ Generic method for get and post """

        data = self.data_from_request(self.request)
        if format is None :
            format = self.get_format_value(data=data)
        reports = self.get_report_values(data=data)
        todownload = self.get_todownload_value(data=data)

        response = self.report_response(request=request,
                            reports=reports, todownload=todownload,
                            format=format, **kwargs)
        if self.vpzactid :
            vpzact = VpzAct.objects.get(pk=self.vpzactid)
            self.manages_persistence(vpzact=vpzact)
        return response

    def get(self, request, format=None, **kwargs):
        """Simulates VpzInput and returns VpzOutput and VpzInput into a \
        report folder.
        """
        return self.common(request, format, **kwargs)

    def post(self, request, format=None, **kwargs):
        """Modifies VpzInput according to post data, simulates it and returns \
        VpzOutput and VpzInput into a report folder.
        """
        return self.common(request, format, **kwargs)

    def report_response(self, request, reports, todownload, format, **kwargs):
        """response about VpzAct"""

        try :
            vpzact = self.get_object()
            vpzact.vpzworkspace.clean_reporthome()
            self.build_folder(vpzact=vpzact, reports=reports)
            zip_folder_path = self.build_zip_folder(vpzact=vpzact)
    
            if todownload :
                download_url = self.build_download_folder(LOGGER=LOGGER,
                        request=request, file_path=zip_folder_path)
                label = "The report can be dowloaded at"
                if format=='html' :
                    context = dict()
                    context['title'] = 'Report (input and output simulation)'
                    context['headsubtitle'] = headsubtitle
                    form = TodownloadUrlForm({'url':download_url})
                    form.set(label=label, lifetime=DOWNLOAD_LIFETIME)
                    context['form'] = form
                    response = Response(context)
                else :
                    response = self.download_response(url=download_url,
                                                      label=label)
            else :
                response = self.zip_response(zip_path=zip_folder_path)
    
        except Exception as e :
            s = get_error_status(e)
            errormsg = build_error_message(error=e)
            logger_report_error(LOGGER)
            response = Response(data={'detail':errormsg,}, status=s)

        return response


class ReportConditionsView(ReportViewMixin, VpzActDetailViewMixin,
                 PersistenceViewMixin,
                 InputViewMixin,
                 StyleViewMixin, FormatViewMixin, DataViewMixin,
                 DetailViewMixin, generics.RetrieveAPIView):
    """input information of a vpz as report(s)

    For more information, see the online documentation
    (:ref:`get_vpz_report`, :ref:`post_vpz_report`).
    
    Creates VpzInput information of a VpzAct by doing the required \
    operations : maybe have modified some of its VpzInput information (only \
    in POST command case). Produces and returns the required report folder.

    The VpzAct is created from a VpzPath or from a VleVpz.
    
    The report produced/returned is xls files, about simulation conditions.
 
    After restitution, VpzAct (and its 'dependencies/tree') may be deleted or \
    kept into the database depending on persistence management.
    """

    content_negotiation_class = FormatContentNegotiation

    def get_object(self):
        data = self.data_from_request(self.request)
        self.vpzactid = None # default
        if self.request.method == 'POST' :
            try :
                vpzact = self.action_input_post(LOGGER=LOGGER, data=data)
                self.vpzactid = vpzact.id
                return vpzact
            except :
                raise
        else : # 'GET'
            try :
                vpzact = self.action_input_get(LOGGER=LOGGER, data=data)
                self.vpzactid = vpzact.id
                return vpzact
            except :
                raise

    def common(self, request, format=None, **kwargs):
        """ Generic method for get and post """

        data = self.data_from_request(self.request)
        if format is None :
            format = self.get_format_value(data=data)
        response = self.report_response(request=request, format=format,
                                        **kwargs)
        if self.vpzactid :
            vpzact = VpzAct.objects.get(pk=self.vpzactid)
            self.manages_persistence(vpzact=vpzact)
        return response

    def get(self, request, format=None, **kwargs):
        """Returns simulation conditions (from VpzInput) into a report \
        folder
        """
        return self.common(request, format, **kwargs)

    def post(self, request, format=None, **kwargs):
        """Modifies VpzInput according to post data, and returns simulation \
        conditions (from VpzInput) into a report folder
        """
        return self.common(request, format, **kwargs)

    def report_response(self, request, format, **kwargs):
        """response about simulation conditions (from VpzInput)"""

        try :
            vpzact = self.get_object()
            vpzact.vpzworkspace.clean_reporthome()
            self.build_conditions_folder(vpzact=vpzact)
            zip_folder_path = self.build_zip_folder(vpzact=vpzact)
            response = self.zip_response(zip_path=zip_folder_path)
    
        except Exception as e :
            s = get_error_status(e)
            errormsg = build_error_message(error=e)
            logger_report_error(LOGGER)
            response = Response(data={'detail':errormsg,}, status=s)

        return response

