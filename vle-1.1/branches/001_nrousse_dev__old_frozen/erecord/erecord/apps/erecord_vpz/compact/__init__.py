"""erecord_vpz.compact

*This package is part of erecord - Record platform web development*

:copyright: Copyright (C) 2014-2015 INRA http://www.inra.fr.
:license: GPLv3, see :ref:`LICENSE` for more details.
:authors: see :ref:`AUTHORS`.

Vpz activities application, code specific to the compact style presentation

In the Vpz activities application, it may be possible to select a compact \
style presentation (see mode=compact or compactlist).

This subpackage is dedicated to code about compact style management.

"""
