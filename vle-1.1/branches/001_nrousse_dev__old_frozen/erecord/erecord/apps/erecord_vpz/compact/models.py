# -*- coding: utf-8 -*-
"""erecord_vpz.compact.models

vpz model relative to compact style presentation

"""

from django.db import models

from erecord_vpz.models import ht_vlepar_pname
from erecord_vpz.models import ht_vlepar_cname
from erecord_vpz.models import ht_vlepar_value
from erecord_vpz.models import ht_vlepar_type


ht_vlepar_selection_name = "Name of a vle parameter (selection format)"
class VleParCompact(models.Model):
    """VlePar relative to compact style (minimum information around values)"""

    pname = models.CharField(max_length=200, blank=False,
            help_text=ht_vlepar_pname)
    cname = models.CharField(max_length=200, blank=False,
            help_text=ht_vlepar_cname)
    value = models.CharField(max_length=1000000, blank=False,
            help_text=ht_vlepar_value)
    type = models.CharField(max_length=200, blank=False,
            help_text=ht_vlepar_type)
    selection_name = models.CharField(max_length=200, blank=False,
            help_text=ht_vlepar_selection_name)

ht_vlecond_selection_name = "Name of a vle condition (selection format)"
class VleCondCompact(models.Model):
    """VleCond relative to compact style"""

    selection_name = models.CharField(max_length=200, blank=False,
            help_text=ht_vlecond_selection_name)

ht_vleview_selection_name = "Name of a vle view (selection format)"
class VleViewCompact(models.Model):
    """VleView relative to compact style"""

    selection_name = models.CharField(max_length=200, blank=False,
            help_text=ht_vleview_selection_name)

ht_vleout_selection_name = "Name of a vle output data (selection format)"
class VleOutCompact(models.Model):
    """VleOut relative to compact style"""

    selection_name = models.CharField(max_length=200, blank=False,
            help_text=ht_vleout_selection_name)


