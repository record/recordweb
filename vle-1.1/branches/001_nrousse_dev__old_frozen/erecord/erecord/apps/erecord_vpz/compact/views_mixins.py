# -*- coding: UTF-8 -*-
"""erecord_vpz.compact.views_mixins

Mixins common to erecord_vpz application views, relative to compact style

"""

#from erecord_cmn.utils.logger import get_logger
#LOGGER = get_logger(__name__)
from erecord_cmn.utils.errors import logger_report_error
from erecord_cmn.utils.errors import build_error_message


from erecord_vpz.models import VlePar
from erecord_vpz.compact.models import VleParCompact


class VleParCompactOptionViewMixin(object):
    """additional methods for views having options allowing to modify VlePar \
    values by selection name options

    The case taken here into account is when a VlePar is modified by the \
    following option : 'selection name of VlePar'=value
    """

    def get_vleparcompact_option_values(self, data):
        """Options that may correspond with the selection name of some VlePar.

        Returns the list of VleParCompact for which the following option has \
        been found : selection_name=value

        Attention : the way how such an option is here recognized is based on \
        the knowledge of the selection name building/definition (see in \
        VlePar build_parameter_selection_name and build_selection_name) : \
        selection_name values cname.pname. If this was not the case anymore, \
        then get_vleparcompact_option_values code should be modified/adapted.

        An option is accepted (that is to say used to add a VleParCompact \
        into the resulting list) if its name contains one and only one '.', \
        which is used to split cname and pname.
        """

        vleparcompact_option = list()
        for k,v in data.iteritems() :
            l = k.split(".")
            if len(l) == 2 :
                cname = l[0]
                pname = l[1]
                value = v
                selection_name=VlePar.build_parameter_selection_name(
                        cname=cname, pname=pname)
                vleparcompact_option.append(VleParCompact(cname=cname, 
                    pname=pname, value=value, selection_name=selection_name))
        return vleparcompact_option

