# -*- coding: utf-8 -*-
"""erecord_vpz.router """


from erecord_cmn.router import DatabaseRouter
from erecord_cmn.configs.config import DB_NAME_VPZ


class VpzRouter(DatabaseRouter):
    """
    The application database router
    """

    db_name = DB_NAME_VPZ
    app_name = 'erecord_vpz'

