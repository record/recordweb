# -*- coding: UTF-8 -*-

""" @file erecord_vpz/models_mixins/transform.py
..

Copyright (C) 2014-2016 INRA http://www.inra.fr \n\n
This file is part of erecord - Record platform web development \n\n
License : see LICENSE file. Authors : see AUTHORS file.
@include LICENSE @include AUTHORS
"""

""" @package erecord_vpz.models_mixins.transform
mixins for erecord_vpz model, about transformations

see erecord_vpz

"""

import os
import json

from erecord_cmn.utils.dir_and_file import make_csv_file
from erecord_cmn.utils.dir_and_file import create_dirs_if_absent

import xlwt
from erecord_cmn.utils.dir_and_file import Worksheet as WS
#from erecord_cmn.utils.dir_and_file import write_title
#from erecord_cmn.utils.dir_and_file import write_line_feed
#from erecord_cmn.utils.dir_and_file import write_lines_byrow
#from erecord_cmn.utils.dir_and_file import write_lines_bycol

from erecord_cmn.utils.logger import get_logger
LOGGER = get_logger(__name__) # should be passed as a parameter of methods ?

# Some mixins for the erecord_vpz models, about transformations

class TransformMixin(object):

    @classmethod
    def encode_utf8(cls, row):
        for i,r in enumerate(row):
            row[i] = row[i].encode('utf8')
            return row

    @classmethod
    def transpose(cls, array):

        maxlength = max([len(r) for r in array])
        for r in array :
            while len(r) < maxlength:
                r.append(None)
        tarray = list()
        for i in range(1,maxlength):
            tarray.append([])
        tarray.append([])
        for i,r_i in enumerate(array):
            for j,m_ij in enumerate(r_i):
                tarray[j].append(m_ij)
        return tarray

    @classmethod
    def make_list_title(cls, title):
        """Builds and returns a list for title (by row)"""
        r = list()
        r.append([title,])
        r.append([])
        return r

    @classmethod
    def make_list_line_feed(cls):
        """Builds and returns a list for a line feed (by row)"""
        return ([[]])

class VpzInputTransformMixin(TransformMixin):
    """additional methods for some VpzInput transformations"""

    def make_vpz_file(self, filename, dirpath):
        """ Builds the vpz file relative to VpzInput and saves it as \
        filename under dirpath """

        exp = self.vle_format_control()
        exp.save_as_vpz(vpzname=filename, dirpath=dirpath)

    def make_txt_file(self, filename, dirpath):
        """ Builds the txt file relative to VpzInput and saves it as \
        filename under dirpath """

        txtfile = os.path.join(dirpath, filename)
        file = open(txtfile, "a") # "w")
        file.write("\n")
        file.write("*** Input information report***\n")
        file.write("... under construction ...\n")
        file.write("\n")
        file.close()

    def make_list_general(self):
        """Builds and returns general information list of input information """

        list_general = list()
        list_general.append(["begin", self.vlebegin.value,])
        list_general.append(["duration", self.vleduration.value,])
        return list_general 

    def make_list_par_values(self, nicknames, types):
        """Builds and returns list of selected parameters values

        Some names and the type of the parameter are given before its values.
        """
        list_par_values = list()
        for vlecond in self.vlecond_list.all() :
            for vlepar in vlecond.vlepar_list.all() :
                if vlepar.is_selected() :
                    selection_name = vlepar.build_selection_name()
                    nickname = nicknames[selection_name]
                    type = types[selection_name]
                    row = [selection_name, type, nickname]
                    row = self.encode_utf8(row)
                    val = vlepar.get_val()
                    if isinstance(val, list) :
                        for i,v in enumerate(val) :
                            row.append(json.dumps(v))
                    else : # singleton case
                        row.append(json.dumps(val))
                    list_par_values.append(row)
        return list_par_values 

    def make_list_par_ident(self):
        """Builds and returns identification information of selected parameters

        list_ident : parameters identification information,
        nicknames : nickname (pname) according to selection name
        types : type according to selection name
        """

        nicknames = dict()
        types = dict()
        list_ident = list()
        list_ident.append(["name (selection name)", "condition name",
                           "parameter name", "type (of first value)",
                           "id (in database)"])
        for vlecond in self.vlecond_list.all() :
            for vlepar in vlecond.vlepar_list.all() :
                if vlepar.is_selected() :
                    selection_name = vlepar.build_selection_name()
                    row = [selection_name, vlepar.cname, vlepar.pname,
                           vlepar.type,]
                    row = self.encode_utf8(row)
                    row.append(vlepar.id)
                    list_ident.append(row)
                    nicknames[selection_name] = vlepar.pname
                    types[selection_name] = vlepar.type
        return (list_ident, nicknames, types)

    def make_list_out_ident(self):
        """Builds and returns identification information of selected output \
        datas

        list_ident : output datas identification information,
        nicknames : nickname (shortname) according to oname
        """

        nicknames = dict()
        nicknames['time'] = 'time'
        list_ident = list()
        list_ident.append(["name (selection name)", "view name",
                           "output data name", "short name",
                           "id (in database)"])
        for vleview in self.vpzact.vpzinput.vleview_list.all() :
            for vleout in vleview.vleout_list.all() :
                if vleout.is_selected() :
                    row = [vleout.build_selection_name(), vleout.vname,
                           vleout.oname, vleout.shortname,]
                    row = self.encode_utf8(row)
                    row.append(vleout.id)
                    list_ident.append(row)
                    nicknames[vleout.oname] = vleout.shortname
        return (list_ident, nicknames)

class VpzOutputTransformMixin(TransformMixin):
    """additional methods for some VpzOutput transformations"""

    def make_res_compact(self):
        """ Builds and returns res_compact where the output datas are \
        identified by their selection_name (as VleOut).

        Has an effect only in 'dataframe' restype case, returns unmodified \
        res in 'matrix' restype case.

        If 'dataframe' restype case, an output data is identified into res
        with a key relative to its name (not exactly oname), under the key _
        vname.

        res (of VpzOutput) is encoded.
        res_compact will be encoded.
        """
        from erecord_vpz.models import VleOut
        def build_res_compact_dataframe_single(res):
            res_compact = dict()
            for vname,view in res.iteritems():
                for outname in view.keys():
                    oname = self.build_output_oname(outname)
                    selection_name = VleOut.build_output_selection_name(
                        vname=vname, oname=oname)
                    res_compact[selection_name] = view.pop(outname)
            return res_compact

        compacted = False # default
        if self.format_res_ok():
            if self.restype == 'dataframe' :
                if self.plan == 'single' :
                    res = json.loads(self.res)
                    res_compact = build_res_compact_dataframe_single(res)
                    res_compact = json.dumps(res_compact)
                    compacted = True
                elif self.plan == 'linear' :
                    res_a = json.loads(self.res)
                    res_compact_a = list()
                    for res in res_a :
                        res_compact = build_res_compact_dataframe_single(res)
                        res_compact_a.append(res_compact)
                    res_compact = json.dumps(res_compact_a)
                    compacted = True
        if not compacted :
            res_compact = self.res
        return res_compact

    def make_list_general(self):
        """Builds and returns general information list of output information"""

        list_general = list()
        list_general.append(["restype", self.restype,])
        list_general.append(["plan", self.plan,])
        return list_general 

    @classmethod
    def get_suffixed_name(cls, name, simunumber=None):
        """Returns name suffixed by simunumber (name_simunumber)"""

        suffixed_name = name
        if simunumber is not None :
            suffixed_name = suffixed_name + "_" +str(simunumber)
        return suffixed_name

    @classmethod
    def get_indexed_name(cls, name, simunumber=None):
        """Returns name indexed by simunumber (name[simunumber])"""

        indexed_name = name
        if simunumber is not None :
            indexed_name = indexed_name + "[" +str(simunumber)+ "]"
        return indexed_name

    def write_csv_out_values(self, dirpath, res):
        """Builds under dirpath the csv reports about output datas values

        res is decoded res (of VpzOutput).

        Creates and fills the required csv files : one by view if 'single' \
        plan, or as many as simulations if 'linear' plan.

        csv files have headers or not according to restype case :
        - no header if 'matrix' restype.
        - header if 'dataframe' restype (same as those of vle simulation in \
          'file' mode).

        (output information)
        """

        def get_filename(viewname, simunumber=None):
            filename = self.get_suffixed_name(viewname, simunumber) + '.csv'
            return filename

        def write_csv_view(dirpath, viewname, outputs, simunumber=None): 
            filename = get_filename(viewname, simunumber)
            r = list()
            for outputname,val in outputs.items() :
                row = list()
                row.append(outputname)
                for i,v in enumerate(val):
                    row.append(v)
                r.append(row)
            t_r = self.transpose(r)
            make_csv_file(row_list=t_r, filename=filename, dirpath=dirpath,
                          delimiter=";")

        if self.restype=='dataframe' and self.plan=='single' :
            for viewname,outputs in res.items() :
                write_csv_view(dirpath=dirpath, viewname=viewname,
                               outputs=outputs) 

        elif self.restype=='dataframe' and self.plan=='linear' :
            for (a,res_a) in enumerate(res) :
                for viewname,outputs in res_a.items() :
                    write_csv_view(dirpath=dirpath, viewname=viewname,
                                   outputs=outputs, simunumber=a)

        elif self.restype=='matrix' and self.plan=='single' :
            for viewname,v in res.items() :
                filename = get_filename(viewname)
                t_v = self.transpose(v)
                make_csv_file(row_list=t_v, filename=filename,
                              dirpath=dirpath, delimiter=";")
        elif self.restype=='matrix' and self.plan=='linear' :
            for (a,res_a) in enumerate(res) :
                for viewname,v in res_a.items() :
                    filename = get_filename(viewname, a)
                    t_v = self.transpose(v)
                    make_csv_file(row_list=t_v, filename=filename,
                                  dirpath=dirpath, delimiter=";")

    def write_xls_out_values(self, wb, res, nicknames):
        """Writes the output datas values into xlwt.Worbook

        res is decoded res (of VpzOutput).

        Creates and fills the required xlwt.Worksheet : \
        - if 'dataframe' restype : only one worksheet for all the views \
          together (whichever 'plan' case). \
        - if 'matrix' restype : one worksheet by view if 'single' plan, or \
          as many as simulations if 'linear' plan.

        Worksheets have headers or not according to restype case :
        - no header if 'matrix' restype.
        - header if 'dataframe' restype, where several names are given to \
          identify each output data.

        (output information)
        """

        def get_sheetname(viewname, simunumber=None):
            sheetname ="output "+ self.get_suffixed_name(viewname, simunumber)
            return sheetname

        from erecord_vpz.models import VleOut
        def get_fullname(outputname, viewname, simunumber=None):
            """Returns the output data selection name suffixed by simunumber"""
            selection_name = VleOut.build_output_selection_name(vname=viewname,
                                                              oname=outputname)
            fullname = self.get_indexed_name(name=selection_name,
                                             simunumber=simunumber)
            return fullname

        def write_output(ws, row, col, viewname, outputname, values,
                         simunumber=None): 
            """writes in the col column of the ws worksheet the output data \
               header and values
            """
            fullname = get_fullname(outputname=outputname, viewname=viewname,
                                    simunumber=simunumber)
            ws.write(row, col, fullname)
            row = row + 1
            ws.write(row, col, self.get_indexed_name(outputname, simunumber))
            row = row + 1
            nickname = nicknames[outputname]
            ws.write(row, col, self.get_indexed_name(nickname, simunumber),
                     style=WS.head_style)
            row_ini = row + 1
            for i,v in enumerate(val):
                ws.write(i+row_ini, col, v)
            col = col + 1
            return (row, col)

        if self.restype=='dataframe' :
            default_sheetname = "output"
            ws = wb.add_sheet(default_sheetname)
            if self.plan=='single' :
                col = 0
                for viewname,outputs in res.items() :
                    for outputname,val in outputs.items() :
                        (row, col) = write_output(ws=ws, row=0, col=col,
                                         viewname=viewname,
                                         outputname=outputname, values=val)
            elif self.plan=='linear' :
                col = 0
                for (a,res_a) in enumerate(res) :
                    for viewname,outputs in res_a.items() :
                        for outputname,val in outputs.items() :
                            (row, col) = write_output(ws=ws, row=0, col=col,
                                             viewname=viewname,
                                             outputname=outputname, values=val,
                                             simunumber=a)
        elif self.restype=='matrix' and self.plan=='single' :
            for viewname,v in res.items() :
                ws = wb.add_sheet(get_sheetname(viewname))
                last_row = WS.write_lines_bycol(ws=ws, row_ini=0, lines=v)
        elif self.restype=='matrix' and self.plan=='linear' :
            for (a,res_a) in enumerate(res) :
                for viewname,v in res_a.items() :
                    ws = wb.add_sheet(get_sheetname(viewname, a))
                    last_row = WS.write_lines_bycol(ws=ws, row_ini=0, lines=v)

    def make_txt_file(self, filename, dirpath):
        """ Builds the txt file relative to VpzOutput and saves it as \
        filename under dirpath """

        txtfile = os.path.join(dirpath, filename)
        file = open(txtfile, "a") # "w")
        file.write("\n")
        file.write("*** Output information report***\n")
        file.write("... under construction ...\n")
        file.write("\n")
        file.close()

class VpzActTransformMixin(TransformMixin):
    """additional methods for some VpzAct transformations"""

    def prepare_reports(self, only_conditions=False):
        """Prepares information (lists) that will be used for reports building

        Information comes from input and output information, but from only \
        input information in only_conditions case.
        """

        list_general_input = self.vpzinput.make_list_general()
        if only_conditions :
            list_general = list_general_input
        else :
            list_general_output = self.vpzoutput.make_list_general()
            list_general = list_general_output + list_general_input
        self.list_general_byrow = list_general
        self.list_general_bycol = self.transpose(list_general)

        (list_par_ident,
         nicknames_par, types_par) = self.vpzinput.make_list_par_ident()
        self.list_par_ident_byrow = list_par_ident
        self.nicknames_par = nicknames_par
        self.types_par = types_par

        if only_conditions :
            self.list_out_ident_byrow = None
            self.nicknames_out = None
        else :
            (list_out_ident, nicknames_out) = self.vpzinput.make_list_out_ident()
            self.list_out_ident_byrow = list_out_ident
            self.nicknames_out = nicknames_out

        list_par_values =  self.vpzinput.make_list_par_values(
                                             nicknames=self.nicknames_par,
                                             types=self.types_par)
        self.list_par_values_byrow =  list_par_values
        self.list_par_values_bycol =  self.transpose(list_par_values)

        if only_conditions :
            self.decoded_res = None
        else :
            decoded_res = json.loads(self.vpzoutput.res)
            self.decoded_res = decoded_res

    def make_folder_vpz(self, dirpath):
        """ Builds under dirpath the vpz report """

        newvpzname = "__updated__" + self.vpzname
        self.vpzinput.make_vpz_file(filename=newvpzname, dirpath=dirpath)

        # transformation from vpz file (... test tmp)
        exp = self.vpzinput.vle_format_control()
        exp.save_as_csv(filename="_tmptest_from_exp_"+newvpzname+".csv",
                        dirpath=dirpath)

    def make_folder_txt(self, dirpath):
        """Builds under dirpath the txt report
        
        (input and output information)
        """

        filename='report.txt'
        self.vpzinput.make_txt_file(filename=filename, dirpath=dirpath)
        self.vpzoutput.make_txt_file(filename=filename, dirpath=dirpath)

    def make_folder_csv_ident(self, dirpath):
        """ Builds under dirpath the csv report about identification

        (input information)
        """

        filename='ident.csv'
        title="*** Parameters identification ***"
        r = self.make_list_title(title)
        r = r + self.list_par_ident_byrow
        r = r + self.make_list_line_feed()
        title="*** Output datas identification ***"
        r = r + self.make_list_title(title)
        r = r + self.list_out_ident_byrow
        make_csv_file(row_list=r, filename=filename, dirpath=dirpath,
                      delimiter=";")

    def make_folder_csv_cond(self, dirpath):
        """ Builds under dirpath the csv reports about simulation conditions

        (input and output information)
        """

        filename='conditions_byrow.csv' # a row for each data
        title="*** General information ***"
        r = self.make_list_title(title)
        r = r + self.list_general_byrow
        r = r + self.make_list_line_feed()
        title="*** Parameters values ***"
        r = r + self.make_list_title(title)
        r = r + self.list_par_values_byrow
        make_csv_file(row_list=r, filename=filename, dirpath=dirpath,
                      delimiter=";")

        filename='conditions_bycol.csv' # a col for each data
        title="*** General information ***"
        r = self.make_list_title(title)
        r = r + self.list_general_bycol
        r = r + self.make_list_line_feed()
        title="*** Parameters values ***"
        r = r + self.make_list_title(title)
        r = r + self.list_par_values_bycol
        make_csv_file(row_list=r, filename=filename, dirpath=dirpath,
                      delimiter=";")


    def make_folder_csv_output(self, dirpath):
        """ Builds under dirpath the csv reports about output datas values

        (output information)
        """

        self.vpzoutput.write_csv_out_values(dirpath=dirpath,
                                            res=self.decoded_res)

    def make_folder_xls(self, dirpath):
        """ Builds under dirpath the xls report

        A worksheet by theme (ident, conditions...).

        (input and output information)
        """

        filename='report.xls'
        xlsfile = os.path.join(dirpath, filename)
        wb = xlwt.Workbook()

        ws_ident = wb.add_sheet("ident")
        last_row = WS.write_group_byrow(ws=ws_ident,
                   info_list=self.list_par_ident_byrow, nb_head=1, col_main=[2],
                   row_ini=0, title="*** Parameters identification ***")
        last_row = WS.write_line_feed(ws=ws_ident, row_ini=last_row+1)
        last_row = WS.write_group_byrow(ws=ws_ident,
                   info_list=self.list_out_ident_byrow, nb_head=1, col_main=[3],
                   row_ini=last_row+1,
                   title="*** Output datas identification ***")

        ws = wb.add_sheet("conditions_byrow")
        last_row = WS.write_group_byrow(ws=ws,
                   info_list=self.list_general_byrow, col_main=[0],
                   row_ini=0, title="*** General information ***")
        last_row = WS.write_line_feed(ws=ws, row_ini=last_row+1)
        last_row = WS.write_group_byrow(ws=ws,
                   info_list=self.list_par_values_byrow, col_main=[2],
                   row_ini=last_row+1, title="*** Parameters values ***")

        ws = wb.add_sheet("conditions_bycol")
        last_row = WS.write_group_bycol(ws=ws,
                   info_list=self.list_general_byrow, row_main=[0],
                   row_ini=0, title="*** General information ***")
        last_row = WS.write_line_feed(ws=ws, row_ini=last_row+1)
        last_row = WS.write_group_bycol(ws=ws,
                   info_list=self.list_par_values_byrow, row_main=[2],
                   row_ini=last_row+1, title="*** Parameters values ***")

        self.vpzoutput.write_xls_out_values(wb=wb, res=self.decoded_res,
                                            nicknames=self.nicknames_out)

        wb.save(xlsfile)

    def make_conditions_folder_xls(self, dirpath):
        """ Builds under dirpath the xls reports about simulation conditions

        A file by row, a file by column.

        (input information)
        """

        filename='conditions_byrow.xls'
        xlsfile = os.path.join(dirpath, filename)
        wb = xlwt.Workbook()
        ws = wb.add_sheet("conditions_byrow")
        last_row = WS.write_group_byrow(ws=ws,
                   info_list=self.list_general_byrow, col_main=[0],
                   row_ini=0, title="*** General information ***")
        last_row = WS.write_line_feed(ws=ws, row_ini=last_row+1)
        last_row = WS.write_group_byrow(ws=ws,
                   info_list=self.list_par_values_byrow, col_main=[2],
                   row_ini=last_row+1, title="*** Parameters values ***")
        wb.save(xlsfile)

        filename='conditions_bycol.xls'
        xlsfile = os.path.join(dirpath, filename)
        wb = xlwt.Workbook()
        ws = wb.add_sheet("conditions_bycol")
        last_row = WS.write_group_bycol(ws=ws,
                   info_list=self.list_general_byrow, row_main=[0],
                   row_ini=0, title="*** General information ***")
        last_row = WS.write_line_feed(ws=ws, row_ini=last_row+1)
        last_row = WS.write_group_bycol(ws=ws,
                   info_list=self.list_par_values_byrow, row_main=[2],
                   row_ini=last_row+1, title="*** Parameters values ***")
        wb.save(xlsfile)

