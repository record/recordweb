# -*- coding: UTF-8 -*-

""" @file erecord_vpz/models_mixins/workspace.py
..

Copyright (C) 2014-2016 INRA http://www.inra.fr \n\n
This file is part of erecord - Record platform web development \n\n
License : see LICENSE file. Authors : see AUTHORS file.
@include LICENSE @include AUTHORS
"""

""" @package erecord_vpz.models_mixins.workspace
mixins for erecord_vpz model, about workspace management

see erecord_vpz

"""

import os

import shutil
from erecord_cmn.utils.dir_and_file import delete_path_if_present
from erecord_cmn.utils.dir_and_file import clean_dir
from erecord_cmn.utils.dir_and_file import create_dir_if_absent

from erecord_cmn.utils.vle import get_rep_pkgs_path
from erecord_cmn.utils.vle import set_vle_home
from erecord_cmn.utils.vle import get_pkg_output_path

from erecord_cmn.utils.logger import get_logger
LOGGER = get_logger(__name__) # should be passed as a parameter of methods ?

# Some mixins for the erecord_vpz models, about workspace management

class WorkspaceMixin(object):

    @classmethod
    def get_undefined_value(cls):
        return "undefined"

    @classmethod
    def is_undefined_value(cls, v):
        return (v == cls.get_undefined_value())

class VlehomeWorkspaceMixin(WorkspaceMixin):

    def init_vlehome(self, LOGGER, as_vlehome=False):
        vlehome = os.path.join(self.homepath, "vle_home")
        if as_vlehome :
            (deletion_done, creation_done) = clean_dir(dirpath=vlehome)
            if deletion_done :
                LOGGER.warning(u"%s has been deleted before to be recreated", 
                        vlehome)
        set_vle_home(vlehome_path=vlehome)
        return vlehome

    def get_vlehome(self):
        return self.vlehome

    def clean_vlehome(self):
        clean_dir(dirpath=self.vlehome)

    def clear_vlehome(self):
        delete_path_if_present(path=self.vlehome)

    def build_pkg_and_its_dependencies(self, vpzname, vlepackage_list):
        """Builds into vlehome a vle package and its dependency ones.

        The input vle packages are given as a list of their \
        (package name, package path), where the first one is the main package \
        (containing the vpz file named vpzname).
        
        The main vle package is built as a copy, and the dependency packages \
        as links.

        About the main package : its whole 'exp' subdirectory is copied, even \
        if only its vpz file vpzname is needed.

        About the main package : 'output' subdirectory is created if not there.

        Go to 'output' subdirectory.
        """

        pkgs_path = get_rep_pkgs_path(rep_path=self.vlehome)

        try:
            (pkgname,vlepkgpath) = vlepackage_list[0] # main package
            src = vlepkgpath
            dest = os.path.join(pkgs_path,pkgname)
            shutil.copytree(src, dest)
            output_path = get_pkg_output_path(pkg_path=dest)
            create_dir_if_absent(dirpath=output_path)
            for (pkgname,vlepkgpath) in vlepackage_list[1:-1] : # dependencies
                src = vlepkgpath
                dest = os.path.join(pkgs_path,pkgname)
                os.symlink(src, dest)
            os.chdir(output_path)
        except OSError as e:
            msg = "unable to build vle packages into vle_home workspace "+ \
                    self.vlehome
            raise Exception(msg)

class ReporthomeWorkspaceMixin(WorkspaceMixin):

    def init_reporthome(self, LOGGER, as_reporthome=False):
        reporthome = os.path.join(self.homepath, "report_home")
        if as_reporthome :
            (deletion_done, creation_done) = clean_dir(dirpath=reporthome)
            if deletion_done :
                LOGGER.warning(u"%s has been deleted before to be recreated", 
                        reporthome)
        return reporthome

    def get_reporthome(self):
        return self.reporthome

    def get_report_folder_path(self, name) :
        """Defines and return a folder path name as a reporthome subdirectory"""
        return os.path.join(self.reporthome, name+'_folder')

    def clean_reporthome(self):
        clean_dir(dirpath=self.reporthome)

    def clear_reporthome(self):
        delete_path_if_present(path=self.reporthome)

