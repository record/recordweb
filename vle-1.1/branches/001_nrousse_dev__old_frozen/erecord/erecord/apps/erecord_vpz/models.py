# -*- coding: utf-8 -*-
"""erecord_vpz.models

Models of the erecord_vpz application

"""

import os
import json

from django.db import models
from django.core.exceptions import ValidationError
from django.db.models.signals import pre_delete

from erecord_cmn.models_mixins import DefaultVerboseNameMixin
from erecord_vpz.models_mixins.models_mixins import VpzPathMixin
from erecord_vpz.models_mixins.models_mixins import AttachedToVpzActMixin
from erecord_vpz.models_mixins.models_mixins import VpzActMixin
from erecord_vpz.models_mixins.models_mixins import VpzWorkspaceMixin
from erecord_vpz.models_mixins.models_mixins import VpzOriginMixin
from erecord_vpz.models_mixins.models_mixins import VpzInputMixin
from erecord_vpz.models_mixins.models_mixins import VpzOutputMixin
from erecord_vpz.models_mixins.transform import VpzInputTransformMixin
from erecord_vpz.models_mixins.transform import VpzOutputTransformMixin
from erecord_vpz.models_mixins.transform import VpzActTransformMixin

from erecord_cmn.configs.config import REPOSITORIES_HOME
from erecord_cmn.utils.vle import is_structured_as_vle_home
from erecord_cmn.utils.vle import is_pkg_of_rep
from erecord_cmn.utils.vle import is_vpz_of_pkg_of_rep
from erecord_cmn.utils.vle import get_vlepath_pkg_name
from erecord_cmn.utils.vle import Exp
from erecord_cmn.utils.coding import byteify

ht_vpzact_vpzname = \
        "Name of the vpz file : path relative to 'exp' directory, with .vpz \
        extension (same as in vle)"
ht_vpzact_pkgname = \
        "the name of the vle package including the vpz file 'name'. The vpz \
        file must be physically installed under the vle package (under 'path' \
        directory)"
ht_vpzact_vlepath = \
        "Directory path where the vle package 'pkg' including the vpz file \
        'name' has been physically installed (directory structured as a \
        VLE_HOME directory)"
ht_vpzact_verbose_name = "Human-readable name of the vpz (...operation? url?)"

class VpzAct(VpzActTransformMixin, VpzActMixin, DefaultVerboseNameMixin,
        models.Model):
    """Vpz activity

    An activity about/on a vpz file is one of the following ones : 

    - vpz/input GET   : see (unmodified) input,
    - vpz/input POST  : modify input and see modified input,
    - vpz/output GET  : run (unmodified) input and see output,
    - vpz/output POST : modify input and run modified input and see output,
    - vpz GET  : idem vpz/output GET + see (unmodified) input,
    - vpz POST : idem vpz/output POTS + see modified input,

    Attributes :

    - vpzname : vpz file name
    - pkgname : vle package name
    - vlepath : vle home path (models repository path)

    - verbose_name : human-readable name ?  operation ? url ?

    - vpzorigin : relative :class:`VpzOrigin`
      (one :class:`VpzAct` to one :class:`VpzOrigin` relationship)
    - vpzinput : relative :class:`VpzInput`
      (one :class:`VpzAct` to one :class:`VpzInput` relationship) 
    - vpzoutput : relative :class:`VpzOutput`
      (one :class:`VpzAct` to one :class:`VpzOutput` relationship)

    - created_at, updated_at : date information for admin management

    :class:`VleRep <erecord_db.models.VleRep>`, :class:`VleRep <erecord_db.models.VlePkg>`, :class:`VleRep <erecord_db.models.VleVpz>` come from :mod:`erecord_db`
    """

    vpzname = models.CharField(max_length=200,
            blank=False,
            help_text=ht_vpzact_vpzname)
    pkgname = models.CharField(max_length=200,
            blank=False,
            help_text=ht_vpzact_pkgname)

    vlepath = models.FilePathField(blank=False,
            help_text=ht_vpzact_vlepath)

    verbose_name = models.CharField(max_length=200,
            blank=True,
            help_text=ht_vpzact_verbose_name)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def delete_if_old(self):
        """delete vpzact and its dependencies, if too old """
        if self.is_old() :
            self.delete()

    def clean(self):

        if not is_structured_as_vle_home(self.vlepath) :
            msg = "The path ("+self.vlepath+") is not a directory " + \
                    "structured as a VLE_HOME directory."
            raise ValidationError(msg)
        if not is_pkg_of_rep(rep_path=self.vlepath, pkgname=self.pkgname) :
            msg = "'"+self.pkgname+ "' doesn't exist (as a vle package) " + \
                    "under the self.vlepath ("+self.vlepath+")"
            raise ValidationError(msg)
        if not is_vpz_of_pkg_of_rep(rep_path=self.vlepath, 
                pkgname=self.pkgname, vpzname=self.vpzname) :
            msg = "'"+self.vpzname+"' doesn't exist as a vpz file of the "+ \
                    "'"+self.pkgname+"' vle package under the "+ \
                    "self.vlepath ("+self.vlepath+")"
            raise ValidationError(msg)

    def save(self, *args, **kwargs):
        vn = "An activity on vpz file '"+self.vpzname+"' (of '"+ \
                self.pkgname+"' of '"+ self.vlepath+"')"
        self.set_default_value_verbose_name(vn)
        super(VpzAct, self).save(*args, **kwargs)

    def __str__(self):
        return "Vpz activity id "+str(self.id)+", on vpz file '"+ \
                self.vpzname+"' (of '"+self.pkgname+"' of '"+ self.vlepath+"')"

    class Meta:
        verbose_name = "vpz activity"
        verbose_name_plural = "vpz activities"


ht_vpzorigin_text = "text describing vpz origin"
class VpzOrigin(VpzOriginMixin, AttachedToVpzActMixin, models.Model):
    """Vpz origin

    - vpzact : relative :class:`VpzAct` (one :class:`VpzAct` to one :class:`VpzOrigin` relationship) 
    - date (copy)

    - vlerep : relative VleRep (one :class:`VpzOrigin` to one VleRep relationship)
    - vlepkg : relative VlePkg (one :class:`VpzOrigin` to one VlePkg relationship)
    - vlevpz : relative VleVpz (one :class:`VpzOrigin` to one VleVpz relationship)

    """
    vpzact = models.OneToOneField(VpzAct, related_name="vpzorigin", 
            blank=False )

    text = models.CharField(max_length=2000, blank=True, default=None,
            help_text=ht_vpzorigin_text )

    def clean(self):
        if self.text is None :
            self.set_undefined_value_text()

    def __str__(self):
        return "Vpz origin id "+str(self.id)

    class Meta:
        verbose_name = "vpz origin"
        verbose_name_plural = "vpz origins"


class VpzInput(VpzInputTransformMixin, VpzInputMixin, AttachedToVpzActMixin,
        models.Model):
    """Vpz input

    - vpzact : relative :class:`VpzAct` (one :class:`VpzAct` to one :class:`VpzInput` relationship) 

    - vlebegin : relative :class:`VleBegin` (one :class:`VpzInput` to one :class:`VleBegin` relationship)
    - vleduration : relative :class:`VleDuration` (one :class:`VpzInput` to one :class:`VleDuration`
      relationship)
    - vlecond_list : list of relative :class:`VleCond` (many VleCond to one :class:`VpzInput`
      relationship)
    - vleview_list : list of relative :class:`VleView` (many :class:`VleView` to one :class:`VpzInput`
      relationship)
    """

    vpzact = models.OneToOneField(VpzAct, related_name="vpzinput",
            blank=False )

    def __str__(self):
        return "Vpz input id "+str(self.id)

    def read_and_add_content(self, exp=None) :
        """reads :class:`VpzInput` content (initial state) and add it into the database.

        Reads into the vpz file (cf :class:`VpzAct`) the vle input information \
                (initial state) and add the relevant objects (:class:`VleBegin`, :class:`VleDuration`, \
                :class:`VleCond`..., :class:`VleView`, :class:`VleOut`) into the database.
        The :class:`VpzInput` is attached to a :class:`VpzAct`.
        """

        if exp is None :
            exp = Exp(vpzname=self.vpzact.vpzname, pkgname=self.vpzact.pkgname)

        begin = exp.get_begin_value()
        vlebegin = VleBegin(vpzinput=self, value=begin)
        vlebegin.save()

        duration = exp.get_duration_value()
        vleduration = VleDuration(vpzinput=self, value=duration)
        vleduration.save()

        condition_name_list = exp.get_condition_name_list()
        for cname in condition_name_list :
            vlecond = self.vlecond_list.create(name=cname)
            vlecond.save()
            vlecond.read_and_add_content(exp=exp)

        view_name_list = exp.listViews()
        for vname in view_name_list :
            type = exp.getViewType(vname)
            timestep = exp.getViewTimeStep(vname)
            output = exp.getViewOutput(vname)
            output_name = output
            output_plugin = exp.getOutputPlugin(output)
            output_format = exp.getOutputFormat(output)
            output_location = exp.getOutputLocation(output)
            vleview = self.vleview_list.create(name=vname, type=type, 
                    timestep=timestep, output_name=output_name, 
                    output_plugin=output_plugin, output_format=output_format, 
                    output_location=output_location)
            vleview.save()
            vleview.read_and_add_content(exp=exp)

    def update_vlepar_values_and_save(self, vleparcompact_list) :
        """Updates into the database :class:`VlePar` values according to
        vleparcompact_list (list of VleParCompact).
        """
        for par in vleparcompact_list :
            for c in self.vlecond_list.all() :
                for p in c.vlepar_list.all() :
                    if par.selection_name==p.build_selection_name():
                        p.value = par.value
                        p.save()

    def set_selected_all_vlepars(self):
        for c in self.vlecond_list.all() :
            for p in c.vlepar_list.all() :
                p.set_selected()
                p.save()

    def unset_selected_all_vlepars(self):
        for c in self.vlecond_list.all() :
            for p in c.vlepar_list.all() :
                p.unset_selected()
                p.save()

    def update_vlepar_selected_and_save(self, parselect=None) :
        """Updates into the database the :class:`VlePar` that are attached to \
                :class:`VpzInput`, according to parselect.

        (for more about parselect values, see ParselectOptionViewMixin)

        Case parselect = None, or
        Case of "all" among the parselect list (priority given to this case) : 
        selected for all the :class:`VlePar`.

        Case of some condname and/or parname (parname=cname.pname) into the
        parselect list :
        For each :class:`VleCond` (with name) :
        (a) if name in parselect, then selected for all its :class:`VlePar`.
        (b) if at least one parname relative to :class:`VleCond` (ie with cname=name) in
          parselect, then selected for all its :class:`VlePar` corresponding to one of
          those parname and unselected for all its other :class:`VlePar`.
        (c) if no (a) and no (b), then unselected for all its :class:`VlePar`.

        If a :class:`VleCond` and at the same time (at least) one of its :class:`VlePar` was
        given in parselect, there is no guarantee about which of both rules
        would be applied.
        """

        if not parselect:
            self.set_selected_all_vlepars()

        else : # if parselect :
            if "all" in parselect :
                self.set_selected_all_vlepars()
            else :
                self.unset_selected_all_vlepars() # default

                selected_cond_list =  list()
                for c in self.vlecond_list.all() :
                    if c.build_selection_name() in parselect :
                        selected_cond_list.append(c)
                cond_with_one_par_selected_list = list() 
                for c in self.vlecond_list.all() :
                    for p in c.vlepar_list.all() :
                        if (p.build_selection_name() in parselect) and \
                        (c not in cond_with_one_par_selected_list) :
                            cond_with_one_par_selected_list.append(c)
                for c in self.vlecond_list.all() :
                    if c in cond_with_one_par_selected_list :
                        for p in c.vlepar_list.all() :
                            if p.build_selection_name() in parselect :
                                p.set_selected()
                                p.save()
                            else :
                                p.unset_selected()
                                p.save()
                    elif c in selected_cond_list :
                        for p in c.vlepar_list.all() :
                            p.set_selected()
                            p.save()
                    else :
                        for p in c.vlepar_list.all() :
                            p.unset_selected()
                            p.save()

                if not selected_cond_list and \
                   not cond_with_one_par_selected_list : # no (a) and no (b)
                    self.set_selected_all_vlepars()


    def update_vleview_vleout_and_save(self, outselect=None, 
                                       output_plugin_choice='storage') :
        """Updates into the database the :class:`VleView` and their \
        :class:`VleOut` that are attached to :class:`VpzInput`, according to \
        outselect and output_plugin_choice.

        (for more about output_plugin_choice, see output_plugin_choice, \
        get_kind_of_simulation_result in ReportOptionViewMixin)
        (for more about outselect values, see OutselectOptionViewMixin)

        1) Initialisation of all :class:`VleOut` selected according to \
        output_plugin_choice.

        2) Updating of :class:`VleView` output_plugin and :class:`VleOut` \
        selected in order to take into account outselect and \
        output_plugin_choice :

        Case outselect = None : no change for any :class:`VleView` neither \
        :class:`VleOut`.

        Case of "all" among the outselect list (priority given to this \
        case) : activation for all the :class:`VleView` and selected for all \
        their :class:`VleOut`.

        Case of some viewname and/or outname (outname=vname.oname) into the
        outselect list :
        For each :class:`VleView` (with name) :
        - if name in outselect, then activation for :class:`VleView` and selected for
          all its :class:`VleOut`.
        - if at least one outname relative to :class:`VleView` (ie with vname=name) in
          outselect, then activation for :class:`VleView`, selected for all its
          :class:`VleOut` corresponding to one of those outname and unactivation for all
          its other :class:`VleOut`.

        If a :class:`VleView` and at the same time (at least) one of its :class:`VleOut` was
        given in outselect, there is no guarantee about which of both rules
        would be applied.

        A view activation operation depends on output_plugin_choice value.
        """

        # VleOut selected initialisation
        for v in self.vleview_list.all() :
            if v.is_activated(output_plugin_choice) :
                for o in v.vleout_list.all() :
                    o.set_selected()
                    o.save()

        # VleView output_plugin and VleOut selected updating
        if outselect is not None :
            if "all" in outselect :
                for v in self.vleview_list.all() :
                    v.activate(output_plugin_choice)
                    v.save()
                    for o in v.vleout_list.all() :
                        o.set_selected()
                        o.save()
            else :
                selected_view_list =  list()
                for v in self.vleview_list.all() :
                    if v.build_selection_name() in outselect :
                        selected_view_list.append(v)
                view_with_one_out_selected_list = list() 
                for v in self.vleview_list.all() :
                    for o in v.vleout_list.all() :
                        if (o.build_selection_name() in outselect) and \
                        (v not in view_with_one_out_selected_list) :
                            view_with_one_out_selected_list.append(v)
                for v in self.vleview_list.all() :
                    if v in view_with_one_out_selected_list :
                        v.activate(output_plugin_choice)
                        v.save()
                        for o in v.vleout_list.all() :
                            if o.build_selection_name() in outselect :
                                o.set_selected()
                                o.save()
                            else :
                                o.unset_selected()
                                o.save()
                    elif v in selected_view_list :
                        v.activate(output_plugin_choice)
                        v.save()
                        for o in v.vleout_list.all() :
                            o.set_selected()
                            o.save()
                    else :
                        v.unactivate()
                        v.save()
                        for o in v.vleout_list.all() :
                            o.unset_selected()
                            o.save()

    def vle_format_control(self):
        """Controls :class:`VpzInput` according to vle format requirements \
        and returns the corresponding Exp.

        Controls that :class:`VpzInput` (current state) is compatible with the
        format expected by vle, by building the corresponding Exp, and returns
        Exp.
        In order to correspond with :class:`VpzInput` current state, Exp is
        firstly created from :class:`VpzAct` initial state (cf vpzname,
        pkgname) and is then updated according to :class:`VpzInput` that may
        have been modified.
        """
        exp = Exp(vpzname=self.vpzact.vpzname, pkgname=self.vpzact.pkgname)
        exp.update(self)
        return exp


    def get_vlecond_selected(self):
        """Filter consisting in keeping only the selected :class:`VleCond`
        
        A :class:`VleCond` is selected if at least one of its :class:`VlePar`
        is selected
        """

        id_list = [ c.id for c in self.vlecond_list.all() 
                if c.get_vlepar_selected() ]
        vlecond_selected_list = self.vlecond_list.filter(id__in=id_list)
        return vlecond_selected_list


    def get_vleview_selected(self):
        """Filter consisting in keeping only the selected :class:`VleView`
        
        A :class:`VleView` is selected if at least one of its :class:`VleOut`
        is selected
        """

        id_list = [ v.id for v in self.vleview_list.all() 
                if v.get_vleout_selected() ]
        vleview_selected_list = self.vleview_list.filter(id__in=id_list)
        return vleview_selected_list

    class Meta:
        verbose_name = "vpz input"
        verbose_name_plural = "vpz inputs"


ht_vpzoutput_res = "Simulation numerical result"
ht_vpzoutput_plan = "Kind of plan of the simulation"
ht_vpzoutput_restype = "Kind of restype of the simulation"
class VpzOutput(VpzOutputTransformMixin, VpzOutputMixin,
        AttachedToVpzActMixin, models.Model):
    """Vpz output

    - vpzact : relative :class:`VpzAct`
      (one :class:`VpzAct` to one :class:`VpzOutput` relationship) 

    - res : simulation numerical result
    - plan : kind of plan of the simulation (single or linear)
    - restype : kind of restype of the simulation (dataframe or matrix)

    """

    vpzact = models.OneToOneField(VpzAct, related_name="vpzoutput")

    res = models.CharField(max_length=1000000000, blank=True, default="",
            help_text=ht_vpzoutput_res )
    plan = models.CharField(max_length=10, blank=True, default="",
            help_text=ht_vpzoutput_plan )
    restype = models.CharField(max_length=10, blank=True, default="",
            help_text=ht_vpzoutput_restype )

    def run_and_add_content(self, plan='single', restype='dataframe'):
        """Simulates, reads :class:`VpzOutput` content and add it into the \
        database.

        Simulates the vpz file relative to :class:`VpzInput` current state. \
        Reads the vle output information and add the relevant objects \
        (:class:`VleView`...) into the database. The :class:`VpzOutput` is \
        attached to a :class:`VpzAct` that has a :class:`VpzInput`.
        
        The simulation running method depends on plan ('single' or 'linear') \
        and restype ('dataframe' or 'matrix').
        """

        self.run(plan=plan, restype=restype)
        self.filter_selected_vleout()
        self.res = json.dumps(self.res)
        self.save()

        #+??? jeter et recreer self.vpzact.vpzinput : 
        #self.vpzact.vpzinput.read_and_add_content(exp=exp)
        #(...a priori inutile...) ???

    def __str__(self):
        return "Vpz output id "+str(self.id)

    class Meta:
        verbose_name = "vpz output"
        verbose_name_plural = "vpz outputs"

ht_vpzpath_vpzpath = "Vpz file absolute path"
ht_vpzpath_verbose_name = "Human-readable name of the vpz path"
class VpzPath(DefaultVerboseNameMixin, VpzPathMixin, models.Model):
    """Vpz path

    A VpzPath corresponds with a vpz file absolute path (that gives its 
    locauion into a vle package into a models repository and its vpz name).

    Attributes :

    - verbose_name : human-readable name ? 
    - vpzpath : vpz file absolute path

    """

    verbose_name = models.CharField(max_length=200,
            blank=True,
            help_text=ht_vpzpath_verbose_name)

    vpzpath = models.FilePathField(blank=False,
            help_text=ht_vpzpath_vpzpath)

    def clean(self):

        e = get_vlepath_pkg_name( vpzpath=self.vpzpath,
                limit=REPOSITORIES_HOME )
        if e is None :
            msg = "unavailable vpzpath '"+self.vpzpath+"'"
            raise ValidationError(msg)
        else :
            (vlepath,pkg,name) = e
            if not is_structured_as_vle_home(vlepath) :
                msg = "The path ("+vlepath+") is not a directory structured \
                        as a VLE_HOME directory."
                raise ValidationError(msg)
            if not is_pkg_of_rep(rep_path=vlepath, pkgname=pkg) :
                msg = "'"+pkg+ "' doesn't exist (as a vle package) under \
                        the vlepath ("+vlepath+")"
                raise ValidationError(msg)
            if not is_vpz_of_pkg_of_rep(rep_path=vlepath, 
                    pkgname=pkg, vpzname=name) :
                msg = "'"+name+"' doesn't exist as a vpz file of the "+ \
                        "'"+pkg+"' vle package under the vlepath "+ \
                        "("+vlepath+")"
                raise ValidationError(msg)

    def save(self, *args, **kwargs):
        vn = "A VpzPath ('"+str(self.vpzpath)+"')"
        self.set_default_value_verbose_name(vn)
        super(VpzPath, self).save(*args, **kwargs)

    def __str__(self):
        return "Vpz path id "+str(self.id)

    class Meta:
        verbose_name = "vpz path"
        verbose_name_plural = "vpz paths"


ht_vlebegin_verbose_name = "Human-readable name of the begin " + \
        "(optional, default value : 'begin')"
ht_vlebegin_value = "Begin value"
class VleBegin(DefaultVerboseNameMixin, models.Model):
    """Vle begin

    Attributes :

    - verbose_name : human-readable name
    - value : value (in vle, begin value)
    - vpzinput : relative :class:`VpzInput` (one :class:`VleBegin` to one :class:`VpzInput` relationship) 

    """

    verbose_name = models.CharField(max_length=200,
            blank=True, default='begin',
            help_text=ht_vlebegin_verbose_name)
    value = models.FloatField( blank=False,
            help_text=ht_vlebegin_value)
    #datevalue = models.DateTimeField( blank=True, default=0,
    #        help_text="just to see for the moment !!!")

    vpzinput = models.OneToOneField(VpzInput, related_name="vlebegin",
            verbose_name="related vpz input", blank=False )

    def save(self, *args, **kwargs):
        self.set_default_value_verbose_name()
        super(VleBegin, self).save(*args, **kwargs)

    def __str__(self):
        return "Begin id "+str(self.id)
                
    class Meta:
        verbose_name = "vle begin"
        verbose_name_plural = "vle begins"


ht_vleduration_verbose_name = "Human-readable name of the duration " + \
        "(optional, default value : 'duration')"
ht_vleduration_value = "Duration value"
class VleDuration(DefaultVerboseNameMixin, models.Model):
    """Vle duration

    Attributes :

    - verbose_name : human-readable name
    - value : value (in vle, duration value)
    - vpzinput : relative :class:`VpzInput` (one :class:`VleDuration` to one :class:`VpzInput` 
      relationship) 
    """

    verbose_name = models.CharField(max_length=200,
            blank=True, default='duration',
            help_text=ht_vleduration_verbose_name)
    value = models.FloatField( blank=False,
            help_text=ht_vleduration_value)

    vpzinput = models.OneToOneField(VpzInput, related_name="vleduration",
            verbose_name="related vpz input",
            blank=False )

    def save(self, *args, **kwargs):
        self.set_default_value_verbose_name()
        super(VleDuration, self).save(*args, **kwargs)

    def __str__(self):
        return "Duration id "+str(self.id)
                
    class Meta:
        verbose_name = "vle duration"
        verbose_name_plural = "vle durations"


ht_vlecond_name = "Condition name (same as in vle)"
ht_vlecond_verbose_name = "Human-readable name of the condition " + \
        "(optional, default value : name)"
ht_vlecond_vpzinput = "the vpz input to which the vle condition belongs"
class VleCond(DefaultVerboseNameMixin, models.Model):
    """Vle condition

    - verbose_name : human-readable name
    - name : name (in vle, condition name)
    - vlepar_list : list of relative :class:`VlePar` (many :class:`VlePar` to one :class:`VleCond`
      relationship)
    - vpzinput : relative :class:`VpzInput` (many :class:`VleCond` to one :class:`VpzInput` relationship) 
    """

    name = models.CharField(max_length=200,
            blank=False,
            help_text=ht_vlecond_name)
    verbose_name = models.CharField(max_length=200,
            blank=True,
            help_text=ht_vlecond_verbose_name)

    vpzinput = models.ForeignKey(VpzInput, related_name="vlecond_list",
            verbose_name="related vpz input",
            blank=False,
            help_text=ht_vlecond_vpzinput)


    def get_vlepar_selected(self):
        """Filter consisting in keeping only the selected :class:`VlePar` """

        vlepar_selected_list = self.vlepar_list.all().filter(selected=True)
        return vlepar_selected_list

    def save(self, *args, **kwargs):
        vn = "Condition '"+self.name+"'" + \
               " of vpz file '"+self.vpzinput.vpzact.vpzname+"'"
        self.set_default_value_verbose_name(vn)
        super(VleCond, self).save(*args, **kwargs)

    def __str__(self):
        return "Condition id "+str(self.id)+", '"+self.name+"'" + \
               " of vpz file '"+self.vpzinput.vpzact.vpzname+"'"

    def read_and_add_content(self, exp=None) :
        """reads :class:`VleCond` content (initial state) and add it into the
        database

        Reads into the vpz file (cf :class:`VpzAct`) the vle condition
        information (initial state) and add the relevant objects
        (:class:`VlePar`) into the database.
        The :class:`VleCond` is attached to a :class:`VpzInput` that is
        attached to a :class:`VpzAct`.
        """

        if exp is None :
            exp = Exp(vpzname=self.vpzinput.vpzact.vpzname, 
                    pkgname=self.vpzinput.vpzact.pkgname)
        parameter_name_list = exp.get_parameter_name_list(
                condition_name=self.name)
        for pname in parameter_name_list :
            parameter_type = exp.get_parameter_firstvalue_type(
                    condition_name=self.name, parameter_name=pname)
            parameter_value = exp.get_parameter_value(condition_name=self.name, 
                    parameter_name=pname)
            #self.vlepar_list.create(pname=pname, cname=self.name, 
            #        type=parameter_type, value=parameter_value, selected=True)
            self.vlepar_list.create(pname=pname, cname=self.name, 
                    type=parameter_type, value=json.dumps(parameter_value),
                    selected=True)

    def build_selection_name(self) :
        return (self.name)

    class Meta:
        verbose_name = "vle condition"
        verbose_name_plural = "vle conditions"


ht_vlepar_pname = "Name of the vle parameter (same as in vle)"
ht_vlepar_cname = "Name of the vle parameter condition (same as in vle)"
ht_vlepar_verbose_name = "Human-readable name of the vle parameter " + \
        "(optional, default value : name)"
ht_vlepar_type = "Type of the vle parameter (...of the first value)"
ht_vlepar_value = "Value of the vle parameter (...charfield for the moment)"
ht_vlepar_selected = "Selected or not for restitution"
ht_vlepar_vlecond = "the vle condition to which the vle parameter belongs"
class VlePar(DefaultVerboseNameMixin, models.Model):
    """Vle parameter (of a :class:`VleCond`)

    Attributes :

    - verbose_name : human-readable name ? 
    - pname : parameter name (in vle, port name)
    - cname : condition name (in vle, condition name)
    - type : value type (in vle, port value type)
    - value : value (in vle, port value)
    - selected : selection or not of the parameter for restitution
    - vlecond : relative :class:`VleCond` (many :class:`VlePar` to one :class:`VleCond` relationship)

    """
    pname = models.CharField(max_length=200, blank=False,
            help_text=ht_vlepar_pname)
    cname = models.CharField(max_length=200, blank=False,
            help_text=ht_vlepar_cname)
    verbose_name = models.CharField(max_length=200, blank=True,
            help_text=ht_vlepar_verbose_name)
    type = models.CharField(max_length=200, blank=False,
            help_text=ht_vlepar_type)
    value = models.CharField(max_length=1000000, blank=False,
            help_text=ht_vlepar_value)
    selected = models.BooleanField(blank=False, help_text=ht_vlepar_selected,
                                   default=False)
    vlecond = models.ForeignKey(VleCond, related_name="vlepar_list",
            verbose_name="related vle condition", blank=False,
            help_text=ht_vlepar_vlecond)

    def save(self, *args, **kwargs):
        vn = "Parameter '"+self.pname+"' of condition '"+ \
                self.cname+"'" + " of vpz file '"+ \
                self.vlecond.vpzinput.vpzact.vpzname+"'" 
        self.set_default_value_verbose_name(vn)
        super(VlePar, self).save(*args, **kwargs)

    def __str__(self):
        return "parameter id "+str(self.id)+", '"+self.pname+ \
                "' of condition '"+self.cname+"'" + " of vpz file '"+ \
                self.vlecond.vpzinput.vpzact.vpzname+"'" 

    def is_selected(self) :
        return self.selected
    def set_selected(self) :
        self.selected = True
    def unset_selected(self) :
        self.selected = False

    @classmethod
    def build_parameter_selection_name(cls, cname, pname):
        """Defines and returns the selection name of a parameter.
        
        Attention : if the selection name did not value cname.pname anymore, 
        then :meth:`get_vleparcompact_option_values <erecord_vpz.compact.views_mixins.VleParCompactOptionViewMixin.get_vleparcompact_option_values>`
        code should be modified/adapted !
        """
        return (cname+"."+pname)

    def build_selection_name(self) :
        """Defines and returns the selection name of a parameter.

        For consistency, must call build_parameter_selection_name (see \
        :meth:`get_vleparcompact_option_values <erecord_vpz.compact.views_mixins.VleParCompactOptionViewMixin.get_vleparcompact_option_values>`)
        """
        return self.build_parameter_selection_name(cname=self.cname,
                pname=self.pname)

    def get_val(self):
        return byteify(json.loads(self.value))

    class Meta:
        verbose_name = "vle parameter"
        verbose_name_plural = "vle parameters"

ht_vleview_name = "View name (same as in vle)"
ht_vleview_type = "View type (same as in vle)"
ht_vleview_timestep = "View timestep (same as in vle)"
ht_vleview_output_name = "View output name (same as in vle)"
ht_vleview_output_plugin = "View output plugin (same as in vle)"
ht_vleview_output_format = "View output format (same as in vle)"
ht_vleview_output_location = "View output location (same as in vle)"
ht_vleview_verbose_name = "Human-readable name of the view " + \
        "(optional, default value : name)"
ht_vleview_vpzinput = "the vpz input to which the vle view belongs"
class VleView(DefaultVerboseNameMixin, models.Model):
    """Vle view

    - verbose_name : human-readable name
    - name : name (in vle, view name)
    - type : type (in vle, view type)
    - timestep : time step (in vle, view time step)
    - output_name : output name (in vle, view output name)
    - output_plugin : output plugin (in vle, view output plugin)
    - output_format : output format (in vle, view output format)
    - output_location : output location (in vle, view output location)
    - vleout_list : list of relative :class:`VleOut` (many :class:`VleOut` to one :class:`VleView`
      relationship)
    - vpzinput : relative :class:`VpzInput` (many :class:`VleView` to one :class:`VpzInput`
      relationship) 
    """

    name = models.CharField(max_length=200, blank=False,
            help_text=ht_vleview_name)
    type = models.CharField(max_length=200, blank=False,
            help_text=ht_vleview_type)
    timestep = models.CharField(max_length=200, blank=False,
            help_text=ht_vleview_timestep)
    output_name = models.CharField(max_length=200, blank=False,
            help_text=ht_vleview_output_name)
    output_plugin = models.CharField(max_length=200, blank=False,
            help_text=ht_vleview_output_plugin)
    output_format = models.CharField(max_length=200, blank=False,
            help_text=ht_vleview_output_format)
    output_location = models.CharField(max_length=200, blank=False,
            help_text=ht_vleview_output_location)
    verbose_name = models.CharField(max_length=200,
            blank=True,
            help_text=ht_vleview_verbose_name)
    vpzinput = models.ForeignKey(VpzInput, related_name="vleview_list",
            verbose_name="related vpz input",
            blank=False,
            help_text=ht_vleview_vpzinput)

    def get_vleout_selected(self):
        """Filter consisting in keeping only the selected :class:`VleOut` """

        vleout_selected_list = self.vleout_list.all().filter(selected=True)
        return vleout_selected_list

    def save(self, *args, **kwargs):
        vn = "View '"+self.name+"'" + \
               " of vpz file '"+self.vpzinput.vpzact.vpzname+"'"
        self.set_default_value_verbose_name(vn)
        super(VleView, self).save(*args, **kwargs)

    def __str__(self):
        return "View id "+str(self.id)+", '"+self.name+"'" + \
               " of vpz file '"+self.vpzinput.vpzact.vpzname+"'"

    def read_and_add_content(self, exp=None) :
        """reads :class:`VleView` content (initial state) and add it into the database

        Reads into the vpz file (cf :class:`VpzAct`) the vle view information
        (initial state) and add the relevant objects (:class:`VleOut`) into
        the database.
        The :class:`VleView` is attached to a :class:`VpzInput` that is
        attached to a :class:`VpzAct`.
        """

        if exp is None :
            exp = Exp(vpzname=self.vpzinput.vpzact.vpzname, 
                    pkgname=self.vpzinput.vpzact.pkgname)

        viewsentries = exp.listViewsEntries()

        outputdata_name_list = viewsentries[self.name]
        for oname in outputdata_name_list :
            l = oname.split(".")
            if len(l) == 2 :
                modelname = l[0]
                shortname = l[1]
            nameinres = Exp.build_output_nameinres(nameinvle=oname)
            self.vleout_list.create(oname=nameinres, vname=self.name, 
                    shortname=shortname, selected=False)
            # old/previous version
            #self.vleout_list.create(oname=oname, vname=self.name, 
            #        shortname=shortname, selected=False)

    def is_storage_mode(self) :
        return (self.output_plugin == "storage")
    def is_file_mode(self) :
        return (self.output_plugin == "file")
    def is_activated(self, output_plugin_choice) :
        return (self.output_plugin == output_plugin_choice)
    def unactivate(self) :
        self.output_plugin = "dummy"
    def activate(self, output_plugin_choice):
        self.output_plugin = output_plugin_choice

    def build_selection_name(self) :
        return (self.name)

    class Meta:
        verbose_name = "vle view"
        verbose_name_plural = "vle views"


ht_vleout_oname = "Name of the vle output data (same as in vle)"
ht_vleout_vname = "Name of the vle output data view (same as in vle)"
ht_vleout_verbose_name = "Human-readable name of the vle output data " + \
        "(optional, default value : name)"
ht_vleout_shortname = "Shortname of the vle output data"
ht_vleout_selected = "Selected or not for retitution"
ht_vleout_vleview = "the vle view to which the vle output data belongs"
class VleOut(DefaultVerboseNameMixin, models.Model):
    """Vle output data (of a :class:`VleView`)

    Attributes :

    - verbose_name : human-readable name ? 
    - oname : output data name (in vle, port name)
    - vname : view name (in vle, view name)
    - shortname : short name
    - selected : selection or not of the output data for restitution
    - vleview : relative :class:`VleView` (many :class:`VleOut` to one :class:`VleView` relationship)

    """

    oname = models.CharField(max_length=200, blank=False,
            help_text=ht_vleout_oname)
    vname = models.CharField(max_length=200, blank=False,
            help_text=ht_vleout_vname)
    verbose_name = models.CharField(max_length=200, blank=True,
            help_text=ht_vleout_verbose_name)
    shortname = models.CharField(max_length=200, blank=False,
            help_text=ht_vleout_shortname)
    selected = models.BooleanField(blank=False, help_text=ht_vleout_selected,
                                   default=False)
    vleview = models.ForeignKey(VleView, related_name="vleout_list",
            verbose_name="related vle view", blank=False,
            help_text=ht_vleout_vleview)

    def save(self, *args, **kwargs):
        vn = "Output data '"+self.oname+"' of view '"+ \
                self.vname+"'" + " of vpz file '"+ \
                self.vleview.vpzinput.vpzact.vpzname+"'" 
        self.set_default_value_verbose_name(vn)
        super(VleOut, self).save(*args, **kwargs)

    def __str__(self):
        return "output data id "+str(self.id)+", '"+self.oname+ \
                "' of view '"+self.vname+"'" + " of vpz file '"+ \
                self.vleview.vpzinput.vpzact.vpzname+"'" 

    def is_selected(self) :
        return self.selected
    def set_selected(self) :
        self.selected = True
    def unset_selected(self) :
        self.selected = False

    @classmethod
    def build_output_selection_name(cls, vname, oname):
        """Defines and returns the selection name of an output data """
        return (vname+"."+oname)

    def build_selection_name(self) :
        """Defines and returns the selection name of an output data """
        return self.build_output_selection_name(vname=self.vname,
                oname=self.oname)
        #return ( self.vname +"."+ self.oname[::-1].replace(',',':',1)[::-1] )

    class Meta:
        verbose_name = "vle output data"
        verbose_name_plural = "vle output datas"

def vpzworkspace_clear(sender, instance, *args, **kwargs):
    """Deletes VpzWorkspace associated folders"""
    instance.clear_dirs()

ht_vpzworkspace_verbose_name = "Human-readable name of the vpz workspace"
ht_vpzworkspace_homepath = "absolute path of the workspace directory"
class VpzWorkspace(DefaultVerboseNameMixin, VpzWorkspaceMixin, models.Model):
    """Vpz workspace

    A VpzWorkspace is attached to a :class:`VpzAct`. It defines a workspace
    directory dedicated to the Vpz activity/manipulation.

    Attributes :

    - verbose_name : human-readable name ? 
    - vpzact : relative :class:`VpzAct` (one :class:`VpzAct` to one
      VpzWorkspace relationship) 
    - homepath : absolute path
    - vlehome : VLE_HOME path (defined if needed/used)
    - reporthome : report home path (defined if needed/used)

    todo : see when to be deleted, homepath deletion
    """

    verbose_name = models.CharField(max_length=200, blank=True,
            help_text=ht_vpzworkspace_verbose_name)

    homepath = models.CharField(max_length=200, blank=False,
            help_text=ht_vpzworkspace_homepath)

    vlehome = models.CharField(max_length=200, blank=True, default=None)
    reporthome = models.CharField(max_length=200, blank=True, default=None)

    vpzact = models.OneToOneField(VpzAct, related_name="vpzworkspace", 
            blank=False )

    @classmethod
    def create(cls, vpzact, as_vlehome=False, as_reporthome=False):
        """create a VpzWorkspace attached to vpzact """
        vpzworkspace = cls(vpzact=vpzact)
        vpzworkspace.define_and_build(as_vlehome=as_vlehome,
                as_reporthome=as_reporthome)
        return vpzworkspace

    def clean(self):
        """verifies that path directories exist"""

        if not os.path.isdir(self.homepath) :
            msg = "unavailable homepath directory '"+self.homepath+"'"
            raise ValidationError(msg)
        #if not self.is_undefined_value(self.vlehome):
        #    if not os.path.isdir(self.vlehome) :
        #        msg = "unavailable vlehome directory '"+self.vlehome+"'"
        #        raise ValidationError(msg)
        #if not self.is_undefined_value(self.reporthome):
        #    if not os.path.isdir(self.reporthome) :
        #        msg = "unavailable reporthome directory '"+self.reporthome+"'"
        #        raise ValidationError(msg)

    def save(self, *args, **kwargs):
        self.set_default_value_verbose_name()
        super(VpzWorkspace, self).save(*args, **kwargs)

    def __str__(self):
        return "Vpz workspace id "+str(self.id)

    class Meta:
        verbose_name = "vpz workspace"
        verbose_name_plural = "vpz workspaces"

# done before a VpzWorkspace deletion
pre_delete.connect(vpzworkspace_clear, sender=VpzWorkspace)

