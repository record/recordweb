# -*- coding: utf-8 -*-
"""erecord_vpz.admin"""

from django.contrib import admin

from erecord_vpz.models import VpzAct
from erecord_vpz.models import VpzOrigin
from erecord_vpz.models import VpzInput
from erecord_vpz.models import VpzOutput
from erecord_vpz.models import VpzWorkspace
from erecord_vpz.models import VpzPath

from erecord_vpz.forms import VpzActForm
from erecord_vpz.forms import VpzPathForm

# To show/hide some admin parts

from django.contrib.auth.models import User
from django.contrib.auth.models import Group
#admin.site.unregister(User)
#admin.site.unregister(Group)

class VpzActInline(admin.TabularInline):
    model = VpzAct
class VpzOriginInline(admin.TabularInline):
    model = VpzOrigin
class VpzInputInline(admin.TabularInline):
    model = VpzInput
class VpzOutputInline(admin.TabularInline):
    model = VpzOutput
class VpzWorkspaceInline(admin.TabularInline):
    model = VpzWorkspace

vpzact_general_description = \
        "VpzAct can be created from a VpzPath (of this 'vpz' database) or \
        from a VleVpz (of the 'db' database). \
        It is made possible to change a vpz but it should be carefully done, keeping it consistent with its related objects (VpzOrigin, VpzInput, VpzOutput...)."
vpzact_identification_description = \
        "This information is automatically calculated from the original \
        information (VpzOrigin relative to a VpzPath or to a VleVpz)."

def delete_too_old_activity(modeladmin, request, queryset):
    for vpzact in queryset:
        vpzact.delete_if_old()
delete_too_old_activity.short_description = "Delete selected vpz activities if too old"

class VpzActAdmin(admin.ModelAdmin):
    form = VpzActForm
    list_display = ('vpzname', 'pkgname', 'vlepath', 'verbose_name', )
    actions = [delete_too_old_activity]
    inlines = [VpzInputInline, VpzOutputInline, VpzOriginInline,
               VpzWorkspaceInline,]
    search_fields = ['vpzname']

    fieldsets = (
        (None, {
            'description': vpzact_general_description,
            'fields': (),
        }),
        (None, {
            'fields': ('verbose_name',),
        }),
        ('Identification', {
            'description' : vpzact_identification_description,
            #'classes': ('collapse',),
            'fields': ('vpzname', 'pkgname', 'vlepath',),
        }),
    )

admin.site.register(VpzAct, VpzActAdmin)

vpzorigin_general_description = \
    "...Vpz origin..."
class VpzOriginAdmin(admin.ModelAdmin):
            # django 1.7.6, error :
            # <class 'erecord_vpz.admin.VpzActInline'>:
            # (admin.E202) 'erecord_vpz.VpzAct' has
            # no ForeignKey to 'erecord_vpz.VpzOrigin'.
    #inlines = [VpzActInline]
    fieldsets = (
        (None, {
            'description': vpzorigin_general_description,
            'fields': ( 'text', ),
        }),
    )
admin.site.register(VpzOrigin, VpzOriginAdmin)

vpzinput_general_description = \
    "...Vpz input..."
class VpzInputAdmin(admin.ModelAdmin):
            # django 1.7.6, error :
            # <class 'erecord_vpz.admin.VpzActInline'>:
            # (admin.E202) 'erecord_vpz.VpzAct' has
            # no ForeignKey to 'erecord_vpz.VpzInput'.
    # inlines = [VpzActInline]
    fieldsets = (
        (None, {
            'description': vpzinput_general_description,
            'fields': (),
        }),
    )
admin.site.register(VpzInput, VpzInputAdmin)

vpzoutput_general_description = \
    "...Vpz output..."
class VpzOutputAdmin(admin.ModelAdmin):
            # django 1.7.6, error :
            # <class 'erecord_vpz.admin.VpzActInline'>:
            # (admin.E202) 'erecord_vpz.VpzAct' has
            # no ForeignKey to 'erecord_vpz.VpzOutput'.
    # inlines = [VpzActInline]
    fieldsets = (
        (None, {
            'description': vpzoutput_general_description,
            'fields': (),
        }),
    )
admin.site.register(VpzOutput, VpzOutputAdmin)

vpzworkspace_general_description = \
    "...Vpz workspace..."
class VpzWorkspaceAdmin(admin.ModelAdmin):
    # inlines = [VpzActInline]
    fieldsets = (
        (None, {
            'description': vpzworkspace_general_description,
            'fields': ( 'verbose_name', 'homepath', 'vlehome', 'reporthome', ), 
        }),
    )
admin.site.register(VpzWorkspace, VpzWorkspaceAdmin)

vpzpath_general_description = \
        "This information will be able to be used to 'choose' a Vpz."

class VpzPathAdmin(admin.ModelAdmin):
    form = VpzPathForm
    list_display = ('verbose_name', 'vpzpath',)
    search_fields = ['verbose_name']

    fieldsets = (
        (None, {
            'description': vpzpath_general_description,
            'fields': (),
        }),
        ('Vpz file absolute path', {
            'fields': ('verbose_name', 'vpzpath',),
        }),
    )

admin.site.register(VpzPath, VpzPathAdmin)

#from erecord_vpz import models as erecord_vpz_models
#from django.db.models.base import ModelBase
#othersite = admin.AdminSite('othersite')
#for name, var in erecord_vpz_models.__dict__.items():
#    if type(var) is ModelBase:
#        othersite.register(var)

