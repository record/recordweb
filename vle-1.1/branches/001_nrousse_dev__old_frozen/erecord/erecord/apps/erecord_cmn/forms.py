# -*- coding: utf-8 -*-
"""erecord_cmn.forms

Forms common to erecord applications

"""

from django import forms
#from django.forms import ModelForm

# Some forms common to erecord applications

class ReadonlyForm(forms.ModelForm):
#class ReadonlyForm(ModelForm):
    """Form with all fields readonly"""
    def __init__(self, *args, **kwargs):
        super(ReadonlyForm, self).__init__(*args, **kwargs)
        # read-only fields
        for (n,f) in self.fields.iteritems() :
            f.widget=forms.Textarea(attrs={'rows':1, 'cols':60})
            f.widget.attrs['readonly'] = True

