# -*- coding: utf-8 -*-
"""erecord_cmn.renderers

Renderers common to erecord applications

"""

from rest_framework.renderers import BaseRenderer

# Some renderers common to erecord applications

#from django.utils.encoding import smart_unicode  ...PlainTextRenderer

class ZIPRenderer(BaseRenderer):
    media_type = 'application/zip'
    format = 'zip'
    #charset = None
    #render_style = 'binary'

    def render(self, data, media_type=None, renderer_context=None):
        return data


class PlainTextRenderer(BaseRenderer):
    media_type = 'text/plain'
    format = 'txt'

    def render(self, data, media_type=None, renderer_context=None):
        return data.encode(self.charset)

class PdfRenderer(BaseRenderer):
    #media_type = 'application/pdf'
    media_type = 'application/zip' # a zipped pdf
    format = 'pdf'
    #charset = None
    #render_style = 'binary'

    def render(self, data, media_type=None, renderer_context=None):
        return data

class CsvRenderer(BaseRenderer):
    #media_type = 'text/csv'
    media_type = 'application/zip' # a zipped csv
    format = 'csv'
    #charset = None
    #render_style = 'binary'

    def render(self, data, media_type=None, renderer_context=None):
        return data
