# -*- coding: UTF-8 -*-
"""erecord_cmn.views """

from django.shortcuts import render

from erecord_cmn.configs.config import ONLINEDOC_URL

#headsubtitle = "erecord_cmn application (common)"
headsubtitle = "(erecord_cmn)"

#------------------------------------------------------------------------------

def onlinedoc_page(request):
    """online documentation page """
    context = { 'ONLINEDOC_URL': ONLINEDOC_URL, }
    return render(request, 'erecord_cmn/onlinedoc.html', context)

#------------------------------------------------------------------------------
