# -*- coding: UTF-8 -*-
"""erecord_cmn.configs.config

Contains the configuration : paths, constants...

"""

import os
base_dir = os.path.dirname(os.path.abspath(__file__))
"""base directory path"""

PACKAGE_HOME = os.path.normpath(os.path.join(base_dir, "..", "..", ".."))
"""erecord package home path"""

PROJECT_HOME = os.path.normpath(os.path.join(PACKAGE_HOME, ".."))
"""erecord project home path"""

factory_path = os.path.normpath(os.path.join(PROJECT_HOME, "factory"))
"""factory path"""

REPOSITORIES_HOME = os.path.normpath(os.path.join(PROJECT_HOME, "repositories"))
"""REPOSITORIES_HOME"""

IN_PRODUCTION = False # True or False
"""IN_PRODUCTION value False or True \n
In case of IN_PRODUCTION, no console destination for logging information ...
"""

#ONLINEDOC_URL = ''
ONLINEDOC_URL = 'http://erecord.toulouse.inra.fr/docs.html'
"""online documentation URL (erecord web site)"""

RUN_HOME = os.path.join(factory_path, "run")
"""run workspace path, workspace divided by applications"""

RUN_HOME_APP_VPZ = os.path.join(RUN_HOME, "erecord_vpz")
"""run workspace of erecord_vpz app"""
REQUESTS_HOME_APP_VPZ = os.path.join(RUN_HOME_APP_VPZ, "requests")
"""requests into run workspace of erecord_vpz app"""

DOWNLOADS_HOME = "slm" # from MEDIA_ROOT
"""media (downloadable and uploaded files) structure (whose path is 
MEDIA_ROOT) \n
DOWNLOADS_HOME is a 'root' dedicated to erecord_slm (the application managing
downloadable and uploaded files), divided by applications (see erecord_slm). \n
DOWNLOADS_HOME path is relative to MEDIA_ROOT.
"""
DOWNLOADS_HOME_APP_VPZ = os.path.join(DOWNLOADS_HOME, "erecord_vpz")
"""media about erecord_vpz app"""

# log

LOGGING_FILE = os.path.join(factory_path, 'log', 'erecord.log' )
"""File destination of logging information \n
Value None for no file destination for logging information
"""

# databases

DB_NAME_DEFAULT = 'default'
"""Database name of default database (admin...)"""

DB_PATH_DEFAULT =  os.path.join(PROJECT_HOME, 'databases',
                                'erecord_default.sqlite3')
"""Database file path of DB_NAME_DEFAULT database"""

DB_NAME_DB = 'db'
"""Database name of database associated with erecord_db
(models repositories and their tree : vle packages, vpz files)
"""

DB_PATH_DB =  os.path.join(PROJECT_HOME, 'databases', 'erecord_db.sqlite3')
"""Database file path of DB_NAME_DB database"""

DB_NAME_VPZ = 'vpz'
"""Database name of database associated with erecord_vpz
(vpz manipulations : inputs, outputs...)
"""

DB_PATH_VPZ =  os.path.join(PROJECT_HOME, 'databases',
                            'erecord_vpz_manipulations.sqlite3')
"""Database file path of DB_NAME_VPZ database"""

DB_NAME_SLM = 'slm'
"""Database name of database associated with erecord_slm
(downloadable and uploaded files management)
"""

DB_PATH_SLM =  os.path.join(PROJECT_HOME, 'databases',
                            'erecord_slm_download_management.sqlite3')
"""Database file path of DB_NAME_SLM database"""

# Media files (downloadable (and later uploaded ?) files)

MEDIA_DESTINATION_HOME = os.path.join(factory_path, 'media')
"""Media files home directory path (where a subdirectory for each project)"""

# Static files

STATIC_DESTINATION_HOME =  os.path.join(factory_path, 'static')
"""Static files home directory path (where a subdirectory for each project)"""

STATIC_APP_CMN = os.path.join(PACKAGE_HOME, 'apps', 'erecord_cmn', 'static')
"""Static files of erecord_cmn app"""
#STATIC_APP_XXX = os.path.join(PACKAGE_HOME, 'apps', 'xxx', 'static')


# templates

## templates of each app
TEMPLATE_APP_CMN = os.path.join(PACKAGE_HOME, 'apps', 'erecord_cmn',
                                'templates')
"""templates of erecord_cmn app"""
TEMPLATE_APP_DB = os.path.join(PACKAGE_HOME, 'apps', 'erecord_db',
                                'templates')
"""templates of erecord_db app"""
TEMPLATE_APP_VPZ = os.path.join(PACKAGE_HOME, 'apps', 'erecord_vpz',
                                'templates')
"""templates of erecord_vpz app"""
TEMPLATE_APP_SLM = os.path.join(PACKAGE_HOME,
                                'apps', 'erecord_slm', 'templates')
"""templates of erecord_slm app"""
#TEMPLATE_APP_XXX = os.path.join(PACKAGE_HOME, 'apps', 'xxx', 'templates')


# some software behavior options

ACTIVITY_PERSISTENCE = False # True or False
"""ACTIVITY_PERSISTENCE value False or True \n
ACTIVITY_PERSISTENCE=True : the activities are kept into the database. \n
ACTIVITY_PERSISTENCE=False : the activities are deleted after restitution. \n
An activity is a VpzAct and its 'dependencies/tree' (VpzInput, VpzOutput...)
"""

ACTIVITY_LIFETIME = 31
"""Age criterion to delete VpzAct into database and associated folders (see
delete_if_old method, and also delete_too_old_activity admin command) \n
unit : number of days
"""

DOWNLOAD_LIFETIME = 21
"""Age criterion to delete a folder into download space into factory (see
delete_if_old method, and also delete_too_old_downloadvpzdocument admin
command) \n
unit : number of days
"""

UPLOAD_LIFETIME = 21
"""Age criterion to delete a folder into upload space into factory (see
delete_if_old method, and also delete_too_old_uploadvpzdocument admin
command) \n
unit : number of days \n
nb : upload space is unused for the moment
"""

