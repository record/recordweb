# -*- coding: utf-8 -*-
"""erecord_cmn.serializers

Serializers common to erecord applications

"""

from rest_framework import serializers

# Some serializer common to erecord applications

def getKeyOptionValues(data, key):
    """returns list of values of key option"""

    #print "...... getKeyOptionValues, data : ", data, type(data)
    from django.http.request import QueryDict
    if isinstance(data, QueryDict) :
        #d = dict(data._iterlists())
        #s = d.get(key, [])
        s = data.getlist(key)
    elif isinstance(data, dict) :
        s = data.get(key, [])
        if not isinstance(s, list) :
            s = [ s ]
    #print "...... getKeyOptionValues returns : ", s
    return s

class FormatOptionSerializer(serializers.Serializer):
    """Option 'format' """
    format = serializers.CharField( required=False )
    def validate(self, attrs):
        if 'format' not in attrs.keys() :
            attrs['format']=None
        return attrs

