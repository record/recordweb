# -*- coding: UTF-8 -*-
"""erecord_cmn.utils.errors

Utils to manage errors

"""

import traceback

from rest_framework import status
from django.http import Http404

def logger_report_error(LOGGER) :
    """ Reports an error to log """
    LOGGER.error(u"Error report :\n %s ", traceback.format_exc())

def build_error_message(error=None, msg=None) :
    """builds a message error 
    
    The message is composed by msg (optional) and error message(s) (optional) 
    """
    txt = ""
    if msg is not None:
        txt = msg + " -- "
    if error is not None:
        if hasattr(error, 'message'):
            txt = txt + error.message
        if hasattr(error, 'messages'):
            txt = txt + "; ".join(error.messages)
    return txt

class Http400(Exception):
    pass

def get_error_status(e) :
    s = status.HTTP_500_INTERNAL_SERVER_ERROR # default
    if isinstance(e, Http404):
        s = status.HTTP_404_NOT_FOUND
    elif isinstance(e, Http400):
        s = status.HTTP_400_BAD_REQUEST
    else :
        s = status.HTTP_500_INTERNAL_SERVER_ERROR
    return s

