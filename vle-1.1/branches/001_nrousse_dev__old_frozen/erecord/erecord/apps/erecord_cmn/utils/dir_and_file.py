# -*- coding: utf-8 -*-
"""erecord_cmn.utils.dir_and_file

Methods on directories and files

"""

import os
import shutil
import csv
import uuid
import xlwt


def create_dir_if_absent(dirpath):
    """ Create the directory dirpath if dirpath does not yet exist.

    Returns True if the creation has been done.
    """

    creation_done = False
    if not os.path.exists(dirpath):
        try:
            os.mkdir(dirpath)
            creation_done = True
        except Exception as e :
            msg = "unable to create directory "+dirpath
            raise Exception(msg)
        return creation_done

def create_dirs_if_absent(path_list):
    for dirpath in path_list:
        create_dir_if_absent(dirpath)

def delete_path_if_present(path):
    """ Delete the path if it exists.

    Returns True if the deletion has been done.
    """

    deletion_done = False
    if os.path.exists(path):
        try:
            if os.path.isfile(path):
                os.remove(path)
                deletion_done = True
            elif os.path.isdir(path):
                shutil.rmtree(path)
                deletion_done = True
        except Exception as e :
            msg = "unable to delete "+dirpath
            raise Exception(msg)
    return deletion_done

def clean_dir(dirpath):
    """ Create the directory dirpath after having deleting dirpath if still
    existing.
    """

    deletion_done = delete_path_if_present(dirpath)
    creation_done = create_dir_if_absent(dirpath)
    return (deletion_done, creation_done)

def make_csv_file(row_list, filename, dirpath, delimiter=";"):
    """Builds a csv file from row_list and saves it as filename under dirpath
    """

    csvfile = os.path.join(dirpath, filename)
    file = open(csvfile, "wb")
    c = csv.writer(file, delimiter=delimiter)
    for row in row_list:
        c.writerow(row)
    file.close()

def make_zip_folder(folderpath):
    """Builds the zip file of the folderpath.
        
    Returns the zip file path name.
    """
    format = 'zip'
    shutil.make_archive( folderpath, format, folderpath)
    zip_path = folderpath+'.'+format
    return zip_path

def get_available_pathname(rootpath, rootname):
    """Defines and returns a path name that doesn't yet exist.

    The path name is based on rootname, is the one of a rootpath
    subdirectory.

    The pathname directory should be immediately created !
    """
    found = False
    while not found:
        pseudoid = str(uuid.uuid4())
        name = rootname + pseudoid
        pathname = os.path.join(rootpath, name)
        if not os.path.exists(pathname):
            found = True
    return pathname

class Worksheet(object):
    """further for a xlwt.Worksheet"""

    default_style = xlwt.XFStyle()

    head_style = xlwt.XFStyle()
    font = xlwt.Font()
    font.bold = True
    head_style.font = font

    @classmethod
    def write_title(cls, ws, row_ini, title):
        """Writes a title into a xlwt.Worksheet """
    
        ws.write(row_ini, 0, title, style=cls.head_style)
        return row_ini
    
    @classmethod
    def write_line_feed(cls, ws, row_ini):
        """Writes a line feed into a xlwt.Worksheet """
    
        ws.write(row_ini, 0, "")
        return row_ini
    
    @classmethod
    def write_lines_byrow(cls, ws, row_ini, lines, nb_head=0, col_main=[]):
        """Writes lines into a xlwt.Worksheet (a row by line) """
    
        last_row = 0
        for rabs,row in enumerate(lines):
            r = rabs + row_ini
            for c,col in enumerate(row) :
                if rabs < nb_head or c in col_main :
                    style=cls.head_style
                else :
                    style=cls.default_style
                ws.write(r, c, col, style=style)
            last_row = r
        return last_row

    @classmethod
    def write_lines_bycol(cls, ws, row_ini, lines, nb_head=0, row_main=[]):
        """Writes lines into a xlwt.Worksheet (a col by line) """
    
        last_row = 0
        for r,row in enumerate(lines):
            for cabs,col in enumerate(row) :
                if cabs < nb_head or cabs in row_main :
                    style=cls.head_style
                else :
                    style=cls.default_style
                c = cabs + row_ini
                ws.write(c, r, col, style=style)
            last_row = c
        return last_row

    @classmethod
    def write_group_byrow(cls, ws, info_list, nb_head=0, col_main=[], row_ini=0,
                          title=None):
        """Writes title and info_list into a xlwt.Worksheet (a row by data)"""

        if title is not None :
            last_row = cls.write_title(ws=ws, row_ini=row_ini, title=title)
            last_row = cls.write_line_feed(ws=ws, row_ini=last_row+1)
            row_ini = last_row + 1
        last_row = cls.write_lines_byrow(ws=ws, row_ini=row_ini,
                           lines=info_list, nb_head=nb_head, col_main=col_main)
        return last_row

    @classmethod
    def write_group_bycol(cls, ws, info_list, nb_head=0, row_main=[], row_ini=0,
                          title=None):
        """Writes title and info_list into a xlwt.Worksheet (a col by data)"""

        if title is not None :
            last_row = cls.write_title(ws=ws, row_ini=row_ini, title=title)
            last_row = cls.write_line_feed(ws=ws, row_ini=last_row+1)
            row_ini = last_row + 1
        last_row = cls.write_lines_bycol(ws=ws, row_ini=row_ini,
                           lines=info_list, nb_head=nb_head, row_main=row_main)
        return last_row


