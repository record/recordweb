# -*- coding: utf-8 -*-
"""erecord_cmn.utils.vle

Vle interface methods

Uses python os, os.path libraries, uses pyvle.

"""

import os
from pyvle import VlePackage
from pyvle import Vle
from erecord_cmn.utils.dir_and_file import make_csv_file
from erecord_cmn.utils.coding import byteify

def setenv_vle_home( vlehome_path ) :
    """Set the VLE_HOME environment variable"""

    os.environ['VLE_HOME'] = vlehome_path

def set_vle_home( vlehome_path ) :
    """Set the VLE_HOME environment (variable and directory)"""

    try:
        setenv_vle_home(vlehome_path)
        VlePackage.getInstalledPackages()
    except OSError as e:
        msg = "unable to set vle_home "+vlehome_path
        raise Exception(msg)

def is_structured_as_vle_home( path ) :
    """determines if the path directory is structured as a VLE_HOME directory.
    
    Returns True if the path directory contains a pkgs subdirectory.

    """

    res = False
    pkgs_path = get_rep_pkgs_path(path)
    if os.path.exists(pkgs_path) and os.path.isdir(pkgs_path) :
        res = True
    return res

def is_pkg_of_rep(rep_path,pkgname) :
    """determines if a vle package exists (as a vle package) under a models
    repository path.

    The vle package name is pkgname.
    The models repository path is rep_path.

    Suppose that rep_path is a models repository path.
    Suppose that pkgname is a vle package.
    """
    if pkgname in get_rep_pkgname_list( rep_path ) :
        return True
    return False

def is_vpz_of_pkg_of_rep(rep_path, pkgname, vpzname) :
    """determines if a vpz file exists as a vpz file of vle package under a
    models repository path.

    The vpz file name is vpzname.
    The vle package name is pkgname.
    The models repository path is rep_path.

    Suppose that rep_path is a models repository path.
    Suppose that pkgname is a vle package of the models repository.
    """

    res = False
    vpz_path = os.path.join(get_rep_pkg_exp_path(rep_path=rep_path,
        pkgname=pkgname), vpzname)
    if os.path.exists(vpz_path) and os.path.isfile(vpz_path) :
        res = True
    return res


def get_pkgs_name() :
    return 'pkgs-1.1'

def get_exp_name() :
    return 'exp'

def get_data_name() :
    return 'data'

def get_output_name() :
    return 'output'

def get_rep_pkgs_path( rep_path ) :
    """returns the 'pkgs' directory path of a models repository
    
    The models repository is located at rep_path, structured as a VLE_HOME
    directory.
    """

    return os.path.join(rep_path, get_pkgs_name())

def get_rep_pkgname_list( rep_path ) :
    """returns the list of the vle packages names of a models repository.

    The models repository is located at rep_path, structured as a VLE_HOME
    directory.
    
    The vle packages taken into account are those found (as vle packages)
    under rep_path.
    """

    pkgname_list = list()
    rep_pkgs_path = get_rep_pkgs_path(rep_path)
    if os.path.exists(rep_pkgs_path) and os.path.isdir(rep_pkgs_path) :
        pkgname_list = os.listdir(rep_pkgs_path)
    for pkgname in pkgname_list :
        if not os.path.isdir(os.path.join(rep_pkgs_path, pkgname)) :
            pkgname_list.remove(pkgname)
    return pkgname_list


def get_rep_pkg_exp_path( rep_path, pkgname ) :
    """returns the 'exp' directory path of a models repository vle package.
    
    The models repository is located at rep_path, structured as a VLE_HOME
    directory.
    The vle package name is pkgname.
    The vpz files taken into account are those found (as vpz files of the
    pkgname vle package) under rep_path.
    """

    return os.path.join(get_rep_pkgs_path(rep_path), pkgname, get_exp_name())

def get_rep_pkg_data_path( rep_path, pkgname ) :
    """returns the 'data' directory path of a models repository vle package.
    
    The models repository is located at rep_path, structured as a VLE_HOME
    directory.
    The vle package name is pkgname.
    """

    return os.path.join(get_rep_pkgs_path(rep_path), pkgname, get_data_name())

def get_rep_pkg_output_path( rep_path, pkgname ) :
    """returns the 'output' directory path of a models repository vle package.
    
    The models repository is located at rep_path, structured as a VLE_HOME
    directory.
    The vle package name is pkgname.
    """

    return os.path.join(get_rep_pkgs_path(rep_path), pkgname, get_output_name())

def get_pkg_output_path( pkg_path ) :
    """returns the 'output' directory path of a vle package.
    
    The vle package is located at pkg_path.
    """

    return os.path.join(pkg_path, get_output_name())

def get_vlepath_pkg_name( vpzpath, limit ):
    """returns (vlepath, pkg, name) of a vpz file absolute path.
    
    vpzpath is the vpz file absolute path.

    vlepath is the vle path (models repository path), structured as a VLE_HOME
    directory.
    pkg is the vle package name.
    name is the vpz file name (path relative to 'exp' directory, with .vpz
    extension) (same as in vle)
    limit is the path not to reach (when searching 'exp' into vpzpath).

    """

    if not os.path.isfile(vpzpath) :
        return None
    #if vpzpath not 'under' limit : return None

    finished = False
    exp_path = os.path.dirname(vpzpath)
    while not finished :
        if os.path.basename(exp_path) == get_exp_name():
            finished = True
        elif os.path.samefile(exp_path,limit) :
            finished = True
        else :
            exp_path = os.path.dirname(exp_path)
    if os.path.basename(exp_path) == get_exp_name():
        name = os.path.relpath( vpzpath, exp_path )
        pkg_path = os.path.dirname(exp_path)
        vlepath = os.path.dirname( os.path.dirname(pkg_path))
        pkg = os.path.basename(pkg_path)
        if is_structured_as_vle_home(vlepath) :
            return (vlepath,pkg,name)
    return None



TYPE_VALUE_DOUBLE   = 'double'
TYPE_VALUE_INTEGER  = 'integer'
TYPE_VALUE_STRING   = 'string'
TYPE_VALUE_BOOLEAN  = 'boolean'
TYPE_VALUE_MAP      = 'map'
TYPE_VALUE_SET      = 'set'
TYPE_VALUE_TUPLE    = 'tuple'
TYPE_VALUE_TABLE    = 'table'
TYPE_VALUE_XMLTYPE  = 'xml'
TYPE_VALUE_MATRIX   = 'matrix'
TYPE_VALUE_NIL      = 'none'

class Exp( Vle ):
    """ To manipulate a vpz file (interface with vle) : read, write, run"""

    def __init__(self, vpzname, pkgname):
       self.vpzname = vpzname.encode('utf8')
       self.pkgname = pkgname.encode('utf8')
       Vle.__init__(self, self.vpzname, self.pkgname)

    def get_begin_value(self):
        return self.getBegin()

    def get_duration_value(self):
        return self.getDuration()

    def get_condition_name_list(self):
        return self.listConditions()

    def get_parameter_name_list(self, condition_name):
        """the list of names of parameters of condition_name condition"""
        return self.listConditionPorts(condition_name)

    def get_parameter_firstvalue_type(self, condition_name, parameter_name):
        return self.getConditionValueType(condition_name, parameter_name, 0)

    def get_parameter_value(self, condition_name, parameter_name):
        """the value of the parameter of the condition (list of values) """
        #!!!return self.getConditionPortValues(condition_name, parameter_name)
        return self.getConditionSetValue(condition_name, parameter_name)

    def clear_parameter_value(self, condition_name, parameter_name):
        """clears the value of the parameter of the condition"""
        self.clearConditionPort(condition_name, parameter_name)

    def add_parameter_value(self, condition_name, parameter_name, type, value):
        """adds value as type to the value of the parameter of the condition
        
        Some modifications are done in str, long, int, float cases
        """

        #value = byteify(value)
        #if isinstance(value, str):
        #    value = value.encode('utf8')

        if type == TYPE_VALUE_BOOLEAN :
            self.addBooleanCondition(condition_name, parameter_name, value)
        elif type == TYPE_VALUE_INTEGER :
            if isinstance(value, float) or isinstance(value, long) :
                value = int(value)
            self.addIntegerCondition(condition_name, parameter_name, value)
        elif type == TYPE_VALUE_DOUBLE :
            if isinstance(value, int) or isinstance(value, long) :
                value = float(value)
            self.addRealCondition(condition_name, parameter_name, value)
        elif type == TYPE_VALUE_STRING :
            self.addStringCondition(condition_name, parameter_name, value)
        elif type == TYPE_VALUE_MAP :
            self.addMapCondition(condition_name, parameter_name, value)
        elif type == TYPE_VALUE_SET :
            self.addSetCondition(condition_name, parameter_name, value)
        elif type == TYPE_VALUE_MATRIX :
            self.addMatrixCondition(condition_name, parameter_name, value)
        elif type == TYPE_VALUE_TABLE :
            self.addTableCondition(condition_name, parameter_name, value)
        elif type == TYPE_VALUE_TUPLE :
            self.addTupleCondition(condition_name, parameter_name, value)
        elif type == TYPE_VALUE_XMLTYPE :
            self.addXMLCondition(condition_name, parameter_name, value)

    def update(self, vpzinput) :
        """updates according to VpzInput

        The value p.value of a parameter p (VlePar) of a condition is supposed
        to always be given as multiple, in the case of its type p.type is
        'list'.
        The case where p.value of 'list' p.type would be given as a singleton
        is not treated.

        The only modification on views is to take into account is_storage_mode
        """

        self.setBegin(vpzinput.vlebegin.value)
        self.setDuration(vpzinput.vleduration.value)

        for c in vpzinput.vlecond_list.all() :
            for p in c.vlepar_list.all() :
                val = p.get_val()
                cname = byteify(p.cname) #cname = p.cname.encode('utf8')
                pname = byteify(p.pname) #pname = p.pname.encode('utf8')
                self.clear_parameter_value(condition_name=cname,
                        parameter_name=pname)

                if isinstance(val, list) :
                    for i,v in enumerate(val) :
                        self.add_parameter_value( condition_name=cname,
                                parameter_name=pname, type=p.type, value=v)
                else : # singleton case
                    self.add_parameter_value( condition_name=cname,
                            parameter_name=pname, type=p.type, value=val)

        for v in vpzinput.vleview_list.all() :
            viewname = byteify(v.name) #viewname = v.name.encode('utf8')
            if v.is_storage_mode() :
                self.set_storage_mode(viewname)
            else :
                self.unset_storage_mode(viewname)

# erreur simu en 'file'
# "detail": "Unable to satisfy the request -- Simulation running error ... \n/!\\ vle error reported: vle::utils::ArgError\nOutput plug-in 'file': cannot open file 'exp_view'"

    def set_storage_mode(self, view):
        """set view in storage mode"""
        output = self.getViewOutput(view)
        self.setOutputPlugin(output=output, location='', format='local',
                             plugin='storage', package='vle.output')

    def unset_storage_mode(self, view):
        """unset storage mode of the view (by setting dummy mode)"""
        output = self.getViewOutput(view)
        self.setOutputPlugin(output, '', 'local', 'dummy', 'vle.output')

    def set_all_storage_mode(self):
        """set all views in storage mode"""
        for view in self.listViews():
            self.set_storage_mode(view)


    def save_as_vpz(self, vpzname, dirpath):
        """Saves as a vpz file named vpzname under dirpath """

        vpzfile = os.path.join(dirpath, vpzname)
        self.save(vpzfile.encode('utf8'))

    def save_as_csv(self, filename, dirpath):
        """Saves as a csv file named filename under dirpath """

        r = list()
        r.append(["*** General ***",])
        r.append(["begin", "duration",])
        r.append([self.getDuration(), self.getBegin(),])
        r.append(["*** Parameters ***",])
        r.append(["condition name", "parameter name", "value(s)",
            "type (of first value)", "other name",])
        for cname in self.listConditions():
            for pname in self.listConditionPorts(cname) :
                row = [ cname, pname, 
                        self.getConditionSetValue(cname, pname), 
                        self.getConditionValueType(cname, pname, 0),
                        cname+'.'+pname, ]
                r.append(row)
        make_csv_file(row_list=r, filename=filename, dirpath=dirpath,
                delimiter=";")


    @classmethod
    def build_output_nameinres(cls, nameinvle):
        """Builds and returns the nameinres value of an output data from its
        nameinvle value.

        nameinvle comes from the Vle method listViewsEntries.

        nameinres comes from the result produced by a vle simulation.

        nameinres looks like nameinvle but is quite different \
        (cf ':' instead of ',')
        """
        nameinres = nameinvle[::-1].replace(',',':',1)[::-1]
        return nameinres

    @classmethod
    def build_output_nameinvle(cls, nameinres):
        """Builds and returns the nameinvle value of an output data from its
        nameinres value.

        nameinvle comes from the Vle method listViewsEntries.

        nameinres comes from the result produced by a vle simulation.

        nameinres looks like nameinvle but is quite different \
        (cf ':' instead of ',')
        """
        nameinvle = nameinres[::-1].replace(':',',',1)[::-1]
        return nameinvle

