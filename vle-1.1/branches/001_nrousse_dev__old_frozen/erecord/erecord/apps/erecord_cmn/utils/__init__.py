# -*- coding: utf-8 -*-
"""erecord_cmn.utils

*This package is part of erecord - Record platform web development*

:copyright: Copyright (C) 2014-2015 INRA http://www.inra.fr.
:license: GPLv3, see :ref:`LICENSE` for more details.
:authors: see :ref:`AUTHORS`.

Python code of the erecord package

Python code, some of it is independant of django applications code

See :ref:`index`

"""

