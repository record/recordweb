# -*- coding: utf-8 -*-
"""erecord_db.models

Models of the erecord_db application

"""

from django.db import models
from django.core.exceptions import ValidationError

from erecord_cmn.models_mixins import DefaultVerboseNameMixin
from erecord_db.models_mixins import VleRepMixin
from erecord_db.models_mixins import VlePkgMixin
from erecord_db.models_mixins import VleVpzMixin

from erecord_cmn.utils.vle import is_structured_as_vle_home
from erecord_cmn.utils.vle import is_pkg_of_rep
from erecord_cmn.utils.vle import is_vpz_of_pkg_of_rep

# !!!!!!!!!!!!! a deplacer dans traitement repository
#
# Operations to record a (already physically installed) models repository into
# the database :
#
# The models repository is recorded into the database through the following
# information : its own characteristics (VleRep), the set of its vle packages
# { VlePkg } and their vpz files { VleVpz }.
#
# inputs :
# - path : path of the directory where the models repository has been
#          installed.
# - name : name given to the models repository into the database
#
# outputs (result into the database) :
# - VleRep, the set of its vle packages { VlePkg } and their vpz files
#   { VleVpz }.
#
# Operations :
#
# 1) Creation of VleRep
#
# 2) Creation of the { VlePackage } of VleRep :
#
#    Pyvle functions are called to define packages_list, the list of the
#    existing vle packages of VleRep ; then for each pkg in packages_list,
#    creation of VlePkg :
#       - VleRep = the VleRep
#       - name = pkg
#      (- others : description, maintainer ...)
#
#    At this step, the local.pkg file could be used (if active) to :
#    a) control packages_list (coherence between packages_list from pyvle
#       functions and packages_list from local.pkg).
#    b) define/get some information for each pkg in packages_list :
#      (- depends_list : list of the vle packages that it depends on.)
#       - others : description, maintainer ...
#
# 3) Creation of the { VleVpz } of the { VlePkg } of the VleRep :
#
#    For each VlePkg of the VleRep : pyvle functions are called to define
#    vpzfiles_list, the list of its vpz files ; then for each vpz in
#    vpzfiles_list, creation of VleVpz :
#       - VlePkg = the VlePkg
#       - name = vpz
#
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#

# A models repository is recorded into the database through the following
# information : its own characteristics (VleRep),
# the set of its vle packages { VlePkg } and their vpz files { VleVpz }.

ht_vlerep_name = \
    "Name identifying the models repository into the database (unique)."
ht_vlerep_verbose_name = \
    "Human-readable name of the models repository (optional, default " + \
    "value : name)."
ht_vlerep_path = \
    "Directory path where the models repository has been physically " + \
    "installed (directory structured as a VLE_HOME directory)."


class VleRep(DefaultVerboseNameMixin, VleRepMixin, models.Model):
    """Models repository

    - name : name given to the models repository for erecord (unique)
    - verbose_name : human-readable name
    - path : directory path where the models repository has been physically
      installed.
    - vlepkg_list : list of relative VlePkg
      (many VlePkg to one VleRep relationship)
   (- others : description ...???)

    """

    name = models.CharField(
        max_length=200, unique=True, blank=False,
        help_text=ht_vlerep_name)
    verbose_name = models.CharField(
        max_length=200, blank=True,
        help_text=ht_vlerep_verbose_name)
    path = models.FilePathField(blank=False, help_text=ht_vlerep_path)

    def delete_tree(self):
        """delete tree """
        for vlepkg in self.vlepkg_list.all():
            vlepkg.delete()

    def generate_tree(self):
        """generate tree """
        self.read_and_add_tree()

    def update_tree(self):
        """update tree """
        self.read_and_update_tree()

    def clean(self):
        """clean """
        if not is_structured_as_vle_home(self.path):
            msg = "The '" + self.name + "' models repository path (" +  \
                self.path + ") is not a directory structured as a " + \
                "VLE_HOME directory."
            raise ValidationError(msg)

    def save(self, *args, **kwargs):
        """save """

        self.set_default_value_verbose_name()

        super(VleRep, self).save(*args, **kwargs)

    def __str__(self):
        """str """
        return "'"+self.name+"'"

    class Meta:
        verbose_name = "models repository"
        verbose_name_plural = "models repositories"


ht_vlepkg_name = "Name of the vle package (same as in vle)."
ht_vlepkg_verbose_name = \
    "Human-readable name of the vle package (optional, default value : name)."
ht_vlepkg_vlerep = \
    "the models repository to which the vle package belongs. The vle " + \
    "package 'name' must be physically installed under the models " + \
    "repository path."


class VlePkg(DefaultVerboseNameMixin, VlePkgMixin, models.Model):
    """Vle package

    (vle notion of package, project)

    - name : vle package name (same as in vle)
    - verbose_name : human-readable name
    - vlerep : relative VleRep (many VlePkg to one VleRep relationship)
    - vlevpz_list : list of relative VleVpz
      (many VleVpz to one VlePkg relationship)

   (- others : description, maintainer ...???)

    """

    name = models.CharField(
        max_length=200, blank=False, help_text=ht_vlepkg_name)
    verbose_name = models.CharField(
        max_length=200, blank=True, help_text=ht_vlepkg_verbose_name)
    vlerep = models.ForeignKey(
        VleRep, related_name="vlepkg_list",
        verbose_name="related models repository", blank=False,
        help_text=ht_vlepkg_vlerep)

    def clean(self):
        #print "self.name : ", self.name
        #print "self.vlerep : ", self.vlerep
        #print "self.vlerep.path : ", self.vlerep.path

        if not is_pkg_of_rep(rep_path=self.vlerep.path,
                             pkgname=self.name):
            msg = "'" + self.name + "' doesn't exist (as a vle package) " + \
                "under the '" + self.vlerep.name + "' models repository " + \
                "path (" + self.vlerep.path + ")"
            raise ValidationError(msg)

    def save(self, *args, **kwargs):
        self.set_default_value_verbose_name()
        super(VlePkg, self).save(*args, **kwargs)

    def __str__(self):
        return "'"+self.name+"' (from '"+self.vlerep.name+"')"
        # return "rep: '"+self.vlerep.name+"', pkg: '"+self.name+"'"

    class Meta:
        verbose_name = "vle package"
        verbose_name_plural = "vle packages"

ht_vlevpz_name = \
    "Name of the vpz file : path relative to 'exp' directory, with .vpz " + \
    "extension (same as in vle)."
ht_vlevpz_verbose_name = \
    "Human-readable name of the vpz file (optional, default value : name)."
ht_vlevpz_vlepkg = \
    "the vle package to which the vpz files belongs. The vpz file 'name' " + \
    "must be physically installed under the vle package (under the models " + \
    "repository path)."


class VleVpz(DefaultVerboseNameMixin, VleVpzMixin, models.Model):
    """Vpz file

    (vle notion of vpz file, also called simulator, simulation scenario)

    - name : vpz file name (path relative to 'exp' directory, with .vpz
      extension) (same as in vle)
    - verbose_name : human-readable name
    - vlepkg : relative VlePkg (many VleVpz to one VlePkg relationship)
    - vpzinput : relative VpzInput (one VleVpz to one VpzInput relationship)
    - vpzoutput : relative VpzOutput (one VleVpz to one VpzOutput relationship)
   (- others : ...???)

    """

    name = models.CharField(
        max_length=200, blank=False, help_text=ht_vlevpz_name)
    verbose_name = models.CharField(
        max_length=200, blank=True, help_text=ht_vlevpz_verbose_name)
    vlepkg = models.ForeignKey(
        VlePkg, related_name="vlevpz_list",
        verbose_name="related vle package", blank=False,
        help_text=ht_vlevpz_vlepkg)

    def clean(self):
        if not is_vpz_of_pkg_of_rep(
                rep_path=self.vlepkg.vlerep.path,
                pkgname=self.vlepkg.name, vpzname=self.name):
            msg = "'" + self.name + "' doesn't exist as a vpz file of the " + \
                "'" + self.vlepkg.name + "' vle package under the " + \
                "'" + self.vlepkg.vlerep.name + "' models repository " + \
                "path (" + self.vlepkg.vlerep.path + ")"
            raise ValidationError(msg)

    def save(self, *args, **kwargs):
        self.set_default_value_verbose_name()
        super(VleVpz, self).save(*args, **kwargs)

    def __str__(self):
        return "'" + self.name + "' (from '" + self.vlepkg.name + \
            "' (from '" + self.vlepkg.vlerep.name + "'))"

#       return "rep: '"+self.vlepkg.vlerep.name+"', pkg: '"+self.vlepkg.name+ \
#               "', vpz: '"+self.name+"'"

    def get_vlerep(self):
        """returns the VleRep relative to the VleVpz """
        return self.vlepkg.vlerep
    get_vlerep.short_description = "Related models repository"

    class Meta:
        verbose_name = "vle vpz"
        verbose_name_plural = "vle vpz"
