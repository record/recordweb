// Menu to see and select rep, pkg, vpz
// and to apply actions on vpz (see, modify, run)

// Init
'option strict';

/* add option to select object */

function add_option(text, value, element_select)
{ // add option (text,value) to element_select options

  var option = document.createElement('option');
  option.value = value;
  option.text = text;
  element_select.add(option);
}
function add_option_selected(text, value, element_select)
{ // add option (text,value,selected) to element_select options

  var option = document.createElement('option');
  option.value = value;
  option.text = text;
  option.selected = 'selected';
  element_select.add(option);
}

/* remove select object options */

function clear_vlerep_select(e_vlerep_select)
{
    if (e_vlerep_select!=null){
        clear_element_content(e_vlerep_select);
        add_option("Models repositories", "", e_vlerep_select);
    }
}
function clear_vlepkg_select(e_vlepkg_select)
{
    if (e_vlepkg_select!=null){
        clear_element_content(e_vlepkg_select);
        add_option("Models", "", e_vlepkg_select);
    }
}
function clear_vlevpz_select(e_vlevpz_select)
{
    if (e_vlevpz_select!=null){
        clear_element_content(e_vlevpz_select);
        add_option("Simulators", "", e_vlevpz_select);
    }
}

/* extraction of some id (vlevpz_id...) from composed name */

function vlerep_decompose_id(idv)
{ // idv looks like vlerep_id

    var vlerep_id = idv;
    return ({vlerep_id:vlerep_id})
}
function vlepkg_decompose_id(idv)
{ // idv looks like vlerep_id,vlepkg_id

    var words = idv.split(',');
    var vlerep_id = words[0];
    var vlepkg_id = words[1];
    return ({vlerep_id:vlerep_id, vlepkg_id:vlepkg_id})
}
function vlevpz_decompose_id(idv)
{ // idv looks like vlerep_id,vlepkg_id,vlevpz_id 

    var words = idv.split(',');
    var vlerep_id = words[0];
    var vlepkg_id = words[1];
    var vlevpz_id = words[2];
    return ({vlerep_id:vlerep_id, vlepkg_id:vlepkg_id, vlevpz_id:vlevpz_id})
}

/* Vars */

// url for vlerep_id=0 (to be changed)
var url0_vlerep_detail; var url0_vlepkg_detail; var url0_vlevpz_detail;

// initial full information
var vlerep_list; var vlepkg_list; var vlevpz_list;

// case of a single simulator to be taken into account (the first one)
var first_one = false;

/* initialisation */
window.addEventListener('load', function(){

    var e = null; e = document.getElementById("first_one");
    if (e!=null){
        if (e.value == "yes"){ first_one = true; }
    }
    // hide selection part in first_one case
    if (first_one==true){
        e = null; e = document.getElementById("selection_part");
        if (e!=null){ e.style.display = 'None'; }
        e = null; e = document.getElementById("miscellaneous_part");
        if (e!=null){ e.style.display = 'None'; }
    }

    url0_vlerep_detail = ""; url0_vlepkg_detail = ""; url0_vlevpz_detail = "";
    var e = null; e = document.getElementById( "url0_vlerep_detail");
    if (e!=null){ url0_vlerep_detail = e.value; }
    e = null; e = document.getElementById( "url0_vlepkg_detail");
    if (e!=null){ url0_vlepkg_detail = e.value; }
    e = null; e = document.getElementById( "url0_vlevpz_detail");
    if (e!=null){ url0_vlevpz_detail = e.value; }

    vlerep_list = []; vlepkg_list = []; vlevpz_list = [];
    var e = null; e = document.getElementById("vlerep_select");
    if (e!=null){
        for (var i=0; i<e.options.length; i++) {
            opt = e.options[i];
            var d = vlerep_decompose_id(opt.value);
            vlerep_list.push({text:opt.text,value:opt.value,
                              vlerep_id:d.vlerep_id});
        }
    }
    e = null; e = document.getElementById("vlepkg_select");
    if (e!=null){
        for (var i=0; i<e.options.length; i++) {
            opt = e.options[i];
            var d = vlepkg_decompose_id(opt.value);
            vlepkg_list.push({text:opt.text,value:opt.value,
                              vlerep_id:d.vlerep_id, vlepkg_id:d.vlepkg_id});
        }
    }
    e = null; e = document.getElementById("vlevpz_select");
    if (e!=null){
        for (var i=0; i<e.options.length; i++) {
            opt = e.options[i];
            var d = vlevpz_decompose_id(opt.value);
            vlevpz_list.push({text:opt.text,value:opt.value,
                              vlerep_id:d.vlerep_id, vlepkg_id:d.vlepkg_id,
                              vlevpz_id:d.vlevpz_id});
        }
    }

    reinit();

    // selection of the first one in first_one case
    if (first_one == true){
        var e_vlevpz_select = null;
        e_vlevpz_select = document.getElementById("vlevpz_select");
        if (e_vlevpz_select!=null){
            e_vlevpz_select.options[1].selected = 'selected';
        }
        vlevpz_update();
    }

});

/* manage the 3 lists */

function set_input_value(id, value)
{ // default value "0"

    var e = null; e = document.getElementById(id);
    if (e!=null){
        if (value==null){ e.value = "0";
        } else { e.value = value; }
    }
}
function update_ids(vlerep_id, vlepkg_id, vlevpz_id)
{ // update according to id selections

    // actions
    set_input_value("id_see", vlevpz_id);
    set_input_value("id_modify", vlevpz_id);
    set_input_value("id_run", vlevpz_id);
    set_input_value("id_seerun", vlevpz_id);

    // show/hide actions menu
    var e = null; e = document.getElementById( "no_simulator_selected");
    if (e!=null){
        if (vlevpz_id==null){
            clear_element_content(e);
            e.style.display = 'block';
            var p = document.createElement("p"); var text;
            if (first_one==true){
                text = "<span style='color:red;'>Unavailable simulator</span>.";
            } else {
                text = "For access, you have to <span style='color:red;'><u>select a simulator</u></span> first.";
            }
            p.innerHTML = text;
            e.appendChild(p);
        } else { 
            e.style.display = 'None';
            clear_element_content(e);
        }
    }
    e =null; e = document.getElementById( "a_simulator_selected");
    if (e!=null){
        if (vlevpz_id==null){ e.style.display = 'None';
        } else { e.style.display = 'block'; }
    }

    // identity
    e =null; e = document.getElementById("url_vlerep_detail");
    if (e!=null){
        if (vlerep_id==null){
            e.action = url0_vlerep_detail;
            e.style.display = 'None';
        } else {
            e.action = url0_vlerep_detail.replace("0",vlerep_id);
            e.style.display = 'block';
        }
    }
    e =null; e = document.getElementById("url_vlepkg_detail");
    if (e!=null){
        if (vlepkg_id==null){
            e.action = url0_vlepkg_detail;
            e.style.display = 'None';
        } else {
            e.action = url0_vlepkg_detail.replace("0",vlepkg_id);
            e.style.display = 'block';
        }
    }
    e =null; e = document.getElementById("url_vlevpz_detail");
    if (e!=null){
        if (vlevpz_id==null){
            e.action = url0_vlevpz_detail;
            e.style.display = 'None';
        } else {
            e.action = url0_vlevpz_detail.replace("0",vlevpz_id);
            e.style.display = 'block';
        }
    }

    // remind
    e =null; e = document.getElementById("url_vlevpz_detail");
    var e_copy =null;
    e_copy = document.getElementById("remind_url_vlevpz_detail");
    if ( (e!=null) && (e_copy!=null) ){
        clear_element_content(e_copy);
        var cln = e.cloneNode(true);
        e_copy.appendChild(cln);
    }
    e = null; e = document.getElementById("vlevpz_select");
    e_copy =null;
    e_copy = document.getElementById("remind_selected_vlevpz");
    if ( (e != null) && (e_copy!=null) ){
        var value = e.options[e.selectedIndex].value;
        var text = e.options[e.selectedIndex].text;
        if (value == ""){
            e_copy.innerHTML = "None";
        } else {
            redtext = "<span style='color:red;'>"+text+"</span>.";
            e_copy.innerHTML = redtext;
        }
    }
}

function reinit()
{ // updates the 3 lists together

    var e_vlerep_select = document.getElementById("vlerep_select");
    var e_vlepkg_select = document.getElementById("vlepkg_select");
    var e_vlevpz_select = document.getElementById("vlevpz_select");
    clear_vlerep_select(e_vlerep_select);
    for (var i in vlerep_list){
        vlerep = vlerep_list[i];
        add_option(vlerep.text, vlerep.value, e_vlerep_select);
    }
    clear_vlepkg_select(e_vlepkg_select);
    for (var i in vlepkg_list){
        vlepkg = vlepkg_list[i];
        add_option(vlepkg.text, vlepkg.value, e_vlepkg_select);
    }
    clear_vlevpz_select(e_vlevpz_select);
    for (var i in vlevpz_list){
        vlevpz = vlevpz_list[i];
        add_option(vlevpz.text, vlevpz.value, e_vlevpz_select);
    }
    update_ids(null, null, null);
}

/* updates the 3 lists together */

function select_vlerep(e_vlerep_select, vlerep_id){
    if (e_vlerep_select != null){
        clear_vlerep_select(e_vlerep_select);
        for (var i in vlerep_list){
            vlerep = vlerep_list[i];
            if (vlerep.vlerep_id == vlerep_id){
                add_option_selected(vlerep.text, vlerep.value,
                                    e_vlerep_select);
            } else {
                add_option(vlerep.text, vlerep.value, e_vlerep_select);
            }
        }
    }
}
function filter_select_vlepkg(e_vlepkg_select, vlerep_id, vlepkg_id){
    if (e_vlepkg_select != null){
        clear_vlepkg_select(e_vlepkg_select);
        for (var i in vlepkg_list){
            vlepkg = vlepkg_list[i];
            if (vlepkg.vlerep_id == vlerep_id){
                if (vlepkg.vlepkg_id == vlepkg_id){
                    add_option_selected(vlepkg.text, vlepkg.value,
                                        e_vlepkg_select);
                } else {
                    add_option(vlepkg.text, vlepkg.value, e_vlepkg_select);
                }
            }
        }
    }
}
function vlerep_update()
{ // e_vlerep_select unchanged,
  // e_vlepkg_select filtered,
  // e_vlevpz_select filtered

    var e_vlerep_select = null;
    e_vlerep_select = document.getElementById("vlerep_select");
    var e_vlepkg_select = null;
    e_vlepkg_select = document.getElementById("vlepkg_select");
    var e_vlevpz_select = null;
    e_vlevpz_select = document.getElementById("vlevpz_select");

    if (e_vlerep_select != null){
        var sel = e_vlerep_select.options[e_vlerep_select.selectedIndex].value;
        if (sel == ""){
            reinit();
        } else {
            var d = vlerep_decompose_id(sel);
            var vlerep_id = d.vlerep_id;
            if (e_vlepkg_select != null){
                clear_vlepkg_select(e_vlepkg_select);
                for (var i in vlepkg_list){
                    vlepkg = vlepkg_list[i];
                    if (vlepkg.vlerep_id == vlerep_id){
                        add_option(vlepkg.text, vlepkg.value, e_vlepkg_select);
                    }
                }
            }
            if (e_vlevpz_select != null){
                clear_vlevpz_select(e_vlevpz_select);
                for (var i in vlevpz_list){
                    vlevpz = vlevpz_list[i];
                    if (vlevpz.vlerep_id == vlerep_id){
                        add_option(vlevpz.text, vlevpz.value, e_vlevpz_select);
                    }
                }
            }
            update_ids(vlerep_id, null, null);
        }
    }
}

function vlepkg_update()
{ // e_vlerep_select selected,
  // e_vlepkg_select selected (filtering those of the selected vlerep),
  // e_vlevpz_select filtered

    var e_vlerep_select = null;
    e_vlerep_select = document.getElementById("vlerep_select");
    var e_vlepkg_select = null;
    e_vlepkg_select = document.getElementById("vlepkg_select");
    var e_vlevpz_select = null;
    e_vlevpz_select = document.getElementById("vlevpz_select");

    if (e_vlepkg_select != null){
        var sel = e_vlepkg_select.options[e_vlepkg_select.selectedIndex].value;
        if (sel == ""){
            reinit();
        } else {
            var d = vlepkg_decompose_id(sel);
            var vlerep_id = d.vlerep_id;
            var vlepkg_id = d.vlepkg_id;
            select_vlerep(e_vlerep_select, vlerep_id);
            filter_select_vlepkg(e_vlepkg_select, vlerep_id, vlepkg_id);
            if (e_vlevpz_select != null){
                clear_vlevpz_select(e_vlevpz_select);
                for (var i in vlevpz_list){
                    vlevpz = vlevpz_list[i];
                    if (vlevpz.vlepkg_id == vlepkg_id){
                        add_option(vlevpz.text, vlevpz.value,
                                   e_vlevpz_select);
                    }
                }
            }
            update_ids(vlerep_id, vlepkg_id, null);
        }
    }
}
function vlevpz_update()
{ // e_vlerep_select selected,
  // e_vlepkg_select selected (filtering those of the selected vlerep),
  // e_vlevpz_select selected (filtering those of the selected vlepkg)

    var e_vlerep_select = null;
    e_vlerep_select = document.getElementById("vlerep_select");
    var e_vlepkg_select = null;
    e_vlepkg_select = document.getElementById("vlepkg_select");
    var e_vlevpz_select = null;
    e_vlevpz_select = document.getElementById("vlevpz_select");


    if (e_vlevpz_select != null){
        var sel = e_vlevpz_select.options[e_vlevpz_select.selectedIndex].value;
        if (sel == ""){
            reinit();
        } else {
            var d = vlevpz_decompose_id(sel);
            var vlerep_id = d.vlerep_id;
            var vlepkg_id = d.vlepkg_id;
            var vlevpz_id = d.vlevpz_id;
            select_vlerep(e_vlerep_select, vlerep_id);
            filter_select_vlepkg(e_vlepkg_select, vlerep_id, vlepkg_id);
            if (e_vlevpz_select != null){
                clear_vlevpz_select(e_vlevpz_select);
                for (var i in vlevpz_list){
                    vlevpz = vlevpz_list[i];
                    if (vlevpz.vlepkg_id == vlepkg_id){
                        if (vlevpz.vlevpz_id == vlevpz_id){
                            add_option_selected(vlevpz.text, vlevpz.value,
                                                e_vlevpz_select);
                        } else {
                            add_option(vlevpz.text, vlevpz.value,
                                       e_vlevpz_select);
                        }
                    }
                }
            }
            update_ids(vlerep_id, vlepkg_id, vlevpz_id);
        }
    }
}

