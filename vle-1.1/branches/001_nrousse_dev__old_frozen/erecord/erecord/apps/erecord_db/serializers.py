# -*- coding: UTF-8 -*-
"""erecord_db.serializers """

from rest_framework import serializers

from erecord_db.models import VleRep
from erecord_db.models import VlePkg
from erecord_db.models import VleVpz


#------------------------------------------------------------------------------

class RepOptionSerializer(serializers.Serializer):
    rep = serializers.IntegerField( required = False )
    def validate(self, attrs):
        if 'rep' not in attrs.keys() :
            attrs['rep']=None
        return attrs

class PkgOptionSerializer(serializers.Serializer):
    pkg = serializers.IntegerField( required = False )
    def validate(self, attrs):
        if 'pkg' not in attrs.keys() :
            attrs['pkg']=None
        return attrs

class VpzOptionSerializer(serializers.Serializer):
    vpz = serializers.IntegerField( required = False )
    def validate(self, attrs):
        if 'vpz' not in attrs.keys() :
            attrs['vpz']=None
        return attrs

#------------------------------------------------------------------------------

class VleRepSerializerLink(serializers.ModelSerializer):
    vlepkg_list = serializers.HyperlinkedRelatedField(many=True,
            read_only=True, # queryset = VlePkg.objects.all(),
            view_name='erecord_db-pkg-detail')
    class Meta:
        model = VleRep
        fields= ( 'id', 'name', 'verbose_name', 'path', 'vlepkg_list',)

class VlePkgSerializerLink( serializers.ModelSerializer ):
    vlevpz_list = serializers.HyperlinkedRelatedField(many=True,
            read_only=True, # queryset = VleVpz.objects.all(),
            view_name='erecord_db-vpz-detail')
    vlerep = serializers.HyperlinkedRelatedField(
            read_only=True, # queryset = VleRep.objects.all(),
            view_name='erecord_db-rep-detail')
    class Meta:
        model = VlePkg
        fields= ( 'id', 'verbose_name', 'name', 'vlerep', 'vlevpz_list',)

class VleVpzSerializerLink( serializers.ModelSerializer ):
    vlepkg = serializers.HyperlinkedRelatedField(
            read_only=True, # queryset = VlePkg.objects.all(),
            view_name='erecord_db-pkg-detail')
    class Meta:
        model = VleVpz
        fields= ( 'id', 'verbose_name', 'name', 'vlepkg',)

#------------------------------------------------------------------------------

class VleRepSerializerTree(serializers.ModelSerializer):
    class Meta:
        model = VleRep
        fields= ( 'id', 'name', 'verbose_name', 'path', 'vlepkg_list',)
        depth = 3

class VlePkgSerializerTree( serializers.ModelSerializer ):
    class Meta:
        model = VlePkg
        fields= ( 'id', 'verbose_name', 'name', 'vlerep', 'vlevpz_list',)
        depth = 3

class VleVpzSerializerTree( serializers.ModelSerializer ):
    class Meta:
        model = VleVpz
        fields= ( 'id', 'verbose_name', 'name', 'vlepkg',)
        depth = 3

#------------------------------------------------------------------------------

