# -*- coding: utf-8 -*-
"""erecord_db.forms"""

from django import forms
from django.core.exceptions import ValidationError

from erecord_cmn.forms import ReadonlyForm

from erecord_db.models import VleRep
from erecord_db.models import VlePkg
from erecord_db.models import VleVpz

from erecord_cmn.configs.config import REPOSITORIES_HOME
from erecord_cmn.utils.vle import get_rep_pkg_exp_path
from erecord_cmn.utils.vle import get_rep_pkgname_list
from erecord_cmn.utils.vle import is_structured_as_vle_home

import os

from erecord_db.models import ht_vlerep_path as mht_vlerep_path
from erecord_db.models import ht_vlepkg_name as mht_vlepkg_name
from erecord_db.models import ht_vlevpz_name as mht_vlevpz_name

empty_choice = ("","---------")


def get_vlereppath_choices() :
    """returns the list to choose a models repository path.
    
    The path taken into account are those found under the REPOSITORIES_HOME
    path, that are structured as a VLE_HOME directory.
    Their vle packages are also shown in order to help choosing.
    """

    choices_list = list()
    tmp = forms.FilePathField(path=REPOSITORIES_HOME, recursive=True,
            allow_files=False, allow_folders=True)
    for (path,c) in tmp.choices :
        if is_structured_as_vle_home(path) :
            choices_list.append( (path,c) )

    text = "Don't choose one of the following values, \
        (existing vle packages shown only for help) :"
    plus = [ empty_choice, ("",text) ]
    for (path,c) in choices_list :
        for pkgname in get_rep_pkgname_list(path) :
            text = c+": "+pkgname
            plus.append( ("",text) )
    for p in plus :
        choices_list.append(p)

    choices_list.insert(0, empty_choice)

    return choices_list

ht_vlerep_path = mht_vlerep_path+ \
        " Choose a directory among those existing ones under "+ \
        REPOSITORIES_HOME+"."

class VleRepForm(forms.ModelForm):

    path = forms.ChoiceField( choices=get_vlereppath_choices(), 
            required=True,
            help_text=ht_vlerep_path)

    class Meta:
        model = VleRep
        fields = '__all__'

def get_vlepkgname_choices() :
    """returns the list to choose a vle package name.
    
    The vle packages taken into account are those found under the path of one
    of the database models repositories.
    """

    choices_list = [ empty_choice ]
    for vlerep in VleRep.objects.all() :
        vlepkgname_list = get_rep_pkgname_list( vlerep.path )
        for vlepkgname in vlepkgname_list :
            choice_info = "rep: "+vlerep.name+" ; pkg: "+vlepkgname 
            choices_list.append( (vlepkgname, choice_info) )
    return choices_list

ht_vlepkg_name = mht_vlepkg_name+ \
        " Choose a vle package among the existing (models repository name, \
        vle package name)"

class VlePkgForm(forms.ModelForm):
    name = forms.ChoiceField( choices=get_vlepkgname_choices(), 
            required=True,
            help_text=ht_vlepkg_name)
    class Meta:
        model = VlePkg
        fields = '__all__'


def get_vlevpzname_choices() :
    """returns the list to choose a vpz file name.
    
    The vpz files taken into account are those found under one of the
    database vle packages (under its models repository path).
    """

    choices_list = [ empty_choice ]
    for vlepkg in VlePkg.objects.all() :
        vlepkg_exp_path = get_rep_pkg_exp_path(rep_path=vlepkg.vlerep.path,
                pkgname=vlepkg.name)
        tmp = forms.FilePathField( path=vlepkg_exp_path, recursive=True,
                allow_files=True, allow_folders=False, match=".*\.vpz$")
        for (v,c) in tmp.choices :
            vlevpzname = os.path.relpath( v, vlepkg_exp_path )
            choice_info = "rep: "+vlepkg.vlerep.name+" ; pkg: "+ \
                    vlepkg.name+" ; vpz: "+vlevpzname
            choices_list.append( (vlevpzname, choice_info) )
    return choices_list

ht_vlevpz_name = mht_vlevpz_name+ \
        " Choose a vpz file among the existing (models repository name, \
        vle package name, vpz file name)"

class VleVpzForm(forms.ModelForm):

    name = forms.ChoiceField( choices=get_vlevpzname_choices(), 
            required=True,
            help_text=ht_vlevpz_name)

    class Meta:
        model = VleVpz
        fields = '__all__'

###############################################################################

class VleRepUserForm(ReadonlyForm):
    class Meta:
        model = VleRep
        fields = '__all__'

class VlePkgUserForm(ReadonlyForm):
    class Meta:
        model = VlePkg
        fields = '__all__'

class VleVpzUserForm(ReadonlyForm):
    class Meta:
        model = VleVpz
        fields = '__all__'

###############################################################################

class VleRepNameForm(ReadonlyForm):
    class Meta:
        model = VleRep
        fields = ['name',]

class VlePkgNameForm(ReadonlyForm):
    class Meta:
        model = VlePkg
        fields = ['name',]

class VleVpzNameForm(ReadonlyForm):
    class Meta:
        model = VleVpz
        fields = ['name',]

###############################################################################
# Forms for menus

# default forms

class FormatForm(forms.Form):
    HTML = "html"
    JSON = "json"
    API = "api"
    YAML = "yaml"
    XML = "xml"
    CHOICES = [ (HTML, "html"), (JSON, "json"), (API, "api"),
                (YAML, "yaml"), (XML, "xml"),]
    LABEL = "format"
    HELP_TEXT = "response format"
    format = forms.ChoiceField(required=True, label=LABEL, choices=CHOICES,
                               help_text=HELP_TEXT, initial=HTML)
class StyleForm(forms.Form):
    COMPACTLIST = "compactlist"
    COMPACT = "compact"
    TREE = "tree"
    LINK = "link"
    CHOICES = [ (TREE, "tree"), (LINK, "link"),]
    LABEL = "style"
    HELP_TEXT = "style of presentation"
    mode = forms.ChoiceField(required=True, label=LABEL, choices=CHOICES,
                             help_text=HELP_TEXT, initial=TREE)
class PlanForm(forms.Form):
    SINGLE = "single"
    LINEAR = "linear"
    CHOICES = [ (SINGLE, "single"), (LINEAR, "linear"),]
    LABEL = "plan"
    HELP_TEXT = "mono or multi-simulation"
    mode = forms.ChoiceField(required=True, label=LABEL, choices=CHOICES,
                             help_text=HELP_TEXT, initial=SINGLE)
class RestypeForm(forms.Form):
    DATAFRAME = "dataframe"
    MATRIX = "matrix"
    CHOICES = [ (DATAFRAME, "dataframe"), (MATRIX, "matrix"),]
    LABEL = "restype"
    HELP_TEXT = "type of result"
    mode = forms.ChoiceField(required=True, label=LABEL, choices=CHOICES,
                             help_text=HELP_TEXT, initial=DATAFRAME)
class StorageForm(forms.Form):
    STORAGE = "storage"
    EMPTY_VALUE = ""
    CHOICES = [ (STORAGE, "yes"), (EMPTY_VALUE, "no"),]
    LABEL = "storage"
    HELP_TEXT = "web storage activation"
    mode = forms.ChoiceField(required=True, label=LABEL, choices=CHOICES,
                             help_text=HELP_TEXT, initial=STORAGE)
class OutselectForm(forms.Form):
    ALL = "all"
    EMPTY_VALUE = ""
    CHOICES = [ (ALL, "yes"), (EMPTY_VALUE, "no"),]
    LABEL = "forcing the outputs"
    HELP_TEXT = "outputs selection"
    outselect = forms.ChoiceField(required=True, label=LABEL, choices=CHOICES,
                                  help_text=HELP_TEXT, initial=ALL)

# vpz/input forms

class VpzInputFormatForm(FormatForm):
    pass
class VpzInputStyleForm(StyleForm):
    mode = forms.ChoiceField(required=True, label=StyleForm.LABEL,
               choices=[ (StyleForm.COMPACTLIST, "compactlist"),
                         (StyleForm.COMPACT, "compact"),
                         (StyleForm.TREE, "tree"),
                         (StyleForm.LINK, "link"),],
               help_text=StyleForm.HELP_TEXT, initial=StyleForm.COMPACTLIST)
class VpzInputOutselectForm(OutselectForm):
    pass

# vpz/output forms

class VpzOutputFormatForm(FormatForm):
    pass
class VpzOutputStyleForm(StyleForm):
    mode = forms.ChoiceField(required=True, label=StyleForm.LABEL,
               choices=[ (StyleForm.COMPACT, "compact"),
                         (StyleForm.TREE, "tree"),
                         (StyleForm.LINK, "link"),],
               help_text=StyleForm.HELP_TEXT, initial=StyleForm.COMPACT)
class VpzOutputPlanForm(PlanForm):
    pass
class VpzOutputRestypeForm(RestypeForm):
    pass
class VpzOutputStorageForm(StorageForm):
    pass
class VpzOutputOutselectForm(OutselectForm):
    pass

# vpz/inout forms

class VpzInoutFormatForm(FormatForm):
    format = forms.ChoiceField(required=True, label=FormatForm.LABEL,
                 choices=FormatForm.CHOICES, help_text=FormatForm.HELP_TEXT,
                 initial=FormatForm.API)
class VpzInoutStyleForm(StyleForm):
    mode = forms.ChoiceField(required=True, label=StyleForm.LABEL,
               choices=StyleForm.CHOICES,
               help_text=StyleForm.HELP_TEXT, initial=StyleForm.TREE)
class VpzInoutPlanForm(PlanForm):
    pass
class VpzInoutRestypeForm(RestypeForm):
    pass
class VpzInoutOutselectForm(OutselectForm):
    pass

# identity forms

class IdentityFormatForm(FormatForm):
    pass
class IdentityStyleForm(StyleForm):
    mode = forms.ChoiceField(required=True, label=StyleForm.LABEL,
               choices=StyleForm.CHOICES, initial=StyleForm.TREE)

