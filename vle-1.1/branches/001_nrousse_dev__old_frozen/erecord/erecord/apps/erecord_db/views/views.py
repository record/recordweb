# -*- coding: UTF-8 -*-
"""erecord_db.views.views """

from rest_framework.response import Response
from rest_framework import generics

from erecord_db.models import VleRep
from erecord_db.models import VlePkg
from erecord_db.models import VleVpz

from erecord_db.forms import VleRepNameForm
from erecord_db.forms import VlePkgNameForm
from erecord_db.forms import VleVpzNameForm

from erecord_cmn.views_mixins import FormatViewMixin
from erecord_cmn.views_mixins import DataViewMixin
from erecord_cmn.views_mixins import RenderViewMixin
from erecord_cmn.views_mixins import DetailViewMixin

from rest_framework.renderers import TemplateHTMLRenderer

from erecord_cmn.utils.logger import get_logger
LOGGER = get_logger(__name__)

headsubtitle = "(erecord_db)"


class VleRepNameView(FormatViewMixin, DataViewMixin, DetailViewMixin,
                     generics.RetrieveAPIView):
    """Name of one vle models repository

    For more information, see the online documentation.
    """

    def get(self, request, format=None, **kwargs):
        vlerep = VleRep.objects.get(pk=kwargs['pk'])
        data = self.request_query_params(request)
        if format is None :
            format = self.get_format_value(data=data)
        if format=='html' :
            context = dict()
            title = 'Name of models repository (Id ' + str(vlerep.id) + ')'
            context['title'] = title 
            context['headsubtitle'] = headsubtitle
            context['form'] = VleRepNameForm(instance=vlerep)
            response = Response(context)
        else :
            response = Response(data={'name':vlerep.name })
        return response

class VlePkgNameView(FormatViewMixin, DataViewMixin, DetailViewMixin,
                     generics.RetrieveAPIView):
    """Name of one vle package

    For more information, see the online documentation.
    """

    def get(self, request, format=None, **kwargs):
        vlepkg = VlePkg.objects.get(pk=kwargs['pk'])
        data = self.request_query_params(request)
        if format is None :
            format = self.get_format_value(data=data)
        if format=='html' :
            context = dict()
            title = 'Name of model (Id ' + str(vlepkg.id) + ')'
            context['title'] = title 
            context['headsubtitle'] = headsubtitle
            context['form'] = VlePkgNameForm(instance=vlepkg)
            response = Response(context)
        else :
            response = Response(data={'name':vlepkg.name })
        return response

class VleVpzNameView(FormatViewMixin, DataViewMixin, DetailViewMixin,
                     generics.RetrieveAPIView):
    """Name of one vpz file

    For more information, see the online documentation.
    """

    def get(self, request, format=None, **kwargs):
        vlevpz = VleVpz.objects.get(pk=kwargs['pk'])
        data = self.request_query_params(request)
        if format is None :
            format = self.get_format_value(data=data)
        if format=='html' :
            context = dict()
            title = 'Name of simulator (Id ' + str(vlevpz.id) + ')'
            context['title'] = title 
            context['headsubtitle'] = headsubtitle
            context['form'] = VleVpzNameForm(instance=vlevpz)
            response = Response(context)
        else :
            response = Response(data={'name':vlevpz.name })
        return response

class VlePkgDatalistView(FormatViewMixin, DataViewMixin, RenderViewMixin,
                         generics.RetrieveAPIView):
    """List of name of files into default data folder of one vle package

    For more information, see the online documentation.
    """

    def get_renderers( self ):
        r = RenderViewMixin.get_renderers(self)
        renderer = TemplateHTMLRenderer()
        renderer.template_name='erecord_db/headedform_datalist.html'
        r.append( renderer )
        return r

    def get(self, request, format=None, **kwargs):

        vlepkg = VlePkg.objects.get(pk=kwargs['pk'])
        data = self.request_query_params(request)
        if format is None :
            format = self.get_format_value(data=data)

        data_list = vlepkg.get_datafilename_list()

        context = dict()
        context['datalist'] = data_list
        if format=='html' :
            title = 'Names of data folder files of model (Id ' + str(vlepkg.id) + ')'
            text = "The default data folder of the model '"+ vlepkg.name +"' (Id " + str(vlepkg.id) + ") contains the following files :"
            context['title'] = title 
            context['headsubtitle'] = headsubtitle
            context['text'] = text
            response = Response(context)
        else :
            response = Response(data=context)
        return response

