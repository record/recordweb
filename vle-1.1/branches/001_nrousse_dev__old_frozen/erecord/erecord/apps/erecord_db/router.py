# -*- coding: utf-8 -*-
"""erecord_db.router """

from erecord_cmn.router import DatabaseRouter
from erecord_cmn.configs.config import DB_NAME_DB


class DbRouter(DatabaseRouter):
    """
    The application database router.
    """
    db_name = DB_NAME_DB
    app_name = 'erecord_db'

