# -*- coding: utf-8 -*-
"""erecord_slm.urls

Urls of the erecord_slm application

"""

from django.conf.urls import patterns, include, url

from django.conf import settings
from django.conf.urls.static import static

# to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',

    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),

    # management of downloadable and uploaded files of erecord_vpz
    #
    # Not online/activated because should be in access only for administrator
    #
    # => Use the admin site (as admin user) instead of this url.
    #
    ##url(r'^vpz/$', 'erecord_slm.views.erecord_vpz_management',
    ##    name="erecord_slm-erecord_vpz_management"),
)

