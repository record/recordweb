# -*- coding: UTF-8 -*-
"""erecord_slm.views_mixins

Mixins common to views using management provided by erecord_slm application

"""

from erecord_slm.models import DownloadVpzDocument

from rest_framework.reverse import reverse
from django.conf import settings

class VpzDownloadViewMixin(object):
    """additional method for views relative to erecord_vpz application, with
    downloadable results
    """

    def build_download_folder(self, LOGGER, request, file_path):
        """Builds a downloadable folder containing the file found at file_path

        Returns the associated absolute url.
        """

        download_document = DownloadVpzDocument.create(file_path=file_path)
        download_url = download_document.get_url()
        pk = download_url.strip(settings.MEDIA_URL)
        download_absolute_url = reverse("media", request=request, args=(pk,))
        return download_absolute_url

