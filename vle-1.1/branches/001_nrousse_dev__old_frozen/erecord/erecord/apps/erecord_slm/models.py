# -*- coding: utf-8 -*-
"""erecord_slm.models

Models of the erecord_slm application

"""

import os

from django.db.models.signals import pre_delete

from django.core.files import File

from django.db import models
from erecord_slm.models_mixins import LoadVpzDocumentMixin

from erecord_cmn.configs.config import DOWNLOADS_HOME_APP_VPZ
from erecord_cmn.configs.config import DOWNLOAD_LIFETIME

class UploadVpzDocument(LoadVpzDocumentMixin, models.Model):
    """Uploaded document relative to erecord_vpz application"""

    date = models.DateTimeField(auto_now_add=True)

    def get_upload_to_path(self, filename):
        return self.get_available_filepath(root=DOWNLOADS_HOME_APP_VPZ,
                sub="upload", date=self.date, filename=filename)

    docfile = models.FileField(upload_to=get_upload_to_path)

    def is_old(self):
        """UploadVpzDocument is old if date 'older' than UPLOAD_LIFETIME"""
        return self.is_old_indays(days=UPLOAD_LIFETIME)

    def delete_if_old(self):
        """Deletes UploadVpzDocument and its associated folder, if too old """
        if self.is_old() :
            self.delete()

def uploadvpzdocument_clear(sender, instance, *args, **kwargs):
    """Deletes UploadVpzDocument associated folder"""
    instance.clear_dir()

# done before a UploadVpzDocument deletion
pre_delete.connect(uploadvpzdocument_clear, sender=UploadVpzDocument)


class DownloadVpzDocument(LoadVpzDocumentMixin, models.Model):
    """Downloadable document relative to erecord_vpz application"""

    date = models.DateTimeField(auto_now_add=True)

    def get_upload_to_path(self, filename):
        return self.get_available_filepath(root=DOWNLOADS_HOME_APP_VPZ,
                sub="download", date=self.date, filename=filename)

    docfile = models.FileField(upload_to=get_upload_to_path)

    def is_old(self):
        """DownloadVpzDocument is old if date 'older' than DOWNLOAD_LIFETIME"""
        return self.is_old_indays(days=DOWNLOAD_LIFETIME)

    def delete_if_old(self):
        """Deletes DownloadVpzDocument and its associated folder, if too old """
        if self.is_old() :
            self.delete()

    @classmethod
    def create(cls, file_path):
        """create a DownloadDocument for file file_path"""

        f = File(open(file_path))
        document = cls()
        document.save()
        document.docfile.save(name=os.path.basename(f.name), content=f,
                save=True) # save=False)
        return document

def downloadvpzdocument_clear(sender, instance, *args, **kwargs):
    """Deletes DownloadVpzDocument associated folder"""
    instance.clear_dir()

# done before a DownloadVpzDocument deletion
pre_delete.connect(downloadvpzdocument_clear, sender=DownloadVpzDocument)

