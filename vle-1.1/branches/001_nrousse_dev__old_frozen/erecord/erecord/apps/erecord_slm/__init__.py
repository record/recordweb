# -*- coding: utf-8 -*-
"""erecord_slm

*This package is part of erecord - Record platform web development*

:copyright: Copyright (C) 2014-2015 INRA http://www.inra.fr.
:license: GPLv3, see :ref:`LICENSE` for more details.
:authors: see :ref:`AUTHORS`.

Downloadable and uploaded files management application
 
The slm application is dedicated to downloadable and uploaded files management.

The applications that have such files call for erecord_slm.

See :ref:`index`

"""
