# -*- coding: UTF-8 -*-
"""erecord_slm.models_mixins

Mixins for erecord_slm model

"""

import os
import datetime

from erecord_cmn.utils.dir_and_file import get_available_pathname
from erecord_cmn.utils.dir_and_file import delete_path_if_present

from django.conf import settings

from erecord_cmn.utils.logger import get_logger
LOGGER = get_logger(__name__) # should be passed as a parameter of methods ?

# Some mixins for the erecord_slm models

class LoadVpzDocumentMixin(object):
    """additional methods for 'side'loadVpzDocument"""

    @classmethod
    def get_available_filepath(cls, root, sub, date, filename):
        """Defines and returns a file path name based on the input information,
        that doesn't yet exist (...and should be 'immediately' created !)
        """
    
        dir = root + '/' + sub
        base = '%s_' % date.strftime("%Y%m%d_%H%M%S")
        dir = get_available_pathname( rootpath=dir, rootname=base)
        dir += '/'+'%s' % filename
        return dir

    def get_url(self):
        return self.docfile.url

    def get_absolute_url(self):
        """Returns docfile absolute path """

        path = self.get_url().strip(settings.MEDIA_URL)
        absolute_url = os.path.join(settings.MEDIA_ROOT, path)
        return absolute_url 

    def clear_dir(self):
        """Deletes (if exists) the folder relative to docfile (containing it)"""

        absolute_dir = os.path.dirname(self.get_absolute_url())
        delete_path_if_present(path=absolute_dir)

    def is_old_indays(self, days):
        """ 'side'loadVpzDocument is old if date 'older' than days """

        d = self.date + datetime.timedelta(days=days)
        now = datetime.datetime.now()
        if d.toordinal() < now.toordinal() :
           return True
        else :
           return False

