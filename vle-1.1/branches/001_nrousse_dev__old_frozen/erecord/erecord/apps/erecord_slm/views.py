# -*- coding: utf-8 -*-
"""erecord_slm.views """

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from erecord_slm.models import UploadVpzDocument
from erecord_slm.forms import UploadDocumentForm
from erecord_slm.models import DownloadVpzDocument


def handle_uploaded_file(f):
    with open('some/file/name.txt', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

def erecord_vpz_management(request):
    """management by erecord_slm of the erecord_vpz downloadable files.

    The erecord_slm application is dedicated to downloadable (and maybe later
    uploaded) files management.

    The erecord_vpz application is dedicated to activities on vpz, where
    some results may be downloadable (cf todownload mode case).

    Be careful, this view should be in access only for administrator !

    The admin site can be used (as admin user) instead of this view.

    """

    # Handle file upload
    if request.method == 'POST':
        form = UploadDocumentForm(request.POST, request.FILES)
        if form.is_valid():
            #print " request.FILES['docfile'] :", request.FILES['docfile'] 
            #print " request.FILES.keys() :", request.FILES.keys()
            #print " request.POST.keys() :", request.POST.keys()
            #print " request.POST :", request.POST
            newdoc = UploadVpzDocument(docfile = request.FILES['docfile'])
            newdoc.save()

            return HttpResponseRedirect(reverse(
                'erecord_slm-erecord_vpz_management'))
    else:
        form = UploadDocumentForm() # A empty, unbound form

    upload_documents = UploadVpzDocument.objects.all()
    download_documents = DownloadVpzDocument.objects.all()

    # Render list page with the documents and the form
    context = {'upload_documents': upload_documents, 'form': form,
            'download_documents': download_documents,}
    return render_to_response( 'erecord_slm/erecord_vpz_management.html',
            context, context_instance=RequestContext(request)
    )

