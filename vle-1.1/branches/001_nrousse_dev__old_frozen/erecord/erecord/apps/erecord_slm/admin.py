# -*- coding: utf-8 -*-
"""erecord_slm.admin"""

from django.contrib import admin

from erecord_slm.models import UploadVpzDocument
from erecord_slm.models import DownloadVpzDocument

# To show/hide some admin parts

from django.contrib.auth.models import User
from django.contrib.auth.models import Group
#admin.site.unregister(User)
#admin.site.unregister(Group)


uploadvpzdocument_general_description = \
        "UploadVpzDocument."

def delete_too_old_uploadvpzdocument(modeladmin, request, queryset):
    for uploadvpzdocument in queryset:
        uploadvpzdocument.delete_if_old()
delete_too_old_uploadvpzdocument.short_description =\
        "Delete selected uploadvpzdocuments if too old"

class UploadVpzDocumentAdmin(admin.ModelAdmin):
    actions = [delete_too_old_uploadvpzdocument]
    fieldsets = (
        (None, {
            'description': uploadvpzdocument_general_description,
            'fields': ('docfile',), # not 'date'
        }),
    )

admin.site.register(UploadVpzDocument, UploadVpzDocumentAdmin)


downloadvpzdocument_general_description = \
        "DownloadVpzDocument containing reports (cf vpz/report resource)."

def delete_too_old_downloadvpzdocument(modeladmin, request, queryset):
    for downloadvpzdocument in queryset:
        downloadvpzdocument.delete_if_old()
delete_too_old_downloadvpzdocument.short_description = \
        "Delete selected downloadvpzdocuments if too old"

class DownloadVpzDocumentAdmin(admin.ModelAdmin):
    actions = [delete_too_old_downloadvpzdocument]
    fieldsets = (
        (None, {
            'description': downloadvpzdocument_general_description,
            'fields': ('docfile',), # not 'date'
        }),
    )

admin.site.register(DownloadVpzDocument, DownloadVpzDocumentAdmin)

