# -*- coding: utf-8 -*-
"""erecord package

*This package is part of erecord - Record platform web development*

:copyright: Copyright (C) 2014-2015 INRA http://www.inra.fr.
:license: GPLv3, see :ref:`LICENSE` for more details.
:authors: see :ref:`AUTHORS`.

The erecord package is part of the erecord project dedicated to the Record
platform web development for vle-1.1 models.

The erecord package contains the production (source code, tests, etc).

The code is written in python language with the django web framework.

Content hierarchy : docs, install, projects, apps...

See :ref:`index`

"""

