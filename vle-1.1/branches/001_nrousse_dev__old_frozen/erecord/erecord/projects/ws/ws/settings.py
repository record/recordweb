# -*- coding: utf-8 -*-
"""erecord.projects.ws.ws.settings

Django settings for ws project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

import sys

# default virtualenv activation ?
#path_to_activate_virtualenv = '/home/nrousse/workspace_svn/DEVELOPPEMENT_WEB/ENCOURS/vle-1.1/trunk/erecord/factory/erecordenv/bin/activate_this.py'
#execfile(path_to_activate_virtualenv, dict(__file__=path_to_activate_virtualenv))

# project_home, apps_home (erecord project, apps) to add to pythonpath
project_home = os.path.normpath(os.path.join(BASE_DIR, "..", "..", ".."))
if project_home not in sys.path :
    sys.path.insert(0, project_home)
apps_home = os.path.normpath(os.path.join(BASE_DIR, "..", "..", "apps"))
if apps_home not in sys.path :
    sys.path.insert(0, apps_home)


#print "project_home : ", project_home
#print "apps_home : ", apps_home

from erecord_cmn.utils.logger import get_logger
LOGGER = get_logger(__name__)

LOGGER.debug(u"sys.path : %s (must contain %s)", sys.path, project_home)
LOGGER.debug(u"sys.path : %s (must contain %s)", sys.path, apps_home)

from erecord_cmn.configs.config import DB_NAME_DEFAULT
from erecord_cmn.configs.config import DB_PATH_DEFAULT
from erecord_cmn.configs.config import DB_NAME_DB
from erecord_cmn.configs.config import DB_PATH_DB
from erecord_cmn.configs.config import DB_NAME_VPZ
from erecord_cmn.configs.config import DB_PATH_VPZ
from erecord_cmn.configs.config import DB_NAME_SLM
from erecord_cmn.configs.config import DB_PATH_SLM

from erecord_cmn.configs.config import MEDIA_DESTINATION_HOME
from erecord_cmn.configs.config import STATIC_DESTINATION_HOME
from erecord_cmn.configs.config import STATIC_APP_CMN
#ajeter from erecord_cmn.configs.config import STATIC_APP_DOC
from erecord_cmn.configs.config import TEMPLATE_APP_CMN
from erecord_cmn.configs.config import TEMPLATE_APP_DB
from erecord_cmn.configs.config import TEMPLATE_APP_VPZ
from erecord_cmn.configs.config import TEMPLATE_APP_SLM

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '!z@35*kn@(cin5&u(-(yy(s9-ev+@s$&u+d_9x++ln6op8smq_'

SITE_ID = 1 # SITE_ID is required by admindocs

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'django.contrib.admin',  # to enable the admin
    'django.contrib.admindocs',

    #'request', # django-request statistics module

    'rest_framework',  # web services
    #??'rest_framework_jwt', # (djangorestframework-jwt==0.1.5)

    # ??? not ok
    #'rest_framework_swagger', # documenting the api
                               # (django-rest-swagger==0.1.14)

    'erecord_db',
    'erecord_vpz',
    'erecord_slm',
)


MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

#!!! to try ?
#REST_FRAMEWORK = {
#    'DEFAULT_CONTENT_NEGOTIATION_CLASS': 'myapp.negotiation.IgnoreClientContentNegotiation',
#}

ROOT_URLCONF = 'ws.urls'

WSGI_APPLICATION = 'ws.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

#DATABASES = {
#    'default': {
#        'ENGINE': 'django.db.backends.sqlite3',
#        #'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#        'NAME': DB_PATH_DB,
#    },
#}
DATABASES = {
    DB_NAME_DEFAULT: {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': DB_PATH_DEFAULT,
    },
    DB_NAME_DB: {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': DB_PATH_DB,
    },
    DB_NAME_VPZ: {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': DB_PATH_VPZ,
    },
    DB_NAME_SLM: {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': DB_PATH_SLM,
    },
}

DATABASE_ROUTERS = ['erecord_db.router.DbRouter',
    'erecord_vpz.router.VpzRouter',
    'erecord_slm.router.SlmRouter',
    ]


# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Media files (downloadable (and later uploaded ?) files)

MEDIA_ROOT = os.path.join(MEDIA_DESTINATION_HOME, 'ws')

MEDIA_URL = '/media/'


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_ROOT = os.path.join(STATIC_DESTINATION_HOME, 'ws')

STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    #ajeter STATIC_APP_DOC,
    STATIC_APP_CMN, # last
#
#      '/var/www/static/',
#
)

#??deprecated??ADMIN_MEDIA_PREFIX = '/static/admin/'

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# Templates locations
TEMPLATE_DIRS = (
    TEMPLATE_APP_DB,
    TEMPLATE_APP_SLM,
    TEMPLATE_APP_VPZ,
    TEMPLATE_APP_CMN, # last
)

# ??? not ok
# => also django_rest_swagger to be desinstalled
#SWAGGER_SETTINGS = {
#    "exclude_namespaces": [], # List URL namespaces to ignore
#    "api_version": '0.0.0',  # Specify your API's version
#    "api_path": "/",  # Specify the path to your API not a root level
#    "enabled_methods": [  # Specify which methods to enable in Swagger UI
#        'get',
#        'post',
#        'put',
#        'patch',
#        'delete'
#    ],
#    "api_key": '', # An API key
#    "is_authenticated": False,  # Set to True to enforce user authentication,
#    "is_superuser": False,  # Set to True to enforce admin only access
#}

