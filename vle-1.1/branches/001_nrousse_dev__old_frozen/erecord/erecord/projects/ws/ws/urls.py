# -*- coding: utf-8 -*-
"""erecord.projects.ws.ws.urls

Urls of the ws project

"""

from django.conf.urls import patterns, include, url

from django.conf import settings
from django.conf.urls.static import static

from django.views.generic import TemplateView

from erecord_cmn import views as cmn_views
from erecord_db.views import menus as db_views_menus

# to enable the admin:
#from django.contrib import admin
#admin.autodiscover()

# internationalisation, translations
urlpatterns = patterns('',
    (r'^i18n/', include('django.conf.urls.i18n')),
    )

# index.html to be written ! root page :
#  - presentation,
#  - access to menu,
#  - html and web services aspects...
# index page (root)
urlpatterns += patterns('',
    url(r'^$', TemplateView.as_view(template_name='erecord_cmn/index.html'),
        name="erecord_cmn-index-page"),
    )

# home page (web user interface : idem erecord_db-menu-main-page-list,erecord_db-menu-main-page-detail)
urlpatterns += patterns('',
    url(r'^home/$', db_views_menus.MenuMainPageList.as_view(),
        name='erecord-home-page'),
    url(r'^menu/$', db_views_menus.MenuMainPageList.as_view()),
    url(r'^menu/(?P<pk>[0-9]+)/$', db_views_menus.MenuMainPageDetail.as_view()),
    )

# online documentation page
urlpatterns += patterns('',
    url(r'^docs/', cmn_views.onlinedoc_page, name="erecord_cmn-onlinedoc-page"),
    )

# database (main)
urlpatterns += patterns('',
    url(r'^db/', include('erecord_db.urls')),
    )

# vpz manipulation
urlpatterns += patterns('',
    url(r'^vpz/', include('erecord_vpz.urls')),
    )

# slm downloadable and uploaded files management, and media

urlpatterns += patterns('',
    url(r'^slm/', include('erecord_slm.urls')),
    )

urlpatterns += patterns( '',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
     {'document_root':settings.MEDIA_ROOT}, name="media"),
)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

