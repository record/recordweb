# -*- coding: utf-8 -*-
"""erecord.projects.ws.ws.wsgi

WSGI config for ws project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""

#original
#
#import os
#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ws.settings")
#
#from django.core.wsgi import get_wsgi_application
#application = get_wsgi_application()

import os
import sys

# ws.settings path 
ws_settings_path = os.path.normpath( os.path.dirname(os.path.dirname(__file__)))
if ws_settings_path not in sys.path :
    sys.path.insert(0, ws_settings_path)

os.environ['DJANGO_SETTINGS_MODULE'] = 'ws.settings'

# Only if a virtual environment is used
apps_path = os.path.normpath( os.path.join(ws_settings_path, '..', '..', 'apps') )
if apps_path not in sys.path :
    sys.path.insert(0, apps_path)
from erecord_cmn.configs.config import IN_PRODUCTION
if IN_PRODUCTION :
    activate_path = "/opt/erecord/factory/erecordenv/bin/activate_this.py"
    activate_this = os.path.expanduser(activate_path)
    execfile(activate_this, dict(__file__=activate_this))

    # added because were not in pythonpath
    python_path = "/opt/erecord/factory/erecordenv/lib/python2.7"
    if python_path not in sys.path :
        sys.path.insert(0, python_path)
    libdynload_path = "/opt/erecord/factory/erecordenv/lib/python2.7/lib-dynload"
    if libdynload_path not in sys.path :
        sys.path.insert(0, libdynload_path)

#import django.core.handlers.wsgi
#application = django.core.handlers.wsgi.WSGIHandler()
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

