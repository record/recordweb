# -*- coding: utf-8 -*-
"""ws project tests """

from django.db import models
from erecord_vpz.models import VpzPath
from erecord_db.models import VleRep
from erecord_db.models import VlePkg
from erecord_db.models import VleVpz
from erecord_cmn.configs.config import REPOSITORIES_HOME

from django.core.urlresolvers import reverse

from rest_framework import status

#from django.test import TestCase
from rest_framework.test import APITestCase

# follow does not work with APIClient
from django.test import Client
#from rest_framework.test import APIClient

import os
import json

class VpzTestUtils(object):
    """Utils about erecord_vpz application tests"""

    def createVpzPathIfNotExists(self, verbose_name, path):
        """ Creates VpzPath if not exists """

        cr_ok = True # default
        vpzpath = VpzPath.objects.all().filter(verbose_name=verbose_name)
        if not vpzpath : # creation (does not exist in test database)
            if os.path.isfile(path) :
                VpzPath.objects.create(verbose_name=verbose_name, vpzpath=path)
            else :
                cr_ok = False # path unavailable file
        #else : # no creation (already exists in test database)
        return cr_ok

    def lookForVpzPathId(self, vpzpath_list_response, verbose_name):
        """Looks for a VpzPath with verbose_name value and returns its id.

        None value if not found.
        The search is done in vpzpath_list_response information resulting
        from a test database query
        """

        id = None
        for vpzpath in vpzpath_list_response.data :
            if vpzpath['verbose_name'] == verbose_name :
                id = vpzpath['id']
                return id
        return id

class DbTestUtils(object):
    """Utils about erecord_db application tests"""

    pass


class VpzTestCase(APITestCase, VpzTestUtils):
    """Tests about erecord_vpz application

    Here are tests methods that can be called in test cases (test_* methods)
    """

    def ControlAndGetBegin(self, input_response):
        """Verify and returns expected begin.

        input_response results from a vpz/input request in 'compact' mode.
        """

        msg = "begin not found"
        self.assertTrue('begin' in input_response.data.keys(), msg)
        begin = input_response.data['begin']
        return begin

    def ControlAndGetDuration(self, input_response):
        """Verify and returns expected duration.

        input_response results from a vpz/input request in 'compact' mode.
        """

        msg = "duration not found"
        self.assertTrue('duration' in input_response.data.keys(), msg)
        duration = input_response.data['duration']
        return duration

    def ControlCondInConds(self, input_response, cond_name):
        """Verify expected conds containing (at least) cond

        input_response results from a vpz/input request in 'compact' mode.
        """
        self.assertTrue('conds' in input_response.data.keys())
        cond_found = False
        for cond in input_response.data['conds'] :
            if cond['selection_name']==cond_name :
                cond_found = True
        msg = "condition '" + cond_name + "' not found in 'conds'"
        self.assertTrue(cond_found, msg)

    def ControlAndGetParameter(self, input_response, cname, pname):
        """Verify expected pars containing (at least) par and returns value 
        and selection_name of par.

        input_response results from a vpz/input request in 'compact' mode.
        """

        self.assertTrue('pars' in input_response.data.keys())
        value = None
        for par in input_response.data['pars'] :
            if par['cname']==cname and par['pname']==pname :
                value = json.loads(par['value'])
                selection_name = par['selection_name']
        msg = "parameter (cname=" + cname + ",pname=" + pname + \
              ") not found in 'pars'"
        self.assertTrue(value is not None, msg)
        return (value, selection_name)

    def ControlAndGetInCompactlistParameter(self, input_response,
            selection_name):
        """Verify expected par and returns its value. 

        input_response results from a vpz/input request in 'compactlist' mode.
        """

        msg = "parameter '" + selection_name + "' not found"
        self.assertTrue(selection_name in input_response.data.keys(), msg)
        value = json.loads(input_response.data[selection_name])
        return value

    def ControlInCompactlistViewInViews(self, input_response, view_name):
        """Verify expected views containing (at least) view

        input_response results from a vpz/input request in 'compactlist' mode.
        """

        self.assertTrue('views' in input_response.data.keys())
        msg = "view '" + view_name + "' not found in 'views'"
        self.assertTrue(view_name in input_response.data['views'], msg)

    def ControlInCompactlistOutInOuts(self, input_response, out_name):
        """Verify expected outs containing (at least) out

        input_response results from a vpz/input request in 'compactlist' mode.
        """

        self.assertTrue('outs' in input_response.data.keys())
        msg = "out '" + out_name + "' not found in 'outs'"
        self.assertTrue(out_name in input_response.data['outs'], msg)

class WsTestCase(VpzTestCase):
    """ws project tests : erecord_vpz and erecord_db applications"""

    def setUp(self):

        # Setup for VpzPath based on vle/vle.examples/smartgardener.vpz
        self.vpzpath_name = "smartgardener for test"
        path=os.path.join(REPOSITORIES_HOME, 'vle', 'pkgs-1.1',
                          'vle.examples', 'exp', 'smartgardener.vpz' )
        self.init_vpzpath_ok = self.createVpzPathIfNotExists(
                           verbose_name=self.vpzpath_name, path=path)

    def test_vpz_from_vpzpath(self):
        """Test of a simulator based on a VpzPath
        
        Main test steps :
        - GET vpz/input to see the simulator (input information)
        - POST vpz/input to validate the input information modifications
        - POST vpz/output to simulate
        """
        msg = "the test can't be run (set up failed)"
        self.assertTrue(self.init_vpzpath_ok, msg)

        #client = APIClient(enforce_csrf_checks=True) #client = APIClient()
        client = Client(enforce_csrf_checks=True) #client = Client()

        url = reverse('erecord_vpz-vpzpath-list') # vpz/vpzpath
        print "*** GET", url, \
              "(for all the vpzpath ; ...to find id of vpzpath '", \
              self.vpzpath_name, "')"
        response = client.get(url, {'format':'json', 'mode':'tree'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.vpzpath_id = self.lookForVpzPathId(vpzpath_list_response=response,
                              verbose_name=self.vpzpath_name)
        msg = "the test can't be continued (set up failed)"
        self.assertTrue(self.vpzpath_id is not None, msg)

        # vpz/vpzpath/id
        url = reverse('erecord_vpz-vpzpath-detail', args=(self.vpzpath_id,))
        print "*** GET", url, "(for the vpzpath with id", self.vpzpath_id, \
              " ; ...to verify that the vpzpath with id", self.vpzpath_id, \
              "is '", self.vpzpath_name, "')"
        response = client.get(url, {'format':'json', 'mode':'tree'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], self.vpzpath_id)
        self.assertEqual(response.data['verbose_name'], self.vpzpath_name)

        url = reverse('erecord_vpz-input') # vpz/input/?vpzpath=id
        print "*** GET", url, "(to edit as is '", self.vpzpath_name, \
              "' ; ...to get and verify input information)"

        data = {'vpzpath':self.vpzpath_id,'format':'json', 'mode':'compact'}
        response = client.get(path=url, data=data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #print "response.data : ", response.data
        #print "response.redirect_chain : ", response.redirect_chain

        # verify expected information and get values (of begin, duration, a, b)
        begin_0 = self.ControlAndGetBegin(input_response=response)
        duration_0 = self.ControlAndGetDuration(input_response=response)

        # expected cond_params condition
        self.ControlCondInConds(input_response=response,
                                cond_name='cond_params')

        # expected cond_params.a and cond_params.b parameters
        (a_0, a_selection_name) = self.ControlAndGetParameter(
            input_response=response, cname='cond_params', pname='a')
        (b_0, b_selection_name) = self.ControlAndGetParameter(
            input_response=response, cname='cond_params', pname='b')

        print "begin_0 : ", begin_0, " ; duration_0 : ", duration_0, \
              " ; a_0 : ", a_0, " ; b_0 : ", b_0

        #client = Client(enforce_csrf_checks=True) #client = Client()

        # vpz/input/?vpzpath=id
        url = reverse('erecord_vpz-input')
        print "*** POST", url, "(to edit after modification '", \
              self.vpzpath_name,\
              "' ; ...to get and verify modified input information)"

        begin_new = 3.0
        duration_new = 4.0
        b_new = 0.012 
        data = {'vpzpath':self.vpzpath_id,'format':'json'}
        data['mode'] = 'compactlist'
        data['parselect'] = 'cond_params'
        data['outselect'] = 'all'
        data['begin'] = begin_new 
        data['duration'] = duration_new 
        data[b_selection_name] = b_new 

        response = client.post(path=url, data=data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #print "response.data : ", response.data
        #print "response.redirect_chain : ", response.redirect_chain

        # verify expected information and get values (begin, duration, a, b)
        begin_1 = self.ControlAndGetBegin(input_response=response)
        duration_1 = self.ControlAndGetDuration(input_response=response)

        # expected cond_params.a and cond_params.b parameters
        a_1 = self.ControlAndGetInCompactlistParameter(
                  input_response=response, selection_name=a_selection_name)
        b_1 = self.ControlAndGetInCompactlistParameter(
                  input_response=response, selection_name=b_selection_name)

        print "begin_1 : ", begin_1, " ; duration_1 : ", duration_1, \
              " ; a_1 : ", a_1, " ; b_1 : ", b_1

        msg = "error in begin (changed) value"
        self.assertEqual(begin_1, begin_new, msg)
        msg = "error in duration (changed) value"
        self.assertEqual(duration_1, duration_new, msg)
        msg = "error in " + a_selection_name + " parameter (unchanged) value"
        self.assertEqual(a_1, a_0, msg)
        msg = "error in " + b_selection_name + " parameter (changed) value"
        self.assertEqual(b_1, b_new, msg)

        # expected view in views and some expected out in outs
        v_name = 'view'
        y_name = 'smartgardener,bio:ladybird.y' # name used in the results
        x_name = 'smartgardener,bio:plantlouse.x' 
        y_selection_name = v_name +'.'+ y_name
        x_selection_name = v_name +'.'+ x_name
        self.ControlInCompactlistViewInViews(input_response=response,
            view_name=v_name)
        self.ControlInCompactlistOutInOuts(input_response=response,
            out_name=y_selection_name)
        self.ControlInCompactlistOutInOuts(input_response=response,
            out_name=x_selection_name)

        # vpz/output/?vpzpath=id
        url = reverse('erecord_vpz-output')
        print "*** POST", url, "(to simulate after modification '", \
              self.vpzpath_name,\
              "' ; ...to get and verify simulation results)"

        data = {'vpzpath':self.vpzpath_id,'format':'json'}
        data['outselect'] = [x_selection_name, y_selection_name]
        data['begin'] = begin_new 
        data['duration'] = duration_new 
        data[b_selection_name] = b_new 
        print "data output request : ", data
        response = client.post(path=url, data=data, follow=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #print "response.data : ", response.data
        #print "response.redirect_chain : ", response.redirect_chain

        self.assertTrue('res' in response.data.keys())
        res = json.loads(response.data['res'])
        self.assertTrue(v_name in res.keys())
        self.assertTrue(y_name in res[v_name].keys())
        self.assertTrue(x_name in res[v_name].keys())
        self.assertTrue('time' in res[v_name].keys())
        y = res[v_name][y_name]
        x = res[v_name][x_name]
        time = res[v_name]['time']
        print "time:", time
        print "x:", x
        print "y:", y
        self.assertEqual(time[0], begin_new)
        self.assertTrue( time[-1] <= (begin_new+duration_new))

    def test_vpz_from_vlevpz(self):
        """Test based on recordb/wwdm/wwdm.vpz

        Creates the corresponding VleRep, VlePkg, VleVpz

        GET vpz/input to see
        POST vpz/input to validate
        POST vpz/output to simulate
        """
        print "VpzTestCase.test_vpz_from_vlevpz..."


        #vlevpz = VleVpz.objects.all().filter(name="smartgardener.vpz")
        #vlevpz = VleVpz.objects.all()
        #print "vlevpz LIST : ", vlevpz
        #for e in vlevpz :
        #    print "OOOOOOOOOOOOOOOOOOOOO", e.name

        client = Client(enforce_csrf_checks=True)
        #response = client.get('/db/vpz/', {'format':'json','mode':'compact'})


