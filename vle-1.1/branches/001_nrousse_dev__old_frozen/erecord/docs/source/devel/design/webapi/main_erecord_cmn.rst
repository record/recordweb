.. _webapi_erecord_cmn:

===============================
Web API erecord_cmn application
===============================

Options
=======

* :doc:`opt_mode`

  :doc:`opt_style` ( :doc:`more <include/opt_style_more>` )

  :doc:`opt_plan_restype` ( :doc:`more <include/opt_plan_restype_more>` )

  :doc:`opt_todownload` ( :doc:`more <include/opt_todownload_more>` )

  :doc:`opt_storage` ( :doc:`more <include/opt_storage_more>` )

* :doc:`opt_format`

More
====
.. include:: ../../../include/more.rst
