
The 'db' web services are provided by the :term:`erecord_db` application that
is dedicated to the **models repositories** that are recorded into the database
(as :ref:`dm_vlerep` , :ref:`dm_vlepkg`, :ref:`dm_vlevpz`).

If the :term:`erecord_db` application is installed, then the
:term:`vpz (as VpzAct)` of the 'vpz' web services can be defined from a
:ref:`dm_vlevpz`.

The 'db' web services are mainly **menus**, **lists and details**.

