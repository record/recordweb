.. _get_db_vpz_id_name:

====================
GET db/vpz/{Id}/name
====================

Resource URL
============
:ref:`online_url`\ **/db/vpz/{Id}/name**

Description
===========

Returns the name of the :term:`simulator` having the Id value (as
:ref:`dm_vlevpz`).

Request parameters
==================

* *Id* : id value of the simulator as :ref:`dm_vlevpz`

* :ref:`webapi_opt_format` 

Response result
===============

"name" : name of the of the simulator as :ref:`dm_vlevpz`

