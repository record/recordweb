.. _webapi_erecord_db_others:

Menus
-----

Menus corresponding with the Web User Interface
+++++++++++++++++++++++++++++++++++++++++++++++

    See :ref:`webui`

Some other menus
++++++++++++++++

  - db/menu/misc : menu for all the models repositories, models and simulators.

  *The following other menus are not kept up-to-date :*

  - *db/home* 
  - *db/home/lists*
  - *db/home/detail/{Id}* for the object (models repository, model, simulator)
    having the Id value.

Admin requests (restricted access)
----------------------------------

  - *db/admin* : access to the admin web interface
  - *db/admin/doc* : some documentation online 

