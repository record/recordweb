.. _get_vpz_inout:

=============
GET vpz/inout
=============

Resource URL
============
:ref:`online_url`\ **/vpz/inout**

Description
===========

Returns the **simulation results** of a simulator (see
:term:`output information of a vpz`) after having run simulation, and
returns also its **input conditions** (see :term:`input information of a vpz`).

Details
-------

*style*
    .. include:: include/vpz_style_inout.rst

Help
----

See :ref:`an example as help on how to know the simulators that can be chosen
with the 'vpz' option <wwdm_ident>`.

Request parameters
==================
**Required** : either vpz or vpzpath must be provided.

* :ref:`webapi_activity_vpz_choice`

* :ref:`webapi_opt_plan_restype`

* :ref:`webapi_opt_style`            
* :ref:`webapi_opt_format`

Response result
===============

The returned result is about both :ref:`dm_vpzoutput` and :ref:`dm_vpzinput`.

Example
=======

    See :ref:`wwdm_bothasis`.

More
====

This resource is part of :ref:`webapi_vpz`

Go to :doc:`include/vpz_inout_more`

