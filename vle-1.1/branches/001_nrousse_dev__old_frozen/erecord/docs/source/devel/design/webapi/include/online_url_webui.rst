.. _webui:

==================
Web User Interface
==================

The `Web User Interface <http://127.0.0.1:8000/db/menu/>`_ makes easier calling the :term:`erecord` web services.

Main menu
=========

**Main menu** to select a simulator, and then see or/and run it :

    http://127.0.0.1:8000/home

Submenus
========

Menu to see a simulator *after having selected it if {Id} was not given* :
 http://127.0.0.1:8000/db/menu/input/{Id}

Menu to run a simulator *after having selected it if {Id} was not given* :
 http://127.0.0.1:8000/db/menu/output/{Id}

Menu to see and run a simulator *after having selected it if {Id} was not given* :
 http://127.0.0.1:8000/db/menu/inout/{Id}

Menu to see or/and run a simulator *after having selected it if {Id} was not given* :
 http://127.0.0.1:8000/db/menu/{Id}

.. note::
   The :ref:`webapi_db_rep_filter`, :ref:`webapi_db_pkg_filter` and
   :ref:`webapi_db_vpz_filter` options can be used to filter the simulators
   shown to be selected.

   Examples :

   http://127.0.0.1:8000/db/menu/input/?rep=2

   http://127.0.0.1:8000/db/menu/?pkg=19

   http://127.0.0.1:8000/db/menu/output/?pkg=41

.. note::
   About some of the URLs above : *'menu'* can be directly used instead of
   *'db/menu'*

