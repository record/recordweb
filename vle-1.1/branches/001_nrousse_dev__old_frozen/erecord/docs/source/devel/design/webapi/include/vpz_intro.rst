
The 'vpz' web services are provided by the :term:`erecord_vpz` application.
They allow to **modify and run simulators**.

The main 'vpz' web services are **activities** on :term:`vpz (as VpzAct)`,
consisting in seeing and may be modifying :term:`vpz input information`,
running simulation and seeing the resulting :term:`vpz output information`.

Some other 'vpz' web services : **reports**, **lists and details**, **menus**.

