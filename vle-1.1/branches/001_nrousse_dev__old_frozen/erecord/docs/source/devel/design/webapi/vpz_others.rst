.. _webapi_erecord_vpz_others:

Menus
-----

  *The following menus are not kept up-to-date :*

  - *vpz/home*
  - *vpz/home/lists/*
  - *vpz/home/detail/{Id}* for the 'object' having the Id value.
  - *vpz/home/services/{Id}* for the simulator having the Id value (as VleVpz),
    for a default simulator if no Id given.


Admin requests (restricted access)
----------------------------------

  - *vpz/admin* : access to the admin web interface
  - *vpz/admin/doc* : some documentation online 

Lists and details about results of already done activities
----------------------------------------------------------

  The following resources are useful/available only if ACTIVITY_PERSISTENCE
  values True (see the configuration file config.py of the erecord_cmn
  application).

  - *vpz/vpzact*, *vpz/vpzact/{Id}*
  - *vpz/vpzorigin*, *vpz/vpzorigin/{Id}*
  - *vpz/vpzworkspace*, *vpz/vpzworkspace/{Id}*
  - *vpz/vpzinput*, *vpz/vpzinput/{Id}*
  - *vpz/begin*, *vpz/begin/{Id}*
  - *vpz/duration*, *vpz/duration/{Id}*
  - *vpz/cond*, *vpz/cond/{Id}*
  - *vpz/par*, *vpz/par/{Id}*
  - *vpz/vpzoutput*, *vpz/vpzoutput/{Id}*
  - *vpz/view*, *vpz/view/{Id}*
  - *vpz/out*, *vpz/out/{Id}*

