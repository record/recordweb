
In addition to the style values 'link' and 'tree', the 'compact' value is also
available (*and also 'compactlist' value, having here the same meaning as
'compact' value*). The default style value is 'tree'.

The 'compact' value is for the output datas to be named by their
'selection_name'.

