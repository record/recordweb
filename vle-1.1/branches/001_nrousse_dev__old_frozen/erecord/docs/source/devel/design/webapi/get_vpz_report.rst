.. _get_vpz_report:

==============
GET vpz/report
==============

Resource URL
============
:ref:`online_url`\ **/vpz/report**

Description
===========

Produces and returns the required **reports**, from the
**simulation results** of a simulator (see
:term:`output information of a vpz`) after having run simulation, and also
from its **input conditions** (see :term:`input information of a vpz`).

Details
-------

*reports*

    The built reports are gathered and returned into a '.zip' file.

Help
----

See :ref:`an example as help on how to know the simulators that can be chosen
with the 'vpz' option <wwdm_ident>`.

Request parameters
==================
**Required** : either vpz or vpzpath must be provided.

* :ref:`webapi_activity_vpz_choice`

* :ref:`webapi_opt_plan_restype`

* :ref:`webapi_opt_report`
* :ref:`webapi_opt_todownload`
* :ref:`webapi_opt_format` 

Response result
===============

The returned result is reports about both :ref:`dm_vpzoutput` and
:ref:`dm_vpzinput`.

Example
=======

    *... under construction ...*

More
====

This resource is part of :ref:`webapi_vpz`

