.. _webapi_db_rep_filter:

===
rep
===

The 'rep' option is used to specify which :term:`models repository` to keep (filtering all the other ones). The models repository under consideration is a :ref:`dm_vlerep`.

- rep

    - value : id of the relevant :ref:`dm_vlerep`

.. note::

   To know the existing models repositories : :ref:`get_db_rep` resource (see 
   :ref:`the 'db' web services <webapi_db_resources>`)

   You will find help in the :ref:`examples <erecord_examples>`, especially
   in :ref:`wwdm_ident`.

