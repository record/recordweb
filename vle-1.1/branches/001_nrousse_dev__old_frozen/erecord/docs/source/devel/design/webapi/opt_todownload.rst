.. _webapi_opt_todownload:

===================
mode for todownload
===================

The :ref:`'mode' option <webapi_opt_mode>` is used to choose whether the
content result is sent or will have to be downloaded.

Value 'todownload' (mode=todownload) for the content result to be downloaded.

