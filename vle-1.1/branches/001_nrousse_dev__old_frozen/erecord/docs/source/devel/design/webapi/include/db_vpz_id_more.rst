
======================
More about db/vpz/{Id}
======================

The 'db/vpz/{Id}' resource is one of the resources of :ref:`webapi_db`.

Code
====

The corresponding class is VleVpzDetail.

For more about VleVpzList, see :

    .. autoclass:: erecord_db.views.objects.VleVpzDetail
       :members:
