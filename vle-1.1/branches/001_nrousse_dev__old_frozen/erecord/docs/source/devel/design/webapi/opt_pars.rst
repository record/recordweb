.. _webapi_opt_pars:

=====================
pars or *cname.pname*
=====================

   *... under construction ...*

* pars : to modify values of some parameters of the experiment :

    - pars = list of dict, each dict being dedicated to a parameter.
      The dict of the parameter named pname of the condition named cname 
      with s as selection_name *(s valuing cname.pname)* is :
      { "selection_name": s, "cname": cname, "pname": pname, "value": newvalue }

* cname.pname : There is another way to modify the value of this parameter, by 
  using the option whose name is composed of cname and pname :

    - cname.pname=newvalue
