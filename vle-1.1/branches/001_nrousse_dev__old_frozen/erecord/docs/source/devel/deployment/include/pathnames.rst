
=====================
Deployment path names
=====================

Here is the presentation of :

- Some names used in the deployment description, to call some paths.
- Some existing files invoked in the deployment description.

.. _deploy_root_path:

root_path
=========

Let's name **root_path** the installation root path. 

In the deployment example, :ref:`deploy_root_path` is '/opt'.

.. _deploy_erecord_path:

erecord_path
============

Let's name **erecord_path** the erecord project software installation path. 
erecord_path is 'erecord' under :ref:`deploy_root_path`.

In the deployment example, erecord_path is '/opt/erecord'.

.. _deploy_erecordenv_path:

erecordenv_path
===============

Let's name **erecordenv_path** the directory where the erecordenv virtualenv is
installed.

In the deployment example, erecordenv_path is 'factory' under
:ref:`deploy_erecord_path`, ie '/opt/erecord/factory'.

.. _deploy_erecord_docs_path:

erecord_docs_path
=================

Let's name **erecord_docs_path** the documentation directory of the erecord
project software. erecord_docs_path is 'docs' under :ref:`deploy_erecord_path`.

In the deployment example, erecord_docs_path is '/opt/erecord/docs'.

.. _deploy_erecord_repositories_path:

erecord_repositories_path
=========================

Let's name **erecord_repositories_path** the models repositories directory of
the erecord project software. erecord_repositories_path is 'repositories' under :ref:`deploy_erecord_path`.

In the deployment example, erecord_repositories_path is
'/opt/erecord/repositories'.

.. _deploy_requirement.txt_file:

requirement.txt file
====================

The requirement.txt file is part of the erecord package. It is used to install
some python modules. Its location is 'erecord/install' under
:ref:`deploy_erecord_path`.

In the deployment example its full path is
'/opt/erecord/erecord/install/requirement.txt'.

.. _deploy_wsgi.py_file:

wsgi.py file
============

The wsgi.py file is part of the ws project. Its location is
'erecord/projects/ws/ws' under :ref:`deploy_erecord_path`.

In the deployment example its full path is
'/opt/erecord/erecord/projects/ws/ws/wsgi.py'.

.. _deploy_config.py_file:

config.py file
==============

The config.py file is part of the erecord_cmn application. Its location is
'erecord/erecord/apps/erecord_com/config' under :ref:`deploy_erecord_path`.
In the deployment example its full path is
'/opt/erecord/erecord/apps/erecord_cmn/configs/config.py'.

.. _deploy_erecord_ws_file:

erecord_ws file
===============

The erecord_ws file is part of the ws project. Its location is
'erecord/erecord/projects/ws/ws' under :ref:`deploy_erecord_path`.
In the deployment example its full path is
'/opt/erecord/erecord/projects/ws/ws/erecord_ws'.

