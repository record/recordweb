
* /etc/apache2/ports.conf file modifications :

Case port 8000 dedicated to the erecord_ws site
(see file /opt/erecord/erecord/projects/ws/ws/erecord_ws file)
Case port 80 dedicated to online documentation

    NameVirtualHost *:80
    Listen 8000
    Listen 80
    ...
    <IfModule mod_ssl.c>
        # SSL name based virtual hosts are not yet supported, therefore no
        # NameVirtualHost statement here
        Listen 443
    </IfModule>

