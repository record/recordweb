
.. _deploy_development_environment_install:

====================================
Development environment installation
====================================

Installation of LibYAML
=======================

    LibYAML is needed by pyyaml.

    .. literalinclude:: include/cmd_install_libyaml.rst

Installation of the erecordenv virtualenv 
=========================================

    The python modules are installed in a **virtualenv**.

erecordenv virtualenv creation
..............................

    The erecordenv virtualenv is created under :ref:`deploy_erecordenv_path`.

    .. literalinclude:: include/cmd_create_erecordenv_virtualenv.rst

    .. warning:: 
       Problem met on Ubuntu 14.04 (development situation) while 'virtualenv'
       command : "_sysconfigdata_nd.py missing in /usr/lib/python2.7/"

       Has been resolved by commands : cd /usr/lib/python2.7 ;
       sudo ln -s plat-x86_64-linux-gnu/_sysconfigdata_nd.py

python modules installation
...........................

    *Python libraries may be installed via pip, easy_install,
    "python setup.py install"...*

erecordenv virtualenv activation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    The activation command is into the erecordenv virtualenv under 
    :ref:`deploy_erecordenv_path`.

    .. literalinclude:: include/cmd_activate_erecordenv_virtualenv.rst

python modules installation
~~~~~~~~~~~~~~~~~~~~~~~~~~~

    About **requirement.txt** file : :ref:`more <deploy_requirement.txt_file>`.

    .. literalinclude:: include/cmd_install_python_modules.rst

Installation of the vle environment
===================================

    It consists in (i) installing vle-1.1, (ii) installing pyvle-1.1 (into the
    erecordenv virtualenv).

    See http://www.vle-project.org/wiki/VLE-1.1

    *Some more notes* : :ref:`cmd_install_vle_pyvle`

