Install LibYAML version 0.1.5
=============================

* Download and extract the source package :
wget http://pyyaml.org/download/libyaml/yaml-0.1.5.tar.gz
tar -zxvf yaml-0.1.5.tar.gz

* Build and install LibYAML :
./configure
make
sudo make install

