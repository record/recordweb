
* apache2.2 :
sudo apt-get install apache2 apache2.2-common apache2-utils libexpat1 ssl-cert

* apache2-mpm-prefork :
sudo apt-get install apache2-mpm-prefork

* mod_wsgi :
sudo apt-get install libapache2-mod-wsgi

