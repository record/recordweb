
.. _deploy_deployment_environment_install:

===================================
Deployment environment installation
===================================

Installations
=============

    .. literalinclude:: include/cmd_install_deploy_environment.rst

Transformations
===============

ports.conf file modifications
.............................

    Modify the apache2 ports.conf file so that the Apache2 server listens to
    the port dedicated to the erecord_ws site and the port dedicated to the
    online documentation.

    .. literalinclude:: include/cmd_modify_portsconf.rst

apache2.conf file modifications
...............................

    .. literalinclude:: include/cmd_modify_apache2conf.rst

