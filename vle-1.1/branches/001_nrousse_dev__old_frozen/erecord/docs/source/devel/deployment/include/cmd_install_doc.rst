
* add server entry :
sudo cp /opt/erecord/docs/server/docs.html /opt/erecord/factory/docs/docs.html
sudo cp /opt/erecord/docs/server/index.html /opt/erecord/factory/docs/index.html

* save maybe existing /var/www/docs.html,index.html :
sudo mv /var/www/docs.html /var/www/docs_svgnewname.html
sudo mv /var/www/index.html /var/www/index_svgnewname.html

* install under /var/www :
sudo ln -s /opt/erecord/factory/docs /var/www/erecord
sudo ln -s /var/www/erecord/docs.html /var/www/docs.html
sudo ln -s /var/www/erecord/index.html /var/www/index.html

