
.. _deploy_erecord_modify_and_generate:

==============================================
erecord software modifications and generations
==============================================

Modifications
=============

wsgi.py file
............

About **wsgi.py** file : :ref:`more <deploy_wsgi.py_file>`.

Into the wsgi.py file, put the appropriate path about the erecordenv
virtualenv : the activate_this.py file location is 'erecordenv/bin' under :ref:`deploy_erecordenv_path`.

    .. literalinclude:: include/cmd_modify_wsgipy.rst

config.py file
..............

About **config.py** file : :ref:`more <deploy_config.py_file>`.

    .. literalinclude:: include/cmd_modify_configpy.rst

erecord_ws file
...............

About **erecord_ws** file : :ref:`more <deploy_erecord_ws_file>`.

Into the erecord_ws file, give the appropriate :ref:`deploy_erecordenv_path`
value :

*...? more modifications ? : statics...*

    .. literalinclude:: include/cmd_modify_erecord_ws.rst

Install the erecord_ws file in the appropriate location and activate the
erecord_ws site :

    .. literalinclude:: include/cmd_install_and_activate_erecord_ws.rst

URLs
....

Some URL values directly appear in source code (:ref:`config.py file <deploy_config.py_file>`, online_url.rst documentation file...). They have to be in conformity with the :ref:`erecord_ws file <deploy_erecord_ws_file>` and with the documentation installation (see :ref:`below <geninstall_documentation>`) :

    .. literalinclude:: include/cmd_modify_urlvalues.rst

Generation/installation
=======================

.. _geninstall_documentation:

Documentation
.............

Activate erecordenv virtualenv (if not yet done) :

    :ref:`cmd_activate_erecordenv_virtualenv`

Generation :

    The generation commands are done under :ref:`erecord_docs_path
    <deploy_erecord_docs_path>`.

    .. literalinclude:: include/cmd_make_doc.rst

Installation :

    Installation under /var/www :

    .. literalinclude:: include/cmd_install_doc.rst

Static files
............

    The static files commands are done under 'erecord/projects/ws' under
    :ref:`deploy_erecord_path`.

    .. literalinclude:: include/cmd_static_files.rst

