
.. _deploy_run_server:

===========================
Running server instructions
===========================

The apache2 user (user www-data and group www-data) must be the owner of
the :ref:`deploy_root_path` and of /var/www (online documentation) :

    sudo chown www-data:www-data -R /opt

    sudo chown www-data:www-data -R /var/www

Activate erecordenv virtualenv (if not yet done) :

    .. literalinclude:: include/cmd_activate_erecordenv_virtualenv.rst

Install and activate the erecord_ws site (if not yet done) :

    .. literalinclude:: include/cmd_install_and_activate_erecord_ws.rst

Apache server commands :

    .. literalinclude:: include/cmd_run_apache.rst

The erecord web site *(online documentation)* and the erecord web services are
now both online at their erecord virtual machine *(whose IP address is ... )* : 

    - erecord web site online at http://erecord.toulouse.inra.fr .
    - erecord web services online at http://erecord.toulouse.inra.fr:8000 .

