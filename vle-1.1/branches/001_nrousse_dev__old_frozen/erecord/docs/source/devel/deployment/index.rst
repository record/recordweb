.. _devel_deployment:

==========
Deployment
==========

Introduction
============

The erecord project software, once developed, can be deployed to a production
server in order to be used by web users.

This implies a deployment environment, in addition to the tools and libraries
already used for development.

Source software
---------------

The source software includes :

    - The erecord project software

    - Some models software, stored as elements of models repositories installed
      into the 'repositories' subdirectory of the erecord project.

Once the installation finished, the resulting hierarchy is such as :

    .. literalinclude:: include/hierarchy_source.rst

Development environment
-----------------------

The erecord project software is developed with :

    - python language (python 2.7 version),
    - django framework,
    - some django applications, like for example django-rest-framework. 
    - SQLite for databases.
    - Sphinx for documentation.

    See the requirement.txt file :

    .. literalinclude:: ../../../../erecord/install/requirement.txt

The stored models software :

    - have previously been developed with vle (vle-1.1 version), a C++ platform.
    - are interfaced with erecord software by pyvle (pyvle-1.1 version).

Deployment environment
----------------------

The software is deployed on a virtual machine with **Debian 7**, **Apache2**
server and **mod_wsgi**.

    *Django’s primary deployment platform is WSGI, the Python standard for web
    servers and applications. mod_wsgi is an Apache module which can host any
    Python WSGI application, including Django.*

Installation procedure in 'from scratch' case
=============================================

Prerequisites
-------------

Some basic tools, installed once and for all : python 2.7, sudo, virtualenv...

    .. literalinclude:: include/cmd_setup.rst


First step : deployment environment installation
------------------------------------------------

The deployment environment installation is done once and for all.

    See :ref:`deploy_deployment_environment_install`.

.. _deploy_beginning_of_erecord_project_software_install:

Second step : beginning of erecord project software installation
----------------------------------------------------------------

The erecord project software installation begins with the code copying. The
following operations will be done later on (see below).

Software copy
+++++++++++++

    The whole erecord project software is copied as
    :ref:`deploy_erecord_path`.

    .. literalinclude:: include/cmd_copy_erecord.rst

    .. note::
       'factory' and 'repositories' under :ref:`deploy_erecord_path` (ie
       '/opt/erecord/factory' and '/opt/erecord/repositories') are empty at
       this stage.
    
       *'/opt/erecord/factory' will contain things once they have been
       generated later on (see the rest of the 'erecord project software
       installation').*
    
       *'/opt/erecord/repositories' will contain some models repositories once
       they have been installed later on.*

Third step : development environment installation
-------------------------------------------------

Prerequisites :

    The :ref:`deploy_beginning_of_erecord_project_software_install` must have
    been done before.

Installation :

    See :ref:`deploy_development_environment_install`.

Fourth step : rest of the erecord project software installation
---------------------------------------------------------------

Now that the erecord project software has been copied (see
:ref:`Beginning of erecord project software installation
<deploy_beginning_of_erecord_project_software_install>`), this consists
in doing the appropriate modifications and generations.

    See :ref:`deploy_erecord_modify_and_generate`.

Fifth step : models software installation
-----------------------------------------

See :ref:`deploy_repositories`.

Running instructions
====================

    See :ref:`deploy_run_server`.

Installation procedure in updating case
=======================================

Development environment updating
--------------------------------

See the concerned part in :ref:`Development environment installation <deploy_development_environment_install>`.

erecord project software updating
---------------------------------

If the **source code** or **documentation** code of the erecord project
software has changed, then the corresponding directories are to be updated
under :ref:`deploy_root_path`.

The concerned directories are 'erecord/erecord' and 'erecord/docs'.

    .. literalinclude:: include/cmd_update_erecord.rst

    .. note::
       Be careful not to lose some **models repositories** by overwriting
       'erecord/repositories' under :ref:`deploy_root_path`.

       Be careful not to inadvertently overwrite the **databases** in
       'erecord/databases' under :ref:`deploy_root_path`.

Then apply on the erecord project software the appropriate modifications and
generations :

    See :ref:`deploy_erecord_modify_and_generate`.

Models software updating
------------------------
Updating an existing model, adding a model...

See :ref:`deploy_repositories`.

=============================================
---------------------
++++++++++++++++++
...............
~~~~~~~~~~~~
