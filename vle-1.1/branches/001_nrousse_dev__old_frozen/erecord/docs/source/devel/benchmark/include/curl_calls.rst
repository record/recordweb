*** Calling erecord web services by cURL command line tool with the "w" option ***

* Information format specified as a literal "string" :

curl -L -H "Content-Type: application/json" -d '{"vpz":266, "mode":["tree","single","dataframe"], "outselect":["view.top:wwdm.LAI","view.top:wwdm.ST"], "format":"json" }' http://erecord.toulouse.inra.fr:8000/vpz/output/ -w "\n\ntime_namelookup:%{time_namelookup}; time_connect:%{time_connect}; time_appconnect:%{time_appconnect}; time_pretransfer:%{time_pretransfer}; time_redirect:%{time_redirect}; time_starttransfer:%{time_starttransfer}; time_total:%{time_total}\n"

curl -L -H "Content-Type: application/json" -d '{"vpz":213, "duration":10.0, "mode":["tree","single","dataframe"], "condLAI.dens":10.0, "outselect":["vueLAI"], "format":"json" }' http://erecord.toulouse.inra.fr:8000/vpz/output/ -w "\n\ntime_namelookup:%{time_namelookup}; time_connect:%{time_connect}; time_appconnect:%{time_appconnect}; time_pretransfer:%{time_pretransfer}; time_redirect:%{time_redirect}; time_starttransfer:%{time_starttransfer}; time_total:%{time_total}\n"

curl -L -H "Content-Type: application/json" -d '{"vpz":213, "duration":350.0, "mode":["tree","single","dataframe"], "condLAI.dens":10.0, "outselect":["vueLAI"], "format":"json" }' http://erecord.toulouse.inra.fr:8000/vpz/output/ -w "\n\ntime_namelookup:%{time_namelookup}; time_connect:%{time_connect}; time_appconnect:%{time_appconnect}; time_pretransfer:%{time_pretransfer}; time_redirect:%{time_redirect}; time_starttransfer:%{time_starttransfer}; time_total:%{time_total}\n"

curl -L -H "Content-Type: application/json" -d '{"vpz":213, "duration":350.0, "mode":["tree","linear","dataframe"], "condLAI.dens":[9.5, 9.6, 9.7, 9.8, 9.9, 10.0, 10.1, 10.2, 10.3, 10.4], "outselect":["vueLAI"], "format":"json" }' http://erecord.toulouse.inra.fr:8000/vpz/output/ -w "\n\ntime_namelookup:%{time_namelookup}; time_connect:%{time_connect}; time_appconnect:%{time_appconnect}; time_pretransfer:%{time_pretransfer}; time_redirect:%{time_redirect}; time_starttransfer:%{time_starttransfer}; time_total:%{time_total}\n"

curl -L -H "Content-Type: application/json" -d '{"vpz":213, "duration":350.0, "mode":["compact","linear","dataframe"], "condLAI.dens":[9.5, 9.6, 9.7, 9.8, 9.9, 10.0, 10.1, 10.2, 10.3, 10.4], "outselect":["vueLAI"], "format":"json" }' http://erecord.toulouse.inra.fr:8000/vpz/output/ -w "\n\ntime_namelookup:%{time_namelookup}; time_connect:%{time_connect}; time_appconnect:%{time_appconnect}; time_pretransfer:%{time_pretransfer}; time_redirect:%{time_redirect}; time_starttransfer:%{time_starttransfer}; time_total:%{time_total}\n"

curl -L -H "Content-Type: application/json" -d '{"vpz":213, "duration":350.0, "mode":["tree","linear","dataframe"], "condLAI.dens":[9.5, 9.6, 9.7, 9.8, 9.9, 10.0, 10.1, 10.2, 10.3, 10.4], "outselect":"all", "format":"json" }' http://erecord.toulouse.inra.fr:8000/vpz/output/ -w "\n\ntime_namelookup:%{time_namelookup}; time_connect:%{time_connect}; time_appconnect:%{time_appconnect}; time_pretransfer:%{time_pretransfer}; time_redirect:%{time_redirect}; time_starttransfer:%{time_starttransfer}; time_total:%{time_total}\n"

* Information format specified into the curl-time-format.txt file :

curl -L -H "Content-Type: application/json" -d '{"vpz":266, "duration":6, "cond_wwdm.Eb":1.86, "mode":["tree","single","dataframe"], "outselect":["view.top:wwdm.LAI","view.top:wwdm.ST"], "format":"json" }' http://erecord.toulouse.inra.fr:8000/vpz/output/ -w "@curl-time-format.txt"

curl -L -H "Content-Type: application/json" -d '{"vpz":213, "duration":350.0, "mode":["compact","single","dataframe"], "condLAI.dens":[9.5, 9.6, 9.7, 9.8, 9.9, 10.0, 10.1, 10.2, 10.3, 10.4], "outselect":["vueLAI"], "format":"json" }' http://erecord.toulouse.inra.fr:8000/vpz/output/ -w "@curl-time-format.txt"

curl -L -H "Content-Type: application/json" -d '{"vpz":213, "duration":350.0, "mode":["compact","linear","dataframe"], "condLAI.dens":[9.5, 9.6, 9.7, 9.8, 9.9, 10.0, 10.1, 10.2, 10.3, 10.4], "outselect":["vueLAI"], "format":"json" }' http://erecord.toulouse.inra.fr:8000/vpz/output/ -w "@curl-time-format.txt"

* Information format specified into the curl-format.txt file :

curl -L -H "Content-Type: application/json" -d '{"vpz":266, "duration":6, "cond_wwdm.Eb":1.86, "mode":["tree","single","dataframe"], "outselect":["view.top:wwdm.LAI","view.top:wwdm.ST"], "format":"json" }' http://erecord.toulouse.inra.fr:8000/vpz/output/ -w "@curl-format.txt"

curl -L -H "Content-Type: application/json" -d '{"vpz":213, "duration":350.0, "mode":["compact","single","dataframe"], "condLAI.dens":[9.5, 9.6, 9.7, 9.8, 9.9, 10.0, 10.1, 10.2, 10.3, 10.4], "outselect":["vueLAI"], "format":"json" }' http://erecord.toulouse.inra.fr:8000/vpz/output/ -w "@curl-format.txt"

curl -L -H "Content-Type: application/json" -d '{"vpz":213, "duration":350.0, "mode":["compact","linear","dataframe"], "condLAI.dens":[9.5, 9.6, 9.7, 9.8, 9.9, 10.0, 10.1, 10.2, 10.3, 10.4], "outselect":["vueLAI"], "format":"json" }' http://erecord.toulouse.inra.fr:8000/vpz/output/ -w "@curl-format.txt"

