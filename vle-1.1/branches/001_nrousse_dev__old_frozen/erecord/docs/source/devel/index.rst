.. _devel:

============================
Development process overview
============================

Design
======

.. toctree::
   :maxdepth: 1

   Web API <design/webapi/index>
   Features <design/features/index>

Source code API reference
=========================

- :ref:`apiref_index`

Documentation, test, deployment
===============================

.. toctree::
   :maxdepth: 2

   documentation/index
   test/index
   deployment/index
   benchmark/index

erecord project content
=======================

.. toctree::
   :maxdepth: 2

   erecord/index

More
====

.. include:: ../include/more.rst

