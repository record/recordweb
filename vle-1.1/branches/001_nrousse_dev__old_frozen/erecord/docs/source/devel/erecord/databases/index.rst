.. _databases:

=========
Databases
=========

Introduction
============

The web services provided by :term:`erecord` allow to edit, modify and simulate
some vle models.

For that, :term:`erecord` manages some models repositories containing some vle
models, containing the simulators that are proposed to users to be manipulated
(seen, modified, simulated).

Before being taken into account, a :term:`simulator` must have been
(i) physically stored into a models repository (see :ref:`repositories`) and
(ii) recorded into an appropriate database.

A possibility is to rely on the **db** database, that is associated with the
erecord_db application. This database contains the following information :

    - models repositories (:ref:`dm_vlerep`),
    - vle models (vle packages) of a models repository (see :ref:`dm_vlepkg`),
    - simulators (vpz file) of a vle model of a models repository (see
      :ref:`dm_vlevpz`).

In that first case, both erecord_db and erecord_vpz applications are needed to
launch simulations.

Another possibility is to rely on the **vpz** database, that is associated
with the erecord_vpz application. The simulators can be simply recorded
into this database, where they are characterized by their vpz file absolute
path (see :ref:`dm_vpzpath`). This path determines the simulator vpz file,
but also its vle model and its models repository.

In that second case, the erecord_vpz application is self sufficient to launch
simulations.

Data model
==========

    See :ref:`domain_dm`

