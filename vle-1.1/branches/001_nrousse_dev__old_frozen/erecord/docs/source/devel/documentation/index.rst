.. _devel_documentation:

=============
Documentation
=============

Introduction
============

The documentation of erecord project is produced with Sphinx.

Sphinx is a documentation generator which converts reStructuredText files into
HTML websites and other formats including PDF, EPub and man).


Production and generation
=========================

Activate erecordenv virtualenv (if not yet done) :

    :ref:`cmd_activate_erecordenv_virtualenv`

Generation :

    The generation commands are done under :ref:`erecord_docs_path
    <deploy_erecord_docs_path>`.

    .. literalinclude:: ../deployment/include/cmd_make_doc.rst


For more, see :ref:`documentation generation and installation <geninstall_documentation>`

