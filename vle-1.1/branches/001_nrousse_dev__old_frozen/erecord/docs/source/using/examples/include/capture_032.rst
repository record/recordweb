.. _erecord_use_capture_032:

=================================================
Example of :ref:`post_vpz_output` in a webbrowser
=================================================

    .. include:: wwdm_wb_intro_postoutput.rst

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters with '**pars**',

  - '**tree**' as style of presentation,

  - value '**single**' as plan, or
    value '**linear**' in case of  multiple simulation
  - value '**dataframe**' as restype *(or 'matrix')*

  - value '**all**' as '**outselect**',

  - with 'application/**json**' as 'Media type'

:download:`REQUEST in case of single plan (single simulation) <capture_request_032.png>`

:download:`REQUEST in case of linear plan (multiple simulation) <capture_request2_032.png>`

    *Memo*

    .. literalinclude:: memo_request_032.rst

