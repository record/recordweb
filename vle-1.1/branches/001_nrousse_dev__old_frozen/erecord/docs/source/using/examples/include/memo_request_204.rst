# Python code :

import pycurl    # version using pycurl
import io        # version using pycurl
#import urllib2   # version using urllib2
import json

#----------------------
# only in python 3

import urllib.parse as urllib_local

#----------------------
# only in python 2

import urllib as urllib_local
from __future__ import print_function

#----------------------

# send GET request and return response datas
def send_get_and_receive(url, id=None, options=None):
    # full_url composed by : url {id} /? options
    full_url = url
    if id is not None :
        full_url = full_url + "%i/" % id
    if options is not None :
        full_url = full_url + "?%s" % urllib_local.urlencode(options)
    print("request GET ", full_url)
    #
    # version using pycurl
    buffer = io.BytesIO()
    c = pycurl.Curl()
    c.setopt(c.CUSTOMREQUEST, 'GET')
    c.setopt(c.URL, full_url)
    c.setopt(c.FOLLOWLOCATION, True) # to follow redirect
    c.setopt(c.WRITEFUNCTION, buffer.write)
    c.perform()
    buffer_str = buffer.getvalue()
    buffer.close()
    buffer_str = buffer_str.decode("utf8")
    responsedata = json.loads(buffer_str)
    #
    # version using urllib2
    #response = urllib2.urlopen(full_url)
    #responsedata = json.loads(response.read())
    #
    #responsedata
    #responsedata.keys()
    return responsedata

pass

