.. _erecord_use_capture_004:

================================================
Example of :ref:`post_vpz_input` in a webbrowser
================================================

    .. include:: wwdm_wb_intro_postinput.rst

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',
  - '**compactlist**' as style of presentation
  - with 'application/**json**' as 'Media type'

:download:`REQUEST <capture_request_004.png>`

    *Memo*

    .. literalinclude:: memo_request_004.rst

