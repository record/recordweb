.. _erecord_use_capture_130:

==========================================================
Example of :ref:`post_vpz_inout` in command line with cURL
==========================================================

    .. include:: wwdm_curl_intro.rst

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - '**tree**' as style of presentation,
  - '**json**' as format,

  - values of type *cname.pname* as '**parselect**' :

    value 'cond_wwdm.A' to select the parameter named 'A' of the condition
    named 'cond_wwdm'.

    value 'cond_wwdm.B' to select the parameter named 'B' of the condition
    named 'cond_wwdm'.

    value 'cond_wwdm.Eb' to select the parameter named 'Eb' of the condition
    named 'cond_wwdm'.

    value 'cond_wwdm.TI' to select the parameter named 'TI' of the condition
    named 'cond_wwdm'.

  - value '**single**' as plan
  - value '**dataframe**' as restype

  - value of type *vname.oname* as '**outselect**' :

    value 'view.top:wwdm.LAI' to select the 'LAI' output data of the view
    named 'view'.

    value 'view.top:wwdm.ST' to select the 'ST' output data of the view
    named 'view'.

  - with 'application/**json**' as 'Content-Type'

:download:`REQUEST and RESPONSE<capture_request_130.png>`

    *Memo*

    .. literalinclude:: memo_request_130.rst

