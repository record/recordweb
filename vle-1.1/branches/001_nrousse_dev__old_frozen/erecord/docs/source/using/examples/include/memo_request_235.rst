# Python code :

#######################
# request and response
#######################

inputdata = {"vpz":266, "format":"json"}
inputdata["duration"] = 6
inputdata["begin"] = 2453982.0

#----------------------
# parameters and mode in case of single plan (single simulation)

# some parameters modification with 'cname.pname'
inputdata["cond_wwdm.A"] = 0.0064
inputdata["cond_wwdm.Eb"] = 1.86

inputdata["mode"]      = ["compact", "single", "dataframe"]
# or inputdata["mode"] = ["compact", "single", "matrix"]

#----------------------
# parameters and mode in case of linear plan (multiple simulation)

# some parameters modification with 'cname.pname' (multiple values)
inputdata["cond_wwdm.A"] = [0.0064,0.0065,0.0066]
inputdata["cond_wwdm.Eb"] = [1.84,1.85,1.86]

inputdata["mode"] = ["compact", "linear", "dataframe"]
# or inputdata["mode"] = ["compact", "linear", "matrix"]

#----------------------

inputdata["outselect"] = ["view.top:wwdm.LAI", "view.top:wwdm.ST"]

responsedata = send_post_and_receive(
    url="http://erecord.toulouse.inra.fr:8000/vpz/output/",
    inputdata=inputdata)

responsedata
keys = responsedata.keys()
keys

##########################################################
# responsedata in case of 'compact' style of presentation
##########################################################

if 'res' in keys and 'plan' in keys and 'restype' in keys :
    content_simulation_results_compact( res=responsedata['res'],
                                        plan=responsedata['plan'],
                                        restype=responsedata['restype'])

pass

