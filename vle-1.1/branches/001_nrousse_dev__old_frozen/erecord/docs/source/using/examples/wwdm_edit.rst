.. _wwdm_edit:

====================
Seeing the simulator
====================

*The erecord web services used in this part are some of the ‘vpz’ web
services* (see :ref:`webapi_vpz`).

Seeing a simulator consists in getting its input information : begin and
duration, parameters... (see :ref:`dm_vpzinput`) maybe after having modified
it.
 
As a reminder, the Id of the simulator ‘wwdm.vpz’ is :ref:`wwdm_vpz_id`
(:ref:`more <wwdm_vpz_id>`).

.. toctree::
   :maxdepth: 1

   wwdm_editasis

.. toctree::
   :maxdepth: 2

   wwdm_editmodif

More
====

*Back to :* :ref:`erecord_examples`

