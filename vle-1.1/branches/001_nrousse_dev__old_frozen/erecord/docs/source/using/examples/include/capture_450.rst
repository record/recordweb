.. _erecord_use_capture_450:

=======================================================================
Example of some :ref:`'db' resources <webapi_db>` calls in PHP language
=======================================================================

The following resources are directly used in this example or could be used in a similar way :
  - :ref:`get_db_rep`, :ref:`get_db_rep_id`
  - :ref:`get_db_pkg`, :ref:`get_db_pkg_id`
  - :ref:`get_db_vpz`, :ref:`get_db_vpz_id`

Use the PHP code into a php tag :

    <?php
      ... PHP code ...
    ?>

    *Memo*

    .. literalinclude:: memo_request_404.rst
    .. literalinclude:: memo_request_450.rst






