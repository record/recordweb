.. _wwdm_editmodif:

=============================
Seeing the modified simulator
=============================

Introduction
============

Seeing the modified simulator corresponds with the :ref:`post_vpz_input` resource.

You may find here : :ref:`help to know how some information is identified/selected in the request <wwdm_ident_fields>`.

A quick overview example
========================

    .. include:: include/wwdm_editmodif_quickoverview_tree.rst

*or lighter* :

    .. include:: include/wwdm_editmodif_quickoverview.rst

Modifications
=============

The different possible modifications are illustrated bellow.

Choosing style of presentation 
------------------------------
*For more see* :ref:`post_vpz_input`.

You can choose your style of presentation (see :ref:`webapi_opt_style`)
between 'link', 'tree', 'compact', 'compactlist'.

    .. include:: include/wwdm_editmodif_quickoverview.rst

    -> *Example* : :ref:`In a webbrowser <erecord_use_capture_002>`

    -> *Example* : :ref:`In a webbrowser <erecord_use_capture_004>`

    .. include:: include/wwdm_editmodif_quickoverview_tree.rst

    .. include:: include/wwdm_editmodif_quickoverview_compact.rst

Modifying begin and duration of simulation
------------------------------------------
*For more see* :ref:`post_vpz_input`.

    .. include:: include/wwdm_editmodif_quickoverview.rst

    -> *Example* : :ref:`In a webbrowser <erecord_use_capture_001>`

    -> *Example* : :ref:`In a webbrowser <erecord_use_capture_006>`

    .. include:: include/wwdm_editmodif_quickoverview_tree.rst

Modifying some parameters
-------------------------
*For more see* :ref:`post_vpz_input`.

To modify the simulator parameters, it is necessary to know their information 
identification ('cname', 'pname'...) : see :ref:`wwdm_ident_parameters`.

A parameter can be modified by two different ways : with 'pars' or
'cname.pname' (see :ref:`webapi_opt_pars`).

Modifying parameters with 'cname.pname'
+++++++++++++++++++++++++++++++++++++++

    .. include:: include/wwdm_editmodif_quickoverview.rst

    -> *Example* : :ref:`In a webbrowser <erecord_use_capture_001>`

    -> *Example* : :ref:`In a webbrowser <erecord_use_capture_003>`

Modifying parameters with 'pars'
++++++++++++++++++++++++++++++++

    .. include:: include/wwdm_editmodif_quickoverview_tree.rst
    
    *Example* : :ref:`In a webbrowser <erecord_use_capture_006>`

Choosing the restituted parameters
----------------------------------
*For more see* :ref:`post_vpz_input`.

You can choose the returned parameters, ask for filtering some of them (see :ref:`webapi_opt_parselect`).

To select parameters or conditions (sets of parameters) to be returned, it is
necessary to know its information identification ('selection_name') : see
:ref:`wwdm_ident_parameters`.

    .. include:: include/wwdm_editmodif_quickoverview.rst

    -> *Example* : :ref:`In a webbrowser <erecord_use_capture_007>`

    -> *Example* : :ref:`In a webbrowser <erecord_use_capture_008>`

    -> *Example* : :ref:`In a webbrowser <erecord_use_capture_009>`

    -> *Example* : :ref:`In a webbrowser <erecord_use_capture_011>`

    .. include:: include/wwdm_editmodif_quickoverview_tree.rst

More
====

*Back to :* :ref:`wwdm_edit` | :ref:`erecord_examples`

