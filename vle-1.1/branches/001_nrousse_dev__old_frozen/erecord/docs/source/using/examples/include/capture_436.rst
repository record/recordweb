.. _erecord_use_capture_436:

================================================
Example of :ref:`post_vpz_input` in PHP language
================================================

    .. include:: wwdm_php_intro.rst

Example illustrating :

  - '**compact**' as style of presentation,
  - '**json**' as format,

  - value '**all**' as 'parselect'
    (to receive information of all the existing parameters and conditions)

  - value '**all**' as 'outselect'
    (to receive information of all the existing output datas and views)

  - with 'application/**json**' as 'Content-Type'

    *Memo*

    .. literalinclude:: memo_request_400.rst
    .. literalinclude:: memo_request_436.rst

