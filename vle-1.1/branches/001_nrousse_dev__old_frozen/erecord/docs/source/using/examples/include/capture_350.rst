.. _erecord_use_capture_350:

===============================================
Example of some :ref:`'db' resources <webapi_db>` calls in R language
===============================================

The following resources are directly used in this example or could be used in a similar way :
  - :ref:`get_db_rep`, :ref:`get_db_rep_id`
  - :ref:`get_db_pkg`, :ref:`get_db_pkg_id`
  - :ref:`get_db_vpz`, :ref:`get_db_vpz_id`

Enter the R code/instructions in a R interpreter.

    *Memo*

    .. literalinclude:: memo_request_305_tree.rst
    .. literalinclude:: memo_request_350.rst

