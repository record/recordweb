# R code :

library('RCurl')
library('rjson')

#######################
# request and response
#######################

header = c('Content-Type'='application/json', Accept='application/json')

options(RCurlOptions=list(followlocation=TRUE))

postfields = toJSON(list(
    vpz=266,
    mode="compact",
    parselect="all",
    outselect="all",
    format="json"))

res = postForm(uri="http://erecord.toulouse.inra.fr:8000/vpz/input/",
    .opts=list(postfields=postfields, httpheader=header))

responsedata = fromJSON(res)
#responsedata

##########################################################
# responsedata in case of 'compact' style of presentation 
##########################################################

# duration in 'compact' style of presentation
duration_value = responsedata$duration
cat("duration value :", duration_value, "\n")

# begin in 'compact' style of presentation
begin_value = responsedata$begin
cat("begin value :", begin_value, "\n")

# parameters in 'compact' style of presentation 
pars = responsedata$pars
cat("Parameters (selection_name, condition name and parameter name, value) :", "\n")
for (par in pars){
    #par.keys()
    par_selection_name = par$selection_name
    par_cname = par$cname
    par_pname = par$pname
    par_value = par$value
    cat ("- Parameter ", par_selection_name, ",", par_cname, par_pname, "\n")
    print(par_value)
}

# conditions identity in 'compact' style of presentation
conds = responsedata$conds
for (cond in conds){
    cond_selection_name = cond$selection_name
    cat("Condition selection_name : ", cond_selection_name, "\n")
}

# views identity in 'compact' style of presentation
views = responsedata$views
for (view in views){
    view_selection_name = view$selection_name
    cat("View selection_name : ", view_selection_name, "\n")
}

# output datas identity in 'compact' style of presentation
outs = responsedata$outs
for (out in outs){
    out_selection_name = out$selection_name
    cat("Output data selection_name : ", out_selection_name, "\n")
}

