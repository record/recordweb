# Python code :

import json
from __future__ import print_function # python 2.7 version case

# content of simulation input information in 'tree' style of presentation
# (vpzinput)
def content_simulation_inputs_tree(vpzinput):
    #
    keys = vpzinput.keys()
    #
    # duration
    if "vleduration" in keys :
        vleduration = vpzinput['vleduration']
        duration_value = vleduration['value']
        #duration_verbose_name = vleduration['verbose_name']
        #duration_id = vleduration['id']
        #vleduration
        print("duration value : ", duration_value)
    #
    # begin
    if "vlebegin" in keys :
        vlebegin = vpzinput['vlebegin']
        begin_value = vlebegin['value']
        #begin_verbose_name = vlebegin['verbose_name']
        #begin_id = vlebegin['id']
        #vlebegin
        print("begin value : ", begin_value)
    #    
    # conditions and parameters
    if "vlecond_list" in keys :
        conds = vpzinput['vlecond_list']
        for cond in conds :
            #cond.keys()
            cond_verbose_name = cond['verbose_name']
            cond_id = cond['id']
            cond_name = cond['name']
            print("\nCondition name ", cond_name)
            #print(cond_id, cond_name, cond_verbose_name)
            print("List of its parameters (id, cname, pname, type, value, selected, verbose_name) :")
            pars = cond['vlepar_list']
            for par in pars :
                #par.keys()
                par_verbose_name = par['verbose_name']
                par_id = par['id']
                par_cname = par['cname']
                par_pname = par['pname']
                par_type = par['type']
                par_value = json.loads(par['value'])
                par_selected = par['selected']
                print("- ", par_id, par_cname, par_pname, par_type, par_value, par_selected, par_verbose_name)
                #type(par_value)
    #
    # views and output datas identity
    if "vleview_list" in keys :
        views = vpzinput['vleview_list']
        for view in views :
            #view.keys()
            view_verbose_name = view['verbose_name']
            view_id = view['id']
            view_name = view['name']
            view_type = view['type']
            view_timestep = view['timestep']
            view_output_format = view['output_format']
            view_output_location = view['output_location']
            view_output_name = view['output_name']
            view_output_plugin = view['output_plugin']
            print("\nView name ", view_name)
            #print(view_id, view_name, view_type, view_timestep, view_output_format, view_output_location, view_output_name, view_output_plugin, view_verbose_name)
            print("List of its output datas (id, vname, oname, shortname, selected, verbose_name) :")
            outs = view['vleout_list']
            for out in outs :
                #out.keys()
                out_verbose_name = out['verbose_name']
                out_id = out['id']
                out_vname = out['vname']
                out_oname = out['oname']
                out_shortname = out['shortname']
                out_selected = out['selected']
                print("- ", out_id, out_vname, out_oname, out_shortname, out_selected, out_verbose_name)
    #
    pass
