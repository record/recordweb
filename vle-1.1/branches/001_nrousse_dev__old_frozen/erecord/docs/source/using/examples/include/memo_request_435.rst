# PHP code :

# content of simulation results (res) in 'compact' style of presentation,
# according to plan values ('single', 'linear') and
# restype values ('dataframe' or 'matrix')
function content_simulation_results_compact($res, $plan, $restype){
    #
    print ("plan, restype :".$plan.' '.$restype."<br />");
    print ("res :".$res."<br />");
    print ("\nDetailing the results :"."<br />");
    $res = json_decode($res, TRUE);

    if ($restype=='dataframe' and $plan=='single'){
        print ("(Output data name (ie selection_name) and value)"."<br />");
        foreach ($res as $compact_outputname=>$val){
            print ("- ".$compact_outputname.' '.$val."<br />");
        }
    }else if ($restype=='dataframe' and $plan=='linear'){
        foreach ($res as $a => $res_a){
            print ("*** simulation number ".' '.$a.":"."<br />");
            print ("(Output data name (ie selection_name) and value)"."<br />");
            foreach ($res_a as $compact_outputname=>$val){
                $val = json_encode($val);
                print_r ("- ".$compact_outputname.' '.$val."<br />");
            }
        }
    }else if ($restype=='matrix' and $plan=='single'){
        print ("(View name (ie selection_name) and value)"."<br />");
        foreach ($res as $viewname=>$v){
            print ("- ".$viewname.' '.$v."<br />");
        }
    }else if ($restype=='matrix' and $plan=='linear'){
        foreach ($res as $a => $res_a){
            print ("*** simulation number ".$a." :"."<br />");
            print ("(View name (ie selection_name) and value)"."<br />");
            foreach ($res_a as $viewname=>$v){
                $v = json_encode($v);
                print ("- ".$viewname.' '.$v."<br />");
            }
        }
    }else{ # error (unexpected)
        ;
    }
}

#######################
# request and response
#######################

$inputdata = ["vpz"=>266, "format"=>"json"];
$inputdata["duration"] = 6;
$inputdata["begin"] = 2453982.0;

#----------------------
# parameters and mode in case of single plan (single simulation)

# some parameters modification with 'cname.pname'
$inputdata["cond_wwdm.A"] = 0.0064;
$inputdata["cond_wwdm.Eb"] = 1.86;

$inputdata["mode"] = ["compact", "single", "dataframe"];
# or $inputdata["mode"] = ["compact", "single", "matrix"];

#----------------------
# parameters and mode in case of linear plan (multiple simulation)

# some parameters modification with 'cname.pname' (multiple values)
$inputdata["cond_wwdm.A"] = [0.0064,0.0065,0.0066];
$inputdata["cond_wwdm.Eb"] = [1.84,1.85,1.86];

$inputdata["mode"] = ["compact", "linear", "dataframe"];
# or $inputdata["mode"] = ["compact", "linear", "matrix"];

#----------------------

$inputdata["outselect"] = ["view.top:wwdm.LAI", "view.top:wwdm.ST"];

$responsedata = send_post_and_receive($url="http://erecord.toulouse.inra.fr:8000/vpz/output/", $inputdata=$inputdata);

##########################################################
# responsedata in case of 'compact' style of presentation
##########################################################

if (array_key_exists('res', $responsedata) && array_key_exists('plan', $responsedata) && array_key_exists('restype', $responsedata)){
    content_simulation_results_compact($res=$responsedata['res'],$plan=$responsedata['plan'],$restype=$responsedata['restype']);
}

