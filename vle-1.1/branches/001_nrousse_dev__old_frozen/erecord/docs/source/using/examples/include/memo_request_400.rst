# PHP code :

# send POST request and return response datas
function send_post_and_receive($url, $inputdata) {
    #
    ob_start();
    $c = curl_init();
    #
    curl_setopt($c, CURLOPT_POST, TRUE);
    curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    #
    curl_setopt($c, CURLOPT_URL, $url);
    #
    # to follow redirect
    curl_setopt($c, CURLOPT_FOLLOWLOCATION, True);
    #
    $json_inputdata = json_encode($inputdata);
    curl_setopt($c, CURLOPT_POSTFIELDS, $json_inputdata);
    #
    curl_exec($c);
    $buffer_str = ob_get_contents();
    ob_end_clean();
    #
    $responsedata = json_decode($buffer_str, TRUE);
    curl_close($c);
    
    return $responsedata;
}

