.. _erecord_examples:

=================
Use case examples
=================

Introduction
============

Here are some examples that aim to show how to call the :term:`erecord` web
services.

The web services provided by :term:`erecord` allow to edit, modify and simulate
some :term:`agronomic model` s of the :term:`Record` platform, and more
generally some :term:`vle model` s.

There are different ways how to call the :term:`erecord` web services : from
software programs, cURL command line tool, a webbrowser... (for more see
:ref:`erecord_callways`). The illustrations bellow sometimes uses one of these
ways and sometimes another one.

The following examples can be read as a User Guide, replacing the considered
model by the agronomic model you are interested in.

A quick overview example
========================

  - **Seeing a modified simulator** :

        .. include:: include/wwdm_editmodif_quickoverview_tree.rst

    *or lighter* :

        .. include:: include/wwdm_editmodif_quickoverview.rst

  - **Running a modified simulator** :

        .. include:: include/wwdm_runmodif_quickoverview_tree.rst

    *or lighter* :

        .. include:: include/wwdm_runmodif_quickoverview.rst

Example based on the WWDM model
===============================

.. toctree::
   :maxdepth: 1

   Presentation of WWDM, the model of the example <wwdm_model>

   Identifying the simulator from erecord point of vue <wwdm_ident>

   Seeing the simulator <wwdm_edit>
   
   Running the simulator (including multiple simulation case) <wwdm_run>

   Both seeing and running the simulator <wwdm_both>

*See also*

.. toctree::
   :maxdepth: 1

   Help on how to select some information in the request <wwdm_ident_fields>

Sorted by language
==================

  - Examples in a webbrowser :

    - :ref:`erecord_use_capture_001`
    - :ref:`erecord_use_capture_002`
    - :ref:`erecord_use_capture_003`
    - :ref:`erecord_use_capture_004`
    - :ref:`erecord_use_capture_006`
    - :ref:`erecord_use_capture_007`
    - :ref:`erecord_use_capture_008`
    - :ref:`erecord_use_capture_009`
    - :ref:`erecord_use_capture_011`
    - :ref:`erecord_use_capture_012`
    - :ref:`erecord_use_capture_020`
    - :ref:`erecord_use_capture_021`
    - :ref:`erecord_use_capture_022`
    - :ref:`erecord_use_capture_023`
    - :ref:`erecord_use_capture_024`
    - :ref:`erecord_use_capture_025`
    - :ref:`erecord_use_capture_026`
    - :ref:`erecord_use_capture_030`
    - :ref:`erecord_use_capture_031`
    - :ref:`erecord_use_capture_032`
    - :ref:`erecord_use_capture_034`
    - :ref:`erecord_use_capture_035`
    - :ref:`erecord_use_capture_036`

  - Examples in command line with cURL :

    - :ref:`erecord_use_capture_130`
    - :ref:`erecord_use_capture_131`
    - :ref:`erecord_use_capture_132`
    - :ref:`erecord_use_capture_134`
    - :ref:`erecord_use_capture_135`
    - :ref:`erecord_use_capture_136`

  - Examples in Python language :

    - :ref:`erecord_use_capture_230`
    - :ref:`erecord_use_capture_231`
    - :ref:`erecord_use_capture_232`
    - :ref:`erecord_use_capture_234`
    - :ref:`erecord_use_capture_235`
    - :ref:`erecord_use_capture_236`

  - Examples in R language :

    - :ref:`erecord_use_capture_330`
    - :ref:`erecord_use_capture_331`
    - :ref:`erecord_use_capture_332`
    - :ref:`erecord_use_capture_334`
    - :ref:`erecord_use_capture_335`
    - :ref:`erecord_use_capture_336`

  - Examples in PHP language :

    - :ref:`erecord_use_capture_430`
    - :ref:`erecord_use_capture_431`
    - :ref:`erecord_use_capture_432`
    - :ref:`erecord_use_capture_434`
    - :ref:`erecord_use_capture_435`
    - :ref:`erecord_use_capture_436`

  - Examples in C++ language :

    - :ref:`erecord_use_capture_530`
    - :ref:`erecord_use_capture_531`
    - :ref:`erecord_use_capture_532`
    - :ref:`erecord_use_capture_534`
    - :ref:`erecord_use_capture_535`
    - :ref:`erecord_use_capture_536`

More
====

.. include:: ../../include/more.rst

