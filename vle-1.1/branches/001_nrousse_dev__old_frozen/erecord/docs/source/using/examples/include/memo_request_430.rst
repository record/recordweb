# PHP code :

#######################
# request and response
#######################

$inputdata = ["vpz"=>266, "format"=>"json"];
$inputdata["duration"] = 6;
$inputdata["begin"] = 2453982.0;

$inputdata["mode"] = ["tree", "single", "dataframe"];

# some parameters modification with 'cname.pname'
$inputdata["cond_wwdm.A"] = 0.0064;
$inputdata["cond_wwdm.Eb"] = 1.86;

$inputdata["parselect"] = ["cond_wwdm.A", "cond_wwdm.B", "cond_wwdm.Eb", "cond_wwdm.TI"];

$inputdata["outselect"] = ["view.top:wwdm.LAI", "view.top:wwdm.ST"];

$responsedata = send_post_and_receive($url="http://erecord.toulouse.inra.fr:8000/vpz/inout/", $inputdata=$inputdata);

#######################################################
# responsedata in case of 'tree' style of presentation
#######################################################

# id as VpzAct
if (array_key_exists('id', $responsedata)){
    $id = $responsedata['id'];
}

if (array_key_exists('verbose_name', $responsedata)){
    $verbose_name = $responsedata['verbose_name'];
}

#----------------------
# input information of simulation
if (array_key_exists('vpzinput', $responsedata)){
    $vpzinput = $responsedata['vpzinput'];
    #
    # id as VpzInput
    if (array_key_exists('id', $vpzinput)){
        $id = $vpzinput['id'];
    }
    #
    content_simulation_inputs_tree($vpzinput=$vpzinput);

}

#----------------------
# output information of simulation
if (array_key_exists('vpzoutput', $responsedata)){
    $vpzoutput = $responsedata['vpzoutput'];
    #
    # id as VpzOutput
    if (array_key_exists('id', $vpzoutput)){
        $id = $vpzoutput['id'];
    }
    #
    if (array_key_exists('res', $vpzoutput) && array_key_exists('plan', $vpzoutput) && array_key_exists('restype', $vpzoutput)){
        content_simulation_results_tree($res=$vpzoutput['res'], $plan=$vpzoutput['plan'], $restype=$vpzoutput['restype']);
    }
}

#----------------------
# others
if (array_key_exists('vpzorigin', $responsedata)){
    $vpzorigin = $responsedata['vpzorigin'];
}

if (array_key_exists('pkgname', $responsedata)){
    $pkgname = $responsedata['pkgname'];
}

if (array_key_exists('vlepath', $responsedata)){
    $vlepath = $responsedata['vlepath'];
}

if (array_key_exists('vpzname', $responsedata)){
    $vpzname = $responsedata['vpzname'];
}

if (array_key_exists('vpzworkspace', $responsedata)){
    $vpzworkspace = $responsedata['vpzworkspace'];
}

