.. _erecord_use_capture_136:

==========================================================
Example of :ref:`post_vpz_input` in command line with cURL
==========================================================

    .. include:: wwdm_curl_intro.rst

Example illustrating :

  - '**compact**' as style of presentation,
  
  - value '**all**' as '**parselect**'
    (to receive information of all the existing parameters and conditions)
  
  - value '**all**' as '**outselect**'
    (to receive information of all the existing output datas and views)

  - with 'application/**json**' as 'Content-Type'

:download:`REQUEST and RESPONSE<capture_request_136.png>`

    *Memo*

    .. literalinclude:: memo_request_136.rst

