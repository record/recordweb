.. _erecord_use_capture_235:

====================================================
Example of :ref:`post_vpz_output` in Python language
====================================================

    .. include:: wwdm_python_intro.rst

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - '**compact**' as style of presentation,
  - '**json**' as format,

  - value '**single**' as plan, or
    value '**linear**' in case of  multiple simulation
  - value '**dataframe**' as restype *(or 'matrix')*

  - value of type *vname.oname* as '**outselect**' :

    value 'view.top:wwdm.LAI' to select the 'LAI' output data of the view
    named 'view'.

    value 'view.top:wwdm.ST' to select the 'ST' output data of the view
    named 'view'.

  - with 'application/**json**' as 'Content-Type'

    *Memo*

    .. literalinclude:: memo_request_200.rst
    .. literalinclude:: memo_request_201_compact.rst
    .. literalinclude:: memo_request_235.rst

