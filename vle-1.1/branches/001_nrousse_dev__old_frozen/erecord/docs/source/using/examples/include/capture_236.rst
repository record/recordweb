.. _erecord_use_capture_236:

===================================================
Example of :ref:`post_vpz_input` in Python language
===================================================

    .. include:: wwdm_python_intro.rst

Example illustrating :

  - '**compact**' as style of presentation,
  - '**json**' as format,

  - value '**all**' as 'parselect'
    (to receive information of all the existing parameters and conditions)

  - value '**all**' as 'outselect'
    (to receive information of all the existing output datas and views)

  - with 'application/**json**' as 'Content-Type'

    *Memo*

    .. literalinclude:: memo_request_200.rst
    .. literalinclude:: memo_request_236.rst

