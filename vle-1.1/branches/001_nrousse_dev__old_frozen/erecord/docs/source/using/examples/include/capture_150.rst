.. _erecord_use_capture_150:

===========================================================
Example of some :ref:`'db' resources <webapi_db>` calls in command line with cURL
===========================================================

The following resources are used in this example :
  - :ref:`get_db_rep`, :ref:`get_db_rep_id`
  - :ref:`get_db_pkg`, :ref:`get_db_pkg_id`
  - :ref:`get_db_vpz`, :ref:`get_db_vpz_id`

Enter the cURL commands in a command line window.

    *Memo*

    .. literalinclude:: memo_request_150.rst

