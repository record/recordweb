.. _erecord_use_capture_232:

====================================================
Example of :ref:`post_vpz_output` in Python language
====================================================

    .. include:: wwdm_python_intro.rst

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters with '**pars**',

  - '**tree**' as style of presentation,
  - '**json**' as format,

  - value '**single**' as plan, or
    value '**linear**' in case of  multiple simulation
  - value '**dataframe**' as restype *(or 'matrix')*

  - value '**all**' as '**outselect**',

  - with 'application/**json**' as 'Content-Type'

    *Memo*

    .. literalinclude:: memo_request_200.rst
    .. literalinclude:: memo_request_201_tree.rst
    .. literalinclude:: memo_request_232.rst

