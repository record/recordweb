# PHP code :

# send GET request and return response datas
function send_get_and_receive($url, $options=NULL, $id=NULL){
    # full_url composed by : url {id} /? options
    $full_url = $url;
    if ($id != NULL){
        $full_url = $full_url.$id;
    }
    if ($options != NULL){
        $full_url = $full_url."?".http_build_query($options);
    }
    print ("<br />request GET ".$full_url."<br />");
    #
    #
    ob_start();
    $c = curl_init();
    set_time_limit(120);
    curl_setopt($c, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    #
    curl_setopt($c, CURLOPT_URL, $full_url);
    # to follow redirect
    curl_setopt($c, CURLOPT_FOLLOWLOCATION, True);
    #
    curl_exec($c);
    $buffer_str = ob_get_contents();
    ob_end_clean();
    #
    $responsedata = json_decode($buffer_str, TRUE);
    curl_close($c);
    #
    return $responsedata;
}

