* 'wwdm' vle package into the erecord databases :

    ...
    {
        "id": 41, 
        "verbose_name": "Winter Wheat Dry Matter (from 'recordb')", 
        "name": "wwdm", 
        "vlerep": {
            "id": 2, 
            "name": "recordb", 
            "verbose_name": "recordb (models)", 
            "path": "/opt/erecord/repositories/recordb"
        }, 
        "vlevpz_list": [
            {
                "id": 266, 
                "name": "wwdm.vpz", 
                "verbose_name": "'wwdm.vpz' (from 'wwdm' (from 'recordb'))", 
                "vlepkg": {
                    "id": 41, 
                    "name": "wwdm", 
                    "verbose_name": "Winter Wheat Dry Matter (from 'recordb')", 
                    "vlerep": {
                        "id": 2, 
                        "name": "recordb", 
                        "verbose_name": "recordb (models)", 
                        "path": "/opt/erecord/repositories/recordb"
                    }
                }
            }
        ]
    }, 
    ...

