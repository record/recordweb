# R code :

library('rjson')

# content of simulation input information in 'tree' style of presentation
# (vpzinput)
content_simulation_inputs_tree <- function(vpzinput){

    # duration
    vleduration = vpzinput$vleduration
    #vleduration
    duration_value = vleduration$value
    duration_verbose_name = vleduration$verbose_name
    duration_id = vleduration$id
    cat("duration value :", duration_value, "\n")

    # begin
    vlebegin = vpzinput$vlebegin
    vlebegin
    begin_value = vlebegin$value
    begin_verbose_name = vlebegin$verbose_name
    begin_id = vlebegin$id
    cat("begin value :", begin_value, "\n")

    # conditions and parameters
    conds = vpzinput$vlecond_list
    for (cond in conds){
        #print(cond)
        cond_verbose_name = cond$verbose_name
        cond_id = cond$id
        cond_name = cond$name
        cat("\nCondition (id, name, verbose_name) : ", cond_id, cond_name, cond_verbose_name, "\n")
        cat("List of its parameters (id, cname, pname, type, selected, verbose_name, value) :", "\n")
        pars = cond$vlepar_list
        for (par in pars){
            #print(par)
            par_verbose_name = par$verbose_name
            par_id = par$id
            par_cname = par$cname
            par_pname = par$pname
            par_type = par$type
            par_value = fromJSON(par$value)
            par_selected = par$selected
            cat("- ", par_id, par_cname, par_pname, par_type, par_selected, par_verbose_name, "\n")
            print(par_value)
        }
    }

    # views and output datas identity
    views = vpzinput$vleview_list
    for (view in views){
        #print(view)
        view_verbose_name = view$verbose_name
        view_id = view$id
        view_name = view$name
        view_type = view$type
        view_timestep = view$timestep
        view_output_format = view$output_format
        view_output_location = view$output_location
        view_output_name = view$output_name
        view_output_plugin = view$output_plugin
        cat("\nView (id, name, type, timestep, output_format, output_location, output_name, output_plugin, verbose_name) : ", view_id, view_name, view_type, view_timestep, view_output_format, view_output_location, view_output_name, view_output_plugin, view_verbose_name, "\n")
        cat("List of its output datas (id, vname, oname, shortname, selected, verbose_name) :", "\n")
        outs = view$vleout_list
        for (out in outs){
            #print(out)
            out_verbose_name = out$verbose_name
            out_id = out$id
            out_vname = out$vname
            out_oname = out$oname
            out_shortname = out$shortname
            out_selected = out$selected
            cat("- ", out_id, out_vname, out_oname, out_shortname, out_selected, out_verbose_name, "\n")
        }
    }
}

