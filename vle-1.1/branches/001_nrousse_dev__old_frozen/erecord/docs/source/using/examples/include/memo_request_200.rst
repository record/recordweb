# Python code :

import pycurl
import io
import json

# send POST request and return response datas
def send_post_and_receive(url, inputdata):
    #
    buffer = io.BytesIO()
    c = pycurl.Curl()
    #
    c.setopt(c.POST, 1)
    c.setopt(c.HTTPHEADER, ['Content-Type: application/json'])
    #
    c.setopt(c.URL, url)
    #
    # to follow redirect
    c.setopt(c.FOLLOWLOCATION, True)
    #
    json_inputdata = json.dumps( inputdata)
    c.setopt(c.POSTFIELDS, json_inputdata)
    #
    c.setopt(c.WRITEFUNCTION, buffer.write)
    #
    c.perform()
    buffer_str = buffer.getvalue()
    buffer.close()
    buffer_str = buffer_str.decode("utf8")
    #
    responsedata = json.loads(buffer_str)
    #responsedata
    #responsedata.keys()
    return responsedata

