# R code :

library('RCurl')
library('rjson')

#######################
# request and response
#######################

header = c('Content-Type'='application/json', Accept='application/json')

options(RCurlOptions=list(followlocation=TRUE))

postfields = toJSON(list(
    vpz=266,
    duration=6,
    begin=2453982.0,
    cond_wwdm.A=0.0064,
    cond_wwdm.Eb=1.86,
    mode=c("tree", "single", "dataframe"),
    parselect=c("cond_wwdm.A","cond_wwdm.B","cond_wwdm.Eb","cond_wwdm.TI"),
    outselect=c("view.top:wwdm.LAI", "view.top:wwdm.ST"),
    format="json"))

res = postForm(uri="http://erecord.toulouse.inra.fr:8000/vpz/inout/",
    .opts=list(postfields=postfields, httpheader=header))

responsedata = fromJSON(res)
#responsedata

#######################################################
# responsedata in case of 'tree' style of presentation 
#######################################################

# id as VpzAct
id = responsedata$id
cat("id :", id, "\n")

verbose_name = responsedata$verbose_name

#----------------------
# input information of simulation

vpzinput = responsedata$vpzinput

# id as VpzInput
id = vpzinput$id

content_simulation_inputs_tree(vpzinput=vpzinput)

#----------------------
# output information of simulation

vpzoutput = responsedata$vpzoutput

# id as VpzOutput
id = vpzoutput$id

content_simulation_results_tree( res=vpzoutput$res,
                                 plan=vpzoutput$plan,
                                 restype=vpzoutput$restype)


#----------------------
# others

vpzorigin = responsedata$vpzorigin

pkgname = responsedata$pkgname

vlepath = responsedata$vlepath

vpzname = responsedata$vpzname

vpzworkspace = responsedata$vpzworkspace

