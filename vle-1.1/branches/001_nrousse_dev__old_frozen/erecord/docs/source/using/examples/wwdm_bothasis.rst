
.. _wwdm_bothasis:

======================================
Seeing and running the simulator as is
======================================

Seeing and running the simulator as is corresponds with the
:ref:`get_vpz_inout` resource.

    ... under construction ...

More
====

*Back to :* :ref:`wwdm_both` | :ref:`erecord_examples`

