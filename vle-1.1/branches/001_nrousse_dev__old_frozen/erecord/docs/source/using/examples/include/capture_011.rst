.. _erecord_use_capture_011:

================================================
Example of :ref:`post_vpz_input` in a webbrowser
================================================

    .. include:: wwdm_wb_intro_postinput.rst

Example illustrating :

  - modifying some parameters by '**cname.pname**',
  - '**compactlist**' as style of presentation
  - values of type *cname.pname* as '**parselect**' :

    value 'cond_wwdm.A' to select the parameter named 'A' of the condition
    named 'cond_wwdm'.

    value 'cond_wwdm.B' to select the parameter named 'B' of the condition
    named 'cond_wwdm'.

    value 'cond_wwdm.Eb' to select the parameter named 'Eb' of the condition
    named 'cond_wwdm'.

    value 'cond_wwdm.TI' to select the parameter named 'TI' of the condition
    named 'cond_wwdm'.

  - with 'application/**json**' as 'Media type'

:download:`REQUEST <capture_request_011.png>`

    *Memo*

    .. literalinclude:: memo_request_011.rst






