.. _wwdm_bothmodif:

=========================================
Seeing and running the modified simulator
=========================================

Seeing and running the modified simulator corresponds with the
:ref:`post_vpz_inout` resource.

You may find here : :ref:`help to know how some information is identified/selected in the request <wwdm_ident_fields>`.

The different possible modifications are illustrated bellow.

Modifications
=============

You can apply the modifications available for :ref:`wwdm_runmodif` :

  - Choosing the **simulation running mode**
  - Modifying **begin** and **duration** of simulation
  - Modifying some **parameters**
  - Choosing the **restituted output datas**
  - Choosing **style of presentation**

You can apply all the modifications available for :ref:`wwdm_editmodif` :

  - Modifying begin and duration of simulation
  - Modifying some parameters
  - Choosing the **restituted parameters**
  - Choosing style of presentation (excepted that the 'compact' and
    'compactlist' values are not available ; the available values are 'link'
    and 'tree').

Examples
========

    .. include:: include/wwdm_bothmodif_quickoverview_tree.rst

Other examples
--------------

    Using the right URL (http://erecord.toulouse.inra.fr:8000/vpz/inout),
    you can combinate :

      - the examples of :ref:`wwdm_editmodif` and 
      - the examples of :ref:`wwdm_runmodif`

More
====

*Back to :* :ref:`wwdm_both` | :ref:`erecord_examples`

