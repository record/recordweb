.. _erecord_callways:

=================================
Ways how to call the web services
=================================

Overview
========

As RESTful web services, the :term:`erecord` web services are based on the HTTP protocol (a call takes the form of an HTTP request). As a consequence, there are different ways how to call the :term:`erecord` web services : they can be called from 'anything' that can send an HTTP request.

The :term:`erecord` web services can be called :

- from **software programs** written **in different languages** (any language supporting the HTTP protocol, such as for example Python, R, C++, Java, Php...).
- from **command line tools** such as for example **cURL** that is  a command-line tool for transferring data using various protocols among which the HTTP one. 
- from a **webbrowser**

See also the :ref:`webui`.

Calling the web services from a Python program
==============================================

    See examples in :ref:`erecord_examples`, for example
    :ref:`this one <erecord_use_capture_232>`.

Calling the web services from a R program
=========================================

    See examples in :ref:`erecord_examples`, for example
    :ref:`this one <erecord_use_capture_332>`.

Calling the web services from a PHP program
===========================================

    See examples in :ref:`erecord_examples`, for example
    :ref:`this one <erecord_use_capture_432>`.

Calling the web services from a C++ program
===========================================

    :ref:`soon <tmp_under_construction>`

Using the web services by cURL command line tool
================================================

    See examples in :ref:`erecord_examples`, for example
    :ref:`this one <erecord_use_capture_132>`.

Using the web services by webbrowser
====================================

    See examples in :ref:`erecord_examples`, for example
    :ref:`this one <erecord_use_capture_032>`.

