.. _webapi:

=======
Web API
=======

Introduction
============

The web services provided by :term:`erecord` allow to edit, modify and simulate
some :term:`agronomic model` s of the :term:`Record` platform, and more
generally some :term:`vle model` s.

The **'vpz'** web services allow to **modify and run simulators**.

To act on a simulator, it is necessary to know its Id. This one can be found
thanks to the **'db'** web services, that allow to **access the models
repositories, their models and simulators**.

Resources
=========

- The :ref:`'vpz' web services <webapi_vpz>` to **modify and run simulators** :

========================= ===================================== ===========
Resource                  Description 
========================= ===================================== ===========
:ref:`get_vpz_input`      to see a simulator
------------------------- ------------------------------------- -----------
:ref:`post_vpz_input`     to see a modified simulator            :ref:`Try <online_url_input_menu>`
------------------------- ------------------------------------- -----------
:ref:`get_vpz_output`     to run a simulator
------------------------- ------------------------------------- -----------
:ref:`post_vpz_output`    to run a modified simulator            :ref:`Try <online_url_output_menu>`
------------------------- ------------------------------------- -----------
:ref:`get_vpz_inout`      to see and run a simulator
------------------------- ------------------------------------- -----------
:ref:`post_vpz_inout`     to see and run a modified simulator    :ref:`Try <online_url_inout_menu>`
========================= ===================================== ===========


- The :ref:`'db' web services <webapi_db>` to access the **models
  repositories, their models and simulators** :

========================= ==================================================
Resource                  Description 
========================= ==================================================
:ref:`get_db_rep`         *to identify some models repositories*
------------------------- --------------------------------------------------
:ref:`get_db_rep_id`      *to identify a models repository*
------------------------- --------------------------------------------------
:ref:`get_db_pkg`         *to identify some models*
------------------------- --------------------------------------------------
:ref:`get_db_pkg_id`      *to identify a model*
------------------------- --------------------------------------------------
:ref:`get_db_vpz`         *to identify some simulators*
------------------------- --------------------------------------------------
:ref:`get_db_vpz_id`      *to identify a simulator*
========================= ==================================================

Others :

============================== =============================================
Resource                       Description 
============================== =============================================
:ref:`get_db_pkg_id_datalist`  to get the list of names of data files of a model
------------------------------ ---------------------------------------------
:ref:`get_db_rep_id_name`      to get the name of a models repository
------------------------------ ---------------------------------------------
:ref:`get_db_pkg_id_name`      to get the name of a model
------------------------------ ---------------------------------------------
:ref:`get_db_vpz_id_name`      to get the name of a simulator
============================== =============================================

More
====

.. include:: ../include/more.rst

