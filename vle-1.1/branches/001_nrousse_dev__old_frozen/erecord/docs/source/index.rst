.. _index:

.. _erecord_home:

=======
erecord
=======

The erecord project is dedicated to the :term:`Record` platform web 
development, concerning more specifically its models developed with the
vle-1.1 version of :term:`Vle`. The web services provided by erecord allow
to edit, modify and simulate some vle models, such as the Record platform
ones.

Quick overview
==============

:ref:`Access to erecord web services <online_url>` |
:ref:`webui` |
:ref:`webapi` |
:ref:`An example as a tutorial <erecord_examples>`

General presentation
====================

.. toctree::
   :maxdepth: 1

   Domain area <domain/index>
   Models repositories <devel/erecord/repositories/index>
   Databases <devel/erecord/databases/index>
   The software content <devel/erecord/index>

The erecord web services are online :
:ref:`Access to the erecord web services <online_url>`.

Project
=======

.. toctree::
   :maxdepth: 2

   The erecord software project <project/index>

Users
=====

.. toctree::
   :maxdepth: 1

   Web API <webapi/index>

.. toctree::
   :maxdepth: 2

   using/index

.. toctree::
   :maxdepth: 1

   Web User Interface <webui/index>

Developers
==========

.. toctree::
   :maxdepth: 1

   Deployment <devel/deployment/index>
   Documentation <devel/documentation/index>
   Test <devel/test/index>
    
   Source code API reference <ref/index>

   The development process in details <devel/index>

How to
======

    * :ref:`How to call/use the web services <webapi>`

    * :ref:`How to install/deploy the erecord software <devel_deployment>`

    * :ref:`How to install/declare a model <repositories>`

    * :ref:`How to build documentation <devel_documentation>`

    * :ref:`Testing the erecord software <devel_test>`

FAQs
====

.. toctree::
   :maxdepth: 1

   faqs/index

More
====

.. include:: include/more.rst

