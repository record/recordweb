.. _domain_dm_erecord_db:

====================================
Data model of erecord_db application
====================================

Data model of the erecord_db application

Schema
======

    - :ref:`dm_vlerep`
        - many :ref:`dm_vlepkg`
            - many :ref:`dm_vlevpz`

Data model
==========

.. _dm_vlepkg:

VlePkg
------
Vle package.

Other denominations : package (vle notion) ; project (vle notion) ;
:term:`vle model` ; model ; agronomic model ; vle package ; vle project.

It corresponds with the vle notion of package, or project.

A VlePkg is attached to a VleRep providing the physical required environnement.

.. _dm_vlerep:

VleRep
------
Models repository.

A VleRep corresponds with a physical :term:`models repository` providing the required environment for running simulations.

.. _dm_vlevpz:

VleVpz
------
Vpz file.

Other denominations : vpz (vle notion) ; vpz file (vle notion) ;
:term:`simulator` ; simulation scenario.

It corresponds with the vle notion of vpz, or vpz file.

A VleVpz is attached to a VlePkg that is attached to a VleRep providing the physical required environnement.

