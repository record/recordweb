.. _domain_intro:

===================
Domain introduction
===================

:term:`Record` is a simulation platform for agro-ecosystems developed at 
:term:`INRA`. The Record platform is based on :term:`Vle` that is a
multi-modeling and simulation platform. And so the agronomic models of the
Record platform are some vle models.

The erecord project is dedicated to the Record platform web development, 
concerning more specifically its models developed with the vle-1.1 version of
Vle. The web services provided by erecord allow to edit, modify and simulate
some vle models, such as the Record platform ones.

The objects of the domain described by the erecord project call on concepts
from Vle *(package, vpz...)*. But they can as well be given other names,
depending on the adopted point of vue.

For example, some 'equivalent' vocabulary : 

=====================  =====================  =============================
**Record**             **Vle**                **erecord objects**
=====================  =====================  =============================
Agronomic model        package, project       :term:`vle package`, VlePkg
---------------------  ---------------------  -----------------------------
Simulator              vle vpz, vpz file      :term:`vpz (as VleVpz)`
=====================  =====================  =============================

