.. _faqs:

==========================
Frequently Asked Questions
==========================

    *... under construction ...*

Users
=====

How to call/use the web services ?
----------------------------------

    See :ref:`webapi` | :ref:`using`

Restitutions
------------
    *... under construction ...*

    See :ref:`webapi` | :ref:`some examples <erecord_examples>`

Agronomic models
================

Can erecord web services be used for any agronomic model ?

    - The agronomic model has to be an operational model developed with
      **vle-1.1** version of vle.

    - The agronomic model software will have to be previously installed on the
      erecord server, that is to say (i) physically stored into one of the
      models repositories and (ii) recorded into the databases of the erecord
      project. For more information, :ref:`contact us <erecord_contacts>`.

    - The agronomic model software has to follow some rules :

          - The output directory of its main vle package must not be used for
            anything else than storing the simulation results, and those
            simulation running productions must not be written anywhere else
            than into this output directory.
          - The model must not get its input datas anywhere else than into the
            data directory of its main vle package.
          - The simulation shouldn't display anything on screen while running.

Developers
==========

Where can I find help as developer ?
------------------------------------

    - :ref:`How to install/deploy the erecord software <devel_deployment>`
    - :ref:`How to install/declare a model <repositories>`
    - :ref:`How to build documentation <devel_documentation>`
    - :ref:`Testing the erecord software <devel_test>`

