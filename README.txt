
                         **********************
                         *                    *
                         *     production     *
                         *                    *
                         **********************

*******************************************************************************
                                  vle-x

erecord project (from 2018) : the web services provided by erecord allow to
edit, modify and simulate some vle models, such as the Record platform ones.

Features :

  - Version using erecord package instead of pyvle.

  - Version supporting several vle versions (vle-1.1.3, vle-2.0.0...).

Is the version installed under erecord VM (erecord.toulouse.inra.fr).

Is the version under production (development stopped, only maintenance).

Note :

  The erecord online documentation uses/refers to the archive of the old
  webrecord site (no longer online) (see vle-1.0 version), that must be
  copied/installed under erecord VM by that way :

    cp recordweb/vle-1.0/archive/webrecord /var/www/misc/webrecord

*******************************************************************************

                   **********************************
                   *                                *
                   *  latest tests (not finalized)  *
                   *                                *
                   **********************************

*******************************************************************************
                                  erecord

erecord project (from 2019) : the web services provided by erecord allow to
edit, modify and simulate some vle models, such as the Record platform ones.

Features :

  - Version using containers for models repositories.

  - Version under python 3.

Is the version of latest tests, not operational.

*******************************************************************************

                         ***********************
                         *                     *
                         *         old         *
                         *                     *
                         ***********************

*******************************************************************************
                                  vle-1.1

The erecord project (2014-2018) is dedicated to the Record platform web
development, concerning more specifically its models developed with vle-1.1
version. The web services provided by erecord allow to edit, modify and
simulate some vle models, such as the Record platform ones.

Version using pyvle.

*******************************************************************************
                                  vle-1.0

The webrecord project (2011-2014), web solutions for models of the Record
platform : a web application prototype that enables remote simulations of
some existing agronomic models developped with vle-1.0.3 version, that have
been previously loaded into the web tool.

An archive :

  - There exists an archive of the webrecord site (no longer online) dedicated
    to the webrecord project presentation and productions :
    recordweb/vle-1.0/archive/webrecord/webrecord.pdf and
    recordweb/vle-1.0/archive/webrecord

  - This archive is required by the erecord online documentation : see
    vle-x version.

*******************************************************************************

