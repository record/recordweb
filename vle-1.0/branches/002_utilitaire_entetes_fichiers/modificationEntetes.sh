#!/bin/bash

#####################################################
# dans model/*.py
# inserer en 2 entete_py.txt

for nom_repertoire in ../model
do
    for fichier in $nom_repertoire/*.py
    #for fichier in $nom_repertoire/mon_essai_ajeter.py
    do
        rm -f xx00 xx01
        csplit $fichier 2
        cat xx00 entete_py.txt xx01 > $fichier
    done
done

#####################################################
# dans controllers/*.py
# inserer en 2 entete_py.txt

for nom_repertoire in ../controllers
do
    for fichier in $nom_repertoire/*.py
    #for fichier in $nom_repertoire/mon_essai_ajeter.py
    do
        rm -f xx00 xx01
        csplit $fichier 2
        cat xx00 entete_py.txt xx01 > $fichier
    done
done

#####################################################
# dans public/js/*.js
# inserer entete_js_css.txt en debut de fichier

for nom_repertoire in ../public/js
do
    for fichier in $nom_repertoire/*.js
    #for fichier in $nom_repertoire/mon_essai_ajeter.js
    do
        rm -f xx00 xx01
        mv $fichier xx00
        cat entete_js_css.txt xx00 > $fichier
    done
done

#####################################################
# dans public/css/*.css
# inserer entete_js_css.txt en debut de fichier

for nom_repertoire in ../public/css
do
    for fichier in $nom_repertoire/*.css
    #for fichier in $nom_repertoire/mon_essai_ajeter.css
    do
        rm -f xx00 xx01
        mv $fichier xx00
        cat entete_js_css.txt xx00 > $fichier
    done
done

#####################################################
