#-*- coding:utf-8 -*-

# For django settings of projects that use site_web_record applications.

###############################################################################

import os

# 'Verifie' le repertoire rep : les liens symboliques sont acceptes
def verificationLargeRepertoireOk( rep ) :
    verif = os.path.exists(rep) and os.path.isdir(rep)
    return verif

class site_web_record_settings(object) :

    def __init__(self, var_RECORDWEB_HOME,
                       var_RECORDWEB_VENDOR ) :

        #######################################################################
        #
        # IN (from settings) : verification de declarations et existences
        #
        #######################################################################

        # RECORDWEB_HOME, repertoire racine (projets, applications, librairies python)
        RECORDWEB_HOME = var_RECORDWEB_HOME
        # verif
        rep = RECORDWEB_HOME
        if not verificationLargeRepertoireOk( rep ) :
            print "ERREUR !!! site_web_record_settings.py : répertoire ", rep, " N'EXISTE PAS"
        rep = os.path.join( RECORDWEB_HOME, 'record' )
        if not verificationLargeRepertoireOk( rep ) :
            print "ERREUR !!! site_web_record_settings.py : répertoire ", rep, " N'EXISTE PAS"

        # vendors #############################################################

        # repertoire des librairies, applications django externes (django-transmeta...)
        RECORDWEB_VENDOR = var_RECORDWEB_VENDOR
        # verif
        rep = RECORDWEB_VENDOR
        if not verificationLargeRepertoireOk( rep ) :
            print "ERREUR !!! site_web_record_settings.py : répertoire ", rep, " N'EXISTE PAS"

        #######################################################################
        #
        # OUT (to settings) : elements de configuration propres a site_web_record
        #
        #######################################################################

        #######################################################################
        # path de site_web_record (correspond a RECORDWEB_HOME/site_web_record)
        SITE_ROOT = os.path.realpath(os.path.dirname(__file__))

        # TEMPLATE_DIRS #######################################################

        # emplacement des templates
        # propres a projet site_web_record : templates_site_web_record 
        # publics : templates_public
        templates_site_web_record = os.path.join( SITE_ROOT, 'templates' )
        templates_public = os.path.join( RECORDWEB_HOME, 'record', 'templates' )

        self.TEMPLATE_DIRS_FIRST = ( templates_site_web_record,)
        self.TEMPLATE_DIRS_LAST = ( templates_public,)

        # LOCALE_PATHS ########################################################

        # emplacement translation file commun recordweb
        # (en amont de ceux de chaque projet)
        translation_recordweb = os.path.join( RECORDWEB_HOME, 'record', 'locale' )

        # directories where Django looks for translation files
        # (en plus des repertoires 'locale' de projet etc)
        # (du plus au moins prioritaire)
        #
        # Actuellement le propre dictionnaire de site_web_record existe mais inusite
        #
        self.LOCALE_PATHS = ( translation_recordweb,)

        # STATICFILES_DIR #####################################################

        # emplacement des fichiers css et js
        # propres a site_web_record : SITE_MEDIA_SITE_WEB_RECORD
        # publics : SITE_MEDIA_PUBLIC
        SITE_MEDIA_SITE_WEB_RECORD = os.path.join( SITE_ROOT, 'site_media' )
        SITE_MEDIA_PUBLIC = os.path.join( RECORDWEB_HOME, 'record', 'site_media' )

        # Additional locations of static files
        self.STATICFILES_DIRS_FIRST = (
            # STATICFILES_DIRS avec collectstatic (du + au - prioritaire )
            SITE_MEDIA_SITE_WEB_RECORD, # os.path.join( SITE_ROOT, 'site_media' )
        )
        self.STATICFILES_DIRS_LAST = (
            # STATICFILES_DIRS avec collectstatic (du + au - prioritaire )
            SITE_MEDIA_PUBLIC,
        )

###############################################################################

