from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('record.site_web_record.views',

    url(r'^$', 'accueil'),
    url(r'^accueil/$', 'accueil'),
    url(r'^swrec/accueil/$', 'accueil'),

    url(r'^page_info/$', 'page_info'),
    url(r'^swrec/page_info/$', 'page_info'),
)

