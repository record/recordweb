#-*- coding:utf-8 -*-

###############################################################################
# File site_web_record/views.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
# View site_web_record
#
# Site d'information concernant le projet web record : affichage de pages 
# d'information permettant notamment d'acceder a l'outil web record
# (pour utilisation, pour administration si admin)
#
###############################################################################

from django.shortcuts import render_to_response
from django.core.context_processors import csrf

from configs.conf_internationalisation import CONF_internationalisation

#from configs.conf_trace import CONF_trace
#from record.utils.trace import TraceEcran, TraceErreur

# pour traces
#t_ecr = TraceEcran(__file__,__name__,CONF_trace)
#t_err = TraceErreur(__file__,__name__,CONF_trace)

from django.utils.translation import ugettext as _

###############################################################################
#
# Aspects configuration/initialisation
#
###############################################################################

###############################################################################
# NomsPages : des constantes determinant les pages appelees.
###############################################################################
class NomsPages(object) :
    NOM_page_accueil = 'infos/accueil.html'
    CHEMIN_page_info = 'infos/'

###############################################################################
# Modele : ChoixOperations
#
# Des constantes relatives aux choix d'operation proposes a l'utilisateur
#
# Un choix a proposer dans une page html est transmis dans le contexte
# d'appel. Valeur VAL_choix_a_montrer : le choix est propose/affiche.
#
###############################################################################
class ChoixOperations(object) :

    # Valeur donnee a certains choix (transmis a une page) qui doivent etre 
    # montres/visibles
    VAL_choix_a_montrer = 'choix_a_montrer'

###############################################################################
#
# Controllers
#
###############################################################################

###############################################################################
# accueil
#
# Affiche la page d'accueil du site
###############################################################################
def accueil(request):

    # flashes
    flashes = [] 

    c = {} # le contexte

    c['flashes'] = flashes # optionnel

    # menu_langage optionnel
    if CONF_internationalisation.menu_langage_site_web_record :
        c['menu_langage'] = ChoixOperations.VAL_choix_a_montrer
    # else : menu_langage non transmis
    c['choix_a_montrer'] = ChoixOperations.VAL_choix_a_montrer # valeur de comparaison

    c.update(csrf(request))

    NOM_page_accueil = NomsPages.NOM_page_accueil

    return render_to_response( NOM_page_accueil, c )

###############################################################################
# page_info
#
# Determine la page a afficher a partir du parametre page et l'affiche
###############################################################################
def page_info(request):

    page = 'accueil' # par defaut
    if "page" in request.POST.keys() :
        page = request.POST["page"]

    # flashes
    flashes = [] # aucun message d'erreur en flashes

    c = {} # le contexte

    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    NOM_page_info = NomsPages.CHEMIN_page_info + page + '.html'

    return render_to_response( NOM_page_info, c )

###############################################################################

