from django.conf.urls.defaults import *

from django.conf import settings

# to enable the admin:
from django.contrib import admin
admin.autodiscover()

###############################################################################

urlpatterns = patterns('',

    # internationalisation traductions
    (r'^i18n/', include('django.conf.urls.i18n')),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        # static files
        (r'^site_media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT }),
    )


urlpatterns += patterns('',

    ###########################################################################
    #
    # partie administration : administration de la bd des modeles record
    #
    ###########################################################################

    url(r'^rwtool/adm/',   include(admin.site.urls)),
    url(r'^rwtool/admin/', include(admin.site.urls)), # synonyme

    ###########################################################################
    #
    # partie utilisation
    #
    ###########################################################################

    ###########################################################################
    # par ordre chronologique d'appel :
    ###########################################################################

    # 1) accueil
    #url(r'^recordweb/accueil/', include('record.site_web_record.urls')), 
    #url(r'^recordweb/', include('record.site_web_record.urls')), 
    url(r'^accueil/', include('record.site_web_record.urls')), 

    url(r'^', include('record.site_web_record.urls')), # ajoute pour choix changement langue

    # 2) les_modeles_record
    url(r'^rwtool/usr/', include('record.bd_modeles_record.urls')), 
    url(r'^bdrec/usr/', include('record.bd_modeles_record.urls')), 

    #old url(r'^', include('record.bd_modeles_record.urls')), # ajoute pour choix changement langue

    # 3) outil_web_record
    url(r'^owrec/usr/', include('record.outil_web_record.urls')), 

    ###########################################################################
)

