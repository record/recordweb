
from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('scn_selection.views',

    url(r'^$', 'liste_scenarios'),
    url(r'^liste_scenarios/$', 'liste_scenarios'),

    url(r'^(?P<id_operation>\d+)/scenario_selection/$', 'scenario_selection'),

    url(r'^(?P<index_modele_record_on>\d+)/controle_mdp/$', 'controle_mdp'),

    url(r'^(?P<index_modele_record_on>\d+)/(?P<index_nom_scenario>\d+)/scenario_initialisation/$', 'scenario_initialisation'), # local
)

