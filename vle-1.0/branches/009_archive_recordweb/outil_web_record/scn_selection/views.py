#-*- coding:utf-8 -*-

###############################################################################
# File views.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
# View scn_selection
#
# Choix/selection d'un scenario (fichier vpz) dans un modele record (paquet).
#
# Fait partie du processus d'exploitation des scenarios par les utilisateurs,
# selon lequel il s'agit de choisir un scenario (dans un paquet), puis de le
# simuler apres en avoir visualise et eventuellement modifie la configuration
# (parametrage), et enfin d'exploiter sous diverses formes les resultats de
# la simulation effectuee (les visualiser a l'ecran, telecharger...).
#
###############################################################################

#from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.core.context_processors import csrf

from models.modele_record.modele_record import LesModelesRecord
from models.scn import espace_exploitation

from models.affichage_saisie.choix_operations import ChoixOperations as CHX # noms des choix
from models.affichage_saisie.appel_page import NomsPages
from models.affichage_saisie.parametres_requete import ParametresRequete
from models.affichage_saisie.appel_page import contexte_liste_scenarios

from record.utils import session
from configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
#t_err = TraceErreur(__file__,__name__,CONF_trace)

###############################################################################
#
# CHOIX/SELECTION SCENARIO
#
###############################################################################

###############################################################################
# Point d'entree de l'outil_web_record : initialisations, nettoyage, 
# preparation et affichage page du choix d'un scenario (dans un modele record)
#
# Pour permettre a l'usr de faire son choix, il est affiche la
# liste_modeles_record_on (et pour info liste_modeles_record_off).
# Lorsque l'usr selectionne un modele dans liste_modeles_record_on :
# - si modele prive non deverouille (mot de passe pas encore saisi) : avant
#   de montrer la liste des scenarios du modele, il est demande a l'usr de
#   saisir le mot de passe du modele.
# - si modele public ou modele prive deverouille (mot de passe deja saisi) :
#   il est affiche la liste des scenarios du modele dans laquelle l'usr peut
#   selectionner celui de son choix.
#
###############################################################################

def liste_scenarios(request):

    ###########################################################################
    # Nettoyage
    ###########################################################################

    # suppression des repertoires d'exploitation obsoletes eventuellement
    # presents (le critere d'obsolescence : anciennete du repertoire)

    liste = espace_exploitation.getListeRepExploitation()

    listeAsupprimer = [ rep for rep in liste if espace_exploitation.isObsolete( rep ) ]
    listeAgarder = [ rep for rep in liste if not espace_exploitation.isObsolete( rep ) ]

    for rep in listeAsupprimer :
        espace_exploitation.supprimerRepertoire( rep )

    # trace 
    t_ecr.trait()
    m = "Liste des répertoires - espaces d'exploitation - qui ont été supprimés (obsolètes) : " + str(listeAsupprimer)
    t_ecr.message(m)
    m = "Liste des répertoires - espaces d'exploitation - restants (non obsolètes) : " + str(listeAgarder)
    t_ecr.message(m)
    t_ecr.trait()

    # suppression de l'eventuel scn precedent
    if session.existe(request, 'scn') :
        session.get(request, 'scn').supprimer() # pour aussi supprimer repertoire d'exploitation
    session.suppression_prealable(request, 'scn')

    ###########################################################################
    # Initialisations
    ###########################################################################

    # les_modeles_record
    les_modeles_record = LesModelesRecord()
    session.suppression_prealable(request, 'les_modeles_record' ) # prealable
    session.set(request, 'les_modeles_record', les_modeles_record)
    session.sauver(request)

    ###########################################################################
    # Preparation du contexte d'affichage page du choix d'un scenario
    ###########################################################################

    # les_modeles_record deja pret

    flashes = [ 'FROM liste_scenarios (tmp a jeter !!!)' ] # aucun message d'erreur en flashes

    c = contexte_liste_scenarios(request, flashes, les_modeles_record )

    return render_to_response( NomsPages.NOM_page_liste_scenarios, c )


###############################################################################
# La page du choix d'un scenario (dans un modele record) a deja ete affichee
# une 1ere fois (initialisations effectuees).
#
# Le traitement effectue varie en fonction de id_operation qui caracterise
# l'endroit depuis lequel scenario_selection est appele et la raison de
# l'appel.
#
# Preparation page du choix d'un scenario, avec raz/oubli des eventuels
# deverouillages existants (si appel avec VAL_operation_selection_raz), avec
# memorisation des eventuels deverouillages existants (si appel avec
# VAL_operation_selection_memo).
# Affichage page du choix d'un scenario (dans un modele record)
#
###############################################################################
def scenario_selection(request, id_operation) :

    id_operation = int(id_operation)

    ###########################################################################
    # Nettoyage, initialisations
    ###########################################################################

    # suppression de l'eventuel scn precedent
    if session.existe(request, 'scn') :
        session.get(request, 'scn').supprimer() # pour aussi supprimer repertoire d'exploitation
    session.suppression_prealable(request, 'scn')

    # les_modeles_record (initialisation conditionnelle)
    if id_operation == CHX.VAL_operation_selection_raz :
        # oubli des deverouillages existants
        les_modeles_record = LesModelesRecord()
        session.suppression_prealable(request, 'les_modeles_record' ) # prealable
        session.set(request, 'les_modeles_record', les_modeles_record)
        session.sauver(request)

    # else CHX.VAL_operation_selection_memo : les_modeles_record inchange

    ###########################################################################
    # Preparation du contexte d'affichage page du choix d'un scenario
    ###########################################################################

    # les_modeles_record deja pret
    les_modeles_record = session.get(request, 'les_modeles_record')

    flashes = [ 'FROM scenario_selection (tmp a jeter !!!)' ] # aucun message d'erreur en flashes

    c = contexte_liste_scenarios(request, flashes, les_modeles_record )

    return render_to_response( NomsPages.NOM_page_liste_scenarios, c )

###############################################################################
# Verifie le mot de passe saisi, et en tient compte pour le ModeleRecordOn
# concerne (index_modele_record_on dans liste_modeles_record_on) et 
# Preparation et affichage page du choix d'un scenario (dans un modele record)
###############################################################################
def controle_mdp(request, index_modele_record_on):

    index_modele_record_on = int(index_modele_record_on)

    parametres_post = ParametresRequete( request.POST )
    mdp_saisi = parametres_post.get_valeur( "mot_de_passe" ) # iiiii ajouter gestion du cas ou il n'existe pas

    ###########################################################################
    # Verification et prise en compte du mot de passe saisi
    ###########################################################################

    les_modeles_record = session.get(request, 'les_modeles_record')

    modele_record_on = les_modeles_record.getModeleRecordOn( index_modele_record_on )
    if modele_record_on.checkPassword( mdp_saisi ) : # bon mot de passe
        # le modele record est rendu accessible
        session.get(request, 'les_modeles_record').deverouillerModeleRecordOn( index_modele_record_on )
        session.sauver(request)

    ###########################################################################
    # Preparation du contexte d'affichage page du choix d'un scenario
    ###########################################################################

    # les_modeles_record deja pret

    # aucun message d'erreur en flashes
    flashes = []

    c = contexte_liste_scenarios(request, flashes, les_modeles_record )

    return render_to_response( NomsPages.NOM_page_liste_scenarios, c )

###############################################################################
# Le scenario est choisi (index_scenario_on de index_modele_record_on, ainsi
# que sa presentation/configuration web index_application_associee)
# Traitement partiel de verification
# (ce n'est pas le traitement scenario_initialisation de scn_selection qui
# sert dans l'outil_web_record, mais scenario_initialisation de scn_edition)
###############################################################################
def scenario_initialisation(request, index_modele_record_on, index_scenario_on, index_application_associee ):

    les_modeles_record = session.get(request, 'les_modeles_record')
    scenario_vpz = les_modeles_record.getScenarioVpz( int(index_modele_record_on), int(index_scenario_on) )
    application_associee = les_modeles_record.getApplicationWeb( int(index_modele_record_on), int(index_scenario_on), int(index_application_associee) )
    infos_application_id = application_associee.getId()

    # trace 
    t_ecr.trait()
    m = "Le scenario choisi est le scenario : " + scenario_vpz.nom_vpz 
    m = m + " du modele record : " + scenario_vpz.paquet.nom + " (paquet " + scenario_vpz.paquet.nom_pkg + "),"
    t_ecr.message(m)
    m = "à présenter selon la configuration web : " 
    t_ecr.msg(m)

    m = "fichier vpz de configuration du dictionnaire : "
    m = m + str( infos_application_id.getNomAbsoluFichierConfigDicos() ) 
    m = m + " ; "
    m = m + "fichier vpz de configuration web propre aux applis web : "
    m = m + str( infos_application_id.getNomAbsoluFichierConfigWebApplis() )
    m = m + " ; "
    m = m + "nom application : "
    m = m + str( infos_application_id.getApplicationName() )
    m = m + " ; "
    m = m + "type source : "
    m = m + str( infos_application_id.getTypeSourceConfWebScenario() )
    t_ecr.msg( m )

    return render_to_response( NomsPages.NOM_page_fin )

###########################################################################

