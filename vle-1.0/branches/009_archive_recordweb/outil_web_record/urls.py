
from django.conf.urls.defaults import *

from django.conf import settings

# to enable the admin:
#from django.contrib import admin
#admin.autodiscover()

###############################################################################

urlpatterns = patterns('',

    # internationalisation traductions
    # a view that accepts a language POST parameter to change session language
    (r'^i18n/', include('django.conf.urls.i18n')),
)

if settings.DEBUG:
    urlpatterns += patterns('',

        # static files

        # nouveau (pour prod)

        #(r'^site_media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATICFILES_DIRS }),
        # (marchait avec un seul element dans STATICFILES_DIRS, sans ',' apres)
        # a ete remplace par

        # avec collecstatic (a tester sur serveur une fois maj /etc/apache2/.../fichier default !!!)
        (r'^site_media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT }),

        # ancien (dev)
        #url(r'^site_media/(?P<path>.*)$', 'django.views.static.serve', {'document_root':settings.SITE_MEDIA_OUTIL_WEB_RECORD} ),

    )

urlpatterns += patterns('',

    # administration : neant

    ###########################################################################
    # Les priorites (ecrasements) :
    ###########################################################################

    # scenario_initialisation de scn_edition et non pas de scn_selection :
    url(r'^(?P<index_modele_record_on>\d+)/(?P<index_scenario_on>\d+)/(?P<index_application_associee>\d+)/scenario_initialisation/$', include('scn_edition.urls')),

    ###########################################################################
    # par ordre chronologique d'appel :
    ###########################################################################

    # 1) scn_selection
    url(r'^', include('scn_selection.urls')),
    url(r'^usr/', include('scn_selection.urls')), # synonyme

    # 2) scn_edition
    url(r'^', include('scn_edition.urls')),

    # 3) scn_simulation
    url(r'^', include('scn_simulation.urls')),

    # 4) res_telechargement, res_numerique, res_graphique
    url(r'^', include('res_telechargement.urls')),
    url(r'^', include('res_numerique.urls')),
    url(r'^', include('res_graphique.urls')),
)

