
from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('scn_edition.views',

    url(r'^$', 'scenario_initialisation'),
    url(r'^(?P<index_modele_record_on>\d+)/(?P<index_scenario_on>\d+)/(?P<index_application_associee>\d+)/scenario_initialisation/$', 'scenario_initialisation'), # local

    url(r'^(?P<id_operation>\d+)/scenario_actualisation/$', 'scenario_actualisation'),
)

