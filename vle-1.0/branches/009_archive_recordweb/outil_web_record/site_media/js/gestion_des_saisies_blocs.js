/*
 * File gestion_des_saisies_blocs.js
 *
 * Application Web RECORD nom_appli_web_record_a_definir
 *
 * Author : Nathalie Rousse, INRA RECORD team member.
 *
 * Copyright (C) 2011 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*****************************************************************************
 * gestion des saisies des donnees des blocs :
 * { gestion_des_saisies_blocs.js }
 *
 * Gestion de la saisie des valeurs multiples dans un bloc : ajout et
 * suppression de valeurs/champs de saisie pour chaque donnee une a une (cf
 * ajout_unique, suppression_unique) ou bien de maniere groupee pour toutes
 * les donneees du bloc (cf ajout_groupe).
 *
 * Verification qu'il n'existe de champ vide dans aucun bloc (en fait dans
 * formulaire), avec message d'alerte (cf messages_a_effacer,
 * message_a_afficher, texte_msg_ok, texte_msg_notok), un champ de saisie
 * etant determine par $('input:text').
 *
 *****************************************************************************/

/*
 * Methode gestion_des_saisies_blocs :
 *
 * Les donnees d'un bloc sont presentees dans un tableau ou chaque
 * donnee (dataname,datavalues) correspond a une ligne.
 * Cette ligne est de la forme :
 *
 * <tr id="${dataname}">
 *
 *    Pour chaque value dans datavalues :
 *    <td> <input type="text", name=... , value=... /> </td>
 *
 *    <td class=ajout_unique> <a href="#${dataname}">Add</a> </td>
 *
 *    <td class=suppression_unique> <a href="#${dataname}">X</a> </td>
 *
 * </tr>
 *
 * Dans la zone (div) des informations du bloc (au dessus des tableaux de
 * chaque donnee), le menu suivant donne la possibilite d'ajouter de maniere
 * groupee une valeur a toutes les donnees du bloc :
 *
 * <p class="ajout_groupe"> <a href="#liste_blocs">Add all</a> </p>
 *
 */
function gestion_des_saisies_blocs( ajout_unique, suppression_unique, ajout_groupe, formulaire, messages_a_effacer, message_a_afficher, texte_msg_ok, texte_msg_notok )
{
    /*************************************************************************
     * Pour la dataname du click, ajout d'une valeur (un champ de saisie)
     *************************************************************************/
    $(ajout_unique).click( function () {

        var le_tr_parent = $(this).parent(); // correspond a <tr>
        var les_valeurs = le_tr_parent.children().filter('td').has('input');
        un_ajout_a( les_valeurs );
    });

    /*************************************************************************
     * Pour le bloc du click,
     * ajout d'une valeur a toutes ses donnees
     *************************************************************************/
    $(ajout_groupe).click( function () {

        var le_div_parent = $(this).parent(); // correspond a <div>
        var les_tr_valeurs = le_div_parent.children().filter('table').children().filter('tbody').children().filter('tr');

        les_tr_valeurs.each( function(){
            var les_valeurs = $(this).children().filter('td').has('input');
            un_ajout_a( les_valeurs );
        });
    });

    /*************************************************************************
     * Pour la dataname du click, suppression de la
     * derniere valeur (champ de saisie), sauf si c'est la seule
     *************************************************************************/
    $(suppression_unique).click( function () {

        var le_tr_parent = $(this).parent(); // correspond a <tr>
        var les_valeurs = le_tr_parent.children().filter('td').has('input');
        if ( les_valeurs.length > 1 ) { // plus d'une valeur
            var last_valeur = les_valeurs.filter(":last");
            $(last_valeur).remove();
        } // sinon : valeur non supprimee
    });

    /*************************************************************************
     * Avant/pour soumission du formulaire, il est controle/verifie qu'aucun
     * champ de saisie n'est vide.
     * Affichage (dans message_a_afficher) du message texte_msg_ok si
     * verification ok ou texte_msg_notok si verification notok, apres avoir
     * efface les eventuellement presents messages_a_effacer.
     *
     * Remarque : un champ de saisie est determine par $('input:text')
     *
     *************************************************************************/
    $(formulaire).submit( function() {

        var cr_ok = true;

        // cr_ok passe a false si au moins un champ vide
        var les_input_values = $('input:text')
        les_input_values.each( function(){
            if ( $(this).val() == "" ) { 
                cr_ok = false;
            }
        });

        $(messages_a_effacer).remove();

        if ( cr_ok == true ) {
            $(message_a_afficher).text( texte_msg_ok );
            return true;
        } else {
            $(message_a_afficher).text( texte_msg_notok );
            return false;
        }
    });
}

/*
 * Methode un_ajout_a :
 *
 * Ajout d'une valeur (champ de saisie) a la serie les_valeurs, avec comme 
 * name et value ceux de son predecesseur (dernier dans les_valeurs)
 */
function un_ajout_a( les_valeurs )
{
    var last_valeur = les_valeurs.filter(":last");
    var son_input = last_valeur.children().filter('input').filter(":last");
    var last_input_name = son_input.attr("name");
    var last_input_value = son_input.val();

    var texte_td = "<td> <input type='text', name='" + last_input_name + "', value='" + last_input_value + "' /> </td>"
    // ajout/creation juste apres la derniere valeur
    $(texte_td).insertAfter(last_valeur);
}

