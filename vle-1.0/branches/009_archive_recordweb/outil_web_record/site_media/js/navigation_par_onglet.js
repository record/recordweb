/*
 * File navigation_par_onglet.js
 *
 * Application Web RECORD nom_appli_web_record_a_definir
 *
 * Author : Nathalie Rousse, INRA RECORD team member.
 *
 * Copyright (C) 2011 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*****************************************************************************
 * navigation par onglet :
 * { navigation_par_onglet.css, navigation_par_onglet.js }
 *
 * Proposition/affichage d'onglets dont la selection conditionne l'affichage
 * de la partie correspondante/associee dans la page.
 *
 *****************************************************************************/

/*
 * Dans le menu 'div.menu', affichage uniquement de la partie correspondant 
 * a l'onglet selectionne au niveau de 'navigation'.
 *
 * <div class="menu">
 *
 *     <ul class="navigation">
 *         <li><a href="#xxx">Affichage partie xxx</a></li>
 *         <li><a href="#yyy">Affichage partie yyy</a></li>
 *         ...
 *     </ul>
 *
 *     <div id="xxx">
 *         ...
 *     </div>
 *
 *     <div id="yyy">
 *         ...
 *     </div>
 *     ...
 *
 * </div>
 *
 */
function navigation_par_onglet()
{
    var boites_page = $('div.menu > div');

    boites_page.hide()
               .filter(':first').show();

    $('div.menu ul.navigation a').click(function () {

        boites_page.hide();
        boites_page.filter(this.hash).show();
        $('div.menu ul.navigation a').removeClass('selected');
        $(this).addClass('selected');
        return false;
    });
}

