/*
 * File accordeon.js
 *
 * Application Web RECORD nom_appli_web_record_a_definir
 *
 * Author : Nathalie Rousse, INRA RECORD team member.
 *
 * Copyright (C) 2011 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*****************************************************************************
 * accordeon : { accordeon.css, accordeon.js }
 *
 * Affichage d'informations en accordeon
 *
 *****************************************************************************/

/*
 * Chaque element de type 'bord' contient un element de type 'depliant'.
 * 'bord' est la partie qui reste apparente quand l'accordeon est replie.
 * 'depliant' est la partie deroulee quand click du 'bord' correspondant
 * (sous lequel 'depliant' se trouve).
 */
function accordeon(bord,depliant)
{
    var accordeon_depliant=".accordeon "+depliant;
    $(accordeon_depliant).hide();

    accordeon_bord=".accordeon "+bord;

    $(accordeon_bord).click(function(){

        depliant_visible=depliant+":visible";
        $(this).next(depliant).slideToggle("slow")
               .siblings(depliant_visible).slideUp("slow");
        $(this).toggleClass("active");
        $(this).siblings(bord).removeClass("active");

    });
}

