#-*- coding:utf-8 -*-

###############################################################################
# File views.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
# View scn_simulation
#
# Simulation d'un scenario (action de simulation)
#
# Fait partie du processus d'exploitation des scenarios par les utilisateurs,
# selon lequel il s'agit de choisir un scenario (dans un paquet), puis de le
# simuler apres en avoir visualise et eventuellement modifie la configuration
# (parametrage), et enfin d'exploiter sous diverses formes les resultats de
# la simulation effectuee (les visualiser a l'ecran, telecharger...).
#
###############################################################################

#from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.core.context_processors import csrf

from models.scn.resultat import Resultat # va etre change d'endroit iiii !!!!
from models.affichage_saisie.choix_operations import ChoixOperations as CHX # noms des choix
from models.affichage_saisie.appel_page import NomsPages, contexte_simulation_menu, contexte_restitution_menu
from models.erreurs_gerees.erreurs_gerees import ErreursGerees as ERR # noms des erreurs

from record.utils import session
from record.utils.erreur import Erreur # gestion des erreurs
from record.utils.erreur import get_message_exception
from record.utils.exp import Exp

###############################################################################
#
# SIMULATION DU SCENARIO
#
###############################################################################

###############################################################################
# Le traitement effectue varie en fonction de id_operation qui caracterise
# l'endroit depuis lequel scenario_simulation est appele et la raison de
# l'appel.
# Si appel depuis affichage menu de simulation (le type de simulation choisi
# est choix_simulation=id_operation) : execution de la simulation (sinon, par
# exemple appel retour en arriere, pas d'action de simulation).
# Affichage du menu de restitution et sa preparation, sauf dans cas de
# simulation si jamais echec de simulation, auquel cas affichage du menu de
# simulation et sa preparation (avec erreur).
###############################################################################
def scenario_simulation(request, id_operation ):

    id_operation = int(id_operation)

    # Initialisations

    erreur_echec_simulation = Erreur( ERR.NOM_erreur_echec_simulation ) # desactivee par defaut

    # Cas appel en retour arriere pour affichage menu de restitution
    if id_operation == CHX.VAL_operation_retour_restitution :
        nom_template = NomsPages.NOM_page_restitution_menu

    # Cas de choix de simulation
    elif CHX.is_choix_simulation( id_operation ) :
        choix_simulation = id_operation

        # type de simulation choisie (et mode de combinaison) :
        (type_simulation, mode_combinaison) = CHX.get_simulation_type_et_mode(int(choix_simulation))

        # Initialisations

        nom_template = NomsPages.NOM_page_simulation_menu # par defaut

        cas_d_erreur = False # par defaut
        message_erreur = "" # par defaut

        #######################################################################
        # exp correspond a scenario_vpz, avec prise en compte des valeurs de
        # definition
        #######################################################################
        scn = session.get(request, 'scn')
        exp = Exp( scn.scenario_vpz.nom_vpz,
                   scn.scenario_vpz.get_nom_pkg().encode() )
        try :
            exp = scn.definition.prendreEnCompteDans( exp )
        except Exception, e :
            cas_d_erreur = True
            #message_erreur = message_erreur + " *** " + e.message
            message_erreur = message_erreur + " *** " + get_message_exception(e)

        else : # bon deroulement de prendreEnCompteDans, continuer

            # objet resultat de simulation
            # depend du type de simulation (simple ou multiple)
            resultat = Resultat( type_simulation, mode_combinaison )

            ###################################################################
	        # Lance la simulation (2 fois) : (1) simulation et sauvegarde 
            # fichiers en preparation de la demande de telechargement des
            # resultats, (2) simulation dans le but d'en afficher les
            # resultats (representations graphiques...)
            ###################################################################

            try:
                resultat.simuler( exp, scn.espace_exploitation )
            except Exception, e:

                cas_d_erreur = True
                message_erreur = message_erreur + " *** " + get_message_exception(e)

            else : # bon deroulement de simuler, continuer
            # (simulation sans levee d'erreur)

                # prise en compte du traitement (resultat)
                # enregistre dans session
                session.get(request, 'scn').resultat = resultat
                session.sauver(request)

                nom_template = NomsPages.NOM_page_restitution_menu

        if cas_d_erreur : # echec de simulation
        # (au niveau de definition.prendreEnCompteDans ou resultat.simuler)

            # maj erreur_echec_simulation
            erreur_echec_simulation.activer()
            erreur_echec_simulation.set_message( message_erreur )

            nom_template = NomsPages.NOM_page_simulation_menu

    ###########################################################################
    # else : rien a faire
    ###########################################################################

    ###########################################################################
    # Preparation du contexte d'affichage du menu de simulation
    ###########################################################################
    if nom_template == NomsPages.NOM_page_simulation_menu :

        scn = session.get(request, 'scn')

        # Liste messages d'erreur en flashes
        flashes = [] # par defaut
        if erreur_echec_simulation.isActive() : # maj flashes selon erreur_echec_simulation
            texte_erreur = "Erreur : la simulation a échoué."
            texte_message_erreur = "Message d'erreur : " + erreur_echec_simulation.get_message() + "."
            texte_precision = "Pas de \'retour en arrière\' pour le scénario en cours. Autrement dit ses informations actuelles restent dans l\'état dans lequel elles étaient lors de la dernière demande de simulation (bouton \'Simulation simple\' ou \'Simulation multiple\'), qui vient d\'échouer."
            texte_et_apres = "Vous pouvez tenter une autre simulation (bouton \'Simulation simple\' ou \'Simulation multiple\') ou retourner voir/modifier l\'état qui a conduit à l\'échec de simulation (\'Retour à visualisation/modification du scénario en cours\') ou choisir un nouveau scénario (\'Retour au choix du scénario\')."
            flashes.append( texte_erreur )
            flashes.append( texte_message_erreur )
            flashes.append( texte_precision )
            flashes.append( texte_et_apres )

        c = contexte_simulation_menu(request, flashes, scn)

    ###########################################################################
    # Preparation du contexte d'affichage du menu de restitution
    ###########################################################################
    elif nom_template == NomsPages.NOM_page_restitution_menu :
 	    # Il s'agit du PREMIER affichage du menu de restitution
        scn = session.get(request, 'scn')
        flashes = [] # pas de flashes
        c = contexte_restitution_menu(request, flashes, scn)

    else :
        c = {} # vide

    return render_to_response(nom_template, c)

