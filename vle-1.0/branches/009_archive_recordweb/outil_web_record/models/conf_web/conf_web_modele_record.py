#-*- coding:utf-8 -*-
###############################################################################
# File conf_web_modele_record.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

import copy

from models.conf_web.conf_web import TypeSourceConfWebModeleRecord
from models.conf_web.conf_web import TypeSourceConfWebScenario

from models.conf_web.transcodage import TranscodageConfWeb as tcw

from models.conf_web.infos_generales import InfosGenerales
from models.conf_web.infos_generales import InfosGeneralesAppli
from models.conf_web.infos_generales import InfosGeneralesScenario

from record.utils.exp import get_liste_vpz_sans_extension

from configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

###############################################################################
#
# Classes et methodes dediees a la configuration web d'un modele record
#
###############################################################################

###############################################################################
# Modele : ConfWebModeleRecord
#
# Les informations de configuration d'un modele record
#
# ConfWebModeleRecord est utilise pour presenter dans l'outil_web_record
# les scenarios possibles/disponibles de chaque modele record (pour en
# choisir un) et/puis les configurations d'application web possibles de
# chaque scenario (pour en choisir une).
#
# ConfWebModeleRecord contient des informations determinant/personnalisant la
# presentation a l'ecran d'un modele record : les scenarios a montrer, les
# possibilites de presentation de chaque scenario...
#
# Objet sur lequel s'appuie ModeleRecordOn (necessaire a sa construction)
#
# Le parametre de construction source_conf_web_modele_record
# (SourceConfWebModeleRecord) indique le format (cf type_source) dans lequel
# les informations de configuration ont ete renseignees et contient ces
# informations.
# Voir SourceConfWebModeleRecord et TypeSourceConfWebModeleRecord.
#
# ConfWebModeleRecord contient :
# - noms_scenarios : la liste des noms des scenarios disponibles (a montrer)
# - applications_web_associees : dict { nom_scenario :
#   liste [] de ses ApplicationWeb applications web associees }
# - infos_generales_scenarios : dict { nom_scenario : InfosGeneralesScenario }
#
# L'objet ConfWebModeleRecord est 'invalide' (cf 'invalider', 'isInvalide')
# si son initialisation/construction non effective (not ok)
#
###############################################################################
class ConfWebModeleRecord(object):

    ###########################################################################
    #
    # Construction a partir de source_conf_web_modele_record en fonction de
    # type_source
    #
    ###########################################################################
    def __init__( self, source_conf_web_modele_record=None ):

        self.initRaz() # objet vide (mais valide)

        if source_conf_web_modele_record == None :
        # fin : ConfWebModeleRecord produit est un objet vide (mais valide)

            t_ecr.message( "ConfWebModeleRecord : ConfWebModeleRecord produit est un objet vide (mais valide)" )

        else : # source_conf_web_modele_record != None

            # indicateur initialisation_effective_ok du bon/mauvais deroulement
            # du traitement (pour False, l'objet construit est rendu invalide)
            initialisation_effective_ok = False # par defaut

            type_source = source_conf_web_modele_record.getTypeSource()

            if not TypeSourceConfWebModeleRecord.isValueTypeSourceConfWebModeleRecord( type_source ) :
                # initialisation_effective_ok reste False
                pass

            elif TypeSourceConfWebModeleRecord.isValueVide( type_source ) :
                # initialisation_effective_ok reste False
                pass

            elif TypeSourceConfWebModeleRecord.isValueWebVpz( type_source ) :

                # Construction a partir de exp_config_web_conf
                exp_conf_web = source_conf_web_modele_record.exp_config_web_conf
                configuration_web = exp_conf_web.configuration_web

                if configuration_web == None : # la configuration n'est pas appliquee (s'est mal passee ou desactivee)
                    t_ecr.message( "ConfWebModeleRecord : la configuration a partir du fichier vpz de configuration web n'est pas appliquee (s'est mal passee ou desactivee)" )
                    # initialisation_effective_ok reste False
                else :
    
                    # deepcopy
                    self.noms_scenarios = copy.deepcopy( configuration_web.noms_scenarios )
                    self.applications_web_associees = copy.deepcopy( configuration_web.applications_web_associees )
                    self.infos_generales_scenarios = copy.deepcopy( configuration_web.infos_generales_scenarios )

                    initialisation_effective_ok = True # a ce stade

                    # remarque : l'ApplicationWeb par defaut n'est pas
                    # 'systematiquement' ajoutee ici ; cela a ete fait auparavant,
                    # dans ExpConfigWebModeleRecord (ApplicationWeb par defaut
                    # avec dico, selon mode_application)

            elif TypeSourceConfWebModeleRecord.isValueParDefaut( type_source ) :
    
                # Construction par defaut a partir de noms_scenarios_existants
                noms_scenarios_existants = source_conf_web_modele_record.noms_scenarios_existants
                self.initParDefaut( noms_scenarios_existants )
    
                initialisation_effective_ok = True # a ce stade

            if initialisation_effective_ok :

                # Quelques regles SYSTEMATIQUEMENT appliquees pour combler 
                # d'eventuels vides/non renseignements, comportements par defaut
                self.applicationRegles()
    
                # trace
                t_ecr.message( "ConfWebModeleRecord, la configuration a ete effectuee (bon deroulement)." )
                #self.traceConfWebModeleRecord()
    
                # initialisation_effective_ok inchange

            if not initialisation_effective_ok :
                # objet rendu invalide pour indiquer 
                #  initialisation/construction non effective (not ok)
                self.invalider()
                t_ecr.message( "ConfWebModeleRecord : ConfWebModeleRecord produit est invalide" )

    ###########################################################################
    # Initialisation - raz
    ###########################################################################
    def initRaz( self ) :

        # noms_scenarios, liste ordonnee des noms des scenarios disponibles
        self.noms_scenarios = []

        # applications_web_associees,
        # dict des applications associees a chaque scenario
        self.applications_web_associees = {}

        # infos_generales_scenarios
        # dict des infos generales de chaque scenario
        self.infos_generales_scenarios = {}

    ###########################################################################
    # Suppression du scenario nomme nom_scenario
    ###########################################################################
    def supprimerScenario( self, nom_scenario ) :
        self.noms_scenarios.remove( nom_scenario ) 
        del self.applications_web_associees[nom_scenario]
        del self.infos_generales_scenarios[nom_scenario]

    ###########################################################################
    # Rendre invalide (mettre tous les champs a None)
    ###########################################################################
    def invalider( self ) :
        self.noms_scenarios = None
        self.applications_web_associees = None
        self.infos_generales_scenarios = None

    ###########################################################################
    # indique si invalide  (tous les champs a None)
    # remarque : un objet issu de initRaz est vide mais valide
    ###########################################################################
    def isInvalide( self ) :
        if self.noms_scenarios==None and self.applications_web_associees==None and self.infos_generales_scenarios==None :
            return True
        else :
            return False

    ###########################################################################
    #
    # Initialisation de ConfWebModeleRecord par defaut,
    # a partir de noms_scenarios_existants qui est la liste des noms des
    # scenarios existants du modele record (noms des fichiers vpz sans leur
    # extension).
    #
    # (correspond a configuration standard)
    #
    ###########################################################################
    def initParDefaut( self, noms_scenarios_existants ) :

        self.initRaz() # raz

        #######################################################################
        # noms_scenarios, liste ordonnee des noms des scenarios disponibles
        #######################################################################

        self.noms_scenarios = copy.deepcopy( noms_scenarios_existants )

        #######################################################################
        # applications_web_associees,
        # dict des applications associees a chaque scenario
        #######################################################################

        # creation des listes (vides)
        for nom_scenario in self.noms_scenarios :
            self.applications_web_associees[ nom_scenario ] = []

        # rentree/ajout de l'ApplicationWeb par defaut
        self.ajouterApplicationWebParDefaut( self.applications_web_associees )

        #######################################################################
        # infos_generales_scenarios,
        # dict des infos generales de chaque scenario
        #######################################################################
        for nom_scenario in self.noms_scenarios :
            infos_generales = InfosGeneralesScenario()
            infos_generales.setValeursStandard( nom_scenario )
            self.infos_generales_scenarios[ nom_scenario ] = infos_generales

    ###########################################################################
    #
    # Ajoute l'ApplicationWeb par defaut
    # a toutes les listes applications_web_associees[nom_scenario]
    # (applications_web_associees[nom_scenario] : liste des ApplicationWeb
    #  associees a nom_scenario)
    #
    ###########################################################################
    @classmethod
    def ajouterApplicationWebParDefaut( cls, applications_web_associees ) :

        for applications_associees in applications_web_associees.values() :

            # construction application par defaut
            application_associee_par_defaut = ApplicationWeb()
            application_associee_par_defaut.setValeursApplicationParDefaut()

            applications_associees.append( application_associee_par_defaut )

    ###########################################################################
    #
    # Ajoute l'ApplicationWeb par defaut avec dico
    # a toutes les listes applications_web_associees[nom_scenario]
    # (applications_web_associees[nom_scenario] : liste des ApplicationWeb
    #  associees a nom_scenario)
    #
    ###########################################################################
    @classmethod
    def ajouterApplicationWebParDefautAvecDico( cls, applications_web_associees, nom_absolu_fichier_config_dicos ) :

        for applications_associees in applications_web_associees.values() :

            # construction application par defaut avec dico
            application_associee_par_defaut_avec_dico = ApplicationWeb()
            application_associee_par_defaut_avec_dico.setValeursApplicationParDefautAvecDico( nom_absolu_fichier_config_dicos )

            applications_associees.append( application_associee_par_defaut_avec_dico )

    ###########################################################################
    #
    # Application a ConfWebModeleRecord de quelques regles
    # pour combler d'eventuels vides/non renseignements
    # comportements par defaut
    # Normalisation des valeurs
    # Empecher/contourner des choix invalides (impossibles, non geres...)
    #
    # Remarque : ConfWebModeleRecord est aussi susceptible d'etre transforme
    # dans assuranceCompatibilite
    #
    ###########################################################################
    def applicationRegles( self ) :

# 999999 a completer ?
# 999999 ajouter traces ecran informant des reactions ?

        #######################################################################
        # infos_generales_scenarios : pour tout scenario nomme dans
        # noms_scenarios, si defaillances/absences de
        # infos_generales_scenarios[nom_scenario] ou d'un de ses champs,
        # alors affectations en reaction 
        #######################################################################
        for nom_scenario in self.noms_scenarios :

            if tcw.valeurVide( self.infos_generales_scenarios[nom_scenario] ) :
                infos_generales = InfosGeneralesScenario()
                infos_generales.setValeursStandard( nom_scenario )
                self.infos_generales_scenarios[ nom_scenario ] = infos_generales
            else :
                self.infos_generales_scenarios[nom_scenario].setValeursDefautUneParUne( nom_scenario ) 

        #######################################################################
        # applications_web_associees vis a vis de noms_scenarios :
        # Si un scenario nomme dans noms_scenarios n'est pas dans
        # applications_web_associees (n'en est pas une cle), alors il y est
        # ajoute avec comme liste application_web_associee le singleton de
        # l'application par defaut
        #######################################################################
        for nom_scenario in self.noms_scenarios :
            if nom_scenario not in self.applications_web_associees.keys() :

                # construction applications_associees (singleton application par defaut)
                application_associee_par_defaut = ApplicationWeb()
                application_associee_par_defaut.setValeursApplicationParDefaut()
                applications_associees = [ application_associee_par_defaut ]

                # ajout
                self.applications_web_associees[ nom_scenario ] = applications_associees

        #######################################################################
        # Pour chaque application_associee de chaque
        # applications_web_associees[nom_scenario], si defaillances/absences
        # d'un des champs de application_associee.infos_generales, alors
        # affectations en reaction 
        #######################################################################
        for applications_associees in self.applications_web_associees.values() :
            for application_associee in applications_associees :
                infos_generales = application_associee.getInformationsGenerales()
                infos_generales.setValeursDefautUneParUne() 

    ###########################################################################
    # assuranceCompatibilite :
    #
    # Controle la compatibilite de ConfWebModeleRecord avec le modele record
    # auquel elle est sensee etre appliquee. Effectue sur ConfWebModeleRecord
    # certaines transformations/modifications pour obtenir la compatibilite.
    # Retourne True si compatibilite au final (apres eventuelles
    # modifications) et False sinon.
    #
    # Les cas/verifications/modifications geres :
    #
    # I. Verification de l'existence/presence des scenarios a proposer :
    #
    # Verifie que tous les scenarios nommes dans noms_scenarios sont bien
    # presents dans la liste noms_scenarios_existants des noms des scenarios
    # existants du modele record (noms des fichiers vpz sans leur extension).
    #
    # => transformation si ce n'est pas le cas :
    # Enlever/sortir de noms_scenarios le nom du scenario
    #
    # Remarque : ConfWebModeleRecord est aussi susceptible d'etre transforme
    # dans applicationRegles
    #
    ###########################################################################
    def assuranceCompatibilite( self, noms_scenarios_existants ) :

# 999999 a completer ?

        compatibilite = True
        message = ""

        #######################################################################
        # Verification I.
        #######################################################################

        # parcours des noms des scenarios (fichiers vpz) montres du modele/paquet
        photo_noms_scenarios = copy.deepcopy( self.noms_scenarios )
        for nom_scenario in photo_noms_scenarios :

            if nom_scenario not in noms_scenarios_existants :
            # scenario indisponible (inexistant)
                self.supprimerScenario( nom_scenario ) # suppression
                message = message+" -- " + " scenario (" +nom_scenario+ ") non propose car inexistant"
            # else : scenario disponible (a la fois existant et montre)

        return ( compatibilite, message )

    ###########################################################################
    # Methode getApplicationWeb 
    #
    # Retourne ApplicationWeb correspondant a l'application associee au
    # scenario nom_scenario, qui est identifiee/caracterisee par son index
    # index_application_associee dans la liste des applications associees au
    # scenario
    ###########################################################################
    def getApplicationWeb( self, nom_scenario, index_application_associee ) :
        applications_associees = self.applications_web_associees[ nom_scenario ]
        application_associee = applications_associees[ index_application_associee ] 
        return application_associee

    ###########################################################################
    #
    # Traces ecran
    #
    ###########################################################################

    def traceConfWebModeleRecord( self ):

        deco = "####################################"
        t_ecr.saut_de_ligne()
        t_ecr.msg( deco )
        t_ecr.msg( "# CONFIGURATION ConfWebModeleRecord" )
        t_ecr.msg( deco )

        self.traceScenariosEtApplicationsWebAssociees( self.noms_scenarios, self.infos_generales_scenarios, self.applications_web_associees )

        t_ecr.msg( deco )
        t_ecr.saut_de_ligne()

    ###########################################################################
    # trace ecran des noms des scenarios, de leur infos_generales_scenario
    # et de leurs applications_web_associees
    ###########################################################################
    @classmethod
    def traceScenariosEtApplicationsWebAssociees( cls, noms_scenarios, infos_generales_scenarios, applications_web_associees ):

        message = "Liste ordonnee des noms des scenarios : " 
        t_ecr.msg( message + str(noms_scenarios) )

        for nom_scenario in noms_scenarios :

            t_ecr.saut_de_ligne()
            message = "Informations generales du scenario " + str( nom_scenario ) + " : "
            t_ecr.msg( message )

            infos_generales_scenario = infos_generales_scenarios[ nom_scenario ]
            t_ecr.msg( "titre : " + str(infos_generales_scenario.titre) )
            t_ecr.msg( "description : " + str(infos_generales_scenario.description) )
            t_ecr.msg( "help : " + str(infos_generales_scenario.help) )

            t_ecr.saut_de_ligne()
            message = "Liste des applications associees au scenario " + str( nom_scenario ) + " : "
            t_ecr.msg( message )

            applications_associees = applications_web_associees[ nom_scenario ]
            for application_associee in applications_associees :
                message = "fichier vpz de configuration du dictionnaire : "
                message = message + str( application_associee.getId().getNomAbsoluFichierConfigDicos() ) 
                message = message + " ; "
                message = message + "fichier vpz de configuration web propre aux applis web : "
                message = message + str( application_associee.getId().getNomAbsoluFichierConfigWebApplis() )
                message = message + " ; "
                message = message + "nom application : "
                message = message + str( application_associee.getId().getApplicationName() )
                message = message + " ; "
                message = message + "type source : "
                message = message + str( application_associee.getId().getTypeSourceConfWebScenario() )
                message = message + " ; "
                message = message + "titre : "
                message = message + str(application_associee.getInformationsGenerales().titre)
                message = message + " ; "
                message = message + "description : "
                message = message + str(application_associee.getInformationsGenerales().description)
                message = message + " ; "
                message = message + "help : "
                message = message + str(application_associee.getInformationsGenerales().help)
                t_ecr.msg( message )

###############################################################################
#
#
# Des classes de composants de la ConfWebModeleRecord
#
# (voir aussi infos_generales)
#
#
###############################################################################

###############################################################################
#
# Informations d'identification d'une application web : caracteristique
# permettant de la retrouver (quand il s'agira de la lire pour construire
# ConfWebScenario si elle a ete choisie par l'usr)
#
# type_source : designe le type de configuration ; peut valoir VAL_par_defaut,
# VAL_web_vpz, VAL_par_defaut_avec_dico (ou VAL_vide)
#
# nom_absolu_fichier_config_web_applis et application_name : dans le cas
# VAL_web_vpz, les informations de l'application correspondent au parametre
# (port) application_name de la condition des applications, dans le fichier
# vpz de configuration web propre aux applis web
# nom_absolu_fichier_config_web_applis.
#
# nom_absolu_fichier_config_dicos : les dictionnaires de documentation des
# donnees (utilises/exploites dans cas VAL_web_vpz, VAL_par_defaut_avec_dico)
# viennent du fichier vpz de configuration du dictionnaire
# nom_absolu_fichier_config_dicos.
#
###############################################################################
class InfosApplicationId(object):

    def __init__( self ):

        self.type_source = TypeSourceConfWebScenario.VAL_vide 
        self.nom_absolu_fichier_config_dicos = None
        self.nom_absolu_fichier_config_web_applis = None
        self.application_name = None

    def setValeursApplicationParDefaut( self ): 
        self.type_source = TypeSourceConfWebScenario.VAL_par_defaut
        self.nom_absolu_fichier_config_dicos = None
        self.nom_absolu_fichier_config_web_applis = None
        self.application_name = None

    def setValeursApplicationParDefautAvecDico( self, nom_absolu_fichier_config_dicos ) :
        self.type_source = TypeSourceConfWebScenario.VAL_par_defaut_avec_dico
        self.nom_absolu_fichier_config_dicos = nom_absolu_fichier_config_dicos 
        self.nom_absolu_fichier_config_web_applis = None
        self.application_name = None

    def setValeursApplicationWebVpz( self, nom_absolu_fichier_config_web_applis, application_name, nom_absolu_fichier_config_dicos ) :
        self.type_source = TypeSourceConfWebScenario.VAL_web_vpz
        self.nom_absolu_fichier_config_dicos = nom_absolu_fichier_config_dicos
        self.nom_absolu_fichier_config_web_applis = nom_absolu_fichier_config_web_applis
        self.application_name = application_name 

    def getNomAbsoluFichierConfigWebApplis( self ) :
        # remarque : pas de verification ici de l'existence du fichier
        return self.nom_absolu_fichier_config_web_applis

    def getNomAbsoluFichierConfigDicos( self ) :
        # remarque : pas de verification ici de l'existence du fichier
        return self.nom_absolu_fichier_config_dicos

    # retourne la valeur (normalisee) de application_name
    def getApplicationName( self ) :
        application_name = tcw.CONST_valeur_vide # par defaut
        v = self.application_name 
        if type(v) == str :
            application_name = v
        return application_name

    # retourne la valeur (normalisee) de type_source (TypeSourceConfWebScenario)
    def getTypeSourceConfWebScenario( self ) :
        type_source = TypeSourceConfWebScenario.VAL_vide # par defaut
        v = self.type_source 
        if TypeSourceConfWebScenario.isValueTypeSourceConfWebScenario( v ) :
            type_source = v
        return type_source

###############################################################################
#
# Informations d'une application web (a ce stade) : contient
# renseignements necessaires a sa presentation a l'usr (pour faire son choix),
# et les caracteristiques permettant d'aller en lire plus a son sujet si
# choisie/selectionnee (pour construire ConfWebScenario)
#
###############################################################################
class ApplicationWeb(object):

    def __init__( self ):

        # infos_generales
        self.infos_generales = InfosGeneralesAppli()

        # infos_application_id
        self.infos_application_id = InfosApplicationId()

    def setValeursApplicationParDefaut( self ): 

        # infos_generales
        self.infos_generales.setValeursStandard()

        # infos_application_id
        self.infos_application_id.setValeursApplicationParDefaut() 

    def setValeursApplicationParDefautAvecDico( self, nom_absolu_fichier_config_dicos ) :

        # infos_generales
        self.infos_generales.setValeursStandardAvecDico()

        # infos_application_id
        self.infos_application_id.setValeursApplicationParDefautAvecDico( nom_absolu_fichier_config_dicos )

    def setValeursApplicationWebVpz( self, infos_generales, nom_absolu_fichier_config_web_applis, application_name, nom_absolu_fichier_config_dicos ) :

        # infos_generales
        self.infos_generales = infos_generales

        # infos_application_id
        self.infos_application_id.setValeursApplicationWebVpz( nom_absolu_fichier_config_web_applis, application_name, nom_absolu_fichier_config_dicos )

    def getInformationsGenerales( self ):
        return self.infos_generales
    def getId( self ):
        return self.infos_application_id 
