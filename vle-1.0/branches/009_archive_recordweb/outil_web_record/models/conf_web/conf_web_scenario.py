#-*- coding:utf-8 -*-

###############################################################################
# File conf_web_scenario.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

import copy

from configs.conf_conf_web import CONF_conf_web
from models.conf_web.transcodage import TranscodageConfWeb as tcw
from models.conf_web.transcodage import TranscodageConfWebScenario as tcws

from models.conf_web.conf_web import TypeSourceConfWebScenario

from models.conf_web.infos_generales import InfosGeneralesAppli
from models.conf_web.infos_generales import InfosGeneralesPageWeb
from models.conf_web.infos_generales import InfosGeneralesPageWeb

from models.conf_web.infos_donnee import InfosDonneeDef

from configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

from configs.conf_vle import CONF_vle # pour version_pyvle

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

###############################################################################
#
# Classes et methodes dediees a la configuration de l'application Web, ie la
# personnalisation de la presentation d'un scenario a l'ecran.
#
# Dans l'outil_web_record, un scenario (fichier vpz) d'un modele record
# (paquet) est presente a l'ecran :
# - soit dans le format de representation par defaut,
# - soit dans un format de representation personnalise (voir ConfWebScenario).
#
###############################################################################

###############################################################################
# Modele : ConfWebScenario
#
# Les informations de configuration de l'application Web
#
# ConfWebScenario est applique a un scenario pour personnaliser la maniere 
# dont celui-ci est presente a l'ecran a l'utilisateur.
#
# ConfWebScenario contient des informations determinant/personnalisant la
# presentation a l'ecran d'un scenario : partie definition (ses pages web de
# definition) et partie resultat (ses pages web de resultat).
#
# Objet sur lequel s'appuie Definition
#
# Le parametre de construction source_conf_web_scenario
# (SourceConfWebScenario) indique le format (cf type_source) dans lequel
# les informations de configuration ont ete renseignees et contient ces
# informations.
# Voir SourceConfWebScenario et TypeSourceConfWebScenario.
#
# L'objet ConfWebScenario est 'invalide' (cf 'invalider', 'isInvalide') si
# son initialisation/construction non effective (not ok)
#
###############################################################################
class ConfWebScenario( object ):

    ###########################################################################
    #
    # Construction a partir de source_conf_web_scenario en fonction de
    # type_source
    #
    ###########################################################################
    def __init__( self, source_conf_web_scenario=None ):

        self.initRaz() # objet vide (mais valide)

        if source_conf_web_scenario == None :
        # fin : ConfWebScenario produit est un objet vide (mais valide)

            t_ecr.message( "ConfWebScenario : ConfWebScenario produit est un objet vide (mais valide)" )

        else : # source_conf_web_scenario != None

            # indicateur initialisation_effective_ok du bon/mauvais deroulement
            # du traitement (pour False, l'objet construit est rendu invalide)
            initialisation_effective_ok = False # par defaut

            type_source = source_conf_web_scenario.getTypeSource()

            if not TypeSourceConfWebScenario.isValueTypeSourceConfWebScenario( type_source ) :
                # initialisation_effective_ok reste False
                pass

            elif TypeSourceConfWebScenario.isValueVide( type_source ) :

                # Construction application vide a partir de infos_generales_appli
                # (plus exactement, finalisation par rapport a initRaz)
                infos_generales = source_conf_web_scenario.infos_generales_appli
                self.infos_generales = infos_generales

                initialisation_effective_ok = True # a ce stade

            elif TypeSourceConfWebScenario.isValueWebVpz( type_source ) :

                # Construction a partir de exp_config_web_applis
                exp_conf_web = source_conf_web_scenario.exp_config_web_applis
                configuration_web = exp_conf_web.configuration_web

                if configuration_web == None : # la configuration s'est mal passee, rejetee
                    t_err.message( "ConfWebScenario : la configuration a partir du fichier vpz de configuration web s'est mal passee, rejetee" )
                    # initialisation_effective_ok reste False
                else :
                    # deepcopy
                    self.infos_generales = copy.deepcopy( configuration_web.infos_generales )
                    self.pages_web_def_names = copy.deepcopy( configuration_web.pages_web_def_names )
                    self.pages_web_def = copy.deepcopy( configuration_web.pages_web_def )
                    self.pages_web_res_names = copy.deepcopy( configuration_web.pages_web_res_names )
                    self.pages_web_res = copy.deepcopy( configuration_web.pages_web_res )

                    initialisation_effective_ok = True # a ce stade

            elif TypeSourceConfWebScenario.isValueParDefaut( type_source ) :
    
                # Construction a partir de exp_scenario
                exp_scenario = source_conf_web_scenario.exp_scenario
                self.initParDefaut( exp_scenario )

                initialisation_effective_ok = True # a ce stade
    
            elif TypeSourceConfWebScenario.isValueParDefautAvecDico( type_source ) :

                # Construction a partir de exp_scenario et exp_config_dicos
                exp_scenario = source_conf_web_scenario.exp_scenario
                exp_config_dicos = source_conf_web_scenario.exp_config_dicos

                # premiere etape (application par defaut hors documentation avec dico)
                self.initParDefaut( exp_scenario )

                # seconde etape (documentation avec dico d'application par defaut)
                exp_config_dicos.documenterPagesWeb( self.pages_web_def, self.pages_web_res )
                initialisation_effective_ok = True # a ce stade

            # else :
                # initialisation_effective_ok reste False

            if initialisation_effective_ok :
    
                # Quelques regles SYSTEMATIQUEMENT appliquees pour combler 
                # d'eventuels vides/non renseignements, comportements par defaut
                self.applicationRegles()
        
                # trace
                t_ecr.trait()
                t_ecr.message( "ConfWebScenario, la configuration a ete effectuee (bon deroulement)." )
                t_ecr.msg( "ConfWebScenario, self.infos_generales : " )
                t_ecr.msg( "titre : " + str(self.infos_generales.titre) )
                t_ecr.msg( "description : " + str(self.infos_generales.description) )
                t_ecr.msg( "help : " + str(self.infos_generales.help) )
                t_ecr.trait()
    
                # initialisation_effective_ok inchange
    
            if not initialisation_effective_ok :
                # objet rendu invalide pour indiquer 
                #  initialisation/construction non effective (not ok)
                self.invalider()
                t_err.message( "ConfWebScenario : ConfWebScenario produit est invalide" )
    
    ###########################################################################
    # Initialisation - raz
    ###########################################################################
    def initRaz( self ) :

        #######################################################################
        # Partie generale de l'application
        #######################################################################
        self.infos_generales = None

        #######################################################################
        # Partie pages web de definition
        #######################################################################

        # pages_web_def_names , liste ordonnee des noms des PageWebDef
        self.pages_web_def_names = []

        # pages_web_def, dict des PageWebDef
        self.pages_web_def = {}

        #######################################################################
        # Partie pages web de resultat
        #######################################################################

        # pages_web_res_names , liste ordonnee des noms des pages web res
        self.pages_web_res_names = []

        # pages_web_res, configuration des pages web res
        self.pages_web_res = {}

    ###########################################################################
    # Rendre invalide (mettre tous les champs a None)
    ###########################################################################
    def invalider( self ) :
        self.infos_generales = None
        self.pages_web_def_names = None
        self.pages_web_def = None
        self.pages_web_res_names = None
        self.pages_web_res = None

    ###########################################################################
    # indique si invalide  (tous les champs a None)
    # remarque : un objet issu de initRaz est vide mais valide
    ###########################################################################
    def isInvalide( self ) :
        if self.infos_generales==None and self.pages_web_def_names==None and self.pages_web_def==None and self.pages_web_res_names==None and self.pages_web_res==None :
            return True
        else :
            return False

    ###########################################################################
    #
    # Initialisation de ConfWebScenario par defaut,
    # en partie a partir de exp (fichier vpz du scenario)
    #
    # (correspond a l'application standard)
    #
    # Avertissement : le traitement d'initialisation par defaut initParDefaut
    # n'est pas complet/autonome. Il est ecrit en complementarite (prealable)
    # du traitement applicationRegles, partant du principe qu'un appel de
    # initParDefaut est toujours suivi d'un appel de applicationRegles.
    #
    # Le traitement initParDefaut ne renseigne/traite pas web_name : pour les
    # besoins du cas d'initialisation par defaut avec dico (cf appel de
    # documenterPagesWeb intercale entre initParDefaut et applicationRegles),
    # cette tache est cedee/reportee a applicationRegles.
    #
    # Le traitement initParDefaut renseigne ou non web_dimension, web_mode et
    # certaines doc en fonction de la donnee dont il s'agit.
    #
    ###########################################################################
    def initParDefaut( self, exp ) :

        self.initRaz() # raz

        #######################################################################
        # partie generale de l'application (self.infos_generales)
        #######################################################################

        self.infos_generales = InfosGeneralesAppli()
        self.infos_generales.setValeursStandard()

        #######################################################################
        # partie pages web de definition
        # (self.pages_web_def_names, self.pages_web_def)
        #
        # Creation/definition des pages web def :
        #
        # - Une PageWebDef pour chaque condition du scenario
        #   (contenant ses parametres/ports)
        #
        # - Une PageWebDef pour le plan (replica seed et number)
        #
        # - Une PageWebDef pour l'ensemble des autres informations
        #   accessibles (debut simulation, duree...)
        #
        #######################################################################

        # liste des conditions du scenario
        liste_conditions_scn = exp.listConditions() 

        #######################################################################
        # Page 'experience' pour 'l'ensemble des autres informations
        # accessibles' : debut simulation, duree...
        #######################################################################

        nom_page = "experience"
        self.pages_web_def_names.append( nom_page )

        page_web_def = PageWebDef() # la page

        # page_web_def.infos_generales : 
        page_web_def.infos_generales.setValeursStandardPageExperience()

        # page_web_def.liste_donnees :
        # (applicationRegles renseignera web_dimension,
        # web_mode et web_name et certaines doc)

        infos_donnee_def = InfosDonneeDef()
        infos_donnee_def.setVpzType( tcws.CONST_vpz_type_exp_begin )
        page_web_def.liste_donnees.append( infos_donnee_def )

        infos_donnee_def = InfosDonneeDef()
        infos_donnee_def.setVpzType( tcws.CONST_vpz_type_exp_duration )
        page_web_def.liste_donnees.append( infos_donnee_def )

        infos_donnee_def = InfosDonneeDef()
        infos_donnee_def.setVpzType( tcws.CONST_vpz_type_exp_name )
        page_web_def.liste_donnees.append( infos_donnee_def )

        infos_donnee_def = InfosDonneeDef()
        infos_donnee_def.setVpzType( tcws.CONST_vpz_type_simu_seed )
        page_web_def.liste_donnees.append( infos_donnee_def )

        # ajout de la page aux pages_web_def 
        self.pages_web_def[ nom_page ] = page_web_def

        #######################################################################
        # Page 'Plan' pour le plan (replica seed et number)
        #######################################################################

        nom_page = "plan"
        self.pages_web_def_names.append( nom_page )

        page_web_def = PageWebDef() # la page

        # page_web_def.infos_generales : 
        page_web_def.infos_generales.setValeursStandardPagePlan()

        # donnee : dict de chaque donnee
        # (applicationRegles renseignera web_dimension, web_mode et web_name et certaines doc)

        infos_donnee_def = InfosDonneeDef()
        infos_donnee_def.setVpzType( tcws.CONST_vpz_type_plan_seed )
        page_web_def.liste_donnees.append( infos_donnee_def )

        infos_donnee_def = InfosDonneeDef()
        infos_donnee_def.setVpzType( tcws.CONST_vpz_type_plan_number )
        page_web_def.liste_donnees.append( infos_donnee_def )

        # ajout de la page aux pages_web_def 
        self.pages_web_def[ nom_page ] = page_web_def

        #######################################################################
        # Une page web def pour chaque condition du scenario
        # (contenant ses parametres/ports)
        #######################################################################
        for condition in liste_conditions_scn :

            nom_page = "__condition__" + condition
            self.pages_web_def_names.append( nom_page )

            page_web_def = PageWebDef() # la page de la condition

            # page_web_def.infos_generales : 
            page_web_def.infos_generales.setValeursStandardPageCondition()
            # prefixe de titre transforme en titre :
            titre = page_web_def.infos_generales.getTitre() +' '+condition
            page_web_def.infos_generales.setTitre( titre )

            # Parametres
            for port in exp.listConditionPorts( condition ) :
                # parametre : dict de chaque parametre/port de condition

                parametre = InfosDonneeDef()

                # web_conf
                parametre.setWebDimension( tcws.CONST_web_dimension_variable )
                parametre.setWebMode( tcws.CONST_web_mode_read_write )
                # web_name non defini ici mais dans applicationRegles
                # web_name=... parametre.setWebName( web_name )

                # vpz_id
                parametre.setVpzType( tcws.CONST_vpz_type_condition_port )
                parametre.setVpzCondName( condition )
                parametre.setVpzPortName( port )

                page_web_def.liste_donnees.append( parametre )

            # ajout de la page de la condition aux pages_web_def 
            self.pages_web_def[ nom_page ] = page_web_def

        #######################################################################
        # partie pages web de resultat
        # (self.pages_web_res_names, self.pages_web_res)
        #
        # Creation/definition des pages web res :
        #
        # - Une page web def pour chaque view du scenario
        #   (contenant ses variables/ports observes)
        #
        #######################################################################

        self.pages_web_res_names = []
        self.pages_web_res = {}

        # liste des vues/views du scenario
        liste_views_scn = exp.listViews() 

        for view in liste_views_scn :

            if view == None :
                t_err.message( "initParDefaut : erreur view None" )

            nom_page = "__vue__" + view
            self.pages_web_res_names.append( nom_page )

            page_web_res = PageWebRes() # page web res de la view
            
            # info_generale : dict de l'info propre a la page entiere 
            page_web_res.infos_generales.setValeursStandardPageWebRes()
            # prefixe de titre transforme en titre :
            titre = page_web_res.infos_generales.getTitre() +' '+view
            page_web_res.infos_generales.setTitre( titre )

            # vpz_view_name
            page_web_res.vpz_view_name = view

            # donnees_documentees
            page_web_res.donnees_documentees = {} # vide

            # trace
            t_ecr.trait()
            t_ecr.msg( "initParDefaut, page_web_res : " )
            t_ecr.msg( "titre               : " + str(page_web_res.infos_generales.titre) )
            t_ecr.msg( "description         : " + str(page_web_res.infos_generales.description) )
            t_ecr.msg( "help                : " + str(page_web_res.infos_generales.help) )
            t_ecr.msg( "vpz_view_name       : " + str(page_web_res.vpz_view_name) )
            t_ecr.msg( "donnees_documentees : " + str(page_web_res.donnees_documentees) )

            # ajout de la page de la view aux pages_web_res 
            self.pages_web_res[ nom_page ] = page_web_res

    ###########################################################################
    #
    # Application a ConfWebScenario de quelques regles
    # pour combler d'eventuels vides/non renseignements
    # comportements par defaut
    # Normalisation des valeurs
    # Empecher/contourner des choix invalides (impossibles, non geres...)
    #
    # Remarque : ConfWebScenario est aussi susceptible d'etre transforme dans
    # assuranceCompatibilite
    #
    ###########################################################################
    def applicationRegles( self ) :

        #######################################################################
        #
        # Utilitaires
        #
        #######################################################################

        #######################################################################
        # tri
        #######################################################################
        def parOrdreDePreference( v1, v2, v3 ):
            if not tcw.valeurVide( v1 ) :
                res = v1
            elif not tcw.valeurVide( v2 ) :
                res = v2
            else :
                res = v3
            return res

        #######################################################################
        # Determine et retourne la valeur de web_name definie par defaut pour
        # une donnee de vpz_type valant CONST_vpz_type_condition_port.
        # web_name est construit a partir de noms de condition et port
        #######################################################################
        def valeurWebNameParDefautVpzTypeConditionPort( cond, port ) :
            web_name = CONF_conf_web.TXTSTANDARD_prefixe_web_name_page_condition +' '+ port +' (de '+ cond +' )'
            return web_name

        #######################################################################
        #
        # Traitement
        #
        #######################################################################

        # Partie generale de l'application
        self.infos_generales.setValeursDefautUneParUne() 

        #######################################################################
        # Partie des pages web de definition
        #######################################################################
    
        # pages_web_def_names
        # rien de plus n'est fait
    
        # pages_web_def
        for page_web_def_name,page_web_def in self.pages_web_def.iteritems() :

            # l'info (infos_generales) propre a la page entiere 
            page_web_def.infos_generales.setValeursDefautUneParUne() 

            for d,infos_donnee_def in enumerate( page_web_def.liste_donnees ) :

                doc_donnee = infos_donnee_def.doc

                ###############################################################
                # web_conf
                ###############################################################

                # web_dimension :
                # Normalisation.
                # Si sans web_dimension alors par defaut :
                # web_dimension_variable pour une donnee de vpz_type
                # type-condition-port, web_dimension_fixe pour tout autre cas.
                web_dimension = infos_donnee_def.getWebDimension()
                if tcw.valeurVide( web_dimension ) :
                    vpz_type = infos_donnee_def.getVpzType() 
                    if tcws.isTypeConditionPortVpzType(vpz_type) :
                        web_dimension = tcws.CONST_web_dimension_variable
                    else :
                        web_dimension = tcws.CONST_web_dimension_fixe
                infos_donnee_def.setWebDimension( web_dimension )

                # web_dimension :
                # Toutes les valeurs de web_dimension n'etant pas
                # valides/possibles pour toutes les donnees
                # (web_dimension_variable n'est possible que pour une donnee
                # de vpz_type type-condition-port), web_dimension est FORCE a
                # web_dimension_fixe pour toute donnee de vpz_type autre que
                # type-condition-port
                web_dimension = infos_donnee_def.getWebDimension()
                vpz_type = infos_donnee_def.getVpzType() 
                if not tcws.isTypeConditionPortVpzType(vpz_type) :
                    if not tcws.isFixeDimension(web_dimension) :
                        web_dimension = tcws.CONST_web_dimension_fixe
                infos_donnee_def.setWebDimension( web_dimension )

                # web_name :
                # Normalisation.
                # Si sans web_name alors par defaut, par ordre de priorite :
                # nom_frenchname de doc, nom_englishname de doc,
                # nom web_name construit ici a partir de vpz_id (web_name_defaut)
                web_name = infos_donnee_def.getWebName()
                if tcw.valeurVide( web_name ) :

                    nom_frenchname = doc_donnee.getNomFrenchname()
                    nom_englishname = doc_donnee.getNomEnglishname()
                    vpz_type = infos_donnee_def.getVpzType()

                    web_name = tcw.CONST_valeur_vide # par defaut
                    web_name_defaut = tcw.CONST_valeur_vide # par defaut
                    if vpz_type == tcws.CONST_vpz_type_exp_name :
                        web_name_defaut = CONF_conf_web.TXTDEFAUT_web_name_donnee_exp_name 
                    elif vpz_type == tcws.CONST_vpz_type_exp_duration :
                        web_name_defaut = CONF_conf_web.TXTDEFAUT_web_name_donnee_exp_duration 
                    elif vpz_type == tcws.CONST_vpz_type_exp_begin :
                        web_name_defaut = CONF_conf_web.TXTDEFAUT_web_name_donnee_exp_begin 
                    elif vpz_type == tcws.CONST_vpz_type_simu_seed :
                        web_name_defaut = CONF_conf_web.TXTDEFAUT_web_name_donnee_simu_seed 
                    elif vpz_type == tcws.CONST_vpz_type_plan_seed :
                        web_name_defaut = CONF_conf_web.TXTDEFAUT_web_name_donnee_plan_seed 
                    elif vpz_type == tcws.CONST_vpz_type_plan_number :
                        web_name_defaut = CONF_conf_web.TXTDEFAUT_web_name_donnee_plan_number 
                    elif vpz_type == tcws.CONST_vpz_type_condition_port :
                        cond = infos_donnee_def.getVpzCondName()
                        port = infos_donnee_def.getVpzPortName()
                        web_name_defaut = valeurWebNameParDefautVpzTypeConditionPort( cond, port )
                    else :
                        web_name_defaut = CONF_conf_web.TXTDEFAUT_web_name 
                    web_name = parOrdreDePreference( nom_frenchname, nom_englishname, web_name_defaut )

                #print "---------------------------------------"
                #print "vpz_type = ", vpz_type
                #print "nom_frenchname = ", nom_frenchname
                #print "nom_englishname = ", nom_englishname
                #print "web_name_defaut = ", web_name_defaut, " (attention, pas toujours/forcement a jour)"
                #print "web_name = ", web_name
                #print "---------------------------------------"

                infos_donnee_def.setWebName( web_name )

                # web_mode
                # Si sans web_mode alors par defaut : web_mode_hidden si
                # vpz_type invalide, et sinon web_mode_read_write
                web_mode = infos_donnee_def.getWebMode()
                if tcw.valeurVide(web_mode) :
                    if tcw.valeurVide( infos_donnee_def.getVpzType() ) :
                        web_mode = tcws.CONST_web_mode_hidden
                    else :
                        web_mode = tcws.CONST_web_mode_read_write
                infos_donnee_def.setWebMode(web_mode)

                ###############################################################
                # vpz_id
                ###############################################################

                # vpz_type
                vpz_type = infos_donnee_def.getVpzType()
                infos_donnee_def.setVpzType(vpz_type)

                # vpz_cond_name
                vpz_cond_name = infos_donnee_def.getVpzCondName()
                infos_donnee_def.setVpzCondName(vpz_cond_name)

                # vpz_port_name
                vpz_port_name = infos_donnee_def.getVpzPortName()
                infos_donnee_def.setVpzPortName(vpz_port_name)

                ###############################################################
                # doc
                ###############################################################
                doc_donnee.setRegleValeursParDefaut()

        #######################################################################
        # Partie des pages web de resultat
        #######################################################################
    
        # pages_web_res_names
        # rien de plus n'est fait
    
        # pages_web_res
        for page_web_res_name,page_web_res in self.pages_web_res.iteritems() :

            # l'info (infos_generales) propre a la page entiere 
            page_web_res.infos_generales.setValeursDefautUneParUne() 

            ###################################################################
            # la vue/view
            ###################################################################
            # rien de plus n'est fait
            # page_web_res.vpz_view_name

            ###################################################################
            # les donnees documentees
            ###################################################################
            for nom_donnee,doc_donnee in page_web_res.donnees_documentees.iteritems() :
                doc_donnee.setRegleValeursParDefaut()
    
    ###########################################################################
    # assuranceCompatibilite :
    #
    # Controle la compatibilite de ConfWebScenario avec le scenario (exp)
    # auquel elle est sensee etre appliquee. Effectue sur ConfWebScenario
    # certaines transformations/modifications pour obtenir la compatibilite.
    # Retourne True si compatibilite au final (apres eventuelles
    # modifications) et False sinon.
    #
    # Les cas/verifications/modifications geres :
    #
    # I. Verification de l'existence dans exp des conditions et ports 
    # correspondant aux donnees de type-condition-port de ConfWebScenario
    # => transformation si ce n'est pas le cas :
    # zzzz !!!! a ecrire
    #
    # II. Verification de l'existence dans exp des views
    # correspondant aux pages web res
    # => transformation si ce n'est pas le cas :
    # zzzz !!!! a ecrire
    #
    # III. Verification du type_value des donnees a afficher/saisir :
    #
    # Verifie qu'il n'y a pas de donnee a afficher/saisir qui soit de
    # type_value nul/none. 
    #
    # *** debut version/avec pyvle-1.0.1 :
    #
    # => transformation si ce n'est pas le cas :
    # Pour une donnee dont web_mode vaut 'read_write' ou 'read_only' les
    # seuls types permis/possibles pour type_value sont : boolean, double,
    # integer, string, map, set. Dans tout autre cas, web_mode est mis/force
    # a 'hidden'.
    #
    # Observations pour memo (15/12/2011) avec pyvle-1.0.1 :
    #
    #   Lors de la saisie de valeurs de parametres (ports de conditions), gvle 
    #   propose les types : Boolean, Double, Integer, String, Map, Set, 
    #                       Table, Tuple, Matrix, Xml, Null
    #   Limitation vle : Sous gvle, impossible de donner/garder le type Null a
    #   un parametre (port de condition) d'un fichier vpz
    #
    #   Correspondance entre (A) les types choisis a la saisie (sous gvle) et
    #   (B) les types retournes par getConditionValueType (de pyvle) :
    #    (A)      (B)
    #   Boolean  boolean
    #   Double   double
    #   Integer  integer
    #   String   string
    #   Map      map
    #   Set      set
    #   Table    none
    #   Tuple    none
    #   Matrix   none
    #   Xml      none
    #   Null
    #
    #   Limitation pyvle : la methode getConditionSetValue de pyvle ne permet
    #   pas de lire les valeurs d'un parametre qui sont de type : none
    #   (types qu'il est permis/possible de lire :
    #   boolean, double, integer, string, map, set)
    #
    #   Remarque : le fait que la methode
    #   setConditionValue(cond, port, v, type_value, i) de pyvle ne puisse pas
    #   etre appliquee/utilisee pour une valeur de type map ou set est traite
    #   dans/au niveau de la methode tenterEcrireValeur (exp;py)
    #
    # *** fin avec pyvle-1.0.1
    #
    # *** debut version/avec pyvle-1.0.2 + patch tmp...!!!... :   
    #
    # => transformation si ce n'est pas le cas :
    # Pour une donnee dont web_mode vaut 'read_write' ou 'read_only', si 
    # type_value n'a pas une des valeurs permises/possibles (sauf nul/none),
    # alors web_mode est mis/force a 'hidden'.
    #
    # Observations pour memo avec pyvle-1.0.2 + patch tmp...!!!... :   
    #
    #   Limitation vle : Sous gvle, impossible de donner/garder le type Null a
    #   un parametre (port de condition) d'un fichier vpz
    #
    #   Limitation pyvle : la methode getConditionSetValue de pyvle ne permet
    #   pas de lire les valeurs d'un parametre qui sont de type nul/none
    #
    # *** fin avec pyvle-1.0.2 + patch tmp...!!!...
    #
    # Remarque : ConfWebScenario est aussi susceptible d'etre transforme dans
    # applicationRegles
    #
    ###########################################################################
    def assuranceCompatibilite( self, exp ) :

        compatibilite = True
        message = ""

        #######################################################################
        # Verification I.
        #######################################################################

        # liste des conditions du scenario
        liste_conditions_scn = exp.listConditions() 

        for page_web_def_name,page_web_def in self.pages_web_def.iteritems() :
            for donnee in page_web_def.liste_donnees :

                vpz_type = donnee.getVpzType()

                if vpz_type == tcws.CONST_vpz_type_condition_port :

                    cond = donnee.getVpzCondName()
                    port = donnee.getVpzPortName()

                    if cond in liste_conditions_scn :
                        if port in exp.listConditionPorts( cond ) :
                            pass # ok
                        else : 
                            compatibilite = False
                            message = message+" -- " + " port "+port+" dans condition "+cond + "n'existe pas dans le scenario"
                    else :
                        compatibilite = False
                        message = message +" -- "+ " condition "+cond + "n'existe pas dans le scenario"

        #######################################################################
        # Verification II.
        #######################################################################

        # liste des vues/views du scenario
        liste_views_scn = exp.listViews() 

        for page_web_res_name,page_web_res in self.pages_web_res.iteritems() :
            # iiiiiiiiiiii 
            view = page_web_res.vpz_view_name

            # zzzz !!!!!! view = self.getVpzViewName(donnee)

            if view != tcw.CONST_valeur_vide :
                if view in liste_views_scn :
                    pass # ok
                elif view == None :
                    compatibilite = False
                    message = message + " view None"
                else :
                    compatibilite = False
                    message = message+" -- "+ " view "+view + "n'existe pas dans le scenario"


        #######################################################################
        # Verification III.
        #######################################################################

        for page_web_def_name,page_web_def in self.pages_web_def.iteritems() :
            for infos_donnee_def in page_web_def.liste_donnees :

                vpz_type = infos_donnee_def.getVpzType()
                if tcws.isTypeConditionPortVpzType(vpz_type) :

                    web_mode = infos_donnee_def.getWebMode()
                    if tcws.isReadWriteMode(web_mode) or tcws.isReadOnlyMode(web_mode) :

                        cond = infos_donnee_def.getVpzCondName()
                        port = infos_donnee_def.getVpzPortName()
                        type_value = exp.get_type_value(cond,port)

                        if CONF_vle.is_pyvle101() :
                            if type_value not in ('boolean', 'integer', 'double', 'string', 'map', 'set' ) : # pyvle101

                                web_mode = tcws.CONST_web_mode_hidden # forcage
                                infos_donnee_def.setWebMode(web_mode)
                                message = message+" -- " + " parametre (" +cond+ "," +port+ ") force a hidden a cause de son type (" + str(type_value) + ")"

                        elif CONF_vle.is_pyvle102() :

                            if not exp.is_type_value_not_nul( type_value ) : # pyvle102
                                web_mode = tcws.CONST_web_mode_hidden # forcage
                                infos_donnee_def.setWebMode(web_mode)
                                message = message+" -- " + " parametre (" +cond+ "," +port+ ") force a hidden a cause de son type (" + str(type_value) + ")"

                        else :
                            CONF_vle.message_erreur_version_pyvle(t_err)

        return ( compatibilite, message )

    ###########################################################################
    #
    # Trace ecran
    #
    ###########################################################################
    def traceConfWebScenario( self ):

        deco = "##################################"
        t_ecr.saut_de_ligne()
        t_ecr.msg( deco )
        t_ecr.msg( "# CONFIGURATION ConfWebScenario" )
        t_ecr.msg( deco )
        t_ecr.saut_de_ligne()
        t_ecr.msg( deco )
        t_ecr.msg( "Partie generale de configuration de l'application :" )
        t_ecr.msg( "titre : " + str(self.infos_generales.titre) )
        t_ecr.msg( "description : " + str(self.infos_generales.description) )
        t_ecr.msg( "help : " + str(self.infos_generales.help) )
        t_ecr.saut_de_ligne()
        t_ecr.msg( deco )
        t_ecr.msg( "Liste ORDONNEE des noms des pages web de definition : " + str(self.pages_web_def_names) )
        t_ecr.saut_de_ligne()
        t_ecr.msg( deco )
        t_ecr.msg( "Partie configuration des pages web de definition :" ) 

        for (page_web_def_name,page_web_def) in self.pages_web_def.iteritems() :
            t_ecr.saut_de_ligne()
            t_ecr.msg( "Page web def " + str(page_web_def_name) + " :" )
            t_ecr.msg( "titre : " + str(page_web_def.infos_generales.titre) )
            t_ecr.msg( "description : " + str(page_web_def.infos_generales.description) )
            t_ecr.msg( "help : " + str(page_web_def.infos_generales.help) )
            t_ecr.msg( "Donnees de la page :" )
            for i,donnee in enumerate( page_web_def.liste_donnees) :
                t_ecr.msg( "Donnees de la page :" + str(donnee) )
                t_ecr.msg( "web dimension :" + str(donnee.getWebDimension()) )
                t_ecr.msg( "web mode : " + str(donnee.getWebMode()) )
                t_ecr.msg( "web name : " + str(donnee.getWebName()) )
                t_ecr.msg( "vpz type : " + str(donnee.getVpzType()) )
                t_ecr.msg( "vpz cond et port names : " + str(donnee.getVpzCondName()) + " , " + str(donnee.getVpzPortName()) )
                t_ecr.msg( "nom frenchname : " + str(donnee.doc.getNomFrenchname()) )
                t_ecr.msg( "nom englishname : " + str(donnee.doc.getNomEnglishname()) )
                t_ecr.msg( "description : " + str(donnee.doc.getDescription()) )
                t_ecr.msg( "unite : " + str(donnee.doc.getUnite()) )
                t_ecr.msg( "val max : " + str(donnee.doc.getValMax()) )
                t_ecr.msg( "val min : " + str(donnee.doc.getValMin()) )
                t_ecr.msg( "val recommandee : " + str(donnee.doc.getValRecommandee()) )
                t_ecr.msg( "help : " + str(donnee.doc.getHelp()) )

        t_ecr.saut_de_ligne()
        t_ecr.msg( deco )
        t_ecr.msg( "Liste ORDONNEE des noms des pages web de resultat : " + str(self.pages_web_res_names) )
        t_ecr.saut_de_ligne()
        t_ecr.msg( deco )
        t_ecr.msg( "Partie configuration des pages web de resultat :" ) 

        for (page_web_res_name,page_web_res) in self.pages_web_res.iteritems() :
            t_ecr.saut_de_ligne()
            t_ecr.msg( "Page web res " + str(page_web_res_name) + " :" )
            t_ecr.msg( "titre : " + str(page_web_res.infos_generales.titre) )
            t_ecr.msg( "description : " + str(page_web_res.infos_generales.description) )
            t_ecr.msg( "help : " + str(page_web_res.infos_generales.help) )
            t_ecr.msg( "vpz_view_name : " + str(page_web_res.vpz_view_name) )
            t_ecr.msg( "donnees_documentees : " + str(page_web_res.donnees_documentees) )
        t_ecr.msg( deco )

###############################################################################
#
#
# Des classes de composants de la ConfWebScenario
#
# (voir aussi infos_generales, infos_donnee)
#
###############################################################################

###############################################################################
#
# Informations d'une page web de definition
#
###############################################################################
class PageWebDef(object) :

    def __init__( self ):

        # info_generale : l'info propre a la page entiere 
        self.infos_generales = InfosGeneralesPageWeb()

        # liste des donnees InfosDonneeDef de la page
        self.liste_donnees = []

###############################################################################
#
# Informations d'une page web de resultat
#
###############################################################################
class PageWebRes(object) :

    def __init__( self ):

        # info_generale : l'info propre a la page entiere 
        self.infos_generales = InfosGeneralesPageWeb()

        # nom de la view/vue
        self.vpz_view_name = None

        # dict des donnees documentees qui pourront etre sollicitees
        # au moment d'afficher la page (les donnees de la view/vue)
        # donnees_documentees est un dict, peut-etre vide
        # donnees_documentees ne contient pas forcement toutes, ni que, des
        # donnees appartenant a la view/vue
        self.donnees_documentees = {}

