#-*- coding:utf-8 -*-

###############################################################################
# File infos_donnee.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from configs.conf_conf_web import CONF_conf_web
from models.conf_web.transcodage import TranscodageConfWeb as tcw
from models.conf_web.transcodage import TranscodageConfWebScenario as tcws

from configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

###############################################################################
#
#
# Classes et methodes dediees a la configuration web :
# des classes de composants de la ConfWebScenario, plus particulierement
# des classes relatives/propres aux informations d'une donnee
# (donnee d'une page web de definition, d'une page web de resultat)
#
#
###############################################################################


###############################################################################
#
# Documentation d'une donnee d'une page
# (intervient dans PageWebDef, PageWebRes)
#
###############################################################################
class DocDonnee(object) :

    def __init__( self ) :
        self.nom_frenchname = None
        self.nom_englishname = None
        self.description = None
        self.unite = None
        self.val_max = None
        self.val_min = None
        self.val_recommandee = None
        self.help = None

    # retourne la valeur (normalisee) de nom_frenchname 
    def getNomFrenchname( self ) :
        nom_frenchname = tcw.CONST_valeur_vide # par defaut
        v = self.nom_frenchname 
        if type(v) == str :
            nom_frenchname = v
        return nom_frenchname

    def setNomFrenchname( self, v ) :
        self.nom_frenchname = v

    # retourne la valeur (normalisee) de nom_englishname 
    def getNomEnglishname( self ) :
        nom_englishname = tcw.CONST_valeur_vide # par defaut
        v = self.nom_englishname 
        if type(v) == str :
            nom_englishname = v
        return nom_englishname

    def setNomEnglishname( self, v ) :
        self.nom_englishname = v

    # retourne la valeur (normalisee) de description 
    def getDescription( self ) :
        description = tcw.CONST_valeur_vide # par defaut
        v = self.description 
        if type(v) == str :
            description = v
        return description

    def setDescription( self, v ) :
        self.description = v

    # retourne la valeur (normalisee) de unite 
    def getUnite( self ) :
        unite = tcw.CONST_valeur_vide # par defaut
        v = self.unite 
        if type(v) == str :
            unite = v
        return unite

    def setUnite( self, v ) :
        self.unite = v

    # retourne la valeur (normalisee) de val_max 
    def getValMax( self ) :
        val_max = tcw.CONST_valeur_vide # par defaut
        v = self.val_max 
        if type(v) == str :
            val_max = v
        return val_max
        # remplace ancien code (valeur numerique)
        #val_max = None # par defaut
        #v = self.val_max 
        #t = type(v)
        #if (t==int) or (t==long) or (t==float) : # type numerique
        #    val_max = v
        #return val_max

    def setValMax( self, v ) :
        self.val_max = v

    # retourne la valeur (normalisee) de val_min 
    def getValMin( self ) :
        val_min = tcw.CONST_valeur_vide # par defaut
        v = self.val_min 
        if type(v) == str :
            val_min = v
        return val_min
        # remplace ancien code (valeur numerique)
        #val_min = None # par defaut
        #v = self.val_min 
        #t = type(v)
        #if (t==int) or (t==long) or (t==float) : # type numerique
        #    val_min = v
        #return val_min

    def setValMin( self, v ) :
        self.val_min = v

    # retourne la valeur (normalisee) de val_recommandee 
    def getValRecommandee( self ) :
        val_recommandee = tcw.CONST_valeur_vide # par defaut
        v = self.val_recommandee 
        if type(v) == str :
            val_recommandee = v
        return val_recommandee
        # remplace ancien code (valeur numerique)
        #val_recommandee = None # par defaut
        #v = self.val_recommandee 
        #t = type(v)
        #if (t==int) or (t==long) or (t==float) : # type numerique
        #    val_recommandee = v
        #return val_recommandee

    def setValRecommandee( self, v ) :
        self.val_recommandee = v

    # retourne la valeur (normalisee) de help 
    def getHelp( self ) :
        help = tcw.CONST_valeur_vide # par defaut
        v = self.help 
        if type(v) == str :
            help = v
        return help

    def setHelp( self, v ) :
        self.help = v

    ###########################################################################
    # Donne une valeur par defaut a chaque attribut 'vide'
    ###########################################################################
    def setRegleValeursParDefaut( self ) :

        nom_frenchname = self.getNomFrenchname()
        if tcw.valeurVide( nom_frenchname ) :
            nom_frenchname = ''
        self.setNomFrenchname( nom_frenchname )

        nom_englishname = self.getNomEnglishname()
        if tcw.valeurVide( nom_englishname ) :
            nom_englishname = ''
        self.setNomEnglishname( nom_englishname )

        description = self.getDescription()
        if tcw.valeurVide( description ) :
            description = ''
        self.setDescription( description )

        unite = self.getUnite()
        if tcw.valeurVide( unite ) :
            unite = ''
        self.setUnite( unite )

        val_max = self.getValMax()
        if tcw.valeurVide( val_max ) :
            val_max = ''
            # remplace ancien code (valeur numerique)
            #val_max = None
        self.setValMax( val_max )

        val_min = self.getValMin()
        if tcw.valeurVide( val_min ) :
            val_min = ''
            # remplace ancien code (valeur numerique)
            #val_min = None
        self.setValMin( val_min )

        val_recommandee = self.getValRecommandee()
        if tcw.valeurVide( val_recommandee ) :
            val_recommandee = ''
            # remplace ancien code (valeur numerique)
            #val_recommandee = None
        self.setValRecommandee( val_recommandee )

        help = self.getHelp()
        if tcw.valeurVide( help ) :
            help = ''
        self.setHelp( help )

###############################################################################
#
# Informations d'une donnee d'une page web de definition
#
###############################################################################

class InfosWebConf(object) :

    def __init__( self ):
        self.web_dimension = None
        self.web_name = None
        self.web_mode = None

    # retourne la valeur (normalisee) de web_dimension
    def getWebDimension( self ) :
        web_dimension = tcw.CONST_valeur_vide # par defaut
        v = self.web_dimension
        if type(v) == str :
            web_dimension = v
        return ( tcws.getStdWebDimension(web_dimension) )

    def setWebDimension( self, v ) :
        self.web_dimension = v

    def getWebName( self ) :
        web_name = tcw.CONST_valeur_vide # par defaut
        v = self.web_name
        if type(v) == str :
            web_name = v
        return web_name

    def setWebName( self, v ) :
        self.web_name = v

    # retourne la valeur (normalisee) de web_mode 
    def getWebMode( self ) :
        web_mode = tcw.CONST_valeur_vide # par defaut
        v = self.web_mode
        if type(v) == str :
            web_mode = v
        return ( tcws.getStdWebMode(web_mode) )

    def setWebMode( self, v ) :
        self.web_mode = v

    def setValeurs( self, web_dimension, web_name, web_mode ) :
        self.setWebDimension( web_dimension )
        self.setWebMode( web_mode )
        self.setWebName( web_name )

class InfosVpzId(object) :

    def __init__( self ):
        self.vpz_type = None
        self.vpz_cond_name = None
        self.vpz_port_name = None

    # retourne la valeur (normalisee) de vpz_type
    def getVpzType( self ) :
        vpz_type = tcw.CONST_valeur_vide # par defaut
        v = self.vpz_type
        if type(v) == str :
            vpz_type = v
        return ( tcws.getStdVpzType(vpz_type) )

    def setVpzType( self, v ) :
        self.vpz_type = v

    # retourne la valeur (normalisee) de vpz_cond_name
    def getVpzCondName( self ) :
        vpz_cond_name = tcw.CONST_valeur_vide # par defaut
        v = self.vpz_cond_name
        if type(v) == str :
            vpz_cond_name = v
        return vpz_cond_name

    def setVpzCondName( self, v ) :
        self.vpz_cond_name = v

    # retourne la valeur (normalisee) de vpz_port_name 
    def getVpzPortName( self ) :
        vpz_port_name = tcw.CONST_valeur_vide # par defaut
        v = self.vpz_port_name
        if type(v) == str :
            vpz_port_name = v
        return vpz_port_name

    def setVpzPortName( self, v ) :
        self.vpz_port_name = v

    def setValeurs( self, vpz_type, vpz_cond_name=None, vpz_port_name=None ) :
        self.setVpzType( vpz_type )
        if vpz_cond_name != None :
            self.setVpzCondName( vpz_cond_name )
        if vpz_port_name != None :
            self.setVpzPortName( vpz_port_name )

class InfosDonneeDef(object) :

    def __init__( self ):
        self.web_conf = InfosWebConf()
        self.vpz_id = InfosVpzId()
        self.doc = DocDonnee()

    def getWebDimension( self ) :
        return ( self.web_conf.getWebDimension() )
    def setWebDimension( self, v ) :
        self.web_conf.setWebDimension(v)

    def getWebMode( self ) :
        return ( self.web_conf.getWebMode() )
    def setWebMode( self, v ) :
        self.web_conf.setWebMode(v)

    def getWebName( self ) :
        return ( self.web_conf.getWebName() )
    def setWebName( self, v ) :
        self.web_conf.setWebName( v )

    def getVpzType( self ) :
        return ( self.vpz_id.getVpzType() )
    def setVpzType( self, v ) :
        self.vpz_id.setVpzType( v )

    def getVpzCondName( self ) :
        return self.vpz_id.getVpzCondName()
    def setVpzCondName( self, v ) :
        self.vpz_id.setVpzCondName( v )

    def getVpzPortName( self ) :
        return self.vpz_id.getVpzPortName()
    def setVpzPortName( self, v ) :
        self.vpz_id.setVpzPortName( v )

    def setWebConfValeurs( self, web_dimension, web_name, web_mode ) :
        self.web_conf.setValeurs( web_dimension, web_name, web_mode )

    def setVpzIdValeurs( self, vpz_type, vpz_cond_name=None, vpz_port_name=None ) :
        self.vpz_id.setValeurs( vpz_type, vpz_cond_name, vpz_port_name )

    ###########################################################################
    # determine et retourne un id pour la donnee, en fonction de son vpz_type
    # attention : pseudo id car non unicite ; par exemple cas vpz_type
    # CONST_vpz_type_condition_port : si 2 parametres de 2 conditions
    # differentes ont meme nom de port
    ###########################################################################
    def getIdParDefaut( self ) :

        id_pour_dico = tcw.CONST_valeur_vide # par defaut
        vpz_type = self.getVpzType()
        if vpz_type == tcws.CONST_vpz_type_exp_name :
            id_pour_dico = CONF_conf_web.TXTDEFAUT_id_donnee_exp_name 
        elif vpz_type == tcws.CONST_vpz_type_exp_duration :
            id_pour_dico = CONF_conf_web.TXTDEFAUT_id_donnee_exp_duration 
        elif vpz_type == tcws.CONST_vpz_type_exp_begin :
            id_pour_dico = CONF_conf_web.TXTDEFAUT_id_donnee_exp_begin 
        elif vpz_type == tcws.CONST_vpz_type_simu_seed :
            id_pour_dico = CONF_conf_web.TXTDEFAUT_id_donnee_simu_seed 
        elif vpz_type == tcws.CONST_vpz_type_plan_seed :
            id_pour_dico = CONF_conf_web.TXTDEFAUT_id_donnee_plan_seed 
        elif vpz_type == tcws.CONST_vpz_type_plan_number :
            id_pour_dico = CONF_conf_web.TXTDEFAUT_id_donnee_plan_number 
        elif vpz_type == tcws.CONST_vpz_type_condition_port :
            id_pour_dico = self.getVpzPortName() # nom du port/parametre
        # else : rien

        return id_pour_dico

