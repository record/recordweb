#-*- coding:utf-8 -*-

###############################################################################
# File resultat.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from record.utils import graphique
from record.utils.graphique import Graphique

from configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

###############################################################################
# Resultat
#
# resultat de la simulation d'un scenario : production et restitution.
#
# Resultat concerne une simulation simple ou multiple.
# type_simulation, qui vaut "mono_simulation" ou "multi_simulation", permet
# la distinction entre ces 2 cas.
# De plus en simulation multiple, mode_combinaison indique le mode de
# combinaison applique (lineaire ou total).
#
###############################################################################
class Resultat(object):

    # Construction
    def __init__( self, type_simulation, mode_combinaison="" ):

        # type de simulation/resultat
        # vaut "mono_simulation" ou "multi_simulation"
        self.type_simulation = type_simulation

        # mode de combinaison (n'a de sens qu'en multi_simulation)
        # vaut "mode_lineaire" ou "mode_total", vaut "" pour 'sans objet'
        self.mode_combinaison = mode_combinaison

        # memorisation du nom du dernier fichier de conservation
        # d'une representation graphique sous forme de fichier
        # rq : peut etre different (plus ancien) de rg
        # nom_dernier_fichier_rg : nom du fichier sans tout son chemin
        self.nom_dernier_fichier_rg = None

        # en fonction de type_simulation, res est du type
        # MonoResultat ou MultiResultat
        if self.type_simulation == "multi_simulation" :
            self.res = MultiResultat()
        else : # cas "mono_simulation" et de plus par defaut
            self.res = MonoResultat()

    ###########################################################################
    #
    # SIMULATION
    #
    ###########################################################################

    def simuler( self, exp, espace_exploitation ):

        # Prise en compte du mode de combinaison choisi
        if self.mode_combinaison == "mode_total" :
            replicaSeed = exp.getReplicaSeed()
            replicaNumber = exp.getReplicaNumber()
            exp.setTotalCombination( replicaSeed, replicaNumber )
        elif self.mode_combinaison == "mode_lineaire" :
            replicaSeed = exp.getReplicaSeed()
            replicaNumber = exp.getReplicaNumber()
            exp.setLinearCombination( replicaSeed, replicaNumber )
        else : # cas "" et de plus par defaut
            pass

        # nettoyage/raz prealable ; voir aussi au niveau 
        # de simulerPourFichiers et simulerPourResult
        self.res.rg = None
        self.nom_dernier_fichier_rg = None
        espace_exploitation.viderRepRestitutions()

        # Lance la simulation et sauvegarde fichiers
        self.simulerPourFichiers( exp, espace_exploitation )

        # Lance la simulation afin d'en recuperer result
        # pour affichages et representations graphiques
        self.simulerPourResult( exp )

    ###########################################################################
    # Methode simulerPourFichiers
    #
    # Lance la simulation dans le but de produire les fichiers de sortie,
    # sauvegarde le fichier vpz tel que simule et les resultats produits.
    #
    ###########################################################################
    def simulerPourFichiers( self, exp, espace_exploitation ):

        # Suppression/nettoyage prealable
        # (repertoires exp, output, output_files)
        espace_exploitation.viderRepPkgExp()
        espace_exploitation.viderRepPkgOutput()
        espace_exploitation.supprimerRepOutputPhoto()

        # verification/trace exp
        # v=exp.get_liste_ports_setvalues()
        # t_ecr.message( str(v) )

        # simulation (l'appel depend de type_simulation)
        self.res.lancerSimulation( exp )

        # Sauvegarde fichiers
        espace_exploitation.sauvegarderVpz( exp )
        espace_exploitation.sauvegarderOutput()

    ###########################################################################
    # Methode simulerPourResult
    #
    # Lance la simulation en 'mode storage' afin d'en recuperer le resultat
    # (dans self.res).
    #
    ###########################################################################
    def simulerPourResult( self, exp ):
        # simulation (l'appel depend de type_simulation)
        self.res.simulerPourResult( exp )

    ###########################################################################
    #
    # REPRESENTATION GRAPHIQUE
    #
    ###########################################################################

    ###########################################################################
    # L'initialisation/creation de la representation graphique self.res.rg
    # depend du type_simulation :
    # voir chacune des classes MonoResultat et MultiSimulation
    ###########################################################################

    ###########################################################################
    # Methode afficherRepresentationGraphique
    #
    # Produit la representation graphique pour affichage
    ###########################################################################
    def afficherRepresentationGraphique( self ) :
        image = self.res.rg.produireGraphique()
        buffer = graphique.getImageBuffer( image )
        return buffer

    ###########################################################################
    # Methode enregistrerRepresentationGraphique
    #
    # Produit la representation graphique et l'enregistre dans nom_fichier
    #
    # nom_fichier est le nom absolu du fichier (avec son chemin)
    #
    ###########################################################################
    def enregistrerRepresentationGraphique( self, nom_fichier ) :
        image = self.res.rg.produireGraphique()
        graphique.getImageFile( image, nom_fichier )

    ###########################################################################
    # Methode memoriserNomDernierFichierRepresentationGraphique
    #
    # Memorise/met a jour nom_dernier_fichier_rg
    #
    # Rappel :
    # nom_dernier_fichier_rg est le nom relatif du fichier, sans le chemin
    #
    ###########################################################################
    def memoriserNomDernierFichierRepresentationGraphique( self, nom ) :
        self.nom_dernier_fichier_rg = nom

    ###########################################################################
    # Methode getNomDernierFichierRepresentationGraphique
    #
    # retourne le nom nom_dernier_fichier_rg
    ###########################################################################
    def getNomDernierFichierRepresentationGraphique( self ) :
        return self.nom_dernier_fichier_rg

###############################################################################
# MonoResultat
#
# La partie du resultat de la simulation d'un scenario qui est
# specifique/propre de/a la simulation simple
#
###############################################################################
class MonoResultat(object):

    # Construction
    def __init__( self ):

        #######################################################################
        # l'information brute
        #######################################################################

        # typesCapture : le type de capture des vues
        # dict ( vue : ( type, timestep ) )
        self.typesCapture = None

        # informations resultats de simulation
        # results_par_vues : les variables observees vue par vue
        # dict ( vue : dict( nomvar : valeurs ) )
        self.results_par_vues = None

        #######################################################################
        # l'information transformee/adaptee a exploitation
        #######################################################################

        # vue_vars : dict( vue_typee : dict( nomvar : donneesvar ) )
        self.vue_vars = None

        # type_capture_vars : dict( typage : dict( nomvar : donneesvar ) )
        # sans nom de la vue, et sans doublon
        self.type_capture_vars = None

        #######################################################################
        # autre
        #######################################################################

        # representation graphique rg : les informations de caracterisation
        # de la derniere representation graphique tracee a l'ecran
        self.rg = None

    ###########################################################################
    #
    # REPRESENTATION GRAPHIQUE
    #
    ###########################################################################

    ###########################################################################
    # Methode setRepresentationGraphique
    #
    # Definit rg a partir de liste_XY...
    #
    # Chaque element (X,Y) de liste_XY est de la forme :
    # ( (type,timestep,nomvar), (type,timestep,nomvar) )
    #
    # memeAbscisse indique si la representation graphique est ou non du type ou
    # tous les traces ont la meme variable en abscisse :
    # - cas ou memeAbscisse est True :
    # legendeX et legendeY sont renseignes, les_legendes contient uniquement les Y
    # - cas ou memeAbscisse est False :
    # legendeX et legendeY ne sont pas renseignes, les_legendes contient les Y(X)
    #
    ###########################################################################
    def setRepresentationGraphique( self, liste_XY, titre, memeAbscisse ) :

        # raz
        self.rg = Graphique()

        # le titre
        self.rg.setTitre( titre )

        # les (X,Y)
        les_XY = []
        for ( (typeX,timestepX,nomvarX), (typeY,timestepY,nomvarY) ) in liste_XY :

            cr_ok = True # par defaut

            typageX = ObsTypage(typeX,timestepX)
            typageY = ObsTypage(typeY,timestepY) 

            # element_tcv : valeur de typage correspondant a typageX dans 
            # type_capture_vars : dict( typage : dict( nomvar : donneesvar ) )
            # (vaut None si typageX n'existe pas dans type_capture_vars)
            element_tcv = typageX.getValues( self.type_capture_vars )
            if element_tcv == None :
                cr_ok = False
            elif nomvarX not in element_tcv.keys() :
                cr_ok = False
            else :
                donneesX = element_tcv[ nomvarX ] 

            # element_tcv : valeur de typage correspondant a typageY dans 
            # type_capture_vars : dict( typage : dict( nomvar : donneesvar ) )
            # (vaut None si typageY n'existe pas dans type_capture_vars)
            element_tcv = typageY.getValues( self.type_capture_vars )
            if element_tcv == None :
                cr_ok = False
            elif nomvarY not in element_tcv.keys() :
                cr_ok = False
            else :
                donneesY = element_tcv[ nomvarX ] 

            if cr_ok :
                nomX = nomvarX 
                valeursX = donneesX.valeurs
                nomY = nomvarY 
                valeursY = donneesY.valeurs

                les_XY.append( ((nomX,valeursX),(nomY,valeursY)) )
            else : # erreur
                t_err.message( " MonoResultat/setRepresentationGraphique : X et/ou Y non trouve/s dans type_capture_vars" )

        self.rg.setLesXY( les_XY )

        # les legendes

        legendeX = "" # va rester vide si not memeAbscisse
        legendeY = "" # va rester vide si not memeAbscisse
        les_legendes = []

        for ( (nomX,valeursX), (nomY,valeursY) ) in les_XY :

            # legendeY dans cas memeAbscisse
            if memeAbscisse :
                legendeY = legendeY + getNomRelatifVar( nomY ) + ", "

            # legende
            if memeAbscisse :
                legende = getNomRelatifVar(nomY)
            else :
                legende = getNomRelatifVar(nomY) +"("+ getNomRelatifVar(nomX) +")"
            les_legendes.append( legende )

        # legendeX dans cas memeAbscisse
        if memeAbscisse :
            ( (nomX,valeursX), (nomY,valeursY) ) = les_XY[0]
            legendeX = getNomRelatifVar( nomX )

        # troncature si trop long
        if len(legendeX) > 50 :
            legendeX = legendeX[:50] + "..."
        if len(legendeY) > 50 :
            legendeY = legendeY[:50] + "..."

        self.rg.setLegendes( legendeX, legendeY, les_legendes )

    ###########################################################################
    # Methode lancerSimulation
    #
    # Lance la simulation en tant que simulation simple.
    #
    ###########################################################################
    def lancerSimulation( self, exp ):
        # simulation (simple)

        r = exp.mono_run()

    ###########################################################################
    # Methode simulerPourResult
    #
    # Lance la simulation en 'mode storage' et en recupere le resultat.
    #
    # La simulation est traitee en tant que simulation simple (lancee avec
    # 'run'). 
    #
    ###########################################################################
    def simulerPourResult( self, exp ):

        #######################################################################
        # Methode getVueVars
        # Construit et retourne vue_vars a partir de
        # results_par_vues et typesCapture.
        # vue_vars : dict( vue_typee : dict( nomvar : donneesvar ) )
        #######################################################################
        def getVueVars( results_par_vues, typesCapture ) :
            vue_vars = {}
            for ( vue, dict_nom_valeurs ) in results_par_vues.iteritems() :
                (type_vue,timestep_vue) = typesCapture[vue]
                vue_typee = VueTypee(vue,type_vue,timestep_vue)
                # d : dict( nomvar : donneesvar )
                d = {}
                for (nomvar,valeurs) in dict_nom_valeurs.iteritems() :
                    diminutif = getNomRelatifVar( nomvar )
                    donneesvar = DonneesVar(diminutif,valeurs)
                    d[ nomvar ] = donneesvar
                vue_vars[ vue_typee ] = d
            return vue_vars

        #######################################################################
        # Methode getTypeCaptureVars
        # Construit et retourne type_capture_vars a partir de
        # results_par_vues et typesCapture.
        # type_capture_vars : dict( typage : dict( nomvar : donneesvar ) )
        # sans nom de la vue, sans doublon
        #######################################################################
        def getTypeCaptureVars( results_par_vues, typesCapture ) :

            type_capture_vars = {}

            # balayage des vues
            for ( vue, dict_nom_valeurs ) in results_par_vues.iteritems() :

                (type_vue, timestep_vue) = typesCapture[vue]
                typage = ObsTypage(type_vue,timestep_vue)

                for ( nomvar, valeurs ) in dict_nom_valeurs.iteritems() :

                    # nomvar,donneesvar
                    diminutif = getNomRelatifVar( nomvar )
                    donneesvar = DonneesVar(diminutif,valeurs)

                    # type_capture_vars[typage]
                    d = typage.getValues( type_capture_vars )

                    if d == None : # n'existe pas encore
                        # creation type_capture_vars[typage]
                        type_capture_vars[ typage ] = { nomvar:donneesvar }

                    elif nomvar not in d.keys() : # pas de doublon
                        # ajout nomvar:donneesvar a type_capture_vars[typage]
                        d[ nomvar ] = donneesvar
                    # else : doublon a ne pas prendre en compte
            return type_capture_vars

        #######################################################################
        # Traitement
        #######################################################################

        # nettoyage/raz prealable
        self.typesCapture = None
        self.results_par_vues = None
        self.type_capture_vars = None

        # results_par_vues (obtenus par simulation simple)
        results_par_vues = exp.mono_runModeStorage()

        #print "" # !!!
        #print "(Mono) type(results_par_vues) = ", type(results_par_vues)
        #print "(Mono) results_par_vues = ", results_par_vues
        #print ""

        # typesCapture
        typesCapture = exp.get_types_capture()

        # vue_vars
        vue_vars = getVueVars( results_par_vues, typesCapture )

        # type_capture_vars
        type_capture_vars = getTypeCaptureVars( results_par_vues, typesCapture )
    
        self.typesCapture = typesCapture
        self.results_par_vues = results_par_vues 
        self.vue_vars = vue_vars 
        self.type_capture_vars = type_capture_vars 

###############################################################################
# MultiResultat
#
# La partie du resultat de la simulation d'un scenario qui est
# specifique/propre de/a la simulation multiple
#
# Le scenario est traite en tant que multi-simulation (plan d'experience),
# meme s'il s'agit d'un scenario simple. Ses fichiers de sortie contiennent
# la numerotation (du type '-0-0') d'identification des experiences du plan.
#
###############################################################################
class MultiResultat( MonoResultat ):

    # Construction
    def __init__( self ):

        #######################################################################
        # l'information brute
        #######################################################################

        # typesCapture : le type de capture des vues
        # dict ( vue : ( type, timestep ) )
        self.typesCapture = None

        # informations resultats de simulation
        # results_par_vues : experience par experience, les variables
        # observees vue par vue. Format :
        # tuple de tuple (correspondant aux 2 indices de l'experience) de
        # dict ( vue : dict( nomvar : valeurs ) )
        self.results_par_vues = None

        #######################################################################
        # l'information transformee/adaptee a exploitation
        #######################################################################

        # vue_vars : tuple de tuple de
        # dict( vue_typee : dict( nomvar : donneesvar ) )
        self.vue_vars = None

        # type_capture_vars : tuple de tuple de
        # dict( typage : dict( nomvar : donneesvar ) )
        # sans nom de la vue, et sans doublon
        self.type_capture_vars = None

        #######################################################################
        # autre
        #######################################################################

        # representation graphique rg : les informations de caracterisation
        # de la derniere representation graphique tracee a l'ecran
        self.rg = None

    ###########################################################################
    #
    # REPRESENTATION GRAPHIQUE
    #
    ###########################################################################

    ###############################################################################
    # Methode setRepresentationGraphique
    #
    # Definit rg a partir de liste_XY...
    #
    # Chaque element (X,Y) de liste_XY est de la forme :
    # ( (i1,i2,type,timestep,nomvar), (i1,i2,type,timestep,nomvar) )
    #
    # memeAbscisse indique si la representation graphique est ou non du type ou
    # tous les traces ont la meme variable en abscisse :
    # - cas ou memeAbscisse est True :
    # legendeX et legendeY sont renseignes, les_legendes contient uniquement les Y
    # - cas ou memeAbscisse est False :
    # legendeX et legendeY ne sont pas renseignes, les_legendes contient les Y(X)
    #
    ###############################################################################
    def setRepresentationGraphique( self, liste_XY, titre, memeAbscisse ) :

        # raz
        self.rg = Graphique()

        # le titre
        self.rg.setTitre( titre )

        # les (X,Y)
        les_XY = []
        for ( (i1X,i2X,typeX,timestepX,nomvarX), (i1Y,i2Y,typeY,timestepY,nomvarY) ) in liste_XY :

            cr_ok = True # par defaut

            typageX = ObsTypage(typeX,timestepX)
            typageY = ObsTypage(typeY,timestepY) 

            type_capture_vars_X = self.type_capture_vars[i1X][i2X]
            # element_tcv : valeur de typage correspondant a typageX dans 
            # type_capture_vars_X : dict( typage : dict( nomvar : donneesvar ) )
            # (vaut None si typageX n'existe pas dans type_capture_vars_X)
            element_tcv = typageX.getValues( type_capture_vars_X )
            if element_tcv == None :
                cr_ok = False
            elif nomvarX not in element_tcv.keys() :
                cr_ok = False
            else :
                donneesX = element_tcv[ nomvarX ] 

            type_capture_vars_Y = self.type_capture_vars[i1Y][i2Y]
            # element_tcv : valeur de typage correspondant a typageY dans 
            # type_capture_vars_Y : dict( typage : dict( nomvar : donneesvar ) )
            # (vaut None si typageY n'existe pas dans type_capture_vars_Y)
            element_tcv = typageY.getValues( type_capture_vars_Y )
            if element_tcv == None :
                cr_ok = False
            elif nomvarY not in element_tcv.keys() :
                cr_ok = False
            else :
                donneesY = element_tcv[ nomvarY ] 

            if cr_ok :

                nomX = nomvarX 
                nomY = nomvarY 
                # ajout information i1,i2 dans nom
                nomX = nomX + "-"+str(i1X)+"-"+str(i2X)
                nomY = nomY + "-"+str(i1Y)+"-"+str(i2Y)

                valeursX = donneesX.valeurs
                valeursY = donneesY.valeurs

                les_XY.append( ((nomX,valeursX),(nomY,valeursY)) )
            else : # erreur
                t_err.message( " MultiResultat/setRepresentationGraphique : X et/ou Y non trouve/s dans type_capture_vars" )

        self.rg.setLesXY( les_XY )

        # les legendes

        legendeX = "" # va rester vide si not memeAbscisse
        legendeY = "" # va rester vide si not memeAbscisse
        les_legendes = []

        for ( (nomX,valeursX), (nomY,valeursY) ) in les_XY :

            # legendeY dans cas memeAbscisse
            if memeAbscisse :
                legendeY = legendeY + getNomRelatifVar( nomY ) + ", "

            # legende
            if memeAbscisse :
                legende = getNomRelatifVar(nomY)
            else :
                legende = getNomRelatifVar(nomY) +"("+ getNomRelatifVar(nomX) +")"
            les_legendes.append( legende )

        # legendeX dans cas memeAbscisse
        if memeAbscisse :
            ( (nomX,valeursX), (nomY,valeursY) ) = les_XY[0]
            legendeX = getNomRelatifVar( nomX )

        # troncature si trop long
        if len(legendeX) > 50 :
            legendeX = legendeX[:50] + "..."
        if len(legendeY) > 50 :
            legendeY = legendeY[:50] + "..."

        self.rg.setLegendes( legendeX, legendeY, les_legendes )

    ###########################################################################
    #
    # SIMULATION (en tant que simulation multiple, plan d'experience)
    #
    ###########################################################################

    ###########################################################################
    # Methode lancerSimulation
    #
    # Lance la simulation en tant que simulation multiple (plan d'experience).
    #
    ###########################################################################
    def lancerSimulation( self, exp ):
        # simulation (multiple)
        r = exp.multi_run()

    ###########################################################################
    # Methode simulerPourResult
    #
    # Lance la simulation en 'mode storage' et en recupere le resultat.
    #
    # La simulation est traitee en tant que simulation multiple (lancee avec
    # 'runManager').
    #
    ###########################################################################
    def simulerPourResult( self, exp ):

        #######################################################################
        # Methode getVueVars
        # Construit et retourne vue_vars a partir de
        # results_par_vues et typesCapture.
        # vue_vars : tuple de tuple de 
        # dict( vue_typee : dict( nomvar : donneesvar ) )
        #######################################################################
        def getVueVars( results_par_vues, typesCapture ) :

            vue_vars = []

            for i1,results_par_vues_i1 in enumerate(results_par_vues) :

                vue_vars_i1 = []

                for i2,results_par_vues_i1i2 in enumerate(results_par_vues_i1) :

                    vue_vars_i1i2 = {}

                    for ( vue, dict_nom_valeurs ) in results_par_vues_i1i2.iteritems() :
                        ( type_vue, timestep_vue ) = typesCapture[vue]
                        vue_typee = VueTypee(vue,type_vue,timestep_vue)

                        # d : dict( nomvar : donneesvar )
                        d = {}
                        for (nomvar,valeurs) in dict_nom_valeurs.iteritems() :
                            diminutif = getNomRelatifVar( nomvar )
                            donneesvar = DonneesVar(diminutif,valeurs)
                            d[ nomvar ] = donneesvar

                        vue_vars_i1i2[ vue_typee ] = d

                    vue_vars_i1.append( vue_vars_i1i2 )
            
                vue_vars.append( vue_vars_i1 )

            return vue_vars

        #######################################################################
        # Methode getTypeCaptureVars
        # Construit et retourne type_capture_vars a partir de
        # results_par_vues et typesCapture.
        # type_capture_vars :
        # tuple de tuple de dict( typage : dict( nomvar : donneesvar ) )
        # sans nom de la vue, sans doublon
        #######################################################################
        def getTypeCaptureVars( results_par_vues, typesCapture ) :

            type_capture_vars = []

            for i1,results_par_vues_i1 in enumerate(results_par_vues) :

                type_capture_vars_i1 = []

                for i2,results_par_vues_i1i2 in enumerate(results_par_vues_i1) :

                    type_capture_vars_i1i2 = {}

                    # balayage des vues
                    for ( vue, dict_nom_valeurs ) in results_par_vues_i1i2.iteritems() :
                        ( type_vue, timestep_vue ) = typesCapture[vue]
                        typage = ObsTypage(type_vue,timestep_vue)

                        for ( nomvar, valeurs ) in dict_nom_valeurs.iteritems() :

                            # nomvar,donneesvar
                            diminutif = getNomRelatifVar( nomvar )
                            donneesvar = DonneesVar(diminutif,valeurs)
        
                            # type_capture_vars[typage]
                            d = typage.getValues( type_capture_vars_i1i2 )
        
                            if d == None : # n'existe pas encore
                                # creation type_capture_vars_i1i2[typage]
                                type_capture_vars_i1i2[ typage ] = { nomvar:donneesvar }
        
                            elif nomvar not in d.keys() : # pas de doublon
                                # ajout nomvar:donneesvar a type_capture_vars_i1i2[typage]
                                d[ nomvar ] = donneesvar
                            # else : doublon a ne pas prendre en compte

                    type_capture_vars_i1.append( type_capture_vars_i1i2 )
            
                type_capture_vars.append( type_capture_vars_i1 )

            return type_capture_vars

        #######################################################################
        # Traitement
        #######################################################################

        # nettoyage/raz prealable
        self.typesCapture = None
        self.results_par_vues = None
        self.type_capture_vars = None

        # results_par_vues (obtenus par simulation multiple)
        results_par_vues = exp.multi_runModeStorage()

        #print "" # !!!
        #print "(Multi) type(results_par_vues) = ", type(results_par_vues)
        #print "(Multi) results_par_vues = ", results_par_vues
        #print ""

        t_ecr.saut_de_ligne()
        t_ecr.message( "Multi simulation :" )
        for i1,res1 in enumerate(results_par_vues) :
            for i2,res12 in enumerate(res1) :
                t_ecr.msg( "-" + str(i1) + "-" + str(i2) )

        # typesCapture construit/produit
        typesCapture = exp.get_types_capture()

        # vue_vars
        vue_vars = getVueVars( results_par_vues, typesCapture )
    
        # type_capture_vars
        type_capture_vars = getTypeCaptureVars( results_par_vues, typesCapture )
    
        self.typesCapture = typesCapture
        self.results_par_vues = results_par_vues 
        self.vue_vars = vue_vars
        self.type_capture_vars = type_capture_vars

###############################################################################
# Retourne le nom relatif NNN d'un nom de variable du type SSS,BB,CCC:DDD.NNN
# (retourne tout ce qui suit le '.')
###############################################################################
def getNomRelatifVar( nomvar ) :
    nom_raccourci = (nomvar.split('.'))[-1]
    return nom_raccourci

###############################################################################
#
# Formats/types
# pour les informations transformees/adaptees a exploitation
# de MonoResultat et MultiResultat
#
###############################################################################

###############################################################################
# Informations de typage/mode d'une observation
###############################################################################
class ObsTypage(object) :

    def __init__( self, type, timestep ):
        self.type = type
        self.timestep = timestep

    ###########################################################################
    # d etant un dict { typage : v }, recherche la cle typage correspondant
    # a ObsTypage dans dict d, et retourne sa valeur v ; retourne None si cle
    # non trouvee dans d
    ###########################################################################
    def getValues( self, d ) :
        for typage in d.keys() :
            if self.egale(typage) :
                return d[typage]
        return None

    ###########################################################################
    # Indique si ObsTypage est une cle du dict d
    ###########################################################################
    def inKeys( self, d ) :
        for typage in d.keys() :
            if self.egale(typage) :
                return True
        return False

    ###########################################################################
    # Indique si ObsTypage a meme contenu que typage (un ObsTypage)
    ###########################################################################
    def egale( self, typage ) :
        if self.type==typage.type and self.timestep==typage.timestep :
            return True
        else :
            return False

###############################################################################
# Ajout de renseignements a une vue : son typage (en plus de son nom vue)
###############################################################################
class VueTypee(object) :
    def __init__( self, vue, type, timestep ):
        self.vue = vue
        self.typage = ObsTypage(type,timestep)

###############################################################################
# Ajout de renseignements aux donnees d'une var : son diminutif (en plus de ses valeurs)
###############################################################################
class DonneesVar(object) :
    def __init__( self, diminutif, valeurs ):
        self.diminutif = diminutif
        self.valeurs = valeurs

