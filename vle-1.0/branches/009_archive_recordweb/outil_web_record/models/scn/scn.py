#-*- coding:utf-8 -*-

###############################################################################
# File scn.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Authors :
# Nathalie Rousse, INRA RECORD team member,
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from models.scenariovpz.scenariovpz import ScenarioVpz

# iii !!! pour l'instant en vrac dans modele scn
#from models.definition import Definition
#from models.espace_exploitation import EspaceExploitation
#from models.espace_exploitation import choisirPrefixeExploitation, getPrefixeRepertoireExploitation, verificationFichierOk
from models.scn.definition import Definition
from models.conf_web.conf_web_gestion import get_conf_web_scenario
from models.scn.espace_exploitation import EspaceExploitation
from models.scn.espace_exploitation import choisirPrefixeExploitation, getPrefixeRepertoireExploitation, verificationFichierOk

from record.utils.exp import Exp

from configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

###############################################################################
# Modele : Scn
#
# Une exploitation d'un scenario par un utilisateur :
# visualisation, manipulation, simulation...
#
###############################################################################
class Scn( object ):

    # Construction/creation a partir du scenario (ScenarioVpz) et des
    # informations d'identification de son application associee de
    # configuration web (InfosApplicationId)
    def __init__( self, scenario_vpz, infos_application_id ) :

        nom_vpz = scenario_vpz.get_nom_vpz()
        nom_paquet = scenario_vpz.get_nom_paquet()
        description_modele_record = scenario_vpz.get_description_paquet()
        nom_pkg = scenario_vpz.get_nom_pkg()
        version_vle = scenario_vpz.get_version_vle()
        infos_generales_initiales_scenario = scenario_vpz.get_infos_generales_initiales_scenario()

        self.scenario_vpz_d_origine = None  # ScenarioVpz 
        self.scenario_vpz = None            # ScenarioVpz 
        self.definition = None              # Definition
        self.resultat = None                # Resultat de simulation
        self.espace_exploitation = None     # espace d'exploitation
        self.configuration_web = None       # configuration appli web 

        #######################################################################
        # scenario_vpz_d_origine
        #######################################################################

        self.scenario_vpz_d_origine = ScenarioVpz( nom_vpz, nom_paquet, description_modele_record, nom_pkg, version_vle, infos_generales_initiales_scenario )

        #######################################################################
        # espace_exploitation
        #######################################################################

        # creation de l'espace d'exploitation
        prefixe = choisirPrefixeExploitation( getPrefixeRepertoireExploitation(), self.scenario_vpz_d_origine )
        self.espace_exploitation = EspaceExploitation( self.scenario_vpz_d_origine, prefixe )

        #######################################################################
        # scenario_vpz
        #######################################################################

        # changements dans scenario_vpz (relatif a espace_exploitation) par
        # rapport a scenario_vpz_d_origine :
        nom_pkg = self.espace_exploitation.nom_pkg
        nom_paquet = 'issu de ' + self.scenario_vpz_d_origine.get_nom_paquet()
        description_modele_record = description_modele_record + "(il s'agit de la description du modele record dans son etat initial)"
        nom_vpz = self.espace_exploitation.nom_vpz
        # copy.deepcopy de infos_generales_initiales_scenario ?

        self.scenario_vpz = ScenarioVpz( nom_vpz, nom_paquet, description_modele_record, nom_pkg, version_vle, infos_generales_initiales_scenario )

        #######################################################################
        # configuration_web
        #######################################################################

        # prealable : exp_scenario, Exp relatif a self.scenario_vpz
        exp_scenario = Exp( self.scenario_vpz.nom_vpz,
                            self.scenario_vpz.get_nom_pkg().encode() )

        self.configuration_web = get_conf_web_scenario( infos_application_id, exp_scenario )

        #######################################################################
        # definition
        #######################################################################

        # initialisation de l'objet de definition du scenario

        # deja fait plus haut :
        # exp_scenario = Exp( scenario_vpz.nom_vpz,
        #                     scenario_vpz.get_nom_pkg().encode() )

        self.definition = Definition( exp_scenario, self.configuration_web )

    # Destruction/suppression
    def supprimer( self ) :
        self.espace_exploitation.supprimerRepExploitation()
        del self

