#-*- coding:utf-8 -*-

###############################################################################
# File definition.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from models.affichage_saisie.affichage_saisie import DonneeSaisie

from record.utils import exp
from record.utils.exp import Exp

# pour traitement_provisoire_date_debut_simulation
from record.utils.date import DateMultiFormat
from models.affichage_saisie.affichage_saisie import DateSaisie

# TranscodageConfWebScenario : le format des informations de definition est
# est grandement calque sur celui de ConfWebScenario (cf interpretation de
# DonneeDef)
from models.conf_web.transcodage import TranscodageConfWebScenario as tcws

from models.conf_web.infos_generales import InfosGeneralesAppli

from configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

from configs.conf_vle import CONF_vle # pour version_pyvle

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)


###############################################################################
# Definition
#
# Les informations de definition d'un scenario
#
# Il s'agit des informations constituant le scenario qui sont affichees pour
# information voire modification avant d'en demander simulation (pour
# obtenir resultats, cf Resultat).
#
# Cas particulier : le mode de combinaison (lineaire ou total) concernant le
# plan d'experience n'est pas traite dans la Definition mais dans Resultat.
# Le mode de combinaison du scenario n'est pas visualise (le mode de
# combinaison choisi est applique au scenario - cf Resultat - mais visualise
# nulle part).
# Par contre le reste des informations concernant le plan d'experience
# (replicaSeed et replicaNumber) est traite dans la Definition.
#
# Definition contient une liste de Bloc.
# Un Bloc contient une liste de DonneeDef.
#
###############################################################################
class Definition( object ):

    ###########################################################################
    # Construction de Definition a partir de exp (scenario, Exp) et
    # configuration_web (configuration web, ConfWebScenario)
    # ou alors par defaut sans rien dedans
    ###########################################################################
    def __init__( self, exp=None, configuration_web=None ):

        # par defaut valeurs standard pour informations generales
        self.infos_generales = InfosGeneralesAppli()
        self.infos_generales.setValeursStandard()

        # informations de definition du scenario, organisees ici en blocs
        # (contenu et format d'affichage/saisie des pages web def)
        # regroupant des DonneeDef
        self.blocs = None

        if (exp!=None) and (configuration_web!=None) :

            # informations generales
            self.infos_generales = configuration_web.infos_generales

            # blocs
            self.setBlocs( exp, configuration_web )

    ###########################################################################
    # Prend en compte (dans les blocs de Definition) les informations de
    # definition du scenario issues de exp, conformement a configuration_web
    ###########################################################################
    def setBlocs( self, exp, configuration_web ):

        self.blocs=list()

        # liste ordonnee des blocs
        for page_web_def_name in configuration_web.pages_web_def_names :
            page_web_def = configuration_web.pages_web_def[page_web_def_name]

            info_generale_page = page_web_def.infos_generales
            titre_bloc = info_generale_page.getTitre()
            description_bloc = info_generale_page.getDescription()
            help_bloc = info_generale_page.getHelp()

            # les donnees du bloc
            liste_datas = None
            liste_datas=list()
            for infos_donnee_def in page_web_def.liste_donnees :
                donnee_def = DonneeDef( infos_donnee_def, exp )
                liste_datas.append( donnee_def )

            bloc = Bloc( titre_bloc, description_bloc, help_bloc, liste_datas )

            self.blocs.append( bloc ) # entree/ajout du bloc

    ###########################################################################
    # Prend en compte dans exp les informations de definition
    # du scenario (issues des blocs de Definition) 
    # retourne exp
    ###########################################################################
    def prendreEnCompteDans( self , exp ):

        for bloc in self.blocs :

            for donnee_def in bloc.liste_datas :

                # maj exp a partir de donnee_def.values

                web_mode = donnee_def.getWebMode()

                if tcws.isReadWriteMode(web_mode) :

                    vpz_type = donnee_def.getVpzType()
                    if tcws.isTypeExpNameVpzType(vpz_type) :
                        v = str( donnee_def.values[0] )
                        exp.setName( donnee_def.values[0] )
                    elif tcws.isTypeExpDurationVpzType(vpz_type) :
                        v = float( donnee_def.values[0] )
                        exp.setDuration( v )
                    elif tcws.isTypeExpBeginVpzType(vpz_type) :
                        v = float( donnee_def.values[0] )
                        exp.setBegin( v )
                    elif tcws.isTypeSimuSeedVpzType(vpz_type) :
                        v = float( donnee_def.values[0] )
                        exp.setSeed( v )
                    elif tcws.isTypePlanSeedVpzType(vpz_type) :
                        v = int( donnee_def.values[0] )
                        exp.setReplicaSeed( v )
                    elif tcws.isTypePlanNumberVpzType(vpz_type) :
                        v = int( donnee_def.values[0] )
                        exp.setReplicaNumber( v )
                    elif tcws.isTypeConditionPortVpzType(vpz_type) :
                        cond = donnee_def.getVpzCondName()
                        port = donnee_def.getVpzPortName()
                        exp.clearConditionPort(cond, port) # effacement prealable
                        for i,v in enumerate(donnee_def.values) :
                            type_value = donnee_def.type_value

                            if CONF_vle.is_pyvle101() :
                                cr = exp.pyvle101_tenterEcrireValeur( cond, port, v, type_value, i) # pyvle101
                            elif CONF_vle.is_pyvle102() :
                                cr = exp.tenterEcrireValeur( cond, port, v, type_value ) # pyvle102
                            else :
                                CONF_vle.message_erreur_version_pyvle(t_err)

                            if not cr : 
                                msg = "prendreEnCompteDans, echec ecriture : cond: " + str(cond) + " port: " + str(port)
                                msg = msg + " v: " + str(v) + " type_value: " + type_value + " i: " + str(i)
                                t_err.message(msg)

                    # else : rien
                # else CONST_web_mode_hidden : rien
                # else CONST_web_mode_read_only : rien

        return exp

    ###########################################################################
    # Methode miseAjourBlocsSaisis
    #
    # Met a jour les informations de definition du scenario (les valeurs des
    # donnees DonneeDef des blocs de Definition) en fonction des valeurs
    # saisies params (ParametresRequete, correspondant a des DonneeSaisie)
    #
    # Les valeurs saisies params concernent des donnees de web_mode valant
    # "read-write" (ni "hidden" ni "read-only").
    #
    # Retourne True si le traitement s'est bien deroule et False sinon (dans ce
    # dernier cas, blocs n'est pas exploitable).
    #
    # Cette methode et les traitements/controles de la page d'affichage/saisie
    # (scenario.html) sont concus en complement l'un de l'autre.
    # Par exemple la page html
    # ne permet pas de supprimer un champ unique. Si ce n'etait pas le cas,
    # alors au niveau de la methode miseAjourBlocsSaisis certaines donnees
    # risqueraient de ne pas etre correctement mises a jour.
    # Par exemple les indices d'une donnee permettant ensuite de la reperer dans
    # Definition.blocs (indice de son bloc dans blocs et son propre indice dans
    # liste_datas de son bloc) sont determines dans la page html au niveau des
    # boucles d'affichage.
    #
    ###############################################################################
    def miseAjourBlocsSaisis( self, params ):

        ###########################################################################
        # Evalue si p1 et p2 correspondent a la meme DonneeDef dans Definition
        # ie dans les blocs (critere memes indbloc et inddata)
        ###########################################################################
        def deMemeData( p1, p2 ) :
            return ( p1['indbloc']==p2['indbloc'] and p1['inddata']==p2['inddata'] )

        ###########################################################################
        # Le traitement :
        #
        # Tout d'abord params { "indbloc=aaa/inddata=ccc" : [v1,v2,v3...] }
        # est converti en params_liste
        # [ { 'indbloc':aaa, 'inddata':ccc, 'valeurs':[v1,v2,v3...] }, ... ]
        #
        # Puis la liste_datas de chacun des blocs est mise a jour en fonction de
        # params_liste
        #
        ###########################################################################

        cr_ok = True # par defaut

        # Conversion de params en params_liste
        params_liste = []

        for k in params.get_cles() :
            v = params.get_valeurs( k )
            d = DonneeSaisie( k, v )
            if d.name.formatBlocVarOk() : # filtre
                params_liste.append( d.decouper() )
            # else : ignorer d

        # Chaque element
        # { 'indbloc':indbloc, 'inddata':inddata, 'valeurs':[v1,v2...] }
        # de la liste params_liste sert a maj values de la donnee DonneeDef
        # correspondante blocs[indbloc].liste_datas[inddata]
        for d in params_liste :
            indbloc = d['indbloc']
            inddata = d['inddata']
            valeurs = d['valeurs']

            # notes_de_developpement definition.py :
            # debut traitement_provisoire_date_debut_simulation
            # maj des 2 valeurs (julian day, gregorian date et time) de date debut simulation
            # (voir aussi DonneeDef pour initialisation)

            vpz_type = self.blocs[indbloc].liste_datas[inddata].infos_donnee_def.getVpzType()
            if tcws.isTypeExpBeginVpzType(vpz_type) :

                julian_day = valeurs[0]
                yyyymmdd_hhmmss = valeurs[1]

                if DateSaisie.isValeurObsolete( julian_day ) :
                # il est tenu compte de gregorian date et time
                # maj de julian day en fonction de gregorian date et time
                    #print "maj de julian day en fonction de gregorian date et time"
                    ( yyyymmdd, hhmmss ) = DateSaisie.decouper( yyyymmdd_hhmmss )
                    la_date = DateMultiFormat()
                    la_date.initialiser_gregorian( yyyymmdd, hhmmss )
                    julian_day = la_date.get_julian_day() 
                    valeurs[0] = julian_day

                else : # (DateSaisie.isValeurObsolete(yyyymmdd_hhmmss) ou par defaut )
                # il est tenu compte de julian day
                # (que gregorian date et time aient ou non ete modifies)
                # maj de gregorian date et time en fonction de julian day
                    #print "maj de gregorian date et time en fonction de julian day"
                    la_date = DateMultiFormat()
                    la_date.initialiser_julian( julian_day )
                    yyyymmdd = la_date.get_gregorian_date()
                    hhmmss = la_date.get_time()
                    valeurs[1] = DateSaisie.texte_yyyymmdd_hhmmss( yyyymmdd, hhmmss )

            # fin traitement_provisoire_date_debut_simulation

            self.blocs[indbloc].liste_datas[inddata].values = valeurs

        return cr_ok

###############################################################################
# Bloc
#
# Les informations de definition d'un scenario (entrees
# de simulation : conditions, date debut...) sont organisees par bloc.
#
# Un Bloc correspond a une page web, contient des DonneeDef.
#
# Contient :
# - des informations generales sur le bloc : titre/nom, description, help
# - liste_datas : la liste des DonneeDef du bloc ( nom de la donnee, etc,
#   liste de ses valeurs )
# - toutes_variable_et_readwrite qui indique si toutes les donnees du bloc
#   sont a la fois VariableDimension et ReadWriteMode
#
# Remarque : pour une donnee de 'type-condition-port'
# la liste de ses valeurs contient plusieurs
# elements si donnee/port de multi-simulation et sinon (donnee/port de 
# simulation simple) un seul element.
#
###############################################################################

class InformationsGeneralesBloc(object):
    # Construction a partir
    # d'informations generales sur le bloc (titre, description, help)
    def __init__(self, titre_bloc, description_bloc, help_bloc):
        self.titre = titre_bloc
        self.description = description_bloc
        self.help = help_bloc

class Bloc(object):

    ###########################################################################
    # Construction a partir
    # d'informations generales sur le bloc (titre, description, help)
    # et de la liste liste_datas de ses donnees DonneeDef 
    ###########################################################################
    def __init__(self, titre_bloc, description_bloc, help_bloc, liste_datas):

        self.infos_generales = InformationsGeneralesBloc( titre_bloc, description_bloc, help_bloc )

        self.liste_datas = liste_datas

        self.toutes_variable_et_readwrite = self.toutesVariableEtReadWrite()

    ###########################################################################
    # Retourne booleen : True si toutes les donnees du bloc sont a la fois
    # VariableDimension et ReadWriteMode (a condition que le bloc possede au
    # moins une donnee), et sinon False 
    ###########################################################################
    def toutesVariableEtReadWrite( self ):
        uneDataNotVariableEtReadWrite = False

        for data in self.liste_datas :
            if data.isVariableDimension() and data.isReadWriteMode() :
                pass
            else :
                uneDataNotVariableEtReadWrite = True

        if len(self.liste_datas) == 0 : # cas particulier ou liste_datas vide
            return False
        elif uneDataNotVariableEtReadWrite == True :
            return False
        else :
            return True

###############################################################################
# DonneeDef
#
# Une donnee parmi les informations de definition d'un scenario (entrees
# de simulation : conditions, date debut...), telle que exploitee/traitee 
# au niveau d'un Bloc (rangee dans liste_datas de Bloc).
#
# Contient toutes les informations necessaires a son affichage/saisie a
# l'ecran et maj.
#
# Une donnee provient d'un scenario vpz (exp), dans lequel elle est par la
# suite maj.
#
# Une donnee contient aussi des informations issues de la configuration web : 
# caracteristiques qui permettent de l'identifier dans le scenario vpz, qui
# definissent les conditions dans lesquelles la gerer/afficher a l'ecran
# dans la page web, doc...
#
# Une donnee est affichee dans une page web pour information et peut-etre
# modification (rentree dans un Bloc).
#
###############################################################################
#
# Les informations provenant de la configuration web s'appuient le plus
# possible sur le format de ConfWebScenario.
#
# Remarque : pour une donnee de type 'type-condition-port', la liste de ses
# valeurs contient plusieurs elements si donnee/port de multi-simulation
# et sinon (donnee/port de simulation simple) un seul element.
#
###############################################################################
#
# CERTAINS TYPES DE VALEURS DE PORTS (vle) NE SONT PAS TRAITES : voir
# assuranceCompatibilite (class ConfWebScenario, conf_web_scenario.py)
#
###############################################################################
class DonneeDef( object ):

    # Construction a partir d'une donnee InfosDonneeDef d'une page web de
    # definition (cf ConfWebScenario) et d'un exp scenario de simulation
    def __init__(self, infos_donnee_def, exp) :

        vpz_type = infos_donnee_def.getVpzType()

        # informations (nom web, documentation, type...)
        self.infos_donnee_def = infos_donnee_def
        # zzz a deepcopier ?

        # valeurs (self.values)
        if tcws.isTypeExpNameVpzType(vpz_type) :
            values = [ exp.getExperimentName() ]
        elif tcws.isTypeExpDurationVpzType(vpz_type) :
            values = [ exp.getDuration() ]
        elif tcws.isTypeExpBeginVpzType(vpz_type) :

            # values = [ exp.getBegin() ]

            # notes_de_developpement definition.py : remplacement temporaire
            # debut traitement_provisoire_date_debut_simulation
            # Saisie au format gregorian date et time en plus de julian date,
            # emploi artificiel des 2nd et 3e indices de values pour 
            # affichage/saisie de gregorian date et time
            # (voir aussi miseAjourBlocsSaisis pour interpretation des saisies)
            # (les 2nd et 3e indices de values ne seront pas transmis a exp)

            jd = exp.getBegin()

            la_date = DateMultiFormat()
            la_date.initialiser_julian( str(jd) )
            yyyymmdd = la_date.get_gregorian_date() 
            hhmmss = la_date.get_time()
            yyyymmdd_hhmmss = DateSaisie.texte_yyyymmdd_hhmmss( yyyymmdd, hhmmss )

            if self.isReadWriteMode() :
                texte_instruction1 = "modifier un seul des 2 formats"
                texte_instruction2 = "saisir '" + DateSaisie.getValeurObsolete() + "' dans l'autre !!!"
                values = [ jd, yyyymmdd_hhmmss, texte_instruction1, texte_instruction2 ]
            else :
                values = [ jd, yyyymmdd_hhmmss ]

            # fin traitement_provisoire_date_debut_simulation

        elif tcws.isTypeSimuSeedVpzType(vpz_type) :
            values = [ exp.getSeed() ]
        elif tcws.isTypePlanSeedVpzType(vpz_type) :
            values = [ exp.getReplicaSeed() ]
        elif tcws.isTypePlanNumberVpzType(vpz_type) :
            values = [ exp.getReplicaNumber() ]
        elif tcws.isTypeConditionPortVpzType(vpz_type) :
            cond = infos_donnee_def.getVpzCondName()
            port = infos_donnee_def.getVpzPortName()
            values = exp.getConditionSetValue(cond,port)
        else :
            values = ""
        self.values = values

        # type (self.type_value)
        type_value = None
        if tcws.isTypeConditionPortVpzType(vpz_type) :
            cond = infos_donnee_def.getVpzCondName()
            port = infos_donnee_def.getVpzPortName()
            type_value = exp.get_type_value(cond,port) # type du premier element
        elif tcws.isTypeExpNameVpzType(vpz_type) :
            type_value = type(self.values[0])
        elif tcws.isTypeExpDurationVpzType(vpz_type) :
            type_value = type(self.values[0])
        elif tcws.isTypeExpBeginVpzType(vpz_type) :
            type_value = type(self.values[0])
        elif tcws.isTypeSimuSeedVpzType(vpz_type) :
            type_value = type(self.values[0])
        elif tcws.isTypePlanSeedVpzType(vpz_type) :
            type_value = type(self.values[0])
        elif tcws.isTypePlanNumberVpzType(vpz_type) :
            type_value = type(self.values[0])
        else :
            type_value = type(self.values)
        self.type_value = type_value

    ###########################################################################
    # Evaluation de certaines informations de DonneeDef
    # (web_dimension, web_mode...)
    ###########################################################################

    def getWebMode(self) :
        return self.infos_donnee_def.getWebMode()

    def getVpzType(self) :
        return self.infos_donnee_def.getVpzType()

    def getVpzCondName(self) :
        return self.infos_donnee_def.getVpzCondName()

    def getVpzPortName(self) :
        return self.infos_donnee_def.getVpzPortName()

    # determine si DonneeDef est de web_dimension "variable" (ou equivalent)
    def isVariableDimension(self) :
        web_dimension = self.infos_donnee_def.getWebDimension()
        return tcws.isVariableDimension(web_dimension)

    # determine si DonneeDef est de web_dimension "fixe" (ou equivalent)
    def isFixeDimension(self) :
        web_dimension = self.infos_donnee_def.getWebDimension()
        return tcws.isFixeDimension(web_dimension)

    # determine si DonneeDef est de web_mode "hidden" (ou equivalent)
    def isHiddenMode(self) :
        web_mode = self.infos_donnee_def.getWebMode()
        return tcws.isHiddenMode(web_mode)

    # determine si DonneeDef est de web_mode "read_write" (ou equivalent)
    def isReadWriteMode(self) :
        web_mode = self.infos_donnee_def.getWebMode()
        return tcws.isReadWriteMode(web_mode)

    # determine si DonneeDef est de web_mode "read_only" (ou equivalent)
    def isReadOnlyMode(self) :
        web_mode = self.infos_donnee_def.getWebMode()
        return tcws.isReadOnlyMode(web_mode)

