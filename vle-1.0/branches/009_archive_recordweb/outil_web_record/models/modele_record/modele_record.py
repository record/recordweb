#-*- coding:utf-8 -*-

###############################################################################
# File modele_record.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Authors :
# Nathalie Rousse, INRA RECORD team member,
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from bd_modeles_record.modele_record_bd.models import ModeleRecord as ModeleRecordBd
from bd_modeles_record.modele_record_bd.models import Individu as IndividuBd

from models.scn.espace_exploitation import getListeUtilePkgs

from models.scenariovpz.scenariovpz import ScenarioVpz
from models.conf_web.conf_web_gestion import get_conf_web_modele_record

from configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
#t_err = TraceErreur(__file__,__name__,CONF_trace)

###############################################################################
#
# Classes et methodes dediees a l'information des modeles record tels
# qu'utilises/exploites dans l'outil_web_record (montres a l'usr...).
# Couche de filtres et complements (notamment complements relatifs a la
# configuration web) des modeles record entre (de) la bd des
# modeles record et (vers) l'outil_web_record.
#
# notes_de_developpement modele_record.py : le mot_de_passe.username du mot
# de passe public est ecrit en dur ('modele_public') dans la methode isPublic
# de ModeleRecord.
#
###############################################################################

###############################################################################
# Modele : LesModelesRecord
#
# L'information de l'ensemble des modeles record tels qu'utilises/exploites
# dans l'outil_web_record. 
#
# LesModelesRecord sert a presenter les modeles record dans
# l'outil_web_record, pour en choisir un, puis un de ses scenarios
#
# Les modeles record sont classes en 2 listes :
# - liste_modeles_record_on : la liste de ceux qui seront proposes
#   (ModeleRecordOn) et
# - liste_modeles_record_off : la liste de ceux qui ne seront pas
#   proposes pour exploitation (ModeleRecordOff).
#
# Rq : (1) ModeleRecordBd (ModeleRecord dans la bd des modeles record)
#  --> (2) ModeleRecord (ModeleRecordOn ou ModeleRecordOff) dans
#          liste_modeles_record_on ou liste_modeles_record_off de
#          LesModelesRecord (presentation des modeles record dans
#          l'outil_web_record, pour choix du modele et du scenario)
#  --> (3) Paquet : construit par rapport au scenario choisi. Paquet est 
#          l'objet traite une fois le scenario choisi dans le modele record.
#
###############################################################################
class LesModelesRecord(object):

    ###########################################################################
    # Construction a partir des ModeleRecordBd (modeles record de la bd des
    # modeles record) et des ConfWebModeleRecord (configurations web de chaque
    # modele record)
    ###########################################################################
    def __init__(self):

        # liste_modeles_record_bd
        liste_modeles_record_bd = ModeleRecordBd.objects.all()
        
        liste_modeles_record_on = []
        liste_modeles_record_off = []

        # trace 
        t_ecr.trait()
        t_ecr.message( "Liste des modèles record : " )

        for modele_record_bd in liste_modeles_record_bd :

            modele_record = ModeleRecord(modele_record_bd)
            if modele_record.exploitabilite.estExploitable() :

                modele_record_on = ModeleRecordOn( modele_record_bd )

                # trace 
                m = modele_record_on.nom + " (visible/accessible : " + str(modele_record_on.visible) + ")"
                t_ecr.msg(m)

                liste_modeles_record_on.append( modele_record_on )

            else :
                msg = modele_record.exploitabilite.getMsgEtat()
                modele_record_off = ModeleRecordOff( modele_record_bd, msg )
                liste_modeles_record_off.append( modele_record_off )

        self.liste_modeles_record_on = liste_modeles_record_on
        self.liste_modeles_record_off = liste_modeles_record_off

    ###########################################################################
    # Retourne l'element ModeleRecordOn de liste_modeles_record_on 
    # y ayant pour index index_modele_record_on
    ###########################################################################
    def getModeleRecordOn( self, index_modele_record_on ) :
        modele_record_on = self.liste_modeles_record_on[ index_modele_record_on ]
        return modele_record_on

    ###########################################################################
    # Rend accessible le ModeleRecordOn de liste_modeles_record_on y ayant
    # pour index index_modele_record_on (visible True)
    ###########################################################################
    def deverouillerModeleRecordOn( self, index_modele_record_on ) :
        modele_record_on = self.liste_modeles_record_on[ index_modele_record_on ]
        modele_record_on.rendreAccessible()

    ###########################################################################
    # Construit et retourne le ScenarioVpz correspondant a un ModeleRecordOn
    # et un de ses scenarios donnes, caracterises par leurs identifiants
    # index_modele_record_on (dans liste_modeles_record_on) et
    # index_scenario_on (dans liste scenarios_on du ModeleRecordOn)
    ###########################################################################
    def getScenarioVpz( self, index_modele_record_on, index_scenario_on ) :

        modele_record_on = self.getModeleRecordOn( index_modele_record_on )

        nom_paquet = modele_record_on.nom
        description_modele_record = modele_record_on.description
        nom_pkg = modele_record_on.nom_pkg
        version_vle = modele_record_on.version_vle

        scenario_on = modele_record_on.scenarios_on[index_scenario_on]
        nom_vpz = scenario_on.nom_scenario + ".vpz"


        # infos_generales_initiales_scenario
        nom_scenario = scenario_on.nom_scenario
        conf_web_modele_record = modele_record_on.conf_web_modele_record
        infos_generales_initiales_scenario = conf_web_modele_record.infos_generales_scenarios[nom_scenario]

        scenario_vpz = ScenarioVpz( nom_vpz, nom_paquet, description_modele_record, nom_pkg, version_vle, infos_generales_initiales_scenario )

        return scenario_vpz

    ###########################################################################
    # Retourne ApplicationWeb correspondant a une application associee a un
    # scenario ScenarioOn d'un modele record ModeleRecordOn, caracterises par
    # leurs identifiants :
    # index_modele_record_on (dans liste_modeles_record_on) et
    # index_scenario_on (dans liste scenarios_on du ModeleRecordOn) et
    # index_application_associee (dans liste infos_applications_associees du
    # ScenarioOn)
    #
    # Attention : On utilise/s'appuie sur le fait que les applications se
    # trouvent exactement dans le meme ordre dans infos_applications_associees
    # de ScenarioOn et dans applications_web_associees[nom_scenario] de
    # ConfWebModeleRecord (ou sera recuperee ApplicationWeb)
    #
    ###########################################################################
    def getApplicationWeb( self, index_modele_record_on, index_scenario_on, index_application_associee ) :

        modele_record_on = self.getModeleRecordOn( index_modele_record_on )

        scenario_on = modele_record_on.scenarios_on[index_scenario_on]
        nom_scenario = scenario_on.nom_scenario

        conf_web_modele_record = modele_record_on.conf_web_modele_record

        application_web = conf_web_modele_record.getApplicationWeb( nom_scenario, index_application_associee )

        return application_web

###############################################################################
# Modele : ModeleRecord
#
# L'information d'un modele record tel qu'utilise/exploite dans
# l'outil_web_record. Reprend/recupere un certain nombre d'informations du
# ModeleRecordBd d'origine (ModeleRecord dans la bd des modeles record) et en
# contient d'autres (visible...).
#
# ModeleRecord sert a presenter le modele record dans l'outil_web_record,
# a travers ses classes filles ModeleRecordOn et ModeleRecordOff (ne sert
# pas directement). ModeleRecord concerne/traite la partie commune de ses
# classes filles ModeleRecordOn et ModeleRecordOff.
#
# exploitabilite : indique si le ModeleRecord est exploitable ou non dans
# l'outil_web_record (en fonction de son etat, deviendra/conduira a un
# ModeleRecordOn ou ModeleRecordOff).
#
# visible : booleen qui indique si la liste noms_scenarios est a afficher ou
# a cacher. Par defaut, un modele public est visible et un prive (avec mot de
# passe) est non visible.
#
###############################################################################
class ModeleRecord(object):

    ###########################################################################
    # Construction a partir d'un ModeleRecordBd (ModeleRecord dans la bd des
    # modeles record)
    #
    # Determine exploitabilite :
    # - verifie que tous les champs du ModeleRecordBd sont renseignes dans bd,
    #   sauf les noms des fichiers vpz de configuration 
    #   nom_fichier_config_... (ce sont les seuls non obligatoires, tous les
    #   autres sont exiges/obligatoires).
    # - verifie que le paquet nom_pkg existe sous pkgs.
    # - ne fait aucune verification concernant les noms des fichiers vpz de
    #   configuration nom_fichier_config_... (existence verifiee
    #   ailleurs/ulterieurement)
    #
    # Determine si possible id et visible.
    #
    ###########################################################################
    def __init__( self, modele_record_bd ) :

        expl = Exploitabilite()
        self.id = None # par defaut
        self.visible = None # par defaut

        if modele_record_bd == None : # pas de modele_record_bd 
            expl.rendreInexploitable( "modèle inconnu" ) 

        else : # modele_record_bd existe

            # verifie que tous les champs de modele_record_bd sont renseignes
            # (sauf mot_de_passe et nom_fichier_config_...)

            if modele_record_bd.id == None :
                expl.rendreInexploitable( "information id manquante" )
            else : # id existe
                self.id = modele_record_bd.id 

            if modele_record_bd.nom==None or modele_record_bd.nom=="" : 
                expl.rendreInexploitable( "information nom manquante" )

            if modele_record_bd.description==None or modele_record_bd.description=="" :
                expl.rendreInexploitable( "information description manquante" )

            if modele_record_bd.nom_pkg==None or modele_record_bd.nom_pkg=="" :
                expl.rendreInexploitable( "information nom_pkg manquante" )
            else : # verifie que le paquet nom_pkg existe sous pkgs

                if modele_record_bd.nom_pkg not in getListeUtilePkgs() :
                # nom_pkg absent sous pkgs
                    txt = "le paquet " + modele_record_bd.nom_pkg + " n'existe pas sous pkgs"
                    expl.rendreInexploitable( txt ) 

            # mot_de_passe verifie plus bas (cf self.visible)

            # nom_fichier_config_web_conf non verifie (non obligatoire)
            # nom_fichier_config_web_applis non verifie (non obligatoire)
            # nom_fichier_config_dicos non verifie (non obligatoire)

            if modele_record_bd.responsable == None :
                expl.rendreInexploitable( "information responsable manquante" )
            else : # verifie que responsable lui-meme estExploitable
                individu = Individu( modele_record_bd.responsable )
                if not individu.exploitabilite.estExploitable() :
                    msg = individu.exploitabilite.getMsgEtat()
                    expl.rendreInexploitable( msg )

            if modele_record_bd.responsable_scientifique == None :
                expl.rendreInexploitable( "information responsable_scientifique manquante" )
            else : # verifie que responsable_scientifique lui-meme estExploitable
                individu = Individu( modele_record_bd.responsable_scientifique )
                if not individu.exploitabilite.estExploitable() :
                    msg = individu.exploitabilite.getMsgEtat()
                    expl.rendreInexploitable( msg )

            if modele_record_bd.responsable_informatique == None :
                expl.rendreInexploitable( "information responsable_informatique manquante" )
            else : # verifie que responsable_informatique lui-meme estExploitable
                individu = Individu( modele_record_bd.responsable_informatique )
                if not individu.exploitabilite.estExploitable() :
                    msg = individu.exploitabilite.getMsgEtat()
                    expl.rendreInexploitable( msg )

            if modele_record_bd.cr_reception==None or modele_record_bd.cr_reception=="" :
                expl.rendreInexploitable( "information cr_reception manquante" )

            if modele_record_bd.version_vle==None or modele_record_bd.version_vle=="" :
                expl.rendreInexploitable( "information version_vle manquante" )

        # self.id deja affecte

        # self.visible
        cr = self.isPublic()
        if cr == None :
            # visible reste None
            expl.rendreInexploitable( "statut (privé ou public) inconnu (mot de passe manquant)" )
        elif cr : # modele public
            self.rendreAccessible()
        else : # modele prive
            self.rendreInaccessible()

        self.exploitabilite = expl 

    ###########################################################################
    # Determine si le ModeleRecord est public. Retourne
    # True pour etat public, False pour etat prive, None pour etat inconnu
    ###########################################################################
    def isPublic(self):

        #######################################################################
        # Retourne le MotDePasse du ModeleRecordBd (identifie dans la bd des
        # modeles record par son id ModeleRecord.id)
        # Retourne None si mot de passe inconnu/inexistant
        #######################################################################
        def getPassword():
            mdp = None # par defaut
            modele_record_bd = ModeleRecordBd.objects.get(id=self.id)
            mdp = modele_record_bd.mot_de_passe
            return mdp

        #######################################################################
        # Traitement
        #######################################################################
        mot_de_passe = getPassword()
        if mot_de_passe == None :
            cr_is_public = None # pour etat inconnu
        elif mot_de_passe.username == 'modele_public' :
            cr_is_public = True
        else :
            cr_is_public = False
        return cr_is_public

    ###########################################################################
    # Compare mot_de_passe et le mot de passe du ModeleRecord, plus exactement
    # du ModeleRecordBd correspondant (identifie dans la bd des modeles
    # record par son id ModeleRecord.id). Retourne True si identite.
    ###########################################################################
    def checkPassword(self, mot_de_passe):
        cr = False # par defaut
        modele_record_bd = ModeleRecordBd.objects.get(id=self.id)
        cr = modele_record_bd.mot_de_passe.check_password( mot_de_passe )
        return cr

    ###########################################################################
    # Rend accessible le ModeleRecord (ie visible True)
    ###########################################################################
    def rendreAccessible(self):
        self.visible = True

    ###########################################################################
    # Rend inaccessible le ModeleRecord (ie visible False)
    ###########################################################################
    def rendreInaccessible(self):
        self.visible = False

###############################################################################
# Modele : ModeleRecordOn
#
# L'information d'un modele record tel qu'utilise/exploite dans
# l'outil_web_record : un ModeleRecord qui est propose pour exploitation.
# ModeleRecordOn sert a presenter le modele record dans
# l'outil_web_record (pour en choisir un, puis un de ses scenarios, puis une
# des configurations d'application web du scenario choisi).
#
# Les informations du ModeleRecordOn reprennent/recuperent un certain nombre
# d'informations du ModeleRecordBd d'origine (ModeleRecord dans la bd des
# modeles record) + en contiennent d'autres (la liste de ses scenarios
# noms_scenarios (fichiers vpz) tenant compte de la configuration web, et pour
# chaque scenario ses configurations d'application web associees/possibles).
#
# Un ModeleRecordOn est exploitable (non initialise sinon).
# Chacun des Individus associes au ModeleRecordOn, lui-meme estExploitable,
# est un IndividuOn.
#
###############################################################################
class ModeleRecordOn(ModeleRecord):

    ###########################################################################
    # Construction a partir d'un ModeleRecordBd
    ###########################################################################
    def __init__( self, modele_record_bd ) :

        ModeleRecord.__init__( self, modele_record_bd )

        etatOk = True # par defaut
        if not self.exploitabilite.estExploitable() : # ModeleRecord estExploitable
            etatOk = False
        else : # chacun des Individus du ModeleRecord estExploitable
            individu = Individu( modele_record_bd.responsable )
            if not individu.exploitabilite.estExploitable() :
                etatOk = False
            individu = Individu( modele_record_bd.responsable_scientifique )
            if not individu.exploitabilite.estExploitable() :
                etatOk = False
            individu = Individu( modele_record_bd.responsable_informatique )
            if not individu.exploitabilite.estExploitable() :
                etatOk = False

        if etatOk :
            self.nom = modele_record_bd.nom 
            self.description = modele_record_bd.description 
            self.nom_pkg = modele_record_bd.nom_pkg 
            self.nom_fichier_config_web_conf = modele_record_bd.nom_fichier_config_web_conf
            self.nom_fichier_config_web_applis = modele_record_bd.nom_fichier_config_web_applis
            self.nom_fichier_config_dicos = modele_record_bd.nom_fichier_config_dicos
            self.responsable = IndividuOn( modele_record_bd.responsable )
            self.responsable_scientifique = IndividuOn( modele_record_bd.responsable_scientifique )
            self.responsable_informatique = IndividuOn( modele_record_bd.responsable_informatique )
            self.cr_reception = modele_record_bd.cr_reception 
            self.version_vle = modele_record_bd.version_vle 

            # liste des ScenarioOn, scenarios (fichiers vpz) disponibles du
            # modele/paquet (non pas ceux presents mais ceux issus/filtres
            # de/par la configuration web)

            # lecture de la configuration web
            conf_web_modele_record = get_conf_web_modele_record( modele_record_bd.nom_pkg, modele_record_bd.nom_fichier_config_web_conf, modele_record_bd.nom_fichier_config_web_applis, modele_record_bd.nom_fichier_config_dicos )

            scenarios_on = []
            for nom_scenario_on in conf_web_modele_record.noms_scenarios :
                infos_generales_scenario = conf_web_modele_record.infos_generales_scenarios[ nom_scenario_on ]
                applications_associees = conf_web_modele_record.applications_web_associees[ nom_scenario_on ]
                scenario_on = ScenarioOn( nom_scenario_on, infos_generales_scenario, applications_associees )

                scenarios_on.append( scenario_on )

            self.scenarios_on = scenarios_on
            self.conf_web_modele_record = conf_web_modele_record

###############################################################################
# Modele : ModeleRecordOff
#
# L'information d'un modele record tel qu'utilise/exploite dans
# l'outil_web_record : un ModeleRecord qui ne sera pas propose pour
# exploitation. ModeleRecordOff sert a presenter le modele record dans
# l'outil_web_record (mais il ne sera pas propose pour exploitation).
#
# Les informations du modeleRecordOff reprennent/recuperent
# un certain nombre d'informations du ModeleRecord (+ en contiennent d'autres)
#
# Si dans l'absolu un ModeleRecordOff etait exploitable (etat_exploitable
# True a la base/origine), alors son etat_exploitable est force a False dans
# le cadre de sa construction/creation.
#
# Chacun des Individus associes au ModeleRecordOff est defini en tant que
# IndividuOff (meme si dans le fond estExploitable).
#
###############################################################################
class ModeleRecordOff(ModeleRecord):

    ###########################################################################
    # Construction a partir d'un ModeleRecordBd et du message msg relatif au
    # fait que le modele n'est pas propose (justification...)
    ###########################################################################
    def __init__(self, modele_record_bd, msg) :

        ModeleRecord.__init__( self, modele_record_bd )

        # eventuel forcage (inexploitabilite)
        if self.exploitabilite.estExploitable() :
            txt = "rendu inexploitable (bien qu'exploitable dans le fond)"
            self.exploitabilite.rendreInexploitable( txt )

        if modele_record_bd == None :
            self.nom = None
            self.nom_pkg = None
            self.responsable = None
        else :
            if modele_record_bd.nom != None :
                self.nom = modele_record_bd.nom  # rq : vaut peut-etre ""
            if modele_record_bd.nom_pkg != None :
                self.nom_pkg = modele_record_bd.nom_pkg  # rq : vaut peut-etre ""
            if modele_record_bd.responsable != None :
                # IndividuOff (meme si dans le fond exploitable)
                txt = "... texte a rediger/remplir ..."
                self.responsable = IndividuOff( modele_record_bd.responsable, txt )

        self.msg = msg

###############################################################################
# Modele : ScenarioOn
#
# L'information d'un scenario tel qu'utilise/exploite dans
# l'outil_web_record : un scenario qui est propose pour exploitation (associe
# a un ModeleRecordOn).
#
# ScenarioOn sert a presenter le scenario et ses configurations d'application
# web (pour etre eventuellement choisi avec l'une d'entre elles).
#
# Les informations du ScenarioOn reprennent/recuperent dans nom_scenario et
# applications_associees (provenant de ConfWebModeleRecord) ce qui est utile
# a la presentation :
# - nom_scenario : le nom du scenario
# - infos_generales : les infos generales du scenario (description, titre...)
# - infos_applications_associees : la liste des infos_generales de ses
#   applications_associees.
#
# Attention : Les applications doivent etre rangees dans la liste resultat
# infos_applications_associees exactement dans l'ordre ou elles se trouvent
# dans la liste source applications_associees (cf traitement
# getApplicationAssociee).
#
###############################################################################
class ScenarioOn(object):

    ###########################################################################
    # Construction a partir de son nom_scenario, de ses infos generales 
    # infos_generales et de la liste de ses applications_associees
    ###########################################################################
    def __init__( self, nom_scenario, infos_generales, applications_associees ) :

        self.nom_scenario = nom_scenario 

        self.infos_generales = infos_generales 

        infos_applications_associees = []
        for application_associee in applications_associees :
            infos_application_associee = application_associee.getInformationsGenerales()
            infos_applications_associees.append( infos_application_associee )
        self.infos_applications_associees = infos_applications_associees 

###############################################################################
# Modele : Individu
#
# L'information d'un individu tel qu'utilise/exploite dans l'outil_web_record.
# Reprend/recupere un certain nombre d'informations de IndividuBd d'origine
# (Individu dans la bd des modeles record) et en contient d'autres
# (exploitabilite...).
#
# Individu sert a presenter dans l'outil_web_record un individu lie/relatif a
# un modele record, a travers ses classes filles IndividuOn et IndividuOff (ne
# sert pas directement). Individu concerne/traite la partie commune de ses
# classes filles IndividuOn et IndividuOff.
#
# exploitabilite : indique si Individu est exploitable ou non dans
# l'outil_web_record (en fonction de son etat, deviendra/conduira a un
# IndividuOn ou IndividuOff).
#
###############################################################################
class Individu(object):

    ###########################################################################
    # Construction a partir d'un IndividuBd (Individu de la bd des modeles
    # record).
    # Determine exploitabilite (verifie que tous les champs de IndividuBd sont
    # renseignes dans bd).
    # Determine si possible id.
    ###########################################################################
    def __init__( self, individu_bd ) :

        expl = Exploitabilite()
        id = None # par defaut

        if individu_bd == None : # pas de individu_bd 
            expl.rendreInexploitable( "individu inconnu" ) 

        else : # individu_bd existe

            # verifie que tous les champs de modele_record_bd sont renseignes
            # (tous sont exiges/obligatoires)
            if individu_bd.id == None :
                expl.rendreInexploitable( "information id manquante" )
            else :
                id = individu_bd.id 
            if individu_bd.first_name==None or individu_bd.first_name=="" :
                expl.rendreInexploitable( "information first_name manquante" )
            if individu_bd.last_name==None or individu_bd.last_name=="" :
                expl.rendreInexploitable( "information last_name manquante" )
            if individu_bd.email==None or individu_bd.email=="" :
                expl.rendreInexploitable( "information email manquante" )

        self.exploitabilite = expl
        self.id = id

###############################################################################
# Modele : IndividuOn
#
# L'information d'un individu tel qu'utilise/exploite dans
# l'outil_web_record : un Individu qui est propose pour
# exploitation (associe a un ModeleRecordOn).
#
# Un IndividuOn est exploitable (non initialise sinon)
#
###############################################################################
class IndividuOn(Individu):

    ###########################################################################
    # Construction a partir d'un IndividuBd
    ###########################################################################
    def __init__( self, individu_bd ) :

        Individu.__init__( self, individu_bd )

        if self.exploitabilite.estExploitable() :
            self.first_name = individu_bd.first_name 
            self.last_name = individu_bd.last_name 
            self.email = individu_bd.email 

###############################################################################
# Modele : IndividuOff
#
# L'information d'un individu tel qu'utilise/exploite dans
# l'outil_web_record : un Individu non propose pour
# exploitation (associe a un ModeleRecordOff).
#
# Si dans l'absolu un IndividuOff etait exploitable (etat_exploitable
# True a la base/origine), alors son etat_exploitable est force a False dans
# le cadre de sa construction/creation.
#
###############################################################################
class IndividuOff(Individu):

    ###########################################################################
    # Construction a partir d'un IndividuBd et du message msg relatif au
    # fait que l'individu n'est pas exploitable/propose (justification...)
    ###########################################################################
    def __init__(self, individu_bd, msg) :

        Individu.__init__( self, individu_bd )

        # eventuel forcage (inexploitabilite)
        if self.exploitabilite.estExploitable() :
            txt = "rendu inexploitable (bien qu'exploitable dans le fond)"
            self.exploitabilite.rendreInexploitable( txt )

        if individu_bd == None :
            self.first_name = None
            self.last_name = None
            self.email = None
        else :
            if individu_bd.first_name != None :
                self.first_name = individu_bd.first_name # rq : vaut peut-etre ""
            if individu_bd.last_name != None :
                self.last_name = individu_bd.last_name # rq : vaut peut-etre ""
            if individu_bd.email != None :
                self.email = individu_bd.email # rq : vaut peut-etre ""

###############################################################################
# Exploitabilite decrit le caractere exploitable ou non dans
# l'outil_web_record d'une information donnee.
#
# etat_exploitable : booleen qui indique si l'information consideree (ie a
# laquelle Exploitabilite est associe) est exploitable ou non dans
# l'outil_web_record.
#
# msg_etat : informations sur les raisons/explications de l'etat de
# l'information consideree.
###############################################################################
class Exploitabilite(object) :

    ###########################################################################
    # Construction par defaut
    ###########################################################################
    def __init__( self ) :
        self.etat_exploitable = True # par defaut
        self.msg_etat = "" # par defaut

    ###########################################################################
    # met etat_exploitable a False et complete msg_etat avec txt
    ###########################################################################
    def rendreInexploitable( self, txt ) :
        self.etat_exploitable = False
        self.msg_etat = self.msg_etat + " -- " + txt

    ###########################################################################
    # Retourne true si exploitabilite (ie etat_exploitable True)
    ###########################################################################
    def estExploitable( self ) :
        return self.etat_exploitable

    ###########################################################################
    # Retourne msg_etat
    ###########################################################################
    def getMsgEtat( self ) :
        return self.msg_etat
