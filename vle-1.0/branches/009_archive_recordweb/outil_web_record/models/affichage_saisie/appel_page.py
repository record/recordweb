#-*- coding:utf-8 -*-

###############################################################################
# File appel_page.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
#
# Classes et methodes dediees a l'appel de pages
#
###############################################################################

from django.core.context_processors import csrf

from django import forms

from models.affichage_saisie.choix_operations import ChoixOperations as CHX # noms des choix

###############################################################################
# Modele : NomsPages
#
# Des constantes (les noms des pages)
#
###############################################################################
class NomsPages(object) :

    NOM_page_liste_scenarios = 'scn_selection/liste_scenarios.html'

    NOM_page_affichage_scenario = 'scn_edition/scenario.html'

    NOM_page_simulation_menu = 'scn_simulation/simulation_menu.html'

    NOM_page_restitution_menu = 'res_menu/restitution_menu.html'

    NOM_page_telechargement_menu = 'res_telechargement/telechargement_menu.html'

    NOM_page_affichage_numerique = 'res_numerique/affichage_numerique.html'

    # menu de choix du type de representation graphique :
    NOM_page_graphique_menu = 'res_graphique/graphique_menu.html'

    # pages de configuration representation graphique (choix des donnees representees...)
    NOM_page_configuration_graphiqueY = 'res_graphique/configuration_graphiqueY.html'
    NOM_page_configuration_graphiqueXY = 'res_graphique/configuration_graphiqueXY.html'

    NOM_page_affichage_graphique = 'res_graphique/affichage_graphique.html'

    NOM_page_fin = 'the_end.html'

###############################################################################
#
# Des methodes de constitution de contextes d'appel de pages
#
###############################################################################

###############################################################################
# Methode contexte_liste_scenarios : constitue et retourne le contexte de
# la page NOM_page_liste_scenarios 
###############################################################################
def contexte_liste_scenarios(request, flashes, les_modeles_record) :

    # pour memo : request.LANGUAGE_CODE 

    # Formulaire de saisie du mot de passe (du modele record prive)
    class MotDePasseForm(forms.Form):
        mot_de_passe = forms.CharField( label=u'Mot de passe', widget=forms.PasswordInput(render_value=False) )

    c = {}

    # le formulaire de saisie du mot de passe
    formulaire_mot_de_passe = MotDePasseForm()

    c = {}
    c['les_modeles_record'] = les_modeles_record
    c['formulaire_mot_de_passe'] = formulaire_mot_de_passe
    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

###############################################################################
# Methode contexte_affichage_scenario : constitue et retourne le contexte de
# la page d'affichage du scenario
###############################################################################
def contexte_affichage_scenario(request, flashes, scn, contenu_repertoire_data) :

    c = {}

    # operations proposees : apres avoir eventuellement modifie le scenario,
    # l'usr quittera la page par choix_validationIntermediaire ou
    # choix_validationFinale (ou retour en arriere)
    c['choix_validationIntermediaire'] = CHX.NOM_choix_validationIntermediaire
    c['choix_validationFinale'] = CHX.NOM_choix_validationFinale

    # autres constantes a transmettre pour identifier les operations
    c['operation_validation'] = CHX.VAL_operation_validation
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo

    c['scenario'] = scn.scenario_vpz
    c['definition'] = scn.definition
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine
    c['contenu_repertoire_data'] = contenu_repertoire_data
    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

###############################################################################
# Methode contexte_simulation_menu : constitue et retourne le contexte de
# la page NOM_page_simulation_menu
###############################################################################
def contexte_simulation_menu(request, flashes, scn) :

    c = {}

    # les 3 types de simulation sont actifs/proposes
    c['choix_simulationSimple'] = CHX.VAL_choix_simulationSimple
    c['choix_simulationMultipleModeLineaire'] = CHX.VAL_choix_simulationMultipleModeLineaire
    # notes_de_developpement appel_page.py, contexte_simulation_menu :
    # simulationMultipleModeTotal est temporairement desactive (en attendant 
    # suppression definitive de cette option/traitement)
    #c['choix_simulationMultipleModeTotal'] = CHX.VAL_choix_simulationMultipleModeTotal
    # remplace par 
    c['choix_simulationMultipleModeTotal'] = CHX.VAL_choix_a_cacher

    # autres constantes a transmettre pour identifier les operations
    c['choix_a_cacher'] = CHX.VAL_choix_a_cacher # valeur de comparaison
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo
    c['operation_retour_edition'] = CHX.VAL_operation_retour_edition

    c['scenario'] = scn.scenario_vpz
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine

    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

###############################################################################
# Methode contexte_restitution_menu : constitue et retourne le contexte de
# la page NOM_page_restitution_menu
###############################################################################
def contexte_restitution_menu(request, flashes, scn) :

    c = {}

    # les types de restitution : choix_num est actif/propose ou non
    # en fonction de type_simulation
    c['choix_graph'] = CHX.VAL_choix_graph
    c['choix_telechgt'] = CHX.VAL_choix_telechgt
    type_simulation = scn.resultat.type_simulation
    if type_simulation == "mono_simulation" :
        c['choix_num'] = CHX.VAL_choix_num
    else : # le choix 'affichage numerique' n'apparait pas dans le menu
        c['choix_num'] = CHX.VAL_choix_a_cacher

    # autres constantes a transmettre pour identifier les operations
    c['choix_a_cacher'] = CHX.VAL_choix_a_cacher # valeur de comparaison
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo
    c['operation_retour_edition'] = CHX.VAL_operation_retour_edition
    c['operation_retour_simulation'] = CHX.VAL_operation_retour_simulation

    c['scenario'] = scn.scenario_vpz
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine
    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

###############################################################################
# Methode contexte_telechargement_menu : constitue et retourne le contexte de
# la page NOM_page_telechargement_menu
###############################################################################
def contexte_telechargement_menu(request, flashes, scn) :

    c = {}

    # les 2 types de telechargement
    # (choix_telechgtDernierFichier est propose/actif sous condition)

    c['choix_telechgtDossier'] = CHX.VAL_choix_telechgtDossier

    # nom du dernier fichier de conservation d'une representation graphique
    # S'il n'existe pas (valeur None), alors il ne faut pas proposer le
    # telechargement correspondant)
    nom = scn.resultat.getNomDernierFichierRepresentationGraphique()
    choix_telechgtDernierFichier = CHX.VAL_choix_telechgtDernierFichier # par defaut
    if nom == None :
        nom = ""
        choix_telechgtDernierFichier = CHX.VAL_choix_a_cacher 
    c['choix_telechgtDernierFichier'] = choix_telechgtDernierFichier
    c['nom_dernier_fichier_rg'] = nom

    # autres constantes a transmettre pour identifier les operations
    c['choix_a_cacher'] = CHX.VAL_choix_a_cacher # valeur de comparaison
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo
    c['operation_retour_edition'] = CHX.VAL_operation_retour_edition
    c['operation_retour_simulation'] = CHX.VAL_operation_retour_simulation
    c['operation_retour_restitution'] = CHX.VAL_operation_retour_restitution

    c['scenario'] = scn.scenario_vpz
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine

    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

###############################################################################
# Methode contexte_affichage_numerique : constitue et retourne le contexte de
# la page d'affichage numerique
###############################################################################
def contexte_affichage_numerique(request, flashes, scn) :

    c = {}

    # autres constantes a transmettre pour identifier les operations
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo
    c['operation_retour_edition'] = CHX.VAL_operation_retour_edition
    c['operation_retour_simulation'] = CHX.VAL_operation_retour_simulation
    c['operation_retour_restitution'] = CHX.VAL_operation_retour_restitution

    c['scenario'] = scn.scenario_vpz
    c['type_capture_vars'] = scn.resultat.res.type_capture_vars
    c['vue_vars'] = scn.resultat.res.vue_vars
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine

    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

###############################################################################
# Methode contexte_graphique_menu : constitue et retourne le contexte de
# la page NOM_page_graphique_menu
###############################################################################
def contexte_graphique_menu(request, flashes, scn) :

    c = {}

    # les 2 types de representations graphiques : les 2 choix sont
    # proposes/actifs sans condition, quel que soit le type de simulation 
    c['choix_graphXY'] = CHX.VAL_choix_graphXY
    c['choix_graphY'] = CHX.VAL_choix_graphY

    # autres constantes a transmettre pour identifier les operations
    c['choix_a_cacher'] = CHX.VAL_choix_a_cacher # valeur de comparaison
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo
    c['operation_retour_edition'] = CHX.VAL_operation_retour_edition
    c['operation_retour_simulation'] = CHX.VAL_operation_retour_simulation
    c['operation_retour_restitution'] = CHX.VAL_operation_retour_restitution

    c['scenario'] = scn.scenario_vpz
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine

    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

###############################################################################
# Methode contexte_configuration_graphique : constitue et retourne le contexte
# de la page NOM_page_configuration_graphiqueY ou
# NOM_page_configuration_graphiqueXY 
# (rq : le parametre choix_graph transmis vaut choix_graphY ou choix_graphXY)
###############################################################################
def contexte_configuration_graphique(request, flashes, scn, type_simulation, type_capture_vars, choix_graph ) :

    c = {}

    # le type de simulation :
    c['type_simulation'] = type_simulation

    # les resultats :
    c['type_capture_vars'] = type_capture_vars

    # le type de representation graphique choisi/en cours (a faire suivre) :
    c['choix_graph'] = choix_graph

    # autres constantes a transmettre pour identifier les operations
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo
    c['operation_retour_edition'] = CHX.VAL_operation_retour_edition
    c['operation_retour_simulation'] = CHX.VAL_operation_retour_simulation
    c['operation_retour_restitution'] = CHX.VAL_operation_retour_restitution

    c['scenario'] = scn.scenario_vpz
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine

    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

###############################################################################
# Methode contexte_affichage_graphique : constitue et retourne le contexte
# de la page NOM_page_affichage_graphique
###############################################################################
def contexte_affichage_graphique(request, flashes, scn ) :

    c = {}

    # liste des fichiers existants sous rep_restitutions
    liste_fichiers = scn.espace_exploitation.getListeFichiersRepRestitutions()
    if len(liste_fichiers) == 0 : # liste vide 
        # artifice pour permettre verification presence de liste_fichiers
        liste_fichiers = [ "aucun" ]
    c['liste_fichiers'] = liste_fichiers

    # autres constantes a transmettre pour identifier les operations
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo
    c['operation_retour_edition'] = CHX.VAL_operation_retour_edition
    c['operation_retour_simulation'] = CHX.VAL_operation_retour_simulation
    c['operation_retour_restitution'] = CHX.VAL_operation_retour_restitution

    c['scenario'] = scn.scenario_vpz
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine

    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

