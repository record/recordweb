#-*- coding:utf-8 -*-

###############################################################################
# File mot_code.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
#
# Classes et methodes dediees a gestion d'affichage et saisie de donnees
#
###############################################################################

###############################################################################
# Modele : MotCode
#
# Classe dediee a un mot code respectant un certain format.
#
# Le mot code contient differents champs d'identification et valeurs
# associees. Il se presente sous la forme : "xxx=aaa/yyy=bbb/zzz=ccc".
#
###############################################################################
class MotCode(object):

    def __init__(self, mot) :
        self.mot = mot

    ###########################################################################
    # Methode contientMotCle :
    # Verifie que le mot contient le champ d'identification motcle (plus
    # exactement la chaine de caractere motcle+"=")
    ###########################################################################
    def contientMotCle(self, motcle) :
        return (motcle+'=') in self.mot

    ###########################################################################
    # Methode extraireDeMot :
    # Extrait de mot la valeur correspondant a motcle.
    # Par exemple avec mot "xxx=aaa/yyy=bbb/zzz=ccc" :
    # retourne "bbb" pour motcle="yyy"
    ###########################################################################
    def extraireDeMot(self, motcle) :
        mot_extrait=self.mot.split(motcle+'=')
        mot_extrait=mot_extrait[-1].split('/')
        mot_extrait=mot_extrait[0]
        return mot_extrait

    ###########################################################################
    # Methode enlever_terminaison :
    #
    # Cas particulier d'un mot avec une terminaison de la forme "-r-c".
    #
    # enlever_terminaison enleve de mot sa terminaison qui est de la forme
    # "-r-c" et retourne cette terminaison (r,c).
    # Par contre si mot n'a pas ce type de terminaison, alors ne le modifie
    # pas et retourne None.
    ###########################################################################
    def enlever_terminaison(self) :

        mot_extrait=self.mot.split('-')
        nb = len(mot_extrait)

        if nb < 3 : # terminaison "-r-c" non trouvee
            return None

        else : # terminaison au bon format "-r-c"

            mot_i2=mot_extrait[ nb-1 ] # dernier
            mot_i1=mot_extrait[ nb-2 ] # avant dernier

            if len(mot_i2)>=1 and len(mot_i1)>=1 :

                # suppression de "-r-c" dans mot
                terminaison = '-' + mot_i1 + '-' + mot_i2
                self.mot=self.mot.rstrip( terminaison )

                return (mot_i1,mot_i2)

            if len(mot_i2)>=1 and len(mot_i1)>=1 :
                return None

    ###########################################################################
    # Methode formatBlocVarOk :
    # verifie que mot est de la forme "indbloc=xxx/inddata=zzz", avec bien
    # precisement les champs d'identification 'indbloc' et 'inddata'.
    ###########################################################################
    def formatBlocVarOk(self) :
        if self.contientMotCle('indbloc') and self.contientMotCle('inddata') :
            return True
        else :
            return False

    ###########################################################################
    # Methode formatObsVarOk :
    # Verifie que mot est de la forme "type=xxx/timestep=yyy/nomvar=zzz", avec
    # bien precisement les champs d'identification 'type','timestep','nomvar'.
    # (ce controle fait abstraction d'une eventuelle terminaison "-r-c")
    ###########################################################################
    def formatObsVarOk( self ) :
        if self.contientMotCle('type') and self.contientMotCle('timestep') and self.contientMotCle('nomvar') :
            return True
        else :
            return False

