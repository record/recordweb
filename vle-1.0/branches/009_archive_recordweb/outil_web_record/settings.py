#-*- coding:utf-8 -*-

# Django settings for outil_web_record project.

###############################################################################


PROD = False # True pour prod # False pour local/developpement

# note : la version pyvle est definie dans config.py

###############################################################################
# Variables, Variables d'environnement
###############################################################################

import os
import sys

# RECORDWEB_HOME, ${HOME}/appli_web/django/recordweb
if PROD :
    RECORDWEB_HOME = "/opt/recordweb"
else :
    RECORDWEB_HOME = os.path.join( os.environ.get('HOME'), "appli_web/django/recordweb" )

# vle #########################################################################

# VLE_HOME, ${HOME}/.vle
if PROD :
    VLE_HOME = "/opt/recordweb_depot/VLE"
else :
    VLE_HOME = os.path.join( os.environ.get('HOME'), ".vle" )

# VARIABLE D'ENVIRONNEMENT VLE_HOME
os.environ['VLE_HOME'] = VLE_HOME

# repertoire pkgs de vle VLE_PKGS_PATH, ${VLE_HOME}/pkgs
VLE_PKGS_PATH = os.path.join( os.environ.get('VLE_HOME'), "pkgs" )

# vendors #####################################################################

# repertoire des librairies, applications django externes (django-transmeta...)
# RECORDWEB_VENDOR, ${HOME}/appli_web/django/recordweb_vendor
if PROD :
    RECORDWEB_VENDOR = "/opt/recordweb_vendor"
else :
    RECORDWEB_VENDOR = os.path.join( os.environ.get('HOME'), "appli_web/django/recordweb_vendor" )

# librairies python ###########################################################

# librairies/modules record communs/mis a disposition
# RECORDWEB_HOME/lib
sys.path.insert(0, os.path.join( RECORDWEB_HOME, "lib" ) )

# pour visibilite inter-projets (projet au sens django du terme)
# (par exemple visibilite du projet outil_web_record sur projet
#  bd_modeles_record (gestion de la bd des modeles record) pour son modele)
# RECORDWEB_HOME
sys.path.insert(0, RECORDWEB_HOME )

# django-transmeta
# RECORDWEB_VENDOR/django-transmeta-read-only
sys.path.insert(0, os.path.join( RECORDWEB_VENDOR, "django-transmeta-read-only" ) )

# trace #######################################################################
print ""
print "pour info variables d'environnement"
print "HOME  = ", os.environ.get('HOME')
print "VLE_HOME  = ", os.environ.get('VLE_HOME')
print "sys.path = ", sys.path
print ""
print "pour info variables (settings)"
print "pour info RECORDWEB_HOME  = ", RECORDWEB_HOME
print "pour info VLE_HOME  = ", VLE_HOME
print "pour info VLE_PKGS_PATH  = ", VLE_PKGS_PATH
print "pour info RECORDWEB_VENDOR  = ", RECORDWEB_VENDOR
print ""
###############################################################################

# path du project outil_web_record (correspond a RECORDWEB_HOME/outil_web_record)
SITE_ROOT = os.path.realpath(os.path.dirname(__file__))

# emplacement et nom de la BD des modeles record exploitee par l'outil_web_record
# ( RECORDWEB_HOME / bd_modeles_record_directory/bd_modeles_record.db )
bd_modeles_record = os.path.join( RECORDWEB_HOME, 'bd_modeles_record_directory', 'bd_modeles_record.db' )

# emplacement des templates
# propres a l'outil_web_record : templates_outil_web_record 
# publics : templates_public
templates_outil_web_record = os.path.join( SITE_ROOT, 'templates' )
templates_public = os.path.join( RECORDWEB_HOME, 'templates' )

# emplacement translation file commun recordweb
# (en amont de ceux de chaque projet)
translation_recordweb = os.path.join( RECORDWEB_HOME, 'locale' )

# emplacement des fichiers css et js
# propres a l'outil_web_record : SITE_MEDIA_OUTIL_WEB_RECORD
# publics : SITE_MEDIA_PUBLIC
SITE_MEDIA_OUTIL_WEB_RECORD = os.path.join( SITE_ROOT, 'site_media' )
SITE_MEDIA_PUBLIC = os.path.join( RECORDWEB_HOME, 'site_media' )

# trace #######################################################################
print ""
print "pour info SITE_ROOT = ", SITE_ROOT 
print "pour info bd_modeles_record = ", bd_modeles_record 
print "pour info templates_outil_web_record = ", templates_outil_web_record 
print "pour info templates_public = ", templates_public 
print "pour info translation_recordweb = ", translation_recordweb 
print "pour info SITE_MEDIA_OUTIL_WEB_RECORD = ", SITE_MEDIA_OUTIL_WEB_RECORD 
print "pour info SITE_MEDIA_PUBLIC = ", SITE_MEDIA_PUBLIC 
print ""
###############################################################################

DEBUG = True # True # False
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
    'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
    'NAME': bd_modeles_record, # Or path to database file if using sqlite3.
    'USER': '',                      # Not used with sqlite3.
    'PASSWORD': '',                  # Not used with sqlite3.
    'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
    'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# directories where Django looks for translation files
# (en plus des repertoires 'locale' de projet etc)
# (du plus au moins prioritaire)
LOCALE_PATHS = (
    translation_recordweb,
)

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
#TIME_ZONE = 'America/Chicago'
TIME_ZONE = 'Europe/Paris'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
# enlever 'django.middleware.locale.LocaleMiddleware' pour que LANGUAGE_CODE soit actif
#LANGUAGE_CODE = 'en-us'
LANGUAGE_CODE = 'fr'

#SITE_ID = 1
SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
# attention ! si USE_L10N = True, alors en fr float avec ',' au lieu de '.' 
USE_L10N = False

# pour internationalisation traductions
_ = lambda s: s
LANGUAGES = (
    ('fr', _(u'Français')),
    ('en', _(u'Anglais')),
)

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
# STATIC_ROOT = '' # a ete remplace par
# STATIC_ROOT avec collectstatic
# (pour rafraichir/generer STATIC_ROOT : 'python manage.py collectstatic')
STATIC_ROOT = os.path.join( RECORDWEB_HOME, 'static_root_outil_web_record' )
print "STATIC_ROOT = ", STATIC_ROOT
print ""
print "STATIC_ROOT, repertoire des static files : !!! maj /etc/apache2/sites-available/default !!!"
print ""

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/static/admin/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    
    # (un seul sans ',' apres !)
    #SITE_MEDIA_OUTIL_WEB_RECORD # os.path.join( SITE_ROOT, 'site_media' )
    # a ete remplace par

    # STATICFILES_DIRS avec collectstatic (du + au - prioritaire )
    # (+ maj /site_media/ dans /etc/apache2/sites-available/default !!!)
    SITE_MEDIA_OUTIL_WEB_RECORD, # os.path.join( SITE_ROOT, 'site_media' )
    SITE_MEDIA_PUBLIC,
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '-$v(dyl_n*l9$ot*5*$4v2194qh@jgo%2*g+n+gh(s5#1le0r0'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (

    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware', # internationalisation traductions (doit etre apres SessionMiddleware) (Autodetect user language (from browser) or use session defined language (or fallback to project default)) : a mettre en commentaire pour que LANGUAGE_CODE soit actif
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',

    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.core.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n", # internationalisation traductions (add LANGUAGE_CODE and LANGUAGES variables to your templates)
    "django.core.context_processors.media",
    "django.core.context_processors.debug",
)


ROOT_URLCONF = 'outil_web_record.urls'

TEMPLATE_DIRS = (
    templates_outil_web_record,
    templates_public,
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    #'django.contrib.admin', # to enable the admin
    #'django.contrib.admindocs', # to enable admin documentation

    # application usr outil_web_record
    'scn_selection',
    'scn_edition',
    'scn_simulation',
    'res_graphique',
    'res_numerique',
    'res_telechargement',

    # modele outil_web_record
    'models.modele_record',
    'models.scenariovpz',
    'models.scn',
    'models.conf_web',
    'models.affichage_saisie',
    'models.erreurs_gerees',

    # modele bd_modeles_record (gestion bd des modeles record)
    # (cf RECORDWEB_HOME dans PYTHONPATH)
    'bd_modeles_record.modele_record_bd',

    # utils (ordre ?)
    'utils',
    'record.utils',

    # configuration
    'configs',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

###############################################################################
# SESSION
#
# Using cached sessions (with Memcached) : not ok with Memcached
# or Using file-based sessions
# or Using cookie-based sessions
#
###############################################################################

# Using cached sessions (with Memcached)
#SESSION_ENGINE = 'django.contrib.sessions.backends.cache' # for a simple caching session store
#CACHES = {
#     'default': {
#        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache', # using the python-memcached binding
#        'BACKEND': 'django.core.cache.backends.memcached.PyLibMCCache', # using the python-pylibmc binding
#        'LOCATION': '127.0.0.1:11211', # Memcached is running on localhost (127.0.0.1) port 11211
#        'LOCATION': 'unix:/tmp/memcached.sock', # Memcached is available through a local Unix socket file /tmp/memcached.sock
#    }
#}

# Using file-based sessions
SESSION_ENGINE = 'django.contrib.sessions.backends.file'
#SESSION_FILE_PATH setting (which defaults to output from tempfile.gettempdir(), most likely /tmp)

###############################################################################

