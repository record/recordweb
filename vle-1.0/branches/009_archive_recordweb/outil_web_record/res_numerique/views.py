#-*- coding:utf-8 -*-

###############################################################################
# File views.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
# View res_numerique
#
# Restitution/observation des resultats de la simulation effectuee d'un
# scenario. Parmi les differentes formes de restitution existantes, il s'agit 
# ici de restitution sous forme d'affichage numerique (tableaux de valeurs
# numeriques) : restitution actuellement implementee dans le cas de
# simulation simple (pas en simulation multiple).
#
# Fait partie du processus d'exploitation des scenarios par les utilisateurs,
# selon lequel il s'agit de choisir un scenario (dans un paquet), puis de le
# simuler apres en avoir visualise et eventuellement modifie la configuration
# (parametrage), et enfin d'exploiter sous diverses formes les resultats de
# la simulation effectuee (les visualiser a l'ecran, telecharger...).
#
###############################################################################

#from django.http import HttpResponse
from django.shortcuts import render_to_response

from models.affichage_saisie.appel_page import NomsPages, contexte_restitution_menu, contexte_affichage_numerique
from models.erreurs_gerees.erreurs_gerees import ErreursGerees as ERR # noms des erreurs

from record.utils import session
from record.utils.erreur import Erreur # gestion des erreurs

###############################################################################
#
# AFFICHAGE NUMERIQUE DE RESULTAT DE SIMULATION DU SCENARIO 
#
###############################################################################

###############################################################################
# Une simulation du scenario choisi a ete effectuee (scn.resultat existe).
# Dans le cas d'une simulation simple, preparation de l'affichage des
# resultats de simulation sous forme numerique et affichage numerique.
# Autrement, affichage du menu de restitution avec erreur.
###############################################################################
def affichage_numerique(request):

    scn = session.get(request, 'scn')

    if scn.resultat.type_simulation == "mono_simulation" : # cas de simulation simple

        #######################################################################
        # Preparation du contexte d'affichage numerique
        #######################################################################

        scn = session.get(request, 'scn')
        flashes = [] # pas de flashes
        c = contexte_affichage_numerique(request, flashes, scn)

        return render_to_response( NomsPages.NOM_page_affichage_numerique, c)

    # Cas d'erreur (ne devrait pas arriver)
    else : # affichage du menu de restitution avec erreur

        # creation erreur_affichage_numerique
        erreur_affichage_numerique = Erreur( ERR.NOM_erreur_affichage_numerique ) # desactivee par defaut
        erreur_affichage_numerique.activer()
        message_erreur = "appel affichage numérique dans cas autre que simulation simple"
        erreur_affichage_numerique.set_message( message_erreur )

        #######################################################################
        # Preparation du contexte d'affichage du menu de restitution
        #######################################################################

        scn = session.get(request, 'scn')

        # Liste messages d'erreur en flashes
        flashes = [] # par defaut
        if erreur_affichage_numerique.isActive() : # maj flashes selon erreur_affichage_numerique
            texte_erreur = "Erreur : l\'affichage numérique a échoué."
            texte_message_erreur = "Message d'erreur : " + erreur_affichage_numerique.get_message() + "."
            flashes.append( texte_erreur )
            flashes.append( texte_message_erreur )

        c = contexte_restitution_menu(request, flashes, scn)

        return render_to_response( NomsPages.NOM_page_restitution_menu, c)

