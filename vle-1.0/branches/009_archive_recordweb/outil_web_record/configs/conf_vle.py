#-*- coding:utf-8 -*-

###############################################################################
# File conf_vle.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from django.conf import settings

###############################################################################
#
# CONFIGURATION
#
###############################################################################

###############################################################################
#
# vle
#
###############################################################################

class CONF_vle(object) :

    # Repertoire pkgs sous lequel se trouvent les paquets
    pkgs = settings.VLE_PKGS_PATH

    ###########################################################################
    # Version pyvle
    # (pour les parties de code dependant de la version de pyvle)
    ###########################################################################

    PYVLE101 = 'pyvle101' # pyvle-1.0.1
    PYVLE102 = 'pyvle102' # pyvle-1.0.2

    version_pyvle = PYVLE102

    @classmethod
    def is_pyvle101( cls ) :
        return ( cls.version_pyvle == cls.PYVLE101 )

    @classmethod
    def is_pyvle102( cls ) :
        return ( cls.version_pyvle == cls.PYVLE102 )

    @classmethod
    def message_erreur_version_pyvle( cls, t_err ) :
        msg = "probleme de version pyvle inconnue (" + str(cls.version_pyvle) + ")"
        t_err.message(msg)

###############################################################################

