#-*- coding:utf-8 -*-

###############################################################################
# File conf_modele_record_bd.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
#
# CONFIGURATION
#
###############################################################################

###############################################################################
#
# contexte d'appel de l'application modele_record_bd
#
###############################################################################

class CONF_modele_record_bd(object) :

    # Mode d'utilisation/appel du traitement modele_record_bd
    # vaut True pour administration, False pour utilisation de la base
    # dans projet outil_web_record :
    mode_administration = False

###############################################################################

