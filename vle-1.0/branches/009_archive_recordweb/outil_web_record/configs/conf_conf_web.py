#-*- coding:utf-8 -*-

###############################################################################
# File conf_conf_web.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
#
# CONFIGURATION
#
###############################################################################

###############################################################################
#
# configuration web
#
###############################################################################

###############################################################################
# Constantes : des valeurs attribuees par defaut a des informations affichees
###############################################################################
class CONF_conf_web(object) :

    # Valeurs s'il n'est pas donne/applique de configuration web 

    TXTSTANDARD_prefixe_titre_scenario = "Scenario"
    TXTSTANDARD_description_scenario = ""
    TXTSTANDARD_help_scenario = ""

    TXTSTANDARD_titre_appli = "Application standard"
    TXTSTANDARD_description_appli = "Application standard, sans personnalisation"
    TXTSTANDARD_help_appli = "pas de configuration web donnee/appliquee"

    TXTSTANDARD_titre_page_experience = "Informations generales"
    TXTSTANDARD_description_page_experience = "Page d'informations sur l'experience"
    TXTSTANDARD_help_page_experience = "Correspond sous gvle aux informations des zones 'Experiment' et 'Simulation' de la page 'Project'"

    TXTSTANDARD_titre_page_plan = "Le plan d'experience"
    TXTSTANDARD_description_page_plan = "Page de configuration du plan d'experience pour le cas de simulation multiple : replicaNumber (nombre de repliquas de chaque combinaison) et replicaSeed (graine qui genere les graines de simulation)."
    TXTSTANDARD_help_page_plan = "Une simulation multiple pourra etre effectuee selon un plan d'experience en mode lineaire (combinaison lineaire de chaque index des valeurs) ou en mode total (produit cartesien de chaque valeur). Correspond sous gvle aux informations de la zone 'Plan' de la page 'Project'"

    TXTSTANDARD_prefixe_titre_page_condition = "Famille de parametres" 
    TXTSTANDARD_description_page_condition = ""
    TXTSTANDARD_help_page_condition = "Correspond aux parametres (ports) de la condition du modèle de meme nom"
    TXTSTANDARD_prefixe_web_name_page_condition = "Parametre "

    TXTSTANDARD_prefixe_titre_page_view = "Vue" 
    TXTSTANDARD_description_page_view = ""
    TXTSTANDARD_help_page_view = ""

    # cas particulier sans configuration web sauf pour dictionnaire issu
    # d'une configuration web, adjoint a l'application standard
    TXTSTANDARDAVECDICO_titre_appli = "Application standard (avec dictionnaire)"
    TXTSTANDARDAVECDICO_description_appli = "Application standard, sans personnalisation (sauf dictionnaire)"
    TXTSTANDARDAVECDICO_help_appli = "pas de configuration web donnee/appliquee, sauf pour dictionnaire dont provient la documentation des donnees"

    # Valeurs de forcage si non renseignement dans configuration web donnee/appliquee 

    TXTDEFAUT_prefixe_titre_scenario = "Scenario"
    TXTDEFAUT_description_scenario = ""
    TXTDEFAUT_help_scenario = ""

    TXTDEFAUT_titre_appli = "Application sans nom"
    TXTDEFAUT_description_appli = ""
    TXTDEFAUT_help_appli = ""

    # page web def ou page web res
    TXTDEFAUT_titre_page_web = "Page sans titre"
    TXTDEFAUT_description_page_web = ""
    TXTDEFAUT_help_page_web = ""

    TXTDEFAUT_web_name = "Donnee sans nom"
    TXTDEFAUT_web_name_donnee_exp_name = "Nom de l'expérience"
    TXTDEFAUT_web_name_donnee_exp_duration = "Durée de simulation"
    TXTDEFAUT_web_name_donnee_exp_begin = "Début de simulation"
    TXTDEFAUT_web_name_donnee_simu_seed = "Simulation Seed"
    TXTDEFAUT_web_name_donnee_plan_seed = "Plan Seed"
    TXTDEFAUT_web_name_donnee_plan_number = "Plan Number"

    # Valeurs des id de recherche par defaut d'une donnee (InfosDonneeDef) 
    # dans un dictionnaire
    TXTDEFAUT_id_donnee_exp_name = "experiment_name"
    TXTDEFAUT_id_donnee_exp_duration = "Exp_Duration"
    TXTDEFAUT_id_donnee_exp_begin = "Exp_Begin"
    TXTDEFAUT_id_donnee_simu_seed = "Simu_Seed"
    TXTDEFAUT_id_donnee_plan_seed = "Plan_Seed"
    TXTDEFAUT_id_donnee_plan_number = "Plan_Number"

    # Valeurs de cas d'erreur

    TXTERREUR_titre_appli = "Situation d'erreur"
    TXTERREUR_description_appli = "Application vide/non applicable"
    TXTERREUR_help_appli = "pour un autre essai dans un autre cas de (modele, scenario, application) : 'Retour au choix du scenario'"

###############################################################################

