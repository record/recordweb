
from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('res_graphique.views',

    #url(r'^$', 'graphique_menu'),
    url(r'^graphique_menu/$', 'graphique_menu'),

    url(r'^(?P<id_operation>\d+)/configuration_graphique/$', 'configuration_graphique'),
    url(r'^(?P<id_operation>\d+)/affichage_graphique/$', 'affichage_graphique'),
    url(r'^representation_graphique/$', 'representation_graphique'),
    url(r'^conservation_graphique/$', 'conservation_graphique'),

)
