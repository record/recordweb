
from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('res_telechargement.views',

    #url(r'^$', 'telechargement_menu'),
    url(r'^telechargement_menu/$', 'telechargement_menu'),

    url(r'^(?P<id_operation>\d+)/telechargement/$', 'telechargement'),

)
