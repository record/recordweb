#!/bin/bash

###
# @author Nathalie Rousse, INRA RECORD team member.
# Copyright (C) 2012 INRA
###

# Script de construction de tous les translation files de recordweb
# (une fois ceux-ci crees/mis en place)

echo "construction de tous les translation files de recordweb"

echo "recordweb/locale :"
# (generer/maj .po)
django-admin makemessages -a
# (compiler/generer .mo)
django-admin compilemessages

echo "recordweb/bd_modeles_record/locale :"
cd bd_modeles_record
django-admin makemessages -a
django-admin compilemessages
cd ..

echo "recordweb/outil_web_record/locale :"
cd outil_web_record
django-admin makemessages -a
django-admin compilemessages
cd ..

