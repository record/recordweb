#-*- coding:utf-8 -*-

###############################################################################
# File erreur.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

import traceback

###############################################################################
# Erreur
#
# Erreur survenue en cours de traitement (pour signalisation...)
#
###############################################################################

class Erreur(object):

    # Construction :
    def __init__( self, nom ):
        self.nom = nom
        self.etat = False # etat erreur
        self.message = "" # message erreur 

    # initialisation (valeur d'initialisation)
    def initialiser( self ) :
        self.etat = False
        self.message = ""

    # raz du message d'erreur (etat inchange)
    def raz_message( self ) :
        self.set_message( "" )

    # affectation (valeur texte) du message d'erreur (etat inchange)
    def set_message( self, texte ) :
        self.message = texte

    # retourne le message d'erreur
    def get_message( self ) :
        return self.message

    # activation etat (valeur True) (message inchange)
    def activer( self ) :
        self.etat = True

    # desactivation etat (valeur False), de plus raz message 
    def desactiver( self ) :
        self.etat = False
        self.raz_message()

    # indique etat
    def isActive( self ) :
        return self.etat

def get_message_exception(e) :
    # type(e).__name__ , e.message 
    return traceback.format_exc()

