
from django.conf.urls.defaults import *

# to enable the admin:
from django.contrib import admin
admin.autodiscover()

###############################################################################

urlpatterns = patterns('',

    # internationalisation traductions
    (r'^i18n/', include('django.conf.urls.i18n')),

    ###########################################################################
    #
    # partie administration
    #
    ###########################################################################

    url(r'^', include(admin.site.urls)),

    # synonymes
    url(r'^admin/', include(admin.site.urls)),
    url(r'^adm/', include(admin.site.urls)),

    ###########################################################################
)

