#-*- coding:utf-8 -*-

###############################################################################
# File modele_record_bd.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Authors :
# Nathalie Rousse, INRA RECORD team member,
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
# Modele : ModeleRecord, etc
#
# Le modele record enregistre en bd
#
# Un ModeleRecord comprend le nom de son pkg vle associe et d'autres
# renseignements necessaires a son exploitation : nom de son responsable,
# version vle, eventuel mot de passe...
# Un ModeleRecord ne contient pas la liste (dynamique) de ses scenarios
# (fichiers vpz).
#
# Tout projet qui 'import bd_modeles_record.modele_record_bd' doit
# contenir/declarer une 'class CONF_modele_record_bd' avec
# 'CONF_modele_record_bd.mode_administration' (cf traitements de ModeleRecord,
# methodes relatives a la conf).
#
# notes_de_developpement models.py : Le code de ModeleRecord (cf nom_fichier_config_web_conf, nom_fichier_config_web_applis, nom_fichier_config_dicos) pourrait evoluer/etre modifie afin de ne plus avoir a imposer l'emplacement de ces fichiers (cf CONF_rep_configuration.repertoire_racine_configuration, repertoire_config_web_conf, repertoire_config_web_applis, repertoire_config_dicos)
#
###############################################################################

from django.db import models
from django.contrib.auth.models import User
from record.utils.exp import get_liste_brute_pkgs

from django import forms

from django.utils.translation import ugettext as _

from transmeta import TransMeta

# uniquement en traitement d'administration de la base (cf dans code)
#from configs.conf_modele_record_bd import CONF_modele_record_bd
#from configs.conf_rep_configuration import CONF_rep_configuration

###############################################################################
#
# Methodes relatives a la conf
#
###############################################################################

def is_conf_modele_record_bd_mode_admin() :
    from configs.conf_modele_record_bd import CONF_modele_record_bd
    return ( CONF_modele_record_bd.mode_administration )

def get_conf_repertoire_racine_configuration() :
    if is_conf_modele_record_bd_mode_admin() :
        from configs.conf_rep_configuration import CONF_rep_configuration
        return CONF_rep_configuration.repertoire_racine_configuration
    else : # not conf_modele_record_bd_mode_admin ou autre
        return ""
def get_conf_repertoire_config_web_conf() :
    if is_conf_modele_record_bd_mode_admin() :
        from configs.conf_rep_configuration import CONF_rep_configuration
        return CONF_rep_configuration.repertoire_config_web_conf
    else : # not conf_modele_record_bd_mode_admin ou autre
        return ""
def get_conf_repertoire_config_web_applis() :
    if is_conf_modele_record_bd_mode_admin() :
        from configs.conf_rep_configuration import CONF_rep_configuration
        return CONF_rep_configuration.repertoire_config_web_applis
    else : # not conf_modele_record_bd_mode_admin ou autre
        return ""
def get_conf_repertoire_config_dicos() :
    if is_conf_modele_record_bd_mode_admin() :
        from configs.conf_rep_configuration import CONF_rep_configuration
        return CONF_rep_configuration.repertoire_config_dicos
    else : # not conf_modele_record_bd_mode_admin ou autre
        return ""

###############################################################################
# MotDePasse
#
# chq ModeleRecord prive est associe a un MotDePasse
# Un meme MotDePasse peut servir pour plusieurs ModeleRecord prives
#
# MotDePasse est derive de User. Seuls certains champs de User sont
# exploites/geres ('username', 'password').
#
###############################################################################
class MotDePasse(User):

    class Meta :
        verbose_name = _(u"Mot de passe de Modèle(s) Record")
        verbose_name_plural = _(u"Mots de passe de Modèle(s) Record")

    def __unicode__(self):
        return 'mot de passe %s' % (self.username)

###############################################################################
# Individu
#
# les responsable, responsable_scientifique et responsable_informatique des
# ModeleRecord sont des Individu.
# Un meme Individu peut jouer differents roles vis-a-vis de differents
# ModeleRecord
#
# Individu est derive de User. Seuls certains champs de User sont
# exploites/geres ('username', 'first_name', 'last_name', 'email').
#
###############################################################################
class Individu(User):

    class Meta :
        verbose_name = _(u"Individu")
        verbose_name_plural = _(u"Individus")

    def __unicode__(self):
        return 'individu %s' % (self.username)


###############################################################################
# FilePathFieldConfig... :
#
# Classes utilitaires pour les noms des fichiers vpz de configuration
# (nom_fichier_config_web_conf, nom_fichier_config_web_applis
# et nom_fichier_config_dicos) de ModeleRecord
#
###############################################################################

# FilePathField pour des noms de fichiers de configuration (partie commune)
class FilePathFieldConfig( models.FilePathField ) :

    # determine/construit et retourne liste_choix qui est la liste des paires
    # (nom,nom) avec nom : nom absolu fichier vpz de configuration
    def get_liste_choix( self ) :

        # recuperation de choices : paires (nom_absolu,nom_depuis_path)
        defaults = { 'path': self.path,
                     'match': self.match,
                     'recursive': self.recursive,
                     'form_class': forms.FilePathField, }
        obj= super(models.FilePathField, self).formfield(**defaults)
        choices = obj.choices

        # liste_choix : paires (nom_absolu,nom_absolu)
        liste_choix = [ (nom_absolu,nom_absolu) for (nom_absolu,nom_depuis_path) in choices ]

        return liste_choix

    # determine si le nom absolu nom_absolu de fichier correspondant a
    # choice=(nom absolu,nom) se situe ou non sous un repertoire nomme
    # nom_repertoire
    @classmethod
    def estSousRepertoire( cls, nom_repertoire, choice ) :
        cr = False # par defaut
        (nom_absolu,nom) = choice
        decoupage = nom_absolu.split("/")
        if len(decoupage) >= 2 :
            le_repertoire = decoupage[-2]
            if le_repertoire == nom_repertoire :
                cr = True
        return cr

# Methode a n'appeler qu'en conf_modele_record_bd_mode_admin
# FilePathField pour des noms de fichiers de configuration web
# propres a la conf web
class FilePathFieldConfigWebConf( FilePathFieldConfig ) :

    # determine/construit et retourne liste_choix qui est la liste des paires
    # (nom,nom) avec nom : nom absolu fichier vpz de configuration
    # filtree relativement a configuration web propre a la conf web
    def get_liste_choix_noms_absolus_fichiers( self ) : 

        choices = self.get_liste_choix()

        # ne garder que ceux situes sous repertoire repertoire_config_web_conf
        # ie nom absolu de la forme */repertoire_config_web_conf/*.*
        # (le filtre par rapport a l'extension a deja eu lieu)
        liste_choix = []
        for choice in choices:
            if self.estSousRepertoire( get_conf_repertoire_config_web_conf(), choice ) :
                liste_choix.append( choice )
        return liste_choix

# Methode a n'appeler qu'en conf_modele_record_bd_mode_admin
# FilePathField pour des noms de fichiers de configuration web
# propres aux applis web
class FilePathFieldConfigWebApplis( FilePathFieldConfig ) :

    # determine/construit et retourne liste_choix qui est la liste des paires
    # (nom,nom) avec nom : nom absolu fichier vpz de configuration
    # filtree relativement a configuration web propre aux applis web
    def get_liste_choix_noms_absolus_fichiers( self ) : 

        choices = self.get_liste_choix()

        # ne garder que ceux situes sous repertoire repertoire_config_web_applis
        # ie nom absolu de la forme */repertoire_config_web_applis/*.*
        # (le filtre par rapport a l'extension a deja eu lieu)
        liste_choix = []
        for choice in choices:
            if self.estSousRepertoire( get_conf_repertoire_config_web_applis(), choice ) :
                liste_choix.append( choice )
        return liste_choix

# Methode a n'appeler qu'en conf_modele_record_bd_mode_admin
# FilePathField pour des noms de fichiers de configuration
# propres aux dictionnaires
class FilePathFieldConfigDicos( FilePathFieldConfig ) :

    # determine/construit et retourne liste_choix qui est la liste des paires
    # (nom,nom) avec nom : nom absolu fichier vpz de configuration
    # filtree relativement a configuration propre aux dictionnaires
    def get_liste_choix_noms_absolus_fichiers( self ) : 

        choices = self.get_liste_choix()

        # ne garder que ceux situes sous repertoire repertoire_config_dicos
        # ie nom absolu de la forme */repertoire_config_dicos/*.*
        # (le filtre par rapport a l'extension a deja eu lieu)
        liste_choix = []
        for choice in choices:
            if self.estSousRepertoire( get_conf_repertoire_config_dicos(), choice ) :
                liste_choix.append( choice )
        return liste_choix

###############################################################################
# ModeleRecord
#
# les ModeleRecord sont les modeles record enregistres en bd, mis a
# disposition au sein de l'outil Web Record
#
# Un ModeleRecord comprend le nom de son pkg vle associe et d'autres
# renseignements : nom de son responsable, version vle, mot de passe (mot de
# passe prive ou mot de passe modele_public) ...
#
# Note : pour en faciliter la saisie (par etape), un ModeleRecord peut etre
# enregistre sans que tous ses champs n'aient ete saisis (champs optionnels).
# Mais tous les champs sont obligatoires (sauf les fichiers vpz de
# configuration nom_fichier_config_web_conf, nom_fichier_config_web_applis,
# nom_fichier_config_dicos) pour que le ModeleRecord puisse etre
# choisi/exploite dans l'outil Web Record (verification au sein de l'outil
# Web Record).
#
###############################################################################
class ModeleRecord(models.Model):

    __metaclass__ = TransMeta

    ###########################################################################
	# nom du modele 
    ###########################################################################
    nom = models.CharField( _(u"Nom"), max_length=200 )
    nom.help_text=_(u"Nom donné au modèle record dans la base")

    ###########################################################################
	# description du modele 
    ###########################################################################
    description = models.TextField( _(u"Description"), max_length=500)
    #description.default="azerty"
    description.help_text=_(u"Description du modèle record")
    description.blank = True # optionnel pour faciliter saisie

    ###########################################################################
    # correspondance avec/relation a pkgs vle
    ###########################################################################

    # nom effectif du paquet sous pkgs (paquet principal du modele)
    liste_choix = [ (pkg,pkg) for pkg in get_liste_brute_pkgs() ]
    nom_pkg = models.CharField( _(u"Nom du paquet principal"), max_length=200, choices=liste_choix )
    nom_pkg.help_text=_(u"Nom du paquet principal (project, sous pkgs) : celui qui contient les fichiers vpz scénario de simulation du modèle")
    nom_pkg.blank = True # optionnel pour faciliter saisie

    ###########################################################################
    # accessibilite (modele prive/public)
    ###########################################################################

	# mot de passe du paquet (si acces prive)
    mot_de_passe = models.ForeignKey(MotDePasse)
    mot_de_passe.help_text=_(u"Associer à un modèle privé un des mots de passe existants ou en créer un nouveau (pas de mot de passe pour un modèle public)")
    mot_de_passe.null = True
    mot_de_passe.blank = True # optionnel (c'est le seul champ reellement optionnel ; pas de mot_de_passe pour un modele public)

    ###########################################################################
    # Fichiers vpz de configuration : 
    # En l'etat actuel des choses, la seule syntaxe imposee pour le
    # nom (absolu) des 3 fichiers vpz de configuration est l'extension 
    # extension_config.
    # Dans la saisie/enregistrement en BD de ces noms de fichiers, les
    # fichiers presentes/proposes a l'usr se situent sous path_config.
    # L'emplacement est impose pour faciliter/simplifier la
    # recherche/presentation des fichiers au niveau de saisie/enregistrement
    # en BD, mais ce n'est en rien une obligation au niveau de
    # l'outil_web_record ou aucune contrainte n'est imposee pour ces noms
    # absolus de fichiers).
    ###########################################################################

    max_length_config=500

    if is_conf_modele_record_bd_mode_admin() :

        path_config = get_conf_repertoire_racine_configuration()
        extension_config = "vpz"
        match_config = ".*\." + extension_config + "$"

        # nom (absolu) du fichier vpz de configuration web propre a la conf web
        file_path_field = FilePathFieldConfigWebConf( max_length=max_length_config, path=path_config, match=match_config, recursive=True )
        liste_choix = file_path_field.get_liste_choix_noms_absolus_fichiers() 
        nom_fichier_config_web_conf = models.CharField( _(u"configuration web conf"), max_length=max_length_config, choices=liste_choix )
        nom_fichier_config_web_conf.help_text=_(u"Nom (absolu) du fichier vpz de configuration web propre à la conf web. Souvent nommé web_conf.vpz mais sans obligation (nom libre). Seuls sont proposés les fichiers situés sous un répertoire") + " " + get_conf_repertoire_config_web_conf() + " " +_(u"de") + " " + path_config + "."
        nom_fichier_config_web_conf.blank = True # optionnel pour faciliter saisie

        # nom (absolu) du fichier vpz de configuration web propre aux applis web
        file_path_field = FilePathFieldConfigWebApplis( max_length=max_length_config, path=path_config, match=match_config, recursive=True )
        liste_choix = file_path_field.get_liste_choix_noms_absolus_fichiers() 
        nom_fichier_config_web_applis = models.CharField( _(u"configuration web applis"), max_length=max_length_config, choices=liste_choix )
        nom_fichier_config_web_applis.help_text=_(u"Nom (absolu) du fichier vpz de configuration web propre aux applis web. Souvent nommé web_applis.vpz mais sans obligation (nom libre). Seuls sont proposés les fichiers situés sous un répertoire") + " " + get_conf_repertoire_config_web_applis() + " " +_(u"de") + " " + path_config + "."
        nom_fichier_config_web_applis.blank = True # optionnel pour faciliter saisie
    
        # nom (absolu) du fichier vpz de configuration propre au dictionnaire
        file_path_field = FilePathFieldConfigDicos( max_length=max_length_config, path=path_config, match=match_config, recursive=True )
        liste_choix = file_path_field.get_liste_choix_noms_absolus_fichiers() 
        nom_fichier_config_dicos = models.CharField( _(u"configuration dictionnaire"), max_length=max_length_config, choices=liste_choix )
        nom_fichier_config_dicos.help_text=_(u"Nom (absolu) du fichier vpz de configuration propre au dictionnaire. Souvent nommé dicos.vpz mais sans obligation (nom libre). Seuls sont proposés les fichiers situés sous un répertoire") + " " + get_conf_repertoire_config_dicos() + " "+_(u"de")+ " " + path_config + "."
        nom_fichier_config_dicos.blank = True # optionnel pour faciliter saisie

    else : # cas not conf_modele_record_bd_mode_admin
        nom_fichier_config_web_conf = models.CharField( _(u"configuration web conf"), max_length=max_length_config )
        nom_fichier_config_web_applis = models.CharField( _(u"configuration web applis"), max_length=max_length_config, choices=liste_choix )
        nom_fichier_config_dicos = models.CharField( _(u"configuration dictionnaire"), max_length=max_length_config, choices=liste_choix )

    # commun aux cas conf_modele_record_bd_mode_admin et not conf_modele_record_bd_mode_admin
    nom_fichier_config_web_conf.blank = True # optionnel pour faciliter saisie
    nom_fichier_config_web_applis.blank = True # optionnel pour faciliter saisie
    nom_fichier_config_dicos.blank = True # optionnel pour faciliter saisie

    ###########################################################################
    # Responsables (individus)
    ###########################################################################

	# responsable du modele record
	# qui en est le correspondant pour administrateur(s),
	# qui en distribue le mot de passe aux personnes de son choix.
    responsable = models.ForeignKey(Individu, related_name=_(u"responsable") )
    responsable.help_text=_(u"Correspondant principal. Dans le cas de modèle privé, il distribue le mot de passe aux personnes de son choix.")
    responsable.null = True
    responsable.blank = True # optionnel pour faciliter saisie

    # responsable scientifique
    responsable_scientifique = models.ForeignKey(Individu, related_name=_(u"responsable_scientifique") )
    responsable_scientifique.help_text=_(u"Correspondant pour les questions scientifiques")
    responsable_scientifique.null = True 
    responsable_scientifique.blank = True # optionnel pour faciliter saisie

    # responsable informatique
    responsable_informatique = models.ForeignKey(Individu, related_name=_(u"responsable_informatique") )
    responsable_informatique.help_text=_(u"Correspondant pour les questions informatiques")
    responsable_informatique.null = True 
    responsable_informatique.blank = True # optionnel pour faciliter saisie

    ###########################################################################
    # compte-rendu de reception/recette
    ###########################################################################
    cr_reception = models.TextField( _(u"Compte rendu de reception") , max_length=500)
    cr_reception.help_text=_(u"Note rapport de la procédure de recette du modele record")
    cr_reception.blank = True # optionnel pour faciliter saisie

    ###########################################################################
	# version vle sous laquelle le paquet a ete receptionne
    ###########################################################################
    version_vle = models.CharField( _(u"Version vle de reception") , max_length=200)
    version_vle.help_text=_(u"Version vle sous laquelle le modèle a été réceptionné")
    version_vle.blank = True # optionnel pour faciliter saisie


    class Meta :
        verbose_name = _(u"Modèle Record")
        verbose_name_plural = _(u"Modèles Record")

        translate = ( 'nom', 'description', 'cr_reception' )

    def __unicode__(self):
        return 'modele %s (pkg %s)' % (self.nom, self.nom_pkg)

###############################################################################
