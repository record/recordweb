#-*- coding:utf-8 -*-

###############################################################################
# File admin.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Authors :
# Nathalie Rousse, INRA RECORD team member,
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
# notes_de_developpement admin.py :
#
# Il manque la creation par defaut du MotDePasse modele_public dans la BD des modeles record lors de sa creation/initialisation => en l'etat actuel des choses, MotDePasse modele_public ajoute/cree manuellement dans la BD des modeles record.
#
# La page d'administration de la BD des modeles record pourrait etre mieux personnalisee (cf labels).
#
###############################################################################

from django.contrib import admin

from django.contrib.auth.admin import UserAdmin

from models import Individu, MotDePasse, ModeleRecord

###############################################################################
# Pour que certaines parties/rubriques administratives n'apparaissent pas
###############################################################################
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.contrib.auth.models import Group

# admin.site.unregister(User) # partie User (en commentaire pour voir les comptes administrateur)
admin.site.unregister(Group) # partie Group
admin.site.unregister(Site) # partie Site

from django.utils.translation import ugettext as _

from transmeta import canonical_fieldname

###############################################################################
#
# Administration MotDePasse
#
###############################################################################
class MotDePasseAdmin(UserAdmin):

    texte_description = _(u"Un mot de passe est associé à un ou plusieurs modèles record privés. Les modèles record publics ont pour mot de passe modele_public.")

    fieldsets = (

        ( _(u"MOT DE PASSE"), { 'description': texte_description,
                            'fields': () } ),

        ( None, { 'fields': ('username',
                            'password') } ),
    )

admin.site.register(MotDePasse, MotDePasseAdmin)

###############################################################################
#
# Administration Individu
#
###############################################################################
class IndividuAdmin(UserAdmin):

    texte_description = _(u"Un individu est une personne susceptible d\'endosser le rôle de responsable et/ou responsable scientifique et/ou responsable informatique par rapport à un ou plusieurs modèles record")

    fieldsets = (

        ( _(u"INDIVIDU"), { 'description': texte_description,
                        'fields': () } ),

        ( None, { 'fields': ('username',
                        'first_name',
                        'last_name',
                        'email') } ),
    )

admin.site.register(Individu, IndividuAdmin)

###############################################################################
#
# Administration ModeleRecord
#
###############################################################################
class ModeleRecordAdmin(admin.ModelAdmin):

    list_display = ('nom', 'nom_pkg')

    texte_description = _(u"Les modèles record sont les modèles mis à disposition au sein de l\'outil Web Record (à condition que tous les champs en soient renseignés). Un modèle record est caractérisé par son paquet principal (contenant ses fichiers vpz scénario de simulation) et un certain nombre d\'autres renseignements nécessaires à son exploitation. Il a un mot de passe privé ou alors son mot de passe est modele_public s\'il est public. Ses responsables sont des individus. Les 3 fichiers vpz de configuration servent à personnaliser la présentation du modèle (ses scénarios et leurs données) au sein de l\'outil Web Record.")

    #
    # pour memo : methode canonical_fieldname
    # pour relation entre 'nom'... et 'nom_fr','nom_en'...
    #
    #def formfield_for_dbfield(self, db_field, **kwargs):
    #    field = super(ModeleRecordAdmin, self).formfield_for_dbfield(db_field, **kwargs)
    #    db_fieldname = canonical_fieldname(db_field)
    #
    #        print "db_field.name : ", db_field.name, "  ***  ", "db_fieldname : ", db_fieldname
    #
    #        if db_fieldname == 'description':
    #        # this applies to all description_* fields
    #        print "!!! description"
    #        #field.widget = MyCustomWidget()
    #    elif db_field.name == 'nom_en':
    #        # this applies only to body_es field
    #        print "!!! nom_en"
    #        ###field.widget = MyCustomWidget()
    #    return field
    #

    # tous les champs sont affiches sauf file_path_field (champ interne pour
    # 'nom_fichier_config_...')
    exclude = ( 'file_path_field', )

    fieldsets = (

        ( _(u"MODELE RECORD"), { 'description': texte_description,
                             'fields': () } ),

        ( None, { 'fields': ( 'nom_fr',
                              'nom_en',
                              'description_fr',
                              'description_en',
                              'nom_pkg',
                              'mot_de_passe',
                              'responsable',
                              'responsable_scientifique',
                              'responsable_informatique',
                              'version_vle',
                              'cr_reception_fr',
                              'cr_reception_en' ) } ),
        ( _(u"CONFIGURATION PERSONNALISATION (optionnel)"), { 'fields': ( 'nom_fichier_config_web_conf',
                              'nom_fichier_config_web_applis',
                              'nom_fichier_config_dicos') } ),
    )

    list_filter = [ 'nom_fr',
                    'nom_en',
                    'nom_pkg',
                    'version_vle',
                    #'nom_fichier_config_web_conf',
                    #'nom_fichier_config_web_applis',
                    #'nom_fichier_config_dicos',
                    'responsable',
                    'responsable_scientifique',
                    'responsable_informatique',
                    'mot_de_passe' ]

    search_fields = [ 'nom_fr',
                    'nom_en',
                    'nom_pkg',
                    'version_vle',
                    'responsable',
                    'responsable_scientifique',
                    'responsable_informatique' ]

admin.site.register(ModeleRecord, ModeleRecordAdmin)

###############################################################################

