#-*- coding:utf-8 -*-

###############################################################################
# File conf_rep_configuration.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from django.conf import settings

###############################################################################
#
# CONFIGURATION
#
###############################################################################

###############################################################################
#
# personnalisation (configuration web, dictionnaires)
#
###############################################################################

class CONF_rep_configuration(object) :

    # Repertoire racine sous lequel est cherche l'ensemble des fichiers vpz
    # de configuration
    repertoire_racine_configuration = settings.CONFIGURATION_PATH

    # Noms des repertoires susceptibles de contenir des fichiers vpz de
    # configuration web propres a la conf web
    repertoire_config_web_conf="web"

    # Noms des repertoires susceptibles de contenir des fichiers vpz de
    # configuration web propres aux applis web
    repertoire_config_web_applis="web"

    # Noms des repertoires susceptibles de contenir des fichiers vpz de
    # configuration propres aux dictionnaires
    repertoire_config_dicos="meta"

###############################################################################

