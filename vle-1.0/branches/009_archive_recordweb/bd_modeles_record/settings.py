#-*- coding:utf-8 -*-

# Django settings for bd_modeles_record project.

###############################################################################



PROD = False # True pour prod # False pour local/developpement


###############################################################################
# Variables, Variables d'environnement
###############################################################################

import os
import sys


# RECORDWEB_HOME, ${HOME}/appli_web/django/recordweb
if PROD :
    RECORDWEB_HOME = "/opt/recordweb"
else :
    RECORDWEB_HOME = os.path.join( os.environ.get('HOME'), "appli_web/django/recordweb" )

# vle #########################################################################

# VLE_HOME, ${HOME}/.vle
if PROD :
    VLE_HOME = "/opt/recordweb_depot/VLE"
else :
    VLE_HOME = os.path.join( os.environ.get('HOME'), ".vle" )

# VARIABLE D'ENVIRONNEMENT VLE_HOME
os.environ['VLE_HOME'] = VLE_HOME

# configuration/personnalisation ##############################################

# repertoire racine sous lequel est cherche l'ensemble des fichiers de configuration (pas partout, seulement dans certains sous repertoires, cf code)
# CONFIGURATION_PATH, ${HOME}/workspace_svn/web_depot
if PROD :
    CONFIGURATION_PATH = "/opt/recordweb_depot"
else :
    CONFIGURATION_PATH = os.path.join( os.environ.get('HOME'), "appli_web/django/recordweb_depot" )

# vendors #####################################################################

# repertoire des librairies, applications django externes (django-transmeta...)
# RECORDWEB_VENDOR, ${HOME}/appli_web/django/recordweb_vendor
if PROD :
    RECORDWEB_VENDOR = "/opt/recordweb_vendor"
else :
    RECORDWEB_VENDOR = os.path.join( os.environ.get('HOME'), "appli_web/django/recordweb_vendor" )

# librairies python ###########################################################

# librairies/modules record communs/mis a disposition
# RECORDWEB_HOME/lib
sys.path.insert(0, os.path.join( RECORDWEB_HOME, "lib" ) )

# pour visibilite inter-projets (projet au sens django du terme)
# (par exemple visibilite du projet outil_web_record sur projet
#  bd_modeles_record (gestion de la bd des modeles record) pour son modele)
# RECORDWEB_HOME
sys.path.insert(0, RECORDWEB_HOME )

# django-transmeta
# RECORDWEB_VENDOR/django-transmeta-read-only
sys.path.insert(0, os.path.join( RECORDWEB_VENDOR, "django-transmeta-read-only" ) )

# trace #######################################################################
print ""
print "pour info variables d'environnement"
print "HOME  = ", os.environ.get('HOME')
print "VLE_HOME  = ", os.environ.get('VLE_HOME')
print "sys.path = ", sys.path
print ""
print "pour info variables (settings)"
print "pour info RECORDWEB_HOME  = ", RECORDWEB_HOME
print "pour info VLE_HOME  = ", VLE_HOME
print "pour info CONFIGURATION_PATH = ", CONFIGURATION_PATH
print "pour info RECORDWEB_VENDOR  = ", RECORDWEB_VENDOR
print ""
###############################################################################

# path du project bd_modeles_record (correspond a RECORDWEB_HOME/bd_modeles_record)
SITE_ROOT = os.path.realpath(os.path.dirname(__file__))

# emplacement et nom de la BD des modeles record exploitee par l'outil_web_record
# ( RECORDWEB_HOME / bd_modeles_record_directory/bd_modeles_record.db )
bd_modeles_record = os.path.join( RECORDWEB_HOME, 'bd_modeles_record_directory', 'bd_modeles_record.db' )

# emplacement des templates
# propres a projet bd_modeles_record : templates_bd_modeles_record 
# publics : templates_public
templates_bd_modeles_record = os.path.join( SITE_ROOT, 'templates' )
templates_public = os.path.join( RECORDWEB_HOME, 'templates' )

# emplacement translation file commun recordweb
# (en amont de ceux de chaque projet)
translation_recordweb = os.path.join( RECORDWEB_HOME, 'locale' )

# trace #######################################################################
print ""
print "pour info SITE_ROOT = ", SITE_ROOT 
print "pour info bd_modeles_record = ", bd_modeles_record
print "pour info templates_bd_modeles_record = ", templates_bd_modeles_record
print "pour info templates_public = ", templates_public
print "pour info translation_recordweb = ", translation_recordweb
print ""
###############################################################################

DEBUG = True # True # False
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': bd_modeles_record, # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# directories where Django looks for translation files
# (en plus des repertoires 'locale' de projet etc)
# (du plus au moins prioritaire)
LOCALE_PATHS = (
    translation_recordweb,
)

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
#TIME_ZONE = 'America/Chicago'
TIME_ZONE = 'Europe/Paris'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
# enlever 'django.middleware.locale.LocaleMiddleware' pour que LANGUAGE_CODE soit actif
# LANGUAGE_CODE : default language in/for the site
#LANGUAGE_CODE = 'en-us'
LANGUAGE_CODE = 'en'
#LANGUAGE_CODE = 'fr'

# TRANSMETA_DEFAULT_LANGUAGE : default language to transmeta
TRANSMETA_DEFAULT_LANGUAGE = 'fr'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
# attention ! si USE_L10N = True, alors en fr float avec ',' au lieu de '.'
USE_L10N = False

# pour internationalisation traductions : redefinition LANGUAGES a laisser ?
_ = lambda s: s
LANGUAGES = (
    ('fr', _(u'Français')),
    ('en', _(u'Anglais')),
)

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = ''

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/static/admin/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'uznws-#4&ee6!)y&xlo@z!9vogvb@7ebby9)f^)tdu@2@owyrz'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    #'django.middleware.locale.LocaleMiddleware', # internationalisation traductions (doit etre apres SessionMiddleware) (Autodetect user language (from browser) or use session defined language (or fallback to project default)) : en commentaire pour que LANGUAGE_CODE soit actif
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',

    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.core.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n", # internationalisation traductions
    "django.core.context_processors.media",
    "django.core.context_processors.debug",
)

ROOT_URLCONF = 'bd_modeles_record.urls'

TEMPLATE_DIRS = (
    templates_bd_modeles_record,
    templates_public,
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin', # to enable the admin

    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',

    'transmeta',

    # application administration bd des modeles record
    'modele_record_bd',

    # utils (ordre ?)
    'record.utils',

    # configs 
    'configs',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}
