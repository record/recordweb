#-*- coding:utf-8 -*-

# Gestion de l'authentification

# from pylons.templating import render_mako as render
from authkit.permissions import ValidAuthKitUser
from authkit.authorize.pylons_adaptors import authorized
from authkit.permissions import HasAuthKitRole


is_valid_user = ValidAuthKitUser()
has_admin_role = HasAuthKitRole(['admin'])
has_public_role = HasAuthKitRole(['public'])

