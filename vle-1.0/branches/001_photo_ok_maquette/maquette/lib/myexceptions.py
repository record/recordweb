#-*- coding:utf-8 -*-

class ConvertionError(Exception):
    """Exception pour signaler une erreur de convertion de type"""

    def __init__(self, value, param, type):
        message = "Error converting `%s` value in %s for param `%s`" % \
                                        (value, type, param)
        super(ConvertionError, self).__init__(message)

class SimuError(Exception):
    """Exception pour signaler une erreur de simulation"""
    
    def __init__(self, dic):
        """Initialisation

        @param dic: un dictionnaire du type:
            C{{'simu':nom, 'year':année, 'vle_msg':message_de_vle}}
        """
        Exception.__init__(self)
        self.message = dic
