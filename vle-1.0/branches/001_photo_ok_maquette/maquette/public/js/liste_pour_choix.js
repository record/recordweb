/*
 * File liste_pour_choix.js
 *
 * Application Web RECORD nom_appli_web_record_a_definir
 *
 * Author : Nathalie Rousse, INRA RECORD team member.
 *
 * Copyright (C) 2011 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*****************************************************************************
 * liste_pour_choix.js
 *
 * Proposition/affichage d'une liste en support a la saisie d'un champ
 * (choix de sa valeur dans la liste).
 *
 *****************************************************************************/

/*
 * A la selection d'une valeur dans 'liste', affichage de cette valeur dans
 * 'champ'.
 */
function onSelectChange( liste, champ){

    var selected = liste+" option:selected";
    var valeur = $(selected).val();
    $(champ).val(valeur);
}

