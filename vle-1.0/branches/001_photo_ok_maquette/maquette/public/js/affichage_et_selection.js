/*
 * File affichage_et_selection.js
 *
 * Application Web RECORD nom_appli_web_record_a_definir
 *
 * Author : Nathalie Rousse, INRA RECORD team member.
 *
 * Copyright (C) 2011 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*****************************************************************************
 * affichage_et_selection.js
 *
 * Gestion de l'affichage selectif d'informations en fonction de choix de 
 * selection effectues (criteres...).
 *
 *****************************************************************************/

/****************************************************************************** 
 *
 * A la demande 'dmd_tous', tous les elements de type 'selection' sont
 * affiches, qu'ils soient selectionnes ou non.
 * A la demande 'dmd_selection', les elements de type 'selection' qui sont
 * selectionnes sont affiches (pas ceux qui sont deselectionnes).
 *
 ******************************************************************************/
function affichage_selection_ou_tous( dmd_selection, dmd_tous, selection )
{
    var elt_selectionne="input."+selection+":not(:not(:checked))";
    var elt_non_selectionne="input."+selection+":not(:checked))";

    $(dmd_selection).click( function () {
	    $(elt_non_selectionne).parent().parent().hide();
	    $(elt_selectionne).parent().parent().show();
    });

    $(dmd_tous).click( function () {
	    $(elt_non_selectionne).parent().parent().show();
	    $(elt_selectionne).parent().parent().show();
    });
}

/****************************************************************************** 
 *
 * A la demande 'dmd_tous',
 * tous les elements de type 'name' sont selectionnes.
 * A la demande 'dmd_aucun',
 * tous les elements de type 'name' sont deselectionnes.
 *
 ******************************************************************************/
function selection_tous_ou_aucun( dmd_tous, dmd_aucun, name )
{
    var element="input[name="+name+"]";

    $(dmd_tous).click(function(){
        $(element).each(function(){
	        this.checked = "checked";
        });
    });					

    $(dmd_aucun).click(function(){
        $(element).each(function(){
	        this.checked = !("checked");
        });
    });					
}

/****************************************************************************** 
 *
 * Une seule selection du type 'name'.
 * Quand un element du type 'name' est selectionne, tous les autres sont
 * deselectionnes. Seul le dernier element selectionne le reste.
 *
 ******************************************************************************/
function selection_un_seul( name )
{
    var element="input[name="+name+"]";

    $(element).click( function () {
        $(element).each(function(){
            this.checked = !("checked");
        });
        this.checked = "checked";
    });
}

/****************************************************************************** 
 *
 * Dans 'recap', texte recapitulatif des elements selectionnes du type 'name'.
 * Le texte recapitulatif commence par 'texte'.
 *
 * Un element selectionne a une valeur de la forme :
 * type=iii/timestep=vvv/nomvar=aaa,bbb,ccc:DDD.VVV
 *
 ******************************************************************************/
function recapitulatif_selection( name, recap, texte )
{
    var element="input[name="+name+"]";

    $(element).click( function () {

        $(recap).html( texte );

        var elt_selectionne=element+":not(:not(:checked))";
        $(elt_selectionne).each(function(){

            // nomcode est de la forme : 
            // type=iii/timestep=vvv/nomvar=aaa,bbb,ccc:DDD.VVV
            var nomcode = $(this).val();

            // nomcode = nomcode.split(':')[1]; // pour nom raccourci/relatif
            nomcode = nomcode.split('nomvar=')[1]; // pour nom entier

            var texte = nomcode + ", ";
            $(recap).append( texte );
        });
    });
}

/****************************************************************************** 
 *
 * A la demande d'ajout 'dmd_ajout' de la paire selectionnee d'elements du
 * type ( 'nameX', 'nameY' ), la paire est rentree/ajoutee dans l'element de
 * type select d'ID 'recap'.
 *
 * Un element selectionne a une valeur de la forme :
 * type=iii/timestep=vvv/nomvar=aaa,bbb,ccc:DDD.VVV
 *
 ******************************************************************************/
function paires_choisies( dmd_ajout, nameX, nameY, recap )
{
    var elementX="input[name="+nameX+"]";
    var elementY="input[name="+nameY+"]";

    $(dmd_ajout).click( function () {

        var elt_selectionneX=elementX+":not(:not(:checked))";
        var elt_selectionneY=elementY+":not(:not(:checked))";

        $(elt_selectionneX).each(function(){

            var nomcodeX = $(this).val();
            nomcodeX_affiche = nomcodeX.split('nomvar=')[1];

            $(elt_selectionneY).each(function(){

                var nomcodeY = $(this).val();
                nomcodeY_affiche = nomcodeY.split('nomvar=')[1];

                var nomcode=nomcodeY+"("+nomcodeX+")";
                var nomcode_affiche=nomcodeY_affiche+"("+nomcodeX_affiche+")";

                // nom_court correspond a nomcode_affiche avec VVV au lieu 
                // de aaa,bbb,ccc:DDD.VVV pour nomcodeX_affiche et nomcodeY_affiche
                //var nom_court = nomcodeY_affiche.split(':')[1] + "(" + nomcodeX_affiche.split(':')[1] + ")";

                var texte="<option selected='selected', " +
                           "value="+nomcode+">"+nomcode_affiche +
                           "</option>";
                $(recap).append( texte );
            });
        });
    });
}

/****************************************************************************** 
 *
 * Parmi tous les elements de classe 'groupe', seuls sont rendus visibles
 * ceux qui sont de plus de classe dont le nom est la concatenation de
 * 'prefixe' et 'valeur_par_defaut'.
 *
 ******************************************************************************/
function parDefautFiltre( groupe, prefixe, valeur_par_defaut ){

    var groupe_entier = "."+groupe;
    var groupe_visible = "."+prefixe+valeur_par_defaut;
        
    $(groupe_entier).hide();
    $(groupe_visible).show();
}

/****************************************************************************** 
 *
 * A la selection d'une 'valeur' dans 'liste_filtre', parmi tous les elements 
 * de classe 'groupe', seuls sont rendus visibles ceux qui sont de plus de
 * classe dont le nom est la concatenation de 'prefixe' et 'valeur'.
 *
 ******************************************************************************/
function onSelectFiltre( liste_filtre, groupe, prefixe ){

    var selected = liste_filtre+" option:selected";
    var valeur = $(selected).val();

    var groupe_entier = "."+groupe;
    var groupe_visible = "."+prefixe+valeur;
        
    $(groupe_entier).hide();
    $(groupe_visible).show();
}

