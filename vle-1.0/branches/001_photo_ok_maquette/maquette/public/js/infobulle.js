/*
 * File infobulle.js
 *
 * Application Web RECORD nom_appli_web_record_a_definir
 *
 * Author : Nathalie Rousse, INRA RECORD team member.
 *
 * Copyright (C) 2011 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*****************************************************************************
 * infobulle : { infobulle.css, infobulle.js, jquery.mon_infobulle.js }
 *
 * Affichage d'un texte infobulle quand la souris passe sur l'element auquel
 * il correspond.
 *
 *****************************************************************************/

/*
 * Au passage de la souris sur un element de classe 'avec_bulle', affichage
 * du texte informatif 'texte_bulle' qui est propre a cet element.
 *
 * Exemple :
 * <td texte_bulle="...texte informatif..." class="avec_bulle">..texte..</td>
 *
 * Valable pour les elements declares ici : <p>, <div>, <td> ...
 */
function infobulle()
{
    var options = {
        offsetX: 30,
        offsetY: 5,
        infobullecss: "infobulle"
    };
    $("p.avec_bulle").mon_infobulle(options);
    $("div.avec_bulle").mon_infobulle(options);
    $("td.avec_bulle").mon_infobulle(options);
    $("h3.avec_bulle").mon_infobulle(options);
}

