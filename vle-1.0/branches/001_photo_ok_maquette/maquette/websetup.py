"""Setup the maquette application"""
import logging

from maquette.config.environment import load_environment
from maquette.model import meta

from pylons import config

from authkit.users.sqlalchemy_driver import UsersFromDatabase
from authkit.users import md5

# Load the models
from sqlalchemy import *

from maquette import model
from maquette.model import *

log = logging.getLogger(__name__)

log.info("Adding the AuthKit model...")

def setup_app(command, conf, vars):
    """Place any commands to setup maquette here"""
    load_environment(conf.global_conf, conf.local_conf)

    # Create the tables if they don't already exist
    meta.metadata.bind = meta.engine

    populate_users()

def populate_users():
#
# Roles
#
# role admin : seul role existant pour l'instant dans maquette.
# Un admin ajoute et modifie des paquets/modeles dans la base.
#
# role responsable_paquet : n'existe pas.
# Un responsable_paquet d'un paquet en est le correspondant pour admin. En cas de paquet/modele prive (avec mot de passe), c'est son responsable_paquet qui en distribue le mot de passe aux personnes de son choix.
#
# role user : n'existe pas.
# Pour l'instant l'utilisation de l'outil est publique, pas de gestion de comptes utilisateurs, l'application n'offre pas d'espace de stockage individuel.
#

    users = UsersFromDatabase(model)
    
    # Create the tables if they don't already exist
    meta.metadata.create_all(bind=meta.engine)
    
    secret = config['app_conf']['authkit.form.authenticate.user.encrypt.secret']

    users.role_create("admin")
    
    users.user_create("a", password=md5("a",secret))
    users.user_add_role("a", role="admin")
    meta.Session.add(Email(address="admin@home", owner="a"))

    users.user_create("admin", password=md5("admin",secret))
    users.user_add_role("admin", role="admin")
    meta.Session.add(Email(address="admin@home", owner="admin"))

    # !!! role user et user nath temporaires pour verifications
    users.role_create("user")
    users.user_create("nath", password=md5("nath", secret))
    users.user_add_role("nath", role="user")
    meta.Session.add(Email(address="nath@home", owner="nath"))
    
    meta.Session.commit()
    
