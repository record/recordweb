#-*- coding:utf-8 -*-

###############################################################################
# File definition.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from maquette.model import donnees_affichage_saisie
from maquette.model import exp
from maquette.model.exp import Exp

###############################################################################
# Definition
#
# Les informations de definition d'un scenario
#
# Cas particulier : le mode de combinaison (lineaire ou total) concernant le
# plan d'experience n'est pas traite dans la Definition mais dans Resultat.
# Le mode de combinaison du scenario n'est pas visualise (le mode de
# combinaison choisi est applique au scenario - cf Resultat - mais visualise
# nulle part).
# Par contre le reste des informations concernant le plan d'experience
# (replicaSeed et replicaNumber) est traite dans la Definition (cf le bloc
# 'autres').
#
###############################################################################

class Definition( object ):

    ###########################################################################
    # Construction a partir de exp
    ###########################################################################
    def __init__( self, exp ):

        # informations de definition du scenario, organisees ici en blocs
        # (dont format s'appuie 'directement' sur le format d'affichage/saisie)
        self.blocs = None
        self.setBlocs( exp )

    ###########################################################################
    # Prend en compte dans les blocs les informations de definition
    # du scenario issues de exp
    ###########################################################################
    def setBlocs( self, exp ):

        self.blocs=list()

        #
        # blocs des conditions :
        #
        # Le bloc d'une condition est du type "condition"
        #
        # Attention, certaines conditions resteront cachees, c'est a dire que
        # l'utilisateur ne pourra ni les voir ni les modifier. Il s'agit de
        # celles qui ne sont pas actives (ie pointees/utilisees par au 
        # moins un modele atomic) et de
        # celles dont le nom commence par '__wh_' ou 'z'.
        #
        # mode_affichage_saisie vaut "read-write" pour le bloc d'une
        # condition visible et "hidden" pour celui d'une condition cachee.
        #

        # Prealable :
        # Determination de la liste des conditions visibles
        #
        conditions_visibles = exp.get_liste_conditions_actives()
        # Suppression de celles dont le nom commence par '__wh_' ou 'z'
        for cond in conditions_visibles :
            if cond.startswith('__wh_') :
                print "La condition ", cond, " n'apparait pas car son nom commence par '__wh_'"
                conditions_visibles.remove( cond )
            elif cond.startswith('z') :
                print "La condition ", cond, " n'apparait pas car son nom commence par 'z'"
                conditions_visibles.remove( cond )

        # liste de toutes les conditions (visibles ou cachees)
        conditions = exp.get_liste_conditions()

        ports_values = exp.get_liste_ports_setvalues()
        # et non pas ports_values = exp.get_liste_ports_values()

        for c, cond in enumerate(conditions) :

            if cond in conditions_visibles :
                type_bloc = "condition"
                mode_affichage_saisie = "read-write"
                dimension_donnees = "variable"
                nom_bloc = cond
                bloc_condition = donnees_affichage_saisie.Bloc( type_bloc, mode_affichage_saisie, dimension_donnees, nom_bloc, ports_values[c])

            else : # condition cachee
                type_bloc = "condition"
                mode_affichage_saisie = "hidden"
                dimension_donnees = "variable"
                nom_bloc = cond
                bloc_condition = donnees_affichage_saisie.Bloc( type_bloc, mode_affichage_saisie, dimension_donnees, nom_bloc, ports_values[c])

            self.blocs.append( bloc_condition )

        #
        # bloc autres :
        #
        liste_datas = [ ( "experimentName", [ exp.getExperimentName() ] ), # name of experiments
                        ( "begin", [ exp.getBegin() ] ), # begin of experiments
                        ( "duration", [ exp.getDuration() ] ), # duration of experiments
                        ( "seed", [ exp.getSeed() ] ), # seed of experiments
                        ( "replicaNumber", [ exp.getReplicaNumber() ] ),
                        ( "replicaSeed", [ exp.getReplicaSeed() ] ) ]
        type_bloc = "anonyme"
        mode_affichage_saisie = "read-write" # sinon "read-only"
        dimension_donnees = "fixe"
        nom_bloc = "autres"
        autres = donnees_affichage_saisie.Bloc( type_bloc, mode_affichage_saisie, dimension_donnees, nom_bloc, liste_datas )
        self.blocs.append( autres )

    ###########################################################################
    # Prend en compte dans exp les informations de definition
    # du scenario issues des blocs 
    # retourne exp
    ###########################################################################
    def prendreEnCompteDans( self , exp ):

        for bloc in self.blocs :

            # Prise en compte uniquement des blocs dont mode_affichage_saisie
            # vaut "read-write" ; impasse sur ceux qui n'ont pas pu etre
            # modifies (ie dont mode_affichage_saisie vaut "hidden" ou
            # "read-only") :

            if bloc.mode_affichage_saisie=="hidden" or bloc.mode_affichage_saisie=="read-only" :
                pass # impasse 

            elif bloc.mode_affichage_saisie == "read-write" :

                # specificite des blocs des conditions
                if bloc.typebloc == "condition" : 

                    cond = str( bloc.nombloc )
                    for (p,values)  in bloc.liste_datas :
                        port = str(p)
                        exp.clearConditionPort(cond, port)
                        for v in values :
                            exp.addValueCondition(cond, port, v)

                if bloc.typebloc == "anonyme" :

                    # specificite du bloc "autres" :
                    if bloc.nombloc == "autres" :
                        for (dataname,datavalues) in bloc.liste_datas :
                            if dataname == "experimentName" :
                                exp.setName( datavalues[0] )
                            elif dataname == "begin" :
                                exp.setBegin( datavalues[0] )
                            elif dataname == "duration" :
                                exp.setDuration( datavalues[0] )
                            elif dataname == "seed" :
                                exp.setSeed( datavalues[0] )
                            elif dataname == "replicaNumber" :
                                exp.setReplicaNumber( datavalues[0] )
                            elif dataname == "replicaSeed" :
                                exp.setReplicaSeed( datavalues[0] )

                    # l'autre bloc de typebloc "anonyme" a pour nombloc
                    # "pour_info", son mode_affichage_saisie vaut "read-only"

                else : 
                    print "prendreEnCompteDans Erreur : le type de bloc ", bloc.typebloc, " n'est pas traite"

            else : 
                print "prendreEnCompteDans Erreur : le mode_affichage_saisie ", bloc.mode_affichage_saisie, " n'est pas traite"

        return exp

