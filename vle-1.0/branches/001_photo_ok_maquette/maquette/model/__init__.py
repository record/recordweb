#-*- coding:utf-8 -*-

###############################################################################
# File __init__.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Authors :
# Nathalie Rousse, INRA RECORD team member,
# Jerome Thiard <jerome.thiard@toulouse.inra.fr> (*).
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
#
# (*) Jerome Thiard : classes PersistantData, OwnedData
#
###############################################################################

"""The application's model objects"""

import sqlalchemy as sa
from sqlalchemy import orm

from maquette.model import meta

import pyvle


def init_model(engine):
    """Call me before using any of the tables or classes in the model"""
    ## Reflected tables must be defined and mapped here
    #global reflected_table
    #reflected_table = sa.Table("Reflected", meta.metadata, autoload=True,
    #                           autoload_with=engine)
    #orm.mapper(Reflected, reflected_table)
    #
    meta.Session.configure(bind=engine)
    meta.engine = engine

#{Tables réfléchies

t_paquets = sa.Table('paquets', meta.metadata,

    sa.Column('uid', sa.types.Integer, primary_key=True),
	# laisse pour l'instant mais inutilise
    sa.Column('user_uid', sa.types.Integer, sa.schema.ForeignKey('users.uid')),

    sa.Column('nom', sa.types.Text, nullable=False),

    sa.Column('description', sa.types.Text, nullable=False),

	# nom effectif du paquet sous pkgs
    sa.Column('nom_pkg', sa.types.Text, nullable=False),

	# mot de passe du paquet en cas d'acces prive
    sa.Column('password', sa.types.Text, nullable=False),

	# responsable du paquet 
	# qui en est le correspondant pour l'(les)administrateur(s),
	# qui en distribue le mot de passe aux personnes de son choix.
    sa.Column('nom_responsable', sa.types.Text, nullable=False),
    sa.Column('mail_responsable', sa.types.Text, nullable=False),

	# informations relatives a la reception du paquet
    sa.Column('nom_resp_scientifique', sa.types.Text, nullable=False),
    sa.Column('nom_resp_informatique', sa.types.Text, nullable=False),

	# version vle sous laquelle le paquet a ete receptionne
    sa.Column('version_vle', sa.types.Text, nullable=False),
    sa.Column('cr_reception', sa.types.Text, nullable=False),

    useexisting=True,
    )

t_emails = sa.Table('emails', meta.metadata,
    sa.Column('uid', sa.types.Integer, primary_key=True),
    sa.Column('address', sa.types.Text, nullable=False),
    sa.Column('user_uid', sa.types.Integer, sa.schema.ForeignKey('users.uid')),
    useexisting=True,
    )
#}

#{Interfaces

class PersistantData(object):
    """
    Interface pour l'ensemble des données peristantes
    """

    @classmethod
    def get_table(cls):
        """
        Fournit la table contenant l'ensemble des données du type

        @rtype: C{sqlalchemy.orm.query.Query}
        """
        return meta.Session.query(cls)

    @classmethod
    def get_by_id(cls, uid):
        """
        Fournit une donnée via son identifiant unique.

        @type id: C{int}
        """
        return cls.get_table().filter_by(uid=uid).first()

    def __init__(self, **kwargs):
        for name, value in kwargs.iteritems():
            setattr(self, name, value)

    def __repr__(self):
        return "%s(%r)" % (self.__class__.__name__, self.__dict__)

    def set(self, **kwargs):
        """
        Permet de mettre à jour un ensemble d'attributs.
        L'encapsulation sera assurée par des `properties`.

        @param kwargs: les attributs à mettre à jour
        """
        for name, value in kwargs.iteritems():
            setattr(self, name, value)
            
class OwnedData(PersistantData):
    """
    Interface pour toutes les données qui ont un propriétaire.
    """

    @staticmethod
    def get_users_table():
        """
        Fournit la table de tous les utilisateurs.

        @rtype: C{sqlalchemy.orm.query.Query}
        """
        return meta.Session.query(User)

    @classmethod
    def get_owned_by(cls, user):
        """
        Fournit toutes les données appartenant à un utilisateur.

        @type user: C{User}
        """
        
        return cls.get_table().filter_by(user_uid=user.uid).all()

    @property
    def owner(self):
        return self.get_users_table().filter_by(uid=self.user_uid).first()

    @owner.setter
    def owner(self, value):
        if isinstance(value, User):
            name = value.username
        else:
            name = value
        self.user_uid = self.get_users_table().filter_by(username=name).first().uid

    def __html__(self):
        return "%s/%s" % (self.owner.username, self.nom)

    def __unicode__(self):
        return self.__html__()

    def __str__(self):
        return self.__unicode__()

    def __lt__(self, other):
        if self.owner.username == other.owner.username:
            return self.nom < other.nom
        return self.owner.username < other.owner.username

#}

class Paquet(OwnedData):

    def get_liste_vpz(self):
        # Fournit la liste des scenarios (fichiers vpz) du paquet

	liste = pyvle.VlePackage((self.nom_pkg).encode()).getVpzList()
	for i,f in enumerate(liste) :
	    liste[i]=liste[i]+'.vpz'
	return liste

    def get_nom(self, id_scenario):
	# Rend le nom du scenario (fichier vpz)
	# de rang id_scenario dans 'la liste des scenarios'
        liste_vpz = self.get_liste_vpz()
        return liste_vpz[int(id_scenario)]

class Email(OwnedData):

    def __html__(self):
        return "mailto:%s" % self

    def __unicode__(self):
        return self.address

    def __str__(self):
        return self.__unicode__()

orm.mapper(Paquet, t_paquets)
orm.mapper(Email, t_emails)

