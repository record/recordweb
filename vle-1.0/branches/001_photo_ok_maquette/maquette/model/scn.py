#-*- coding:utf-8 -*-

###############################################################################
# File scn.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

import copy

from maquette.model.scenariovpz import ScenarioVpz
from maquette.model.exp_scenariovpz import ExpScenarioVpz
from maquette.model.definition import Definition
from maquette.model.espace_exploitation import EspaceExploitation
from maquette.model.espace_exploitation import choisirPrefixeExploitation
from maquette.model.espace_exploitation import getPrefixeRepertoireExploitation

###############################################################################
# Scn
#
# Une exploitation d'un scenario par un utilisateur :
# visualisation, manipulation, simulation...
#
###############################################################################

class Scn( object ):

    # Construction/creation
    def __init__( self, paquet, id_scenario ) :

        self.scenario_vpz_d_origine = None  # ScenarioVpz 
        self.scenario_vpz = None            # ScenarioVpz 
        self.definition = None              # Definition
        self.resultat = None                # Resultat de simulation
        self.espace_exploitation = None     # espace d'exploitation

        # Initialisation en fonction du vpz et de son paquet ##################

        # initialisation du scenario d'origine selon le paquet et le vpz choisis
        self.scenario_vpz_d_origine = ScenarioVpz( paquet )
        self.scenario_vpz_d_origine.set( id_scenario )

        # creation de l'espace d'exploitation
        prefixe = choisirPrefixeExploitation( getPrefixeRepertoireExploitation(), self.scenario_vpz_d_origine )
        self.espace_exploitation = EspaceExploitation( self.scenario_vpz_d_origine, prefixe )

        # creation de scenario_vpz relatif a espace_exploitation,
        # a partir de scenario_vpz_d_origine :
        paquet = copy.deepcopy( self.scenario_vpz_d_origine.get_paquet() )
        paquet.nom_pkg = self.espace_exploitation.nom_pkg
        paquet.nom = 'issu de ' + self.scenario_vpz_d_origine.get_paquet_nom()
        paquet.description = ' blablabla bla bla bla'
        scenario_vpz = ScenarioVpz( paquet )
        scenario_vpz.set_nom_vpz( self.espace_exploitation.nom_vpz )
        self.scenario_vpz = scenario_vpz

        # initialisation de l'objet de definition du scenario
        exp = ExpScenarioVpz( self.scenario_vpz )
        self.definition = Definition( exp )

    # Destruction/suppression
    def supprimer( self ) :
        self.espace_exploitation.supprimerRepExploitation()
        del self

