#-*- coding:utf-8 -*-

###############################################################################
# File compression.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

import os
import os.path
import glob
import zipfile

###############################################################################
#
# Compression de fichiers/repertoires
#
# Methodes concernant la production de fichiers zip
#
###############################################################################

###############################################################################
# Production recursive d'un fichier zip
# filezip : fichier zip (ou buffer) resultat
# repertoire_a_zipper : repertoire a zipper
# nouveau_nom : nom donne au repertoire_a_zipper dans le zip
###############################################################################

def creer_zip( filezip, repertoire_a_zipper, nouveau_nom):
    mode = 'w'
    lenpathparent = len(repertoire_a_zipper)+1 # pour arcname (chemins relatifs)
    z = zipfile.ZipFile( filezip, mode, compression=zipfile.ZIP_DEFLATED )
    zipdirectory( z, repertoire_a_zipper , lenpathparent, nouveau_nom)
    z.close()

def completer_zip( filezip, repertoire_a_zipper, nouveau_nom):
    mode = 'a'
    lenpathparent = len(repertoire_a_zipper)+1 # pour arcname (chemins relatifs)
    z = zipfile.ZipFile( filezip, mode, compression=zipfile.ZIP_DEFLATED )
    zipdirectory( z, repertoire_a_zipper , lenpathparent, nouveau_nom)
    z.close()

def zipdirectory( z, path, lenpathparent, nouveau_nom ):
    # balayage du contenu de path a zipper
    for f in glob.glob( path+'/*' ) :
        if os.path.isdir(f):
            zipdirectory( z, f, lenpathparent, nouveau_nom)
        else:
            nom = nouveau_nom +'/'+ f[lenpathparent:]
            z.write( filename=f, arcname=nom)

###############################################################################
# Production du fichier zip filezip contenant le fichier fichier_a_zipper
# renomme nouveau_nom
###############################################################################
def creer_zip_fichier( filezip, fichier_a_zipper, nouveau_nom):
    z = zipfile.ZipFile( filezip, 'w', compression=zipfile.ZIP_DEFLATED )
    z.write( filename=fichier_a_zipper, arcname=nouveau_nom)
    z.close()

