#-*- coding:utf-8 -*-

###############################################################################
# File erreurs.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
# Erreurs
#
# Gestion des erreurs survenues en cours de traitement (pour signalisation...)
#
###############################################################################
class Erreurs(object):

    # Construction/initialisation :
    def __init__( self, liste_noms_erreurs ):

        #######################################################################
        # les_erreurs : dict des erreurs gerees
        # { nom de l'erreur : ( etat erreur (True ou False), message erreur ) }
        # message erreur est un texte explicatif accompagnant l'erreur. Il
        # est gere dans le cas de certaines erreurs seulement (pas toutes)
        #######################################################################
        # creation et raz de toutes les erreurs
        self.les_erreurs = dict()

        self.les_erreurs[ 'erreur_saisie_blocs' ] = ( False, "" )
        self.les_erreurs[ 'erreur_echec_simulation' ] = ( False, "" )
        self.les_erreurs[ 'erreur_aucun_trace_choisi' ] = ( False, "" )

    # raz du message d'une erreur (etat inchange)
    def raz_message( self, erreur ) :
        self.set_message( erreur, "" )

    # affectation (valeur texte) du message d'une erreur (etat inchange)
    def set_message( self, erreur, texte ) :
        (etat_prec, message_prec) = self.les_erreurs[ erreur ]
        self.les_erreurs[ erreur ] = ( etat_prec, texte )

    # retourne le message d'une erreur
    def get_message( self, erreur ) :
        (etat, message) = self.les_erreurs[ erreur ]
        return message

    # activation etat (valeur True) d'une erreur (message inchange)
    def activer( self, erreur ) :
        (etat_prec, message_prec) = self.les_erreurs[ erreur ]
        self.les_erreurs[ erreur ] = ( True, message_prec )

    # desactivation etat (valeur False) d'une erreur, de plus raz message 
    def desactiver( self, erreur ) :
        self.les_erreurs[ erreur ] = ( False, "" )

    # indique etat d'une erreur
    def isActive( self, erreur ) :
        (etat, message) = self.les_erreurs[ erreur ]
        return etat

