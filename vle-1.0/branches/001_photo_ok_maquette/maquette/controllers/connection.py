#-*- coding:utf-8 -*-

###############################################################################
# File connection.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

import logging

from pylons import request, response, session, tmpl_context as c
from pylons.controllers.util import abort, redirect_to

from maquette.lib.base import BaseController, render

log = logging.getLogger(__name__)

class ConnectionController(BaseController):

    def signout(self):
        """
        Suppression des cookies
        @return: renvoie vers L{compute_signout}
        """
        redirect_to('/connection/compute_signout')
        
    def compute_signout(self):
        """
        Nettoyage des variables de sessions
        @return: renvoie vers L{logged_out}
        """

        if 'scn' in session : # scn existe deja
            session['scn'].supprimer()
            session.save()

        session.clear()
        session.save() 

        redirect_to('/connection/logged_out')

    def logged_out(self):
        """
        Affichage de la page montrant que la déconnection s'est bien effectuée
        @return: la page C{connexion/logout.html}
        """
        return redirect_to('/')

