#-*- coding:utf-8 -*-

###############################################################################
# File accueil.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from maquette.lib.base import BaseController, render

from maquette.model import espace_exploitation


class AccueilController(BaseController):

    def __before__(self):
        pass

    def index(self):
    # retourne la page d'accueil apres avoir au prealable
    # supprime les repertoires d'exploitation obsoletes eventuellement presents
    # (le critere d'obsolescence : anciennete du repertoire)

        liste = espace_exploitation.getListeRepExploitation()

        listeAsupprimer = [ rep for rep in liste if espace_exploitation.isObsolete( rep ) ]
        listeAgarder = [ rep for rep in liste if not espace_exploitation.isObsolete( rep ) ]

        for rep in listeAsupprimer :
            espace_exploitation.supprimerRepertoire( rep )

        print ""
        print "Liste des répertoires - espaces d'exploitation - qui ont été supprimés (obsolètes) : ", listeAsupprimer
        print "Liste des répertoires - espaces d'exploitation - restants (non obsolètes) : ", listeAgarder
        print ""

        return render('base.html')

