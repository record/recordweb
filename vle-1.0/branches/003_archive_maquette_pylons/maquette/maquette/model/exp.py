#-*- coding:utf-8 -*-

###############################################################################
# File exp.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from ast import literal_eval

from pyvle import Vle, VlePackage

###############################################################################
#
# Interface avec pyvle (class Vle, class VlePackage)
#
###############################################################################

###############################################################################
# Exp
#
# Objet de manipulation du fichier vpz (definition, simulation)
#
###############################################################################
class Exp( Vle ):

    ###########################################################################
    # Construction a partir du nom du fichier vpz et du paquet
    ###########################################################################
    def __init__( self, nom_vpz, nom_paquet ) :
        Vle.__init__( self, nom_vpz, nom_paquet )

    ###########################################################################
    # Retourne la liste de toutes les conditions, qu'elles soient ou non
    # actives (ie pointees/utilisees par au moins un modele atomic)
    ###########################################################################
    def get_liste_conditions(self):
        return self.listConditions()

    ###########################################################################
    # Retourne la liste des conditions actives
    # ie pointees/utilisees par au moins un modele atomic
    ###########################################################################
    def get_liste_conditions_actives(self):

        liste_conditions_actives = list()

        # parcours des dynamics (actives ou non,
        # ie pointees/utilisees ou non par modele(s) atomic(s))
        for dyn in self.listDynamics() :
            # liste des modeles atomics qui utilisent/pointent dyn
            liste_ma = self.listDynamicModels(dyn)
            for ma in liste_ma :
                # liste des conditions pointees/utilisees par ma
                liste_conditions_ma = self.listAtomicModelConditions(ma)
                for cond in liste_conditions_ma :
                    if cond not in liste_conditions_actives :
                        liste_conditions_actives.append( cond )

        return liste_conditions_actives

    ###########################################################################
    #
    # Retourne ports, la liste des ports de chaque condition 
    #
    # for cond in self.listConditions() :
    #     ports[cond] : liste des ports de la condition cond
    #
    ###########################################################################
    def get_liste_ports_par_condition(self):
        ports = [self.listConditionPorts(cond) for cond in self.listConditions()]
        return ports

    ###########################################################################
    #
    # Retourne la liste ports_values
    #
    # ports_values est la liste des ( port, la liste des values du port ) de
    # chaque condition
    #
    # Remarque : la 'liste des values du port' comporte plusieurs elements
    # dans le cas d'un port de multi-simulation et un element unique sinon
    #
    # for cond in self.listConditions() :
    #     ports_values[cond] :
    #     liste des ( port, liste des values de port ) de cond
    #
    ###########################################################################
    def get_liste_ports_setvalues(self):

        ports_values = [ [ ( port, self.getConditionSetValue(cond,port) ) for p,port in enumerate( self.listConditionPorts(cond) ) ] for c,cond in enumerate(self.listConditions()) ]

        # trace
        #print "exp/get_liste_ports_setvalues :"
        #for c,cond in enumerate(self.listConditions()) :
            #print "ports_values[", cond, "] : ", ports_values[c]
        #print ""

        return ports_values

    ###########################################################################
    #
    # Retourne ports_values, la liste des (port,values) de chaque condition 
    #
    # Remarque : values est une liste dans le cas d'un port de
    # multi-simulation et un element unique sinon
    #
    # for cond in self.listConditions() :
    #     ports_values[cond] : liste des (port,values) de la condition cond
    #
    ###########################################################################
    def get_liste_ports_values(self):

        # condition par condition : port, values
        ports_values = [ [ (port,self.getConditionPortValues(cond,port)) for port in self.listConditionPorts(cond)] for cond in self.listConditions()]
        return ports_values

    ###########################################################################
    #
    # Retourne le type de la 1ere value du port de la condition
    #
    # Valeurs retournees :   
    # boolean (pour Boolean choisi/saisi sous gvle)
    # double (pour Double choisi/saisi sous gvle)
    # integer (pour Integer choisi/saisi sous gvle)
    # string (pour String choisi/saisi sous gvle)
    # map (pour Map choisi/saisi sous gvle)
    # set (pour Set choisi/saisi sous gvle)
    # none (pour Table, Tuple, Matrix, Xml choisis/saisis sous gvle)
    ###########################################################################
    def get_type_value(self, condition, port ):
        type_value = self.getConditionValueType(condition, port, 0 ) # type du premier element
        return type_value


    ###########################################################################
    # tenterEcrireValeur
    #
    # traitement fonction de type_value
    #
    # Tentative d'ecriture de v dans le port (de cond) :
    # - ajout (non type) de v a la liste des elements/valeurs du port,
    # - puis ecrasement (type) du ieme element avec v.
    #
    # Les types 'set' et 'map' sont traites comme des cas particuliers
    # (exceptions) car la methode
    # setConditionValue(cond, port, v, type_value, i) ne peut pas leur etre
    # appliquee.
    #
    # Retourne True si ecriture effective/reussie et sinon False
    #
    # raise exception en cas d'echec
    #
    ###########################################################################
    def tenterEcrireValeur( self, cond, port, v, type_value, i) :
        cr_ok = True # par defaut
        # debut de txt_exception 
        txt_exception = "tenterEcrireValeur(cond:"+cond+",port:"+port+",v:"+v+",type_value:"+type_value+", i:"+str(i)+ "): "

        # cas d'ajout puis ecrasement avec valeur typee
        if type_value in ('boolean', 'integer', 'double', 'string') :
            self.addValueCondition(cond, port, v)
            self.setConditionValue(cond, port, v, type_value, i)

        # cas non securise (si mauvais type) d'ajout seulement (sans ecrasement)
        elif type_value in ('map', 'set' ) :

            val = None # par defaut
            try: # essai extraction/conversion
                c = literal_eval( v ) # contenu du str
                # extraction ok, s'assurer du format
                try:
                    val = c
                except (SyntaxError): # echec de conversion
                    print "exp.py, tenterEcrireValeur, echec de conversion"
                    txt_exception = txt_exception + " -- " + "echec de conversion"
                    cr_ok = False
            except (ValueError, SyntaxError): # cas str
                print "exp.py, tenterEcrireValeur, echec - cas str"
                txt_exception = txt_exception + " -- " + "echec - cas str"
                cr_ok = False

            # verification du type
            if cr_ok :
                if val != None :
                    if type_value=='map' and not isinstance(val, dict) :
                        print "exp.py, tenterEcrireValeur, echec de type/format (dict attendu), type(val) :", type(val)
                        txt_exception = txt_exception + " -- " + "echec de type/format (dict attendu), type(val):" + type(val)
                        cr_ok = False
                    elif type_value=='set' and not isinstance(val, list) :
                        print "exp.py, tenterEcrireValeur, echec de type/format (list attendu), type(val) :", type(val)
                        txt_exception = txt_exception + " -- " + "echec de type/format (list attendu), type(val):" + type(val)
                        cr_ok = False
        
            if cr_ok :
                if val != None :
                    self.addValueCondition(cond, port, val)

        else : # notamment type_value 'none'
            print "exp.py, tenterEcrireValeur, echec de type_value"
            txt_exception = txt_exception + " -- " + "echec de type_value"
            cr_ok = False

        if not cr_ok :
            raise Exception( txt_exception )

        return cr_ok


    ###########################################################################
    # Methode get_types_capture
    #
    # Construit typesCapture, le type de capture des vues :
    # dict ( vue : ( type, timestep ) )
    #
    ###########################################################################
    def get_types_capture( self ):
        typesCapture = dict()
        for vue in self.listViews() :
            typesCapture[vue] = ( self.getViewType(vue), self.getViewTimeStep(vue) )
        return typesCapture

    ###########################################################################
    # Lance la simulation en tant que simulation simple (avec run)
    # et retourne le resultat de la simulation simple.
    #
    # Le resultat : il s'agit des variables observees vue par vue.
    #
    # Format du resultat : dict ( vue : dict( nomvar : valeurs ) )
    #
    # Exemple de cle 'vue' : 'vueContraintes'
    # Exemple de cle 'nomvar' : 'sunfloV1,...contrainte_azote.FNIRUE'
    #
    ###########################################################################
    def mono_run(self):
        r = None # par defaut
        r = self.run()
        return r

    ###########################################################################
    # Lance la simulation en tant que simulation multiple (avec runManager) 
    # et retourne le resultat de la simulation multiple (plan d'experience).
    #
    # Le resultat : il s'agit, experience par experience, des variables
    # observees vue par vue.
    #
    # Format du resultat :
    # tuple de tuple (correspondant aux 2 indices de l'experience) de
    # dict ( vue : dict( nomvar : valeurs ) )
    #
    # Les 2 indices de l'experience/simulation sont ceux de son
    # identification parmi l'ensemble des simulations composant le plan
    # d'experience. Exemple '-0-0'.
    #
    # Exemple de cle 'vue' : 'vueContraintes'
    # Exemple de cle 'nomvar' : 'sunfloV1,...contrainte_azote.FNIRUE'
    #
    ###########################################################################
    def multi_run(self):
        r = None # par defaut
        r = self.runManager()
        return r

    ###########################################################################
    # Passe toutes les vues existantes en mode 'storage'
    ###########################################################################
    def setAllStorage(self):

        # Passe la vue view en mode 'storage'
        def setStorage( view ):
            output = self.getViewOutput(view)
            self.setOutputPlugin(output, '', 'local', 'storage')

        for view in self.listViews():
            setStorage(view)

    ###########################################################################
    # Lance la simulation en tant que simulation simple (avec run)
    # apres avoir passe toutes les vues en mode 'storage'
    # et retourne le resultat de la simulation simple
    ###########################################################################
    def mono_runModeStorage(self):
        self.setAllStorage()
        return self.mono_run()

    ###########################################################################
    # Lance la simulation en tant que simulation multiple (avec runManager) 
    # apres avoir passe toutes les vues en mode 'storage'
    # et retourne le resultat de la simulation multiple (plan d'experience)
    ###########################################################################
    def multi_runModeStorage(self):
        self.setAllStorage()
        return self.multi_run()

###############################################################################
# Gestion des paquets
###############################################################################

# Fournit la liste brute (complete) des paquets presents sous pkgs 
# tries par ordre alphabetique
def get_liste_brute_pkgs():
    liste = VlePackage.getInstalledPackages()
    liste.sort() # ordre alphabetique
    return liste

