#-*- coding:utf-8 -*-

###############################################################################
# File exp_config.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from pyvle import Vle, VlePackage
from maquette.model.exp import Exp

from maquette.model.conf_web_scenario import ConfWebScenario
from maquette.model.conf_web_scenario import TranscodageConfWebScenario
from maquette.model.conf_web_scenario import InfosGeneralesAppli
from maquette.model.conf_web_scenario import DocDonnee, InfosDonneeDef
from maquette.model.conf_web_scenario import PageWebDef, PageWebRes

# pour preparation/construction de InfosDonneeDef
tcw = TranscodageConfWebScenario()

###############################################################################
#
# Interface avec pyvle (class Vle, class VlePackage)
#
###############################################################################

###############################################################################
#
# Definition des constantes propres au format de la configuration
# web sous forme de fichier vpz
#
###############################################################################

class ConstantesExpConfigWeb( object ):

    # la condition principale : point d'entree des informations
    CONST_condition_application_web = 'web_application'

    # la condition principale condition_application_web contient des ports
    # renseignant chacun un theme de configuration
    CONST_port_conf_appli = 'appli'
    CONST_port_conf_dictionnaire = 'dictionnaire'
    CONST_port_conf_pages_def_name = 'pages_def_name'
    CONST_port_conf_pages_res_name = 'pages_res_name'

    # les valeurs donnees a web_name dans le cas ou il prend valeur issue de doc
    CONST_frenchname_de_doc = 'frenchname' # prend valeur nom_frenchname de doc
    CONST_englishname_de_doc = 'englishname' # prend valeur nom_englishname de doc

C = ConstantesExpConfigWeb() # pour utilisation dans ExpConfigWeb

###############################################################################
# ExpConfigWeb
#
# Objet de manipulation du fichier vpz de configuration de l'application web
# (du repertoire web)
#
# Interface entre le fichier vpz de configuration de l'application web et
# la ConfWebScenario : construit la ConfWebScenario a partir du contenu du
# fichier vpz de configuration 
#
# Les verifications de format ne sont pas toutes faites dans ExpConfigWeb,
# de preference localisees/remontees au niveau de ConfWebScenario.
#
    # zzz il faudra ajouter lecture de la condition 'web_conf' (nom des 
    # fichiers vpz visibles...)

    # zzz traiter cas ou condition_application_web (...etc) n'existe pas

###############################################################################
class ExpConfigWeb( Exp ):

    ###########################################################################
    # Construction a partir du nom du fichier vpz (chemin absolu)
    ###########################################################################
    def __init__( self, nom_vpz ) :

        Vle.__init__( self, nom_vpz.encode() )

        cr_ok = True # par defaut

        # definition/initialisation de self.configuration_web au fur et a  
        # mesure de la lecture de chaque rubrique/partie de la configuration

        self.configuration_web = ConfWebScenario()
        
        #######################################################################
        # Partie generale application
        #######################################################################

        # lecture/initialisation de self.configuration_web.conf_appli
        self.initialisationConfAppli()

        #######################################################################
        # Partie dictionnaire 
        # qui servira a documenter les pages web (def et res)
        #######################################################################

        # La liste ordonnee des noms des dicos
        dicos_names = self.lireDicosNames() 

        # Le dictionnaire est un dict de dicos
        dictionnaire = self.lireConfDictionnaire( dicos_names )

        #######################################################################
        # Partie pages web de definition
        #######################################################################

        #######################################################################
        # pages_web_def_brutes

        # condition_pages_web_def : nom de la condition dediee aux pages web def
        condition_pages_web_def = self.lireConditionPagesWebDef()

        # La liste des noms des pages web def (pour l'instant non ordonnee)
        pages_web_def_names = self.lirePagesWebNames( condition_pages_web_def )

        # Les pages web de definition brutes : format d'origine, sans y avoir
        # traite la documentation
        pages_web_def_brutes = self.lireConfPagesWeb( condition_pages_web_def, pages_web_def_names )

        #######################################################################
        # self.configuration_web.pages_web_def
        # a partir de pages_web_def_brutes, dicos_names, dictionnaire
        # (mise au format PageWebDef et documentation des pages web def)
        self.initialisationPagesWebDef( pages_web_def_brutes, dicos_names, dictionnaire )

        #######################################################################
        # self.configuration_web.pages_web_def_names,
        # la liste ORDONNEE des noms des pages web def,
        # a partir de pages_web_def_names
        self.configuration_web.pages_web_def_names = pages_web_def_names 
        # fonction de tri de la liste pages_web_def_names par ordre croissant
        # de numero de page, pour affichage a l'ecran dans l'ordre
        def numero_page_web_def(page_name): 
            page_web = self.configuration_web.pages_web_def[page_name]
            return page_web.infos_generales.getNumeroPage()
        self.configuration_web.pages_web_def_names.sort(key=numero_page_web_def)

        #######################################################################
        # Partie pages web de resultat
        #######################################################################

        #######################################################################
        # pages_web_res_brutes

        # condition_pages_web_res : nom de la condition dediee aux pages web res
        condition_pages_web_res = self.lireConditionPagesWebRes()

        # La liste des noms des pages web res (pour l'instant non ordonnee)
        pages_web_res_names = self.lirePagesWebNames( condition_pages_web_res )

        # Les pages web de resultat brutes : format d'origine, sans y avoir traite la documentation
        pages_web_res_brutes = self.lireConfPagesWeb( condition_pages_web_res, pages_web_res_names )

        #######################################################################
        # self.configuration_web.pages_web_res
        # a partir de pages_web_res_brutes, dictionnaire
        # (mise au format PageWebRes et documentation des pages web res)
        self.initialisationPagesWebRes( pages_web_res_brutes, dictionnaire )

        #######################################################################
        # self.configuration_web.pages_web_res_names,
        # la liste ORDONNEE des noms des pages web res,
        # a partir de pages_web_res_names
        self.configuration_web.pages_web_res_names = pages_web_res_names 
        # fonction de tri de la liste pages_web_res_names par ordre croissant
        # de numero de page, pour affichage a l'ecran dans l'ordre
        def numero_page_web_res(page_name): 
            page_web = self.configuration_web.pages_web_res[page_name]
            return page_web.infos_generales.getNumeroPage()
        self.configuration_web.pages_web_res_names.sort(key=numero_page_web_res)

        if cr_ok != True :
            self.configuration_web = None

    ###########################################################################
    # Initialisation partie generale de configuration de l'application,
    # partie relative a port_conf_appli
    ###########################################################################

    # Lecture (dans exp) de la partie relative a port_conf_appli : conf_appli
    # definit self.configuration_web.conf_appli a partir de conf_appli
    def initialisationConfAppli( self ) :

        condition = C.CONST_condition_application_web 
        port = C.CONST_port_conf_appli 

        # port_conf_appli a une seule valeur (map) pour l'instant
        # conf_appli = self.getConditionSetValue( condition, port )
        conf_appli = self.getConditionPortValues( condition, port )

        # zzzz ajouter controles de format
        # verifier que conf_appli : map/dict

        # self.configuration_web.conf_appli 
        titre = self.getTitre( conf_appli )
        description = self.getDescription( conf_appli )
        help = self.getHelp( conf_appli )
        self.configuration_web.conf_appli = InfosGeneralesAppli()
        self.configuration_web.conf_appli.setValeurs( titre, description, help )

    ###########################################################################
    # Initialisation partie dictionnaire (documentation),
    # partie relative a port_conf_dictionnaire
    ###########################################################################

    ###########################################################################
    # Lecture (dans exp) de la liste ordonnee des noms des dicos
    # c'est la liste des valeurs (string) du port port_conf_dictionnaire
    def lireDicosNames( self ) :
        condition = C.CONST_condition_application_web 
        port = C.CONST_port_conf_dictionnaire
        dicos_names = self.getConditionSetValue( condition, port )

        # zzzz ajouter controles de format
        # verifier que dicos_names : liste de string (noms de dicos)

        return dicos_names

    ###########################################################################
    # Lecture (dans exp) de la partie relative a port_conf_dictionnaire
    # Retourne dict 'nom du dico' : dico
    # dico correspond aux informations de la condition dico lue (dans exp)
    # + d'autres informations (cf cle 'dico')
    # et format transforme (en dict)
    # Remarque : les dicos ne peuvent pas etre tous regroupes en un seul
    # car 2 peuvent contenir un meme nom (la cle nom n'est pas unique)
    def lireConfDictionnaire( self, dicos_names ) :
    
        dictionnaire = {}
        for dico_name in dicos_names :
            dico = {}
            for p in self.listConditionPorts(dico_name) :
                e = self.getConditionPortValues(dico_name,p)
                # la cle 'dico' est ajoutee a chaque e (informant sur 
                # son origine) 
                e['dico'] = dico_name
                dico[p] = e
            dictionnaire[ dico_name ] = dico

        # zzzz ajouter controles de format
        # verifier que la condition dico_name existe
        # verifier que e : map/dict
        # dans e = self.getConditionPortValues(dico_name,p)

        return dictionnaire

    ###########################################################################
    # initialisation partie pages web de definition
    # et partie pages web de resultat
    ###########################################################################

    ###########################################################################
    # Lecture (dans exp) du nom de la condition dediee aux pages web def
    # c'est la valeur du port port_conf_pages_def_name de la condition
    # condition_application_web 
    def lireConditionPagesWebDef( self ) :
        condition = C.CONST_condition_application_web 
        port = C.CONST_port_conf_pages_def_name 
        condition_pages_web_def = self.getConditionPortValues( condition, port )

        # zzzz ajouter controles de format
        # verifier condition_pages_web_def : string

        return condition_pages_web_def

    ###########################################################################
    # Lecture (dans exp) du nom de la condition dediee aux pages web res
    # c'est la valeur du port port_conf_pages_res_name de la condition
    # condition_application_web 
    def lireConditionPagesWebRes( self ) :
        condition = C.CONST_condition_application_web 
        port = C.CONST_port_conf_pages_res_name 
        condition_pages_web_res = self.getConditionPortValues( condition, port )

        # zzzz ajouter controles de format
        # verifier condition_pages_web_res : string

        return condition_pages_web_res

    ###########################################################################
    # fonction commune aux pages web def et res
    # Lecture (dans exp) de la liste des noms des pages web
    # c'est la liste des ports de la condition condition_pages_web
    # a ce niveau, la liste n'est pas ordonnee
    def lirePagesWebNames( self, condition_pages_web ) :
        pages_web_names = self.listConditionPorts(condition_pages_web) 

        # zzzz ajouter controles de format
        # verifier que la condition condition_pages_web existe

        return pages_web_names

    ###########################################################################
    # fonction commune aux pages web def et res
    # Lecture (dans exp) de la configuration des pages web
    # a ce niveau, la documentation n'a pas ete traitee
    def lireConfPagesWeb( self, condition_pages_web, pages_web_names ) :
        pages_web = {}
        for page_web_name in pages_web_names :
            pages_web[page_web_name] = self.getConditionSetValue(condition_pages_web,page_web_name)

        # zzzz ajouter controles de format
        # verifier que chaque port (qui correspond/definit une page web) :
        # contient liste de map/dict
        # +? verifier que la premiere map (generalites sur la page) est
        # particuliere par rapport aux suivantes (les donnees de la page) ?

        return pages_web

    ###########################################################################
    # Construction/creation de la documentation DocDonnee d'une donnee d'une
    # page web def
    def docDef( self, dicos_names, dictionnaire, doc_dicos, doc_locale ) :

        # dico_prefere et name
        # pour eventuellement recuperer infos de documentation dans 
        # donnee/element (parametre) name de dico (condition) dico_prefere
        dico_prefere = None # par defaut
        name = None # par defaut
        if doc_dicos != None :
            if 'dico_prefere' in doc_dicos.keys() :
                dico_prefere = doc_dicos['dico_prefere']
            if 'name' in doc_dicos.keys() :
                name = doc_dicos['name']

        doc = DocDonnee()

        # on parcourt les maj de la moins prioritaire :
        # a la plus prioritaire :

        # maj de doc par rapport a name trouve dans
        # dictionnaire (le 1er trouve en consultant les
        # dicos selon leur liste ordonnee)
        if name != None :
            liste_sources = []
            for dico_name in dicos_names :
                dico = dictionnaire[dico_name]
                if name in dico.keys() :
                    liste_sources.append( dico[name] ) 
            if len(liste_sources) >= 1 :
                source = liste_sources[0]
                self.ecrireExpDocDansDocDonnee( source, doc )

        # puis maj de doc par rapport
        # a name trouve dans dico_prefere
        if name != None and dico_prefere != None :
            if dico_prefere in dictionnaire.keys() :
                dico = dictionnaire[dico_prefere]
                if name in dico.keys() :
                    source = dico[name]
                    self.ecrireExpDocDansDocDonnee( source, doc )

        # puis maj de doc par rapport a doc_locale
        if doc_locale != None :
            source = doc_locale
            self.ecrireExpDocDansDocDonnee( source, doc )

        #
        # zzzz ajouter controles de format ?
        #

        return doc

    ###########################################################################
    # Fonction de mise a jour de web_name si vaut 'frenchname' ou 'englishname'
    # (recuperer alors sa valeur dans doc)
    def maj_web_name( self, infos_donnee_def ) :

        web_name = infos_donnee_def.getWebName()

        if web_name == C.CONST_frenchname_de_doc :
            # a recuperer dans nom_frenchname si existe
            web_name = '' # par defaut
            v = infos_donnee_def.doc_donnee.getNomFrenchname()
            if v != None :
                web_name = v
            infos_donnee_def.setWebName( web_name ) # maj

        elif web_name == C.CONST_englishname_de_doc :
            # a recuperer dans nom_englishname si existe
            web_name = '' # par defaut
            v = infos_donnee_def.doc_donnee.getNomEnglishname()
            if v != None :
                web_name = v
            infos_donnee_def.setWebName( web_name ) # maj

    ###########################################################################
    # self.configuration_web.pages_web_def a partir de 
    # pages_web_def_brutes, dicos_names, dictionnaire :
    # - Mise au format PageWebDef 
    # - Documentation des pages web def, a partir du dictionnaire et
    #   doc_locale, selon doc_priorite
    # - Mise a jour de web_name si vaut 'frenchname' ou 'englishname' (valeur
    #   alors recupere dans doc)
    def initialisationPagesWebDef( self, pages_web_def_brutes, dicos_names, dictionnaire ):

        for ( nom_page, page_web_brute ) in pages_web_def_brutes.iteritems() :

            page_web_def = PageWebDef() # la page

            # parcours des donnees de page_web_brute
            # pour les rentrer documentees dans page_web_def
            for d,donnee_brute in enumerate( page_web_brute ) :

                if d == 0 : # dict de l'info propre a la page entiere 
                    # infos_generales
                    titre = self.getTitre(donnee_brute)
                    description = self.getDescription(donnee_brute)
                    help = self.getHelp(donnee_brute)
                    numero = self.getNumero(donnee_brute)
                    page_web_def.infos_generales.setValeurs( titre, description, help, numero )

                else : # liste_donnees
                    # donnee a formater/documenter

                    infos_donnee_def = InfosDonneeDef()

                    # infos gardees de donnee_brute
                    # reorganisees (en dict thematiques)

                    web_dimension = self.getWebDimension( donnee_brute )
                    web_name = self.getWebName( donnee_brute )
                    web_mode = self.getWebMode( donnee_brute )
                    infos_donnee_def.setWebConfValeurs( web_dimension, web_name, web_mode )

                    vpz_type = self.getVpzType( donnee_brute )
                    if vpz_type == 'type-condition-port' :
                        # condition et port
                        vpz_cond_name = self.getVpzCondName( donnee_brute )
                        vpz_port_name = self.getVpzPortName( donnee_brute )
                        infos_donnee_def.setVpzIdValeurs( vpz_type, vpz_cond_name, vpz_port_name )
                    else : 
                        infos_donnee_def.setVpzIdValeurs( vpz_type )

                    # ajout documentation creee ici
                    if 'doc_dicos' in donnee_brute.keys() :
                        doc_dicos = donnee_brute['doc_dicos']
                    else :
                        doc_dicos = None
                    if 'doc_locale' in donnee_brute.keys() :
                        doc_locale = donnee_brute['doc_locale']
                    else :
                        doc_locale = None
                    infos_donnee_def.doc_donnee = self.docDef( dicos_names, dictionnaire, doc_dicos, doc_locale )

                    # maintenant que la doc est construite/connue
                    # maj de web_name si vaut 'frenchname' ou 'englishname'
                    self.maj_web_name( infos_donnee_def )

                    page_web_def.liste_donnees.append( infos_donnee_def )

            self.configuration_web.pages_web_def[nom_page] = page_web_def

        # zzzz ajouter controles de format ?

    ###########################################################################
    # Constitution du dict des donnees documentees d'une page web res
    # Ces donnees sont issues du dico prefere
    def docRes( self, doc_dico_prefere, dictionnaire ) :

        donnees_documentees = {}

        if doc_dico_prefere in dictionnaire.keys() :

            dico = dictionnaire[ doc_dico_prefere ]

            for (nom_donnee,doc_issue_de_dico) in dico.iteritems() :
                doc_donnee = DocDonnee()
                self.ecrireExpDocDansDocDonnee( doc_issue_de_dico, doc_donnee )
                donnees_documentees[ nom_donnee ] = doc_donnee

        return donnees_documentees

    ###########################################################################
    # self.configuration_web.pages_web_res a partir de 
    # pages_web_res_brutes, dictionnaire :
    # - Mise au format PageWebRes 
    # - Documentation des pages web res a partir doc_dico_prefere et dictionnaire
    # Une page web res correspond a une unique vue view
    def initialisationPagesWebRes( self, pages_web_res_brutes, dictionnaire ):

        for ( nom_page, page_web_brute ) in pages_web_res_brutes.iteritems() :

            page_web_res = PageWebRes() # la page

            # page_web_brute contient exactement 2 elements (ou moins)
            # qui vont servir a renseigner page_web_res

            # dict de l'info propre a la page entiere 
            if len( page_web_brute ) < 1 :
                pass
            else :
                # l'info propre a la page entiere 
                donnee_brute = page_web_brute[0]

                # infos_generales
                titre = self.getTitre(donnee_brute)
                description = self.getDescription(donnee_brute)
                help = self.getHelp(donnee_brute)
                numero = self.getNumero(donnee_brute)
                page_web_res.infos_generales.setValeurs( titre, description, help, numero )

            # dict de l'info propre a l'unique vue view correspondant 
            # a la page (nom de la view et son dico prefere)
            if len( page_web_brute ) < 2 :
                pass
            else :
                donnee_brute = page_web_brute[1]

                doc_dico_prefere = self.getDocDicoPrefere( donnee_brute )
                vpz_view_name = self.getVpzViewName( donnee_brute )

                # on ne garde que le nom de la vue view
                page_web_res.vpz_view_name = vpz_view_name

                # ajout du dict des donnees documentees issues de doc_dico_prefere
                # donnees est un dict, peut-etre vide
                donnees_documentees = self.docRes( doc_dico_prefere, dictionnaire )
                page_web_res.donnees_documentees = donnees_documentees

            self.configuration_web.pages_web_res[nom_page] = page_web_res

        # zzzz ajouter controles de format ?

    ###########################################################################
    #
    # Transcodage : retourne valeur de certaines informations de ExpConfigWeb
    #
    ###########################################################################

    # retourne la valeur de la cle du dict objet, avec valeur
    # CONST_valeur_vide par defaut
    def getValeur( self, objet, cle ) :
        v = tcw.CONST_valeur_vide # par defaut
        if cle in objet.keys() :
            v = objet[cle]
        return v

    # retourne la valeur de titre du dict objet
    def getTitre( self, objet ) :
        return self.getValeur( objet, 'titre' )

    # retourne la valeur de description du dict objet
    def getDescription( self, objet ) :
        return self.getValeur( objet, 'description' )

    # retourne la valeur de help du dict objet
    def getHelp( self, objet ) :
        return self.getValeur( objet, 'help' )

    # retourne la valeur du numero du dict objet
    def getNumero( self, info_page ) :
        return self.getValeur( info_page, 'numero' )

    # retourne la valeur de doc_dico_prefere de donnee
    def getDocDicoPrefere( self, donnee ) :
        return self.getValeur( donnee, 'doc_dico_prefere' )

    # retourne la valeur de vpz_view_name de donnee
    def getVpzViewName( self, donnee ) :
        return self.getValeur( donnee, 'vpz_view_name' )

    # retourne la valeur de nom_frenchname de donnee
    def getNomFrenchname( self, donnee ) :
        return self.getValeur( donnee, 'nom_frenchname' )

    # retourne la valeur de nom_englishname de donnee
    def getNomEnglishname( self, donnee ) :
        return self.getValeur( donnee, 'nom_englishname' )

    # retourne la valeur de description de donnee
    def getDescription( self, donnee ) :
        return self.getValeur( donnee, 'description' )

    # retourne la valeur de unite de donnee
    def getUnite( self, donnee ) :
        return self.getValeur( donnee, 'unite' )

    # retourne la valeur de val_max de donnee
    def getValMax( self, donnee ) :
        return self.getValeur( donnee, 'val_max' )

    # retourne la valeur de val_min de donnee
    def getValMin( self, donnee ) :
        return self.getValeur( donnee, 'val_min' )

    # retourne la valeur de val_recommandee de donnee
    def getValRecommandee( self, donnee ) :
        return self.getValeur( donnee, 'val_recommandee' )

    # retourne la valeur de web_dimension de donnee
    def getWebDimension( self, donnee ) :
        return self.getValeur( donnee, 'web_dimension' )

    # retourne la valeur de web_mode de donnee
    def getWebMode( self, donnee ) :
        return self.getValeur( donnee, 'web_mode' )

    # retourne la valeur de web_name de la donnee
    def getWebName( self, donnee ) :
        return self.getValeur( donnee, 'web_name' )

    # retourne la valeur de vpz_type de donnee
    def getVpzType( self, donnee ) :
        return self.getValeur( donnee, 'vpz_type' )

    # retourne la valeur de vpz_cond_name de donnee
    def getVpzCondName( self, donnee ) :
        return self.getValeur( donnee, 'vpz_cond_name' )

    # retourne la valeur de vpz_port_name de donnee
    def getVpzPortName( self, donnee ) :
        return self.getValeur( donnee, 'vpz_port_name' )

    ###########################################################################
    # Documentation d'une donnee d'une page web 
    # cf le dict correspondant a la cle 'doc' cote ExpConfigWeb
    # (dont la cle 'dico' n'est pas recuperee),
    # cf DocDonnee cote ConfWebScenario
    ###########################################################################

    ###########################################################################
    # maj a jour DocDonnee a partir des champs valides/existants de exp_doc
    def ecrireExpDocDansDocDonnee( self, exp_doc, doc ) :

        v = self.getNomFrenchname(exp_doc)
        if not tcw.valeurVide(v) :
            doc.setNomFrenchname( v )
        v = self.getNomEnglishname(exp_doc)
        if not tcw.valeurVide(v) :
            doc.setNomEnglishname( v )
        v = self.getDescription(exp_doc)
        if not tcw.valeurVide(v) :
            doc.setDescription( v )
        v = self.getUnite(exp_doc)
        if not tcw.valeurVide(v) :
            doc.setUnite( v )
        v = self.getValMax(exp_doc)
        if not tcw.valeurVide(v) :
            doc.setValMax( v )
        v = self.getValMin(exp_doc)
        if not tcw.valeurVide(v) :
            doc.setValMin( v )
        v = self.getValRecommandee(exp_doc)
        if not tcw.valeurVide(v) :
            doc.setValRecommandee( v )
        v = self.getHelp(exp_doc)
        if not tcw.valeurVide(v) :
            doc.setHelp( v )

    ###########################################################################
    # retourne la valeur de la cle dans le dict 'doc' du dict donnee,
    # avec valeur CONST_valeur_vide  par defaut
    def get_docValeur( self, donnee, cle ) :
        v = tcw.CONST_valeur_vide # par defaut
        if 'doc' in donnee.keys() :
            if cle in donnee['doc'].keys() :
                v = donnee['doc'][cle]
        return v

    # pour donnee (d'une page web ou d'un dictionnaire),
    # retourne la valeur de nom_frenchname de doc de donnee
    def get_docNomFrenchname( self, donnee ) :
        return self.get_docValeur( donnee, 'nom_frenchname' )

    # pour donnee (d'une page web ou d'un dictionnaire),
    # retourne la valeur de nom_englishname de doc de donnee
    def get_docNomEnglishname( self, donnee ) :
        return self.get_docValeur( donnee, 'nom_englishname' )

    # pour donnee (d'une page web ou d'un dictionnaire),
    # retourne la valeur de description de doc de donnee
    def get_docDescription( self, donnee ) :
        return self.get_docValeur( donnee, 'description' )

    # pour donnee (d'une page web ou d'un dictionnaire),
    # retourne la valeur de unite de doc de donnee
    def get_docUnite( self, donnee ) :
        return self.get_docValeur( donnee, 'unite' )

    # pour donnee (d'une page web ou d'un dictionnaire),
    # retourne la valeur de val_max de doc de donnee
    def get_docValMax( self, donnee ) :
        return self.get_docValeur( donnee, 'val_max' )

    # pour donnee (d'une page web ou d'un dictionnaire),
    # retourne la valeur de val_min de doc de donnee
    def get_docValMin( self, donnee ) :
        return self.get_docValeur( donnee, 'val_min' )

    # pour donnee (d'une page web ou d'un dictionnaire),
    # retourne la valeur de val_recommandee de doc de donnee
    def get_docValRecommandee( self, donnee ) :
        return self.get_docValeur( donnee, 'val_recommandee' )

    # pour donnee (d'une page web ou d'un dictionnaire),
    # retourne la valeur de help de doc de donnee
    def get_docHelp( self, donnee ) :
        return self.get_docValeur( donnee, 'help' )


