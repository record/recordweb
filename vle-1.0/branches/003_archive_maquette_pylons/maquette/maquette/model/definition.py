#-*- coding:utf-8 -*-

###############################################################################
# File definition.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from maquette.model.donnees_affichage_saisie import DonneeSaisie
from maquette.model import exp
from maquette.model.exp import Exp

from maquette.model.conf_web_scenario import TranscodageConfWebScenario 
from maquette.model.conf_web_scenario import InfosGeneralesAppli
from maquette.model.conf_web_scenario import DocDonnee

from pylons import config # pour conf_web (valeurs par defaut)

# format infos de definition grandement calque sur celui de ConfWebScenario
# pour interpretation de DonneeDef
tcw = TranscodageConfWebScenario()

###############################################################################
# Definition
#
# Les informations de definition d'un scenario
#
# Il s'agit des informations constituant le scenario qui sont affichees pour
# information voire modification avant d'en demander simulation (pour
# obtenir resultats, cf Resultat).
#
# Cas particulier : le mode de combinaison (lineaire ou total) concernant le
# plan d'experience n'est pas traite dans la Definition mais dans Resultat.
# Le mode de combinaison du scenario n'est pas visualise (le mode de
# combinaison choisi est applique au scenario - cf Resultat - mais visualise
# nulle part).
# Par contre le reste des informations concernant le plan d'experience
# (replicaSeed et replicaNumber) est traite dans la Definition.
#
# Definition contient une liste de Bloc.
# Un Bloc contient une liste de DonneeDef.
#
###############################################################################
class Definition( object ):

    ###########################################################################
    # Construction de Definition a partir de exp (scenario, Exp) et
    # configuration_web (configuration web, ConfWebScenario)
    # ou alors par defaut sans rien dedans
    ###########################################################################
    def __init__( self, exp=None, configuration_web=None ):

        # par defaut valeurs standard pour informations generales
        self.infos_generales = InfosGeneralesAppli()
        self.infos_generales.setValeursStandard()

        # informations de definition du scenario, organisees ici en blocs
        # (contenu et format d'affichage/saisie des pages web def)
        # regroupant des DonneeDef
        self.blocs = None

        if (exp!=None) and (configuration_web!=None) :

            # informations generales
            self.infos_generales = configuration_web.conf_appli

            # blocs
            self.setBlocs( exp, configuration_web )

    ###########################################################################
    # Prend en compte (dans les blocs de Definition) les informations de
    # definition du scenario issues de exp, conformement a configuration_web
    ###########################################################################
    def setBlocs( self, exp, configuration_web ):

        self.blocs=list()

        # liste ordonnee des blocs
        for page_web_def_name in configuration_web.pages_web_def_names :
            page_web_def = configuration_web.pages_web_def[page_web_def_name]

            info_generale_page = page_web_def.infos_generales
            titre_bloc = info_generale_page.getTitre()
            description_bloc = info_generale_page.getDescription()
            help_bloc = info_generale_page.getHelp()

            # les donnees du bloc
            liste_datas = None
            liste_datas=list()
            for infos_donnee_def in page_web_def.liste_donnees :
                donnee_def = DonneeDef( infos_donnee_def, exp )
                liste_datas.append( donnee_def )

            bloc = Bloc( titre_bloc, description_bloc, help_bloc, liste_datas )

            self.blocs.append( bloc ) # entree/ajout du bloc

    ###########################################################################
    # Prend en compte dans exp les informations de definition
    # du scenario (issues des blocs de Definition) 
    # retourne exp
    ###########################################################################
    def prendreEnCompteDans( self , exp ):

        for bloc in self.blocs :

            for donnee_def in bloc.liste_datas :

                # maj exp a partir de donnee_def.values

                web_mode = donnee_def.getWebMode()

                if tcw.isReadWriteMode(web_mode) :

                    vpz_type = donnee_def.getVpzType()
                    if tcw.isTypeExpNameVpzType(vpz_type) :
                        v = str( donnee_def.values[0] )
                        exp.setName( donnee_def.values[0] )
                    elif tcw.isTypeExpDurationVpzType(vpz_type) :
                        v = float( donnee_def.values[0] )
                        exp.setDuration( v )
                    elif tcw.isTypeExpBeginVpzType(vpz_type) :
                        v = float( donnee_def.values[0] )
                        exp.setBegin( v )
                    elif tcw.isTypeSimuSeedVpzType(vpz_type) :
                        v = float( donnee_def.values[0] )
                        exp.setSeed( v )
                    elif tcw.isTypePlanSeedVpzType(vpz_type) :
                        v = int( donnee_def.values[0] )
                        exp.setReplicaSeed( v )
                    elif tcw.isTypePlanNumberVpzType(vpz_type) :
                        v = int( donnee_def.values[0] )
                        exp.setReplicaNumber( v )
                    elif tcw.isTypeConditionPortVpzType(vpz_type) :
                        cond = donnee_def.getVpzCondName()
                        port = donnee_def.getVpzPortName()
                        exp.clearConditionPort(cond, port) # effacement prealable
                        for i,v in enumerate(donnee_def.values) :
                            type_value = donnee_def.type_value
                            cr = exp.tenterEcrireValeur( cond, port, v, type_value, i)
                            if not cr : 
                                print "definition.py , prendreEnCompteDans, echec ecriture : cond: ", cond, " port: ", port, " v: ", v, " type_value: ", type_value, " i: ", i

                    # else : rien
                # else CONST_web_mode_hidden : rien
                # else CONST_web_mode_read_only : rien

        return exp

    ###########################################################################
    # Methode miseAjourBlocsSaisis
    #
    # Met a jour les informations de definition du scenario (les valeurs des
    # donnees DonneeDef des blocs de Definition) en fonction des valeurs
    # saisies params (contenant des DonneeSaisie)
    #
    # Les valeurs saisies params concernent des donnees de web_mode
    # valant "read-write" (ni "hidden" ni "read-only").
    #
    # Retourne True si le traitement s'est bien deroule et False sinon (dans ce
    # dernier cas, blocs n'est pas exploitable).

    # Cette methode et les traitements/controles de la page d'affichage/saisie
    # (scenario.html) sont concus en complement l'un de l'autre.
    # Par exemple la page html
    # ne permet pas de supprimer un champ unique. Si ce n'etait pas le cas,
    # alors au niveau de la methode miseAjourBlocsSaisis certaines donnees
    # risqueraient de ne pas etre correctement mises a jour.
    # Par exemple les indices d'une donnee permettant ensuite de la reperer dans
    # Definition.blocs (indice de son bloc dans blocs et son propre indice dans
    # liste_datas de son bloc) sont determines dans la page html au niveau des
    # boucles d'affichage.
    #
    ###############################################################################
    def miseAjourBlocsSaisis( self, params ):

        ###########################################################################
        # Evalue si p1 et p2 correspondent a la meme DonneeDef dans Definition
        # ie dans les blocs (critere memes indbloc et inddata)
        ###########################################################################
        def deMemeData( p1, p2 ) :
            return ( p1['indbloc']==p2['indbloc'] and p1['inddata']==p2['inddata'] )

        ###########################################################################
        # Le traitement :
        #
        # Tout d'abord params
        # [ [ name="indbloc=aaa/inddata=ccc", value=vvv ], ... ]
        # est converti en params_liste
        # [ { 'indbloc':aaa, 'inddata':ccc, 'valeurs':vvv }, ... ]
        #
        # Puis la liste_datas de chacun des blocs est mise a jour en fonction de
        # params_liste
        #
        # Remarque : des qu'un probleme est rencontre, arret du traitement et
        # retour de False.
        #
        ###########################################################################
    
        cr_ok = True # par defaut
    
        # Conversion de params en params_liste
        params_liste = list()
        for (k,v) in params.iteritems() :
            d = DonneeSaisie( k.encode(), v.encode())
            if d.formatNameOk() : # filtre
                params_liste.append( d.decouper() )
            # else : ignorer d
    
        # Dans la liste params_liste
        # [ { 'indbloc':indbloc, 'inddata':inddata, 'valeurs':vvv }, ... ],
        # on lit pour la donnee caracterisee par son indbloc et son inddata :
        # - ou bien un singleton :
        #   { 'indbloc':indbloc, 'inddata':inddata, 'valeurs':vvv_1 }
        # - ou bien une suite :
        #   { 'indbloc':indbloc, 'inddata':inddata, 'valeurs':vvv_1 }
        #   { 'indbloc':indbloc, 'inddata':inddata, 'valeurs':vvv_2 }
        #   { 'indbloc':indbloc, 'inddata':inddata, 'valeurs':vvv_3 }
        #   ...
        # A partir desquels est mise a jour values de la donnee DonneeDef
        # correspondante blocs[indbloc].liste_datas[inddata]
    
        # initialisations
        imax = len(params_liste)
        i = 0
    
        # parcours
        while i < imax :
    
            # pzero est le premier element dans params_liste
            # qui correspond dans blocs a la donnee blocs[indbloc].liste_datas[inddata]
            # dont values va etre mis a jour a partir de pzero['valeurs']
            # et des elements de params_liste suivant p qui correspondent eux aussi
            # a la donnee
    
            pzero = params_liste[i]
    
            if pzero['valeurs'] == None : # probleme
                return False # ARRET DU TRAITEMENT, retour en erreur
    
            indbloc = pzero['indbloc']
            inddata = pzero['inddata']
    
            # traitement de la donnee
    
            datavalues = list()
            datavalues.append( pzero['valeurs'] )
    
            # cas d'une suite
            fin_de_list = False
            while fin_de_list == False :
                i = i + 1 # suivant
                if i >= imax :
                    fin_de_list = True # qui plus est fin de params_liste_blocs
                else :
                    p = params_liste[i]
    
                    if p['valeurs'] == None : # probleme
                        return False # ARRET DU TRAITEMENT, retour en erreur
    
                    if not deMemeData( p, pzero ) :
                        fin_de_list = True # i correspond au pzero de la donnee suivante
                    else : # ajouter a la suite
                        datavalues.append( p['valeurs'] )
    
            self.blocs[indbloc].liste_datas[inddata].values = datavalues
    
        return cr_ok


###############################################################################
# Bloc
#
# Les informations de definition d'un scenario (entrees
# de simulation : conditions, date debut...) sont organisees par bloc.
#
# Un Bloc correspond a une page web, contient des DonneeDef.
#
# Contient des informations generales sur le bloc : titre/nom, description,
# help
#
# Contient liste_datas : la liste des DonneeDef du bloc
# ( nom de la donnee, etc, liste de ses valeurs )
#
# Remarque : pour une donnee de 'type-condition-port'
# la liste de ses valeurs contient plusieurs
# elements si donnee/port de multi-simulation et sinon (donnee/port de 
# simulation simple) un seul element.
#
###############################################################################

class InformationsGeneralesBloc(object):
    # Construction a partir
    # d'informations generales sur le bloc (titre, description, help)
    def __init__(self, titre_bloc, description_bloc, help_bloc):
        self.titre = titre_bloc
        self.description = description_bloc
        self.help = help_bloc

class Bloc(object):

    # Construction a partir
    # d'informations generales sur le bloc (titre, description, help)
    # et de la liste liste_datas de ses donnees DonneeDef 
    def __init__(self, titre_bloc, description_bloc, help_bloc, liste_datas):

        self.infos_generales = InformationsGeneralesBloc( titre_bloc, description_bloc, help_bloc )

        self.liste_datas = liste_datas

###############################################################################
# DonneeDef
#
# Une donnee parmi les informations de definition d'un scenario (entrees
# de simulation : conditions, date debut...), telle que exploitee/traitee 
# au niveau d'un Bloc (rangee dans liste_datas de Bloc).
#
# Contient toutes les informations necessaires a son affichage/saisie a
# l'ecran et maj.
#
# Une donnee provient d'un scenario vpz (exp), dans lequel elle est par la
# suite maj.
#
# Une donnee contient aussi des informations issues de la configuration web : 
# caracteristiques qui permettent de l'identifier dans le scenario vpz, qui
# definissent les conditions dans lesquelles la gerer/afficher a l'ecran
# dans la page web, doc...
#
# Une donnee est affichee dans une page web pour information et peut-etre
# modification (rentree dans un Bloc).
#
###############################################################################
#
# Les informations provenant de la configuration web s'appuient le plus
# possible sur le format de ConfWebScenario.
#
# Remarque : pour une donnee de type 'type-condition-port', la liste de ses
# valeurs contient plusieurs elements si donnee/port de multi-simulation
# et sinon (donnee/port de simulation simple) un seul element.
#
###############################################################################
#
# CERTAINS TYPES DE VALEURS DE PORTS (vle) NE SONT PAS TRAITES : voir
# assuranceCompatibilite (class ConfWebScenario, conf_web_scenario.py)
#
###############################################################################

class DonneeDef( object ):

    # Construction a partir d'une donnee InfosDonneeDef d'une page web de
    # definition (cf ConfWebScenario) et d'un exp scenario de simulation
    def __init__(self, infos_donnee_def, exp) :

        vpz_type = infos_donnee_def.getVpzType()

        # informations (nom web, documentation, type...) (self._donnee_def
        self.infos_donnee_def = infos_donnee_def
        # zzz a deepcopier ?

        # valeurs (self.values)
        if tcw.isTypeExpNameVpzType(vpz_type) :
            values = [ exp.getExperimentName() ]
        elif tcw.isTypeExpDurationVpzType(vpz_type) :
            values = [ exp.getDuration() ]
        elif tcw.isTypeExpBeginVpzType(vpz_type) :
            values = [ exp.getBegin() ]
        elif tcw.isTypeSimuSeedVpzType(vpz_type) :
            values = [ exp.getSeed() ]
        elif tcw.isTypePlanSeedVpzType(vpz_type) :
            values = [ exp.getReplicaSeed() ]
        elif tcw.isTypePlanNumberVpzType(vpz_type) :
            values = [ exp.getReplicaNumber() ]
        elif tcw.isTypeConditionPortVpzType(vpz_type) :
            cond = infos_donnee_def.getVpzCondName()
            port = infos_donnee_def.getVpzPortName()
            values = exp.getConditionSetValue(cond,port)
        else :
            values = ""
        self.values = values

        # type (self.type_value)
        type_value = None
        if tcw.isTypeConditionPortVpzType(vpz_type) :
            cond = infos_donnee_def.getVpzCondName()
            port = infos_donnee_def.getVpzPortName()
            type_value = exp.get_type_value(cond,port) # type du premier element
        elif tcw.isTypeExpNameVpzType(vpz_type) :
            type_value = type(self.values[0])
        elif tcw.isTypeExpDurationVpzType(vpz_type) :
            type_value = type(self.values[0])
        elif tcw.isTypeExpBeginVpzType(vpz_type) :
            type_value = type(self.values[0])
        elif tcw.isTypeSimuSeedVpzType(vpz_type) :
            type_value = type(self.values[0])
        elif tcw.isTypePlanSeedVpzType(vpz_type) :
            type_value = type(self.values[0])
        elif tcw.isTypePlanNumberVpzType(vpz_type) :
            type_value = type(self.values[0])
        else :
            type_value = type(self.values)
        self.type_value = type_value

    ###########################################################################
    # Evaluation de certaines informations de DonneeDef
    # (web_dimension, web_mode...)
    ###########################################################################

    def getWebMode(self) :
        return self.infos_donnee_def.getWebMode()

    def getVpzType(self) :
        return self.infos_donnee_def.getVpzType()

    def getVpzCondName(self) :
        return self.infos_donnee_def.getVpzCondName()

    def getVpzPortName(self) :
        return self.infos_donnee_def.getVpzPortName()

    # determine si DonneeDef est de web_dimension "variable" (ou equivalent)
    def isVariableDimension(self) :
        web_dimension = self.infos_donnee_def.getWebDimension()
        return tcw.isVariableDimension(web_dimension)

    # determine si DonneeDef est de web_dimension "fixe" (ou equivalent)
    def isFixeDimension(self) :
        web_dimension = self.infos_donnee_def.getWebDimension()
        return tcw.isFixeDimension(web_dimension)

    # determine si DonneeDef est de web_mode "hidden" (ou equivalent)
    def isHiddenMode(self) :
        web_mode = self.infos_donnee_def.getWebMode()
        return tcw.isHiddenMode(web_mode)

    # determine si DonneeDef est de web_mode "read_write" (ou equivalent)
    def isReadWriteMode(self) :
        web_mode = self.infos_donnee_def.getWebMode()
        return tcw.isReadWriteMode(web_mode)

    # determine si DonneeDef est de web_mode "read_only" (ou equivalent)
    def isReadOnlyMode(self) :
        web_mode = self.infos_donnee_def.getWebMode()
        return tcw.isReadOnlyMode(web_mode)

