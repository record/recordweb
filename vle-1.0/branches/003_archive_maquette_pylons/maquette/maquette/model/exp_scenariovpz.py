#-*- coding:utf-8 -*-

###############################################################################
# File exp_scenariovpz.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from maquette.model.exp import Exp

###############################################################################
# ExpScenarioVpz
#
# Exp propre a un ScenarioVpz
#
###############################################################################
class ExpScenarioVpz( Exp ):

    # Construction a partir de scenario_vpz, plus exactement
    # du nom du fichier vpz et du paquet lui correspondant
    def __init__( self, scenario_vpz ):
        nom_vpz = scenario_vpz.nom_vpz
        nom_paquet = scenario_vpz.get_paquet_nom_pkg().encode()
        Exp.__init__( self, nom_vpz, nom_paquet )

