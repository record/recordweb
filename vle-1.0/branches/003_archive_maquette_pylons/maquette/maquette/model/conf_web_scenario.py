#-*- coding:utf-8 -*-

###############################################################################
# File conf_web_scenario.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from pylons import config # pour conf_web (valeurs par defaut)
import copy

###############################################################################
# TranscodageConfWebScenario
#
# Elements de transcodage d'informations de configuration de
# l'application Web
#
# Objet dedie a ConfWebScenario
#
# Remarque : objet sur lequel s'appuient egalement exp_conf_web et
# donnee_affichage_saisie pour les aspects communs (formats calques)
#
# Contient des constantes :
# valeurs 'normalisees' prises par des informations de ConfWebScenario
#
# Contient des methodes de transcodage
#
###############################################################################

class TranscodageConfWebScenario( object ):

    ###########################################################################
    # Constantes : des valeurs attribuees par defaut
    ###########################################################################
    # cf config['app_conf']['conf_web.TXTDEFAUT_...']

    ###########################################################################
    # Constantes
    # valeurs 'normalisees' prises par des informations de ConfWebScenario
    ###########################################################################

    CONST_valeur_vide = None

    CONST_web_dimension_variable = 'variable'
    CONST_web_dimension_fixe     = 'fixe'

    CONST_web_mode_hidden        = 'hidden'
    CONST_web_mode_read_write    = 'read_write'
    CONST_web_mode_read_only     = 'read_only'

    CONST_vpz_type_exp_name      = 'type_exp_name'
    CONST_vpz_type_exp_duration  = 'type_exp_duration'
    CONST_vpz_type_exp_begin     = 'type_exp_begin'
    CONST_vpz_type_simu_seed     = 'type_simu_seed'
    CONST_vpz_type_plan_seed     = 'type_plan_seed'
    CONST_vpz_type_plan_number   = 'type_plan_number'
    CONST_vpz_type_condition_port = 'type_condition_port' 

    def valeurVide( self, v ) :
        return ( v == tcw.CONST_valeur_vide )

    ###########################################################################
    # Methodes de transcodage qui retournent
    # valeur normalisee prise par certaines informations de ConfWebScenario
    ###########################################################################

    # retourne la valeur (normalisee) de web_dimension
    def getStdWebDimension( self, web_dimension ) :
        v = tcw.CONST_valeur_vide # par defaut
        if web_dimension == 'variable' :
            v = tcw.CONST_web_dimension_variable
        elif web_dimension == 'fixe' :
            v = tcw.CONST_web_dimension_fixe
        return v

    # retourne la valeur (normalisee) de web_mode
    def getStdWebMode( self, web_mode ) :
        v = tcw.CONST_valeur_vide # par defaut
        if web_mode == 'hidden' :
            v = tcw.CONST_web_mode_hidden
        for val in ('read-write', 'read_write', 'readwrite', 'rw' ) : 
            if web_mode == val :
                v = tcw.CONST_web_mode_read_write
        for val in ( 'read-only', 'read_only', 'readonly' ) :
            if web_mode == val :
                v = tcw.CONST_web_mode_read_only
        return v

    # retourne la valeur (normalisee) de vpz_type
    def getStdVpzType( self, vpz_type ) :
        v = tcw.CONST_valeur_vide # par defaut
        for val in ( 'type_exp_name', 'type-exp-name', 'typeexpname' ) :
            if vpz_type == val :
                v = tcw.CONST_vpz_type_exp_name
        for val in ( 'type_exp_duration', 'type-exp-duration', 'typeexpduration' ) : 
            if vpz_type == val :
                v = tcw.CONST_vpz_type_exp_duration
        for val in ( 'type_exp_begin', 'type-exp-begin', 'typeexpbegin' ) : 
            if vpz_type == val :
                v = tcw.CONST_vpz_type_exp_begin
        for val in ( 'type_simu_seed', 'type-simu-seed', 'typesimuseed' ) : 
            if vpz_type == val :
                v = tcw.CONST_vpz_type_simu_seed
        for val in ( 'type_plan_seed', 'type-plan-seed', 'typeplanseed' ) : 
            if vpz_type == val :
                v = tcw.CONST_vpz_type_plan_seed
        for val in ( 'type_plan_number', 'type-plan-number', 'typeplannumber' ) : 
            if vpz_type == val :
                v = tcw.CONST_vpz_type_plan_number
        for val in ( 'type_condition_port', 'type-condition-port', 'typeconditionport', 'type_cond_port', 'type-cond-port', 'typecondport' ) : 
            if vpz_type == val :
                v = tcw.CONST_vpz_type_condition_port
        return v

    ###########################################################################
    # Methodes qui retournent la signification d'une information
    # La comparaison est faite sur sa valeur normalisee
    ###########################################################################

    # Valeurs de web_dimension 

    # determine si web_dimension est "variable" (ou equivalent)
    def isVariableDimension(self, web_dimension) :
        r = False # par defaut
        if self.getStdWebDimension(web_dimension) == self.CONST_web_dimension_variable :
            r = True
        return r

    # determine si web_dimension est "fixe" (ou equivalent)
    def isFixeDimension(self, web_dimension) :
        r = False # par defaut
        if self.getStdWebDimension(web_dimension) == self.CONST_web_dimension_fixe :
            r = True
        return r

    # Valeurs de web_mode 

    # determine si web_mode est "hidden" (ou equivalent)
    def isHiddenMode(self, web_mode) :
        r = False # par defaut
        if self.getStdWebMode(web_mode) == self.CONST_web_mode_hidden :
            r = True
        return r

    # determine si web_mode est "read_write" (ou equivalent)
    def isReadWriteMode(self, web_mode) :
        r = False # par defaut
        if self.getStdWebMode(web_mode) == self.CONST_web_mode_read_write :
            r = True
        return r

    # determine si web_mode est "read_only" (ou equivalent)
    def isReadOnlyMode(self, web_mode) :
        r = False # par defaut
        if self.getStdWebMode(web_mode) == self.CONST_web_mode_read_only :
            r = True
        return r

    # Valeurs de vpz_type 

    # determine si vpz_type est "type_exp_name" (ou equivalent)
    def isTypeExpNameVpzType(self, vpz_type) :
        r = False # par defaut
        if self.getStdVpzType(vpz_type) == self.CONST_vpz_type_exp_name :
            r = True
        return r

    # determine si vpz_type est "type_exp_duration" (ou equivalent)
    def isTypeExpDurationVpzType(self, vpz_type) :
        r = False # par defaut
        if self.getStdVpzType(vpz_type) == self.CONST_vpz_type_exp_duration :
            r = True
        return r

    # determine si vpz_type est "type_exp_begin" (ou equivalent)
    def isTypeExpBeginVpzType(self, vpz_type) :
        r = False # par defaut
        if self.getStdVpzType(vpz_type) == self.CONST_vpz_type_exp_begin :
            r = True
        return r

    # determine si vpz_type est "type_simu_seed" (ou equivalent)
    def isTypeSimuSeedVpzType(self, vpz_type) :
        r = False # par defaut
        if self.getStdVpzType(vpz_type) == self.CONST_vpz_type_simu_seed :
            r = True
        return r

    # determine si vpz_type est "type_plan_seed" (ou equivalent)
    def isTypePlanSeedVpzType(self, vpz_type) :
        r = False # par defaut
        if self.getStdVpzType(vpz_type) == self.CONST_vpz_type_plan_seed :
            r = True
        return r

    # determine si vpz_type est "type_plan_number" (ou equivalent)
    def isTypePlanNumberVpzType(self, vpz_type) :
        r = False # par defaut
        if self.getStdVpzType(vpz_type) == self.CONST_vpz_type_plan_number :
            r = True
        return r

    # determine si vpz_type est "type_condition_port" (ou equivalent)
    def isTypeConditionPortVpzType(self, vpz_type) :
        r = False # par defaut
        if self.getStdVpzType(vpz_type) == self.CONST_vpz_type_condition_port :
            r = True
        return r

tcw = TranscodageConfWebScenario() # pour utilisation dans ConfWebScenario

###############################################################################
# ConfWebScenario
#
# Les informations de configuration de l'application Web
#
# ConfWebScenario est applique a un scenario pour personnaliser la maniere 
# dont celui-ci est presente a l'ecran a l'utilisateur.
#
# ConfWebScenario contient des informations determinant/personnalisant la
# presentation a l'ecran d'un scenario : partie definition (ses pages web de
# definition) et partie resultat (ses pages web de resultat).
#
# Objet sur lequel s'appuie Definition
#
# type_source indique le format dans lequel les informations de configuration
# ont ete renseignees. Pour l'instant :
# - "par_defaut" : ConfWebScenario est configure par defaut
# - "exp_conf_web" : ConfWebScenario est configure a partir d'un fichier
#   vpz de configuration web, cf source (ExpConfigWeb).
#
###############################################################################

class ConfWebScenario( object ):

    ###########################################################################
    # Construction en fonction de type_source, a partir de source
    ###########################################################################
    def __init__( self, type_source="", source=None ):

        self.initRaz()

        if type_source != "" :
        # Construction en fonction de type_source, a partir de source

            # l'indicateur configurationParDefaut doit etre active pour tout
            # cas/situation ou il faut configurer par defaut
            # il est pris en compte en fin du traitement d'initialisation
            configurationParDefaut = False

            if type_source == "par_defaut" :
                configurationParDefaut = True

            elif type_source == "exp_conf_web" :
                # Construction a partir de exp_conf_web
                # (cas de fichier vpz de configuration web)
                exp_conf_web = source
                configuration_web = exp_conf_web.configuration_web

                if configuration_web == None : # la configuration s'est mal passee, rejetee
                    print "!!! la configuration a partir du fichier vpz de configuration web s'est mal passee, rejetee"
                    configurationParDefaut = True
                else :
                    # deepcopy
                    self.conf_appli = copy.deepcopy( configuration_web.conf_appli )
                    self.pages_web_def_names = copy.deepcopy( configuration_web.pages_web_def_names )
                    self.pages_web_def = copy.deepcopy( configuration_web.pages_web_def )
                    self.pages_web_res_names = copy.deepcopy( configuration_web.pages_web_res_names )
                    self.pages_web_res = copy.deepcopy( configuration_web.pages_web_res )

            if configurationParDefaut :
                # Construction par defaut en partie a partir de exp
                # (fichier vpz du scenario)
                exp = source
                self.initParDefaut( exp )

            # Quelques regles SYSTEMATIQUEMENT appliquees pour combler 
            # d'eventuels vides/non renseignements,
            # comportements par defaut
            self.applicationRegles()
    
            print "!!!!!!"
            print " self.conf_appli : ", self.conf_appli.titre
            print " self.conf_appli : ", self.conf_appli.description
            print " self.conf_appli : ", self.conf_appli.help
            print "!!!!!!"

    ###########################################################################
    # Initialisation - raz
    ###########################################################################
    def initRaz( self ) :

        #######################################################################
        # Partie generale de l'application
        #######################################################################
        self.conf_appli = None

        #######################################################################
        # Partie pages web de definition
        #######################################################################

        # pages_web_def_names , liste ordonnee des noms des PageWebDef
        self.pages_web_def_names = []

        # pages_web_def, dict des PageWebDef
        self.pages_web_def = {}

        #######################################################################
        # Partie pages web de resultat
        #######################################################################

        # pages_web_res_names , liste ordonnee des noms des pages web res
        self.pages_web_res_names = []

        # pages_web_res, configuration des pages web res
        self.pages_web_res = {}

    ###########################################################################
    #
    # Initialisation de ConfWebScenario par defaut,
    # en partie a partir de exp (fichier vpz du scenario)
    #
    # (correspond a l'application standard)
    #
    ###########################################################################
    def initParDefaut( self, exp ) :

        self.initRaz() # raz

        #######################################################################
        # partie generale de l'application (self.conf_appli)
        #######################################################################

        self.conf_appli = InfosGeneralesAppli()
        self.conf_appli.setValeursStandard()

        #######################################################################
        # partie pages web de definition
        # (self.pages_web_def_names, self.pages_web_def)
        #
        # Creation/definition des pages web def :
        #
        # - Une PageWebDef pour chaque condition du scenario
        #   (contenant ses parametres/ports)
        #
        # - Une PageWebDef pour le plan (replica seed et number)
        #
        # - Une PageWebDef pour l'ensemble des autres informations
        #   accessibles (debut simulation, duree...)
        #
        #######################################################################

        # liste des conditions du scenario
        liste_conditions_scn = exp.listConditions() 

        numero_page = 0 # initialisation

        #######################################################################
        # Page 'experience' pour 'l'ensemble des autres informations
        # accessibles' : debut simulation, duree...
        #######################################################################

        nom_page = "experience"
        self.pages_web_def_names.append( nom_page )

        page_web_def = PageWebDef() # la page

        # page_web_def.infos_generales : 
        page_web_def.infos_generales.setValeursStandardPageExperience( numero_page )
        numero_page = numero_page + 1

        # page_web_def.liste_donnees :
        # (applicationRegles renseignera web_dimension,
        # web_mode et web_name et certaines doc)

        infos_donnee_def = InfosDonneeDef()
        infos_donnee_def.setVpzType( tcw.CONST_vpz_type_exp_begin )
        page_web_def.liste_donnees.append( infos_donnee_def )

        infos_donnee_def = InfosDonneeDef()
        infos_donnee_def.setVpzType( tcw.CONST_vpz_type_exp_duration )
        page_web_def.liste_donnees.append( infos_donnee_def )

        infos_donnee_def = InfosDonneeDef()
        infos_donnee_def.setVpzType( tcw.CONST_vpz_type_exp_name )
        page_web_def.liste_donnees.append( infos_donnee_def )

        infos_donnee_def = InfosDonneeDef()
        infos_donnee_def.setVpzType( tcw.CONST_vpz_type_simu_seed )
        page_web_def.liste_donnees.append( infos_donnee_def )

        # ajout de la page aux pages_web_def 
        self.pages_web_def[ nom_page ] = page_web_def

        #######################################################################
        # Page 'Plan' pour le plan (replica seed et number)
        #######################################################################

        nom_page = "plan"
        self.pages_web_def_names.append( nom_page )

        page_web_def = PageWebDef() # la page

        # page_web_def.infos_generales : 
        page_web_def.infos_generales.setValeursStandardPagePlan( numero_page )
        numero_page = numero_page + 1

        # donnee : dict de chaque donnee
        # (applicationRegles renseignera web_dimension, web_mode et web_name et certaines doc)

        infos_donnee_def = InfosDonneeDef()
        infos_donnee_def.setVpzType( tcw.CONST_vpz_type_plan_seed )
        page_web_def.liste_donnees.append( infos_donnee_def )

        infos_donnee_def = InfosDonneeDef()
        infos_donnee_def.setVpzType( tcw.CONST_vpz_type_plan_number )
        page_web_def.liste_donnees.append( infos_donnee_def )

        # ajout de la page aux pages_web_def 
        self.pages_web_def[ nom_page ] = page_web_def

        #######################################################################
        # Une page web def pour chaque condition du scenario
        # (contenant ses parametres/ports)
        #######################################################################
        for condition in liste_conditions_scn :

            nom_page = "__condition__" + condition
            self.pages_web_def_names.append( nom_page )

            page_web_def = PageWebDef() # la page de la condition

            # page_web_def.infos_generales : 
            page_web_def.infos_generales.setValeursStandardPageCondition( numero_page )
            numero_page = numero_page + 1
            # prefixe de titre transforme en titre :
            titre = page_web_def.infos_generales.getTitre() +' '+condition
            page_web_def.infos_generales.setTitre( titre )

            # Parametres
            for port in exp.listConditionPorts( condition ) :
                # parametre : dict de chaque parametre/port de condition

                parametre = InfosDonneeDef()

                # web_conf
                parametre.setWebDimension( tcw.CONST_web_dimension_variable )
                parametre.setWebMode( tcw.CONST_web_mode_read_write )
                web_name = config['app_conf']['conf_web.TXTSTANDARD_prefixe_web_name_page_condition'] +' '+ port
                parametre.setWebName( web_name )

                # vpz_id
                parametre.setVpzType( tcw.CONST_vpz_type_condition_port )
                parametre.setVpzCondName( condition )
                parametre.setVpzPortName( port )

                page_web_def.liste_donnees.append( parametre )

            # ajout de la page de la condition aux pages_web_def 
            self.pages_web_def[ nom_page ] = page_web_def

        #######################################################################
        # partie pages web de resultat
        # (self.pages_web_res_names, self.pages_web_res)
        #
        # Creation/definition des pages web res :
        #
        # - Une page web def pour chaque view du scenario
        #   (contenant ses variables/ports observes)
        #
        #######################################################################

        self.pages_web_res_names = []
        self.pages_web_res = {}

        # liste des vues/views du scenario
        liste_views_scn = exp.listViews() 

        numero_page = 0 # initialisation

        for view in liste_views_scn :

            if view == None :
                print "!!!!! conf_web_scenario.py, initParDefaut : erreur view None"

            nom_page = "__vue__" + view
            self.pages_web_res_names.append( nom_page )

            page_web_res = PageWebRes() # page web res de la view
            
            print page_web_res.infos_generales.titre
            print page_web_res.infos_generales.description
            print page_web_res.infos_generales.help
            print page_web_res.infos_generales.numero_page
            print page_web_res.vpz_view_name
            print page_web_res.donnees_documentees

            # info_generale : dict de l'info propre a la page entiere 
            page_web_res.infos_generales.setValeursStandardPageWebRes( numero_page )
            numero_page = numero_page + 1
            # prefixe de titre transforme en titre :
            titre = page_web_res.infos_generales.getTitre() +' '+view
            page_web_res.infos_generales.setTitre( titre )

            # vpz_view_name
            page_web_res.vpz_view_name = view

            # donnees_documentees
            page_web_res.donnees_documentees = {} # vide

            # ajout de la page de la view aux pages_web_res 
            self.pages_web_res[ nom_page ] = page_web_res

    ###########################################################################
    #
    # Application a ConfWebScenario de quelques regles
    # pour combler d'eventuels vides/non renseignements
    # comportements par defaut
    # Normalisation des valeurs
    # Empecher/contourner des choix invalides (impossibles, non geres...)
    #
    # Remarque : ConfWebScenario est aussi susceptible d'etre transforme dans
    # assuranceCompatibilite
    #
    ###########################################################################
    def applicationRegles( self ) :

        #######################################################################
        # Utilitaire
        #######################################################################
        def parOrdreDePreference( v1, v2, v3 ):
            if not tcw.valeurVide( v1 ) :
                res = v1
            elif not tcw.valeurVide( v2 ) :
                res = v2
            else :
                res = v3
            return res

        #######################################################################

        # Partie generale de l'application
        self.conf_appli.setValeursDefautUneParUne() 

        #######################################################################
        # Partie des pages web de definition
        #######################################################################
    
        # pages_web_def_names
        # rien de plus n'est fait
        # zzz verifier ordre par numero de page croissant ?
    
        # pages_web_def
        for page_web_def_name,page_web_def in self.pages_web_def.iteritems() :

            # l'info propre a la page entiere 
            page_web_def.infos_generales.setValeursDefautUneParUne() 

            for d,infos_donnee_def in enumerate( page_web_def.liste_donnees ) :

                doc_donnee = infos_donnee_def.doc

                ###############################################################
                # web_conf
                ###############################################################

                # web_dimension :
                # Normalisation.
                # Si sans web_dimension alors par defaut :
                # web_dimension_variable pour une donnee de vpz_type
                # type-condition-port, web_dimension_fixe pour tout autre cas.
                web_dimension = infos_donnee_def.getWebDimension()
                if tcw.valeurVide( web_dimension ) :
                    vpz_type = infos_donnee_def.getVpzType() 
                    if tcw.isTypeConditionPortVpzType(vpz_type) :
                        web_dimension = tcw.CONST_web_dimension_variable
                    else :
                        web_dimension = tcw.CONST_web_dimension_fixe
                infos_donnee_def.setWebDimension( web_dimension )

                # web_dimension :
                # Toutes les valeurs de web_dimension n'etant pas
                # valides/possibles pour toutes les donnees
                # (web_dimension_variable n'est possible que pour une donnee
                # de vpz_type type-condition-port), web_dimension est FORCE a
                # web_dimension_fixe pour toute donnee de vpz_type autre que
                # type-condition-port
                web_dimension = infos_donnee_def.getWebDimension()
                vpz_type = infos_donnee_def.getVpzType() 
                if not tcw.isTypeConditionPortVpzType(vpz_type) :
                    if not tcw.isFixeDimension(web_dimension) :
                        web_dimension = tcw.CONST_web_dimension_fixe
                infos_donnee_def.setWebDimension( web_dimension )

                # web_name :
                # Normalisation.
                # Si sans web_name alors par defaut, par ordre de priorite :
                # nom_frenchname de doc, nom_englishname de doc,
                # nom web_name construit ici a partir de vpz_id
                web_name = infos_donnee_def.getWebName()
                if tcw.valeurVide( web_name ) :

                    nom_frenchname = doc_donnee.getNomFrenchname()
                    nom_englishname = doc_donnee.getNomEnglishname()
                    vpz_type = infos_donnee_def.getVpzType()

                    web_name = tcw.CONST_valeur_vide # par defaut
                    if vpz_type == tcw.CONST_vpz_type_exp_name :
                        w = config['app_conf']['conf_web.TXTDEFAUT_web_name_donnee_exp_name'] 
                        web_name = parOrdreDePreference( nom_frenchname, nom_englishname, w )
                    elif vpz_type == tcw.CONST_vpz_type_exp_duration :
                        w = config['app_conf']['conf_web.TXTDEFAUT_web_name_donnee_exp_duration'] 
                        web_name = parOrdreDePreference( nom_frenchname, nom_englishname, w )
                    elif vpz_type == tcw.CONST_vpz_type_exp_begin :
                        w = config['app_conf']['conf_web.TXTDEFAUT_web_name_donnee_exp_begin'] 
                        web_name = parOrdreDePreference( nom_frenchname, nom_englishname, w )
                    elif vpz_type == tcw.CONST_vpz_type_simu_seed :
                        w = config['app_conf']['conf_web.TXTDEFAUT_web_name_donnee_simu_seed'] 
                        web_name = parOrdreDePreference( nom_frenchname, nom_englishname, w )
                    elif vpz_type == tcw.CONST_vpz_type_plan_seed :
                        w = config['app_conf']['conf_web.TXTDEFAUT_web_name_donnee_plan_seed'] 
                        web_name = parOrdreDePreference( nom_frenchname, nom_englishname, w )
                    elif vpz_type == tcw.CONST_vpz_type_plan_number :
                        w = config['app_conf']['conf_web.TXTDEFAUT_web_name_donnee_plan_number'] 
                        web_name = parOrdreDePreference( nom_frenchname, nom_englishname, w )
                    elif vpz_type == tcw.CONST_vpz_type_condition_port :
                        cond = infos_donnee_def.getVpzCondName()
                        port = infos_donnee_def.getVpzPortName()
                        w = port + '__de__' + cond
                        web_name = parOrdreDePreference( nom_frenchname, nom_englishname, w )
                    else :
                        w = config['app_conf']['conf_web.TXTDEFAUT_web_name'] 
                        web_name = parOrdreDePreference( nom_frenchname, nom_englishname, w )
                infos_donnee_def.setWebName( web_name )

                # web_mode
                # Si sans web_mode alors par defaut : web_mode_hidden si
                # vpz_type invalide, et sinon web_mode_read_write
                web_mode = infos_donnee_def.getWebMode()
                if tcw.valeurVide(web_mode) :
                    if tcw.valeurVide( infos_donnee_def.getVpzType() ) :
                        web_mode = tcw.CONST_web_mode_hidden
                    else :
                        web_mode = tcw.CONST_web_mode_read_write
                infos_donnee_def.setWebMode(web_mode)

                ###############################################################
                # vpz_id
                ###############################################################

                # vpz_type
                vpz_type = infos_donnee_def.getVpzType()
                infos_donnee_def.setVpzType(vpz_type)

                # vpz_cond_name
                vpz_cond_name = infos_donnee_def.getVpzCondName()
                infos_donnee_def.setVpzCondName(vpz_cond_name)

                # vpz_port_name
                vpz_port_name = infos_donnee_def.getVpzPortName()
                infos_donnee_def.setVpzPortName(vpz_port_name)

                ###############################################################
                # doc
                ###############################################################
                doc_donnee.setRegleValeursParDefaut()

        #######################################################################
        # Partie des pages web de resultat
        #######################################################################
    
        # pages_web_res_names
        # rien de plus n'est fait
        # zzz verifier ordre par numero de page croissant ?
    
        # pages_web_res
        for page_web_res_name,page_web_res in self.pages_web_res.iteritems() :

            # l'info propre a la page entiere 
            page_web_res.infos_generales.setValeursDefautUneParUne() 

            ###################################################################
            # la vue/view
            ###################################################################
            # rien de plus n'est fait
            # page_web_res.vpz_view_name

            ###################################################################
            # les donnees documentees (issues du dico prefere)
            ###################################################################
            for nom_donnee,doc_donnee in page_web_res.donnees_documentees.iteritems() :
                doc_donnee.setRegleValeursParDefaut()
    
    ###########################################################################
    # assuranceCompatibilite :
    #
    # Controle la compatibilite de ConfWebScenario avec le scenario (exp)
    # auquel elle est sensee etre appliquee. Effectue sur ConfWebScenario
    # certaines transformations/modifications pour obtenir la compatibilite.
    # Retourne True si compatibilite au final (apres eventuelles
    # modifications) et False sinon.
    #
    # Les cas/verifications/modifications geres :
    #
    # I. Verification de l'existence dans exp des conditions et ports 
    # correspondant aux donnees de type-condition-port de ConfWebScenario
    # => transformation si ce n'est pas le cas :
# zzzz !!!! a ecrire
    #
    # II. Verification de l'existence dans exp des views
    # correspondant aux pages web res
    # => transformation si ce n'est pas le cas :
# zzzz !!!! a ecrire
    #
    # III. Verification du type_value des donnees a afficher/saisir :
    #
    # Verifie qu'il n'y a pas de donnee a afficher/saisir qui soit de
    # type_value none (voir les types (B)). 
    #
    # => transformation si ce n'est pas le cas :
    # Pour une donnee dont web_mode vaut 'read_write' ou 'read_only' les
    # seuls types permis/possibles pour type_value sont : boolean, double,
    # integer, string, map, set. Dans tout autre cas, web_mode est mis/force
    # a 'hidden'.
    #
    # Observations pour memo (15/12/2011) :
    #
    #   Lors de la saisie de valeurs de parametres (ports de conditions), gvle 
    #   propose les types : Boolean, Double, Integer, String, Map, Set, 
    #                       Table, Tuple, Matrix, Xml, Null
    #   Limitation vle : Sous gvle, impossible de donner/garder le type Null a
    #   un parametre (port de condition) d'un fichier vpz
    #
    #   Correspondance entre (A) les types choisis a la saisie (sous gvle) et
    #   (B) les types retournes par getConditionValueType (de pyvle) :
    #    (A)      (B)
    #   Boolean  boolean
    #   Double   double
    #   Integer  integer
    #   String   string
    #   Map      map
    #   Set      set
    #   Table    none
    #   Tuple    none
    #   Matrix   none
    #   Xml      none
    #   Null
    #
    #   Limitation pyvle : la methode getConditionSetValue de pyvle ne permet
    #   pas de lire les valeurs d'un parametre qui sont de type : none
    #   (types qu'il est permis/possible de lire :
    #   boolean, double, integer, string, map, set)
    #
    #   Remarque : le fait que la methode
    #   setConditionValue(cond, port, v, type_value, i) de pyvle ne puisse pas
    #   etre appliquee/utilisee pour une valeur de type map ou set est traite
    #   dans/au niveau de la methode tenterEcrireValeur (exp;py)
    #
    # Remarque : ConfWebScenario est aussi susceptible d'etre transforme dans
    # applicationRegles
    #
    ###########################################################################
    def assuranceCompatibilite( self, exp ) :

        compatibilite = True
        message = ""

        #######################################################################
        # Verification I.
        #######################################################################

        # liste des conditions du scenario
        liste_conditions_scn = exp.listConditions() 

        for page_web_def_name,page_web_def in self.pages_web_def.iteritems() :
            for donnee in page_web_def.liste_donnees :

                vpz_type = donnee.getVpzType()

                if vpz_type == tcw.CONST_vpz_type_condition_port :

                    cond = donnee.getVpzCondName()
                    port = donnee.getVpzPortName()

                    if cond in liste_conditions_scn :
                        if port in exp.listConditionPorts( cond ) :
                            pass # ok
                        else : 
                            compatibilite = False
                            message = message+" -- " + " port "+port+" dans condition "+cond + "n'existe pas dans le scenario"
                    else :
                        compatibilite = False
                        message = message +" -- "+ " condition "+cond + "n'existe pas dans le scenario"

        #######################################################################
        # Verification II.
        #######################################################################

        # liste des vues/views du scenario
        liste_views_scn = exp.listViews() 

        for page_web_res_name,page_web_res in self.pages_web_res.iteritems() :
            # iiiiiiiiiiii 
            view = page_web_res.vpz_view_name

            # zzzz !!!!!! view = self.getVpzViewName(donnee)

            if view != tcw.CONST_valeur_vide :
                if view in liste_views_scn :
                    pass # ok
                elif view == None :
                    compatibilite = False
                    message = message + " view None"
                else :
                    compatibilite = False
                    message = message+" -- "+ " view "+view + "n'existe pas dans le scenario"


        #######################################################################
        # Verification III.
        #######################################################################

        for page_web_def_name,page_web_def in self.pages_web_def.iteritems() :
            for infos_donnee_def in page_web_def.liste_donnees :

                vpz_type = infos_donnee_def.getVpzType()
                if tcw.isTypeConditionPortVpzType(vpz_type) :

                    web_mode = infos_donnee_def.getWebMode()
                    if tcw.isReadWriteMode(web_mode) or tcw.isReadOnlyMode(web_mode) :

                        cond = infos_donnee_def.getVpzCondName()
                        port = infos_donnee_def.getVpzPortName()
                        type_value = exp.get_type_value(cond,port)

                        if type_value not in ('boolean', 'integer', 'double', 'string', 'map', 'set' ) :
                            web_mode = tcw.CONST_web_mode_hidden # forcage
                            infos_donnee_def.setWebMode(web_mode)
                            message = message+" -- " + " parametre (" +cond+ "," +port+ ") force a hidden a cause de son type"

        return ( compatibilite, message )

    ###########################################################################
    #
    # Transcodage : retourne ou modifie
    # valeur prise par certaines informations de ConfWebScenario
    #
    # Methodes qui s'appuient sur le transcodage TranscodageConfWebScenario
    #
    ###########################################################################
    ###########################################################################
    # Trace ecran
    ###########################################################################
    def traceConfWebScenario( self ):

        print ""
        print "##################################"
        print "# CONFIGURATION ConfWebScenario"
        print "##################################"
        print ""
        print "##################################"
        print "Partie generale de configuration de l'application :"
        print "titre : ", self.conf_appli.titre
        print "description : ", self.conf_appli.description
        print "help : ", self.conf_appli.help
        print ""
        print "##################################"
        print "Liste ORDONNEE des noms des pages web de definition : ", self.pages_web_def_names
        print ""
        print "##################################"
        print "Partie configuration des pages web de definition :" 

        for (page_web_def_name,page_web_def) in self.pages_web_def.iteritems() :
            print ""
            print "Page web def ", page_web_def_name, " :"
            print "titre : ", page_web_def.infos_generales.titre
            print "description : ", page_web_def.infos_generales.description
            print "help : ", page_web_def.infos_generales.help
            for i,donnee in enumerate( page_web_def.liste_donnees) :
                print "Donnees de la page :", donnee
        print ""
        print "##################################"
        print "Liste ORDONNEE des noms des pages web de resultat : ", self.pages_web_res_names
        print ""
        print "##################################"
        print "Partie configuration des pages web de resultat :" 

        for (page_web_res_name,page_web_res) in self.pages_web_res.iteritems() :
            print ""
            print "Page web res ", page_web_res_name, " :"
            print "titre : ", page_web_res.infos_generales.titre
            print "description : ", page_web_res.infos_generales.description
            print "help : ", page_web_res.infos_generales.help
            print "vpz_view_name : ", page_web_res.vpz_view_name
            print "donnees_documentees : ", page_web_res.donnees_documentees
        print ""


###############################################################################
#
#
# Les classes des composants de la ConfWebScenario
#
#
###############################################################################

###############################################################################
# Informations generales d'une entite web (application, page web...)
###############################################################################
class InfosGenerales(object) :

    def __init__( self ) :
        self.titre = None
        self.description = None
        self.help = None

    def set( self, titre, description, help ) :
        self.setTitre( titre )
        self.setDescription( description )
        self.setHelp( help )

    # retourne la valeur (normalisee) de titre
    def getTitre( self ) :
        titre = tcw.CONST_valeur_vide # par defaut
        v = self.titre
        if type(v) == str :
            titre = v
        return titre

    def setTitre( self, titre ) :
        self.titre = titre

    # retourne la valeur (normalisee) de description
    def getDescription( self ) :
        description = tcw.CONST_valeur_vide # par defaut
        v = self.description
        if type(v) == str :
            description = v
        return description

    def setDescription( self, description ) :
        self.description = description

    # retourne la valeur (normalisee) de help
    def getHelp( self ) :
        help = tcw.CONST_valeur_vide # par defaut
        v = self.help
        if type(v) == str :
            help = v
        return help

    def setHelp( self, help ) :
        self.help = help

###############################################################################
# Informations generales d'une application
###############################################################################
class InfosGeneralesAppli(InfosGenerales) :

    # Valeurs titre, description, help
    def setValeurs( self, titre=None, description=None, help=None ):

        if type(titre) != str :
            titre = config['app_conf']['conf_web.TXTDEFAUT_titre_appli']
        if type(description) != str :
            description = config['app_conf']['conf_web.TXTDEFAUT_description_appli']
        if type(help) != str :
            help = config['app_conf']['conf_web.TXTDEFAUT_help_appli']
        self.set( titre, description, help )

    # Valeurs par defaut
    def setValeursDefaut( self ): # sert ?
        titre = config['app_conf']['conf_web.TXTDEFAUT_titre_appli']
        description = config['app_conf']['conf_web.TXTDEFAUT_description_appli']
        help = config['app_conf']['conf_web.TXTDEFAUT_help_appli']
        self.set( titre, description, help )

    # Valeurs par defaut pour chaque champ considere individuellement
    def setValeursDefautUneParUne( self ): 

        titre_appli = self.getTitre()
        if tcw.valeurVide( titre_appli ) :
            titre_appli = config['app_conf']['conf_web.TXTDEFAUT_titre_appli']
            self.setTitre( titre_appli ) 

        description_appli = self.getDescription()
        if tcw.valeurVide( description_appli ) :
            description_appli = config['app_conf']['conf_web.TXTDEFAUT_description_appli']
            self.setDescription( description_appli )

        help_appli = self.getHelp()
        if tcw.valeurVide( help_appli ) :
            help_appli = config['app_conf']['conf_web.TXTDEFAUT_help_appli']
            self.setHelp( help_appli )

    # Valeurs standard
    def setValeursStandard( self ):
        titre = config['app_conf']['conf_web.TXTSTANDARD_titre_appli']
        description = config['app_conf']['conf_web.TXTSTANDARD_description_appli']
        help = config['app_conf']['conf_web.TXTSTANDARD_help_appli']
        self.set( titre, description, help )

###############################################################################
# Informations generales d'une page web (de definition ou de resultat)
###############################################################################
class InfosGeneralesPageWeb(InfosGenerales) :

    def __init__( self ):
        InfosGenerales.__init__( self )
        self.numero_page = None

    def set( self, titre, description, help, numero_page ) : # le numero en plus
        self.setTitre( titre )
        self.setDescription( description )
        self.setHelp( help )
        self.setNumeroPage( numero_page )

    # Valeurs titre, description, help
    def setValeurs( self, titre=None, description=None, help=None, numero_page=None ):

        if type(titre) != str :
            titre = config['app_conf']['conf_web.TXTDEFAUT_titre_page_web']
        if type(description) != str :
            description = config['app_conf']['conf_web.TXTDEFAUT_description_page_web']
        if type(help) != str :
            help = config['app_conf']['conf_web.TXTDEFAUT_help_page_web']
        if type(numero_page) != str :
            numero_page = config['app_conf']['conf_web.TXTDEFAUT_numero_page_web']
        self.set( titre, description, help, numero_page )

    # Valeurs par defaut
    def setValeursDefaut( self ): # sert ?
        titre = config['app_conf']['conf_web.TXTDEFAUT_titre_page_web']
        description = config['app_conf']['conf_web.TXTDEFAUT_description_page_web']
        help = config['app_conf']['conf_web.TXTDEFAUT_help_page_web']
        numero_page = config['app_conf']['conf_web.TXTDEFAUT_numero_page_web']
        self.set( titre, description, help, numero_page )

    # Valeurs par defaut pour chaque champ considere individuellement
    def setValeursDefautUneParUne( self ): 

        titre = self.getTitre()
        if tcw.valeurVide( titre ) :
            titre = config['app_conf']['conf_web.TXTDEFAUT_titre_page_web']
            self.setTitre(titre)

        description = self.getDescription()
        if tcw.valeurVide( description ) :
            description = config['app_conf']['conf_web.TXTDEFAUT_description_page_web']
            self.setDescription(description)

        help = self.getHelp()
        if tcw.valeurVide( help ) :
            help = config['app_conf']['conf_web.TXTDEFAUT_help_page_web']
            self.setHelp(help)

        numero = self.getNumeroPage()
        if tcw.valeurVide( numero ) :
            numero = config['app_conf']['conf_web.TXTDEFAUT_numero_page_web']
            self.setNumeroPage(numero)

    # Valeurs standard page experience
    def setValeursStandardPageExperience( self, numero_page ):
        titre = config['app_conf']['conf_web.TXTSTANDARD_titre_page_experience']
        description = config['app_conf']['conf_web.TXTSTANDARD_description_page_experience']
        help = config['app_conf']['conf_web.TXTSTANDARD_help_page_experience']
        self.set( titre, description, help, numero_page )

    # Valeurs standard page plan d'experience
    def setValeursStandardPagePlan( self, numero_page ):
        titre = config['app_conf']['conf_web.TXTSTANDARD_titre_page_plan']
        description = config['app_conf']['conf_web.TXTSTANDARD_description_page_plan']
        help = config['app_conf']['conf_web.TXTSTANDARD_help_page_plan']
        self.set( titre, description, help, numero_page )

    # Valeurs standard page condition
    def setValeursStandardPageCondition( self, numero_page ):
        # attention titre contient en fait le prefixe du titre qui sera donne a la page condition
        titre = config['app_conf']['conf_web.TXTSTANDARD_prefixe_titre_page_condition']
        description = config['app_conf']['conf_web.TXTSTANDARD_description_page_condition']
        help = config['app_conf']['conf_web.TXTSTANDARD_help_page_condition']
        self.set( titre, description, help, numero_page )

    # Valeurs standard page web res (view/vue)
    def setValeursStandardPageWebRes( self, numero_page ):
        # attention titre contient en fait le prefixe du titre qui sera donne a la page web res

        titre = config['app_conf']['conf_web.TXTSTANDARD_prefixe_titre_page_view']
        description = "Page correspondant à la vue (du scénario) "
        print "description : ", description
        help = config['app_conf']['conf_web.TXTSTANDARD_help_page_view'] 
        self.set( titre, description, help, numero_page )

    # retourne la valeur (normalisee) du numero de page
    def getNumeroPage( self ) :
        numero_page = tcw.CONST_valeur_vide # par defaut
        v = self.numero_page
        if type(v) == str :
            numero_page = v
        return numero_page

    def setNumeroPage( self, v ) :
        self.numero_page = v

###############################################################################
# Documentation d'une donnee d'une page (intervient dans PageWebDef, PageWebRes)
###############################################################################
class DocDonnee(object) :

    def __init__( self ) :
        self.nom_frenchname = None
        self.nom_englishname = None
        self.description = None
        self.unite = None
        self.val_max = None
        self.val_min = None
        self.val_recommandee = None
        self.help = None

    # retourne la valeur (normalisee) de nom_frenchname 
    def getNomFrenchname( self ) :
        nom_frenchname = tcw.CONST_valeur_vide # par defaut
        v = self.nom_frenchname 
        if type(v) == str :
            nom_frenchname = v
        return nom_frenchname

    def setNomFrenchname( self, v ) :
        self.nom_frenchname = v

    # retourne la valeur (normalisee) de nom_englishname 
    def getNomEnglishname( self ) :
        nom_englishname = tcw.CONST_valeur_vide # par defaut
        v = self.nom_englishname 
        if type(v) == str :
            nom_englishname = v
        return nom_englishname

    def setNomEnglishname( self, v ) :
        self.nom_englishname = v

    # retourne la valeur (normalisee) de description 
    def getDescription( self ) :
        description = tcw.CONST_valeur_vide # par defaut
        v = self.description 
        if type(v) == str :
            description = v
        return description

    def setDescription( self, v ) :
        self.description = v

    # retourne la valeur (normalisee) de unite 
    def getUnite( self ) :
        unite = tcw.CONST_valeur_vide # par defaut
        v = self.unite 
        if type(v) == str :
            unite = v
        return unite

    def setUnite( self, v ) :
        self.unite = v

    # retourne la valeur (normalisee) de val_max 
    def getValMax( self ) :
        val_max = None # par defaut
        v = self.val_max 
        t = type(v)
        if (t==int) or (t==long) or (t==float) : # type numerique
            val_max = v
        return val_max

    def setValMax( self, v ) :
        self.val_max = v

    # retourne la valeur (normalisee) de val_min 
    def getValMin( self ) :
        val_min = None # par defaut
        v = self.val_min 
        t = type(v)
        if (t==int) or (t==long) or (t==float) : # type numerique
            val_min = v
        return val_min

    def setValMin( self, v ) :
        self.val_min = v

    # retourne la valeur (normalisee) de val_recommandee 
    def getValRecommandee( self ) :
        val_recommandee = None # par defaut
        v = self.val_recommandee 
        t = type(v)
        if (t==int) or (t==long) or (t==float) : # type numerique
            val_recommandee = v
        return val_recommandee

    def setValRecommandee( self, v ) :
        self.val_recommandee = v

    # retourne la valeur (normalisee) de help 
    def getHelp( self ) :
        help = tcw.CONST_valeur_vide # par defaut
        v = self.help 
        if type(v) == str :
            help = v
        return help

    def setHelp( self, v ) :
        self.help = v

    ###########################################################################
    # Donne une valeur par defaut a chaque attribut 'vide'
    ###########################################################################
    def setRegleValeursParDefaut( self ) :

        nom_frenchname = self.getNomFrenchname()
        if tcw.valeurVide( nom_frenchname ) :
            nom_frenchname = ''
        self.setNomFrenchname( nom_frenchname )

        nom_englishname = self.getNomEnglishname()
        if tcw.valeurVide( nom_englishname ) :
            nom_englishname = ''
        self.setNomEnglishname( nom_englishname )

        description = self.getDescription()
        if tcw.valeurVide( description ) :
            description = ''
        self.setDescription( description )

        unite = self.getUnite()
        if tcw.valeurVide( unite ) :
            unite = ''
        self.setUnite( unite )

        val_max = self.getValMax()
        if tcw.valeurVide(  val_max ) :
            val_max = None
        self.setValMax( val_max )

        val_min = self.getValMin()
        if tcw.valeurVide( val_min ) :
            val_min = None
        self.setValMin( val_min )

        val_recommandee = self.getValRecommandee()
        if tcw.valeurVide( val_recommandee ) :
            val_recommandee = None
        self.setValRecommandee( val_recommandee )

        help = self.getHelp()
        if tcw.valeurVide( help ) :
            help = ''
        self.setHelp( help )

###############################################################################
# Informations d'une donnee d'une page web de definition
###############################################################################

class InfosWebConf(object) :

    def __init__( self ):
        self.web_dimension = None
        self.web_name = None
        self.web_mode = None

    # retourne la valeur (normalisee) de web_dimension
    def getWebDimension( self ) :
        web_dimension = tcw.CONST_valeur_vide # par defaut
        v = self.web_dimension
        if type(v) == str :
            web_dimension = v
        return ( tcw.getStdWebDimension(web_dimension) )

    def setWebDimension( self, v ) :
        self.web_dimension = v

    def getWebName( self ) :
        web_name = tcw.CONST_valeur_vide # par defaut
        v = self.web_name
        if type(v) == str :
            web_name = v
        return web_name

    def setWebName( self, v ) :
        self.web_name = v

    # retourne la valeur (normalisee) de web_mode 
    def getWebMode( self ) :
        web_mode = tcw.CONST_valeur_vide # par defaut
        v = self.web_mode
        if type(v) == str :
            web_mode = v
        return ( tcw.getStdWebMode(web_mode) )

    def setWebMode( self, v ) :
        self.web_mode = v

    def setValeurs( self, web_dimension, web_name, web_mode ) :
        self.setWebDimension( web_dimension )
        self.setWebMode( web_mode )
        self.setWebName( web_name )

class InfosVpzId(object) :

    def __init__( self ):
        self.vpz_type = None
        self.vpz_cond_name = None
        self.vpz_port_name = None

    # retourne la valeur (normalisee) de vpz_type
    def getVpzType( self ) :
        vpz_type = tcw.CONST_valeur_vide # par defaut
        v = self.vpz_type
        if type(v) == str :
            vpz_type = v
        return ( tcw.getStdVpzType(vpz_type) )

    def setVpzType( self, v ) :
        self.vpz_type = v

    # retourne la valeur (normalisee) de vpz_cond_name
    def getVpzCondName( self ) :
        vpz_cond_name = tcw.CONST_valeur_vide # par defaut
        v = self.vpz_cond_name
        if type(v) == str :
            vpz_cond_name = v
        return vpz_cond_name

    def setVpzCondName( self, v ) :
        self.vpz_cond_name = v

    # retourne la valeur (normalisee) de vpz_port_name 
    def getVpzPortName( self ) :
        vpz_port_name = tcw.CONST_valeur_vide # par defaut
        v = self.vpz_port_name
        if type(v) == str :
            vpz_port_name = v
        return vpz_port_name

    def setVpzPortName( self, v ) :
        self.vpz_port_name = v

    def setValeurs( self, vpz_type, vpz_cond_name=None, vpz_port_name=None ) :
        self.setVpzType( vpz_type )
        if vpz_cond_name != None :
            self.setVpzCondName( vpz_cond_name )
        if vpz_port_name != None :
            self.setVpzPortName( vpz_port_name )

class InfosDonneeDef(object) :

    def __init__( self ):
        self.web_conf = InfosWebConf()
        self.vpz_id = InfosVpzId()
        self.doc = DocDonnee()

    def getWebDimension( self ) :
        return ( self.web_conf.getWebDimension() )
    def setWebDimension( self, v ) :
        self.web_conf.setWebDimension(v)

    def getWebMode( self ) :
        return ( self.web_conf.getWebMode() )
    def setWebMode( self, v ) :
        self.web_conf.setWebMode(v)

    def getWebName( self ) :
        web_name = tcw.CONST_valeur_vide # par defaut
        return ( self.web_conf.getWebName() )
    def setWebName( self, v ) :
        self.web_conf.setWebName( v )

    def getVpzType( self ) :
        return ( self.vpz_id.getVpzType() )
    def setVpzType( self, v ) :
        self.vpz_id.setVpzType( v )

    def getVpzCondName( self ) :
        return self.vpz_id.getVpzCondName()
    def setVpzCondName( self, v ) :
        self.vpz_id.setVpzCondName( v )

    def getVpzPortName( self ) :
        return self.vpz_id.getVpzPortName()
    def setVpzPortName( self, v ) :
        self.vpz_id.setVpzPortName( v )

    def setWebConfValeurs( self, web_dimension, web_name, web_mode ) :
        self.web_conf.setValeurs( web_dimension, web_name, web_mode )

    def setVpzIdValeurs( self, vpz_type, vpz_cond_name=None, vpz_port_name=None ) :
        self.vpz_id.setValeurs( vpz_type, vpz_cond_name, vpz_port_name )


###############################################################################
# Informations d'une page web de definition
###############################################################################
class PageWebDef(object) :

    def __init__( self ):

        # info_generale : l'info propre a la page entiere 
        self.infos_generales = InfosGeneralesPageWeb()

        # liste des donnees InfosDonneeDef de la page
        self.liste_donnees = []

###############################################################################
# Informations d'une page web de resultat
###############################################################################
class PageWebRes(object) :

    def __init__( self ):

        # info_generale : l'info propre a la page entiere 
        self.infos_generales = InfosGeneralesPageWeb()

        # nom de la view/vue
        self.vpz_view_name = None

        # dict des donnees documentees qui pourront etre sollicitees
        # au moment d'afficher la page (les donnees de la view/vue)
        # donnees_documentees est un dict, peut-etre vide
        # donnees_documentees ne contient pas forcement toutes, ni que, des
        # donnees appartenant a la view/vue
        self.donnees_documentees = {}

