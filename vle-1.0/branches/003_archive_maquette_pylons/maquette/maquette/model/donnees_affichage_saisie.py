#-*- coding:utf-8 -*-

###############################################################################
# File donnees_affichage_saisie.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# import copy
import pickle
from ast import literal_eval

from maquette.model.conf_web_scenario import TranscodageConfWebScenario 

# pour utilisation dans DonneeDef
# dont format grandement calque sur celui de ConfWebScenario
tcw = TranscodageConfWebScenario()

###############################################################################
#
# Classes pour gestion de l'affichage et la saisie de donnees :
#
# - donnees d'un scenario,
# - donnees d'un paquet,
# - donnees d'une representation graphique
# ...
#
###############################################################################


###############################################################################
# Methode getValeurs
# Retourne la liste des valeurs saisies
###############################################################################
def getValeursSaisies( params ):
    listeValeurs = list()
    for (k,v) in params.iteritems() :
        listeValeurs.append( v.encode() )
    return listeValeurs

###############################################################################
# Methode existe_dans
# Retourne True s'il existe dans params un element de type/name name 
###############################################################################
def existe_dans( name, params ):
    return ( name in params.keys() )

###############################################################################
# Methode get_valeur
# Retourne la valeur de l'unique element de type/name name (plus exactement
# le premier trouve) saisi dans params.
###############################################################################
def get_valeur( name, params ):
    for (n,v) in params.iteritems() :
        if n == name :
            return v


###############################################################################
###############################################################################
#
# TRAITEMENTS UTILISES DANS/POUR DEFINITION D'UN SCENARIO
#
###############################################################################
###############################################################################

#
# DonneeDef : dans definition.py
#

###########################################################################
# Classe MotCode
#
# Un mot qui contient differents champs d'identification et valeurs 
# associees. Se presente sous la forme : "xxx=aaa/yyy=bbb/zzz=ccc".
#
###########################################################################
class MotCode(object):

    def __init__(self, mot) :
        self.mot = mot

    # la methode contient verifie que le mot contient le champ
    # d'identification motcle (plus exactement la chaine de caractere
    # motcle+"=")
    def contientMotCle(self, motcle) :
        return (motcle+'=') in self.mot

    # la methode extraireDeMot extrait de mot la valeur correspondant a motcle
    # exemple : retourne "bbb" pour motcle="yyy"
    def extraireDeMot(self, motcle) :
        mot_extrait=self.mot.split(motcle+'=')
        mot_extrait=mot_extrait[-1].split('/')
        mot_extrait=mot_extrait[0]
        return mot_extrait

    #######################################################################
    # Cas particulier d'un mot avec une terminaison de la forme "-r-c"
    #
    # la methode enlever_terminaison enleve de mot sa terminaison qui est 
    # de la forme "-r-c" et retourne cette terminaison (r,c).
    # Par contre si mot n'a pas ce type de terminaison, alors ne le
    # modifie pas et retourne None.
    #######################################################################
    def enlever_terminaison(self) :

        mot_extrait=self.mot.split('-')
        nb = len(mot_extrait)

        if nb < 3 : # terminaison "-r-c" non trouvee
            return None

        else : # terminaison au bon format "-r-c"

            mot_i2=mot_extrait[ nb-1 ] # dernier
            mot_i1=mot_extrait[ nb-2 ] # avant dernier

            if len(mot_i2)>=1 and len(mot_i1)>=1 :

                # suppression de "-r-c" dans mot
                terminaison = '-' + mot_i1 + '-' + mot_i2
                self.mot=self.mot.rstrip( terminaison )

                return (mot_i1,mot_i2)

            if len(mot_i2)>=1 and len(mot_i1)>=1 :
                return None


###############################################################################
# Classe DonneeSaisie
#
# Donnee correspondant a la saisie d'une DonneeDef
#
# name : name est le nom de la DonneeSaisie, correspondant a name dans
# la page de saisie. name contient differents champs d'identification et
# caracteristiques de la donnee (indbloc, inddata), dont
# l'interpretation/la reconstruction se fait au sein d'une liste de
# DonneeSaisie.
# name se presente sous la forme : "xxx=aaa/yyy=bbb/zzz=ccc".
# value : valeur de la DonneeSaisie. 
#
###############################################################################
class DonneeSaisie(object):

    def __init__(self, name, value) :
        self.name = MotCode(name)
        self.value = value

    ###########################################################################
    # verifie que name est de la forme "indbloc=xxx/inddata=zzz"
    ###########################################################################
    def formatNameOk(self) :
        if self.name.contientMotCle('indbloc') and self.name.contientMotCle('inddata') :
            return True
        else :
            return False

    ###########################################################################
    # Methode decouper, decoupage de (name,value) :
    #
    # name etant de la forme "indbloc=xxx/inddata=zzz", retourne
    # le dict { 'indbloc':xxx, 'inddata':zzz, 'valeurs':value }
    #
    # Plus exactement :
    # - dans 'valeurs', value saisi (type str) reste dans ce type.
    # - dans 'indbloc' et 'inddata', indbloc et inddata saisis (type str) ont
    #   ete convertis en int (leur type reel).
    #
    # Remarque : dans le dict retourne
    # { 'indbloc':xxx, 'inddata':zzz, 'valeurs':value },
    # le fait que value vaille None revele un probleme/erreur.
    #
    # Remarque : avant d'appeler decouper, s'assurer du bon format de
    # name en appelant formatNameOk.
    #
    ###########################################################################
    def decouper(self) :

        indbloc = int( self.name.extraireDeMot( 'indbloc') )
        inddata = int( self.name.extraireDeMot( 'inddata') )
        value = self.value

        element = { 'indbloc':indbloc, 'inddata':inddata, 'valeurs':value }

        return element

    ###########################################################################
    # Methode decouper, decoupage de (name,value) :
    #
    # name etant de la forme "indbloc=xxx/inddata=zzz", retourne
    # le dict { 'indbloc':xxx, 'inddata':zzz, 'valeurs':value }
    #
    # Plus exactement :
    # - dans 'valeurs', value saisi (type str) a ete converti en son type
    #   'attendu/reel'. En cas d'erreur de cette conversion, None est
    #   attribue a 'valeurs'.
    #
    # - dans 'indbloc' et 'inddata', indbloc et inddata saisis (type str) ont
    #   ete convertis en int (son type reel).
    #
    # Remarque : dans le dict retourne
    # { 'indbloc':xxx, 'inddata':zzz, 'valeurs':value },
    # le fait que value vaille None revele un probleme/erreur.
    #
    # Remarque : avant d'appeler decouper, s'assurer du bon format de
    # name en appelant formatNameOk.
    #
    ###########################################################################
    def ANCIEN_INUSITEdecouper(self) :

        indbloc = int( self.name.extraireDeMot( 'indbloc') )
        inddata = int( self.name.extraireDeMot( 'inddata') )

        # Determination de la valeur value attribuee a 'valeurs' :
        value = None # par defaut
        try: # essai d'extraction/conversion
            c = literal_eval( self.value ) # contenu du str
            # extraction ok, s'assurer du format
            try:
                value = c
            except (SyntaxError): # echec de conversion
                print "type ERREUR !!!"
                #pass
        except (ValueError, SyntaxError): # cas str
            value = self.value
            print "literal_eval ERREUR pour self.value : ", self.value
            #pass

        element = { 'indbloc':indbloc, 'inddata':inddata, 'valeurs':value }

        return element

###############################################################################
###############################################################################
#
# INFORMATIONS D'UN PAQUET POUR LE CHOIX D'UN SCENARIO
#
###############################################################################
###############################################################################

###############################################################################
# PaquetInfosFormulaireScenario
#
# Le choix d'un scenario dans un paquet necessite d'afficher les paquets.
# PaquetInfosFormulaireScenario correspond aux informations d'affichage d'un 
# paquet dans le cas du choix d'un scenario (pas dans le cas d'administration
# des paquets), lorsqu'il s'agit d'un paquet ok, sans probleme (type Paquet).
#
# uid, nom et nom_pkg sont des informations d'origine du paquet
# liste_vpz est la liste des fichier vpz du paquet
# visible : booleen qui indique si la liste_vpz est a afficher ou a cacher
# Par defaut, un paquet public (sans mot de passe) est visible et un paquet
# prive (avec mot de passe) est non visible.
#
###############################################################################
class PaquetInfosFormulaireScenario(object):

    def __init__( self, paquet ) :

        self.uid = paquet.uid
        self.nom = paquet.nom
        self.nom_pkg = paquet.nom_pkg

        # iii ajouts a revoir/repreciser
        self.description = paquet.description
        self.nom_responsable = paquet.nom_responsable
        self.mail_responsable = paquet.mail_responsable
        self.nom_resp_scientifique = paquet.nom_resp_scientifique
        self.nom_resp_informatique = paquet.nom_resp_informatique
        self.version_vle = paquet.version_vle

        if paquet.password=="" :
            self.visible = True
        else :
            self.visible = False

        self.liste_vpz = paquet.get_liste_vpz()


###############################################################################
# PaquetNotOkInfosFormulaireScenario
#
# Le choix d'un scenario dans un paquet necessite d'afficher les paquets.
# PaquetNotOkInfosFormulaireScenario correspond aux informations d'affichage
# d'un paquet dans le cas du choix d'un scenario (pas dans le cas
# d'administration des paquets), lorsqu'il s'agit d'un paquet not ok, qui
# pose probleme (type PaquetNotOk).
#
###############################################################################
class PaquetNotOkInfosFormulaireScenario(object):

    def __init__(self, paquet_not_ok ) :
        self.nom = paquet_not_ok.paquet.nom
        self.nom_pkg = paquet_not_ok.paquet.nom_pkg
        self.msg_not_ok = paquet_not_ok.msg_not_ok


###############################################################################
###############################################################################
#
# POUR UN PAQUET (CAS ADMINISTRATION)
#
###############################################################################
###############################################################################

###############################################################################
# Methode getIndicePaquetDansListe
#
# Retourne l'indice de l'element paquet (type Paquet) de liste_paquets
# dont uid vaut uid
###############################################################################
def getIndicePaquetDansListe( uid, liste_paquets ) :
    liste_index = [ i for i,p in enumerate(liste_paquets) if p.uid==uid ]
    if len(liste_index) >= 1 :
        return liste_index[0]
    else :
        return None

###############################################################################
# Methode motDePasseSaisi
#
# Retourne la valeur de mot de passe saisie (unique valeur saisie)
###############################################################################
def motDePasseSaisi( params ) :
    listeValeurs = getValeursSaisies( params )
    mdp = listeValeurs[0] # unique valeur saisie
    return mdp

###############################################################################
###############################################################################
#
# POUR UNE REPRESENTATION GRAPHIQUE
#
#
# Rappel :
# - Format de type_capture_vars d'une simulation simple (voir MonoResultat) :
#   list ( type, timestep, nomvar, valeurs ).
# - Format de type_capture_vars d'une simulation multiple (voir
#   MultiResultat) :
#   tuple de tuple de list ( type, timestep, nomvar, valeurs ).
#
# Dans le but de simplifier la gestion des affichages (reduire le code de
# pages html), type_capture_vars est traite/gere dans un seul format au
# niveau des affichages/saisies : celui de la simulation multiple. Pour cela,
# au niveau de la gestion des saisies/affichages, type_capture_vars d'une
# simulation simple va etre transforme (voir methode
# transformationArtificielle) : 'ajout de 2 dimensions' pour se trouver dans
# le meme format qu'en multi-simulation.
#
# Apres cette transformation artificielle appliquee pour une simulation
# simple, type_capture_vars contient un unique element type_capture_vars[0][0].
# Si jamais besoin de distinguer le cas de simulation simple du cas de
# simulation multiple qui ne comporterait qu'une seule experience : utiliser
# l'attribut type_simulation ("mono_simulation" ou "multi_simulation") de
# Resultat.
#
###############################################################################
###############################################################################


###############################################################################
# Methode transformationArtificielle
#
# Met type_capture_vars d'une simulation simple (tcv_d_origine) au format
# d'une simulation multiple (tcv_transforme).
#
###############################################################################
def transformationArtificielle( tcv_d_origine ) :
    tcv_transforme = list()
    tcv = list()
    tcv.append( tcv_d_origine )
    tcv_transforme.append( tcv )
    return tcv_transforme

# Verifie que le MotCode motCode est de la forme "type=xxx/timestep=yyy/nomvar=zzz"
# remarque : ce controle fait abstraction de la terminaison "-r-c"
def formatVarOk( motCode ) :
    if motCode.contientMotCle('type') and motCode.contientMotCle('timestep') and motCode.contientMotCle('nomvar') :
        return True
    else :
        return False

###############################################################################
# Methode definir_Y
#
# Cas d'une representation graphique ou l'abscisse est la meme pour tous les
# traces. Cas de simulation simple et de simulation multiple.
#
# Definit la liste liste_XY des (var en abscisse, var en ordonnee) fonction 
# des choix faits dans params. Les elements utiles de params sont : (axe,var)
# avec axe= "X" ou "Y".
#
# Dans le cas type_simulation valant "multi_simulation" :
#
# - var est de la forme type=xxx/timestep=yyy/nomvar=zzz-r-c"
#
# - Dans un element (elementX,elementY) de liste_XY,
#   elementX et elementY sont de la forme
#   ( valeur de r, valeur de c, valeur de type, valeur de timestep,
#     valeur de nomvar (sans terminaison) )
#
# Dans le cas type_simulation valant "mono_simulation" :
#
# - var est de la forme type=xxx/timestep=yyy/nomvar=zzz"
#
# - Dans un element (elementX,elementY) de liste_XY,
#   elementX et elementY sont de la forme
#   ( valeur de type, valeur de timestep, valeur de nomvar )
#
# Retourne un booleen qui indique s'il y a eu ou non probleme lors de la
# constitution de liste_XY : X ou Y inexistant...
#
###############################################################################
def definir_Y( liste_XY, params, type_simulation ):

    cr_ok = True # par defaut

    ###########################################################################
    # abscisse X unique (meme pour tous traces)	
    ###########################################################################
    X = None
    for ( axe, var ) in params.iteritems() :

        if axe == "X" :
            mot = MotCode( var.encode() )
            if formatVarOk( mot ) :
            # var est bien de la forme "type=xxx/timestep=yyy/nomvar=zzz"

                ###############################################################
                # cas "multi_simulation" :
                ###############################################################
                if type_simulation == "multi_simulation" :
                # var est attendu de la forme "type=xxx/timestep=yyy/nomvar=zzz-r-c"

                    i1i2 = mot.enlever_terminaison()
                    if i1i2 != None :
                    # var a bien terminaison de la forme attendue "-r-c"
                        (mot_i1,mot_i2) = i1i2
                        element = ( int(mot_i1),
                                    int(mot_i2),
                                    mot.extraireDeMot('type'),
                                    float(mot.extraireDeMot('timestep')),
                                    mot.extraireDeMot('nomvar') )
                        X = element 
                    else :
                    # var n'a pas terminaison de la forme attendue "-r-c"
                        cr_ok = False

                ###############################################################
                # cas "mono_simulation" :
                ###############################################################
                else : # "mono_simulation" et de plus par defaut
                # var est attendu de la forme "type=xxx/timestep=yyy/nomvar=zzz"
                # (sans terminaison "-r-c")

                    element = ( mot.extraireDeMot('type'),
                                float(mot.extraireDeMot('timestep')),
                                mot.extraireDeMot('nomvar') )
                    X = element 
            else :
            # var n'est pas de la forme "type=xxx/timestep=yyy/nomvar=zzz"
                cr_ok = False

    if X == None : # X inexistant
        cr_ok = False
    else :

        #######################################################################
        # ordonnees
        #######################################################################
        for ( axe, var ) in params.iteritems() :

            if axe == "Y" :
                mot = MotCode( var.encode() )
                if formatVarOk( mot ) :
                # var est bien de la forme "type=xxx/timestep=yyy/nomvar=zzz"

                    ###########################################################
                    # cas "multi_simulation" :
                    ###########################################################
                    if type_simulation == "multi_simulation" :
                    # var est attendu de la forme "type=xxx/timestep=yyy/nomvar=zzz-r-c"

                        i1i2 = mot.enlever_terminaison()
                        if i1i2 != None :
                        # var a bien terminaison de la forme attendue "-r-c"
                            (mot_i1,mot_i2) = i1i2
                            element = ( int(mot_i1),
                                        int(mot_i2),
                                        mot.extraireDeMot('type'),
                                        float(mot.extraireDeMot('timestep')),
                                        mot.extraireDeMot('nomvar') )
                            liste_XY.append( (X,element) )
                        else :
                        # var n'a pas terminaison de la forme attendue "-r-c"
                            cr_ok = False

                    ###########################################################
                    # cas "mono_simulation" :
                    ###########################################################
                    else : # "mono_simulation" et de plus par defaut
                    # var est attendu de la forme "type=xxx/timestep=yyy/nomvar=zzz"
                    # (sans terminaison "-r-c")
                        element = ( mot.extraireDeMot('type'),
                                    float(mot.extraireDeMot('timestep')),
                                    mot.extraireDeMot('nomvar') )
                        liste_XY.append( (X,element) )

                else :
                # var n'est pas de la forme "type=xxx/timestep=yyy/nomvar=zzz"
                    cr_ok = False

        if len(liste_XY) == 0 : # Y inexistant
            cr_ok = False

    return cr_ok

###############################################################################
# Methode definir_XY
#
# Cas d'une representation graphique ou l'abscisse n'est pas forcement la
# meme pour tous les traces. Cas de simulation simple et de simulation multiple.
#
# Definit la liste liste_XY des (var en abscisse, var en ordonnee) fonction 
# des choix faits dans params. Les elements utiles de params sont : ("XY",var)
# avec var de la forme "Y(X)".
#
# Dans le cas type_simulation valant "multi_simulation" :
#
# - var est de la forme "Y(X)", ou X et Y sont de la forme
#   type=xxx/timestep=yyy/nomvar=zzz-r-c"
#
# - Dans un element (elementX,elementY) de liste_XY,
#   elementX et elementY sont de la forme
#   ( valeur de r, valeur de c, valeur de type, valeur de timestep,
#     valeur de nomvar (sans terminaison) )
#
# Dans le cas type_simulation valant "mono_simulation" :
#
# - var est de la forme "Y(X)", ou X et Y sont de la forme
#   type=xxx/timestep=yyy/nomvar=zzz"
#
# - Dans un element (elementX,elementY) de liste_XY,
#   elementX et elementY sont de la forme
#   ( valeur de type, valeur de timestep, valeur de nomvar )
#
# Retourne un booleen qui indique s'il y a eu ou non probleme lors de la
# constitution de liste_XY : aucun couple (X,Y)...
#
###############################################################################
def definir_XY( liste_XY, params, type_simulation ):

    # la methode extraireX sort X de mot qui est de la forme Y(X)
    def extraireX( mot ) :
        mot_extrait = mot.split(')')[0]
        X = mot_extrait.split('(')[-1]
        return X

    # la methode extraireY sort Y de mot qui est de la forme Y(X)
    def extraireY( mot ) :
        Y = mot.split('(')[0]
        return Y

    ###########################################################################
    # Le traitement :
    ###########################################################################

    cr_ok = True # par defaut

    for ( type, var ) in params.iteritems() :

        if type == "XY" :

            # var est attendu de la forme Y(X)
            X = extraireX( var.encode() )
            Y = extraireY( var.encode() )
            motX = MotCode( X )
            motY = MotCode( Y )

            if formatVarOk( motX ) and formatVarOk( motY ) :
            # X et Y sont bien de la forme "type=xxx/timestep=yyy/nomvar=zzz"

                ###############################################################
                # cas "multi_simulation" :
                ###############################################################
                if ( type_simulation == "multi_simulation" ) :
                # X et Y sont attendus de la forme "type=xxx/timestep=yyy/nomvar=zzz-r-c"

                    motX_i1i2 = motX.enlever_terminaison()
                    motY_i1i2 = motY.enlever_terminaison()

                    if motX_i1i2 != None and motY_i1i2 != None :
                    # X et Y ont bien terminaison de la forme attendue "-r-c"

                        (motX_i1,motX_i2) = motX_i1i2
                        (motY_i1,motY_i2) = motY_i1i2

                        elementX = ( int(motX_i1),
                                     int(motX_i2),
                                     motX.extraireDeMot('type'),
                                     float(motX.extraireDeMot('timestep')),
                                     motX.extraireDeMot('nomvar') )
                        elementY = ( int(motY_i1),
                                     int(motY_i2),
                                     motY.extraireDeMot('type'),
                                     float(motY.extraireDeMot('timestep')),
                                     motY.extraireDeMot('nomvar') )
    
                        liste_XY.append( (elementX,elementY) )
    
                    else :
                    # X ou/et Y n'a pas terminaison de la forme attendue "-r-c"
                        cr_ok = False

                ###############################################################
                # cas "mono_simulation" :
                ###############################################################
                else : # "mono_simulation" et de plus par defaut
                # X et Y sont attendus de la forme "type=xxx/timestep=yyy/nomvar=zzz"
                # (sans terminaison "-r-c")

                    elementX = ( motX.extraireDeMot('type'),
                                 float(motX.extraireDeMot('timestep')),
                                 motX.extraireDeMot('nomvar') )
                    elementY = ( motY.extraireDeMot('type'),
                                 float(motY.extraireDeMot('timestep')),
                                 motY.extraireDeMot('nomvar') )
    
                    liste_XY.append( (elementX,elementY) )

            else :
            # X ou/et Y n'est pas de la forme "type=xxx/timestep=yyy/nomvar=zzz"
                cr_ok = False

    if len(liste_XY) >= 1 : # au moins un (X,Y)
        cr_ok = True
    else : # aucun (X,Y)
        cr_ok = False

    return cr_ok

