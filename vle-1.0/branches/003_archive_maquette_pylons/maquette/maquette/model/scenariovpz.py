#-*- coding:utf-8 -*-

###############################################################################
# File scenariovpz.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################


###############################################################################
# ScenarioVpz
#
# Informations d'identification du fichier vpz qui a ete selectionne dans un
# paquet pour etre simule.
#
###############################################################################

class ScenarioVpz(object):

    ###########################################################################
    # Construction (partielle) a partir du paquet choisi 
    ###########################################################################
    def __init__(self, paquet):

        # le paquet dont il fait partie
        self.paquet = paquet

        # le vpz
        self.nom_vpz = None

    ###########################################################################
    # Construction (suite) a partir du choix du fichier vpz
    ###########################################################################
    def set( self, id_scenario ) :
        # le vpz
        self.nom_vpz = self.paquet.get_nom(id_scenario)

    def get_paquet(self):
        return self.paquet
    def get_paquet_uid(self):
        return self.paquet.uid
    def get_paquet_nom(self):
        return self.paquet.nom
    def get_paquet_nom_pkg(self):
        return self.paquet.nom_pkg
    def get_nom_vpz(self):
        return self.nom_vpz

    def set_nom_vpz(self, nom_vpz):
        self.nom_vpz = nom_vpz

