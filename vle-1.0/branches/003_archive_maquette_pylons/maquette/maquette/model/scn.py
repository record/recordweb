#-*- coding:utf-8 -*-

###############################################################################
# File scn.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

import copy

from maquette.model.scenariovpz import ScenarioVpz
from maquette.model.exp_scenariovpz import ExpScenarioVpz
from maquette.model.definition import Definition
from maquette.model.exp_config_web import ExpConfigWeb
from maquette.model.conf_web_scenario import ConfWebScenario
from maquette.model.espace_exploitation import EspaceExploitation
from maquette.model.espace_exploitation import choisirPrefixeExploitation
from maquette.model.espace_exploitation import getPrefixeRepertoireExploitation
from maquette.model.espace_exploitation import verificationFichierOk

#import logging
#log = logging.getLogger(__name__)
#log.debug('Your trace message')

###############################################################################
# Scn
#
# Une exploitation d'un scenario par un utilisateur :
# visualisation, manipulation, simulation...
#
###############################################################################

class Scn( object ):

    # Construction/creation
    def __init__( self, paquet, id_scenario ) :

        self.scenario_vpz_d_origine = None  # ScenarioVpz 
        self.scenario_vpz = None            # ScenarioVpz 
        self.definition = None              # Definition
        self.resultat = None                # Resultat de simulation
        self.espace_exploitation = None     # espace d'exploitation
        self.configuration_web = None       # configuration appli web (contenu
                                            # des pages web...)

        #######################################################################
        # scenario_vpz_d_origine
        #######################################################################

        # Initialisation en fonction du vpz et de son paquet ##################

        # initialisation du scenario d'origine selon le paquet et le vpz choisis
        self.scenario_vpz_d_origine = ScenarioVpz( paquet )
        self.scenario_vpz_d_origine.set( id_scenario )

        #######################################################################
        # espace_exploitation
        #######################################################################

        # creation de l'espace d'exploitation
        prefixe = choisirPrefixeExploitation( getPrefixeRepertoireExploitation(), self.scenario_vpz_d_origine )
        self.espace_exploitation = EspaceExploitation( self.scenario_vpz_d_origine, prefixe )

        #######################################################################
        # scenario_vpz
        #######################################################################

        # creation de scenario_vpz relatif a espace_exploitation,
        # a partir de scenario_vpz_d_origine :
        paquet = copy.deepcopy( self.scenario_vpz_d_origine.get_paquet() )
        paquet.nom_pkg = self.espace_exploitation.nom_pkg
        paquet.nom = 'issu de ' + self.scenario_vpz_d_origine.get_paquet_nom()
        paquet.description = ' blablabla bla bla bla'
        scenario_vpz = ScenarioVpz( paquet )
        scenario_vpz.set_nom_vpz( self.espace_exploitation.nom_vpz )
        self.scenario_vpz = scenario_vpz

        #######################################################################
        # prealable a l'initialisation de l'objet de configuration web
        # (exp besoin pour la configuration web par defaut
        #  et pour controle de compatibilite avec la configuration web)
        #######################################################################
        exp = ExpScenarioVpz( self.scenario_vpz )

        #######################################################################
        # configuration_web
        #######################################################################
        configurerParDefaut = True # par defaut

        # essai de configuration web a partir fichier vpz de configuration web

        # zzz : pour l'instant on prend comme fichier .vpz : 
        # sous le repertoire web du paquet, le fichier vpz de
        # nom = concatenation 'web_' et nom du scn

        nomAbsoluConfigWeb = self.espace_exploitation.getRepertoireConfigWeb()+'/web_'+self.scenario_vpz_d_origine.get_nom_vpz()

        if verificationFichierOk(nomAbsoluConfigWeb) :
        # le fichier vpz de configuration existe
            exp_config_web = ExpConfigWeb( nomAbsoluConfigWeb )
            configuration_web = ConfWebScenario( "exp_conf_web", exp_config_web )

            #print "configuration_web TRACE : "
            #configuration_web.traceConfWebScenario()

            # controle de compatibilite entre exp et configuration_web
            (compatibilite,message) = configuration_web.assuranceCompatibilite( exp )
            if compatibilite :
            # configuration web a partir fichier vpz de configuration web ok/acceptee
                configurerParDefaut = False
            else :
                print "Scn erreur !!! : incompatibilite entre le fichier vpz de configuration web et le scenario."
                print "Pour memo le fichier vpz de configuration web : ", nomAbsoluConfigWeb 
                print "Pour memo le fichier vpz scenario : ", self.scenario_vpz_d_origine.get_nom_vpz()

            print "message en retour assuranceCompatibilite : ", message
        else :
            print "Scn erreur !!! : le fichier vpz de configuration n'existe pas"
            print "Pour memo le fichier vpz de configuration web : ", nomAbsoluConfigWeb 

        if configurerParDefaut :
            configuration_web = ConfWebScenario( "par_defaut", exp )

            # controle de compatibilite entre exp et configuration_web par defaut
            (compatibilite,message) = configuration_web.assuranceCompatibilite( exp )
            if not compatibilite :
                print "Scn erreur !!! : incompatibilite entre la configuration web par defaut et le scenario."
                print "Pour memo le fichier vpz scenario : ", self.scenario_vpz_d_origine.get_nom_vpz()
            print "message en retour assuranceCompatibilite : ", message

        self.configuration_web = configuration_web 

        #######################################################################
        # definition
        #######################################################################

        # initialisation de l'objet de definition du scenario

        # exp = ExpScenarioVpz( self.scenario_vpz ) deja fait plus haut

        self.definition = Definition( exp, self.configuration_web )

    # Destruction/suppression
    def supprimer( self ) :
        self.espace_exploitation.supprimerRepExploitation()
        del self

