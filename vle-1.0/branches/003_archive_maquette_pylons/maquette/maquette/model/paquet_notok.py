#-*- coding:utf-8 -*-

###############################################################################
# File paquet_notok.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

import copy

from maquette.model.espace_exploitation import getListeUtilePkgs

###############################################################################
#
# Gestion des paquets qui posent probleme
#
###############################################################################

###############################################################################
# PaquetNotOk
#
# PaquetNotOk correspond a un paquet qui pose probleme. PaquetNotOk contient,
# en plus du paquet auquel il correspond, des informations sur le(s)
# probleme(s) rencontre(s).
#
# Critere(s) pour considerer qu'un paquet pose probleme : voir methode
# estUnPaquetNotOk.
#
###############################################################################
class PaquetNotOk(object) :

    def __init__(self, paquet) :
        self.paquet = copy.deepcopy( paquet ) # copie du paquet correspondant
        self.msg_not_ok = "" # message supplementaire

    def ajouterMessage(self, msg) :
        if self.msg_not_ok == "" :
            entre = ""
        else :
            entre = " - "
        self.msg_not_ok = self.msg_not_ok + entre + msg

###############################################################################
# Methode separer
#
# separation de la liste paqs (elements de type Paquet)
# en liste des paquets ok d'un cote (elements de type Paquet)
# et liste des paquets not ok de l'autre (elements de type PaquetNotOk)
#
# Critere(s) pour considerer qu'un paquet pose probleme : voir methode
# estUnPaquetNotOk.
#
###############################################################################
def separer( paqs, paqs_ok, paqs_not_ok ) :

    for paq in paqs :
        if estUnPaquetNotOk( paq ) :
            paq_not_ok = PaquetNotOk(paq)
            message="paquet "+str(paq_not_ok.paquet.nom_pkg)+" non trouvé sous pkgs"
            paq_not_ok.ajouterMessage(message)
            paqs_not_ok.append( paq_not_ok )
        else :
            paqs_ok.append( paq )

###############################################################################
# Methode estUnPaquetNotOk determine si un paquet (type Paquet) pose probleme
#
# Critere(s) actuel(s) pour considerer qu'un paquet pose probleme :
# Le paquet nom_pkg ne se trouve pas sous pkgs.
#
###############################################################################
def estUnPaquetNotOk( paquet ) :

    # liste utile des paquets/projets existants sous pkgs
    liste_paquets_sous_pkgs = getListeUtilePkgs()

    return not paquet.nom_pkg in liste_paquets_sous_pkgs

