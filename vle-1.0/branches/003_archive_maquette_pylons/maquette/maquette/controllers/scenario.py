#-*- coding:utf-8 -*-

###############################################################################
# File scenario.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from pylons import request, response, session, tmpl_context as c
from pylons.controllers.util import abort, redirect_to

from maquette.lib.helpers import flash
from maquette.lib.base import BaseController, render

from maquette.model.scn import Scn
from maquette.model import Paquet
from maquette.model import donnees_affichage_saisie
from maquette.model.definition import Definition
from maquette.model.donnees_affichage_saisie import PaquetInfosFormulaireScenario
from maquette.model.donnees_affichage_saisie import PaquetNotOkInfosFormulaireScenario
from maquette.model.paquet_notok import separer
from maquette.model.exp_scenariovpz import ExpScenarioVpz
from maquette.model.resultat import Resultat
from maquette.model.erreurs import Erreurs

import copy

import threading
imageThreadLock = threading.Lock() # make sure methods for graphics do not overwrite each other

# import logging
# log = logging.getLogger(__name__)

###############################################################################
# ScenarioController
#
# Exploitations des scenarios par les utilisateurs
#
# Il s'agit de choisir un scenario (dans un paquet),
# puis de le simuler apres en avoir visualise et eventuellement modifie la
# configuration (parametrage),
# et enfin d'exploiter sous diverses formes les resultats de la simulation
# effectuee (les visualiser a l'ecran, telecharger...).
#
###############################################################################

class ScenarioController(BaseController):

    def __before__(self):
        self.paquet_q = Paquet.get_table()
        # self.username = request.environ.get('REMOTE_USER')

        #######################################################################
        #
        # Constantes 
        #
        #######################################################################

        #######################################################################
	    # Constante K
        #
        # K est utilisee dans la saisie de id_paquet et id_vpz (cf
        # liste_scenarios etc).
        # id_paquet et id_scenario sont recuperes dans id qui vaut 
        # id = id_paquet * K + id_scenario
        #
        # !!! cette facon de recuperer id_paquet et id_scenario est a changer
        #######################################################################
        self.K = 1000

        #######################################################################
	    # Constantes choix_...
        #
        # Une fois que l'utilisateur a choisi le scenario (cf scenario_choisi),
        # differentes operations lui seront proposees dans la suite :
        # - Validations des informations du scenario en cours : voir au
        #   niveau de valider_scenario.
        # - Simulations : voir au niveau de simuler.
        # - Restitution/observation des resultats de simulation (differentes
        #   formes de restitution : affichages, telechargements) : voir au
        #   niveau de restitution_outputs_menu.
        #
        # La gestion/recuperation du choix fait par l'utilisateur parmi les
        # differentes operations qui lui sont proposees s'appuie sur les
        # constantes definies ici, qui sont utilisees de la maniere suivante :
        # - Lorsqu'il s'agira de proposer un certain choix choix_... dans une
        #   page html, il sera transmis a cette page :
        #   c.choix_... = self.choix_...
        # - Gestion des propositions conditionnelles : la valeur ""
        #   est transmise a la page (c.choix_... = "") pour que le choix ne
        #   soit pas propose/affiche (si le choix c.choix_... vaut "" alors 
        #   la page cache le choix en question et sinon le montre).
        #
        #######################################################################

        # transcodage pour signifier que le choix doit etre cache/invisible :
        self.choix_a_cacher = ""

        # transcodage du choix du type de validation :
        self.choix_validationIntermediaire = 'choix_validationIntermediaire'
        self.choix_validationFinale = 'choix_validationFinale'

        # transcodage du choix du type de simulation :
        self.choix_simulationSimple = 9
        self.choix_simulationMultipleModeLineaire = 11
        self.choix_simulationMultipleModeTotal = 12

        # transcodage du choix du type de restitution :
        self.choix_graph = 20 # ensemble des representations graphiques
        self.choix_graphY = 21
        self.choix_graphXY = 22
        self.choix_telechgt = 30 # ensemble des telechargements
        self.choix_telechgtDossier = 31
        self.choix_telechgtDernierFichier = 32
        self.choix_num = 40

        #######################################################################
	    # Constantes erreur_... , liste des noms des erreurs gerees
        #
        # Pour la gestion des erreurs, voir la classe Erreurs.
        #
        # Les erreurs gerees sont :
        #
        # - 'erreur_saisie_blocs ' :
        #   Indique le cas d'erreur de recuperation des valeurs qui ont ete
        #   saisies pour les donnees des blocs.
        #   C'est dans la methode valider_scenario que cette erreur est
        #   eventuellement activee/levee.
        #   C'est dans la methode affichage_scenario que le message de 
        #   signalement de cette erreur est construit pour envoi/affichage en
        #   page scenario.html. A maj dans chaque methode menant a la methode
        #   affichage_scenario.
        #
        # - 'erreur_echec_simulation' :
        #   Indique le cas d'une erreur de simulation.
        #   C'est dans la methode simuler que cette erreur est eventuellement
        #   activee/levee.
        #   C'est dans la methode simulation_menu que le message de 
        #   signalement de cette erreur est construit pour envoi/affichage en
        #   page simulation.html. A maj dans chaque methode menant a la
        #   methode simulation_menu.
        #
        # - 'erreur_aucun_trace_choisi' :
        #   Indique le cas d'erreur ou aucun trace (X,Y) n'a ete selectionne
        #   pour le graphique.
        #   C'est dans la methode graphique que cette erreur est 
        #   eventuellement activee/levee.
        #   C'est dans la methode show_outputs_graph que le message de
        #   signalement de cette erreur est construit pour envoi/affichage en
        #   pages resultats_graph(X)Y.html. A maj dans chaque methode menant
        #   a la methode show_outputs_graph.
        #
        #######################################################################

        self.liste_noms_erreurs = [ 'erreur_saisie_blocs',
                                    'erreur_echec_simulation',
                                    'erreur_aucun_trace_choisi' ] 

    ###########################################################################
    #
    # Choix d'un scenario (dans un paquet)
    #
    ###########################################################################

    ###########################################################################
	# Methode liste_scenarios
	# suite : liste des scenarios dans liste des paquets pour choix du
	# scenario
    ###########################################################################
    def liste_scenarios(self):

        if 'scn' in session : # scn existe deja
            session['scn'].supprimer()
            session.save()

        # paquets en BD
        paqs = sorted(self.paquet_q.all())

        paqs_ok = list()
        paqs_not_ok = list()
        separer( paqs, paqs_ok, paqs_not_ok )

        # informations paquets transmises au formulaire
        c.paqs_ok = [ PaquetInfosFormulaireScenario(paq) for paq in paqs_ok ]
        c.paqs_not_ok = [ PaquetNotOkInfosFormulaireScenario(paq) for paq in paqs_not_ok ]

        c.K = self.K
    
        return render('liste_scenarios.html')

    ###########################################################################
    # Methode controle_mdp
	# Verification du mot de passe saisi pour le paquet selectionne
	# suite : nouvel affichage de la liste des paquets avec cette fois
	# acces au paquet si bon mot de passe 
    ###########################################################################
    def controle_mdp(self, id):

        # Mot de passe
        le_paquet = Paquet.get_by_id(id)
        if not le_paquet:
            abort(404)
        mdp_attendu = le_paquet.password

        params = request.POST
        mdp_saisi = donnees_affichage_saisie.motDePasseSaisi( params )

        # Nouvel affichage (avec ou non acces au paquet selon mot de passe)

        paqs = sorted(self.paquet_q.all())
        paqs_ok = list()
        paqs_not_ok = list()
        separer( paqs, paqs_ok, paqs_not_ok )
        c.paqs_ok = [ PaquetInfosFormulaireScenario(paq) for paq in paqs_ok ]
        c.paqs_not_ok = [ PaquetNotOkInfosFormulaireScenario(paq) for paq in paqs_not_ok ]

        # ajout acces au paquet ou non selon mot de passe saisi
        if mdp_saisi==mdp_attendu :
            # recherche du paquet dans c.paqs_ok :
            i = donnees_affichage_saisie.getIndicePaquetDansListe(le_paquet.uid,c.paqs_ok)
            if i != None : # paquet present dans c.paqs_ok
                paq = c.paqs_ok[ i ]
                paq.visible = True
            else : # paquet non present dans c.paqs_ok
                # recherche du paquet dans c.paqs_not_ok :
                i = donnees_affichage_saisie.getIndicePaquetDansListe(le_paquet.uid,c.paqs_not_ok)
                if i != None : # paquet present dans c.paqs_not_ok
                    paq = c.paqs_not_ok[ i ]
                    paq.visible = True

        c.K = self.K
    
        return render('liste_scenarios.html')

    ###########################################################################
    # Methode scenario_choisi
	# Prise en compte du paquet et du scenario (fichier vpz) choisi
	# suite : affichage scenario choisi
    ###########################################################################
    def scenario_choisi(self, id):

        # id_paquet et id_scenario sont recuperes dans 
        # id valant id_paquet * K + id_scenario
        id_scenario = int(id) % self.K
        id_paquet = (int(id) - id_scenario) / self.K

        paq = Paquet.get_by_id(id_paquet)
        if not paq:
            abort(404)

        # creation/demarrage exploitation,
        # initialisation en fonction du vpz et de son paquet
    
        if 'scn' in session : # scn existe deja
            session['scn'].supprimer()
            session.save()

        session['scn'] = Scn( paq, id_scenario )
        session.save()

        #######################################################################
        #
        # Avec la creation/construction de 'scn', il a ete cree un espace
        # d'exploitation dedie aux differentes operations qui vont par la
        # suite etre effectuees/demandees sur le scenario retenu.
        # On travaille dorenavant dans cet espace d'exploitation, sur
        # scenario_vpz et non plus scenario_vpz_d_origine.
        #
        #######################################################################

        #######################################################################
        # La gestion des erreurs commence a ce niveau
        #######################################################################
        session['erreurs'] = Erreurs( self.liste_noms_erreurs )
        session.save()

        # Initialisation (raz) de 'erreur_saisie_blocs ' avant appel de 
        # affichage_scenario
        session['erreurs'].desactiver('erreur_saisie_blocs')
        session.save()

        redirect_to(controller="scenario", action="affichage_scenario")

    ###########################################################################
    #
    # Visualisation  des informations du scenario
    # et eventuelles modifications
    #
    ###########################################################################

    ###########################################################################
    # Methode affichage_scenario
	# Affichage des informations du scenario choisi
	# (pour visualisation/modification)
    ###########################################################################
    def affichage_scenario(self):

        c.choix_validationIntermediaire = self.choix_validationIntermediaire
        c.choix_validationFinale = self.choix_validationFinale

        c.scenario = session['scn'].scenario_vpz
        c.definition = session['scn'].definition

        # dimension_et_mode permet d'identifier les blocs pour lesquels
        # proposer 'Add all'
        dimension_et_mode = {}
        for bloc in c.definition.blocs :
            uneDataNotVariableEtReadWrite = False
            for data in bloc.liste_datas :
                if data.isVariableDimension() and data.isReadWriteMode() :
                    pass
                else :
                    uneDataNotVariableEtReadWrite = True
            if uneDataNotVariableEtReadWrite == True :
                dimension_et_mode[bloc.infos_generales.titre] = ""
            else :
                dimension_et_mode[bloc.infos_generales.titre] = "variable_et_read_write"
        c.dimension_et_mode = dimension_et_mode

        c.scenario_d_origine = session['scn'].scenario_vpz_d_origine

        # Dans le cas ou retour/arrivee a affichage_scenario en erreur
        # 'erreur_saisie_blocs', constitution du message associe pour
        # envoi/affichage en page scenario.html
        if session['erreurs'].isActive( 'erreur_saisie_blocs' ) :

            texte_erreur = u'Erreur : Les informations du scénario en cours (scénario d\'origine après saisie ou non de modifications) n\'ont pas pu être prises en compte.'
            flash( texte_erreur )

            message_erreur = session['erreurs'].get_message( 'erreur_saisie_blocs' )
            if message_erreur != "" :
                texte_message_erreur = "Message d'erreur : " + message_erreur + "."
                flash( texte_message_erreur.decode() )

            texte_precision = u'Retour à l\'état précédent du scénario en cours. Autrement dit ses informations actuelles ignorent les modifications qui avaient éventuellement été apportées au scénario juste avant la dernière demande de validation (boutons \'Validation intermédiaire\' ou \'Validation finale\'), qui a échoué.'
            flash( texte_precision )

            texte_et_apres = u'Effectuer éventuellement de nouvelles modifications sur les informations du scénario en cours avant de valider (boutons \'Validation intermédiaire\' ou \'Validation finale\').'
            flash( texte_et_apres )

        return render('scenario.html')

    ###########################################################################
    # Methode valider_scenario
    # Met a jour le scenario (sa definition) a partir de la saisie.
	# Si les informations affichees/saisies du scenario ont bien ete 
    # recuperees, alors suite :
    # simulation_menu (si validation finale) ou retour sans erreur a
    # affichage_scenario (si validation intermediaire)
    # Sinon retour en erreur a affichage_scenario
    ###########################################################################
    def valider_scenario(self):

        # validation de la saisie :
        # enregistre dans definition les modifications saisies
        params = request.POST

        # type de validation choisie :
        if donnees_affichage_saisie.existe_dans( self.choix_validationIntermediaire, params ) :
            choix_validation = self.choix_validationIntermediaire
        else : # session['choix_validationFinale'] et de plus par defaut
            choix_validation = self.choix_validationFinale

        # donnees/blocs saisis :

        # Le traitement miseAjourBlocsSaisis est applique sur blocs_copie
        # (plus exactement blocs de definition_copie) copie temporaire,
        # et ne sera repercute sur session['scn'].definition.blocs
        # qu'en cas de bon deroulement. En cas de mauvais deroulement du
        # traitement, session['scn'].definition.blocs reste inchange.
        definition_copie = Definition()
        definition_copie.blocs = copy.deepcopy( session['scn'].definition.blocs )

        try :
            cr_ok = definition_copie.miseAjourBlocsSaisis( params )
            message_erreur = ""
        except Exception, e :
            cr_ok = False
            message_erreur = e.message

        # trace_ecran_contenu_blocs( definition_copie.blocs, "Les blocs constitues a partir des informations saisies : " ) # observation/trace ecran des blocs obtenus/maj

        # 'erreur_saisie_blocs ' va etre maj : activee/levee ou desactivee
        # en fonction du deroulement du traitement
        # (d'autre part dans certains cas, appel ensuite de affichage_scenario)

        if not cr_ok : # mauvais deroulement de miseAjourBlocsSaisis

            print "controller/scenario/simuler ERREUR : probleme de recuperation des informations saisies pour le scenario en cours"

            session['erreurs'].activer( 'erreur_saisie_blocs' )
            session['erreurs'].set_message( 'erreur_saisie_blocs', message_erreur )
            session.save()
            redirect_to(controller="scenario", action="affichage_scenario")

        else : # bon deroulement de miseAjourBlocsSaisis, continuer

            # prise en compte du traitement
            session['scn'].definition.blocs = copy.deepcopy( definition_copie.blocs )
            session['erreurs'].desactiver('erreur_saisie_blocs')
            session.save()

            if choix_validation == self.choix_validationIntermediaire :
                redirect_to(controller="scenario", action="affichage_scenario")

            else : # self.choix_validationFinale et de plus par defaut

                # Initialisation (raz) de 'erreur_echec_simulation ' avant 
                # appel de simulation_menu
                session['erreurs'].desactiver('erreur_echec_simulation')
                session.save()

                redirect_to(controller="scenario", action="simulation_menu" )

    ###########################################################################
    #
    # Simulation du scenario
    #
    ###########################################################################

    ###########################################################################
    # Methode simulation_menu
	# Affichage du menu de choix du type de simulation
    ###########################################################################
    def simulation_menu(self):

        # les 3 choix sont toujours proposes (jamais caches)
        c.choix_simulationSimple = self.choix_simulationSimple
        c.choix_simulationMultipleModeLineaire  = self.choix_simulationMultipleModeLineaire 
        c.choix_simulationMultipleModeTotal = self.choix_simulationMultipleModeTotal

        c.scenario = session['scn'].scenario_vpz
        c.definition = session['scn'].definition
        c.scenario_d_origine = session['scn'].scenario_vpz_d_origine

        # Dans le cas ou retour/arrivee a simulation_menu en erreur
        # 'erreur_echec_simulation', constitution du message associe pour
        # envoi/affichage en page simulation.html
        if session['erreurs'].isActive( 'erreur_echec_simulation' ) :

            texte_erreur = u'Erreur : la simulation a échoué.'
            flash( texte_erreur )

            message_erreur = session['erreurs'].get_message( 'erreur_echec_simulation' )
            if message_erreur != "" :
                texte_message_erreur = "Message d'erreur : " + message_erreur + "."
                flash( texte_message_erreur.decode() )

            texte_precision = u'Pas de \'retour en arrière\' pour le scénario en cours. Autrement dit ses informations actuelles restent dans l\'état dans lequel elles étaient lors de la dernière demande de simulation (bouton \'Simulation simple\' ou \'Simulation multiple\'), qui vient d\'échouer.'
            flash( texte_precision )

            texte_et_apres = u'Vous pouvez tenter une autre simulation (bouton \'Simulation simple\' ou \'Simulation multiple\') ou retourner voir/modifier l\'état qui a conduit à l\'échec de simulation (\'Retour à visualisation/modification du scénario en cours\') ou choisir un nouveau scénario (\'Retour au choix du scénario\').'
            flash( texte_et_apres )

        return render('simulation.html')

    ###########################################################################
    # Methode simuler
    # Execution de la simulation
    # suite : si simulation sans echec, alors menu de restitution des
    # des resultats restitution_outputs_menu, sinon retour en erreur a
    # simulation_menu.
    ###########################################################################
    def simuler(self,id):

        # type de simulation choisie (et mode) :
        choix_simulation = int(id)
        if choix_simulation == self.choix_simulationMultipleModeTotal :
            type_simulation = "multi_simulation"
            mode_combinaison = "mode_total"
        elif choix_simulation == self.choix_simulationMultipleModeLineaire  :
            type_simulation = "multi_simulation"
            mode_combinaison = "mode_lineaire"
        else : # self.choix_simulationSimple et de plus par defaut
            type_simulation = "mono_simulation"
            mode_combinaison = "" # sans objet

        # 'erreur_echec_simulation' va etre maj : activee/levee ou desactivee
        # en fonction du deroulement du traitement
        # (d'autre part dans certains cas, appel ensuite de simulation_menu)

        cas_d_erreur = False # par defaut
        message_erreur = "" # par defaut

        # Initialisations

        # exp correspond a scenario_vpz, avec prise en compte des valeurs de definition
        exp = ExpScenarioVpz( session['scn'].scenario_vpz )

        try :
            exp = session['scn'].definition.prendreEnCompteDans( exp )
        except Exception, e :
            cas_d_erreur = True
            message_erreur = message_erreur + " *** " + e.message

        else : # bon deroulement de prendreEnCompteDans, continuer

            # objet resultat de simulation
            # depend du type de simulation (simple ou multiple)
            session['scn'].resultat = Resultat( type_simulation, mode_combinaison )

            session.save()

	        # Lance la simulation (2 fois) :
	        # - simulation et sauvegarde fichiers en preparation
            #   de la demande de telechargement des resultats 
	        # - simulation dans le but d'en afficher les resultats
	        #   (representations graphiques...)
            try:
                session['scn'].resultat.simuler( exp, session['scn'].espace_exploitation )
            except Exception, e:
                cas_d_erreur = True
                message_erreur = message_erreur + " *** " + e.message

            else : # bon deroulement de simuler, continuer
            # (simulation sans levee d'erreur)
  
                session['erreurs'].desactiver('erreur_echec_simulation')
                session.save()

                redirect_to(controller="scenario", action="restitution_outputs_menu" )

        if cas_d_erreur : # echec de simulation
        # (au niveau de definition.prendreEnCompteDans ou resultat.simuler)

            session['erreurs'].activer( 'erreur_echec_simulation' )
            session['erreurs'].set_message( 'erreur_echec_simulation', message_erreur )
            session.save()

            redirect_to(controller="scenario", action="simulation_menu")


    ###########################################################################
    #
    # Restitution/observation des resultats de la simulation effectuee :
    # differentes formes de restitution (affichages ecran et telechargement).
    #
    # Representations graphiques :
    # - choix_graphY : representation graphique
    #   ou tous les traces ont la meme var en abscisse 
    # - choix_graphXY : representation graphique
    #   ou tous les traces n'ont pas forcement la meme var en abscisse 
    #
    # Telechargements :
    # - choix_telechgtDossier : telechargement du dossier complet
    # - choix_telechgtDernierFichier : telechargement du dernier fichier de
    #   representation graphique conserve
    #
    # Affichage numerique :
    # - choix_num : tableaux de valeurs numeriques (actuellement implemente
    #   dans le cas de simulation simple, pas en simulation multiple)
    #
    ###########################################################################

    ###########################################################################
    # Methode restitution_outputs_menu
	# Affichage du menu de choix de restitution des resultats de simulation
    ###########################################################################
    def restitution_outputs_menu(self):

        c.scenario = session['scn'].scenario_vpz
        c.scenario_d_origine = session['scn'].scenario_vpz_d_origine

        c.choix_graph = self.choix_graph
        c.choix_telechgt = self.choix_telechgt
        type_simulation = session['scn'].resultat.type_simulation
        if type_simulation == "mono_simulation" :
            c.choix_num = self.choix_num
        else : # le choix 'affichage numerique' n'apparait pas dans le menu
            c.choix_num = self.choix_a_cacher
        c.choix_a_cacher = self.choix_a_cacher

        return render('resultats.html')

    ###########################################################################
    #
    # Affichages numeriques
    #
    ###########################################################################

    ###########################################################################
    # Methode show_outputs_num
	# Affichage des resultats de simulation sous forme numerique
	# uniquement dans le cas d'une simulation simple
    ###########################################################################
    def show_outputs_num(self):

        # 'controle' :
        if session['scn'].resultat.type_simulation != "mono_simulation" :
            print "[controllers/show_outputs_num] ERREUR : appel dans cas autre que simulation simple"

        else : # cas de simulation simple

            c.scenario = session['scn'].scenario_vpz
            c.resultat = session['scn'].resultat
            c.scenario_d_origine = session['scn'].scenario_vpz_d_origine

            return render('resultats_num.html')

    ###########################################################################
    #
    # Representations graphiques
    #
    # Traite 2 types de representations graphiques :
    # - choix_graphY :
    #   cas ou tous les traces ont la meme var en abscisse
    #   ce choix existe en simulation simple et en simulation multiple 
    # - choix_graphXY :
    #   cas ou tous les traces n'ont pas forcement la meme var en abscisse
    #   ce choix existe en simulation simple et en simulation multiple
    #
    ###########################################################################

    ###########################################################################
    # Methode show_outputs_graph_menu
	# Affichage des resultats de simulation sous forme de
	# representations graphiques : choix du type de representation graphique
    ###########################################################################
    def show_outputs_graph_menu(self):

        # les 2 choix sont proposes quel que soit le type de simulation 
        # (session['scn'].resultat.type_simulation). (jamais caches)
        c.choix_graphXY = self.choix_graphXY
        c.choix_graphY = self.choix_graphY

        # Initialisation (raz) de 'erreur_aucun_trace_choisi' avant appel de 
        # show_outputs_graph 
        session['erreurs'].desactiver('erreur_aucun_trace_choisi')
        session.save()

        c.scenario = session['scn'].scenario_vpz
        c.resultat = session['scn'].resultat
        c.scenario_d_origine = session['scn'].scenario_vpz_d_origine

        return render('resultats_graph.html')

    ###########################################################################
    # Methode show_outputs_graph
	# Affichage des resultats de simulation sous forme de
	# representation graphique : choix des donnees representees.
    # Le traitement depend du type de simulation (simple, multiple)
    # et du type de representation qui ont ete choisis.
    ###########################################################################
    def show_outputs_graph(self,id):

        type_de_representation = int(id)
        type_simulation = session['scn'].resultat.type_simulation

        c.scenario = session['scn'].scenario_vpz
        c.scenario_d_origine = session['scn'].scenario_vpz_d_origine

        if type_simulation == "multi_simulation" : # cas "multi_simulation" 
            c.type_capture_vars = session['scn'].resultat.res.type_capture_vars
        else : # cas "mono_simulation" et de plus par defaut
            type_capture_vars = session['scn'].resultat.res.type_capture_vars
            c.type_capture_vars = donnees_affichage_saisie.transformationArtificielle( type_capture_vars )

        c.type_simulation = type_simulation 

        #######################################################################
        # Cas simulation simple ("mono_simulation") ou
        # Cas simulation multiple ("multi_simulation")
        #######################################################################

        texte_erreur = u'Erreur : aucun couple (X,Y) à tracer n\'a été choisi.'

        if type_de_representation == self.choix_graphY :
        ###################################################################
        # Type de representation 'choix_graphY'
        ###################################################################

            c.choix_graphY = self.choix_graphY

            # Dans le cas ou retour/arrivee a show_outputs_graph en erreur
            # 'erreur_aucun_trace_choisi', constitution du message associe
            # pour envoi/affichage en page resultats_graphY.html
            if session['erreurs'].isActive( 'erreur_aucun_trace_choisi' ) :

                flash( texte_erreur )

                texte_et_apres = u'Sélectionner au moins un couple (X,Y), c\'est à dire dans le tableau la variable en abscisse et au moins une variable en ordonnée avant de \'Valider les choix\''
                flash( texte_et_apres )

            return render('resultats_graphY.html')

        elif type_de_representation == self.choix_graphXY :
        ###################################################################
        # Type de representation 'choix_graphXY'
        ###################################################################

            c.choix_graphXY = self.choix_graphXY

            # Dans le cas ou retour/arrivee a show_outputs_graph en erreur
            # 'erreur_aucun_trace_choisi', constitution du message associe
            # pour envoi/affichage en pages resultats_graphXY.html
            if session['erreurs'].isActive( 'erreur_aucun_trace_choisi' ) :

                flash( texte_erreur )

                texte_et_apres = u'Sélectionner au moins un couple (X,Y) : après l\'avoir sélectionné dans le tableau (c\'est à dire sa variable en abscisse et sa variable en ordonnée), ne pas oublier de l\'\'Ajouter au tracé\' avant de \'Valider les choix\''
                flash( texte_et_apres )

            return render('resultats_graphXY.html')

    ###########################################################################
    # Methode graphique
	# Prend en compte les donnees a representer choisies.
    # Le traitement depend du type de simulation (simple, multiple)
    # et du type de representation.
    # S'il a ete choisi au moins un trace (X,Y), alors suite :
	# representation graphique des donnees choisies, sinon 
	# retour en erreur a show_outputs_graph (choix des donnees representees)
    ###########################################################################
    def graphique(self,id) :

        type_de_representation = int(id)
        type_simulation = session['scn'].resultat.type_simulation

        params = request.POST

        titre_du_trace = donnees_affichage_saisie.get_valeur( "titre", params )

        ###################################################################
        # liste_XY : liste des couples (X,Y) a tracer
        # le decodage de params depend du type de simulation
        # et du type de representation
        ###################################################################
        liste_XY = list()
        cr_ok = False # par defaut
        if type_de_representation == self.choix_graphY :
            cr_ok = donnees_affichage_saisie.definir_Y( liste_XY, params, type_simulation )
        elif type_de_representation == self.choix_graphXY :
            cr_ok = donnees_affichage_saisie.definir_XY( liste_XY, params, type_simulation )

        # 'erreur_aucun_trace_choisi' va etre maj : activee/levee ou 
        # desactivee en fonction du deroulement du traitement
        # (d'autre part dans certains cas, appel ensuite de show_outputs_graph)

        if not cr_ok : # mauvais deroulement

            session['erreurs'].activer( 'erreur_aucun_trace_choisi' )
            session.save()
            redirect_to(controller="scenario", action="show_outputs_graph", id=id)

        else : # bon deroulement

            session['erreurs'].desactiver('erreur_aucun_trace_choisi')
            session.save()

            # indicateur pour la construction des legendes
            if type_de_representation == self.choix_graphY :
                memeAbscisse = True
            elif type_de_representation == self.choix_graphXY :
                memeAbscisse = False

            resultat = session['scn'].resultat
            resultat.res.setRepresentationGraphique( liste_XY, titre_du_trace, memeAbscisse )

            session.save()

            c.scenario = session['scn'].scenario_vpz
            c.resultat = session['scn'].resultat
            c.scenario_d_origine = session['scn'].scenario_vpz_d_origine

	        # liste des fichiers existants sous rep_restitutions
            c.liste_fichiers = session['scn'].espace_exploitation.getListeFichiersRepRestitutions()

            return render('graphique.html')

    ###########################################################################
    # Methode graph
    # Representation graphique une fois choisies les donnees a representer
    ###########################################################################
    def graph(self) :

        # set the response type to PNG, since we at least hope to return a PNG image here
        response.headers['Content-type'] = 'image/png'
    
        # imageThreadLock to prevent threads from writing over each other's graphics
        # we don't want different threads to write on each other's canvases,
        # so make sure we have a new one
        global imageThreadLock
        imageThreadLock.acquire() # lock graphics

        resultat = session['scn'].resultat
    
        buffer = resultat.afficherRepresentationGraphique()

        imageThreadLock.release() # unlock graphics

        return buffer.getvalue()

    ###########################################################################
    # Methode conserver_graph
    # enregistrement du fichier de la derniere representation graphique 
	# suite : retour a la derniere representation graphique
    ###########################################################################
    def conserver_graph(self) :

        params = request.POST
        nom = donnees_affichage_saisie.get_valeur( "nomfichier", params )

        repertoire = session['scn'].espace_exploitation.preparerRepertoireExploitationRestitutions()
        nom_fichier_absolu = repertoire +'/'+ nom
        session['scn'].resultat.enregistrerRepresentationGraphique( nom_fichier_absolu )

        session['scn'].resultat.memoriserNomDernierFichierRepresentationGraphique( nom )
        session.save()

        flash(u'Fichier "%s" conservé.' % nom )

	    # retour a la representation graphique des dernieres donnees choisies
        c.scenario = session['scn'].scenario_vpz
        c.resultat = session['scn'].resultat
        c.scenario_d_origine = session['scn'].scenario_vpz_d_origine

	    # liste des fichiers existants sous rep_restitutions
        c.liste_fichiers = session['scn'].espace_exploitation.getListeFichiersRepRestitutions()

        return render('graphique.html')

    ###########################################################################
    #
    # Telechargements
    #
    # Traite 2 types de telechargements :
    # - choix_telechgtDossier : telechargement du dossier complet
    # - choix_telechgtDernierFichier : telechargement uniquement du dernier
    #   fichier de representation graphique conserve
    #
    ###########################################################################

    ###########################################################################
    # Methode telechargements_menu
	# Affichage du menu de choix du type de telechargement
    ###########################################################################
    def telechargements_menu(self):

        c.choix_telechgtDossier = self.choix_telechgtDossier
        c.choix_telechgtDernierFichier = self.choix_telechgtDernierFichier # par defaut

        # nom du dernier fichier de conservation d'une representation graphique
        # S'il n'existe pas (valeur None), alors il ne faut pas proposer le
        # telechargement correspondant)
        nom = session['scn'].resultat.getNomDernierFichierRepresentationGraphique()
        if nom == None :
            nom = ""
            c.choix_telechgtDernierFichier = self.choix_a_cacher
        c.choix_a_cacher = self.choix_a_cacher
        c.nom_dernier_fichier_rg = nom

        c.scenario = session['scn'].scenario_vpz
        c.resultat = session['scn'].resultat
        c.scenario_d_origine = session['scn'].scenario_vpz_d_origine

        return render('telechargements.html')

    ###########################################################################
    # Methode telecharger
	# Execution du telechargement de resultats
    ###########################################################################
    def telecharger(self, id):

        type_de_telechargement = int(id)

        if type_de_telechargement == self.choix_telechgtDossier :
            buffer = session['scn'].espace_exploitation.produireBufferTelechargementDossier_zip()
            response.content_type = 'application/zip'
            response.headers['Content-Disposition'] = str( 'attachment; filename=' + session['scn'].espace_exploitation.getNomDossier_zip() )
            return buffer.getvalue()

        elif type_de_telechargement == self.choix_telechgtDernierFichier :
            nom_fichier = session['scn'].resultat.getNomDernierFichierRepresentationGraphique()
            buffer = session['scn'].espace_exploitation.produireBufferTelechargementDernierFichier_zip( nom_fichier )
            response.content_type = 'application/zip'
            response.headers['Content-Disposition'] = str( 'attachment; filename=' + session['scn'].espace_exploitation.getNomDernierFichier_zip() )
            return buffer.getvalue()


###############################################################################
#
# Outillage de developpement/debug
#
###############################################################################

###############################################################################
# Observation/trace ecran du contenu des blocs
###############################################################################
def trace_ecran_contenu_blocs( blocs, titre_affichage ) :
    print ""
    print titre_affichage
    for bloc in blocs :
        print ""
        print "Nom bloc : ", bloc.infos_generales.titre
        #for ( dataname, datavalues ) in bloc.liste_datas  :
            #print "dataname : ", dataname, " datavalues : ", datavalues
    print ""

