#-*- coding:utf-8 -*-

###############################################################################
# File paquets.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from pylons import request, response, session, tmpl_context as c
from pylons.controllers.util import abort, redirect_to

from maquette.lib.helpers import flash
from maquette.lib.base import BaseController, render

from authkit.authorize.pylons_adaptors import authorize
from maquette.lib import auth

from maquette.model import Paquet
from maquette.model.meta import Session
from maquette.model.paquet_notok import separer
from maquette.model import espace_exploitation

import logging
log = logging.getLogger(__name__)

###############################################################################
###############################################################################

class PaquetsController(BaseController):
    
    @authorize( auth.is_valid_user )
    def __before__(self):
        self.paquet_q = Paquet.get_table()
        self.username = request.environ.get('REMOTE_USER')

    def liste_paquets(self):
    ###########################################################################
    # Affichage des paquets issus de la BD, en
    # distinguant les paquets sans probleme detecte
    # et ceux presentant un probleme (par exemple non trouves sous pkgs)
    ###########################################################################

        # liste des paquets en BD
        paqs = sorted(self.paquet_q.all())

        paqs_ok = list()
        paqs_not_ok = list()
        separer( paqs, paqs_ok, paqs_not_ok )

        c.paqs_ok = paqs_ok
        c.paqs_not_ok = paqs_not_ok

        return render('liste_paquets.html')

    def show(self, id):
    ###########################################################################
    # Affichage du paquet selectionne (paquet d'ID id en BD)
    # pour visualisation/modification
    ###########################################################################

        paq = Paquet.get_by_id(id)
        if not paq:
            abort(404)

        c.paq = paq

        # liste des paquets/projets utiles existants sous pkgs
        c.liste_paquets = espace_exploitation.getListeUtilePkgs()

        return render('paquet.html')

    def update(self, id):
    ###########################################################################
    # Prise en compte en BD de la modification saisie pour le paquet 
    # (paquet d'ID id en BD), a condition qu'il
    # n'y ait pas en BD d'autre paquet avec ce nom (nom saisi pour paquet).
    # Dans cas contraire, retour au menu de modification
    ###########################################################################

        paq = Paquet.get_by_id(id)
        if not paq:
            abort(404)
        paq.set(**request.POST)

        # paquets_concurrents_en_bd
        paqs = sorted(self.paquet_q.all())
        paquets_concurrents_en_bd = [ p for p in paqs if p.nom==paq.nom and p.uid!=paq.uid ]

        if len(paquets_concurrents_en_bd) == 0 :

            # Pas d'autre paquet en BD avec ce nom
            # Prise en compte en BD de la modification

            Session.add(paq)
            Session.commit()

            flash(u'Paquet "%s" sauvegardé.' % paq.nom)
            redirect_to(controller="paquets", action="liste_paquets")

        else :

            # Deja un paquet en BD avec ce nom
            # Non prise en compte en BD de la modification
            # et retour a la saisie de la modification

            flash(u'Le paquet ne peut pas être sauvegardé en l\'état car il existe déjà dans la base un paquet nommé "%s".' % paq.nom)
            flash(u'Changer le nom du paquet pour le sauvegarder.')

            c.paq = paq

            # liste des paquets/projets utiles existants sous pkgs
            c.liste_paquets = espace_exploitation.getListeUtilePkgs()

            return render('paquet.html')
        
    def new(self):
    ###########################################################################
    # Affichage du menu de creation d'un paquet en BD
    ###########################################################################

        # valeurs par defaut
        c.paq_nom = ""
        c.paq_description = ""
        c.paq_nom_pkg = ""
        c.paq_nom_responsable = ""
        c.paq_mail_responsable = "@."
        c.paq_nom_resp_scientifique = ""
        c.paq_nom_resp_informatique = ""
        c.paq_version_vle = ""
        c.paq_cr_reception = ""
        c.paq_password = ""

	    # liste des paquets/projets utiles existants sous pkgs
        c.liste_paquets = espace_exploitation.getListeUtilePkgs()

        return render('new_paquet.html')

    def create(self):
    ###########################################################################
    # Creation en BD du paquet saisi a condition qu'il
    # n'y ait pas encore en BD de paquet avec ce nom (nom saisi pour paquet).
    # Dans cas contraire, retour au menu de creation
    ###########################################################################

        paq = Paquet(**request.POST)
        paq.owner = self.username # !!!  a voir

        # paquets_concurrents_en_bd
        paqs = sorted(self.paquet_q.all())
        paquets_concurrents_en_bd = [ p for p in paqs if p.nom==paq.nom and p.uid!=paq.uid ]

        if len(paquets_concurrents_en_bd) == 0 :

            # Pas d'autre paquet en BD avec ce nom
            # Creation en BD

            Session.add(paq)
            Session.commit()

            flash(u'Paquet "%s" créé.' % paq.nom)
            redirect_to(controller="paquets", action="liste_paquets")

        else :

            # Deja un paquet en BD avec ce nom
            # Non creation en BD
            # et retour a la saisie (modification et non pas creation)

            flash(u'Le paquet ne peut pas être créé en l\'état car il existe déjà dans la base un paquet nommé "%s".' % paq.nom)
            flash(u'Changer le nom du paquet pour le créer.')

            c.paq_nom = paq.nom
            c.paq_description = paq.description
            c.paq_nom_pkg = paq.nom_pkg
            c.paq_nom_responsable = paq.nom_responsable
            c.paq_mail_responsable = paq.mail_responsable
            c.paq_nom_resp_scientifique = paq.nom_resp_scientifique
            c.paq_nom_resp_informatique = paq.nom_resp_informatique
            c.paq_version_vle = paq.version_vle
            c.paq_cr_reception = paq.cr_reception
            c.paq_password = paq.password

            # liste des paquets/projets utiles existants sous pkgs
            c.liste_paquets = espace_exploitation.getListeUtilePkgs()

            return render('new_paquet.html')

    def delete(self, id):
    ###########################################################################
    # Suppression en BD du paquet d'ID id en BD)
    ###########################################################################

        paq = Paquet.get_by_id(id)
        if not paq:
            abort(404)

        Session.delete(paq)
        Session.commit()

        flash(u'Paquet "%s" supprimé.' % paq.nom)
        redirect_to(controller="paquets", action="liste_paquets")

