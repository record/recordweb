/*
 * File infobulle.js
 *
 * Authors : Nathalie Rousse, RECORD platform team member, INRA.
 *
 *-----------------------------------------------------------------------------
 *
 * recordweb - RECORD platform Web Development
 *
 * Copyright (C) 2011-2012 INRA http://www.inra.fr
 *
 * This file is part of recordweb.
 *
 * recordweb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * recordweb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-----------------------------------------------------------------------------
 */

/*****************************************************************************
 * infobulle : { infobulle.css, infobulle.js, jquery.mon_infobulle.js }
 *
 * Affichage d'un texte infobulle quand la souris passe sur l'element auquel
 * il correspond.
 *
 *****************************************************************************/

/*
 * Au passage de la souris sur un element de classe 'avec_bulle', affichage
 * du texte informatif 'texte_bulle' qui est propre a cet element.
 *
 * Exemple :
 * <td texte_bulle="...texte informatif..." class="avec_bulle">..texte..</td>
 *
 * Valable pour les elements declares ici : <p>, <div>, <td> ...
 */
function infobulle()
{
    var options = {

        /* offset relatif a position souris */
        offsetX: 30, offsetY: 5,

        /* offset relatif a origine ecran
        offsetX: 10, offsetY: 84, */

        infobullecss: "infobulle"
    };
    $("p.avec_bulle").mon_infobulle(options);
    $("div.avec_bulle").mon_infobulle(options);
    $("th.avec_bulle").mon_infobulle(options);
    $("tr.avec_bulle").mon_infobulle(options);
    $("td.avec_bulle").mon_infobulle(options);
    $("h3.avec_bulle").mon_infobulle(options);
}

