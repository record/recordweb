#-*- coding:utf-8 -*-

## @file record/utils/erreur.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File erreur.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

import traceback

##..
#*****************************************************************************\n
# Erreur
#
# Erreur survenue en cours de traitement (pour signalisation...)
#
#*****************************************************************************
class Erreur(object):

    ## Construction :
    def __init__( self, nom ):
        self.nom = nom
        self.etat = False # etat erreur
        self.message = "" # message erreur 

    ## initialisation (valeur d'initialisation)
    def initialiser( self ) :
        self.etat = False
        self.message = ""

    ## raz du message d'erreur (etat inchange)
    def raz_message( self ) :
        self.set_message( "" )

    ## affectation (valeur texte) du message d'erreur (etat inchange)
    def set_message( self, texte ) :
        self.message = texte

    ## retourne le message d'erreur
    def get_message( self ) :
        return self.message

    ## activation etat (valeur True) (message inchange)
    def activer( self ) :
        self.etat = True

    ## desactivation etat (valeur False), de plus raz message 
    def desactiver( self ) :
        self.etat = False
        self.raz_message()

    ## indique etat
    def isActive( self ) :
        return self.etat

def get_message_exception(e) :
    # type(e).__name__ , e.message 
    return traceback.format_exc()

