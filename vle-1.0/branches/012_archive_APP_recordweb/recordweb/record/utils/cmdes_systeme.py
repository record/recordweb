#-*- coding:utf-8 -*-

## @file record/utils/cmdes_systeme.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File cmdes_systeme.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

import os

try:
    from configs.conf_trace import CONF_trace
except ImportError:
    from record.utils.configs.conf_trace import CONF_trace

from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

#*****************************************************************************
#
# Methodes concernant commandes systeme
#
#*****************************************************************************

def executerCommandeSysteme( commande ) :
 
    t_ecr.trait()
    t_ecr.message( "executerCommandeSysteme : " + str(commande) )
    os.system( commande ) # run commands in the shell (blocking)

#*****************************************************************************

