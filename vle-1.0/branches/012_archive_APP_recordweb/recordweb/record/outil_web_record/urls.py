
## @file record/outil_web_record/urls.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File urls.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('record.outil_web_record.scn_selection.views',

    # 1) scn_selection
    url(r'^', include('record.outil_web_record.scn_selection.urls')),
    url(r'^owrec/usr/', include('record.outil_web_record.scn_selection.urls')),

    # 2) scn_edition
    url(r'^', include('record.outil_web_record.scn_edition.urls')),
    url(r'^owrec/usr/', include('record.outil_web_record.scn_edition.urls')),

    # 3) scn_simulation
    url(r'^', include('record.outil_web_record.scn_simulation.urls')),
    url(r'^owrec/usr/', include('record.outil_web_record.scn_simulation.urls')),

    # 4) res_telechargement, res_numerique, res_graphique

    url(r'^', include('record.outil_web_record.res_telechargement.urls')),
    url(r'^owrec/usr/', include('record.outil_web_record.res_telechargement.urls')),

    url(r'^', include('record.outil_web_record.res_numerique.urls')),
    url(r'^owrec/usr/', include('record.outil_web_record.res_numerique.urls')),

    url(r'^', include('record.outil_web_record.res_graphique.urls')),
    url(r'^owrec/usr/', include('record.outil_web_record.res_graphique.urls')),
)

