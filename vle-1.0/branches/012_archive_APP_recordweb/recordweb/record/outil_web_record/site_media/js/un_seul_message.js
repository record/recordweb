/*
 * File un_seul_message.js
 *
 * Authors : Nathalie Rousse, RECORD platform team member, INRA.
 *
 *-----------------------------------------------------------------------------
 *
 * recordweb - RECORD platform Web Development
 *
 * Copyright (C) 2011-2012 INRA http://www.inra.fr
 *
 * This file is part of recordweb.
 *
 * recordweb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * recordweb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-----------------------------------------------------------------------------
 */

/*****************************************************************************
 * affichage d'un seul message parmi une famille de messages :
 * { un_seul_message.js }
 *
 * La famille de messages comprend un certain nombre de messages qui sont
 * chacun inclus dans un 'div' de class 'famille_message' : ces messages 
 * vont tous etre supprimes.
 * La famille de messages comprend un message de 'span' de class
 * 'famille_message' (qui lui n'est pas inclus dans un 'div' de class
 * 'famille_message' ! ): il s'agit du seul message qui doit rester, dans
 * lequel il sera affiche 'texte'.
 *
 *****************************************************************************/

/*
 * Methode un_seul_message :
 *
 * La famille des messages est de la forme :
 *
 * - une serie de messages qui vont etre supprimes :
 *
 *   <div class=famille_message> <span class="...">bla bla bla</span> </div>
 *
 * - le seul message qui va rester :
 *
 *   <div class=" ... autre que famille_message !!! ... ">
 *       <span class=famille_message></span>
 *   </div>
 *
 */
function un_seul_message( famille_message, texte )
{
    var messages_a_effacer = "div." + famille_message;
    $(messages_a_effacer).remove();
    var le_seul_message = "span." +  famille_message;
    $(le_seul_message).text( texte );
}

