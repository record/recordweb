/*
 * File liste_pour_choix.js
 *
 * Authors : Nathalie Rousse, RECORD platform team member, INRA.
 *
 *-----------------------------------------------------------------------------
 *
 * recordweb - RECORD platform Web Development
 *
 * Copyright (C) 2011-2012 INRA http://www.inra.fr
 *
 * This file is part of recordweb.
 *
 * recordweb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * recordweb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-----------------------------------------------------------------------------
 */

/*****************************************************************************
 * liste_pour_choix.js
 *
 * Proposition/affichage d'une liste en support a la saisie d'un champ
 * (choix de sa valeur dans la liste).
 *
 *****************************************************************************/

/*
 * A la selection d'une valeur dans 'liste', affichage de cette valeur dans
 * 'champ'.
 */
function onSelectChange( liste, champ){

    var selected = liste+" option:selected";
    var valeur = $(selected).val();
    $(champ).val(valeur);
}

