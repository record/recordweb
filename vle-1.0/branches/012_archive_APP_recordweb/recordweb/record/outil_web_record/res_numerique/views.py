#-*- coding:utf-8 -*-

## @file record/outil_web_record/res_numerique/views.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File views.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
# View res_numerique
#
# Restitution/observation des resultats de la simulation effectuee d'un
# scenario. Parmi les differentes formes de restitution existantes, il s'agit 
# ici de restitution sous forme d'affichage numerique (tableaux de valeurs
# numeriques) : restitution actuellement implementee dans le cas de
# simulation simple (pas en simulation multiple).
#
# Fait partie du processus d'exploitation des scenarios par les utilisateurs,
# selon lequel il s'agit de choisir un scenario (d'un modele record), puis de
# le simuler apres en avoir visualise et eventuellement modifie la configuration
# (parametrage), et enfin d'exploiter sous diverses formes les resultats de
# la simulation effectuee (les visualiser a l'ecran, telecharger...).
#
#*****************************************************************************

from django.shortcuts import render_to_response

from record.outil_web_record.models.affichage_saisie.appel_page import NomsPages, contexte_restitution_menu, contexte_affichage_numerique
from record.outil_web_record.models.erreurs_gerees.erreurs_gerees import ErreursGerees as ERR # noms des erreurs

from record.outil_web_record.utils.commun_views import initialiserControleur

from record.utils import session
from record.utils.erreur import Erreur # gestion des erreurs
from configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

from django.utils.translation import ugettext as _

#*****************************************************************************
#
# AFFICHAGE NUMERIQUE DE RESULTAT DE SIMULATION DU SCENARIO 
#
#*****************************************************************************

##..
#*****************************************************************************\n
#
# Une simulation du scenario choisi a ete effectuee (scn.resultat existe). \n
# Dans le cas d'une simulation simple, preparation de l'affichage des
# resultats de simulation sous forme numerique et affichage numerique.
# Autrement, affichage du menu de restitution avec erreur.
#
#*****************************************************************************
def affichage_numerique(request):
    t_ecr.trait()
    t_ecr.message("(affichage_numerique:) appel de initialiserControleur")
    initialiserControleur(request)

    scn = session.get(request, 'scn')

    if scn.resultat.type_simulation == "mono_simulation" : # cas de simulation simple

        #*********************************************************************
        # Preparation du contexte d'affichage numerique
        #*********************************************************************

        scn = session.get(request, 'scn')
        le_modele_record = session.get(request, 'le_modele_record')
        flashes = [] # pas de flashes
        c = contexte_affichage_numerique(request, flashes, scn, le_modele_record)

        return render_to_response( NomsPages.NOM_page_affichage_numerique, c)

    # Cas d'erreur (ne devrait pas arriver)
    else : # affichage du menu de restitution avec erreur

        # creation erreur_affichage_numerique
        erreur_affichage_numerique = Erreur( ERR.NOM_erreur_affichage_numerique ) # desactivee par defaut
        erreur_affichage_numerique.activer()
        message_erreur = _(u"appel affichage numérique dans cas autre que simulation simple")
        erreur_affichage_numerique.set_message( message_erreur )

        #*********************************************************************
        # Preparation du contexte d'affichage du menu de restitution
        #*********************************************************************

        scn = session.get(request, 'scn')
        le_modele_record = session.get(request, 'le_modele_record')

        # Liste messages d'erreur en flashes
        flashes = [] # par defaut
        if erreur_affichage_numerique.isActive() : # maj flashes selon erreur_affichage_numerique
            texte_erreur = _(u"Erreur : l\'affichage numérique a échoué") + "."
            texte_message_erreur = _(u"Message d'erreur") + " : " + erreur_affichage_numerique.get_message() + "."
            flashes.append( texte_erreur )
            flashes.append( texte_message_erreur )

        c = contexte_restitution_menu(request, flashes, scn, le_modele_record)

        return render_to_response( NomsPages.NOM_page_restitution_menu, c)

#*****************************************************************************

