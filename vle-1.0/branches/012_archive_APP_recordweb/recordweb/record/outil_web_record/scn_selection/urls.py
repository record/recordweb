
## @file record/outil_web_record/scn_selection/urls.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File urls.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('record.outil_web_record.scn_selection.views',

    url(r'^liste_scenarios/(?P<modele_record_id>\d+)/$', 'liste_scenarios'),
    url(r'^scenario_selection/$', 'scenario_selection'),


    #url(r'^$', 'liste_scenarios'),
    #url(r'^liste_scenarios/$', 'liste_scenarios'),
    #
    #url(r'^(?P<id_operation>\d+)/scenario_selection/$', 'scenario_selection'),
    #
    #url(r'^(?P<index_modele_record_on>\d+)/controle_mdp/$', 'controle_mdp'),
    #
    #url(r'^(?P<index_modele_record_on>\d+)/(?P<index_nom_scenario>\d+)/scenario_initialisation/$', 'scenario_initialisation'), # local
)

