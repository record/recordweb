#-*- coding:utf-8 -*-

## @file record/outil_web_record/scn_selection/views.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File views.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# View scn_selection
#
# Choix/selection d'un scenario de simulation (fichier vpz) parmi ceux du
# modele record considere.
#
# Fait partie du processus d'exploitation des scenarios par les utilisateurs,
# selon lequel il s'agit de choisir un scenario de simulation, puis de le
# simuler apres en avoir visualise et eventuellement modifie la configuration
# (parametrage), et enfin d'exploiter sous diverses formes les resultats de
# la simulation effectuee (les visualiser a l'ecran, telecharger...).
#
#*****************************************************************************


from django.shortcuts import render_to_response

from record.bd_modeles_record.models import ModeleRecord as ModeleRecordBd
from record.bd_modeles_record.forms.user_forms import MotDePasseAskingForm
from record.bd_modeles_record.forms.user_forms import ModeleRecordFormComplet

from record.outil_web_record.models.modele_record.modele_record import ModeleRecordOn
from record.outil_web_record.models.espace_exploitation.espace_exploitation import EspaceExploitation

from record.outil_web_record.models.affichage_saisie.appel_page import NomsPages
from record.outil_web_record.models.affichage_saisie.appel_page import contexte_modele_record_mdp
from record.outil_web_record.models.affichage_saisie.appel_page import contexte_liste_scenarios

from record.outil_web_record.utils.commun_views import initialiserControleur

from record.utils import session
from configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

from django.utils.translation import ugettext as _

#*****************************************************************************
#
# CHOIX/SELECTION SCENARIO
#
#*****************************************************************************

##..
#*****************************************************************************\n
# liste_scenarios
#
# Point d'entree de l'outil_web_record : initialisations, nettoyage, 
# preparation et affichage page du choix d'un scenario de simulation (parmi
# ceux du modele record)
#
# - Si modele record prive non deverrouille (mot de passe pas encore saisi) : 
# avant de montrer la liste des scenarios du modele, il est demande a l'usr 
# de saisir le mot de passe du modele.
# - Si modele public ou modele prive deverrouille (mot de passe deja saisi) :
# il est affiche la liste des scenarios du modele dans laquelle l'usr peut
# selectionner celui de son choix.
#
#*****************************************************************************
def liste_scenarios(request, modele_record_id):

    t_ecr.trait()
    t_ecr.message("(liste_scenarios:) appel de initialiserControleur")
    initialiserControleur(request)

    #*************************************************************************
    # les informations en entree
    #*************************************************************************

    modele_record_id = int(modele_record_id)
    modele_record_bd = ModeleRecordBd.objects.get(id=modele_record_id)

    formulaire_mot_de_passe = MotDePasseAskingForm(request.POST)

    #*************************************************************************
    # determination de l'etat deverrouillage (avec maj de flashes)
    #*************************************************************************

    # flashes, initialisation
    flashes = [] # aucun message d'erreur en flashes par defaut

    deverrouillage = False # par defaut
    if modele_record_bd.isPublic() :
        deverrouillage = True # modele public
    else : # modele prive
        if formulaire_mot_de_passe.is_valid(): # verifier mot de passe saisi
            if modele_record_bd.checkPassword( formulaire_mot_de_passe.get_valeur_saisie() ) :
                deverrouillage = True # bon mot de passe
            else : # mauvais mot de passe, le redemander
                deverrouillage = False
                flashes.append( _(u"mauvaise saisie") +", " + _(u"nouvel essai") )

    #*************************************************************************
    # traitement (en fonction de deverrouillage)
    #*************************************************************************

    if not deverrouillage : # demande du mot de passe
        c = contexte_modele_record_mdp(request, flashes, modele_record_id )
        return render_to_response( NomsPages.NOM_page_modele_record_mdp, c )

    else : # deverrouillage

        #*********************************************************************
        # Nettoyage
        #*********************************************************************

        # nettoyage relatif aux repertoires d'exploitation devenus obsoletes
        # (suppression de repertoires VLE_HOME propres aux sessions/connexions)
        EspaceExploitation.nettoyage()

        # session : suppression de l'eventuel scn precedent
        if session.existe(request, 'scn') :
            session.get(request, 'scn').supprimer() # supprime rep_exploitation, vide nom_repertoire_lot_pkgs
        session.suppression_prealable(request, 'scn')

        #*********************************************************************
        # Initialisations
        #*********************************************************************

        flashes = [] # aucun message d'erreur en flashes par defaut

        # formulaire_modele_record (rappel des infos completes a titre d'information/memo)
        formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

        # le_modele_record
        le_modele_record = None # par defaut
        le_modele_record = ModeleRecordOn( modele_record_bd ) # tentative
        if not le_modele_record.estExploitable() : # echec
            msg_off = le_modele_record.exploitabilite.getMsgEtat()
            msg = _(u"Le modèle est considéré indisponible") + ", " + _(u"problème") + " : " + msg_off
            flashes.append( msg )
            le_modele_record = None # par defaut

        #*********************************************************************
        # Mise en session
        #*********************************************************************

        session.suppression_prealable(request, 'le_modele_record' ) # prealable
        if le_modele_record is not None :
            session.set(request, 'le_modele_record', le_modele_record)
            session.sauver(request)

        #*********************************************************************
        # Preparation du contexte d'affichage page du choix d'un scenario
        #*********************************************************************

        c = contexte_liste_scenarios(request, flashes, le_modele_record, formulaire_modele_record )
        return render_to_response( NomsPages.NOM_page_liste_scenarios, c )


##..
#*****************************************************************************\n
# scenario_selection
#
# La page du choix d'un scenario du modele record a deja ete affichee une 1ere
# fois (cf liste_scenarios). Les initialisations ont deja ete effectuees :
# le_modele_record est  enregistre en session ; s'il s'agit d'un modele prive,
# il a deja ete deverrouille (mot de passe saisi). Pas de reinitialisation.
#
# Preparation et affichage de la page du choix d'un scenario du modele record
#
#*****************************************************************************
def scenario_selection(request) :

    t_ecr.trait()
    t_ecr.message("(scenario_selection:) appel de initialiserControleur")
    initialiserControleur(request)

    #*************************************************************************
    # les informations en entree
    #*************************************************************************

    id_operation = None # par defaut
    if "id_operation" in request.POST.keys() :
        id_operation = request.POST["id_operation"]
    id_operation = int(id_operation)

    #*************************************************************************
    # Nettoyage
    #*************************************************************************

    # suppression de l'eventuel scn precedent
    if session.existe(request, 'scn') :
        session.get(request, 'scn').supprimer() # pour aussi supprimer repertoire d'exploitation
    session.suppression_prealable(request, 'scn')

    #*************************************************************************
    # Initialisations
    #*************************************************************************

    # aucun message d'erreur en flashes
    flashes = []

    # le_modele_record
    le_modele_record = session.get(request, 'le_modele_record')

    # formulaire_modele_record (rappel des infos completes a titre d'information/memo)
    modele_record_bd = ModeleRecordBd.objects.get(id=le_modele_record.id)
    formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

    #*************************************************************************
    # Preparation du contexte d'affichage page du choix d'un scenario
    #*************************************************************************

    c = contexte_liste_scenarios(request, flashes, le_modele_record, formulaire_modele_record )
    return render_to_response( NomsPages.NOM_page_liste_scenarios, c )

#*****************************************************************************

