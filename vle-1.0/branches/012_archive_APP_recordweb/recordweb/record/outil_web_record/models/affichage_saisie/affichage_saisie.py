#-*- coding:utf-8 -*-

## @file record/outil_web_record/models/affichage_saisie/affichage_saisie.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File affichage_saisie.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

import string

import pickle
from ast import literal_eval

from record.outil_web_record.models.affichage_saisie.mot_code import MotCode 

#from configs.conf_trace import CONF_trace
#from record.utils.trace import TraceEcran, TraceErreur

#*****************************************************************************
#
# Classes et methodes dediees a gestion d'affichage et saisie de donnees
#
#*****************************************************************************

#*****************************************************************************
#
# Classes pour gestion de l'affichage et la saisie de donnees :
#
# - donnees d'un scenario,
# - donnees d'un paquet,
# - donnees d'une representation graphique
# ...
#
#*****************************************************************************

#*****************************************************************************
#*****************************************************************************
#
# TRAITEMENTS UTILISES DANS/POUR DEFINITION D'UN SCENARIO
#
#*****************************************************************************
#*****************************************************************************

#
# DonneeDef : dans definition.py
#

##..
#*****************************************************************************\n
# Classe DonneeSaisie
#
# Donnee correspondant a la saisie d'une DonneeDef
#
# name : name est le nom de la DonneeSaisie, correspondant a name dans
# la page de saisie. name contient differents champs d'identification et
# caracteristiques de la donnee (indbloc, inddata),
# name se presente sous la forme : "xxx=aaa/yyy=bbb/zzz=ccc". \n
# value : valeur de la DonneeSaisie (plus exactement [v1,v2,...]), value n'est
# pas transformee dans/par les traitements de la classe DonneeSaisie.
#
#*****************************************************************************
class DonneeSaisie(object):

    def __init__(self, name, value) :
        self.name = MotCode(name)
        self.value = value

    ##..
    #*************************************************************************\n
    # Methode decouper, decoupage de (name,value) :
    #
    # name etant de la forme "indbloc=xxx/inddata=zzz", retourne
    # le dict { 'indbloc':xxx, 'inddata':zzz, 'valeurs':value }
    #
    # Plus exactement : \n
    # - dans 'valeurs', value d'origine n'est pas transforme (type inchange)
    # - dans 'indbloc' et 'inddata', indbloc et inddata (type str) ont ete
    #   convertis en int (leur type reel).
    #
    # Remarque : dans le dict retourne
    # { 'indbloc':xxx, 'inddata':zzz, 'valeurs':value },
    # le fait que value vaille None revele un probleme/erreur.
    #
    # Remarque : avant d'appeler decouper, s'assurer du bon format de
    # name en appelant formatBlocVarOk.
    #
    #*************************************************************************
    def decouper(self) :

        indbloc = int( self.name.extraireDeMot( 'indbloc') )
        inddata = int( self.name.extraireDeMot( 'inddata') )
        value = self.value

        element = { 'indbloc':indbloc, 'inddata':inddata, 'valeurs':value }

        return element

    ##..
    #*************************************************************************\n
    # Methode decouper, decoupage de (name,value) :
    #
    # name etant de la forme "indbloc=xxx/inddata=zzz", retourne
    # le dict { 'indbloc':xxx, 'inddata':zzz, 'valeurs':value }
    #
    # Plus exactement : \n
    # - dans 'valeurs', value saisi (type str) a ete converti en son type
    #   'attendu/reel'. En cas d'erreur de cette conversion, None est
    #   attribue a 'valeurs'.
    #
    # - dans 'indbloc' et 'inddata', indbloc et inddata saisis (type str) ont
    #   ete convertis en int (son type reel).
    #
    # Remarque : dans le dict retourne
    # { 'indbloc':xxx, 'inddata':zzz, 'valeurs':value },
    # le fait que value vaille None revele un probleme/erreur.
    #
    # Remarque : avant d'appeler decouper, s'assurer du bon format de
    # name en appelant formatNameOk.
    #
    #*************************************************************************
    def ANCIEN_INUSITEdecouper(self) :

        indbloc = int( self.name.extraireDeMot( 'indbloc') )
        inddata = int( self.name.extraireDeMot( 'inddata') )

        # Determination de la valeur value attribuee a 'valeurs' :
        value = None # par defaut
        try: # essai d'extraction/conversion
            c = literal_eval( self.value ) # contenu du str
            # extraction ok, s'assurer du format
            try:
                value = c
            except (SyntaxError): # echec de conversion
                TraceErreur.message( "type ERREUR !!!" )
                #pass
        except (ValueError, SyntaxError): # cas str
            value = self.value
            m = "literal_eval ERREUR pour self.value : " + str( self.value )
            TraceErreur.message(m)
            #pass

        element = { 'indbloc':indbloc, 'inddata':inddata, 'valeurs':value }

        return element

##..
#*****************************************************************************\n
# Classe DateSaisie
#
# Donnee correspondant a la saisie d'une DateMultiFormat
#
# notes_de_developpement affichage_saisie.py :
# traitement_provisoire_date_debut_simulation
#
#*****************************************************************************
class DateSaisie(object):

    @classmethod
    def decouper( cls, yyyymmdd_hhmmss ) :
        decoupage = string.split( yyyymmdd_hhmmss.strip(), "time" )
        yyyymmdd = string.split( decoupage[0].strip(), "date" )[1].strip()
        hhmmss = decoupage[1].strip()
        return ( yyyymmdd, hhmmss )

    @classmethod
    def texte_yyyymmdd_hhmmss( cls, yyyymmdd, hhmmss ) :
        texte = "date  " + yyyymmdd + "   time  " + hhmmss
        return texte

    # valeur saisie pour champ dont valeur n'est pas a prendre en compte
    valeur_obsolete = '-'

    @classmethod
    def getValeurObsolete( cls ) :
        return cls.valeur_obsolete

    @classmethod
    def isValeurObsolete( cls, v ) :
        return ( v == cls.valeur_obsolete )

#*****************************************************************************
#*****************************************************************************
#
# INFORMATIONS D'UN PAQUET POUR LE CHOIX D'UN SCENARIO
#
#*****************************************************************************
#*****************************************************************************

##..
#*****************************************************************************\n
# PaquetInfosFormulaireScenario
#
# Le choix d'un scenario dans un paquet necessite d'afficher les paquets.
# PaquetInfosFormulaireScenario correspond aux informations d'affichage d'un 
# paquet dans le cas du choix d'un scenario (pas dans le cas d'administration
# des paquets), lorsqu'il s'agit d'un paquet ok, sans probleme (type Paquet).
#
# uid, nom et nom_pkg sont des informations d'origine du paquet
# liste_vpz est la liste des fichier vpz du paquet
# visible : booleen qui indique si la liste_vpz est a afficher ou a cacher
# Par defaut, un paquet public (sans mot de passe) est visible et un paquet
# prive (avec mot de passe) est non visible.
#
#*****************************************************************************
class ANCIEN_INUSITEPaquetInfosFormulaireScenario(object):

    def __init__( self, paquet ) :

        self.uid = paquet.uid
        self.nom = paquet.nom
        self.nom_pkg = paquet.nom_pkg

        # iii ajouts a revoir/repreciser
        self.description = paquet.description
        self.nom_responsable = paquet.nom_responsable
        self.mail_responsable = paquet.mail_responsable
        self.nom_resp_scientifique = paquet.nom_resp_scientifique
        self.nom_resp_informatique = paquet.nom_resp_informatique
        self.version_vle = paquet.version_vle

        if paquet.password=="" :
            self.visible = True
        else :
            self.visible = False

        self.liste_vpz = paquet.get_liste_vpz()


##..
#*****************************************************************************\n
# PaquetNotOkInfosFormulaireScenario
#
# Le choix d'un scenario dans un paquet necessite d'afficher les paquets.
# PaquetNotOkInfosFormulaireScenario correspond aux informations d'affichage
# d'un paquet dans le cas du choix d'un scenario (pas dans le cas
# d'administration des paquets), lorsqu'il s'agit d'un paquet not ok, qui
# pose probleme (type PaquetNotOk).
#
#*****************************************************************************
class ANCIEN_INUSITEPaquetNotOkInfosFormulaireScenario(object):

    def __init__(self, paquet_not_ok ) :
        self.nom = paquet_not_ok.paquet.nom
        self.nom_pkg = paquet_not_ok.paquet.nom_pkg
        self.msg_not_ok = paquet_not_ok.msg_not_ok


#*****************************************************************************
#*****************************************************************************
#
# POUR UN PAQUET (CAS ADMINISTRATION)
#
#*****************************************************************************
#*****************************************************************************

##..
#*****************************************************************************\n
# Methode getIndicePaquetDansListe \n
# Retourne l'indice de l'element paquet (type Paquet) de liste_paquets
# dont uid vaut uid
#\n*****************************************************************************
def ANCIEN_INUSITEgetIndicePaquetDansListe( uid, liste_paquets ) :
    liste_index = [ i for i,p in enumerate(liste_paquets) if p.uid==uid ]
    if len(liste_index) >= 1 :
        return liste_index[0]
    else :
        return None

##..
#*****************************************************************************\n
# Methode motDePasseSaisi \n
# Retourne la valeur de mot de passe saisie (unique valeur saisie)
#\n*****************************************************************************
def ANCIEN_INUSITEmotDePasseSaisi( params ) :

    # getValeursSaisies a ete deplacee ici en comptant la virer (a maj depuis
    # QueryDict, parametres_post)
    # Methode getValeurs : retourne la liste des valeurs saisies
    def getValeursSaisies( params ):
        listeValeurs = list()
        for (k,v) in params.iteritems() :
            listeValeurs.append( v.encode() )
        return listeValeurs

    listeValeurs = getValeursSaisies( params )
    mdp = listeValeurs[0] # unique valeur saisie
    return mdp

#*****************************************************************************
#*****************************************************************************
#
# POUR UNE REPRESENTATION GRAPHIQUE
#
#
# Rappel :
# - Format de type_capture_vars d'une simulation simple (voir MonoResultat) :
#   list ( type, timestep, nomvar, valeurs ).
# - Format de type_capture_vars d'une simulation multiple (voir
#   MultiResultat) :
#   tuple de tuple de list ( type, timestep, nomvar, valeurs ).
#
# Dans le but de simplifier la gestion des affichages (reduire le code de
# pages html), type_capture_vars est traite/gere dans un seul format au
# niveau des affichages/saisies : celui de la simulation multiple. Pour cela,
# au niveau de la gestion des saisies/affichages, type_capture_vars d'une
# simulation simple va etre transforme (voir methode
# transformationArtificielle) : 'ajout de 2 dimensions' pour se trouver dans
# le meme format qu'en multi-simulation.
#
# Apres cette transformation artificielle appliquee pour une simulation
# simple, type_capture_vars contient un unique element type_capture_vars[0][0].
# Si jamais besoin de distinguer le cas de simulation simple du cas de
# simulation multiple qui ne comporterait qu'une seule experience : utiliser
# l'attribut type_simulation ("mono_simulation" ou "multi_simulation") de
# Resultat.
#
#*****************************************************************************
#*****************************************************************************

##..
#*****************************************************************************\n
# Methode transformationArtificielle
#
# Met type_capture_vars d'une simulation simple (tcv_d_origine) au format
# d'une simulation multiple (tcv_transforme).
#
#*****************************************************************************
def transformationArtificielle( tcv_d_origine ) :
    tcv_transforme = list()
    tcv = list()
    tcv.append( tcv_d_origine )
    tcv_transforme.append( tcv )
    return tcv_transforme

##..
#*****************************************************************************\n
# Methode definir_Y
#
# Cas d'une representation graphique ou l'abscisse est la meme pour tous les
# traces. Cas de simulation simple et de simulation multiple.
#
# Definit la liste liste_XY des (var en abscisse, var en ordonnee) fonction 
# des choix faits dans params. Les elements utiles de params sont : (axe,var)
# avec axe= "X" ou "Y".
#
# Dans le cas type_simulation valant "multi_simulation" :
#
# - var est de la forme type=xxx/timestep=yyy/nomvar=zzz-r-c"
#
# - Dans un element (elementX,elementY) de liste_XY,
#   elementX et elementY sont de la forme
#   ( valeur de r, valeur de c, valeur de type, valeur de timestep,
#     valeur de nomvar (sans terminaison) )
#
# Dans le cas type_simulation valant "mono_simulation" :
#
# - var est de la forme type=xxx/timestep=yyy/nomvar=zzz"
#
# - Dans un element (elementX,elementY) de liste_XY,
#   elementX et elementY sont de la forme
#   ( valeur de type, valeur de timestep, valeur de nomvar )
#
# Retourne un booleen qui indique s'il y a eu ou non probleme lors de la
# constitution de liste_XY : X ou Y inexistant...
#
#*****************************************************************************
def definir_Y( liste_XY, params, type_simulation ):

    cr_ok = True # par defaut

    #*************************************************************************
    # abscisse X unique (meme pour tous traces)	
    #*************************************************************************
    X = None
    for axe in params.get_cles() :

        if axe == "X" :

            for var in params.get_valeurs( axe ) :

                mot = MotCode( var.encode() )
                if mot.formatObsVarOk() :
                # var est bien de la forme "type=xxx/timestep=yyy/nomvar=zzz"

                    #*********************************************************
                    # cas "multi_simulation" :
                    #*********************************************************
                    if type_simulation == "multi_simulation" :
                    # var est attendu de la forme "type=xxx/timestep=yyy/nomvar=zzz-r-c"
    
                        i1i2 = mot.enlever_terminaison()
                        if i1i2 != None :
                        # var a bien terminaison de la forme attendue "-r-c"
                            (mot_i1,mot_i2) = i1i2
                            element = ( int(mot_i1),
                                        int(mot_i2),
                                        mot.extraireDeMot('type'),
                                        float(mot.extraireDeMot('timestep')),
                                        mot.extraireDeMot('nomvar') )
                            X = element 
                        else :
                        # var n'a pas terminaison de la forme attendue "-r-c"
                            cr_ok = False
    
                    #*********************************************************
                    # cas "mono_simulation" :
                    #*********************************************************
                    else : # "mono_simulation" et de plus par defaut
                    # var est attendu de la forme "type=xxx/timestep=yyy/nomvar=zzz"
                    # (sans terminaison "-r-c")
    
                        element = ( mot.extraireDeMot('type'),
                                    float(mot.extraireDeMot('timestep')),
                                    mot.extraireDeMot('nomvar') )
                        X = element 
                else :
                # var n'est pas de la forme "type=xxx/timestep=yyy/nomvar=zzz"
                    cr_ok = False

    if X == None : # X inexistant
        cr_ok = False
    else :

        #*********************************************************************
        # ordonnees
        #*********************************************************************
        for axe in params.get_cles() :

            if axe == "Y" :

                for var in params.get_valeurs( axe ) :

                    mot = MotCode( var.encode() )
                    if mot.formatObsVarOk() :
                    # var est bien de la forme
                    # "type=xxx/timestep=yyy/nomvar=zzz"

                        #*****************************************************
                        # cas "multi_simulation" :
                        #*****************************************************
                        if type_simulation == "multi_simulation" :
                        # var est attendu de la forme "type=xxx/timestep=yyy/nomvar=zzz-r-c"

                            i1i2 = mot.enlever_terminaison()
                            if i1i2 != None :
                            # var a bien terminaison de la forme attendue "-r-c"
                                (mot_i1,mot_i2) = i1i2
                                element = ( int(mot_i1),
                                            int(mot_i2),
                                            mot.extraireDeMot('type'),
                                            float(mot.extraireDeMot('timestep')),
                                            mot.extraireDeMot('nomvar') )
                                liste_XY.append( (X,element) )
                            else :
                            # var n'a pas terminaison de la forme attendue "-r-c"
                                cr_ok = False

                        #*****************************************************
                        # cas "mono_simulation" :
                        #*****************************************************
                        else : # "mono_simulation" et de plus par defaut
                        # var est attendu de la forme "type=xxx/timestep=yyy/nomvar=zzz"
                        # (sans terminaison "-r-c")
                            element = ( mot.extraireDeMot('type'),
                                        float(mot.extraireDeMot('timestep')),
                                        mot.extraireDeMot('nomvar') )
                            liste_XY.append( (X,element) )

                    else :
                    # var n'est pas de la forme "type=xxx/timestep=yyy/nomvar=zzz"
                        cr_ok = False

        if len(liste_XY) == 0 : # Y inexistant
            cr_ok = False

    return cr_ok

##..
#*****************************************************************************\n
# Methode definir_XY
#
# Cas d'une representation graphique ou l'abscisse n'est pas forcement la
# meme pour tous les traces. Cas de simulation simple et de simulation multiple.
#
# Definit la liste liste_XY des (var en abscisse, var en ordonnee) fonction 
# des choix faits dans params. Les elements utiles de params sont : ("XY",var)
# avec var de la forme "Y(X)".
#
# Dans le cas type_simulation valant "multi_simulation" :
#
# - var est de la forme "Y(X)", ou X et Y sont de la forme
#   type=xxx/timestep=yyy/nomvar=zzz-r-c"
#
# - Dans un element (elementX,elementY) de liste_XY,
#   elementX et elementY sont de la forme
#   ( valeur de r, valeur de c, valeur de type, valeur de timestep,
#     valeur de nomvar (sans terminaison) )
#
# Dans le cas type_simulation valant "mono_simulation" :
#
# - var est de la forme "Y(X)", ou X et Y sont de la forme
#   type=xxx/timestep=yyy/nomvar=zzz"
#
# - Dans un element (elementX,elementY) de liste_XY,
#   elementX et elementY sont de la forme
#   ( valeur de type, valeur de timestep, valeur de nomvar )
#
# Retourne un booleen qui indique s'il y a eu ou non probleme lors de la
# constitution de liste_XY : aucun couple (X,Y)...
#
#*****************************************************************************
def definir_XY( liste_XY, params, type_simulation ):

    ## la methode extraireX sort X de mot qui est de la forme Y(X)
    def extraireX( mot ) :
        mot_extrait = mot.split(')')[0]
        X = mot_extrait.split('(')[-1]
        return X

    ## la methode extraireY sort Y de mot qui est de la forme Y(X)
    def extraireY( mot ) :
        Y = mot.split('(')[0]
        return Y

    #*************************************************************************
    # Le traitement :
    #*************************************************************************

    cr_ok = True # par defaut

    for type in params.get_cles() :

        for var in params.get_valeurs( type ) :

            if type == "XY" :

                # var est attendu de la forme Y(X)
                X = extraireX( var.encode() )
                Y = extraireY( var.encode() )
                motX = MotCode( X )
                motY = MotCode( Y )

                if motX.formatObsVarOk() and motY.formatObsVarOk() :
                # X et Y sont bien de la forme
                # "type=xxx/timestep=yyy/nomvar=zzz"

                    #*********************************************************
                    # cas "multi_simulation" :
                    #*********************************************************
                    if ( type_simulation == "multi_simulation" ) :
                    # X et Y sont attendus de la forme "type=xxx/timestep=yyy/nomvar=zzz-r-c"

                        motX_i1i2 = motX.enlever_terminaison()
                        motY_i1i2 = motY.enlever_terminaison()

                        if motX_i1i2 != None and motY_i1i2 != None :
                        # X et Y ont bien terminaison de la forme attendue "-r-c"

                            (motX_i1,motX_i2) = motX_i1i2
                            (motY_i1,motY_i2) = motY_i1i2

                            elementX = ( int(motX_i1),
                                         int(motX_i2),
                                         motX.extraireDeMot('type'),
                                         float(motX.extraireDeMot('timestep')),
                                         motX.extraireDeMot('nomvar') )
                            elementY = ( int(motY_i1),
                                         int(motY_i2),
                                         motY.extraireDeMot('type'),
                                         float(motY.extraireDeMot('timestep')),
                                         motY.extraireDeMot('nomvar') )
    
                            liste_XY.append( (elementX,elementY) )
    
                        else :
                        # X ou/et Y n'a pas terminaison de la forme attendue "-r-c"
                            cr_ok = False

                    #*********************************************************
                    # cas "mono_simulation" :
                    #*********************************************************
                    else : # "mono_simulation" et de plus par defaut
                    # X et Y sont attendus de la forme "type=xxx/timestep=yyy/nomvar=zzz"
                    # (sans terminaison "-r-c")
    
                        elementX = ( motX.extraireDeMot('type'),
                                     float(motX.extraireDeMot('timestep')),
                                     motX.extraireDeMot('nomvar') )
                        elementY = ( motY.extraireDeMot('type'),
                                     float(motY.extraireDeMot('timestep')),
                                     motY.extraireDeMot('nomvar') )
    
                        liste_XY.append( (elementX,elementY) )

                else :
                # X ou/et Y n'est pas de la forme "type=xxx/timestep=yyy/nomvar=zzz"
                    cr_ok = False

    if len(liste_XY) >= 1 : # au moins un (X,Y)
        cr_ok = True
    else : # aucun (X,Y)
        cr_ok = False

    return cr_ok

#*****************************************************************************

