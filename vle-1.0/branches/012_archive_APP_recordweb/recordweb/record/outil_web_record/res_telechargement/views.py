#-*- coding:utf-8 -*-

## @file record/outil_web_record/res_telechargement/views.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File views.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
# View res_telechargement
#
# Restitution/observation des resultats de la simulation effectuee d'un
# scenario. Parmi les differentes formes de restitution existantes, il s'agit 
# ici de restitution sous forme de telechargement. Deux possibilites :
# - telechargement du dossier complet
# - telechargement du dernier fichier de representation graphique conserve
#
# Fait partie du processus d'exploitation des scenarios par les utilisateurs,
# selon lequel il s'agit de choisir un scenario (d'un modele record), puis de
# le simuler apres en avoir visualise et eventuellement modifie la configuration
# (parametrage), et enfin d'exploiter sous diverses formes les resultats de
# la simulation effectuee (les visualiser a l'ecran, telecharger...).
#
#*****************************************************************************

from django.http import HttpResponse
from django.shortcuts import render_to_response

from record.outil_web_record.models.affichage_saisie.choix_operations import ChoixOperations as CHX # noms des choix
from record.outil_web_record.models.affichage_saisie.appel_page import NomsPages, contexte_telechargement_menu
from record.outil_web_record.models.erreurs_gerees.erreurs_gerees import ErreursGerees as ERR # noms des erreurs

from record.outil_web_record.utils.commun_views import initialiserControleur

from record.utils import session
from record.utils.erreur import Erreur # gestion des erreurs
from configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

from django.utils.translation import ugettext as _

#*****************************************************************************
#
# TELECHARGEMENT DE RESULTAT DE SIMULATION DU SCENARIO 
#
#*****************************************************************************

##..
#*****************************************************************************\n
#
# Affichage du menu de telechargement et sa preparation
#
#*****************************************************************************
def telechargement_menu(request) :
    t_ecr.trait()
    t_ecr.message("(telechargement_menu:) appel de initialiserControleur")
    initialiserControleur(request)

    #*************************************************************************
    # Preparation du contexte d'affichage du menu de telechargement
    #*************************************************************************

    scn = session.get(request, 'scn')

    le_modele_record = session.get(request, 'le_modele_record')

    flashes = [] # pas de flashes
    c = contexte_telechargement_menu(request, flashes, scn, le_modele_record)

    return render_to_response( NomsPages.NOM_page_telechargement_menu, c)

##..
#*****************************************************************************\n
#
# Une simulation du scenario choisi a ete effectuee (scn.resultat existe) \n
# Le traitement effectue varie en fonction de id_operation qui caracterise
# l'endroit depuis lequel telechargement est appele et la raison de
# l'appel. \n
# Appel depuis affichage menu de telechargement (le type de telechargement
# choisi est choix_telechargement=id_operation) : traitement de
# telechargement de resultats correspondant. \n
# En cas d'erreur, affichage du menu de telechargement avec erreur.
#
#*****************************************************************************
def telechargement(request):
    t_ecr.trait()
    t_ecr.message("(telechargement:) appel de initialiserControleur")
    initialiserControleur(request)

    #*************************************************************************
    # les informations en entree
    #*************************************************************************

    id_operation = None # par defaut
    if "id_operation" in request.POST.keys() :
        id_operation = request.POST["id_operation"]
    id_operation = int(id_operation)

    # Cas Choix de telechargement
    if CHX.is_choix_telechargement( id_operation ) :

        choix_telechargement = id_operation

        scn = session.get(request, 'scn')

        if choix_telechargement == CHX.VAL_choix_telechgtDossier :
            buffer = scn.espace_exploitation.produireBufferTelechargementDossier_zip()
            content_disposition = str( 'attachment; filename=' + scn.espace_exploitation.getNomDossier_zip() )
        elif choix_telechargement == CHX.VAL_choix_telechgtDernierFichier :
            nom_fichier = scn.resultat.getNomDernierFichierRepresentationGraphique()
            buffer = scn.espace_exploitation.produireBufferTelechargementDernierFichier_zip( nom_fichier )
            content_disposition = str( 'attachment; filename=' + scn.espace_exploitation.getNomDernierFichier_zip() )

        response = HttpResponse( buffer.getvalue(), content_type='application/zip' )
        response['Content-Disposition'] = content_disposition

        return response

    # Cas d'erreur (ne devrait pas arriver)
    else : # affichage du menu de telechargement avec erreur

        # creation erreur_telechargement
        erreur_telechargement = Erreur( ERR.NOM_erreur_telechargement ) # desactivee par defaut
        erreur_telechargement.activer()
        message_erreur = _(u"type de téléchargement demandé inconnu")
        erreur_telechargement.set_message( message_erreur )

        #*********************************************************************
        # Preparation du contexte d'affichage du menu de telechargement
        #*********************************************************************

        scn = session.get(request, 'scn')

        le_modele_record = session.get(request, 'le_modele_record')

        # Liste messages d'erreur en flashes
        flashes = [] # par defaut
        if erreur_telechargement.isActive() : # maj flashes selon erreur_telechargement
            texte_erreur = _(u"Erreur") + " : " + _(u"le téléchargement a échoué") + "."
            texte_message_erreur = _(u"Message d'erreur") + " : " + erreur_telechargement.get_message() + "."
            flashes.append( texte_erreur )
            flashes.append( texte_message_erreur )

        c = contexte_telechargement_menu(request, flashes, scn, le_modele_record)

        return render_to_response( NomsPages.NOM_page_telechargement_menu, c)

#*****************************************************************************

