#-*- coding:utf-8 -*-

## @file record/record_settings.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File record_settings.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# For django settings of projects that use record applications (common part).
#
#*****************************************************************************

import os

RECORDWEB_RECORD_DIR = os.path.abspath(os.path.dirname(__file__))

# TEMPLATE_DIRS **************************************************************

TEMPLATE_DIRS_RECORD = ( os.path.join(RECORDWEB_RECORD_DIR,'templates'), ) 

# STATICFILES_DIR ************************************************************

STATICFILES_DIRS_RECORD= ( os.path.join(RECORDWEB_RECORD_DIR,'site_media'), )

#*****************************************************************************

print "[record/record_settings.py] pour info variables (settings) :"
print "[record/record_settings.py] RECORDWEB_RECORD_DIR = ", RECORDWEB_RECORD_DIR
print "[record/record_settings.py] TEMPLATE_DIRS_RECORD = ", TEMPLATE_DIRS_RECORD
print "[record/record_settings.py] STATICFILES_DIRS_RECORD= ", STATICFILES_DIRS_RECORD

#*****************************************************************************

