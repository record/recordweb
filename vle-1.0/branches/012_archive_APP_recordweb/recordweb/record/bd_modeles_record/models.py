#-*- coding:utf-8 -*-

## @file record/bd_modeles_record/models.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File bd_modeles_record/models.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# Le modele record enregistre en bd (ModeleRecord ...)
#
#*****************************************************************************

import os # pour getNomRepertoireDataDefault

from django.db import models
from django.contrib.auth.models import User

from django.utils.translation import ugettext as _

from transmeta import TransMeta

from record.utils.vle.pkg import Pkg

from record.bd_modeles_record.configs.conf_bd_modeles_record import CONF_bd_modeles_record

##..
#*****************************************************************************\n
# MotDePasse
#
# Chaque ModeleRecord prive est associe a un MotDePasse mot_de_passe. \n
# Un meme MotDePasse peut servir pour plusieurs ModeleRecord prives. \n
# Le mot_de_passe 'modele_public' est reserve/attribue a tout ModeleRecord
# public ; il doit exister par defaut dans la bd.
#
# MotDePasse est derive de User. Seuls certains champs de User sont
# exploites/geres ('username', 'password' ; cf MotDePasseAdmin).
#
#*****************************************************************************
class MotDePasse(User):

    class Meta :
        verbose_name = _(u"Mot de passe de Modèle(s)")
        verbose_name_plural = _(u"Mots de passe de Modèle(s)")

    def __unicode__(self):
        return 'mot de passe : %s' % (self.username)

#*****************************************************************************
# Constantes et methodes relatives au mot de passe public
#*****************************************************************************

mot_de_passe_public_username = 'modele_public'
mot_de_passe_public_password = 'public'

## construit et enregistre dans la bd le mot de passe public
def enregistrerMotDePassePublic():
    mot_de_passe_public = MotDePasse( username=mot_de_passe_public_username, password=mot_de_passe_public_password )
    mot_de_passe_public.save()

## determine si le MotDePasse mot_de_passe est ou non public
# (selon critere base sur username).
# retourne True pour public, False pour prive, None pour etat inconnu
def estMotDePassePublic( mot_de_passe ):
    cr_is_public = None # pour etat inconnu, par defaut
    if mot_de_passe is not None :
        if mot_de_passe.username == mot_de_passe_public_username :
            cr_is_public = True
        else :
            cr_is_public = False
    return cr_is_public

##..
#*****************************************************************************\n
# Individu
#
# Les responsable, responsable_scientifique et responsable_informatique des
# ModeleRecord sont des Individu. \n
# Un meme Individu peut jouer differents roles vis-a-vis de differents
# ModeleRecord.
#
# Individu est derive de User. Seuls certains champs de User sont
# exploites/geres ('username', 'first_name', 'last_name', 'email' ;
# cf IndividuAdmin).
#
#*****************************************************************************
class Individu(User):

    class Meta :
        verbose_name = _(u"Individu")
        verbose_name_plural = _(u"Individus")

    def __unicode__(self):
        return 'individu : %s' % (self.username)

#*****************************************************************************
# Methodes utiles au traitement de champs du ModeleRecord qui sont des noms 
# de repertoires/fichiers definis en tant que chemin relatif par rapport a un
# repertoire racine. Methodes egalement utilisees ailleurs
# (ParDefautModeleRecord, ModeleRecord...Form)
# Champs concernes :
# nom_repertoire_lot_pkgs, nom_repertoire_data,
# nom_fichier_config_web_conf, nom_fichier_config_web_applis,
# nom_fichier_config_dicos.
#*****************************************************************************

## get_conf_repertoire_racine_configuration retourne le repertoire racine
# (chemin absolu) a partir duquel sont donnes les chemins relatifs
# nom_fichier_config_web_conf, nom_fichier_config_web_applis et
# nom_fichier_config_dicos
def get_conf_repertoire_racine_configuration() :
    return CONF_bd_modeles_record.repertoire_racine_configuration

## get_conf_repertoire_racine_lots_pkgs retourne le repertoire racine (chemin 
# absolu) a partir duquel est donne le chemin relatif nom_repertoire_lot_pkgs
def get_conf_repertoire_racine_lots_pkgs() :
    return CONF_bd_modeles_record.repertoire_racine_lots_pkgs

## get_conf_repertoire_racine_data retourne le repertoire racine (chemin
# absolu) a partir duquel est donne le chemin relatif nom_repertoire_data
def get_conf_repertoire_racine_data() :
    return CONF_bd_modeles_record.repertoire_racine_data

# Methodes determinant et retournant le nom absolu de repertoire/fichier
# correspondant au nom relatif de repertoire/fichier.
# retourne None pour entree None ou ""

def getNomAbsoluRepertoireLotPkgs( nom_repertoire_lot_pkgs ) :
    rep = None # par defaut
    if (nom_repertoire_lot_pkgs is not None) and (nom_repertoire_lot_pkgs != "") :
        rep = os.path.join( get_conf_repertoire_racine_lots_pkgs(), nom_repertoire_lot_pkgs )
    return rep
def getNomAbsoluRepertoireData( nom_repertoire_data ) :
    rep = None # par defaut
    if (nom_repertoire_data is not None) and (nom_repertoire_data != "") :
        rep = os.path.join( get_conf_repertoire_racine_data(), nom_repertoire_data )
    return rep
def getNomAbsoluFichierConfigWebConf( nom_fichier_config_web_conf ) :
    f = None # par defaut
    if (nom_fichier_config_web_conf is not None) and (nom_fichier_config_web_conf != "") :
        f = os.path.join( get_conf_repertoire_racine_configuration(), nom_fichier_config_web_conf )
    return f
def getNomAbsoluFichierConfigWebApplis( nom_fichier_config_web_applis ) :
    f = None # par defaut
    if (nom_fichier_config_web_applis is not None) and (nom_fichier_config_web_applis != "") :
        f = os.path.join( get_conf_repertoire_racine_configuration(), nom_fichier_config_web_applis )
    return f
def getNomAbsoluFichierConfigDicos( nom_fichier_config_dicos ) :
    f = None # par defaut
    if (nom_fichier_config_dicos is not None) and (nom_fichier_config_dicos != "") :
        f = os.path.join( get_conf_repertoire_racine_configuration(), nom_fichier_config_dicos )
    return f

#*****************************************************************************
# Methodes utiles au traitement de nom_repertoire_data du ModeleRecord
# egalement utilisees ailleurs (ParDefautModeleRecord, ModeleRecord...Form)
#*****************************************************************************

## retourne la valeur de nom_repertoire_data correspondant a demande du
# repertoire par defaut
def defaultValueNomRepertoireData() :
    return ( "default" )

## determine et retourne le nom (chemin absolu) du repertoire des donnees de
# simulation par defaut. Il s'agit du repertoire 'data' du paquet principal, 
# caracterise par le nom du paquet principal nom_pkg et son emplacement
# nom_repertoire_lot_pkgs (chemin relatif).
# rq : il existe aussi (dans ModeleRecord)
# getNomAbsoluRepertoireDataParDefaut(self)
def getNomAbsoluRepertoireDataParDefaut( nom_repertoire_lot_pkgs, nom_pkg ) :
    nom_absolu_repertoire_lot_pkgs = getNomAbsoluRepertoireLotPkgs( nom_repertoire_lot_pkgs )
    repertoire_absolu_paquet = os.path.join( nom_absolu_repertoire_lot_pkgs, nom_pkg )
    return ( Pkg.getRepertoirePaquetData( repertoire_absolu_paquet ) )

##..
#*****************************************************************************\n
# ParDefautModeleRecord : classe utilitaire relative a ModeleRecord
#
# Classe contenant/definissant des constantes de configuration de champs de
# ModeleRecord (valeurs par defaut de label, help_text...) servant a
# ModeleRecord et egalement susceptibles de servir a des ModeleRecord_xx_Form
#
#*****************************************************************************
class ParDefautModeleRecord(object):

	# nom
    label_nom = _(u"Nom")
    help_text_nom = _(u"Nom donné au modèle dans la base")
    max_length_nom = 200

	# description
    label_description = _(u"Description")
    help_text_description = _(u"Description du modèle")
    max_length_description = 500

    # nom_repertoire_lot_pkgs
    label_nom_repertoire_lot_pkgs = _(u"lot des paquets vle")
    max_length_nom_repertoire_lot_pkgs = 500
    help_text_nom_repertoire_lot_pkgs = _(u"Nom (relatif) du répertoire contenant l'ensemble des paquets vle du modèle (nom libre).") + " " + _(u"Nom relatif depuis le répertoire") + " " + get_conf_repertoire_racine_lots_pkgs() + "."

    # nom_repertoire_data
    label_nom_repertoire_data = _(u"répertoire des fichiers de données")
    max_length_nom_repertoire_data = 500
    help_text_nom_repertoire_data = _(u"Nom (relatif) du répertoire contenant les fichiers de données de simulation du modèle (nom libre).") + " " + _(u"Nom relatif depuis le répertoire") + " " + get_conf_repertoire_racine_data() + "."
    help_text_nom_repertoire_data = help_text_nom_repertoire_data + " " + _(u"Valeur") + " '" + defaultValueNomRepertoireData() + "' " + _(u"pour le répertoire 'data' du paquet principal")

    # nom_pkg
    label_nom_pkg = _(u"Nom du paquet principal")
    max_length_nom_pkg = 200
    help_text_nom_pkg = _(u"Nom du paquet principal (project, sous pkgs) : celui qui contient les fichiers vpz scénario de simulation du modèle")

    # mot_de_passe
    label_mot_de_passe = _(u"Mot de passe")
    help_text_mot_de_passe = _(u"Mot de passe du modèle ('modele_public' pour un modèle public).")

    # fichiers vpz de configuration

    max_length_config = 500 # commun

    label_nom_fichier_config_web_conf = _(u"configuration web conf")
    help_text_nom_fichier_config_web_conf = _(u"Nom (relatif) du fichier vpz de configuration web propre à la conf web. Souvent nommé web_conf.vpz mais sans obligation (nom libre).") + " " + _(u"Nom relatif depuis le répertoire") + " " + get_conf_repertoire_racine_configuration() + "."

    label_nom_fichier_config_web_applis = _(u"configuration web applis")
    help_text_nom_fichier_config_web_applis = _(u"Nom (relatif) du fichier vpz de configuration web propre aux applis web. Souvent nommé web_applis.vpz mais sans obligation (nom libre).") + " " + _(u"Nom relatif depuis le répertoire") + " " + get_conf_repertoire_racine_configuration() + "."
    
    label_nom_fichier_config_dicos = _(u"configuration dictionnaire")
    help_text_nom_fichier_config_dicos = _(u"Nom (relatif) du fichier vpz de configuration propre au dictionnaire. Souvent nommé dicos.vpz mais sans obligation (nom libre).") + " " + _(u"Nom relatif depuis le répertoire") + " " + get_conf_repertoire_racine_configuration() + "."

    # responsables

    label_responsable = _(u"Responsable")
    help_text_responsable = _(u"Correspondant principal. Dans le cas de modèle privé, il distribue le mot de passe aux personnes de son choix.")

    label_responsable_scientifique = _(u"Responsable scientifique")
    help_text_responsable_scientifique = _(u"Correspondant pour les questions scientifiques")

    label_responsable_informatique = _(u"Responsable informatique")
    help_text_responsable_informatique = _(u"Correspondant pour les questions informatiques")

    # cr_reception
    label_cr_reception = _(u"Compte rendu de réception")
    max_length_cr_reception = 500
    help_text_cr_reception = _(u"Note rapport de la procédure de recette du modele")

	# version_vle
    label_version_vle = _(u"Version vle de réception")
    max_length_version_vle = 200
    help_text_version_vle = _(u"Version vle sous laquelle le modèle a été réceptionné")

##..
#*****************************************************************************\n
# ModeleRecord
#
# Les ModeleRecord sont les modeles record enregistres en bd, mis a
# disposition au sein de l'outil Web Record
#
# Un ModeleRecord comprend le nom de son pkg vle associe et d'autres
# renseignements necessaires a son exploitation : nom de son responsable,
# version vle, mot de passe (mot de passe prive ou 'modele_public') ...
# Un ModeleRecord ne contient pas la liste (dynamique) de ses scenarios
# (fichiers vpz).
#
# Note : la plupart des champs d'un ModeleRecord sont obligatoires mais
# certains sont optionnels (les fichiers vpz de configuration
# nom_fichier_config_xxx). Des verifications sont faites au sein de l'outil
# Web record pour/avant de considerer que le ModeleRecord peut etre
# choisi/exploite dans l'outil Web Record.
#
#*****************************************************************************
class ModeleRecord(models.Model):

    __metaclass__ = TransMeta

    #*************************************************************************
    #
    # Les champs du ModeleRecord
    #
    #*************************************************************************

    #*************************************************************************
	# nom du modele 
    #*************************************************************************
    nom = models.CharField( ParDefautModeleRecord.label_nom, max_length=ParDefautModeleRecord.max_length_nom )
    nom.help_text = ParDefautModeleRecord.help_text_nom
    nom.blank = False # obligatoire

    #*************************************************************************
	# description du modele 
    #*************************************************************************
    description = models.TextField( ParDefautModeleRecord.label_description, max_length=ParDefautModeleRecord.max_length_description )
    description.help_text = ParDefautModeleRecord.help_text_description
    description.blank = False # obligatoire

    #*************************************************************************
    # correspondance avec/relation a pkgs vle
    #*************************************************************************

    # nom (relatif) du repertoire regroupant les paquets constituant le modele
    # (le paquet principal, les paquets de dependance).
    # nom relatif depuis 'get_conf_repertoire_racine_lots_pkgs'
    nom_repertoire_lot_pkgs = models.CharField( ParDefautModeleRecord.label_nom_repertoire_lot_pkgs, max_length=ParDefautModeleRecord.max_length_nom_repertoire_lot_pkgs )
    nom_repertoire_lot_pkgs.help_text = ParDefautModeleRecord.help_text_nom_repertoire_lot_pkgs
    nom_repertoire_lot_pkgs.blank = False # obligatoire

    # nom effectif du paquet sous pkgs (paquet principal du modele)
    nom_pkg = models.CharField( ParDefautModeleRecord.label_nom_pkg, max_length=ParDefautModeleRecord.max_length_nom_pkg )
    nom_pkg.help_text = ParDefautModeleRecord.help_text_nom_pkg
    nom_pkg.blank = False # obligatoire

    # nom (relatif) du repertoire contenant les fichiers de donnees de
    # simulation (valeur a donner par defaut : chemin du sous-repertoire
    # data du paquet principal).
    # nom relatif depuis 'get_conf_repertoire_racine_data'
    nom_repertoire_data = models.CharField( ParDefautModeleRecord.label_nom_repertoire_data, max_length=ParDefautModeleRecord.max_length_nom_repertoire_data )
    nom_repertoire_data.help_text = ParDefautModeleRecord.help_text_nom_repertoire_data
    nom_repertoire_data.blank = True # optionnel 

    #*************************************************************************
    # accessibilite (modele prive/public)
    #*************************************************************************

	# mot de passe du paquet (cf acces prive)
    mot_de_passe = models.ForeignKey( MotDePasse )
    mot_de_passe.help_text = ParDefautModeleRecord.help_text_mot_de_passe
    mot_de_passe.null = True
    mot_de_passe.blank = False # obligatoire (mot_de_passe 'modele_public' pour un modele public)

    #*************************************************************************
    # Fichiers vpz de configuration : 
    # Au niveau de l'outil_web_record, il est attendu concernant le nom
    # (absolu) des 3 fichiers vpz de configuration de correspondre a des 
    # fichiers au format vpz.
    # Voir ModeleRecordAdminForm pour les autres contraintes imposees dans le
    # cadre de la saisie/enregistrement en bd de ces noms de fichiers.
    #*************************************************************************

    # nom (relatif) du fichier vpz de configuration web propre a la conf web
    # nom relatif depuis 'get_conf_repertoire_racine_configuration'
    nom_fichier_config_web_conf = models.CharField( ParDefautModeleRecord.label_nom_fichier_config_web_conf, max_length=ParDefautModeleRecord.max_length_config )
    nom_fichier_config_web_conf.help_text = ParDefautModeleRecord.help_text_nom_fichier_config_web_conf
    nom_fichier_config_web_conf.blank = True # optionnel 

    # nom (relatif) du fichier vpz de configuration web propre aux applis web
    # nom relatif depuis 'get_conf_repertoire_racine_configuration'
    nom_fichier_config_web_applis = models.CharField( ParDefautModeleRecord.label_nom_fichier_config_web_applis, max_length=ParDefautModeleRecord.max_length_config )
    nom_fichier_config_web_applis.help_text = ParDefautModeleRecord.help_text_nom_fichier_config_web_applis
    nom_fichier_config_web_applis.blank = True # optionnel
    
    # nom (relatif) du fichier vpz de configuration propre au dictionnaire
    # nom relatif depuis 'get_conf_repertoire_racine_configuration'
    nom_fichier_config_dicos = models.CharField( ParDefautModeleRecord.label_nom_fichier_config_dicos, max_length=ParDefautModeleRecord.max_length_config )
    nom_fichier_config_dicos.help_text = ParDefautModeleRecord.help_text_nom_fichier_config_dicos
    nom_fichier_config_dicos.blank = True # optionnel

    #*************************************************************************
    # Responsables (individus)
    #*************************************************************************

	# responsable du modele record
	# qui en est le correspondant pour administrateur(s),
	# qui en distribue le mot de passe aux personnes de son choix.
    responsable = models.ForeignKey(Individu, related_name=_(u"responsable") )
    responsable.help_text = ParDefautModeleRecord.help_text_responsable
    responsable.null = True
    responsable.blank = False # obligatoire

    # responsable scientifique
    responsable_scientifique = models.ForeignKey(Individu, related_name=_(u"responsable_scientifique") )
    responsable_scientifique.help_text = ParDefautModeleRecord.help_text_responsable_scientifique
    responsable_scientifique.null = True 
    responsable_scientifique.blank = False # obligatoire

    # responsable informatique
    responsable_informatique = models.ForeignKey(Individu, related_name=_(u"responsable_informatique") )
    responsable_informatique.help_text = ParDefautModeleRecord.help_text_responsable_informatique
    responsable_informatique.null = True 
    responsable_informatique.blank = False # obligatoire

    #*************************************************************************
    # compte-rendu de reception/recette
    #*************************************************************************
    # debut temporaire - informations deposees temporairement dans le champ
    # cr_reception, a prendre en compte dans la structure finale de la BD
    # des modeles record

    texte_default = "------------------------------ " + _(u"à renseigner :") + "\n"
    texte_default = texte_default + _(u"- Accord pour depot dans l'outil Web record : oui-non")
    texte_default = texte_default + "\n"
    texte_default = texte_default + "------------------------------ " + _(u"en particulier :") + "\n"
    texte_default = texte_default + _(u"- le modele fonctionne sous Linux : oui-non")
    texte_default = texte_default + "\n"
    texte_default = texte_default + _(u"- Le modele n'effectue pas d'affichage/trace ecran : oui-non")
    texte_default = texte_default + "\n"
    texte_default = texte_default + _(u"- Procedure d'installation du modele mentionnant notamment les librairies requises (par exemple GDAL pour Gengiscan, gfortran pour NativeStics) : ...")
    texte_default = texte_default + "\n"
    texte_default = texte_default + "------------------------------ " + _(u"+ texte libre :") + "\n"
    texte_default = texte_default + _(u"rapport du deroulement/passage de la procedure d'acceptation (verification fonctionnelle...)...")
    texte_default = texte_default + "\n"

    # fin temporaire
    cr_reception = models.TextField( ParDefautModeleRecord.label_cr_reception, max_length=ParDefautModeleRecord.max_length_cr_reception, default=texte_default )

    cr_reception.help_text = ParDefautModeleRecord.help_text_cr_reception
    cr_reception.blank = False # obligatoire

    #*************************************************************************
	# version vle sous laquelle le paquet a ete receptionne
    #*************************************************************************
    version_vle = models.CharField( ParDefautModeleRecord.label_version_vle, max_length=ParDefautModeleRecord.max_length_version_vle )
    version_vle.help_text = ParDefautModeleRecord.help_text_version_vle
    version_vle.blank = False # obligatoire

    #*************************************************************************

    class Meta :
        verbose_name = _(u"Modèle")
        verbose_name_plural = _(u"Modèles")

        translate = ( 'nom', 'description', 'cr_reception' )

    def __unicode__(self):
        return 'modele %s (pkg %s)' % (self.nom, self.nom_pkg)

    ##..
    #*************************************************************************\n
    # isPublic determine si le ModeleRecord est public.
    #\n*************************************************************************
    def isPublic(self):
        return ( estMotDePassePublic(self.mot_de_passe) )

    ##..
    #*************************************************************************\n
    # checkPassword determine si mdp correspond ou non au mot de passe du
    # ModeleRecord. Retourne True si identite.
    #\n*************************************************************************
    def checkPassword(self, mdp):
        return ( self.mot_de_passe.check_password(mdp) )

    #*************************************************************************
    # Methodes relatives a nom_repertoire_data
    # Valeur/cas particulier du repertoire par defaut :
    # nom_repertoire_data vaut 'defaultValueNomRepertoireData' dans le cas de
    # demande du repertoire par defaut. Dans ce cas, le nom (absolu) de
    # repertoire correspondant est getNomAbsoluRepertoireDataParDefaut.
    # (voir certaines de ces methodes en dehors de la classe ModeleRecord)
    #*************************************************************************

    ## determine et retourne le nom (chemin absolu) du repertoire des donnees
    # de simulation par defaut. Il s'agit du repertoire 'data' du paquet
    # principal.
    # rq : il existe aussi (hors classe)
    # getNomAbsoluRepertoireDataParDefaut(nom_repertoire_lot_pkgs,nom_pkg)
    def getNomAbsoluRepertoireDataParDefaut( self ) :
        return getNomAbsoluRepertoireDataParDefaut( self.nom_repertoire_lot_pkgs, self.nom_pkg )

    ## retourne True si nom_repertoire_data correspond a demande du repertoire
    # par defaut
    def aNomRepertoireDataParDefaut(self) :
        return (self.nom_repertoire_data == defaultValueNomRepertoireData() )


    #*************************************************************************
    # Methodes determinant et retournant le nom absolu de repertoire/fichier
    # correspondant a un champ du type nom relatif de repertoire/fichier
    # Champs concernes :
    # nom_repertoire_lot_pkgs, nom_repertoire_data,
    # nom_fichier_config_web_conf, nom_fichier_config_web_applis,
    # nom_fichier_config_dicos.
    #*************************************************************************

    def getNomAbsoluRepertoireLotPkgs(self) :
        return getNomAbsoluRepertoireLotPkgs( self.nom_repertoire_lot_pkgs )

    def getNomAbsoluRepertoireData(self) :
        return getNomAbsoluRepertoireData( self.nom_repertoire_data )

    def getNomAbsoluFichierConfigWebConf(self) :
        return getNomAbsoluFichierConfigWebConf( self.nom_fichier_config_web_conf )

    def getNomAbsoluFichierConfigWebApplis(self) :
        return getNomAbsoluFichierConfigWebApplis( self.nom_fichier_config_web_applis )

    def getNomAbsoluFichierConfigDicos(self) :
        return getNomAbsoluFichierConfigDicos( self.nom_fichier_config_dicos )

#*****************************************************************************

