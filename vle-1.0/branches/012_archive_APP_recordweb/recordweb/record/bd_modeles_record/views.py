#-*- coding:utf-8 -*-

## @file record/bd_modeles_record/views.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File bd_modeles_record/views.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
# View bd_modeles_record
#
# Visualisation des modeles record enregistres dans la bd des modeles record
# (edition read only destinee a l'utilisateur)
#
# Fait partie du processus d'exploitation des modeles record par les
# utilisateurs. L'ensemble des modeles record est affiche (information
# partielle accessible quel que soit le caractere prive/public du modele)
# et l'utilisateur peut choisir l'un d'eux pour un certain nombre d'actions
# (visualisation de la fiche de renseignement complete...), apres en avoir
# saisi le mot de passe dans le cas d'un modele prive. Les actions proposees
# en menu sont configurees dans CONF_bd_modeles_record (cf menu_actions,
# les_actions).
#
# Cas nominal (modele prive) :
#   les_modeles_record     -->  NOM_page_liste_modeles_record
#   un_modele_record_menu  <--  NOM_page_liste_modeles_record
#   un_modele_record_menu  -->  NOM_page_mdp
#   un_modele_record_mdp   <--  NOM_page_mdp
#   un_modele_record_mdp   -->  NOM_page_un_modele_record
#
# Cas nominal (modele public) :
#   les_modeles_record     -->  NOM_page_liste_modeles_record
#   un_modele_record_menu  <--  NOM_page_liste_modeles_record
#   un_modele_record_menu  -->  NOM_page_un_modele_record
#
#*****************************************************************************

from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.core.context_processors import csrf

from record.bd_modeles_record.models import ModeleRecord 
from record.bd_modeles_record.forms.user_forms import ModeleRecordFormResume
from record.bd_modeles_record.forms.user_forms import ModeleRecordFormPartiel
from record.bd_modeles_record.forms.user_forms import ModeleRecordFormComplet
from record.bd_modeles_record.forms.user_forms import MotDePasseAskingForm

from configs.conf_internationalisation import CONF_internationalisation

#from configs.conf_trace import CONF_trace
#from record.utils.trace import TraceEcran, TraceErreur

# pour traces
#t_ecr = TraceEcran(__file__,__name__,CONF_trace)
#t_err = TraceErreur(__file__,__name__,CONF_trace)

from django.utils.translation import ugettext as _

#*****************************************************************************
#
# Aspects configuration/initialisation
#
#*****************************************************************************

from record.bd_modeles_record.configs.conf_bd_modeles_record import CONF_bd_modeles_record

##..
#*****************************************************************************\n
#
# NomsPages : des constantes determinant les pages appelees. Certaines des
# valeurs sont systematiquement utilisees, d'autres sont des valeurs par
# defaut servant ou non selon les circonstances (cf CONF, les_actions)
#
#*****************************************************************************
class NomsPages(object) :

    # valeur par defaut si besoin dans les_modeles_record
    #NOM_page_liste_modeles_record = 'les_modeles_record/liste_modeles_record_tableau.html'
    NOM_page_liste_modeles_record = 'les_modeles_record/liste_modeles_record_fiches.html'

    # valeur par defaut si besoin par rapport a les_actions (cf url dans cas
    # type 'informations_completes', 'informations_partielles')
    NOM_page_un_modele_record = 'un_modele_record/un_modele_record.html'

    # invariable
    NOM_page_mdp = 'un_modele_record/mdp.html'

##..
#*****************************************************************************\n
# Modele : ChoixOperations
#
# Des constantes relatives aux choix d'operation proposes a l'utilisateur
#
# Un choix a proposer dans une page html est transmis dans le contexte
# d'appel. Valeur VAL_choix_a_montrer : le choix est propose/affiche.
#
#*****************************************************************************
class ChoixOperations(object) :

    # Valeur donnee a certains choix (transmis a une page) qui doivent etre 
    # montres/visibles
    VAL_choix_a_montrer = 'choix_a_montrer'


#*****************************************************************************
#
# Controllers
#
#*****************************************************************************

##..
#*****************************************************************************\n
# les_modeles_record
#
# Affiche tous les modeles record (informations partielles), et propose d'en
# choisir un pour differentes actions (cf menu_actions)
#
#*****************************************************************************
def les_modeles_record(request):

    # flashes
    flashes = [] # aucun message d'erreur en flashes

    # menu des actions a proposer, en totalite
    menu_actions = CONF_bd_modeles_record.menu_actions

    ( c, NOM_page_liste_modeles_record ) = fabriquer_les_modeles_record( request, menu_actions, flashes )

    return render_to_response( NOM_page_liste_modeles_record, c )

##..
#*****************************************************************************\n
# un_modele_record_menu
#
# Gestion du choix fait dans le menu des actions proposees pour chaque modele
# record. Les types d'action actuellement pris en compte sont
# 'informations_completes', 'informations_partielles',
# 'redirection' et 'redirection_modele_record_id'.
#
# Traitement du modele record choisi dans le cas du type d'action
# 'informations_completes' : affiche ses informations completes si modele
# public, demande son mot de passe si modele prive.
#
# Traitement du modele record choisi dans le cas du type d'action
# 'informations_partielles' : affiche ses informations partielles (que le
# modele soit public ou prive).
#
# Traitement du modele record choisi dans le cas du type 'redirection' :
# redirige vers l'url correspondant a action_demandee (quel que soit le
# caractere public/prive du modele).
#
# Traitement du modele record choisi dans le cas du type
# 'redirection_modele_record_id' : redirige vers l'url correspondant a
# action_demandee a laquelle est accole modele_record_id (quel que soit le
# caractere public/prive du modele).
#
#*****************************************************************************
def un_modele_record_menu(request) :

    #*************************************************************************
    # les informations en entree, initialisations
    #*************************************************************************

    modele_record_id = None # par defaut
    if "modele_record_id" in request.POST.keys() :
        modele_record_id = request.POST["modele_record_id"]

    action_demandee = None # par defaut
    if "action" in request.POST.keys() :
        action_demandee = request.POST["action"]

    # modele_record_id et modele_record_bd
    modele_record_id = int(modele_record_id)
    modele_record_bd = ModeleRecord.objects.get(id=modele_record_id)

    # type et url relatifs a action_demandee
    les_actions = CONF_bd_modeles_record.les_actions
    type = None # par defaut
    url = None # par defaut
    if action_demandee in les_actions.keys() :
        type = les_actions[action_demandee]['type']
        url = les_actions[action_demandee]['url']

    #*************************************************************************
    # traitement en fonction de l'action demandee
    #*************************************************************************

    retour_a_tous_les_modeles_record = False # par defaut

    flashes = [] # aucun message d'erreur en flashes par defaut

    if type == 'informations_completes' :

        if modele_record_bd.isPublic() : # modele public
        # un modele record (informations completes)

            # menu des actions a proposer, sans action_demandee
            menu_actions = [ m for m in CONF_bd_modeles_record.menu_actions if m.value != action_demandee ]

            # le formulaire (informations completes) du modele record a afficher
            formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

            ( c, NOM_page_un_modele_record ) = fabriquer_un_modele_record( request, formulaire_modele_record, url, menu_actions, flashes )

            return render_to_response( NOM_page_un_modele_record, c )

        else : # modele prive
        # demande du mot de passe

            c = fabriquer_demande_mot_de_passe( request, modele_record_id, action_demandee, flashes )
            return render_to_response( NomsPages.NOM_page_mdp, c )

    elif type == 'informations_partielles' :
    # un modele record (informations partielles)

        # menu des actions a proposer, sans action_demandee
        menu_actions = [ m for m in CONF_bd_modeles_record.menu_actions if m.value != action_demandee ]

        # le formulaire (informations partielles) du modele record a afficher
        formulaire_modele_record = ModeleRecordFormPartiel(instance=modele_record_bd)

        ( c, NOM_page_un_modele_record ) = fabriquer_un_modele_record( request, formulaire_modele_record, url, menu_actions, flashes )

        return render_to_response( NOM_page_un_modele_record, c )

    elif type == 'redirection' :

            if url is not None : # redirection
                NOM_url_redirection = url
                return HttpResponseRedirect( NOM_url_redirection )

            else : # reaction : retour a tous les modeles record
                flashes.append( _(u"action invalide (adresse absente)") )
                retour_a_tous_les_modeles_record = True

    elif type == 'redirection_modele_record_id' :

            if url is not None : # redirection
                if not url.endswith( '/' ) : 
                    NOM_url_redirection = url + '/' + str(modele_record_id)
                else :
                    NOM_url_redirection = url + str(modele_record_id)
                return HttpResponseRedirect( NOM_url_redirection )

            else : # reaction : retour a tous les modeles record
                flashes.append( _(u"action invalide (adresse absente)") )
                retour_a_tous_les_modeles_record = True

    else : # type inconnu/non traite
    # reaction : retour a tous les modeles record
        flashes.append( _(u"action invalide (type inconnu)") )
        retour_a_tous_les_modeles_record = True

    if retour_a_tous_les_modeles_record :
        menu_actions = CONF_bd_modeles_record.menu_actions # en totalite
        ( c, NOM_page_liste_modeles_record ) = fabriquer_les_modeles_record( request, menu_actions, flashes )
        return render_to_response( NOM_page_liste_modeles_record, c )

##..
#*****************************************************************************\n
# mot_de_passe_un_modele_record
#
# Verifie le mot de passe saisi pour le modele record choisi, avant de
# poursuivre l'action demandee pour le modele record (si bon mot de passe).
# Le seul type d'action pris en compte/gere a ce niveau est
# 'informations_completes' (affichage des informations completes du modele
# record).
#
#*****************************************************************************
def un_modele_record_mdp(request) :

    #*************************************************************************
    # les informations en entree, initialisations
    #*************************************************************************

    modele_record_id = None # par defaut
    if "modele_record_id" in request.POST.keys() :
        modele_record_id = request.POST["modele_record_id"]

    action_demandee = None # par defaut
    if "action" in request.POST.keys() :
        action_demandee = request.POST["action"]

    formulaire_mot_de_passe = MotDePasseAskingForm(request.POST)

    # modele_record_id et modele_record_bd
    modele_record_id = int(modele_record_id)
    modele_record_bd = ModeleRecord.objects.get(id=modele_record_id)

    #*************************************************************************
    # determination de deverrouillage (avec maj de flashes)
    #*************************************************************************

    # flashes, initialisation
    flashes = [] # aucun message d'erreur en flashes par defaut

    deverrouillage = False # par defaut
    if formulaire_mot_de_passe.is_valid():
    # le mot de passe a ete saisi, le verifier
        if modele_record_bd.checkPassword( formulaire_mot_de_passe.get_valeur_saisie() ) :
        # bon mot de passe
            deverrouillage = True
        else : # mauvais mot de passe, le redemander
            deverrouillage = False
            flashes = [ _(u"mauvaise saisie, nouvel essai") ] 

    #*************************************************************************
    # la suite
    #*************************************************************************

    if deverrouillage : # un modele record (informations completes)

        # type et url relatifs a action_demandee
        les_actions = CONF_bd_modeles_record.les_actions
        type = None # par defaut
        url = None # par defaut
        if action_demandee in les_actions.keys() :
            type = les_actions[action_demandee]['type']
            url = les_actions[action_demandee]['url']

        if type == 'informations_completes' : # seul type traite a ce niveau

            # menu des actions a proposer, sans action_demandee
            menu_actions = [ m for m in CONF_bd_modeles_record.menu_actions if m.value != action_demandee ]
    
            # le formulaire (informations completes) du modele record a afficher
            formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

            ( c, NOM_page_un_modele_record ) = fabriquer_un_modele_record( request, formulaire_modele_record, url, menu_actions, flashes )

            return render_to_response( NOM_page_un_modele_record, c )

        else : # tout autre type (inattendu/non traite)
        # reaction : retour a tous les modeles record

            menu_actions = CONF_bd_modeles_record.menu_actions # en totalite
            flashes = [ _(u"action invalide (type inconnu)") ] 
            ( c, NOM_page_liste_modeles_record ) = fabriquer_les_modeles_record( request, menu_actions, flashes )
            return render_to_response( NOM_page_liste_modeles_record, c )

    else : # demande du mot de passe
        c = fabriquer_demande_mot_de_passe( request, modele_record_id, action_demandee, flashes )
        return render_to_response( NomsPages.NOM_page_mdp, c )

##..
#*****************************************************************************\n
# un_modele_record
#
# Affichage des informations completes du modele record, sans
# demander/verifier son mot de passe si jamais il est prive (le faire en
# prealable !)
#
# Cette methode ne tient pas compte de la configuration CONF_bd_modeles_record
# concernant les actions proposees (pas d'action proposee), ni
# NOM_page_un_modele_record (valeur par defaut). Elle ne recoit/traite pas non
# plus de parametre action_demandee.
#
#*****************************************************************************
def un_modele_record(request) :

    #*************************************************************************
    # les informations en entree, initialisations
    #*************************************************************************

    modele_record_id = None # par defaut
    if "modele_record_id" in request.POST.keys() :
        modele_record_id = request.POST["modele_record_id"]

    # modele_record_id
    modele_record_id = int(modele_record_id)

    ( c, NOM_page_un_modele_record ) = fabriquer_un_modele_record( request, modele_record_id, url=None, menu_actions=[], flashes=[] )

    return render_to_response( NOM_page_un_modele_record, c )


#*****************************************************************************
#
# Methode de fabrication d'appels des views
#
#*****************************************************************************

##..
#*****************************************************************************\n
# Methode fabriquer_les_modeles_record :
#
# construit et retourne le contexte d'appel de NOM_page_liste_modeles_record
#
#*****************************************************************************
def fabriquer_les_modeles_record( request, menu_actions, flashes ) :
     
    # les formulaires des modeles record a afficher
    liste_modeles_record_bd = ModeleRecord.objects.all()
    formulaires_modeles_record = []
    for modele_record_bd in liste_modeles_record_bd :
        formulaires_modeles_record.append( ModeleRecordFormResume(instance=modele_record_bd) )

    c = {} # le contexte

    c['formulaires_modeles_record'] = formulaires_modeles_record

    c['menu_actions'] = menu_actions # optionnel

    c['flashes'] = flashes # optionnel

    # menu_langage optionnel
    if CONF_internationalisation.menu_langage_bd_modeles_record :
        c['menu_langage'] = ChoixOperations.VAL_choix_a_montrer
    # else : menu_langage non transmis
    c['choix_a_montrer'] = ChoixOperations.VAL_choix_a_montrer # valeur de comparaison

    c.update(csrf(request))

    NOM_page_liste_modeles_record = CONF_bd_modeles_record.NOM_page_liste_modeles_record
    if NOM_page_liste_modeles_record == '' :
        NOM_page_liste_modeles_record = NomsPages.NOM_page_liste_modeles_record

    return ( c, NOM_page_liste_modeles_record )

##..
#*****************************************************************************\n
# Methode fabriquer_un_modele_record
#
# construit et retourne le contexte d'appel de NOM_page_un_modele_record
#
#*****************************************************************************
def fabriquer_un_modele_record( request, formulaire_modele_record, url, menu_actions, flashes ) :

    c = {} # le contexte

    c['formulaire_modele_record'] = formulaire_modele_record
    c['menu_actions'] = menu_actions # optionnel
    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    NOM_page_un_modele_record = url
    if NOM_page_un_modele_record is None :
        NOM_page_un_modele_record = NomsPages.NOM_page_un_modele_record

    return ( c, NOM_page_un_modele_record )

##..
#*****************************************************************************\n
# Methode fabriquer_demande_mot_de_passe
#
# construit et retourne le contexte d'appel de NOM_page_mdp
#
#*****************************************************************************
def fabriquer_demande_mot_de_passe( request, modele_record_id, action_demandee, flashes ) :
     
    # Formulaire de saisie du mot de passe (du modele record prive)
    formulaire_mot_de_passe = MotDePasseAskingForm()
    # informations complementaires (a faire suivre)
    formulaire_mot_de_passe.action_demandee = action_demandee
    formulaire_mot_de_passe.modele_record_id = modele_record_id
            
    # rappel des infos partielles a titre d'information/memo
    modele_record_bd = ModeleRecord.objects.get(id=modele_record_id)
    formulaire_modele_record = ModeleRecordFormPartiel(instance=modele_record_bd)

    c = {} # le contexte

    c['formulaire_mot_de_passe'] = formulaire_mot_de_passe
    c['formulaire_modele_record'] = formulaire_modele_record
    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

#*****************************************************************************

