###############################################################################
#                                                                             #
#         INSTALLATION du projet web nom_du_projet_web                        #
#         sur machine de production sous /opt/nom_du_projet_web               #
#         sur serveur Apache (ou Django, local)                               #
#                                                                             #
###############################################################################

On appelle 'espace web' l'emplacement/repertoire partage par tous les projets web installes (ici /opt)

Un 'projet web' correspond a une installation { rmlib, rwtool, rwsite, et leurs bd associees (la bd des modeles et la bd du site) } ie pour l'instant { recordweb, recordweb_depot, bd_modeles_record_directory }  

Pour l'INSTALLATION/CONFIGURATION de l'environnement serveur d'hebergement de projet web : voir install_serveur.txt.

                                        ***************************************
                                        * MEMO proprietaires :
                                        *  /opt     : root
                                        *  /var/www : www-data (Apache)
                                        *  chown -R www-data:www-data /opt
                                        *  chown -R nrousse /opt
                                        ***************************************

#******************************************************************************
login :
- sur VM (webrecord) de mise en production : ssh nrousse@webrecord
- sur VM (webrecordtest) des essais avant mise en production : vmplayer ; login nrousse

#******************************************************************************
Depot dans l'espace de travail/preparation ($HOME) du code a installer :

cd /home/nrousse

( vendor (code externe utilise/appele) :
  svn export svn+ssh://rousse@scm.mulcyber.toulouse.inra.fr/svnroot/recordweb/vendor/recordweb_vendor recordweb_vendor )

--------- debut propre au projet web nom_du_projet_web :

Pour nom_du_projet_web, telechargement du code entierement a partir depot svn recordweb (rien ne vient du depot svn recordwebdepot) :

- recordweb (code outil_web_record, bd_modeles_record...) :
svn export svn+ssh://rousse@scm.mulcyber.toulouse.inra.fr/svnroot/recordweb/trunk/recordweb recordweb

- recordweb_depot (depot de l'outil_web_record : le repertoire dedie configuration/personnalisation et le repertoire pkgs) :
svn export svn+ssh://rousse@scm.mulcyber.toulouse.inra.fr/svnroot/recordweb/trunk/recordweb_depot recordweb_depot

- bd des modeles record :
svn export svn+ssh://rousse@scm.mulcyber.toulouse.inra.fr/svnroot/recordweb/trunk/bd_modeles_record_directory bd_modeles_record_directory

   ### demonstrateur_prive
   (telechargement des parties propres a demonstrateur_prive a partir depot svn recordwebdepot)

   svn export svn+ssh://rousse@scm.mulcyber.toulouse.inra.fr/svnroot/recordweb/trunk/recordweb recordweb
   (... attention a recordweb/rwsite)
   svn export svn+ssh://rousse@scm.mulcyber.toulouse.inra.fr/svnroot/recordwebdepot/trunk/recordweb_depot recordweb_depot
   svn checkout svn+ssh://rousse@scm.mulcyber.toulouse.inra.fr/svnroot/recordwebdepot/trunk/demonstrateur_prive demonstrateur_prive 

         ######### maj (svn commit) depuis la version mise en prod
         #
         # demonstrateur_prive pour bd_modeles_record_directory et pour rwsite, en commande checkout pour ensuite permettre mises a jour (des bd modeles et site, de media du site) de demonstrateur_prive a partir de sa version mise en production :
         #
      
         # commit :
         prealable : sudo chown -R nrousse /opt
      
         commit de la bd des modeles record :
         cd /opt/demonstrateur_prive/demonstrateur_prive/bd_modeles_record_directory
         svn commit bd_modeles_record.db -m"maj depuis machine de prod"
      
         commit de la bd du site :
         cd /opt/demonstrateur_prive/demonstrateur_prive/rwsite
         svn commit bd_site_web_record.db -m"maj depuis machine de prod"
      
         commit de media (ses fichiers) du site :
         cd /opt/demonstrateur_prive/demonstrateur_prive/rwsite/media/uploads/files
         svn add NOMFICHIERAJOUTE
         cd /opt/demonstrateur_prive/demonstrateur_prive/rwsite
         svn commit media -m"COMMENT"
      
         retablissement : sudo chown -R www-data:www-data /opt
         #
         ######### maj depuis la version mise en prod

   ###

   ### demonstrateur_public
   (telechargement des parties propres a demonstrateur_public a partir depot svn recordwebdepot)

   svn export svn+ssh://rousse@scm.mulcyber.toulouse.inra.fr/svnroot/recordweb/trunk/recordweb recordweb
   (... attention a recordweb/rwsite)
   svn export svn+ssh://rousse@scm.mulcyber.toulouse.inra.fr/svnroot/recordwebdepot/trunk/recordweb_depot recordweb_depot
   svn checkout svn+ssh://rousse@scm.mulcyber.toulouse.inra.fr/svnroot/recordwebdepot/trunk/demonstrateur_public demonstrateur_public 

         ######### maj (svn commit) depuis la version mise en prod
         #
         # demonstrateur_public pour bd_modeles_record_directory et pour rwsite, en commande checkout pour ensuite permettre mises a jour (des bd modeles et site, de media du site) de demonstrateur_public a partir de sa version mise en production :
         #
      
         # commit :
         prealable : sudo chown -R nrousse /opt
      
         commit de la bd des modeles record :
         cd /opt/demonstrateur_public/demonstrateur_public/bd_modeles_record_directory
         svn commit bd_modeles_record.db -m"maj depuis machine de prod"
      
         commit de la bd du site :
         cd /opt/demonstrateur_public/demonstrateur_public/rwsite
         svn commit bd_site_web_record.db -m"maj depuis machine de prod"
      
         commit de media (ses fichiers) du site :
         cd /opt/demonstrateur_public/demonstrateur_public/rwsite/media/uploads/files
         svn add NOMFICHIERAJOUTE
         cd /opt/demonstrateur_public/demonstrateur_public/rwsite
         svn commit media -m"COMMENT"
      
         retablissement : sudo chown -R www-data:www-data /opt
         #
         ######### maj depuis la version mise en prod

   ###

--------- fin propre au projet web nom_du_projet_web.

(le code sera ensuite copie sous son emplacement d'exploitation /opt/...)

#******************************************************************************
Compilation des modeles record  (sous $HOME/.vle) :

('vle' -> creation de /home/nrousse/.vle)
cd /home/nrousse/.vle

pour chq lot de modele record :
  rm -f pkgs
  ln -s ../recordweb_depot/pkgs_recordweb/vle-1.0/lot_wwdm pkgs
  vle -P meteo configure build
  vle -P wwdm configure build

#******************************************************************************
Copie sous emplacement d'exploitation /opt/... :

prealable : chown -R nrousse /opt

cd /home/nrousse

( plus besoin de copier recordweb_vendor sous /opt ; a remettre si jamais installation particuliere de certaines applications/librairies (cf RECORDWEB_VENDOR dans PYTHON_PATH) )

--------- debut propre au projet web nom_du_projet_web :

mkdir /opt/nom_du_projet_web
   ### demonstrateur_prive
   mkdir /opt/demonstrateur_prive
   ###
   ### demonstrateur_public
   mkdir /opt/demonstrateur_public
   ###

Des specificites de demonstrateur_prive et demonstrateur_public :
   ### demonstrateur_prive
   mv demonstrateur_prive /opt/demonstrateur_prive/.
   (-> /opt/demonstrateur_prive/demonstrateur_prive/bd_modeles_record_directory
    et /opt/demonstrateur_prive/demonstrateur_prive/rwsite)
   ###
   ### demonstrateur_public
   mv demonstrateur_public /opt/demonstrateur_public/.
   (-> /opt/demonstrateur_public/demonstrateur_public/bd_modeles_record_directory
   (et /opt/demonstrateur_public/demonstrateur_public/rwsite)
   ###

cp -fr recordweb /opt/nom_du_projet_web/.

   ### demonstrateur_prive
   cp -fr recordweb /opt/demonstrateur_prive/.
   et rwsite :
   rm -fr /opt/demonstrateur_prive/recordweb/rwsite
   ln -s /opt/demonstrateur_prive/demonstrateur_prive/rwsite /opt/demonstrateur_prive/recordweb/rwsite
   ###

   ### demonstrateur_public
   cp -fr recordweb /opt/demonstrateur_public/.
   et rwsite :
   rm -fr /opt/demonstrateur_public/recordweb/rwsite
   ln -s /opt/demonstrateur_public/demonstrateur_public/rwsite /opt/demonstrateur_public/recordweb/rwsite
   ###

cp -fr recordweb_depot /opt/nom_du_projet_web/.
remarque : il est possible de ne laisser dans la version de recordweb_depot situee sous /opt que ce qui est necessaire a l'exploitation des modeles record, autrement dit de supprimer les repertoires (src, build, cmake) des paquets vle et d'en garder les repertoires (lib, exp, data).

   ### demonstrateur_prive
   cp -fr recordweb_depot /opt/demonstrateur_prive/.
   ###
   ### demonstrateur_public
   cp -fr recordweb_depot /opt/demonstrateur_public/.
   + enlever de /opt/demonstrateur_public/recordweb_depot les modeles 'hors version publique'
   ###

cp -fr bd_modeles_record_directory /opt/nom_du_projet_web/.

   ### demonstrateur_prive
   ln -s /opt/demonstrateur_prive/demonstrateur_prive/bd_modeles_record_directory /opt/demonstrateur_prive/.
   ###
   ### demonstrateur_public
   ln -s /opt/demonstrateur_public/demonstrateur_public/bd_modeles_record_directory /opt/demonstrateur_public/.
   ###

--------- fin propre au projet web nom_du_projet_web.

#******************************************************************************
Preparation VLE_HOME (repertoire racine des VLE_HOME propres aux sessions/connexions) :

--------- debut propre au projet web nom_du_projet_web :

mkdir /opt/nom_du_projet_web/VLE
   ### demonstrateur_prive
   mkdir /opt/demonstrateur_prive/VLE
   ###
   ### demonstrateur_public
   mkdir /opt/demonstrateur_public/VLE
   ###
--------- fin propre au projet web nom_du_projet_web.

#******************************************************************************

--------- debut propre au projet web nom_du_projet_web :
#******************************************************************************
Modification/adaptation du code 'settings.py'
/opt/nom_du_projet_web/recordweb/rmlib,rwtool,rwsite/settings.py :
   ### demonstrateur_prive
   /opt/demonstrateur_prive/recordweb/rmlib,rwtool,rwsite/settings.py :
   ###
   ### demonstrateur_public
   /opt/demonstrateur_public/recordweb/rmlib,rwtool,rwsite/settings.py :
   ###

configurer EN_PRODUCTION, MACHINE_PROD et SERVEUR_DJANGO :
- garder le cas voulu : 'sur VM de production (webrecord)' 
- et mettre les 2 autres cas en commentaire : 'sur VM d'essai (webrecordtest)', 'en developpement (travail local)'
S'il s'agit des essais (webrecordtest) avant mise en production : inverser ci-dessus 'VM de production' et 'VM d'essai'

      (pour memo : SERVEUR_DJANGO=False => DEBUG=False)

configurer pour nom_du_projet_web :
- espace_projet_web
- debut_url_rwtool

#******************************************************************************
Modification/adaptation du code 'urls.py'
/opt/nom_du_projet_web/recordweb/rwtool/urls.py :
   ### demonstrateur_prive
   /opt/demonstrateur_prive/recordweb/rwtool/urls.py :
   ###
   ### demonstrateur_public
   /opt/demonstrateur_public/recordweb/rwtool/urls.py :
   ###

Pour empecher/desactiver la possibilite d'administration de la bd des modeles record depuis l'outil web record (ie via rwtool et obliger ainsi a passer par rmlib) :
- mettre en commentaire les lignes 'admin.site.urls'

#******************************************************************************
Generation documentation (doxygen) du code source :

documentation sous /opt/nom_du_projet_web/recordweb/doxygen_genere :
cd /opt/nom_du_projet_web/recordweb
doxygen doxygen_config.cfg

documentation sous /opt/nom_du_projet_web/recordweb/record/site_media/doxygen_genere
!!! a effectuer avant generations STATIC_ROOT :
cd /opt/nom_du_projet_web/recordweb/record
doxygen doxygen_config.cfg

   ### demonstrateur_prive
   cd /opt/demonstrateur_prive/recordweb
   doxygen doxygen_config.cfg
   cd /opt/demonstrateur_prive/recordweb/record
   doxygen doxygen_config.cfg
   ###
   ### demonstrateur_public
   cd /opt/demonstrateur_public/recordweb
   doxygen doxygen_config.cfg
   cd /opt/demonstrateur_public/recordweb/record
   doxygen doxygen_config.cfg
   ###

#******************************************************************************

Generations STATIC_ROOT de chaque projet django (rmlib,rwtool,rwsite) :
cd /opt/nom_du_projet_web/recordweb/rmlib
python manage.py collectstatic
cd /opt/nom_du_projet_web/recordweb/rwtool
python manage.py collectstatic
cd /opt/nom_du_projet_web/recordweb/rwsite
python manage.py collectstatic

   ### demonstrateur_prive
   cd /opt/demonstrateur_prive/recordweb/rmlib
   python manage.py collectstatic
   cd /opt/demonstrateur_prive/recordweb/rwtool
   python manage.py collectstatic
   cd /opt/demonstrateur_prive/recordweb/rwsite
   python manage.py collectstatic
   ###
   ### demonstrateur_public
   cd /opt/demonstrateur_public/recordweb/rmlib
   python manage.py collectstatic
   cd /opt/demonstrateur_public/recordweb/rwtool
   python manage.py collectstatic
   cd /opt/demonstrateur_public/recordweb/rwsite
   python manage.py collectstatic
   ###

!!! attention, il faut avoir genere la documentation doxygen du code source 
    (rangee dans site_media) avant

#******************************************************************************

Generation/construction de tous les translation files :
cd /opt/nom_du_projet_web/recordweb 
./bpo.sh

   ### demonstrateur_prive
   cd /opt/demonstrateur_prive/recordweb 
   ./bpo.sh
   ###
   ### demonstrateur_public
   cd /opt/demonstrateur_public/recordweb 
   ./bpo.sh
   ###

#******************************************************************************
--------- fin propre au projet web nom_du_projet_web.


#******************************************************************************
#******************************************************************************
#              Fonctionnement sur serveur Apache :
#******************************************************************************
#******************************************************************************

#******************************************************************************
*******************************************************************************
Les fichiers specifiques serveur :

--------- debut propre au projet web nom_du_projet_web :

Fichier wsgi.py de chaque projet django :
cp /opt/nom_du_projet_web/recordweb/specifique_serveur/fichiers_specifiques_serveur/rwtool/wsgi.py /opt/nom_du_projet_web/recordweb/rwtool/.
cp /opt/nom_du_projet_web/recordweb/specifique_serveur/fichiers_specifiques_serveur/rmlib/wsgi.py /opt/nom_du_projet_web/recordweb/rmlib/.
cp /opt/nom_du_projet_web/recordweb/specifique_serveur/fichiers_specifiques_serveur/rwsite/wsgi.py /opt/nom_du_projet_web/recordweb/rwsite/.

   ### demonstrateur_prive
   cp /opt/demonstrateur_prive/recordweb/specifique_serveur/fichiers_specifiques_serveur/rwtool/wsgi.py /opt/demonstrateur_prive/recordweb/rwtool/.
   cp /opt/demonstrateur_prive/recordweb/specifique_serveur/fichiers_specifiques_serveur/rmlib/wsgi.py /opt/demonstrateur_prive/recordweb/rmlib/.
   cp /opt/demonstrateur_prive/recordweb/specifique_serveur/fichiers_specifiques_serveur/rwsite/wsgi.py /opt/demonstrateur_prive/recordweb/rwsite/.
   ###
   ### demonstrateur_public
   cp /opt/demonstrateur_public/recordweb/specifique_serveur/fichiers_specifiques_serveur/rwtool/wsgi.py /opt/demonstrateur_public/recordweb/rwtool/.
   cp /opt/demonstrateur_public/recordweb/specifique_serveur/fichiers_specifiques_serveur/rmlib/wsgi.py /opt/demonstrateur_public/recordweb/rmlib/.
   cp /opt/demonstrateur_public/recordweb/specifique_serveur/fichiers_specifiques_serveur/rwsite/wsgi.py /opt/demonstrateur_public/recordweb/rwsite/.
   ###

Fichier apache 'default' de chaque projet django (tournant simultanement) :
sudo cp /opt/nom_du_projet_web/recordweb/specifique_serveur/fichiers_specifiques_serveur/rwtool/default_rwtool_nom_du_projet_web /etc/apache2/sites-available/.
sudo cp /opt/nom_du_projet_web/recordweb/specifique_serveur/fichiers_specifiques_serveur/rmlib/default_rmlib_nom_du_projet_web /etc/apache2/sites-available/.
sudo cp /opt/nom_du_projet_web/recordweb/specifique_serveur/fichiers_specifiques_serveur/rwsite/default_rwsite_nom_du_projet_web /etc/apache2/sites-available/.

   ### demonstrateur_prive
   sudo cp /opt/demonstrateur_prive/recordweb/specifique_serveur/fichiers_specifiques_serveur/rwtool/default_rwtool_demonstrateur_prive /etc/apache2/sites-available/.
   sudo cp /opt/demonstrateur_prive/recordweb/specifique_serveur/fichiers_specifiques_serveur/rmlib/default_rmlib_demonstrateur_prive /etc/apache2/sites-available/.
   sudo cp /opt/demonstrateur_prive/recordweb/specifique_serveur/fichiers_specifiques_serveur/rwsite/default_rwsite_demonstrateur_prive /etc/apache2/sites-available/.
   ###
   ### demonstrateur_public
   sudo cp /opt/demonstrateur_public/recordweb/specifique_serveur/fichiers_specifiques_serveur/rwtool/default_rwtool_demonstrateur_public /etc/apache2/sites-available/.
   sudo cp /opt/demonstrateur_public/recordweb/specifique_serveur/fichiers_specifiques_serveur/rmlib/default_rmlib_demonstrateur_public /etc/apache2/sites-available/.
   sudo cp /opt/demonstrateur_public/recordweb/specifique_serveur/fichiers_specifiques_serveur/rwsite/default_rwsite_demonstrateur_public /etc/apache2/sites-available/.
   ###

--------- fin propre au projet web nom_du_projet_web.

#******************************************************************************
Modification/adaptation de code apache :

Verifier, dans le fichier /etc/apache2/ports.conf, que le serveur Apache2 ecoute les ports choisis pour les projets django rwsite,rwtool,rmlib du projet web nom_du_projet_web :

    # If you just change the port or add more ports here, you will likely also
    # have to change the VirtualHost statement in
    # /etc/apache2/sites-enabled/000-default
    # This is also true if you have upgraded from before 2.2.9-3 (i.e. from
    # Debian etch). See /usr/share/doc/apache2.2-common/NEWS.Debian.gz and
    # README.Debian.gz
    
    NameVirtualHost *:80
    Listen 80
    Listen xxxx    # port de rwsite du projet web nom_du_projet_web
    Listen yyyy    # port de rwtool du projet web nom_du_projet_web
    Listen zzzz    # port de rmlib  du projet web nom_du_projet_web
    ...
    <IfModule mod_ssl.c>
        # SSL name based virtual hosts are not yet supported, therefore no
        # NameVirtualHost statement here
        Listen 443
    </IfModule>

    ### demonstrateur_public
    Listen 80 # 24010 # demonstrateur_public_rwsite
    Listen 24000      # demonstrateur_public_rwtool
    Listen 24020      # demonstrateur_public_rmlib
    ###
    ### demonstrateur_prive
    Listen 24012 # demonstrateur_prive_rwsite
    Listen 24002 # demonstrateur_prive_rwtool
    Listen 24022 # demonstrateur_prive_rmlib
    ###

(il faut un port par projet django rmlib, rwtool, rwsite installe,
 ie par fichier /etc/apache2/sites-available/default_... active)

Verifier, dans le fichier /etc/apache2/apache2.conf, une requete par process ie MaxRequestsPerChild=1 (pour bonne gestion de la variable d'environnement VLE_HOME) :

    ##
    ## Server-Pool Size Regulation (MPM specific)
    ##

    # prefork MPM
    # StartServers: number of server processes to start
    # MinSpareServers: minimum number of server processes which are kept spare
    # MaxSpareServers: maximum number of server processes which are kept spare
    # MaxClients: maximum number of server processes allowed to start
    # MaxRequestsPerChild: maximum number of requests a server process serves
    #
    # l'original :
    #<IfModule mpm_prefork_module>
    #    StartServers          5
    #    MinSpareServers       5
    #    MaxSpareServers      10
    #    MaxClients          150
    #    MaxRequestsPerChild   0
    #</IfModule>
    #
    # une requete par process ie MaxRequestsPerChild=1 (pour bonne gestion de la variable d'environnement VLE_HOME) :
    <IfModule mpm_prefork_module>
        StartServers          5
        MinSpareServers       5
        MaxSpareServers      10
        MaxClients          150
        MaxRequestsPerChild   1
    </IfModule>

#******************************************************************************
Commandes de lancement sur serveur Apache :

prealable :
chown -R www-data:www-data /opt

en tant que root :
- sur VM (webrecord) de mise en production : ssh root@webrecord ou sudo ...
- sur VM (webrecordtest) des essais avant mise en production : sudo ...

(activer un projet django :    sudo a2ensite  default_...)
(desactiver un projet django : sudo a2dissite default_...)

desactiver le projet django par defaut :
sudo a2dissite default

--------- debut propre au projet web nom_du_projet_web :

pour activer tout le projet web nom_du_projet_web
(ses 3 projets django rmlib, rwtool, rwsite simultanement) :
sudo a2ensite default_rwsite_nom_du_projet_web
sudo a2ensite default_rwtool_nom_du_projet_web
sudo a2ensite default_rmlib_nom_du_projet_web
sudo /etc/init.d/apache2 reload

pour desactiver tout le projet web nom_du_projet_web :
sudo a2dissite default_rwsite_nom_du_projet_web
sudo a2dissite default_rwtool_nom_du_projet_web
sudo a2dissite default_rmlib_nom_du_projet_web
sudo /etc/init.d/apache2 reload

   ### demonstrateur_prive
   sudo a2ensite default_rwsite_demonstrateur_prive
   sudo a2ensite default_rwtool_demonstrateur_prive
   sudo a2ensite default_rmlib_demonstrateur_prive
   sudo a2dissite default_rwsite_demonstrateur_prive
   sudo a2dissite default_rwtool_demonstrateur_prive
   sudo a2dissite default_rmlib_demonstrateur_prive
   ###
   ### demonstrateur_public
   sudo a2ensite default_rwsite_demonstrateur_public
   sudo a2ensite default_rwtool_demonstrateur_public
   sudo a2ensite default_rmlib_demonstrateur_public
   sudo a2dissite default_rwsite_demonstrateur_public
   sudo a2dissite default_rwtool_demonstrateur_public
   sudo a2dissite default_rmlib_demonstrateur_public
   ###

   ### demonstrateur_prive
   sudo a2ensite default_rwsite_demonstrateur_prive
   sudo a2ensite default_rwtool_demonstrateur_prive
   sudo a2ensite default_rmlib_demonstrateur_prive
   ###
   ### demonstrateur_public
   sudo a2ensite default_rwsite_demonstrateur_public
   sudo a2ensite default_rwtool_demonstrateur_public
   sudo a2ensite default_rmlib_demonstrateur_public
   ###
   sudo /etc/init.d/apache2 reload
   ###

--------- fin propre au projet web nom_du_projet_web.

    !!!! --------------------------------------------------
    Fichier /etc/hosts local  :
    par rapport a VM (webrecord) de mise en production : rien
    par rapport a VM (webrecordtest) des essais avant mise en production : ligne " 147.99.96.72 webrecord.toulouse.inra.fr "
    !!!! --------------------------------------------------

sudo /etc/init.d/apache2 stop
sudo /etc/init.d/apache2 start
sudo /etc/init.d/apache2 restart
sudo /etc/init.d/apache2 reload
sudo /etc/init.d/apache2 force-reload

-----------------------------------------------------------
sur VM de mise en production (webrecord, http://147.99.96.155) ou
sur VM des essais avant mise en production (webrecordtest, http://147.99.96.72) :

--------- debut propre au projet web nom_du_projet_web :

*** rwsite (nom_du_projet_web_rwsite sur port xxxx) :
http://webrecord.toulouse.inra.fr:xxxx
http://webrecord.toulouse.inra.fr:xxxx/admin

*** rwtool (nom_du_projet_web_rwtool sur port yyyy) :
http://webrecord.toulouse.inra.fr:yyyy
http://webrecord.toulouse.inra.fr:yyyy/rwtool/usr
(http://webrecord.toulouse.inra.fr:yyyy/rwtool/adm desactive)

*** rmlib (nom_du_projet_web_rmlib sur port zzzz) :
http://webrecord.toulouse.inra.fr:zzzz
http://webrecord.toulouse.inra.fr:zzzz/rmlib/usr
http://webrecord.toulouse.inra.fr:zzzz/rmlib/adm

--------- fin propre au projet web nom_du_projet_web.

   ### demonstrateur_prive
   *** rwsite (demonstrateur_prive_rwsite) :
   http://webrecord.toulouse.inra.fr:24012
   http://webrecord.toulouse.inra.fr:24012/admin

   *** rwtool (demonstrateur_prive_rwtool) :
   http://webrecord.toulouse.inra.fr:24002
   http://webrecord.toulouse.inra.fr:24002/rwtool/usr
   (http://webrecord.toulouse.inra.fr:24002/rwtool/adm desactive)

   *** rmlib (demonstrateur_prive_rmlib) :
   http://webrecord.toulouse.inra.fr:24022
   http://webrecord.toulouse.inra.fr:24022/rmlib/usr
   http://webrecord.toulouse.inra.fr:24022/rmlib/adm
   ###

   ### demonstrateur_public
   *** rwsite (demonstrateur_public_rwsite) :
   (http://webrecord.toulouse.inra.fr)
   http://webrecord.toulouse.inra.fr:80       # 24010
   http://webrecord.toulouse.inra.fr:80/admin # 24010/admin

   *** rwtool (demonstrateur_public_rwtool) :
   http://webrecord.toulouse.inra.fr:24000
   http://webrecord.toulouse.inra.fr:24000/rwtool/usr
   (http://webrecord.toulouse.inra.fr:24000/rwtool/adm desactive)

   *** rmlib (demonstrateur_public_rmlib) :
   http://webrecord.toulouse.inra.fr:24020
   http://webrecord.toulouse.inra.fr:24020/rmlib/usr
   http://webrecord.toulouse.inra.fr:24020/rmlib/adm
   ###

-----------------------------------------------------------

/var/log/apache2/error.log

ps -f -u www-data
netstat -a -t -p

#******************************************************************************

#******************************************************************************
#******************************************************************************
#        Fonctionnement sur serveur Django (de developpement, local) :
#******************************************************************************
#******************************************************************************

#******************************************************************************
Modification/adaptation de code : 

dans rmlib,rwtool,rwsite/settings.py : SERVEUR_DJANGO = True

#******************************************************************************
Commandes de lancement sur serveur Django :

prealable : chown -R nrousse /opt

*** rwsite :
python manage.py syncdb
python manage.py runserver
=>
http://127.0.0.1:8000/

*** rwtool (prive) :
python manage.py syncdb
python manage.py runserver 24002
=>
http://127.0.0.1:24002/

*** rwtool (public) :
python manage.py syncdb
python manage.py runserver 24000
=>
http://127.0.0.1:24000/

*** rmlib :
python manage.py syncdb
python manage.py runserver 240XX
=>
http://127.0.0.1:240XX/

#******************************************************************************

