
## @file rwsite/urls.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File urls.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

from django.conf.urls.defaults import patterns, include, url
from django.conf.urls.defaults import *
from django.conf import settings

# to enable the admin:
from django.contrib import admin
admin.autodiscover()

#*****************************************************************************

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'rwsite.views.home', name='home'),
    # url(r'^rwsite/', include('rwsite.foo.urls')),

    # internationalisation traductions
    (r'^i18n/', include('django.conf.urls.i18n')),
)

urlpatterns += patterns('',

    # django-fiber (+ voir a la fin)
    (r'^api/v2/', include('fiber.rest_api.urls')),
    (r'^api/v1/', include('fiber.rest_api.urls')), #(r'^api/v1/', include('fiber.api.urls')),
    (r'^admin/fiber/', include('fiber.admin_urls')),
    (r'^jsi18n/$', 'django.views.i18n.javascript_catalog', {'packages': ('fiber',),}),
)

urlpatterns += patterns('',

    # partie administration : administration de la bd du site record web

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^admin/', include(admin.site.urls)),
    #???url(r'^rwsite/adm/',   include(admin.site.urls)),
    #???url(r'^rwsite/admin/', include(admin.site.urls)), # synonyme

)

if settings.DEBUG:
    urlpatterns += patterns('',

        #??? static files
        #(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT }),

        # media files
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT,}),
    )

#*****************************************************************************

urlpatterns += patterns('',
    (r'', 'fiber.views.page'), # this should always be placed last
)

#*****************************************************************************

