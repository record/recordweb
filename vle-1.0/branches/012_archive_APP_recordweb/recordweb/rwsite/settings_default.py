#-*- coding:utf-8 -*-

## @file rwsite/settings_default.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File settings_default.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

import os

import django.conf.global_settings as DEFAULT_SETTINGS

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
print "[rwsite/settings_default.py] BASE_DIR = ", BASE_DIR

DEBUG = True
TEMPLATE_DEBUG = DEBUG

#*****************************************************************************
# Configuration recueil de statistiques sur le site
#*****************************************************************************

#************* avec django-request (statistics module) ***********************
# See http://django-request.readthedocs.org/en/latest/settings.html#settings

#REQUEST_IGNORE_AJAX
#REQUEST_IGNORE_IP = ( '147.99.96.95',)
#REQUEST_IGNORE_USERNAME
#REQUEST_IGNORE_PATHS
REQUEST_TRAFFIC_MODULES = (
    #'request.traffic.Ajax', # to show the amount of requests made from javascript
    #'request.traffic.NotAjax', # to show the amount of requests that are NOT made from javascript
    'request.traffic.UniqueVisitor', # to show the amount of requests made from unique visitors based upon IP address
    'request.traffic.UniqueVisit', # to show visits based from outsider referrals
    'request.traffic.Hit', # to show the total amount of requests
    'request.traffic.Search', # to display requests from search engines
    #'request.traffic.Secure', # to show the amount of requests over SSL
    #'request.traffic.Unsecure', # to show the amount of requests NOT over SSL
    #'request.traffic.User', # to show the amount of requests made from a valid user account
    #'request.traffic.UniqueUser', # to show the amount of users
    'request.traffic.Error', # to show the amount of error’s, this includes error 500 and page not found
    'request.traffic.Error404', # to show the amount of page not found
    )
#REQUEST_PLUGINS
#REQUEST_BASE_URL
#REQUEST_ONLY_ERRORS

#*****************************************************************************

# ADMIN for 500 errors
ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)

# MANAGERS for 404 errors
MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': '',                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# directories where Django looks for translation files
# Actuellement un seul dictionnaire utilise/renseigne : celui du site (projet rwsite)
LOCALE_PATHS = ( os.path.join(BASE_DIR, 'locale') )
print "[rwsite/settings_default.py] LOCALE_PATHS = ", LOCALE_PATHS

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
#TIME_ZONE = 'America/Chicago'
TIME_ZONE = 'Europe/Paris'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
# enlever 'django.middleware.locale.LocaleMiddleware' pour que LANGUAGE_CODE soit actif
# LANGUAGE_CODE : default language in/for the site
#LANGUAGE_CODE = 'en-us'
LANGUAGE_CODE = 'fr'
#LANGUAGE_CODE = 'en'

# TRANSMETA_DEFAULT_LANGUAGE : default language to transmeta
TRANSMETA_DEFAULT_LANGUAGE = 'fr'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
# attention ! si USE_L10N = True, alors en fr float avec ',' au lieu de '.'
USE_L10N = False

# pour internationalisation traductions : redefinition LANGUAGES a laisser ?
_ = lambda s: s
LANGUAGES = (
    ('fr', _(u'Français')),
    ('en', _(u'Anglais')),
)

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
print "[rwsite/settings_default.py] MEDIA_ROOT = ", MEDIA_ROOT

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
print "[rwsite/settings_default.py] STATIC_ROOT = ", STATIC_ROOT

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = STATIC_URL + 'admin/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = DEFAULT_SETTINGS.STATICFILES_FINDERS + (
    'compressor.finders.CompressorFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '%-$o$r0p4xqkwx)w$$r2-r^s9%%a^7$d76ygv2s+2*lxv+g+yc'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = DEFAULT_SETTINGS.MIDDLEWARE_CLASSES + (
    'fiber.middleware.ObfuscateEmailAddressMiddleware',
    'fiber.middleware.AdminPageMiddleware',

    'django.middleware.csrf.CsrfViewMiddleware', # besoin pour internationalisation ?
    'django.contrib.sessions.middleware.SessionMiddleware', # besoin pour internationalisation ?
    'django.middleware.locale.LocaleMiddleware', # internationalisation traductions (doit etre apres SessionMiddleware) (Autodetect user language (from browser) or use session defined language (or fallback to project default)) : en commentaire pour que LANGUAGE_CODE soit actif

    'request.middleware.RequestMiddleware', # for django-request # a placer apres django.contrib.auth, avant django.contrib.flatpages
)

ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

TEMPLATE_CONTEXT_PROCESSORS = DEFAULT_SETTINGS.TEMPLATE_CONTEXT_PROCESSORS + (
    'django.core.context_processors.request',
    'fiber.context_processors.page_info',
    'django.core.context_processors.i18n', # internationalisation traductions
)

INSTALLED_APPS = (

    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'django.contrib.admin', # to enable the admin

    'transmeta',

    # Uncomment the next line for django-request statistics module :
    'request',

    'piston',       # pour django-fiber
    'mptt',         # pour django-fiber
    'compressor',   # pour django-fiber
    'pages',        # pour django-fiber
    'fiber',        # pour django-fiber
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

