#!/bin/bash

###
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
# Copyright (C) 2011-2012 INRA http://www.inra.fr
###

# Script de construction de tous les translation files de recordweb
# (une fois ceux-ci crees/mis en place)

echo "construction de tous les translation files de recordweb"

echo "recordweb/rwsite/locale :"
cd rwsite
django-admin.py makemessages -a
django-admin.py compilemessages
cd .. # retour a recordweb

cd record

echo "recordweb/record/locale :"
# (generer/maj .po)
django-admin.py makemessages -a
# (compiler/generer .mo)
django-admin.py compilemessages

echo "recordweb/record/bd_modeles_record/locale :"
cd bd_modeles_record
django-admin.py makemessages -a
django-admin.py compilemessages
cd .. # retour a record

echo "recordweb/record/outil_web_record/locale :"
cd outil_web_record
django-admin.py makemessages -a
django-admin.py compilemessages
cd .. # retour a record

