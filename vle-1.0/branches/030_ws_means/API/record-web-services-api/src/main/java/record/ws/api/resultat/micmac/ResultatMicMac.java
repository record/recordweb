/*
  Copyright (C) 2014-2015 INRA http://www.inra.fr
  meansrecordapi - RECORD platform web services API for MEANS platform

  This file is part of meansrecordapi.

  meansrecordapi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  meansrecordapi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
  
  Contributors:
        Meradi Rabah (rabahmeradi@gmail.com) - initial API and implementation"
 */
package record.ws.api.resultat.micmac;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import record.ws.api.resultat.Resultat;
import record.ws.api.resultat.micmac.cultures.Culture;

/**
 * Le résultat de simulation de modèle MicMac.
 */
public class ResultatMicMac extends Resultat {

	/** La liste des flux polluants. */
	@JsonProperty("fluxPolluant")
	private List<FluxPolluant> fluxPolluant;

	/** Le co-produit. */
	@JsonProperty("coProduit")
	private CoProduit coProduit;

	/** La variable sol érosion de MEANS. */
	@JsonProperty("solErosion")
	private SolErosion solErosion;

	/** Les informations sur la culture principale. */
	@JsonProperty("culturePrincipale")
	private Culture culturePrincipale;

	/** Les informations sur la culture précédente. */
	@JsonProperty("culturePrecedente")
	private Culture culturePrecedente;

	/** Les informations sur la culture intermédiaire. */
	@JsonProperty("cultureIntermediaire")
	private Culture cultureIntermediaire;

	/** The additional properties. */
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * Retourne la liste des flux polluants.
	 *
	 * @return la liste des flux polluants
	 */
	public List<FluxPolluant> getFluxPolluant() {
		return fluxPolluant;
	}

	/**
	 * Retourne les valeurs dun flux polluant.
	 *
	 * @param nom
	 *            le nom du flux polluant
	 * @return les valeur du flux
	 */
	public List<Valeur> getFluxValeurs(String nom) {
		for (FluxPolluant flux : fluxPolluant) {
			if (flux.getNom().equals(nom))
				return flux.getValeurs();
		}
		return null;
	}

	/**
	 * Initialise la liste des flux polluants.
	 *
	 * @param fluxPolluant
	 *            la liste des flux polluants
	 */
	public void setFluxPolluant(List<FluxPolluant> fluxPolluant) {
		this.fluxPolluant = fluxPolluant;
	}

	/**
	 * Retourne le co-produit.
	 *
	 * @return le co-produit
	 */
	public CoProduit getCoProduit() {
		return coProduit;
	}

	/**
	 * Initialise le co-produit.
	 *
	 * @param coProduit
	 *            le co-produit
	 */
	public void setCoProduit(CoProduit coProduit) {
		this.coProduit = coProduit;
	}

	/**
	 * Retourne la variable sol érosion.
	 *
	 * @return sol érosion
	 */
	public SolErosion getSolErosion() {
		return solErosion;
	}

	/**
	 * Initialise la variable sol érosion.
	 *
	 * @param solErosion
	 *            le variable sol érosion
	 */
	public void setSolErosion(SolErosion solErosion) {
		this.solErosion = solErosion;
	}

	/**
	 * Retourne les informations de la culture principale.
	 *
	 * @return la culture principale
	 */
	public Culture getCulturePrincipale() {
		return culturePrincipale;
	}

	/**
	 * Initialise les informations de la culture principale.
	 *
	 * @param culturePrincipale
	 *            la culture principale
	 */
	public void setCulturePrincipale(Culture culturePrincipale) {
		this.culturePrincipale = culturePrincipale;
	}

	/**
	 * Retourne les informations de la culture précédente.
	 *
	 * @return la culture précédente
	 */
	public Culture getCulturePrecedente() {
		return culturePrecedente;
	}

	/**
	 * Initialise les informations de la culture précédente.
	 *
	 * @param culturePrecedente
	 *            la culture précédente
	 */
	public void setCulturePrecedente(Culture culturePrecedente) {
		this.culturePrecedente = culturePrecedente;
	}

	/**
	 * Retourne les informations de la culture intermédiaire.
	 *
	 * @return la culture intermédiaire
	 */
	public Culture getCultureIntermediaire() {
		return cultureIntermediaire;
	}

	/**
	 * Initialise les informations de la culture intermédiaire.
	 *
	 * @param cultureIntermediaire
	 *            la culture intermédiaire
	 */
	public void setCultureIntermediaire(
			Culture cultureIntermediaire) {
		this.cultureIntermediaire = cultureIntermediaire;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other) {
		return EqualsBuilder.reflectionEquals(this, other);
	}

	/* (non-Javadoc)
	 * @see record.ws.api.resultat.Resultat#getAdditionalProperties()
	 */
	@JsonAnyGetter
	public Map<String, Object> getResultat() {
		return this.additionalProperties;
	}

	/* (non-Javadoc)
	 * @see record.ws.api.resultat.Resultat#setAdditionalProperty(java.lang.String, java.lang.Object)
	 */
	@JsonAnySetter
	public void setPropriete(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
