/*
  Copyright (C) 2014-2015 INRA http://www.inra.fr
  meansrecordapi - RECORD platform web services API for MEANS platform

  This file is part of meansrecordapi.

  meansrecordapi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  meansrecordapi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
  
  Contributors:
        Meradi Rabah (rabahmeradi@gmail.com) - initial API and implementation"
 */
package record.ws.api.resultat.micmac;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * La valeur d'un flux polluant à une date donnée.
 */
public class Valeur {

	/** La date. */
	@JsonProperty("date")
	private Date date;

	/** La valeur. */
	@JsonProperty("valeur")
	private double valeur;

	/** Les autres propriétés. */
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * Le constructeur. Il est obligatoire d'avoir un constructeur vide pour
	 * faire fonctionner la sérialisation/desérialisation.
	 */
	public Valeur() {
	}

	/**
	 * Le constructeur.
	 *
	 * @param date
	 *            la date
	 * @param valeur
	 *            la valeur
	 */
	public Valeur(Date date, long valeur) {
		this.date = date;
		this.valeur = valeur;
	}

	/**
	 * Retourne la date.
	 *
	 * @return la date
	 */
	@JsonProperty("date")
	public Date getDate() {
		return date;
	}

	/**
	 * Initialise la date.
	 *
	 * @param date
	 *            la date
	 */
	@JsonProperty("date")
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Retourne la valeur.
	 *
	 * @return la valeur
	 */
	@JsonProperty("valeur")
	public double getValeur() {
		return valeur;
	}

	/**
	 * Initialise la valeur.
	 *
	 * @param valeur
	 *            la valeur
	 */
	@JsonProperty("valeur")
	public void setValeur(double valeur) {
		this.valeur = valeur;
	}
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other) {
		return EqualsBuilder.reflectionEquals(this, other);
	}

	/**
	 * Retourne les autres propriétés de la classe.
	 *
	 * @return les autres propriétés
	 */
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	/**
	 * Ajoute une propriété et sa valeur.
	 *
	 * @param nom
	 *            le nom de la propriété
	 * @param valeur
	 *            la valeur de la propriété
	 */
	@JsonAnySetter
	public void setAdditionalProperty(String nom, Object valeur) {
		this.additionalProperties.put(nom, valeur);
	}

}
