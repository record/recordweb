/*
  Copyright (C) 2014-2015 INRA http://www.inra.fr
  meansrecordapi - RECORD platform web services API for MEANS platform

  This file is part of meansrecordapi.

  meansrecordapi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  meansrecordapi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
  
  Contributors:
        Meradi Rabah (rabahmeradi@gmail.com) - initial API and implementation"
 */
package record.ws.api.resultat.micmac.activites;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Contient la liste des activités effectuées sur une culture par le modèle MicMac.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "fertilisation", "travailSol", "semis", "recolte",
		"irrigationFertirrigation", "traitementsPhytosanitaires" })
public class Activites {

	/** Les activités de fertilisation. */
	@JsonProperty("fertilisation")
	@Valid
	private List<Fertilisation> fertilisation = new ArrayList<Fertilisation>();

	/** Les activités de travail de sol. */
	@JsonProperty("travailSol")
	@Valid
	private List<TravailSol> travailSol = new ArrayList<TravailSol>();

	/** L'activité de semis. */
	@JsonProperty("semis")
	@Valid
	private Semis semis;

	/** L'activité de récolte. */
	@JsonProperty("recolte")
	@Valid
	private Recolte recolte;

	/** Les activités d'irrigation et de fertirrigation. */
	@JsonProperty("irrigationFertirrigation")
	@Valid
	private List<IrrigationFertirrigation> irrigationFertirrigation = new ArrayList<IrrigationFertirrigation>();

	/** Les traitements phytosanitaires. */
	@JsonProperty("traitementsPhytosanitaires")
	@Valid
	private List<TraitementsPhytosanitaire> traitementsPhytosanitaires = new ArrayList<TraitementsPhytosanitaire>();
	
	
	/** Les activités de post-récolte. */
	@JsonProperty("postRecolte")
	@Valid
	private List<PostRecolte> postRecolte = new ArrayList<PostRecolte>();

	
	/** Les autres activités. */
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * Retourne la liste des activités de fertilisation.
	 *
	 * @return la liste des activités de fertilisation
	 */
	@JsonProperty("fertilisation")
	public List<Fertilisation> getFertilisation() {
		return fertilisation;
	}

	/**
	 * Initialise la liste des activités de fertilisation.
	 *
	 * @param fertilisation
	 *            la liste des activités de fertilisation
	 */
	@JsonProperty("fertilisation")
	public void setFertilisation(List<Fertilisation> fertilisation) {
		this.fertilisation = fertilisation;
	}

	/**
	 * Retourne la liste des activités de travail de sol.
	 *
	 * @return la liste des activités de travail de sol
	 */
	@JsonProperty("travailSol")
	public List<TravailSol> getTravailSol() {
		return travailSol;
	}

	/**
	 * Initialise la liste des activités de travail de sol.
	 *
	 * @param travailSol
	 *            la liste des activités de travail de sol
	 */
	@JsonProperty("travailSol")
	public void setTravailSol(List<TravailSol> travailSol) {
		this.travailSol = travailSol;
	}

	/**
	 * Retourne l'activité de semis.
	 *
	 * @return l'activité de semis
	 */
	@JsonProperty("semis")
	public Semis getSemis() {
		return semis;
	}

	/**
	 * Initialise l'activité de semis.
	 *
	 * @param semis
	 *            l'activité de semis
	 */
	@JsonProperty("semis")
	public void setSemis(Semis semis) {
		this.semis = semis;
	}

	/**
	 * Retourne l'activité de récolte.
	 *
	 * @return l'activité de récolte
	 */
	@JsonProperty("recolte")
	public Recolte getRecolte() {
		return recolte;
	}

	/**
	 * Initialise l'activité de récolte.
	 *
	 * @param recolte
	 *            l'activité de récolte
	 */
	@JsonProperty("recolte")
	public void setRecolte(Recolte recolte) {
		this.recolte = recolte;
	}

	/**
	 * Retourne la liste des activités d'irrigation et de fertirrigation.
	 *
	 * @return la liste des activités d'irrigation et de fertirrigation
	 */
	@JsonProperty("irrigationFertirrigation")
	public List<IrrigationFertirrigation> getIrrigationFertirrigation() {
		return irrigationFertirrigation;
	}

	/**
	 * Initialise la liste des activités de'irrigation et de fertirrigation.
	 *
	 * @param irrigationFertirrigation
	 *            la liste des activités d'irrigation et de fertirrigation
	 */
	@JsonProperty("irrigationFertirrigation")
	public void setIrrigationFertirrigation(
			List<IrrigationFertirrigation> irrigationFertirrigation) {
		this.irrigationFertirrigation = irrigationFertirrigation;
	}

	/**
	 * Retourne les traitements phytosanitaires.
	 *
	 * @return les traitements phytosanitaires
	 */
	@JsonProperty("traitementsPhytosanitaires")
	public List<TraitementsPhytosanitaire> getTraitementsPhytosanitaires() {
		return traitementsPhytosanitaires;
	}

	/**
	 * Initialise les traitements phytosanitaires.
	 *
	 * @param traitementsPhytosanitaires
	 *            les traitements phytosanitaires
	 */
	@JsonProperty("traitementsPhytosanitaires")
	public void setTraitementsPhytosanitaires(
			List<TraitementsPhytosanitaire> traitementsPhytosanitaires) {
		this.traitementsPhytosanitaires = traitementsPhytosanitaires;
	}
	
	
	/**
	 * Retourne les activités de post-récolte.
	 *
	 * @return les activités de post-récolte
	 */
	@JsonProperty("postRecolte")
	public List<PostRecolte> getPostRecolte() {
		return postRecolte;
	}

	/**
	 * Initialise les activités de post-récolte.
	 *
	 * @param postRecolte
	 *            les activités de post-récolte
	 */
	@JsonProperty("postRecolte")
	public void setPostRecolte(
			List<PostRecolte> postRecolte) {
		this.postRecolte = postRecolte;
	}
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other) {
		return EqualsBuilder.reflectionEquals(this, other);
	}

	/**
	 * Retourne les autres propriétés.
	 *
	 * @return les autres propriétés
	 */
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	/**
	 * Ajoute une propriété et sa valeur.
	 *
	 * @param nom
	 *            le nom de la propriété
	 * @param valeur
	 *            la valeur de la propriété
	 */
	@JsonAnySetter
	public void setAdditionalProperty(String nom, Object valeur) {
		this.additionalProperties.put(nom, valeur);
	}

}
