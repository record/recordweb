/*
  Copyright (C) 2014-2015 INRA http://www.inra.fr
  meansrecordapi - RECORD platform web services API for MEANS platform

  This file is part of meansrecordapi.

  meansrecordapi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  meansrecordapi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
  
  Contributors:
        Meradi Rabah (rabahmeradi@gmail.com) - initial API and implementation"
 */

package record.ws.api.resultat.micmac;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Contient les valeurs d'un flux polluant.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "nom",
    "valeurs"
})
public class FluxPolluant {

    /** le nom de flux polluant. */
    @JsonProperty("nom")
    private String nom;
    
    /** L'unité  de mesure utilisée. */
	@JsonProperty("unite")
	private String unite;
    
    /** Les valeurs de flux polluant. */
    @JsonProperty("valeurs")
    @Valid
    private List<Valeur> valeurs = new ArrayList<Valeur>();
    
    /** Les autres informations concernant le flux polluant. */
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * Retourne le nom de flux polluant.
     *
     * @return le nom de flux polluant
     */
    @JsonProperty("nom")
    public String getNom() {
        return nom;
    }

    /**
     * Initialise le nom de flux polluant.
     *
     * @param nom le nom
     */
    @JsonProperty("nom")
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    /**
	 * Retourne l'unité de mesure.
	 *
	 * @return l'unité
	 */
	@JsonProperty("unite")
	public String getUnite() {
		return unite;
	}

	/**
	 * Initialise l'unité de mesure.
	 *
	 * @param unite
	 *            l'unité
	 */
	@JsonProperty("unite")
	public void setUnite(String unite) {
		this.unite = unite;
	}
    

    /**
     * Retourne les valeurs du flux.
     *
     * @return les valeurs
     */
    @JsonProperty("valeurs")
    public List<Valeur> getValeurs() {
        return valeurs;
    }

    /**
     * Initialise les valeurs.
     *
     * @param valeurs les valeurs
     */
    @JsonProperty("valeurs")
    public void setValeurs(List<Valeur> valeurs) {
        this.valeurs = valeurs;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    /**
     * Retourne les autres informations concernant le flux polluant.
     *
     * @return les autres informations
     */
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    /**
     * Ajoute une information sur le flux polluant.
     *
     * @param nom le nom de la propriété
     * @param valeur sa valeur
     */
    @JsonAnySetter
    public void setAdditionalProperty(String nom, Object valeur) {
        this.additionalProperties.put(nom, valeur);
    }

}
