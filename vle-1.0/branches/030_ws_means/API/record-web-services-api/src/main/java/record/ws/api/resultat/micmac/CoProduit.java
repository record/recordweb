/*
  Copyright (C) 2014-2015 INRA http://www.inra.fr
  meansrecordapi - RECORD platform web services API for MEANS platform

  This file is part of meansrecordapi.

  meansrecordapi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  meansrecordapi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
  
  Contributors:
        Meradi Rabah (rabahmeradi@gmail.com) - initial API and implementation"
 */

package record.ws.api.resultat.micmac;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Contient les informations sur un co-produit.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "nom", "rendement", "contenuCarbon" })
public class CoProduit {

	/** Le nom de co-produit. */
	@JsonProperty("nom")
	private String nom;

	/** Le rendement de co-produit. */
	@JsonProperty("rendement")
	private double rendement;

	/** Le contenu en carbone. */
	@JsonProperty("contenuCarbone")
	private double contenuCarbone;

	/** Toutes les autres données de co-produit. */
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * Retourne le nom de co-produit.
	 *
	 * @return le nom de co-produit
	 */
	@JsonProperty("nom")
	public String getNom() {
		return nom;
	}

	/**
	 * Initialise le nom de co-produit.
	 *
	 * @param nom
	 *            le nouveau nom
	 */
	@JsonProperty("nom")
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Retourne le rendement.
	 *
	 * @return le rendement
	 */
	@JsonProperty("rendement")
	public double getRendement() {
		return rendement;
	}

	/**
	 * Initialise le rendement.
	 *
	 * @param rendement
	 *            le rendement
	 */
	@JsonProperty("rendement")
	public void setRendement(double rendement) {
		this.rendement = rendement;
	}

	/**
	 * Retourne le contenu en carbone.
	 *
	 * @return le contenu en carbone
	 */
	@JsonProperty("contenuCarbone")
	public double getContenuCarbone() {
		return contenuCarbone;
	}

	/**
	 * Initialise le contenu carbone.
	 *
	 * @param contenuCarbon
	 *            le contenu carbone
	 */
	@JsonProperty("contenuCarbon")
	public void setContenuCarbon(double contenuCarbon) {
		this.contenuCarbone = contenuCarbon;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other) {
		return EqualsBuilder.reflectionEquals(this, other);
	}

	/**
	 * Retourne toutes les autres informations sur le co-produit.
	 *
	 * @return les autres informations sur le co-produit
	 */
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	/**
	 * Initialise une information sur le co-produit.
	 *
	 * @param nom
	 *            le nom de l'information
	 * @param valeur
	 *            sa valeur
	 */
	@JsonAnySetter
	public void setAdditionalProperty(String nom, Object valeur) {
		this.additionalProperties.put(nom, valeur);
	}

}
