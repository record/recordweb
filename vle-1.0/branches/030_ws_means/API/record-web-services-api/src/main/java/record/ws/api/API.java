/*
  Copyright (C) 2014-2015 INRA http://www.inra.fr
  meansrecordapi - RECORD platform web services API for MEANS platform

  This file is part of meansrecordapi.

  meansrecordapi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  meansrecordapi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
  
  Contributors:
        Meradi Rabah (rabahmeradi@gmail.com) - initial API and implementation"
 */
package record.ws.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;

import javax.security.auth.login.FailedLoginException;

import record.ws.api.auth.Token;
import record.ws.api.erreurs.ErreurSimulation;
import record.ws.api.erreurs.NonAuthentifie;
import record.ws.api.requete.Requete;
import record.ws.api.resultat.Resultat;
import record.ws.outils.Outils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Elle fournit l'ensemble des fonctionnalités de l'API.
 */
public class API {

	/** Timeout pour la connexion. Réglé à 30 seconds. */
	private static final int CONNECTION_CONNECT_TIMEOUT_MSEC = 300000;

	/** Timeout pour la lecture. Réglé à 30 seconds. */
	private static final int CONNECTION_READ_TIMEOUT_MSEC = 300000;

	/** L'url des services web. */
	private String apiUrl;

	/** Pour activer la compression. */
	private boolean zipped = true;

	/** Le token utilisé pour l'authentification. */
	private Token token = null;

	private Logger logger;

	/**
	 * Créer une instance en spécifiant l'url et le port. Le port est nécessaire
	 * que s'il est différent de 80.
	 *
	 * @param url
	 *            l'url
	 * @param port
	 *            le port
	 */
	public API(String url, int port) {
		this(url + ":" + port);
	}

	/**
	 * Un constructeur sans le port. C'est le port 80 qui est utilisé par
	 * défaut.
	 *
	 * @param url
	 *            l'url
	 */
	public API(String url) {
		this.apiUrl = url;
		logger = Logger.getLogger("api");
		logger.setLevel(Level.WARNING);
	}

	/**
	 * Permet de s'authentifier au sein des services web.
	 *
	 * @param username
	 *            le nom d'utilisateur
	 * @param password
	 *            le mot de passe
	 * @throws IOException
	 *             Signale qu'une erreur de connexion a eu lieu.
	 * @throws FailedLoginException
	 *             l'authentification n'a pas fonctionné.
	 */
	public void login(String username, char[] password) throws IOException,
			FailedLoginException {
		String data = "{\"username\":\"%s\",\"password\":\"%s\"}";
		data = String.format(data, URLEncoder.encode(username, "UTF-8"),
				URLEncoder.encode(new String(password), "UTF-8"));
		String url = apiUrl + "/api-token-auth/";
		Reponse response = post(url, data);
		if (!initialiserToken(response))
			throw new FailedLoginException(response.getContent());
	}

	/**
	 * Permet de s'authentifier au sein des services web.
	 *
	 * @param username
	 *            le nom d'utilisateur
	 * @param password
	 *            le mot de passe
	 * @throws IOException
	 *             Signale qu'une erreur de connexion a eu lieu.
	 * @throws FailedLoginException
	 *             l'authentification n'a pas fonctionné.
	 */
	public void login(String username, String password) throws IOException,
			FailedLoginException {
		login(username, password.toCharArray());
	}

	/**
	 * Permet de lancer la simulation d'un modèle. La fonction peut fonctionner
	 * avec n'importe quel modèle. Il suffit de donner les données d'entrées de
	 * ce modèle et l'objet où stocker le résultat. Vous pouvez utiliser les
	 * objets Requete et Resultat qui sont génériques.
	 * 
	 * Avant de lancer la simulation d'un modèle, il faut s'assurer que vous
	 * êtes authentifié aux services web et que vous avez le droit d'utiliser le
	 * modèle.
	 *
	 * @param modele
	 *            le nom de modèle à simuler
	 * @param requete
	 *            les données d'entrés du modèle
	 * @param c
	 *            la classe ou sera stocké le résultat de simulation
	 * @return le résultat de simulation
	 * @throws NonAuthentifie
	 *             Si vous n'êtes pas authentifié
	 * @throws IOException
	 */
	@SuppressWarnings("rawtypes")
	public Resultat simuler(String modele, Requete requete, Class c)
			throws NonAuthentifie, ErreurSimulation, IOException {
		String url = this.apiUrl + "/simulation/" + modele + "/";
		String data = Outils.serialiser(requete);
		if (token == null || !token.isValide())
			throw new NonAuthentifie();
		Reponse reponse = post(url, data);
		if (reponse.getCode() == 200)
			return (Resultat) Outils.desrialiser(reponse.getContent(), c);
		else
			throw new ErreurSimulation(reponse.getContent());
	}

	/**
	 * Initialise le token d'après la réponse des services web à la demande
	 * d'authenrification.
	 *
	 * @param reponse
	 *            la réponse à la demande d'authentification
	 * @return true, en cas succès de l'authentification
	 */
	private boolean initialiserToken(Reponse reponse) {
		token = null;
		if (reponse.getCode() != 200) {
			logWarning("initialiserToken", "Erreur d'authentification : "
					+ reponse.getContent());
			return false;
		} else {
			logInfo("initialiserToken", "Authentification faîtes avec succès !");
			ObjectMapper mapper = new ObjectMapper();
			try {
				token = mapper.readValue(reponse.getContent(), Token.class);
				return true;
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	/**
	 * Permet de faire un post HTTP(s) et récupérer la réponse. Le contenu à
	 * envoyer doit être de json.
	 *
	 * @param url
	 *            l'url
	 * @param data
	 *            les données à envoyer
	 * @return la réponse de serveur
	 * @throws IOException
	 *             en cas d'erreur de connexion
	 */
	protected Reponse post(String url, String data) throws IOException {
		logInfo("post", "Posting to url " + url);
		URL con = new URL(url);
		HttpURLConnection connection = (HttpURLConnection) con.openConnection();
		connection.setDoOutput(true);
		connection.setConnectTimeout(CONNECTION_CONNECT_TIMEOUT_MSEC);
		if (tokenEstValide())
			// on ajoute les informations d'authentification
			connection.setRequestProperty("Authorization", "JWT " + token);
		// on ajoute qu'on envoit de code json
		connection.setRequestProperty("Content-Type", "application/json");
		// on spécifie qu'on veut une réponse en format json
		connection.setRequestProperty("Accept", "application/json");
		connection.setReadTimeout(CONNECTION_READ_TIMEOUT_MSEC);
		if (zipped)
			connection.setRequestProperty("Accept-encoding", "gzip");
		connection.connect();
		OutputStreamWriter out = new OutputStreamWriter(
				connection.getOutputStream(), "UTF-8");
		out.write(data);
		out.close();
		String line;
		int codeHTTP = connection.getResponseCode();
		StringBuilder temp = new StringBuilder(100000);
		InputStream is;
		if (codeHTTP == 200) {
			logInfo("post", "Réponse 200 Ok !");
			is = connection.getInputStream();
		} else {
			is = connection.getErrorStream();
			logWarning("post", "Erreur : code " + codeHTTP);
		}
		if ("gzip".equals(connection.getContentEncoding()))
			is = new GZIPInputStream(is);
		BufferedReader in = new BufferedReader(new InputStreamReader(is,
				"UTF-8"));
		while ((line = in.readLine()) != null) {
			temp.append(line);
			temp.append("\n");
		}
		in.close();
		return new Reponse(codeHTTP, temp.toString());
	}

	/**
	 * Vérfie si le token est valide.
	 *
	 * @return true, si le token est valide
	 */
	private boolean tokenEstValide() {
		return token != null && token.isValide();
	}

	/**
	 * Affiche une information sur la console. Elle utilisée pour le débogage.
	 *
	 * @param methode
	 *            la méthode qui fait l'affichage
	 * @param message
	 *            le message à afficher
	 */
	protected void logInfo(String methode, String message) {
		logger.logp(Level.INFO, "API", methode, message);
	}

	/**
	 * Affiche un message d'avertissement. Elle est utilisée pour le débogage.
	 *
	 * @param methode
	 *            le nom de la méthode qui affiche le message
	 * @param message
	 *            le message à afficher
	 */
	protected void logWarning(String methode, String message) {
		logger.logp(Level.WARNING, "API", methode, message);
	}

	/**
	 * Retourne le token d'authentification.
	 *
	 * @return le token
	 */
	public Token getToken() {
		return token;
	}

	/**
	 * Elle contient la réponse des services web.
	 */
	private class Reponse {

		/** le code HTTP de la réponse. */
		private int codeHTTP;

		/** le contenu de la réponse. */
		private String contenu;

		/**
		 * Instancier une réponse.
		 *
		 * @param codeHTTP
		 *            le code HTTP de la réponse
		 * @param contenu
		 *            le contenu de la réponse
		 */
		public Reponse(int codeHTTP, String contenu) {
			this.codeHTTP = codeHTTP;
			this.contenu = contenu;
		}

		/**
		 * Retourne le code HTTP.
		 *
		 * @return le code
		 */
		public int getCode() {
			return codeHTTP;
		}

		/**
		 * Retourne le contenu de la réponse.
		 *
		 * @return le contenu de la réponse
		 */
		public String getContent() {
			return contenu;
		}
	}
}
