/*
  Copyright (C) 2014-2015 INRA http://www.inra.fr
  meansrecordapi - RECORD platform web services API for MEANS platform

  This file is part of meansrecordapi.

  meansrecordapi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  meansrecordapi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
  
  Contributors:
        Meradi Rabah (rabahmeradi@gmail.com) - initial API and implementation"
 */
package record.ws.outils;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Elle regroupe des fonctions outils.
 */
public class Outils {
	private static ObjectMapper mapper = new ObjectMapper();

	/**
	 * Desérialise le code json en un objet Java.
	 *
	 * @param json
	 *            la représentation json du l'objet
	 * @param c
	 *            la classe qui sera utilisée pour la desérialisation
	 * @return le résultat de desérialisation
	 * @throws JsonParseException
	 *             si le code json n'est pas valide
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             the IO exception
	 */
	@SuppressWarnings("rawtypes")
	public static Object desrialiser(String json, Class c)
			throws JsonParseException, JsonMappingException, IOException {
		@SuppressWarnings("unchecked")
		Object objet = mapper.readValue(json, c);
		return objet;
	}

	/**
	 * Sérialise un objet en sa représentation en format json.
	 *
	 * @param objet
	 *            l'objet à sérialiser
	 * @return la représentation json de l'objet
	 * @throws JsonProcessingException
	 */
	public static String serialiser(Object objet)
			throws JsonProcessingException {
		String json = "";
		json = mapper.writeValueAsString(objet);
		return json;
	}

}
