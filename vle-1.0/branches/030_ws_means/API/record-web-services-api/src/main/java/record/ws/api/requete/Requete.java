/*
  Copyright (C) 2014-2015 INRA http://www.inra.fr
  meansrecordapi - RECORD platform web services API for MEANS platform

  This file is part of meansrecordapi.

  meansrecordapi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  meansrecordapi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
  
  Contributors:
        Meradi Rabah (rabahmeradi@gmail.com) - initial API and implementation"
 */
package record.ws.api.requete;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

/**
 * Elle contient l'ensemble des données d'entrées d'un modèle. Utilisée pour
 * construire la requête à envoyer au service web.
 */
public class Requete {

	/** Les paramètres d'entrées du modèle. */
	Map<String, Object> parametres = new HashMap<String, Object>();

	/**
	 * Retourne l'ensemble des paramètres d'entrées du modèle.
	 *
	 * @return les paramètres
	 */
	@JsonAnyGetter
	public Map<String, Object> getParametres() {
		return this.parametres;
	}

	/**
	 * Ajoute un paramètre avec sa valeur aux paramètres d'entrées.
	 *
	 * @param nom
	 *            le nom de paramètre
	 * @param valeur
	 *            la valeur de paramètre
	 */
	@JsonAnySetter
	public void setAdditionalProperty(String nom, Object valeur) {
		this.parametres.put(nom, valeur);
	}

}
