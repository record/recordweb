/*
  Copyright (C) 2014-2015 INRA http://www.inra.fr
  meansrecordapi - RECORD platform web services API for MEANS platform

  This file is part of meansrecordapi.

  meansrecordapi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  meansrecordapi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
  
  Contributors:
        Meradi Rabah (rabahmeradi@gmail.com) - initial API and implementation"
 */
package record.ws.api.resultat.micmac.activites;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Contient les données sur une activité de irrigation et de fertirrigation.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "date", "sourceEnergie", "unite", "quantiteEau",
		"materiel" })
public class IrrigationFertirrigation {

	/** La date de cette activité. */
	@JsonProperty("date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "CET")
	private Date date;

	/** La source d'énergie utilisée. */
	@JsonProperty("sourceEnergie")
	private String sourceEnergie;

	/** La quantité d'eau utilisée. */
	@JsonProperty("quantiteEau")
	private double quantiteEau;

	/** Le nom de matériel utilisé. */
	@JsonProperty("materiel")
	@Valid
	private String materiel;

	/** L'unite de mesure utilisée. */
	@JsonProperty("unite")
	private String unite;

	/** Les autres propriétés. */
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * Retourne la date de cette activité.
	 *
	 * @return la date de cette activité
	 */
	@JsonProperty("date")
	public Date getDate() {
		return date;
	}

	/**
	 * Initialise la date de cette activité.
	 *
	 * @param date
	 *            la date
	 */
	@JsonProperty("date")
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Retourne la source d'énergie utilisée.
	 *
	 * @return la source d'énergie
	 */
	@JsonProperty("sourceEnergie")
	public String getSourceEnergie() {
		return sourceEnergie;
	}

	/**
	 * Initialise la source d'énergie.
	 *
	 * @param sourceEnergie
	 *            la source d'énergie
	 */
	@JsonProperty("sourceEnergie")
	public void setSourceEnergie(String sourceEnergie) {
		this.sourceEnergie = sourceEnergie;
	}

	/**
	 * Retourne la quantité d'eau utilisée.
	 *
	 * @return la quantité d'eau
	 */
	@JsonProperty("quantiteEau")
	public double getQuantiteEau() {
		return quantiteEau;
	}

	/**
	 * Initialise la quantité d'eau pour cette activité.
	 *
	 * @param quantiteEau
	 *            la quantité eau
	 */
	@JsonProperty("quantiteEau")
	public void setQuantiteEau(double quantiteEau) {
		this.quantiteEau = quantiteEau;
	}

	/**
	 * Retourne l'unite de mesure utilisée.
	 *
	 * @return l'unite de mesure
	 */
	@JsonProperty("unite")
	public String getUnite() {
		return unite;
	}

	/**
	 * Initialise l'unite de mesure.
	 *
	 * @param unite
	 *            l'unite de mesure
	 */
	@JsonProperty("unite")
	public void setUnite(String unite) {
		this.unite = unite;
	}

	/**
	 * Retourne le matériel utilisé pour cette activité.
	 *
	 * @return le nom de matériel
	 */
	@JsonProperty("materiel")
	public String getMateriel() {
		return materiel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other) {
		return EqualsBuilder.reflectionEquals(this, other);
	}

	/**
	 * Retourne les autres propriétés de cette activités.
	 *
	 * @return les autres propriétés
	 */
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	/**
	 * Ajoute une propriété et sa valeur.
	 *
	 * @param nom
	 *            le nom de la propriété
	 * @param valeur
	 *            la valeur de la propriété
	 */
	@JsonAnySetter
	public void setAdditionalProperty(String nom, Object valeur) {
		this.additionalProperties.put(nom, valeur);
	}

}
