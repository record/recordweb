/*
  Copyright (C) 2014-2015 INRA http://www.inra.fr
  meansrecordapi - RECORD platform web services API for MEANS platform

  This file is part of meansrecordapi.

  meansrecordapi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  meansrecordapi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
  
  Contributors:
        Meradi Rabah (rabahmeradi@gmail.com) - initial API and implementation"
 */

package record.ws.api.resultat.micmac.activites;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Contient les données sur une activité de travail de sol.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "date",
    "nom",
    "profondeur",
    "nbPassages"
})
public class TravailSol {

    /** La date de l'activité. */
    @JsonProperty("date")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="CET")
    private Date date;
    
    /** Le nom de l'actvité. */
    @JsonProperty("nom")
    private String nom;
    
    /** La profondeur de cette activité. */
    @JsonProperty("profondeur")
    private double profondeur;
    
    /** Le nombre de passages. */
    @JsonProperty("nbPassages")
    private long nbPassages;
    
    /** Les autres propriétés. */
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
	 * Retourne la date de cette activité.
	 *
	 * @return la date
	 */
    @JsonProperty("date")
    public Date getDate() {
        return date;
    }

    /**
	 * Initialise la date de cette activité.
	 *
	 * @param date
	 *            la date
	 */
    @JsonProperty("date")
    public void setDate(Date date) {
        this.date = date;
    }

    /**
	 * Retourne le nom de l'actvité.
	 *
	 * @return le nom de l'actvité
	 */
    @JsonProperty("nom")
    public String getNom() {
        return nom;
    }

    /**
	 * Initialise le nom de l'activité.
	 *
	 * @param nom
	 *            le nom
	 */
    @JsonProperty("nom")
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
	 * Retourne la profondeur.
	 *
	 * @return la profondeur
	 */
    @JsonProperty("profondeur")
    public double getProfondeur() {
        return profondeur;
    }

    /**
	 * Initialise la profondeur.
	 *
	 * @param profondeur
	 *            la profondeur
	 */
    @JsonProperty("profondeur")
    public void setProfondeur(double profondeur) {
        this.profondeur = profondeur;
    }

    /**
	 * Retourne le nombre de passages.
	 *
	 * @return le nombre passages
	 */
    @JsonProperty("nbPassages")
    public long getNbPassages() {
        return nbPassages;
    }

    /**
	 * Initialise le nombre de passages.
	 *
	 * @param nbPassages
	 *            le nombre passages
	 */
    @JsonProperty("nbPassages")
    public void setNbPassages(long nbPassages) {
        this.nbPassages = nbPassages;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    /**
	 * Retourne les autres propriétés de cette activité.
	 *
	 * @return les autres activités
	 */
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    /**
	 * Ajoute une propriété et sa valeur.
	 *
	 * @param nom
	 *           le nom de la propriété
	 * @param valeur
	 *            la valeur de la propriété
	 */
    @JsonAnySetter
    public void setAdditionalProperty(String nom, Object valeur) {
        this.additionalProperties.put(nom, valeur);
    }

}
