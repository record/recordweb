/*
  Copyright (C) 2014-2015 INRA http://www.inra.fr
  meansrecordapi - RECORD platform web services API for MEANS platform

  This file is part of meansrecordapi.

  meansrecordapi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  meansrecordapi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
  
  Contributors:
        Meradi Rabah (rabahmeradi@gmail.com) - initial API and implementation"
 */

package record.ws.api.resultat.micmac.cultures;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import record.ws.api.resultat.micmac.activites.Activites;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

// TODO: Auto-generated Javadoc
/**
 * The Class CulturePrincipale.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "nom",
    "rendement",
    "activites"
})
public class Culture {

    /** Le nom de la culture. */
    @JsonProperty("nom")
    private String nom;
    
    /** Le rendement de la culture. */
    @JsonProperty("rendement")
    private double rendement;
    
    /** Les activités effectuées sur la culture. */
    @JsonProperty("activites")
    @Valid
    private Activites activites;
    
    /** Les autres propriétés de la culture. */
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * Retourne le nom de la culture.
     *
     * @return le nom de la culture
     */
    @JsonProperty("nom")
    public String getNom() {
        return nom;
    }

    /**
     * Initialise le nom de la culture.
     *
     * @param nom le nom de la culture
     */
    @JsonProperty("nom")
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Retourne le rendement de la culture.
     *
     * @return le rendement
     */
    @JsonProperty("rendement")
    public double getRendement() {
        return rendement;
    }

    /**
     * Initialise le rendement de la culture.
     *
     * @param rendement le rendement
     */
    @JsonProperty("rendement")
    public void setRendement(double rendement) {
        this.rendement = rendement;
    }

    /**
     * Retourne le activités effectués sur la culture.
     *
     * @return les activités effectués sur la culture
     */
    @JsonProperty("activites")
    public Activites getActivites() {
        return activites;
    }

    /**
     * Initialise le activités effectués sur la culture.
     *
     * @param activites les activités effectuées sur la culture
     */
    @JsonProperty("activites")
    public void setActivites(Activites activites) {
        this.activites = activites;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    /**
     * Retourne les autres propriétés de la culture.
     *
     * @return les autres propriétés
     */
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    /**
     * Ajoute une propriété et sa valeur
     *
     * @param nom le nom de la propriété
     * @param valeur la valeur de la propriété
     */
    @JsonAnySetter
    public void setAdditionalProperty(String nom, Object valeur) {
        this.additionalProperties.put(nom, valeur);
    }

}
