/*
  Copyright (C) 2014-2015 INRA http://www.inra.fr
  meansrecordapi - RECORD platform web services API for MEANS platform

  This file is part of meansrecordapi.

  meansrecordapi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  meansrecordapi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
  
  Contributors:
        Meradi Rabah (rabahmeradi@gmail.com) - initial API and implementation"
 */
package record.ws.api.auth;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

// TODO: Auto-generated Javadoc
/**
 * Contient les données incluses dans le token des services web.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JWTClaims implements Serializable {
	
	private static final long serialVersionUID = 1L;

	/** le nom d'utilisateur. */
	private String username;
	
	/** L'identifiant de l'utilisateur */
	private String user_id;
	
	/** L'adresse mail. */
	private String email;
	
	/** La durée de validité. */
	private long exp;


	/**
	 * Instantiates a new JWT claims.
	 */
	public JWTClaims() {
	}

	/**
	 * Initialise la date d'expération.
	 *
	 * @param exp la date d'expiration
	 */
	public void setExp(long exp) {
		this.exp = exp;
	}
	
	/**
	 * Retourne la date d'expération.
	 *
	 * @return la date d'expération
	 */
	public long getExp(){
		return exp;
	}

	/**
	 * Retourne le nom d'utilisateur.
	 *
	 * @return le nom d'utilisateur
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Initialise le nom d'utilisateur.
	 *
	 * @param username the new username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Retourne l'identifiant.
	 *
	 * @return the user_id
	 */
	public String getUser_id() {
		return user_id;
	}

	/**
	 * Initialise l'identifiant.
	 *
	 * @param user_id the new user_id
	 */
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	/**
	 * Retourne l'adresse mail.
	 *
	 * @return l'adresse mail
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Initialise l'adresse mail.
	 *
	 * @param email l'adresse mail
	 */
	public void setEmail(String email) {
		this.email = email;
	}


}