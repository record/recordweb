/*
  Copyright (C) 2014-2015 INRA http://www.inra.fr
  meansrecordapi - RECORD platform web services API for MEANS platform

  This file is part of meansrecordapi.

  meansrecordapi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  meansrecordapi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
  
  Contributors:
        Meradi Rabah (rabahmeradi@gmail.com) - initial API and implementation"
 */
package record.ws.api.requete.micmac;

import record.ws.api.requete.Requete;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Elle contient les données d'entrées de modèle MicMac.
 */
public class RequeteMicMac extends Requete {

	/** Le nom de la culture principale. */
	private String culturePrincipale;

	/** Le nom de la culture précédente. */
	private String culturePrecedente;

	/** La culture intermédiaire. */
	private String cultureIntermediaire;

	/** La région. */
	@JsonProperty
	private String region;

	/**
	 * Le constructeur.
	 *
	 * @param culturePrincipale
	 *            La culture principale
	 * @param culturePrecedente
	 *            La culture précédente
	 * @param cultureIntermediaire
	 *            La culture intermédiaire
	 * @param region
	 *            La région
	 */
	public RequeteMicMac(String culturePrincipale, String culturePrecedente,
			String cultureIntermediaire, String region) {
		this.culturePrincipale = culturePrincipale;
		this.culturePrecedente = culturePrecedente;
		this.cultureIntermediaire = cultureIntermediaire;
		this.region = region;
	}
	

	/**
	 * Retourne la culture principale.
	 *
	 * @return La culture principale
	 */
	public String getCulturePrincipale() {
		return culturePrincipale;
	}

	/**
	 * Initialise la culture principale.
	 *
	 * @param culturePrincipale
	 *            La culture principale
	 */
	public void setCulturePrincipale(String culturePrincipale) {
		this.culturePrincipale = culturePrincipale;
	}

	/**
	 * Retourne la culture precedente.
	 *
	 * @return La culture precedente
	 */
	public String getCulturePrecedente() {
		return culturePrecedente;
	}

	/**
	 * Initialise la culture précédente.
	 *
	 * @param culturePrecedente
	 *            La culture précédente
	 */
	public void setCulturePrecedente(String culturePrecedente) {
		this.culturePrecedente = culturePrecedente;
	}

	/**
	 * Retourne la culture intermédiaire.
	 *
	 * @return La culture intermédiaire
	 */
	public String getCultureIntermediaire() {
		return cultureIntermediaire;
	}

	/**
	 * Initialise la culture intermédiaire.
	 *
	 * @param cultureIntermediaire
	 *            La culture intermédiaire
	 */
	public void setCultureIntermediaire(String cultureIntermediaire) {
		this.cultureIntermediaire = cultureIntermediaire;
	}

	/**
	 * Retourne la region.
	 *
	 * @return La region
	 */
	public String getRegion() {
		return region;
	}

	/**
	 * Initialise la région.
	 *
	 * @param region
	 *            La région
	 */
	public void setRegion(String region) {
		this.region = region;
	}

}
