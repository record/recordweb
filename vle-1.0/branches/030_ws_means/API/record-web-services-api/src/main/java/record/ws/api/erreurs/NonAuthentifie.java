/*
  Copyright (C) 2014-2015 INRA http://www.inra.fr
  meansrecordapi - RECORD platform web services API for MEANS platform

  This file is part of meansrecordapi.

  meansrecordapi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  meansrecordapi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
  
  Contributors:
        Meradi Rabah (rabahmeradi@gmail.com) - initial API and implementation"
 */
package record.ws.api.erreurs;

/**
 * Signale une erreur en cas de problème d'authentification.
 */
public class NonAuthentifie extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Un constructeur avec le message d'erreur par défaut.
	 */
	public NonAuthentifie() {
		super("Il faut s'authentifier d'abord");
	}

	/**
	 * Un constructeur avec le message d'erreur.
	 *
	 * @param message
	 *            le message d'erreur
	 */
	public NonAuthentifie(String message) {
		super(message);
	}

}
