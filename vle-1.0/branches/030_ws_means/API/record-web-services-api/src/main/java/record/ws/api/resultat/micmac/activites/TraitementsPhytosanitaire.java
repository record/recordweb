/*
  Copyright (C) 2014-2015 INRA http://www.inra.fr
  meansrecordapi - RECORD platform web services API for MEANS platform

  This file is part of meansrecordapi.

  meansrecordapi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  meansrecordapi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
  
  Contributors:
        Meradi Rabah (rabahmeradi@gmail.com) - initial API and implementation"
 */

package record.ws.api.resultat.micmac.activites;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import record.ws.api.resultat.micmac.OpAgricole;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Contient les données d'un traitement phytosanitaire.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "date",
    "quantite",
    "unite",
    "nature",
    "pourcentage",
    "opAgricole",
    "nbPassages"
})
public class TraitementsPhytosanitaire {

    /** La date. */
    @JsonProperty("date")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="CET")
    private Date date;
    
    /** The quantité de phosphore utilisée. */
    @JsonProperty("quantite")
    private double quantite;
    
    /** L'unité  de mesure utilisée. */
	@JsonProperty("unite")
	private String unite;
    
    /** La nature de l'actvité. */
    @JsonProperty("nature")
    private String nature;
    
    /** Le pourcentage de la surface traitée. */
    @JsonProperty("pourcentage")
    private int pourcentage;
    
    /** L'opération agricole. */
    @JsonProperty("opAgricole")
    @Valid
    private OpAgricole opAgricole;
    
    /** Le nombre de passages. */
    @JsonProperty("nbPassages")
    private long nbPassages;
    
    /** Les autres propriétés. */
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
	 * Retourne la date de cette activité.
	 *
	 * @return la date
	 */
    @JsonProperty("date")
    public Date getDate() {
        return date;
    }

    /**
	 * Initialise la date de cette activité.
	 *
	 * @param date
	 *            la date
	 */
    @JsonProperty("date")
    public void setDate(Date date) {
        this.date = date;
    }

    /**
	 * Retourne le quantité de phosphore.
	 *
	 * @return the quantité
	 */
    @JsonProperty("quantite")
    public double getQuantite() {
        return quantite;
    }

    /**
	 * Initialise la quantité de phosphore de cette activité.
	 *
	 * @param quantite
	 *            la quantité de phosphore
	 */
    @JsonProperty("quantite")
    public void setQuantite(double quantite) {
        this.quantite = quantite;
    }
    
    /**
 	 * Retourne l'unité de mesure.
 	 *
 	 * @return l'unité
 	 */
 	@JsonProperty("unite")
 	public String getUnite() {
 		return unite;
 	}

 	/**
 	 * Initialise l'unité de mesure.
 	 *
 	 * @param unite
 	 *            l'unité
 	 */
 	@JsonProperty("unite")
 	public void setUnite(String unite) {
 		this.unite = unite;
 	}
    

    /**
	 * Retourne la nature de cette activité.
	 *
	 * @return la nature
	 */
    @JsonProperty("nature")
    public String getNature() {
        return nature;
    }

    /**
	 * Initialise la nature de cette activité.
	 *
	 * @param nature
	 *            la nature
	 */
    @JsonProperty("nature")
    public void setNature(String nature) {
        this.nature = nature;
    }

    /**
	 * Retourne le pourcentage de la surface traitée.
	 *
	 * @return le pourcentage
	 */
    @JsonProperty("pourcentage")
    public int getPourcentage() {
        return pourcentage;
    }

    /**
	 * Initialise le pourcentage de la surface traitée.
	 *
	 * @param pourcentage
	 *            le pourcentage
	 */
    @JsonProperty("pourcentage")
    public void setPourcentage(int pourcentage) {
        this.pourcentage = pourcentage;
    }

    /**
	 * Retourne l'opération agricole.
	 *
	 * @return l'opération agricole
	 */
    @JsonProperty("opAgricole")
    public OpAgricole getOpAgricole() {
        return opAgricole;
    }

    /**
	 * Initialise l'opération agricole.
	 *
	 * @param opAgricole
	 *            l'opération agricole
	 */
    @JsonProperty("opAgricole")
    public void setOpAgricole(OpAgricole opAgricole) {
        this.opAgricole = opAgricole;
    }

    /**
	 * Retourne le nombre de passages.
	 *
	 * @return le nombre de passages
	 */
    @JsonProperty("nbPassages")
    public long getNbPassages() {
        return nbPassages;
    }

    /**
	 * Initialise le nombre de passages.
	 *
	 * @param nbPassages
	 *            le nombre passages
	 */
    @JsonProperty("nbPassages")
    public void setNbPassages(long nbPassages) {
        this.nbPassages = nbPassages;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    /**
	 * Retourne les autres propriétés de cette activité.
	 *
	 * @return les autres propriétés
	 */
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    /**
	 * Ajoute une propriété et sa valeur.
	 *
	 * @param nom
	 *            le nom de la propriété
	 * @param valeur
	 *            la valeur de la propriété
	 */
    @JsonAnySetter
    public void setAdditionalProperty(String nom, Object valeur) {
        this.additionalProperties.put(nom, valeur);
    }

}
