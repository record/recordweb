/*
  Copyright (C) 2014-2015 INRA http://www.inra.fr
  meansrecordapi - RECORD platform web services API for MEANS platform

  This file is part of meansrecordapi.

  meansrecordapi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  meansrecordapi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
  
  Contributors:
        Meradi Rabah (rabahmeradi@gmail.com) - initial API and implementation"
 */

package record.ws.api.auth;

import java.io.IOException;

import org.apache.commons.codec.binary.Base64;

import record.ws.outils.Outils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

/*
 * 
 */
/**
 * Contient le token d'authentification et fournit une fonction pour tester sa
 * validité
 */
public class Token {

	/** Le token. */
	@JsonProperty("token")
	private String token;

	/**
	 * Instancier un nouveau Token.
	 *
	 * @param token
	 *            le token
	 */
	public Token(String token) {
		this.token = token;
	}
	
	/**
	 * Ce constructeur est utilisé pour permettre la desérialisation.
	 */
	public Token(){
	}

	/**
	 * Retourne le token.
	 *
	 * @return le token
	 */
	public String getToken() {
		return token;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return token;
	}

	/**
	 * Vérifie si le token est toujours valide. On vérifie que son format et sa
	 * date d'expiration.
	 * 
	 *
	 * @return true, s'il est valide
	 */
	public boolean isValide() {

		if ("".equals(token))
			return false;

		String[] pieces = token.split("\\.");
		// format
		if (pieces.length != 3 && pieces.length != 2)
			return false;

		try {
			JWTClaims jwtClaims = (JWTClaims) decodeAndParse(pieces[1],
					JWTClaims.class);
			// la date d'expiration
			if (jwtClaims.getExp() != 0
					&& System.currentTimeMillis() / 1000L >= jwtClaims.getExp())
				return false;
			else
				return true;
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return false;

	}

	/**
	 * Décode le token qui est encodé en Base64 et desérialise son contenu.
	 *
	 * @param b64String
	 *            le token encodé en Base64
	 * @param jsonClass
	 *            la classe ou sera stocké les données de token
	 * @return le données de token
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	private Object decodeAndParse(String b64String, Class<JWTClaims> jsonClass)
			throws JsonParseException, JsonMappingException, IOException {
		String jsonString = new String(Base64.decodeBase64(b64String), "UTF-8");
		Object obj = Outils.desrialiser(jsonString, jsonClass);
		return obj;
	}

}