/*
  Copyright (C) 2014-2015 INRA http://www.inra.fr
  meansrecordapi - RECORD platform web services API for MEANS platform

  This file is part of meansrecordapi.

  meansrecordapi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  meansrecordapi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
  
  Contributors:
        Meradi Rabah (rabahmeradi@gmail.com) - initial API and implementation"
 */
package record.ws.api.resultat.micmac.activites;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Contient les données d'une activité de post-récolte.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "date", "nom", "quantite" })
public class PostRecolte {

	/** La date de l'activité. */
	@JsonProperty("date")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="CET")
	private Date date;

	/** Le nom de l'activité. */
	@JsonProperty("nom")
	@Valid
	private String nom;
	
	/** L'unité  de mesure utilisée. */
	@JsonProperty("unite")
	private String unite;
	
	/** Les autres propriété. */
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/** La quantité. */
	@JsonProperty("quantite")
	private double quantite;

	/**
	 * Retourne la date de l'activité.
	 *
	 * @return la date
	 */
	@JsonProperty("date")
	public Date getDate() {
		return date;
	}

	/**
	 * Initialise la date de l'activité.
	 *
	 * @param date
	 *            la date de l'activité
	 */
	@JsonProperty("date")
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Retourne le nom.
	 *
	 * @return the nom
	 */
	@JsonProperty("nom")
	public String getNom() {
		return nom;
	}

	/**
	 * Initialise la quantité.
	 *
	 * @param quantite
	 *            la quantité
	 */
	@JsonProperty("quantite")
	public void setQuantite(double quantite) {
		this.quantite = quantite;
	}

	/**
	 * Retourne la quantité.
	 *
	 * @return la quantité
	 */
	@JsonProperty("quantite")
	public double getQuantite() {
		return quantite;
	}

	/**
	 * Initialise le nom de l'activité.
	 *
	 * @param nom
	 *            le nom de l'activité
	 */
	@JsonProperty("nom")
	public void setNom(String nom) {
		this.nom = nom;
	}

	 /**
		 * Retourne l'unité de mesure.
		 *
		 * @return l'unité
		 */
		@JsonProperty("unite")
		public String getUnite() {
			return unite;
		}

		/**
		 * Initialise l'unité de mesure.
		 *
		 * @param unite
		 *            l'unité
		 */
		@JsonProperty("unite")
		public void setUnite(String unite) {
			this.unite = unite;
		}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other) {
		return EqualsBuilder.reflectionEquals(this, other);
	}

	/**
	 * Retourne les autres activités.
	 *
	 * @return les autres activités
	 */
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	/**
	 * Ajoute une propriété et sa valeur.
	 *
	 * @param nom
	 *            le nom de la propriété
	 * @param valeur
	 *            la valeur de la propriété
	 */
	@JsonAnySetter
	public void setAdditionalProperty(String nom, Object valeur) {
		this.additionalProperties.put(nom, valeur);
	}

}
