/*
  Copyright (C) 2014-2015 INRA http://www.inra.fr
  meansrecordapi - RECORD platform web services API for MEANS platform

  This file is part of meansrecordapi.

  meansrecordapi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  meansrecordapi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
  
  Contributors:
        Meradi Rabah (rabahmeradi@gmail.com) - initial API and implementation"
 */

package record.ws.api.resultat.micmac.activites;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import record.ws.api.resultat.micmac.OpAgricole;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Contient les données sur une activité de fertilisation.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "date",
    "typeN",
    "apportN",
    "unité",
    "opAgricole",
    "nbPassages"
})
public class Fertilisation {

    /** La date de l'activité. */
    @JsonProperty("date")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd", timezone="CET")
    private Date date;
    
    /** Le type de fertilisant. */
    @JsonProperty("typeN")
    private String typeN;
    
    /** La quantité de fertilisant utilisé. */
    @JsonProperty("apportN")
    private double apportN;
    
    /** L'unité  de mesure utilisée. */
  	@JsonProperty("unite")
  	private String unite;
    
    /** L'opération agricole. */
    @JsonProperty("opAgricole")
    @Valid
    private OpAgricole opAgricole;
    
    /** Le nombre de passages. */
    @JsonProperty("nbPassages")
    private long nbPassages;
    
    /** Les autres propriétés. */
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
	 * Retourne le date de l'activité.
	 *
	 * @return la date de l'activité
	 */
    @JsonProperty("date")
    public Date getDate() {
        return date;
    }

    /**
	 * Initialise la date de l'activité.
	 *
	 * @param date
	 *            la date de l'activité
	 */
    @JsonProperty("date")
    public void setDate(Date date) {
        this.date = date;
    }

    /**
	 * Retourne le type de fertilisant.
	 *
	 * @return le type de fertilisant
	 */
    @JsonProperty("typeN")
    public String getTypeN() {
        return typeN;
    }

    /**
	 * Initialise le type de fertilisant.
	 *
	 * @param typeN
	 *            le type de fertilisant
	 */
    @JsonProperty("typeN")
    public void setTypeN(String typeN) {
        this.typeN = typeN;
    }

    /**
	 * Retourne la quantité de fertilisant.
	 *
	 * @return la quantité de fertilisant
	 */
    @JsonProperty("apportN")
    public double getApportN() {
        return apportN;
    }

    /**
	 * Initialise la quantité de fertilisant.
	 *
	 * @param apportN
	 *            la quantité de fertilisant
	 */
    @JsonProperty("apportN")
    public void setApportN(double apportN) {
        this.apportN = apportN;
    }

    /**
	 * Retourne l'opération agricole.
	 *
	 * @return l'opération agricole
	 */
    @JsonProperty("opAgricole")
    public OpAgricole getOpAgricole() {
        return opAgricole;
    }

    /**
	 * Initialise l'opération agricole.
	 *
	 * @param opAgricole
	 *            l'opération agricole
	 */
    @JsonProperty("opAgricole")
    public void setOpAgricole(OpAgricole opAgricole) {
        this.opAgricole = opAgricole;
    }

    /**
	 * Retourne le nombre de passages.
	 *
	 * @return le nombre passages
	 */
    @JsonProperty("nbPassages")
    public long getNbPassages() {
        return nbPassages;
    }

    /**
	 * Initialise le nombre passages.
	 *
	 * @param nbPassages
	 *            le nombre passages
	 */
    @JsonProperty("nbPassages")
    public void setNbPassages(long nbPassages) {
        this.nbPassages = nbPassages;
    }
    
    /**
 	 * Retourne l'unité de mesure.
 	 *
 	 * @return l'unité
 	 */
 	@JsonProperty("unite")
 	public String getUnite() {
 		return unite;
 	}

 	/**
 	 * Initialise l'unité de mesure.
 	 *
 	 * @param unite
 	 *            l'unité
 	 */
 	@JsonProperty("unite")
 	public void setUnite(String unite) {
 		this.unite = unite;
 	}
    

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    /**
	 * Retourne les propriétés de cette activité.
	 *
	 * @return les autres propriétés
	 */
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    /**
	 * Ajoute une propriété et sa valeur.
	 *
	 * @param nom
	 *            le nom de la propriété
	 * @param valeur
	 *            la valeur de la propriété
	 */
    @JsonAnySetter
    public void setAdditionalProperty(String nom, Object valeur) {
        this.additionalProperties.put(nom, valeur);
    }

}
