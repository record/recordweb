/*
  Copyright (C) 2014-2015 INRA http://www.inra.fr
  meansrecordapi - RECORD platform web services API for MEANS platform

  This file is part of meansrecordapi.

  meansrecordapi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  meansrecordapi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
  
  Contributors:
        Meradi Rabah (rabahmeradi@gmail.com) - initial API and implementation"
 */
package record.ws.api.micmac.examples;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.security.auth.login.FailedLoginException;

import record.ws.api.API;
import record.ws.api.erreurs.ErreurSimulation;
import record.ws.api.erreurs.NonAuthentifie;
import record.ws.api.requete.Requete;
import record.ws.api.requete.micmac.RequeteMicMac;
import record.ws.api.resultat.micmac.CoProduit;
import record.ws.api.resultat.micmac.FluxPolluant;
import record.ws.api.resultat.micmac.ResultatMicMac;
import record.ws.api.resultat.micmac.SolErosion;
import record.ws.api.resultat.micmac.Valeur;
import record.ws.api.resultat.micmac.activites.Activites;
import record.ws.api.resultat.micmac.activites.Fertilisation;
import record.ws.api.resultat.micmac.activites.TraitementsPhytosanitaire;
import record.ws.api.resultat.micmac.activites.PostRecolte;
import record.ws.api.resultat.micmac.activites.Recolte;
import record.ws.api.resultat.micmac.activites.Semis;
import record.ws.api.resultat.micmac.activites.TravailSol;
import record.ws.api.resultat.micmac.cultures.Culture;
import record.ws.api.tests.outils.PrettyPrinter;

/**
 * Exemple d'utilisation de l'API. On lance la simulation du modèle MicMac et
 * affiche le résultat de simulation.
 *
 */
public class ExempleMicMac {

	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	public static void main(String[] args) {
		Logger log = Logger.getLogger("test");

		API api = new API("http://147.99.96.186");
		log.info("Authentification aux services web RECORD");
		try {
			api.login("MEANS", "MEANS2014".toCharArray());
			log.info("Authentification réussie !");
		} catch (FailedLoginException e) {
			log.warning("Erreur d'authentification :");
			log.warning(e.getLocalizedMessage());
		} catch (IOException e) {
			log.warning("Erreur de connexion aux services web :");
			log.warning(e.getLocalizedMessage());
		}
		Requete requete = new RequeteMicMac("BleDur", "Feverole", "Moutarde",
				"Auzeville");
		log.info("Damande de simulation du modèle MicMac avec culture intermédiaire");
		ResultatMicMac res;
		try {
			res = (ResultatMicMac) api.simuler("MicMac", requete,
					ResultatMicMac.class);
			log.info("La simulation s'est bien déroulée, affichage des résultats :");
			System.out.println("Flux Polluants :");
			printFluxPolluants(res.getFluxPolluant());
			printCulturePrecedente(res.getCulturePrecedente());
			printCultureIntermediaire(res.getCultureIntermediaire());
			printCulturePrincipale(res.getCulturePrincipale());
			printCoProduit(res.getCoProduit());
			printSolErosion(res.getSolErosion());
		} catch (ErreurSimulation e) {
			log.warning("Erreur de simuylation :");
			log.warning(e.getLocalizedMessage());
		} catch (NonAuthentifie e) {
			log.warning("Erreur d'authentification :");
			log.warning(e.getLocalizedMessage());
		} catch (IOException e) {
			log.warning("Erreur de connexion :");
			log.warning(e.getLocalizedMessage());
		}

	}

	private static void printSolErosion(SolErosion solErosion) {
		System.out.println("Sol érosion :");
		System.out.println("Méthode de travail du sol la plus érosive : "
				+ solErosion.getMethodeTravailSolPlusErosive());

	}

	private static void printCoProduit(CoProduit coProduit) {
		System.out.println("Co-produit :");
		String[][] tab = new String[][] {
				{ "Nom", "Rendement", "Contenu en carbone" },
				{ coProduit.getNom(), String.valueOf(coProduit.getRendement()),
						String.valueOf(coProduit.getContenuCarbone()) } };

		PrettyPrinter p = new PrettyPrinter(System.out);
		p.print(tab);

	}

	private static void printCultureIntermediaire(Culture cultureIntermediaire) {
		System.out.println("Culture intermediéaire :");
		System.out.println("Nom : " + cultureIntermediaire.getNom());
		System.out
				.println("Rendement : " + cultureIntermediaire.getRendement());
		printRecolte(cultureIntermediaire.getActivites().getRecolte());

	}

	private static void printCulturePrecedente(Culture culturePrecedente) {
		System.out.println("Culture Précédente :");
		System.out.println("Nom : " + culturePrecedente.getNom());
		printSemis(culturePrecedente.getActivites().getSemis());

	}

	private static void printCulturePrincipale(Culture culturePrincipale) {
		System.out.println("Culture Principale :");
		System.out.println("Nom : " + culturePrincipale.getNom());
		System.out.println("Rendement : " + culturePrincipale.getRendement());
		printActvities(culturePrincipale.getActivites());

	}

	private static void printActvities(Activites activites) {
		printFertilisation(activites.getFertilisation());
		printRecolte(activites.getRecolte());
		printSemis(activites.getSemis());
		printTraitement(activites.getTraitementsPhytosanitaires());
		printTravailSol(activites.getTravailSol());
		printPostRecolte(activites.getPostRecolte());

	}

	private static void printPostRecolte(List<PostRecolte> postRecoltes) {
		List<String[]> list = new ArrayList<String[]>();
		if (postRecoltes.isEmpty()) {
			System.out.println("Pas d'activités de post-récolte !");
			return;
		}
		System.out.println("Activités de post-récolte :");
		String[] header = new String[] { "Date", "Nom", "Quantité", "Unité" };
		list.add(header);
		for (PostRecolte postRecolte : postRecoltes) {
			String[] tab = new String[] { sdf.format(postRecolte.getDate()),
					postRecolte.getNom(),
					String.valueOf(postRecolte.getQuantite()),
					postRecolte.getUnite() };
			list.add(tab);
		}

		String[][] tab = new String[list.size()][];
		tab = list.toArray(tab);
		PrettyPrinter p = new PrettyPrinter(System.out);
		p.print(tab);

	}

	private static void printTravailSol(List<TravailSol> travailSol) {
		List<String[]> list = new ArrayList<String[]>();
		if (travailSol.isEmpty()) {
			System.out.println("Pas d'activités de travail de sol !");
			return;
		}
		System.out.println("Activités de travail de sol :");
		String[] header = new String[] { "Date", "Nom", "Profondeur",
				"Nombre de passages" };
		list.add(header);
		for (TravailSol travail : travailSol) {
			String[] tab = new String[] { sdf.format(travail.getDate()),
					travail.getNom(), String.valueOf(travail.getProfondeur()),
					String.valueOf(travail.getNbPassages()) };
			list.add(tab);
		}

		String[][] tab = new String[list.size()][];
		tab = list.toArray(tab);
		PrettyPrinter p = new PrettyPrinter(System.out);
		p.print(tab);

	}

	private static void printTraitement(
			List<TraitementsPhytosanitaire> traitementsPhytosanitaires) {
		if (traitementsPhytosanitaires.isEmpty()) {
			System.out
					.println("Pas d'activités de traitements phytosanitaires !");
			return;
		}
		System.out.println("Traitements phytosanitaires :");
		List<String[]> list = new ArrayList<String[]>();
		String[] header = new String[] { "Date", "TypeN", "ApportN", "Unité",
				"Pourcentage", "Opération agricole", "Nombre de passages" };
		list.add(header);
		for (TraitementsPhytosanitaire traitement : traitementsPhytosanitaires) {
			String[] tab = new String[] {
					sdf.format(traitement.getDate()),
					traitement.getNature(),
					String.valueOf(traitement.getQuantite()),
					traitement.getUnite(),
					String.valueOf(traitement.getPourcentage()),
					"(" + traitement.getOpAgricole().getType() + ", "
							+ traitement.getOpAgricole().getDuree() + ")",
					String.valueOf(traitement.getNbPassages()) };
			list.add(tab);
		}

		String[][] tab = new String[list.size()][];
		tab = list.toArray(tab);
		PrettyPrinter p = new PrettyPrinter(System.out);
		p.print(tab);

	}

	private static void printSemis(Semis semis) {
		if (semis == null) {
			System.out.println("Pas d'activité de Semis !");
			return;
		}
		System.out.println("Activité de semis :");
		String[][] tab = new String[][] {
				{ "Date de semis", "Quantité", "Unité", "Opération agricole",
						"Nombre de passages" },
				{
						sdf.format(semis.getDate()),
						String.valueOf(semis.getQuantite()),
						semis.getUnite(),
						"(" + semis.getOpAgricole().getType() + ", "
								+ semis.getOpAgricole().getDuree() + ")",
						String.valueOf(semis.getNbPassages()) } };

		PrettyPrinter p = new PrettyPrinter(System.out);
		p.print(tab);

	}

	private static void printRecolte(Recolte recolte) {
		if (recolte == null) {
			System.out.println("Pas d'activité de Récolte !");
			return;
		}
		System.out.println("Activité de Récolte :");
		String[][] tab = new String[][] {
				{ "Date de récolte", "Opération agricole" },
				{
						sdf.format(recolte.getDate()),
						"(" + recolte.getOpAgricole().getType() + ", "
								+ recolte.getOpAgricole().getDuree() + ")" } };

		PrettyPrinter p = new PrettyPrinter(System.out);
		p.print(tab);

	}

	private static void printFertilisation(List<Fertilisation> fertilisations) {
		List<String[]> list = new ArrayList<String[]>();
		if (fertilisations.isEmpty()) {
			System.out.println("Pas d'activités de fertilisartion !");
			return;
		}
		System.out.println("Activités de fertilisation :");
		String[] header = new String[] { "Date", "TypeN", "ApportN", "Unité",
				"Opération agricole", "Nombre de passages" };
		list.add(header);
		for (Fertilisation fertilisation : fertilisations) {
			String[] tab = new String[] {
					sdf.format(fertilisation.getDate()),
					fertilisation.getTypeN(),
					String.valueOf(fertilisation.getApportN()),
					fertilisation.getUnite(),
					"(" + fertilisation.getOpAgricole().getType() + ", "
							+ fertilisation.getOpAgricole().getDuree() + ")",
					String.valueOf(fertilisation.getNbPassages()) };
			list.add(tab);
		}

		String[][] tab = new String[list.size()][];
		tab = list.toArray(tab);
		PrettyPrinter p = new PrettyPrinter(System.out);
		p.print(tab);

	}

	private static void printFluxPolluants(List<FluxPolluant> fluxPolluant) {
		List<String[]> list = new ArrayList<String[]>();
		String[] header = new String[] { "Nom", "Unité", "Valeurs" };
		list.add(header);
		for (FluxPolluant flux : fluxPolluant) {
			String stringValeur = "";
			for (Valeur valeur : flux.getValeurs()) {
				Date d = valeur.getDate();
				stringValeur += valeur.getValeur() + " (" + sdf.format(d)
						+ "), ";
			}
			stringValeur = stringValeur.substring(0, stringValeur.length() - 2);
			String[] tab = new String[] { flux.getNom(), flux.getUnite(),
					stringValeur };
			list.add(tab);
		}

		String[][] tab = new String[list.size()][];
		tab = list.toArray(tab);
		PrettyPrinter p = new PrettyPrinter(System.out);
		p.print(tab);

	}

}
