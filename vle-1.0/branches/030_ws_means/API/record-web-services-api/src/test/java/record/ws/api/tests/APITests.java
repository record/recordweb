/*
  Copyright (C) 2014-2015 INRA http://www.inra.fr
  meansrecordapi - RECORD platform web services API for MEANS platform

  This file is part of meansrecordapi.

  meansrecordapi is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  meansrecordapi is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
  
  Contributors:
        Meradi Rabah (rabahmeradi@gmail.com) - initial API and implementation"
 */
package record.ws.api.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import javax.security.auth.login.FailedLoginException;

import org.json.JSONException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import record.ws.api.API;
import record.ws.api.auth.Token;
import record.ws.api.erreurs.ErreurSimulation;
import record.ws.api.erreurs.NonAuthentifie;
import record.ws.api.requete.Requete;
import record.ws.api.requete.micmac.RequeteMicMac;
import record.ws.api.resultat.Resultat;
import record.ws.api.resultat.micmac.ResultatMicMac;
import record.ws.outils.Outils;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * La classe qui regroupe les testes unitaires.
 */
public class APITests {

	private static API api;
	private static String username, url;
	private static char[] password;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		username = "MEANS";
		password = "MEANS2014".toCharArray();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		url = "http://147.99.96.186";
		api = new API(url);

	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * On teste la fonction login avec un identifiant et mot de passe corrects.
	 */
	@Test
	public void testSuccessLogin() {
		System.out.println(Thread.currentThread().getStackTrace()[1]
				.getMethodName());
		boolean error = false;
		try {
			api.login(username, password);
		} catch (IOException e) {
			e.printStackTrace();
			error = true;
		} catch (FailedLoginException e) {
			e.printStackTrace();
		}
		if (!error)
			assertNotNull(api.getToken());
	}

	/**
	 * On teste la fonction login avec des données de connexion erronées.
	 */
	@Test
	public void testFailedLogin() {
		System.out.println(Thread.currentThread().getStackTrace()[1]
				.getMethodName());
		boolean error = false;
		String username = "test";
		String password = "badpassword";
		try {
			api.login(username, password);
		} catch (IOException e) {
			e.printStackTrace();
			error = true;
		} catch (FailedLoginException e) {
		}
		if (!error)
			assertNull(api.getToken());
	}

	/**
	 * On teste la fonction simuler avec le modèle MicMac et avec une culture
	 * Intermédiaire.
	 */
	@Test
	public void testSimulerAvecCultureIntermediare() {
		System.out.println(Thread.currentThread().getStackTrace()[1]
				.getMethodName());
		Requete request = new RequeteMicMac("BleDur", "Feverole", "Moutarde",
				"Auzeville");
		Resultat res = null;
		try {
			api.login(username, password);
			res = api.simuler("MicMac", request, ResultatMicMac.class);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NonAuthentifie e) {
			e.printStackTrace();
		} catch (FailedLoginException e) {
		} catch (ErreurSimulation e) {
			e.printStackTrace();
		}
		assertNotNull(res);
	}

	/**
	 * On teste la fonction simuler avec le modèle MicMac et sans culture
	 * Intermédiaire.
	 */
	@Test
	public void testSimulerSansCultureIntermediare() {
		System.out.println(Thread.currentThread().getStackTrace()[1]
				.getMethodName());
		Requete request = new RequeteMicMac("BleDur", "Tournesol", null,
				"Auzeville");
		ResultatMicMac res = null;
		try {
			api.login(username, password);
			res = (ResultatMicMac) api.simuler("MicMac", request,
					ResultatMicMac.class);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NonAuthentifie e) {
			e.printStackTrace();
		} catch (FailedLoginException e) {
		} catch (ErreurSimulation e) {
			e.printStackTrace();
		}
		assertNotNull(res);
	}

	/**
	 * On teste que la fonction de sérialisation.
	 *
	 * @throws JSONException
	 * @throws JsonProcessingException
	 */
	@Test
	public void testSerialiser() throws JSONException, JsonProcessingException {
		System.out.println(Thread.currentThread().getStackTrace()[1]
				.getMethodName());
		Requete requete = new RequeteMicMac("Tournesol", "BleDur", "Moutarde",
				"Auzeville");
		String result = "";
		result = Outils.serialiser(requete);
		String expected = "{\"culturePrecedente\":\"BleDur\",\"cultureIntermediaire\":\"Moutarde\",\"culturePrincipale\":\"Tournesol\",\"region\":\"Auzeville\"}";
		JSONAssert.assertEquals(expected, result, true);

	}

	/**
	 * On teste la fonction de desérialisation.
	 */
	@Test
	public void testDeserialiser() {
		System.out.println(Thread.currentThread().getStackTrace()[1]
				.getMethodName());
		InputStream is = getClass().getClassLoader().getResourceAsStream(
				"response.json");
		ResultatMicMac res = null;
		@SuppressWarnings("resource")
		Scanner s = new Scanner(is).useDelimiter("\\Z");
		try {
			String json = s.next();
			s.close();
			is.close();
			res = (ResultatMicMac) Outils.desrialiser(json, ResultatMicMac.class);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			s.close();
		}

		assertEquals(res.getCultureIntermediaire().getNom(), "Moutarde");
	}

	/**
	 * On teste la fonction qui verifie si un token est valide. On utilise ici
	 * un token non valide.
	 */
	@Test
	public void testTokenEstValide() {
		System.out.println(Thread.currentThread().getStackTrace()[1]
				.getMethodName());
		Token token = new Token("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c"
				+ "2VybmFtZSI6Ik1FQU5TIiwidXNlcl9pZCI6MiwiZ"
				+ "W1haWwiOiJtZWFuc0B0ZXN0LmZyIiwiZXhwIjoxN"
				+ "DA0MDY0NDEwfQ.zEh0G0xSfSKJubyuvX-pOSJBwd"
				+ "8pUwn07ssOTDvsTQc");
		assertFalse(token.isValide());
	}

	/**
	 * On teste la fonction qui vérifie si un token est valide. On récupère un
	 * token valide depuis les services web.
	 */
	@Test
	public void testTokenValide() {
		System.out.println(Thread.currentThread().getStackTrace()[1]
				.getMethodName());
		boolean error = false;
		try {
			api.login(username, password);
		} catch (IOException e) {
			e.printStackTrace();
			error = true;
		} catch (FailedLoginException e) {
		}
		if (!error)
			assertTrue(api.getToken().isValide());
	}
}
