import record.ws.api;

//Instanciation en spécifiant que l'url
API api = new API("http://147.99.96.186");

//Ou avec le port si différent de 80
API api = new API("http://147.99.96.186", 24000);

 //...

 API api = new API(url);
 
 try {
    //authentification
    api.login(username, password);
 } catch (IOException e) {
 // erreur connexion
 } catch (FailedLoginException e) {
 //erreur d'authentification
}

//...

API api = new API(url);
//On construit notre requête
Requete requete = new Requete("BleDur", "Tournesol", "Moutarde",
                "Auzeville");
//L'objet où sera stocké le résultat
ResultatMicMac res = null;
try {
    //On s'authentifie d'abord
    api.login(username, password);
    //On lance la simulation de modèle (ici c'est le modèle MicMac)
    res = api.simuler("MicMac", requete, ResultatMicMac.class);
    //On affiche le résultat
    System.out.println(res);
    //On affiche le résultat sous le format json
    printAsJson(res);
} catch (IOException e) {
    //Erreur de connexion
} catch (NonAuthentifie e) {
    //Erreur d'authentification
} catch (FailedLoginException e) {
    //Erreur d'authentification
} catch (ErreurSimulation e) {
    //Erreur de simulation
}

 //...
 
 //On récupère le résultat de simulation de MicMac
 ResultatMicMac resultat = api.simuler("MicMac", requete, ResultatMicMac.class);
 //On récupère les valeurs des émission de CO2 liée à l'épandage de chaux
 List<Valeur> co2ChauxValeurs = resultat.
                              getFluxValeurs("CO2Chaux");
 //On affiche les valeurs
 System.out.println(co2ChauxValeurs);

  //...
  
  API api = new API(url);
  
  ResultatMicMac resultat = api.simule("MicMac", requete, ResultatMicMac.class);
  
  //On récupère les informations sur la culture précédente
  CulturePrecedente culturePrecedente = resultat.getCulturePrecedente();
  
  //on récupère l'activité de semis
  Semis semis = culturePrecedente.getActivites().getSemis();
  
  //On affiche la date de semis
  System.out.println(semis.getDate());
  
  //...
  
  API api = new API(url);
  
  ResultatMicMac resultat = api.simule("MicMac", requete, ResultatMicMac.class);
  
  //On récupère les informations sur la culture précédente
  CultureIntermediaire cultureIntermediaire = resultat.getCultureIntermediaire();
  
  //on affiche le rendement
  System.out.println(cultureIntermediaire.getRendement());
  
  //on récupère l'activité de récolte
  Recolte recolte = cultureIntermediaire.getActivites().getRecolte();
  
  //On affiche la date de récolte
  System.out.println(recolte.getDate());
  
  //...
  
  API api = new API(url);
  
  ResultatMicMac resultat = api.simule("MicMac", requete, ResultatMicMac.class);
  
  //On récupère les informations sur la culture précédente
  CulturePrincipale culturePrincipale = resultat.getCulturePrincipale();
  
  //on affiche le rendement
  System.out.println(culturePrincipale.getRendement());
  
  //on récupère l'activité de récolte
  Recolte recolte = culturePrincipale.getActivites().getRecolte();
  
  //On affiche la date de récolte
  System.out.println(recolte.getDate());


  //...
  
  API api = new API(url);
  
  ResultatMicMac resultat = api.simule("MicMac", requete, ResultatMicMac.class);
  
  //On récupère les informations sur la culture précédente
  CoProduit coProduit = resultat.getCoProduit();
  
  //On affiche son nom
  System.out.println(coProduit.getNom());
  
  //on affiche le rendement
  System.out.println(coProduit.getRendement());
  
  //On affiche son contenu en carbone
  System.out.println(coProduit.getContenuCarbon())
  

  //...
  
  API api = new API(url);
  
  ResultatMicMac resultat = api.simule("MicMac", requete, ResultatMicMac.class);
  
  //On récupère les informations sur le sol et érosion
  SolErosion solErosion = resultat.getSolErosion();
  
  //On affiche le nom de la méthode de travail du sol la plus érosive
  System.out.println(solErosion.getMethodeTravailSolPlusErosive());
