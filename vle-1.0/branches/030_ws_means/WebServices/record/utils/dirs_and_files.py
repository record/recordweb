#-*- coding:utf-8 -*-

#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File dirs_and_files.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# Methodes concernant la manipulation de repertoires/fichiers
#
#*****************************************************************************

import os.path



# ..
#*****************************************************************************\n
# 'Verifie' le fichier f (existe et il s'agit bien d'un fichier)
#\n***********************************************************************
def verificationFichierOk(f):
    verif = os.path.exists(f) and os.path.isfile(f)
    return verif

# ..
#*****************************************************************************\n
# 'Verifie' le repertoire rep : les liens symboliques ne sont pas acceptes
#\n***********************************************************************


def verificationRepertoireOk(rep):
    verif = os.path.exists(rep) and os.path.isdir(
        rep) and not os.path.islink(rep)
    return verif

# ..
#*****************************************************************************\n
# Determine si repertoire est un lien
#\n***********************************************************************


def isLienRepertoire(rep):
    return (os.path.islink(rep))

# ..
#*****************************************************************************\n
# 'Verifie' le repertoire rep : les liens symboliques sont acceptes
#\n***********************************************************************


def verificationLargeRepertoireOk(rep):
    verif = os.path.exists(rep) and os.path.isdir(rep)
    return verif

# ..
#*****************************************************************************\n
# Determine si nom est un nom de repertoire ou de fichier cache (ie avec
# prefixe '.')
#\n***********************************************************************


def isFichierOuRepertoireCache(nom):
    verif = nom.startswith('.')
    return verif

# ..
#*****************************************************************************\n
# Fournit la liste des fichiers existant sous repertoire rep
#
# Traitement (permissif) cas d'erreur : une erreur sur un repertoire/fichier
# est non bloquante (reaction : le repertoire/fichier en question est ignore
# + un message ecran d'avertissement).
#
#\n***********************************************************************


def getListeFichiersRep(rep):
    liste_noms = []
    if verificationRepertoireOk(rep):
        liste = os.listdir(rep)
        for element in liste:
            try:
                f = element.encode('utf8')
                if verificationFichierOk(os.path.join(rep, f)):
                    liste_noms.append(f)
            except Exception, e:
                diag = "echec : " + str(e) + " => " + element + " ignored"
                print diag
    return liste_noms

# ..
#*****************************************************************************\n
# Fournit la liste des fichiers existant sous repertoire rep qui ne sont
# pas des fichiers caches (ie prefixes '.')
#
# Traitement (permissif) cas d'erreur : une erreur sur un repertoire/fichier
# est non bloquante (reaction : le repertoire/fichier en question est ignore
# + un message ecran d'avertissement).
#
#\n***********************************************************************


def getListeFichiersNonCachesRep(rep):
    liste_noms_absolus = []
    if verificationRepertoireOk(rep):
        liste = os.listdir(rep)
        for element in liste:
            try:
                f = element.encode('utf8')
                if verificationFichierOk(os.path.join(rep, f)) and not isFichierOuRepertoireCache(f):
                    liste_noms_absolus.append(f)
            except Exception, e:
                diag = "echec : " + str(e) + " => " + element + " ignored"
                print diag
    return liste_noms_absolus

# ..
#*****************************************************************************\n
# Fournit la liste des repertoires existant sous repertoire rep
#
# Traitement (permissif) cas d'erreur : une erreur sur un repertoire/fichier
# est non bloquante (reaction : le repertoire/fichier en question est ignore
# + un message ecran d'avertissement).
#
#\n***********************************************************************


def getListeRepertoiresRep(rep):
    liste_noms = []
    if verificationRepertoireOk(rep):
        liste = os.listdir(rep)
        for element in liste:
            try:
                f = element.encode('utf8')
                if verificationRepertoireOk(os.path.join(rep, f)):
                    liste_noms.append(f)
            except Exception, e:
                diag = "echec : " + str(e) + " => " + element + " ignored"
                print diag
    return liste_noms

# ..
#*****************************************************************************\n
# Fournit la liste des repertoires existant sous repertoire rep qui ne sont
# pas des repertoires caches (ie prefixes '.')
#
# Traitement (permissif) cas d'erreur : une erreur sur un repertoire/fichier
# est non bloquante (reaction : le repertoire/fichier en question est ignore
# + un message ecran d'avertissement).
#
#\n***********************************************************************


def getListeRepertoiresNonCachesRep(rep):
    liste_noms_absolus = []
    if verificationRepertoireOk(rep):
        liste = os.listdir(rep)
        for element in liste:
            try:
                f = element.encode('utf8')
                if verificationRepertoireOk(os.path.join(rep, f)) and not isFichierOuRepertoireCache(f):
                    liste_noms_absolus.append(f)
            except Exception, e:
                diag = "echec : " + str(e) + " => " + element + " ignored"
                print diag
    return liste_noms_absolus


# ..
#*****************************************************************************\n
# Fournit la liste des repertoires existant sous repertoire rep qui ne sont
# pas des repertoires caches (ie prefixes '.'), les liens symboliques sont
# acceptes/pris en compte
#
# Traitement (permissif) cas d'erreur : une erreur sur un repertoire/fichier
# est non bloquante (reaction : le repertoire/fichier en question est ignore
# + un message ecran d'avertissement).
#
#\n***********************************************************************
def getListeRepertoiresLargesNonCachesRep(rep):
    liste_noms_absolus = []
    if verificationLargeRepertoireOk(rep):
        liste = os.listdir(rep)
        for element in liste:
            try:
                f = element.encode('utf8')
                if verificationLargeRepertoireOk(os.path.join(rep, f)) and not isFichierOuRepertoireCache(f):
                    liste_noms_absolus.append(f)
            except Exception, e:
                #diag = "echec : " + e.message + " => " + element + " ignored"
                diag = "echec : " + str(e) + " => " + element + " ignored"
                print diag
    return liste_noms_absolus
