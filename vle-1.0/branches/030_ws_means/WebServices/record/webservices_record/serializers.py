# coding=UTF-8
#
# meansrecordws - RECORD platform web services for MEANS platform
#
# Copyright (C) 2014-2015 INRA http://www.inra.fr
#
# This file is part of meansrecordws.
#
# meansrecordws is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# meansrecordws is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#

from rest_framework import serializers
from models import Request


class GeneralInfoSerializer(serializers.Serializer):
    """
        Les informations générales pour le lancement
        d'une simulation.
    """
    debutSimulation = serializers.FloatField()
    dureeSimulation = serializers.FloatField()
    nomExp = serializers.CharField(max_length=30)
    seed = serializers.FloatField()


class CultureSerializer(serializers.Serializer):
    """
        Les données d'une culture.
    """
    nom = serializers.CharField(max_length=30)


class ParametreSerializer(serializers.Serializer):
    """
        Les informations concernant un paramètre VLE.
        Un paramètre VLE est caractérisé par une
        condition, un port et une valeur.
    """
    condition = serializers.CharField(max_length=30)
    port = serializers.CharField(max_length=30)
    valeur = serializers.CharField(max_length=30)


class WsSerializer(serializers.Serializer):
    """
        Un serializer pour les données d'entrées nécessaires
        pour le lancement d'une simulation. On construit à
        partir des données envoyées par le client l'objet
        Request.
    """
    culturePrincipale = serializers.CharField(max_length=30)
    culturePrecedente = serializers.CharField(max_length=30)
    cultureIntermediaire = serializers.CharField(max_length=30, required=False)
    region = serializers.CharField(max_length=30)
    info = GeneralInfoSerializer(required=False)
    pacrelle = serializers.CharField(max_length=30, required=False)
    scenario = serializers.CharField(max_length=30, required=False)
    parametres = ParametreSerializer(many=True, required=False)

    def __init__(self, *args, **kwargs):
        super(WsSerializer, self).__init__(*args, **kwargs)

    def restore_object(self, attrs, instance=None):
        """
            On déserialise les données envoyées par le client
            pour construire Un objet Request.
        """
        if instance is not None:
            instance.culturePrincipale = attrs.get('culturePrincipale',
                                         instance.culturePrincipale)
            instance.culturePrecedente = attrs.get('culturePrecedente',
                                      instance.culturePrecedente)
            instance.region = attrs.get('region', instance.region)
            instance.cultureIntermediaire = attrs.get('cultureIntermediaire',
                                          instance.cultureIntermediaire)
            return instance
        return Request(**attrs)

    def validate(self, attrs):
        """
            Valide la requête de l'utilisateur. On vérifié
            s'il y a pas eu d'erreur lors de l'initialisation
            de scénario.
        """
        # On construit l'objet Request
        request = Request(**attrs)
        # On vérifie s'il y eu des erreurs dans
        # la requête de client.
        if request.erreur:
            raise serializers.ValidationError(
                        request.erreur)
        return attrs


class OpAgricoleSerializer(serializers.Serializer):
    """
        Un serializer pour une opération agricole.
    """
    type = serializers.CharField(max_length=30)
    duree = serializers.FloatField()


class SemisSerializer(serializers.Serializer):
    """
        Contient les informations concerant une activité
        de semis.
    """
    date = serializers.DateField()
    quantite = serializers.FloatField()
    unite = serializers.CharField(max_length=30)
    opAgricole = OpAgricoleSerializer()
    nbPassages = serializers.IntegerField()


class FertilisationSerializer(serializers.Serializer):
    date = serializers.DateField()
    typeN = serializers.CharField(max_length=30)
    apportN = serializers.FloatField()
    unite = serializers.CharField(max_length=30)
    opAgricole = OpAgricoleSerializer()
    nbPassages = serializers.FloatField()


class TraitementsPhytosanitairesSerializer(serializers.Serializer):
    date = serializers.DateField()
    quantite = serializers.FloatField()
    nature = serializers.CharField(max_length=30)
    unite = serializers.CharField(max_length=30)
    pourcentage = serializers.FloatField()
    opAgricole = OpAgricoleSerializer()
    nbPassages = serializers.FloatField()


class AutreSerializer(serializers.Serializer):
    date = serializers.DateField()
    nature = serializers.CharField(max_length=30)
    opAgricole = OpAgricoleSerializer()
    nbPassages = serializers.FloatField()


class IrrigationFertirrigationSerializer(serializers.Serializer):
    date = serializers.DateField()
    sourceEnergie = serializers.CharField(max_length=30)
    quantiteEau = serializers.FloatField()
    unite = serializers.CharField(max_length=30)
    materiel = serializers.CharField()


class TravailSolSerializer(serializers.Serializer):
    date = serializers.DateField()
    nom = serializers.CharField(max_length=30)
    profondeur = serializers.IntegerField()
    nbPassages = serializers.FloatField()


class RecolteSerializer(serializers.Serializer):
    date = serializers.DateField()
    opAgricole = OpAgricoleSerializer()


class SolErosionSerializer(serializers.Serializer):
    methodeTravailSolPlusErosive = serializers.CharField(max_length=30)


class ActiviteCulturePrincipaleSerializer(serializers.Serializer):
    fertilisation = FertilisationSerializer(many=True)
    travailSol = TravailSolSerializer(many=True)
    semis = SemisSerializer()
    recolte = RecolteSerializer()
    irrigationFertirrigation = IrrigationFertirrigationSerializer(
                                                            many=True)
    traitementsPhytosanitaires = TraitementsPhytosanitairesSerializer(
                                                            many=True)


class ActiviteCulturePrecedenteSerializer(serializers.Serializer):
    semis = SemisSerializer()


class ActiviteCultureIntermediaireSerializer(serializers.Serializer):
    recolte = RecolteSerializer()


class CulturePrincipaleSerializer(serializers.Serializer):
    nom = serializers.CharField(max_length=30)
    rendement = serializers.FloatField()
    activites = ActiviteCulturePrincipaleSerializer()


class CulturePrecedenteSerializer(serializers.Serializer):
    nom = serializers.CharField(max_length=30)
    activites = ActiviteCulturePrecedenteSerializer()


class cultureIntermediaireSerializer(serializers.Serializer):
    nom = serializers.CharField(max_length=30)
    rendement = serializers.FloatField()
    activites = ActiviteCultureIntermediaireSerializer()


class ValeurSerializer(serializers.Serializer):
    date = serializers.DateField()
    valeur = serializers.FloatField()


class FluxPolluantSerializer(serializers.Serializer):
    nom = serializers.CharField(max_length=30)
    unite = serializers.CharField(max_length=30)
    valeurs = ValeurSerializer()


class CoProduitSerializer(serializers.Serializer):
    nom = serializers.CharField(max_length=30)
    rendement = serializers.FloatField()
    contenuCarbone = serializers.FloatField()


class ResultatSerializer(serializers.Serializer):
    fluxPolluant = FluxPolluantSerializer(many=True)
    solErosion = SolErosionSerializer()
    culturePrincipale = CulturePrincipaleSerializer()
    culturePrecedente = CulturePrecedenteSerializer()
    cultureIntermediaire = cultureIntermediaireSerializer()
    coProduit = CoProduitSerializer()


class ScenarioSerializer(serializers.Serializer):
    nom_scenario = serializers.CharField(max_length=30)
    nom_model = serializers.CharField(max_length=30)
