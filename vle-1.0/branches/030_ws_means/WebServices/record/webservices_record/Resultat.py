# coding=UTF-8
#
# meansrecordws - RECORD platform web services for MEANS platform
#
# Copyright (C) 2014-2015 INRA http://www.inra.fr
#
# This file is part of meansrecordws.
#
# meansrecordws is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# meansrecordws is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#


class OpAgricole(object):
    type = "Inconnu"
    duree = 0


class Culture(object):

    def __init__(self, nom, rendement):
            self.nom = nom
            self.rendement = rendement
            self.activites = Activites()

    def ajouterActivite(self, activite):
        self.activites.ajouterActivite(activite)

    def __str__(self):
        return self.nom


class Fertilisation(object):

    def __init__(self, date, typeN, apportN, unite="Inconnu"):
        self.date = date
        self.apportN = apportN
        self.typeN = typeN
        self.unite = unite
        self.opAgricole = OpAgricole()
        self.nbPassages = 1


class TraitementsPhytosanitaires(object):

    def __init__(self, date, nature, quantite, pourcentage=100,
                 unite="Inconnu"):
        self.date = date
        self.quantite = quantite
        self.nature = nature
        self.unite = unite
        self.pourcentage = pourcentage
        self.opAgricole = OpAgricole()
        self.nbPassages = 1


class Autre(object):

    def __init__(self, date, nature, quantite, unite="Inconnu"):
        self.date = date
        self.quantite = quantite
        self.unite = unite
        self.opAgricole = OpAgricole()
        self.nbPassages = 1


class IrrigationFertirrigation(object):

    def __init__(self, date, apportE, sourceEnergie="Inconnu",
                 materiel="Inconnu", unite="Inconnu"):
        self.date = date
        self.sourceEnergie = sourceEnergie
        self.ApportEau = apportE
        self.unite = unite
        self.materiel = materiel


class TravailSol(object):

    def __init__(self, date, nom, profondeur):
        self.nom = nom
        self.profondeur = profondeur
        self.date = date
        self.nbPassages = 1


class Recolte(object):

    def __init__(self, date, typeRec):
        self.date = date
        self.typeRec = typeRec
        self.opAgricole = OpAgricole()


class SolErosion(object):

    def __init__(self, methodeTravailSolPlusErosive="Inconnu"):
        self.methodeTravailSolPlusErosive = methodeTravailSolPlusErosive


class Semis(object):

    def __init__(self, date, densite, unite="Inconnu"):
        self.date = date
        self.quantite = self.getQuantite(densite)
        self.unite = unite
        self.opAgricole = OpAgricole()
        self.nbPassages = 1

    def getQuantite(self, densite):
        return densite


class Activites(object):

    def __init__(self):
        self.fertilisation = []
        self.travailSol = []
        self.semis = None
        self.recolte = None
        self.irrigationFertirrigation = []
        self.traitementsPhytosanitaires = []

    def ajouterActivite(self, activite):
        if isinstance(activite, Fertilisation):
            self.fertilisation.append(activite)
        elif isinstance(activite, TravailSol):
            self.travailSol.append(activite)
        elif isinstance(activite, Semis):
            self.semis = activite
        elif isinstance(activite, Recolte):
            self.recolte = activite
        elif isinstance(activite, IrrigationFertirrigation):
            self.irrigationFertirrigation.append(activite)
        elif isinstance(activite, TraitementsPhytosanitaires):
            self.traitementsPhytosanitaires.append(activite)


class Valeur(object):

    def __init__(self, date, value):
        self.date = date
        self.valeur = value


class Flux(object):

    def __init__(self, nom, unite="Inconnu"):
        self.nom = nom
        self.unite = unite
        self.valeurs = []


class FluxPolluant(object):
    flux = []

    def ajouter(self, flux):
        self.flux.append(flux)


class CoProduit(object):

    def __init__(self):
        self.nom = "Inconnu"
        self.rendement = 0
        self.contenuCarbone = 0


class CulturePrincipale(object):
    def __init__(self, culture):
            self.nom = culture.nom
            self.rendement = culture.rendement
            self.activites = culture.activites


class CulturePrecedente(object):
    def __init__(self, culture):
            self.nom = culture.nom
            self.rendement = culture.rendement
            self.activites = culture.activites

    def ajouterActivite(self, activite):
        self.activites.ajouterActivite(activite)


class CultureIntermediaire(object):
    def __init__(self, culture):
            self.nom = culture.nom
            self.rendement = culture.rendement
            self.activites = culture.activites


class Resultat(object):

    def __init__(self, fluxPolluant, cultures, solErosion=None):
        self.fluxPolluant = fluxPolluant
        self.solErosion = solErosion
        self.culturePrincipale = CulturePrincipale(cultures['culturePrincipale'])
        self.culturePrecedente = CulturePrecedente(cultures['culturePrecedente'])
        if 'cultureIntermediaire' in cultures:
            self.cultureIntermediaire = CultureIntermediaire(cultures['cultureIntermediaire'])
        else:
            self.cultureIntermediaire = None
        self.coProduit = CoProduit()
