# coding=UTF-8
#
# meansrecordws - RECORD platform web services for MEANS platform
#
# Copyright (C) 2014-2015 INRA http://www.inra.fr
#
# This file is part of meansrecordws.
#
# meansrecordws is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# meansrecordws is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#

from django.db import models
from django.contrib.auth.models import User

from record.webservices_record.Log import get_logger
logger = get_logger(__name__)


#******************************************************************************
# Model and its scenarios list (in order to choose one of them)
#
# application_associee :
# For the moment the applications_associees list is not proposed/treated. !!!
# Yet the application_associee is used as a filter !!!
#
#******************************************************************************

class WsModel(models.Model):
    VERSIONS_CHOICES = (
        ('vle-0.8.9', 'Version 0.8.9'),
        ('vle-1.0', 'Version 1.0'),
        ('vle-1.1', 'Version 1.1'),
    )
    YES_NO_CHOICES = (
        (True, "Oui"),
        (False, "Non"),
    )
    nom = models.CharField(max_length=30, blank=False, primary_key=True)

    modele_active = models.BooleanField(default=True)
    accord_exploitation = models.BooleanField(u"Accord d'exploitation",
                                           max_length=3, blank=False,
                                           choices=YES_NO_CHOICES)
    version_vle = models.CharField("Version de VLE", max_length=20,
                                   choices=VERSIONS_CHOICES, blank=False)

    nom_repertoire_lot_pkgs = models.CharField(max_length=50, blank=False)
    nom_pkg = models.CharField(max_length=50, blank=False)

    responsable = models.ForeignKey(
        User, related_name='responsabilities',
        blank=False)

    responsable_scientifique = models.ForeignKey(
        User, related_name='scientific_responsabilities',
        blank=False)

    responsable_informatique = models.ForeignKey(
        User, related_name='software_responsabilities',
        blank=False)

    users = models.ManyToManyField(User, related_name='users')

    def __str__(self):
        return self.nom

    class Meta:
        verbose_name = 'Modèle'


class WsParameter(models.Model):
    TYPE_CHOICES = (
        ('int', 'Integer'),
        ('bool', 'Boolean'),
        ('list', 'List'),
        ('array', 'array'),
        ('str', 'String'),
        ('file', 'File'),
    )
    nom = models.CharField(max_length=30, blank=False)
    type = models.CharField(max_length=10, choices=TYPE_CHOICES)
    portname = models.CharField(max_length=30)

    def __str__(self):
        return self.nom

    class Meta:
        verbose_name = 'Paramètre'


class ParameterDescription(models.Model):
    parameter = models.ForeignKey(WsParameter,
                                  related_name="paramerterDescription")
    language = models.CharField(max_length=5, blank=False)
    description = models.TextField()

    class Meta:
        unique_together = (("parameter", "language"),)
        verbose_name = 'Description d\'un paramètre'
        verbose_name_plural = "Descriptions des paramètres"


class WsModelScenario(models.Model):

    nom_scenario = models.CharField(max_length=30, blank=False)

    wsmodel = models.ForeignKey(WsModel, related_name='wsmodelscenarios')

    def __unicode__(self):
        return self.nom_scenario

    class Meta:
        verbose_name = 'Scénario'


class WsScenarioDescription(models.Model):
    scenario = models.ForeignKey(WsModelScenario,
                                 related_name="scenariDescription")
    language = models.CharField(max_length=5, blank=False)
    description = models.TextField()

    class Meta:
        unique_together = (("scenario", "language"),)
        verbose_name = 'Description d\'un scénario'
        verbose_name_plural = "Descriptions des scénarios"


class WsScenarioParameters(models.Model):
    scenario = models.ForeignKey(WsModelScenario)
    parameter = models.ForeignKey(WsParameter)

    class Meta:
        unique_together = (("scenario", "parameter"),)
        verbose_name = 'Paramètre d\'un scénario'
        verbose_name_plural = 'Pramètres des scénarios'


class Request(object):

    def __init__(self, culturePrincipale,
                 culturePrecedente,
                 region, cultureIntermediaire=None):
        # TODO: Il faut voir si on peut considrer la Moutarde par
        # exemple comme une culture principale.
        self.culturesPrincipales = [u'Tournesol', u'BleDur', u'Sorgho',
                                    u'Feverole']
        self.culturesIntermediaires = [u'Moutarde', u'Avoine', u'Vesce']
        self.scenarios_valides = ["BI_TB", "BI_BS", "BI_ST", "TBI_AFB",
                                  "TBI_BVT", "TBI_TAF", "TBI_FMB"]
        self.regions = [u'Auzeville']
        self.culturePrincipale = culturePrincipale
        self.culturePrecedente = culturePrecedente
        self.region = region
        self.cultureIntermediaire = cultureIntermediaire
        self.scenario = None
        self.nom_scenario = None
        self.erreur = None
        self.set_scenario()
        self.set_nom_scenario()
        if self.nom_scenario not in self.scenarios_valides:
            logger.info(u"Scénario nom pris en charge %s", self.nom_scenario)
            logger.info(self.scenarios_valides)
            self.erreur = u"Scénario non pris en charge !" + str(self.scenarios_valides)
            self.nom_scenario = None
        if self.region not in self.regions:
            self.erreur = u"Région non disponible\n"
            self.erreur += u"Les régions disponibles sont : "
            self.erreur += str(self.regions)
            self.nom_scenario = None

    def set_nom_scenario(self):
        self.nom_scenario = ""
        if self.cultureIntermediaire:
            self.nom_scenario = "TBI_"
            self.nom_scenario += self.culturePrecedente[0]
            self.nom_scenario += self.cultureIntermediaire[0]
            self.nom_scenario += self.culturePrincipale[0]
        else:
            self.nom_scenario = "BI_"
            self.nom_scenario += self.culturePrecedente[0]
            self.nom_scenario += self.culturePrincipale[0]

    def set_scenario(self):
        if not self.valideCultures():
            self.scenario = None
            return
        else:
            if not self.cultureIntermediaire:
                self.scenario = WsModelScenario.objects.get(
                                nom_scenario="MicMac_BI_TBS_F1")
            else:
                self.scenario = WsModelScenario.objects.get(
                                nom_scenario="MicMac_TBI_AFMBVT_I1")

    def valideCultures(self):
        if self.culturePrincipale not in self.culturesPrincipales:
            self.erreur = self.culturePrincipale + "n'existe pas !\n"
            self.erreur += "Les cultures possibles sont : "
            self.erreur += str(self.culturesPrincipales)
            return False
        if self.culturePrecedente not in self.culturesPrincipales:
            self.erreur = self.culturePrecedente + + "n'existe pas !"
            self.erreur += "Les cultures possibles sont : "
            self.erreur += str(self.culturesPrincipales)
            return False
        if self.cultureIntermediaire is not None and \
                self.cultureIntermediaire not in self.culturesIntermediaires:
            self.erreur = self.cultureIntermediaire + "n'existe pas !"
            self.erreur += u"Les cultures intermédiaires possibles sont : "
            self.erreur += str(self.culturesIntermediaires)
            return False
        return True
