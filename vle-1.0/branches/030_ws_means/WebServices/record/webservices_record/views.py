# coding=UTF-8
#
# meansrecordws - RECORD platform web services for MEANS platform
#
# Copyright (C) 2014-2015 INRA http://www.inra.fr
#
# This file is part of meansrecordws.
#
# meansrecordws is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# meansrecordws is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from models import WsModel
from serializers import WsSerializer, ResultatSerializer
from rest_framework import parsers
from django.conf import settings
import utils

from django.core.exceptions import ObjectDoesNotExist
import os
import glob

from models import WsModelScenario
from record.webservices_record.simulation import SimulationVle

from Log import get_logger

LOGGER = get_logger(__name__)


class Simulation(APIView):
    u"""
        Lancement de la simulation d'un modèle
    """
    permission_classes = (IsAuthenticated,)
    parser_classes = (parsers.FormParser, parsers.JSONParser,)
    serializer_class = WsSerializer

    def has_permission(self, user, nom_model):
        u"""
            Vérifie si l'utilisateur user a la permission d'utiliser
            le modèle nom_model.
        """
        try:
            model = WsModel.objects.get(nom=nom_model)
            if model.users.filter(id=user.id).exists():
                return True
            else:
                return False
        except ObjectDoesNotExist:
            return False

    def post(self, request, nom_model):
        u"""
            Lance la simulation de modèle nom_model avec les données
            d'entrées envoyées par le client. On vérifie d'abords si
            l'utilisateur a le droit d'utiliser le modèle.
        """
        LOGGER.info(u"demande de simulation de modèle %s par %s",
                    nom_model, request.user)
        if self.has_permission(request.user, nom_model):
            data = request.DATA
            # On construit la requête de client
            serializer = WsSerializer(data=data)
            if not serializer.is_valid():
                return Response(serializer.errors, status=400)
            else:
                sim = SimulationVle(serializer.object)
                dic = sim.run()
                sim.supprimer_repertoire_simulation()
                if not isinstance(dic, dict):
                    LOGGER.info("Erreur de simulation : %s\n ", dic)
                    return Response("Erreur de simulation !",
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR)
                else:
                    res = utils.getResultat(dic, serializer.object)
                    res = ResultatSerializer(res)
                return Response(res.data, status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

simulation = Simulation.as_view()


class Administration(APIView):
    u"""
        Ajoute les scénarios disponibles dans le repertoire 'exp'
        d'un modèle à base de données.
    """
    permission_classes = (IsAdminUser)

    def post(self, request, nom_model):
        u"""
            Ajoute à la base de données tous les scénarios de modèle
            'nom_model'.
        """
        try:
            model = WsModel.objects.get(nom=nom_model)
            rep = self.get_scenarios_rep(model)
            i = self.add_scenarios(rep, model)
            msg = "{0} scénarios ont été crées".format(i)
            return Response(msg, status=status.HTTP_201_CREATED)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def get_scenarios_rep(self, model):
        u"""
            Retourne le repertoire où stockés les scénario
            d'un modèle donné.
        """
        lot_rep = model.nom_repertoire_lot_pkgs
        model_principal = model.nom_pkg
        vle = model.version_vle
        rep = settings.LOTS_PKGS_PATH
        rep = os.path.join(rep, vle)
        rep = os.path.join(rep, lot_rep)
        rep = os.path.join(rep, model_principal)
        rep = os.path.join(rep, 'exp')
        return rep

    def add_scenarios(self, rep, model):
        u"""
            Ajoute tous les scénario de modèle 'model'
            qui se trouvent dans le repertoire 'rep'.
        """
        scenarios = glob.glob(rep + "/*.vpz")
        i = 0
        for scenario in scenarios:
            nom = os.path.basename(scenario)
            nom = os.path.splitext(nom)[0]
            scn = WsModelScenario(nom_scenario=nom, wsmodel=model)
            scn.save()
            i += 1
        return i
administration = Administration.as_view()
