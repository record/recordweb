# coding=UTF-8
#
#------------------------------------------------------------------------------
#
# Authors : Rabah Meradi, for RECORD platform, INRA.
#
#------------------------------------------------------------------------------
#
# meansrecordws - RECORD platform web services for MEANS platform
#
# Copyright (C) 2014-2015 INRA http://www.inra.fr
#
# This file is part of meansrecordws.
#
# meansrecordws is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# meansrecordws is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#

import logging


def get_logger(nom):
    """
        Crée un logger avec le nom passé en paramètre
        et le configure avec le niveau DEBUG. L'affichage
        se fait à la console.
    """
    logger = logging.getLogger(nom)
    logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter(
                '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)

    logger.addHandler(handler)
    return logger
