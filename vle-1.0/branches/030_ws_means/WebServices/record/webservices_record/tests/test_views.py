#-*- coding:utf-8 -*-
#
#------------------------------------------------------------------------------
#
# Authors : Rabah Meradi, for RECORD platform, INRA.
#
#------------------------------------------------------------------------------
#
# meansrecordws - RECORD platform web services for MEANS platform
#
# Copyright (C) 2014-2015 INRA http://www.inra.fr
#
# This file is part of meansrecordws.
#
# meansrecordws is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# meansrecordws is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#

import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), "../../.."))
os.environ['DJANGO_SETTINGS_MODULE'] = 'rwtool.settings'

import unittest
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.compat import patterns
from rest_framework.test import APIClient
import json

from record.webservices_record.models import WsModel, WsModelScenario
from record.webservices_record import utils

urlpatterns = patterns('record.webservices_record',
            (r'^simulation/(?P<nom_model>.*)/', 'views.simulation'),
            (r'^administration/(?P<nom_model>.*)/', 'views.administration')
)
urlpatterns += patterns('',
            (r'^api-token-auth/', 'rest_framework_jwt.views.obtain_jwt_token'),
)


class Test(unittest.TestCase):
    urls = 'record.webservices_record.views'

    def setUp(self):
        self.email = 'test2@example.com'
        self.username = 'test2'
        self.password = 'password'
        self.user = User.objects.create_user(
                       self.username, self.email, self.password)
        self.user = User.objects.get(username=self.username)
        self.data = json.dumps({
            'username': self.username,
            'password': self.password
        })
        self.info = {
    "culturePrincipale": "BleDur",
    "culturePrecedente": "Tournesol",
    "region": "Auzeville"
}
        self.info = json.dumps(self.info)
        self.client = APIClient(enforce_csrf_checks=True)
        self.id = self.user.id
        self.model = WsModel(nom='MicMac', accord_exploitation=True,
                             modele_active=True, responsable_id=self.id,
                             responsable_scientifique_id=self.id,
                             responsable_informatique_id=self.id,
                             nom_pkg="MicMac-Design",
                             version_vle="vle-1.0",
                             nom_repertoire_lot_pkgs="lot_micmac"
                             )
        self.model.save()
        self.model.users.add(self.user)
        scn = WsModelScenario(nom_scenario="MicMac_BI_TBS_F1",
                              wsmodel=self.model)
        scn.save()
        scn = WsModelScenario(nom_scenario="MicMac_TBI_AFMBVT_I1",
                              wsmodel=self.model)
        scn.save()

    def tearDown(self):
        self.user.delete()
        WsModelScenario.objects.all().delete()

    def test_simulation_authentification(self):
        response = self.client.post('/api-token-auth/', self.data,
                                    content_type='application/json')
        token = response.data['token']
        auth = 'JWT {0}'.format(token)
        resp = self.client.post(
            '/simulation/model/', self.info,
             HTTP_AUTHORIZATION=auth,
             content_type='application/json')
        print resp
        self.assertEqual(resp.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_simulation_no_authentification(self):
        response = self.client.post('/simulation/model/')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_simulation_model_avec_scenario(self):
        self.client.get('/administration/MicMac/')
        response = self.client.post('/api-token-auth/', self.data,
                                    content_type='application/json')
        token = response.data['token']
        auth = 'JWT {0}'.format(token)
        resp = self.client.post(
            '/simulation/MicMac/', self.info,
            content_type='application/json;  charset=utf-8',
             HTTP_AUTHORIZATION=auth,
             )
        print resp
        self.assertEqual(resp.status_code, status.HTTP_200_OK)


if __name__ == "__main__":
    unittest.main()
