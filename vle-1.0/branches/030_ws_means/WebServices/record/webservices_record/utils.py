# coding=UTF-8
#
# meansrecordws - RECORD platform web services for MEANS platform
#
# Copyright (C) 2014-2015 INRA http://www.inra.fr
#
# This file is part of meansrecordws.
#
# meansrecordws is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# meansrecordws is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#

import ast
import re
import datetime
from record.webservices_record.Resultat import (Flux, Valeur, Culture,
                                                Resultat, SolErosion)
from Log import get_logger

logger = get_logger(__name__)

# Contient les informations concernant les cultures
cultures = []
# Contient la correspondance entre le nom utilisé par MicMac
# et le type d'actvité.
classes = {
    'Semis': ('Semis', 1),
    'Recolte': ('Recolte', 1),
    'Desherbage': ('TraitementsPhytosanitaires', 2),
    'Fongicide': ('TraitementsPhytosanitaires', 2),
    'Insecticide': ('TraitementsPhytosanitaires', 2),
    'Herbi_semis': ('TraitementsPhytosanitaires', 2),
    'Labour': ('TravailSol', 2),
    'Faux_semis': ('TravailSol', 2),
    'Reprise': ('TravailSol', 2),
    'Preparation_Sol': ('TravailSol', 2),
    'Binage_': ('TravailSol', 2),
    'Enfouissement_CI': ('TravailSol', 2),
    'Herse_etrille_Post_Levee_': ('TravailSol', 2),
    'Apport_P': ('TraitementsPhytosanitaires', 2),
    'Dechaumage': ('TravailSol', 2),
    'Broyage_CI': ('Autre', 2),
    'Binage_1': ('TravailSol', 2),
    'Broyage_cannes': ('Autre', 2),
    'Apport_N': ('Fertilisation', 2),
}

# L'unité de chaque flux polluant
unites = {
    'QCO2sol': 'kg.ha-1',
    'Qem_N2O': 'kg.ha-1',
    'QLES': 'kg.ha-1',
    'CO2Uree': 'kg.ha-1',
    'CO2Chaux': 'kg.ha-1',
    'NH3': 'kg.ha-1',
    'N2ODirect': 'kg.ha-1'
    }

# Le nom à donner pour chaque flux polluant
# On évite de donner le même nom utilisé par
# MicMac.
nomFlux = {
    'QCO2sol': 'QCO2sol',
    'Qem_N2O': 'N2OIndirect',
    'QLES': 'NO3 (QLES)',
    'CO2Uree': 'CO2Uree',
    'CO2Chaux': 'CO2Chaux',
    'NH3': 'NH3',
    'N2ODirect': 'N2ODirect'
    }


def jd2gd(julian_day):
    """
        convertir une date du calendrier julien
        en une date du calendrier grégorien.
    """
    a = (julian_day - 2440587.5) * 86400
    return datetime.datetime.fromtimestamp(a).strftime('%Y-%m-%d')


def ajouterActvite(culture, activite):
    """
        Ajoute une activité à la liste des activités effectués
        sur une culture.
    """
    i = 0
    # On vérifie si la culture existe déjà
    while i < len(cultures):
        if cultures[i].nom == culture:
            cultures[i].ajouterActivite(activite)
            return
        i += 1
    culture = Culture(culture, 0)
    culture.ajouterActivite(activite)
    cultures.append(culture)


def getActiviteClasse(nom):
    """
        Retourne la classe qui correspond à l'activité
        passé en paramètre.
    """
    for item in classes:
        if item in nom:
            return classes[item]
    logger.error(u"Activité non pris en charge %s", nom)
    return None


def process(operation, date, request):
    """
        Analyse les les sorties de décisions et extrait
        le résultat à renvoyer pour MEANS.
    """
    pattern = "\[T?BI_(.*?)_(.*?) , (-?[0-9\.]+)\]"
    date = jd2gd(date)
    regex = re.compile(pattern)
    for match in regex.finditer(operation):
        value = ast.literal_eval(match.group(3))
        nom = getActiviteClasse(match.group(2))
        classe = None
        module = __import__('record.webservices_record.Resultat',
                         fromlist=[nom[0]])
        class_ = getattr(module, nom[0])
        if nom[1] == 1:
            classe = class_(date, value)
        else:
            classe = class_(date, match.group(2), value)
        ajouterActvite(match.group(1), classe)


def getDatesDeRecolte(cultures, request):
    """
        Retourne les dates de récoltes des cultures passées
        en paramètres. Elle vérifie que la date de récolte
        de la principale existe.
    """
    dates = []
    trouv = False
    for culture in cultures:
        if culture.activites.recolte:
            if culture.nom == request.culturePrincipale:
                dates = [culture.activites.recolte.date] + dates
                trouv = True
            else:
                dates.append(culture.activites.recolte.date)
    if trouv:
        return dates
    else:
        logger.error("Pas d'opération de récolte pour la culture principale")
        return None


def getPositions(dates, time):
    i = 0
    j = 0
    positions = []
    while i < len(time) and j < len(dates):
        date = jd2gd(time[i])
        if date == dates[j]:
            positions.append(i)
            j += 1
        i += 1
    return positions


def getNom(key):
    """
        Determine le nom d'une variable à partir de nom
        de la clé utilisée par MicMac. Elle retourne la
        chaîne de caractère après le dernier point de nom
        de la clé.
    """
    key = key.split(".")[-1]
    return nomFlux[key]


def getUnite(nomFlux):
    """
        Retourne l'unité de mesure d'un flux polluant.
    """
    return unites[nomFlux]


def getFlux(dates, nom, val):
    """
        Utilisée pour crée les flux polluants pour
        on lesquels donne des valeurs par défaut.
    """
    unite = getUnite(nom)
    flux = Flux(nom, unite)
    for date in dates:
        value = Valeur(date, val)
        flux.valeurs.append(value)
    return flux


def getValeursDeFluxPolluant(dic, dates, positions):
    """
        Extrait les valeurs des flux polluants aux dates
        données.
    """
    assert len(dates) == len(positions)
    fluxPolluant = []
    for key in dic.keys():
        # les variables à ignorer
        # TODO: Trouver une meilleure façon de le faire.
        if any(x in key for x in ['time', 'totir', 'masec(n)', 'NPlante',
                                  'drat', 'mafruit']):
            continue
        nom = getNom(key)
        flux = Flux(nom)
        i = 0
        while i < len(positions):
            val = dic[key][positions[i]]
            value = Valeur(dates[i], val)
            flux.valeurs.append(value)
            i += 1
        fluxPolluant.append(flux)
    fluxPolluant.append(getFlux(dates, "CO2Uree", 0))
    fluxPolluant.append(getFlux(dates, "CO2Chaux", 0))
    # TODO: On donne des valeurs par défaut les flux suivants
    fluxPolluant.append(getFlux(dates, "NH3", 0))
    fluxPolluant.append(getFlux(dates, "N2ODirect", 0))
    return fluxPolluant


def getFluxPolluant(dates, dic):
    """
        Retourne l'objet qui contient les flux polluants
        intéressant la plate-forme MEANS.
    """
    time = dic['vue_seminaire']['time']
    positions = getPositions(dates, time)
    fluxPolluant = getValeursDeFluxPolluant(dic['vue_seminaire'],
                                         dates, positions)
    return fluxPolluant


def getCultures(cultures, request):
    """
        On retourne les informations de la culture
        précédente, intermédiaire et principale
    """
    dic = {}
    for culture in cultures:
        if culture.nom == request.culturePrincipale:
            dic['culturePrincipale'] = culture
        elif culture.nom == request.culturePrecedente:
            dic['culturePrecedente'] = culture
        elif culture.nom == request.cultureIntermediaire:
            dic['cultureIntermediaire'] = culture
    return dic


def ajouterRendement(cultures, dic):
    """
        Ajoute la valeur de rendement des cultures
        fournies en paramètre. La valeur de rendement
        est celle à la date de récolte.
    """
    # TODO: actuellement on retourne le rendement à la date de récolte
    # Il faut vérifier que c'est à cette  date qu'il faut prendre la
    # valeur à retourner.
    time = dic['vue_seminaire']['time']
    rendements = dic['vue_seminaire']['Top model,Crop:SticsOut.mafruit']
    for culture in cultures:
        if culture.activites.recolte is None:
            continue
        dateRec = culture.activites.recolte.date
        i = 0
        while i < len(time):
            date = jd2gd(time[i])
            i += 1
            if date == dateRec:
                culture.rendement = rendements[i]
                continue


def getResultat(dic, request):
    """
        Construit le résultat de simulation depuis les résultats
         bruts de simulation. On analyse les sorties de la vue
         vue décision (activités) et les données de la vue
         séminaire. On extrait les informations concernant toutes
         les cultures mais à la fin on le résultat intéressant la
         plate-forme MEANS.
    """
    operations = dic['vue_decision'][
                            'Top model,Decision:osbervatorITK.Operations']
    time = dic['vue_decision']['time']

    i = 0
    while i < len(operations):
        operation = operations[i]
        process(operation, time[i], request)
        i += 1
    datesRec = getDatesDeRecolte(cultures, request)
    fluxPolluant = None
    if datesRec:
        t = []
        t.append(datesRec[0])
        fluxPolluant = getFluxPolluant(t, dic)
    ajouterRendement(cultures, dic)
    # TODO: Valeur par défaut
    solErosion = SolErosion()
    culture = getCultures(cultures, request)
    res = Resultat(fluxPolluant, culture, solErosion)
    return res
