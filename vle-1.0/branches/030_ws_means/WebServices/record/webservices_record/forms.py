# coding=UTF-8

# @file record/bd_modeles_record/forms/admin_forms.py
# ..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File admin_forms.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#****************************************************************************
#
# Formulaires d'administration des modeles record enregistres en bd
#
# Saisie (creation...) des modeles record (edition 'read write')
#
#*****************************************************************************

from record.webservices_record.models import WsModel

from django.db import models
from django.conf import settings
from django import forms

import os.path

from django.utils.translation import ugettext as _
from record.utils.dirs_and_files import (getListeRepertoiresNonCachesRep,
                                         getListeRepertoiresLargesNonCachesRep
                                        )



#*****************************************************************************
#
# Des constantes
#
#*****************************************************************************

# Valeur 'vide' d'une liste de choix
choix_vide = ("", "---------")

#*****************************************************************************
# Methodes relatives a la conf
# qui servent dans des criteres de recherche de repertoires/fichiers qui
# seront proposes dans des listes menus
#*****************************************************************************

#*****************************************************************************
# FilePathFieldConfig... :
#
# Classes utilitaires pour les noms des fichiers vpz de configuration
# (nom_fichier_config_web_conf, nom_fichier_config_web_applis
# et nom_fichier_config_dicos) de ModeleRecord
#
#*****************************************************************************

# FilePathField pour des noms de fichiers de configuration (partie commune)

class ParDefautModeleRecord(object):

        # nom
    label_nom = _(u"Nom")
    help_text_nom = _(u"Nom donné au modèle dans la base")
    max_length_nom = 200

    # description
    label_description = _(u"Description")
    help_text_description = _(u"Description du modèle")
    max_length_description = 500

    # url
    label_url = _(u"Plus d'informations")
    max_length_url = 200
    help_text_url = _(
        u"Lien vers une adresse internet donnant plus d'informations sur le modèle")

    # nom_repertoire_lot_pkgs
    label_nom_repertoire_lot_pkgs = _(u"lot des paquets vle")
    max_length_nom_repertoire_lot_pkgs = 500
    help_text_nom_repertoire_lot_pkgs = _(u"Nom (relatif) du répertoire "
    u"contenant l'ensemble des paquets vle du modèle (nom libre).") + " "
    help_text_nom_repertoire_lot_pkgs += _(u"Nom relatif depuis le répertoire")
    help_text_nom_repertoire_lot_pkgs += " " + settings.LOTS_PKGS_PATH
    help_text_nom_repertoire_lot_pkgs += "."

    # nom_repertoire_data
    label_nom_repertoire_data = _(u"répertoire des fichiers de données")
    max_length_nom_repertoire_data = 500
    help_text_nom_repertoire_data = _(u"Nom (relatif) du répertoire contenant les fichiers de données de simulation du modèle (nom libre).") + " " + _(
        u"Nom relatif depuis le répertoire") + " " + settings.DATA_PATH + "."
    help_text_nom_repertoire_data = help_text_nom_repertoire_data + " " + \
        _(u"Valeur") + " 'default" + \
        "' " + _(u"pour le répertoire 'data' du paquet principal")

    # nom_pkg
    label_nom_pkg = _(u"Nom du paquet principal")
    max_length_nom_pkg = 200
    help_text_nom_pkg = _(
        u"Nom du paquet principal (project, sous pkgs) : celui qui contient "
        u"les fichiers vpz scénario de simulation du modèle")

    # mot_de_passe
    label_mot_de_passe = _(u"Mot de passe")
    help_text_mot_de_passe = _(
        u"Mot de passe du modèle ('modele_public' pour un modèle public).")

    # modele_active
    label_modele_active = _(u"Modèle activé")
    max_length_modele_active = 200
    help_text_modele_active = _(
        u"Indique si le modèle doit être activé/proposé ou non dans l'outil "
        u"Web Record ; un modèle qui est désactivé est présent dans la base "
        u"mais non proposé par/au niveau de l'outil Web Record")

    texte_provision = _(u"(provision, signification à définir ultérieurement)")

    # fichiers vpz de configuration

    max_length_config = 500  # commun

    label_nom_fichier_config_web_conf = _(u"configuration web conf")
    help_text_nom_fichier_config_web_conf = _(
        u"Nom (relatif) du fichier vpz de configuration web propre à la conf "
        u"web. Souvent nommé web_conf.vpz mais sans obligation (nom libre).") + " "
    help_text_nom_fichier_config_web_conf += _(
        u"Nom relatif depuis le répertoire") + " " + settings.CONFIGURATION_PATH + "."

    label_nom_fichier_config_web_applis = _(u"configuration web applis")
    help_text_nom_fichier_config_web_applis = _(u"Nom (relatif) du fichier vpz de configuration web propre aux applis web. Souvent nommé web_applis.vpz mais sans obligation (nom libre).") + " " + _(
        u"Nom relatif depuis le répertoire") + " " + settings.CONFIGURATION_PATH + "."

    label_nom_fichier_config_dicos = _(u"configuration dictionnaire")
    help_text_nom_fichier_config_dicos = _(
        u"Nom (relatif) du fichier vpz de configuration propre au "
        u"dictionnaire. Souvent nommé dicos.vpz mais sans obligation "
        u"(nom libre).") + " " + _(
        u"Nom relatif depuis le répertoire") + " " + settings.CONFIGURATION_PATH + "."

    # responsables

    label_responsable = _(u"Responsable")
    help_text_responsable = _(
        u"Correspondant principal. Dans le cas de modèle privé, il distribue "
        "le mot de passe aux personnes de son choix.")

    label_responsable_scientifique = _(u"Responsable scientifique")
    help_text_responsable_scientifique = _(
        u"Correspondant pour les questions scientifiques")

    label_responsable_informatique = _(u"Responsable informatique")
    help_text_responsable_informatique = _(
        u"Correspondant pour les questions informatiques")

    # cr_reception
    label_cr_reception = _(u"Compte rendu de réception")
    max_length_cr_reception = 500
    help_text_cr_reception = _(
        u"Rapport de livraison du modèle (vérifications effectuées à la "
        u"livraison, notes...)")

    # accord_exploitation
    label_accord_exploitation = _(u"Accord d'exploitation")
    max_length_accord_exploitation = 200
    help_text_accord_exploitation = _(
        u"Autorisation d'exploitation du modèle dans l'outil Web Record")



class FilePathFieldConfig(models.FilePathField):

    # determine/construit et retourne liste_choix qui est la liste des paires
    # (nom relatif,nom relatif) associees aux fichiers vpz de configuration
    # (chemin relatif par rapport a self.path).
    #
    # Traitement (permissif) cas d'erreur : une erreur sur un fichier est non
    # bloquante, reaction : le fichier en question est ignore + un message
    # ecran d'avertissement.
    #

    def get_liste_choix(self):

        debutmsg = "AVERTISSEMENT !!! FilePathFieldConfig::get_liste_choix : "

        # recuperation de choices : paires (nom_absolu,nom_depuis_path)
        defaults = {'path': self.path,
                    'match': self.match,
                    'recursive': self.recursive,
                    'form_class': forms.FilePathField, }
        obj = super(models.FilePathField, self).formfield(**defaults)
        choices = obj.choices
        # liste_choix : paires (nom_relatif,nom_relatif)
        liste_choix = []
        for (_, nom_depuis_path) in choices:
            nom_relatif = nom_depuis_path[1:]  # suppression debut '/'
            try:
                nom_texte = u"fichier" + " " + nom_relatif
            except Exception, e:
                diag = "echec : " + str(e) + " => " + nom_relatif + " ignored"
                print ""
                print debutmsg, diag
                print ""
                # nom_relatif ignored
            else:
                liste_choix.append((nom_relatif, nom_texte))
        return liste_choix

    # etant donne un nom relatif de fichier (nom relatif par rapport a
    # self.path) correspondant a choice=(nom_depuis_path,nom_depuis_path),
    # la methode estSousRepertoire determine si le fichier en question se
    # situe ou non directement sous un repertoire nomme nom_repertoire
    def estSousRepertoire(self, nom_repertoire, choice):
        cr = False  # par defaut
        (_, nom_depuis_path) = choice
        nom_absolu = os.path.join(self.path, nom_depuis_path)
        decoupage = nom_absolu.split("/")
        if len(decoupage) >= 2:
            le_repertoire = decoupage[-2]
            if le_repertoire == nom_repertoire:
                cr = True
        return cr

# FilePathField pour des noms de fichiers de configuration web
# propres a la conf web


class FilePathFieldConfigWebConf(FilePathFieldConfig):

    # determine/construit et retourne liste_choix qui est la liste des paires
    # (nom,nom) avec nom : nom relatif fichier vpz de configuration
    # filtree relativement a configuration web propre a la conf web

    def get_liste_choix_noms_relatifs_fichiers(self):

        choices = self.get_liste_choix()

        # ne garder que ceux situes sous repertoire repertoire_config_web_conf
        # ie dont nom absolu est de la forme */repertoire_config_web_conf/*.*
        # (le filtre par rapport a l'extension a deja eu lieu)
        liste_choix = [choix_vide]
        for choice in choices:
            if self.estSousRepertoire("web",
                                      choice):
                liste_choix.append(choice)
        # print "---------------------------------------"
        # print " CONFIG : ", liste_choix
        # print "---------------------------------------"
        return liste_choix

# FilePathField pour des noms de fichiers de configuration web
# propres aux applis web


class FilePathFieldConfigWebApplis(FilePathFieldConfig):

    # determine/construit et retourne liste_choix qui est la liste des paires
    # (nom,nom) avec nom : nom relatif fichier vpz de configuration
    # filtree relativement a configuration web propre aux applis web

    def get_liste_choix_noms_relatifs_fichiers(self):

        choices = self.get_liste_choix()

        # ne garder que ceux situes sous repertoire
        # repertoire_config_web_applis
        # ie dont nom absolu est de la forme */repertoire_config_web_applis/*.*
        # (le filtre par rapport a l'extension a deja eu lieu)
        liste_choix = [choix_vide]
        for choice in choices:
            if self.estSousRepertoire("web",
                                      choice):
                liste_choix.append(choice)
        return liste_choix

# FilePathField pour des noms de fichiers de configuration
# propres aux dictionnaires


class FilePathFieldConfigDicos(FilePathFieldConfig):

    # determine/construit et retourne liste_choix qui est la liste des paires
    # (nom,nom) avec nom : nom relatif fichier vpz de configuration
    # filtree relativement a configuration propre aux dictionnaires

    def get_liste_choix_noms_relatifs_fichiers(self):

        choices = self.get_liste_choix()

        # ne garder que ceux situes sous repertoire repertoire_config_dicos
        # ie dont nom absolu est de la forme */repertoire_config_dicos/*.*
        # (le filtre par rapport a l'extension a deja eu lieu)
        liste_choix = [choix_vide]
        for choice in choices:
            if self.estSousRepertoire("web",
                                      choice):
                liste_choix.append(choice)
        return liste_choix

#*****************************************************************************
#
# Methodes relatives aux nom_repertoire_lot_pkgs et nom_pkg de ModeleRecord
#
#*****************************************************************************

# ..
#****************************************************************************\n
#
# get_liste_choix_noms_relatifs_repertoires_lots_pkgs
# determine/construit et retourne liste_choix qui est la liste des paires
# correspondant aux noms relatifs de repertoire de lot de paquets vle
# (nom relatif depuis repertoire_racine_lots_pkgs). \n
# Les repertoires sont recherches sous
# repertoire_racine_lots_pkgs / version_vle (pour chaque version_vle de la
# liste_versions_vle )
#
#*****************************************************************************


def choix_noms_relatifs_repertoires_lots_pkgs(repertoire_racine_lots_pkgs,
                                              liste_versions_vle):

    liste_choix = []

    for version_vle in liste_versions_vle:

        # liste les repertoires (non caches) situes sous
        # repertoire_racine_lots_pkgs / version_vle
        repertoire_version_vle = os.path.join(
            repertoire_racine_lots_pkgs, version_vle)
        liste_noms_lot_pkgs = getListeRepertoiresNonCachesRep(
            repertoire_version_vle)

        # liste_choix : paires (nom_relatif,nom_texte)
        sep = ",  "
        for nom_lot_pkgs in liste_noms_lot_pkgs:
            nom_texte = u"version" + " " + version_vle + \
                sep + u"lot" + " " + nom_lot_pkgs
            liste_choix.append((nom_lot_pkgs, nom_texte))

    return liste_choix

# ..
#*****************************************************************************
#
# get_liste_choix_noms_pkg
# determine/construit et retourne liste_choix qui est la liste des paires
# correspondant aux noms de paquet vle. \n
# Les paquets vle sont recherches sous
# repertoire_racine_lots_pkgs / version_vle / lot_pkg (pour toutes versions
# de liste_versions_vle et tous lots de paquets vle trouves dessous)
#
#*****************************************************************************


def get_liste_choix_noms_pkg(repertoire_racine_lots_pkgs, liste_versions_vle):

    liste_choix = []

    for version_vle in liste_versions_vle:

        # liste les repertoires (non caches) situes sous
        # repertoire_racine_lots_pkgs / version_vle
        repertoire_version_vle = os.path.join(
            repertoire_racine_lots_pkgs, version_vle)
        liste_noms_lot_pkgs = getListeRepertoiresNonCachesRep(
            repertoire_version_vle)

        for nom_lot_pkgs in liste_noms_lot_pkgs:

            repertoire_lot_pkgs = os.path.join(
                repertoire_version_vle, nom_lot_pkgs)
            # liste les repertoires (non caches) situes sous
            # repertoire_lot_pkgs
            liste_noms_pkg = getListeRepertoiresNonCachesRep(
                repertoire_lot_pkgs)

            for nom_pkg in liste_noms_pkg:

                # liste_choix : paires (nom_pkg,nom_texte)
                sep = ",  "
                nom_texte = u"version" + " " + version_vle + sep + u"lot" + \
                    " " + nom_lot_pkgs + sep + u"paquet" + " " + nom_pkg
                liste_choix.append((nom_pkg, nom_texte))

    # print "---------------------------------------"
    # print " NOMS_PKG : ", liste_choix
    # print "---------------------------------------"
    return liste_choix

#*****************************************************************************
#
# Methodes relatives aux repertoires des fichiers de donnees de simulation
# ie a nom_repertoire_data de ModeleRecord
#
#*****************************************************************************

# ..
#****************************************************************************\n
#
# Methodes relatives a la conf
# qui servent dans des criteres de recherche de repertoires/fichiers qui
# seront proposes dans des listes menus
#
#*****************************************************************************


def get_conf_repertoire_data_base():
    return "data"

# ..
#****************************************************************************\n
#
# get_liste_choix_noms_relatifs_repertoires_datas
# determine/construit et retourne liste_choix qui est la liste des paires
# correspondant aux noms relatifs de repertoire de donnees de simulation de
# paquets vle (nom relatif depuis repertoire_racine_data). \n
# Les repertoires sont recherches sous repertoire_racine_data, avec un nom
# contenant repertoire_data_base.
#
#*****************************************************************************


def get_liste_choix_noms_relatifs_repertoires_datas(repertoire_racine_data,
                                                    repertoire_data_base):

    # recherche sous repertoire_racine / repertoire_relatif
    def ajouter(liste_noms_relatifs, repertoire_racine, nom_base,
                repertoire_relatif=""):

        if repertoire_relatif == "":
            repertoire_absolu = repertoire_racine
        else:
            repertoire_absolu = os.path.join(
                repertoire_racine, repertoire_relatif)
        liste_noms = getListeRepertoiresLargesNonCachesRep(repertoire_absolu)
        for nom in liste_noms:  # candidat
            nom_relatif = os.path.join(repertoire_relatif, nom)
            if nom_base in nom:  # elu
                liste_noms_relatifs.append(nom_relatif)
            # recursivite
            ajouter(
                liste_noms_relatifs, repertoire_racine, nom_base, nom_relatif)

    # liste_noms_relatifs_data (chemin relatif par rapport a
    # repertoire_racine_data)
    liste_noms_relatifs_data = []
    ajouter(liste_noms_relatifs_data,
            repertoire_racine_data, repertoire_data_base)

    # liste_choix : paires (nom_relatif,nom_texte)
    liste_choix = []
    for nom_relatif_data in liste_noms_relatifs_data:
        nom_texte = u"répertoire" + " " + nom_relatif_data
        liste_choix.append((nom_relatif_data, nom_texte))

    # print "---------------------------------------"
    # print " DATAS : ", liste_choix
    # print "---------------------------------------"
    return liste_choix

# ..
#****************************************************************************\n
#
# Formulaire d'administration des modeles record
#
# Saisie (creation...) des modeles record (edition 'read write')
#
# Note : la saisie normalement obligatoire de certains champs d'un
# ModeleRecord est en fait rendue optionnelle afin de faciliter
# l'enregistrement des informations en bd.
#
#*****************************************************************************


class ModeleRecordAdminForm(forms.ModelForm):
    path_lots_pkgs = settings.LOTS_PKGS_PATH
    liste_versions_vle = settings.VERSIONS_VLE

    # nom_repertoire_lot_pkgs
    # (nom relatif a partir du repertoire racine path_lots_pkgs).
    # Les repertoires presentes/proposes a l'usr se situent sous un sous
    # repertoire liste_versions_vle du repertoire racine path_lots_pkgs.

    label_admin = ParDefautModeleRecord.label_nom_repertoire_lot_pkgs
    help_text = ParDefautModeleRecord.help_text_nom_repertoire_lot_pkgs
    help_text = " " + _(u"Seuls sont proposés les répertoires situés " +
                        u"sous  le répertoire")
    help_text += path_lots_pkgs + ", "
    help_text += _(u"plus exactement sous un de ses sous-répertoires " +
                   u"de version.")

    liste_choix = choix_noms_relatifs_repertoires_lots_pkgs(
        path_lots_pkgs, liste_versions_vle)
    nom_repertoire_lot_pkgs = forms.ChoiceField(
        label=label_admin, help_text=help_text, choices=liste_choix,
        required=True)

    # nom_pkg
    label_admin = ParDefautModeleRecord.label_nom_pkg
    help_text = ParDefautModeleRecord.help_text_nom_pkg

    liste_choix = get_liste_choix_noms_pkg(
        path_lots_pkgs, liste_versions_vle)

    nom_pkg = forms.ChoiceField(
        label=label_admin, help_text=help_text, choices=liste_choix,
        required=True)

    class Meta:
        model = WsModel

    # ..
    #************************************************************************\n
    #
    # Methode de verification du ModeleRecord saisi
    #
    #*************************************************************************
    def clean(self):

        def valeur(cleaned_data, key):
            v = None  # par defaut
            if key in cleaned_data:
                v = cleaned_data.get(key)  # lecture
            return v

        def isNotNoneAuSensLarge(v):
            # la valeur "" est consideree comme None
            return ((v is not None) and (v != ""))

        cleaned_data = super(ModeleRecordAdminForm, self).clean()
        print cleaned_data
        return cleaned_data

#*****************************************************************************
