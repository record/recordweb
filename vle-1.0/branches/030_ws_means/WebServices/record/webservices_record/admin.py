# coding=UTF-8

# @file record/bd_modeles_record/admin.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File bd_modeles_record/admin.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# Administration de la bd des modeles record
#
#*****************************************************************************

from django.contrib import admin
from record.webservices_record.forms import ModeleRecordAdminForm

#*****************************************************************************
# Pour que certaines parties/rubriques administratives n'apparaissent pas
#*****************************************************************************
from django.contrib.sites.models import Site
from django.contrib.auth.models import Group
# admin.site.unregister(User) # partie User (en commentaire pour voir les
# comptes administrateur)
admin.site.unregister(Group)  # partie Group
admin.site.unregister(Site)   # partie Site

from record.webservices_record.models import (
                                              WsModel, WsScenarioDescription,
                                              ParameterDescription,
                                              WsParameter, WsModelScenario,
                                              WsScenarioParameters,
                                              )


class ParameterDescriptionAdmin(admin.ModelAdmin):
    pass

admin.site.register(ParameterDescription, ParameterDescriptionAdmin)


class WsModelScenarioAdmin(admin.ModelAdmin):
    pass

admin.site.register(WsModelScenario, WsModelScenarioAdmin)


class WsScenarioDescriptionAdmin(admin.ModelAdmin):
    pass

admin.site.register(WsScenarioDescription, WsScenarioDescriptionAdmin)


class WsScenarioParametersAdmin(admin.ModelAdmin):
    pass

admin.site.register(WsScenarioParameters, WsScenarioParametersAdmin)


class WsParameterAdmin(admin.ModelAdmin):
    pass

admin.site.register(WsParameter, WsParameterAdmin)


class WsModelAdmin(admin.ModelAdmin):
    form = ModeleRecordAdminForm
    pass

admin.site.register(WsModel, WsModelAdmin)

#*****************************************************************************
