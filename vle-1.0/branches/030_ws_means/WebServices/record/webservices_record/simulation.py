# coding=UTF-8
#
# meansrecordws - RECORD platform web services for MEANS platform
#
# Copyright (C) 2014-2015 INRA http://www.inra.fr
#
# This file is part of meansrecordws.
#
# meansrecordws is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# meansrecordws is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#

import uuid
import os
import shutil
import json
import ast
from record.webservices_record.models import WsModel

from django.conf import settings
from pyvle import VlePackage, Vle, to_value
from Log import get_logger


class SimulationVle(object):
    """
        Permet de lancer la simulation d'un scénario.
        Avant de lancer la simulation on configure
        d'abords le scénario en utilisant les paramètres
        stockés dans le fichier de configuration.
    """
    def __init__(self, request):
        assert request.scenario is not None
        self.request = request
        self.scenario = request.scenario
        self.model = None
        self.errors = None
        self.path = None
        self.lot_package = None
        self.vle_run = None
        self.logger = get_logger(__name__)
        self.config = os.path.join(
            os.path.dirname(
                os.path.realpath(__file__)), "configs/config.conf")
        if self.scenario_est_valide():
            self.set_environnement()

    def scenario_est_valide(self):
        """
            Vérifie si le scénario existe bien dans la base
            de données.
        """
        self.model = WsModel.objects.get(pk=self.scenario.wsmodel)
        if self.model.accord_exploitation and self.model.modele_active:
            return True
        else:
            self.errors = u"Le modèle n'est pas active !"
            return False

    def set_environnement(self):
        """
            Crée l'environnement pour la simulation. Elle crée
            le repertoire qui sera utilisé comme le VLE_HOME.
        """
        self.set_vle_rep()
        try:
            os.makedirs(self.path)
            self.inistialiser_vle()
            self.initialiser_simulation()
        except OSError as ex:
            self.logger.info(str(ex))
            self.errors = "Erreur de simulation !"

    def set_vle_rep(self):
        """
            Initialise le nom du repértoire qui sera utilisé
            pour la simulation.
        """
        vle_home = settings.VLE_HOME
        nom_rep = 'Simulation_' + str(uuid.uuid4())
        self.path = os.path.join(vle_home, nom_rep)

    def inistialiser_vle(self):
        """
            Initialise la variable d'environnement VLE_HOME
        """
        os.environ['VLE_HOME'] = self.path
        VlePackage.getInstalledPackages()

    def initialiser_simulation(self):
        """
            Initialise la simulation du scénario. Elle recopie
            le paquet principale au repértoire de simulation et
            elle crée des liens symboliques vers autres paquets
            necéssaires pour la simulation.
        """
        self.lot_package = os.path.join(settings.LOTS_PKGS_PATH,
                            self.model.version_vle,
                            self.model.nom_repertoire_lot_pkgs,)
        self.copier_scenario_et_pkg()
        pkgs_path = os.path.join(self.path, 'pkgs')
        pakages = os.listdir(self.lot_package)
        for package in pakages:
            if package == self.model.nom_pkg:
                continue
            src = os.path.join(self.lot_package, package)
            dest = os.path.join(pkgs_path, package)
            os.symlink(src, dest)

    def copier_scenario_et_pkg(self):
        """
        Copie le scénario de simulation et le paquet principale
        au repertoire où sera lancé la simulation.
        """
        path = os.path.join(self.lot_package,
                            self.model.nom_pkg,
                            'exp',
                            self.scenario.nom_scenario + ".vpz")
        shutil.copy2(path, self.path)
        path = os.path.join(self.lot_package,
                            self.model.nom_pkg)
        pkg = os.path.join(self.path, 'pkgs', self.model.nom_pkg)
        try:
            shutil.copytree(path, pkg)
        except shutil.Error as error:
            self.errors = "Erreur de simulation !"
            self.logger.error(error)

    def supprimer_repertoire_simulation(self):
        """
            Supprime le repertoire de simulation. Elle sera
            appelé après avoir lancer la simulation.
        """
        try:
            shutil.rmtree(self.path)
        except Exception as error:
            self.logger.error(error)

    def run(self):
        """
            Lance la simulation du scénario et retourne le résultat
            de la simulation. En cas d'erreur si le message d'erreur
            qui est retourné.
        """
        if self.errors:
            return self.errors
        scn = os.path.join(self.path,
                           'pkgs',
                           self.model.nom_pkg,
                           'exp',
                           self.scenario.nom_scenario + ".vpz")
        curdir = os.getcwd()
        path = os.path.join(self.path,
                 'pkgs',
                 self.model.nom_pkg)
        os.chdir(path)
        scn = str(scn)
        self.vle_run = Vle(scn)
        self.configure()
        self.set_all_storage()
        try:
            dic = self.vle_run.run()
        except Exception as e:
            logger.info(str(e))

        os.chdir(curdir)
        return dic

    def set_all_storage(self):
        """
            Elle mets toutes les vues en mode storage.
        """
        for view in self.vle_run.listViews():
            output = self.vle_run.getViewOutput(view)
            self.vle_run.setOutputPlugin(output, '', 'local', 'storage')

    def configure(self):
        """
            Configure le scénario en utilisant le fichier de
            configuration.
        """
        data = json.load(open(self.config))
        liste = data['Parametres']
        for element in liste:
            condition = element['condition']
            port = element['port']
            if self.request.nom_scenario in element['valeurs'].keys():
                valeur = element['valeurs'][self.request.nom_scenario]
                self.set_condition(condition, port, valeur)

    def set_condition(self, condition, port, valeur):
        """
            Modifie la valeur d'un paramètre.
        """
        condition = str(condition)
        port = str(port)
        valeur = str(valeur)
        try:
            _evel = ast.literal_eval(valeur)
            valeur = _evel
        except Exception:
            pass
        try:
            valeur = to_value(valeur)
            self.vle_run.setConditionPortValue(condition, port,
                                           to_value(valeur), 0)
        except Exception as exception:
            self.logger.info("Erreur de simulation !")
            self.errors = str(exception)
