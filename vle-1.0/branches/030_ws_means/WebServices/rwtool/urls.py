
# @file rwtool/urls.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File urls.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

from django.conf.urls import url, include
from django.conf.urls import patterns
from django.conf import settings
from django.conf.urls.static import static

# to enable the admin:
from django.contrib import admin
admin.autodiscover()

#*****************************************************************************

urlpatterns = patterns('',

                       # internationalisation traductions
                       (r'^i18n/', include('django.conf.urls.i18n')),
                       )


urlpatterns += patterns('',

                        #******************************************************
                        #
                        # partie administration : administration de la bd des modeles
                        # record
                        #
                        #******************************************************

                        # pour empecher l'administration de la bd de rwtool (ie empecher
                        # l'administration de la bd des modeles record depuis l'outil web record),
                        # mettre en commentaire les lignes 'admin.site.urls'
                        url(r'^rwtool/adm/',   include(admin.site.urls)),
                        url(r'^rwtool/admin/', include(admin.site.urls)),


                        # 2) webservices_record
                        url(r'^', include('record.webservices_record.urls')),

                        #******************************************************
                        )

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
