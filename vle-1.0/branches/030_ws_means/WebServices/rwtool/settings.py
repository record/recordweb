#-*- coding:utf-8 -*-

# @file rwtool/settings.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File settings.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
# Voir commentaires directement dans code
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# Django settings for rwtool project.
#
# Record web tool (based on bd_modeles_record and outil_web_record
# applications)
#
#
# notes_de_developpement : les duplications ne sont pas gerees pour les
# variables VAR construites a partir de series de VAR_FIRST et VAR_LAST
#
#*****************************************************************************

import os
import sys
import datetime
import logging

from record.webservices_record.Log import get_logger

LOGGER = get_logger(__name__)


#*****************************************************************************
# Configuration EN_PRODUCTION, MACHINE_PROD et SERVEUR_DJANGO
#
# EN_PRODUCTION (etat) (impact sur les emplacements/chemins : /opt/...) :
# - EN_PRODUCTION = True pour situation de mise en production
#   soit effective (sur VM de production webrecord) soit dans le cadre des
#   essais (sur VM d'essai webrecord_test) avant mise en production effective
# - EN_PRODUCTION = False pour situation de/en developpement
#
# MACHINE_PROD (la machine d'installation) (impact sur les URLs) :
# - MACHINE_PROD = True  pour machine de production (147.99.96.155)
# - MACHINE_PROD = False pour machine de developpement (mon PC 147.99.96.72)
#
# SERVEUR_DJANGO (le serveur utilise)
#                (impact sur DEBUG, les URLs, specificites apache...) :
# - SERVEUR_DJANGO = True  pour serveur de developpement (django)
# - SERVEUR_DJANGO = False pour serveur de production (apache)
#
#*****************************************************************************

EN_PRODUCTION = False  # en developpement
MACHINE_PROD = True
SERVEUR_DJANGO = True  # serveur django

ESPACE_WEB = os.path.join(
        os.path.dirname(
            os.path.realpath(__file__)),
        "../..")
ESPACE_WEB = os.path.normpath(ESPACE_WEB)


#*****************************************************************************
# Variables, Variables d'environnement
#*****************************************************************************

# RECORDWEB_HOME, repertoire racine (projets, applications, librairies python)
RECORDWEB_HOME = os.path.join(
    os.path.dirname(
        os.path.realpath(__file__)),
    "..")

# sous le repertoire racine RECORDWEB_HOME, repertoire 'record' : code record
# (librairies python et applications au sens django du terme)

# vle ************************************************************************
# VLE_HOME : la variable d'environnement VLE_HOME va etre initialisee a la
# valeur VLE_HOME. Le repertoire VLE_HOME est le repertoire sous lequel
# sont construits les repertoires VLE_HOME attribues a chaque
# session/connexion

# pour outil_web_record (usr)
# VLE_HOME (${HOME}/.vle)
if EN_PRODUCTION:
    VLE_HOME = os.path.join(ESPACE_WEB, 'VLE')
else:
    VLE_HOME = os.path.join(os.environ.get('HOME'), ".vle")

# pour outil_web_record (usr)
# variable d'environnement VLE_HOME
os.environ['VLE_HOME'] = VLE_HOME

# pour bd_modeles_record (adm)
# VERSIONS_VLE, liste des versions vle gerees/prises en compte
VERSIONS_VLE = ["vle-0.8.9", "vle-1.0", "vle-1.1"]

# depot des modeles record ***************************************************
# - LOTS_PKGS_PATH : lots des paquets vle (pouvant contenir des fichiers de
# configuration, contenant des repertoires data)
# - CONFIGURATION_PATH : repertoire englobant l'ensemble des fichiers de
# configuration/personnalisation (ceux presents dans les paquets vle, et
# d'autres en plus)
# - DATA_PATH : repertoire englobant l'ensemble des repertoires de donnees de
# simulation (ceux presents dans les paquets vle, et d'autres en plus)

# pour bd_modeles_record (adm)
# DEPOT_PATH, racine du depot des modeles record (emplacement)
DEPOT_PATH = os.path.join(ESPACE_WEB, "recordweb_depot")

# pour bd_modeles_record (adm)
# LOTS_PKGS_PATH, repertoire racine sous lequel est cherche l'ensemble des
# lots des paquets vle des modeles record
LOTS_PKGS_PATH = os.path.join(DEPOT_PATH, "pkgs_recordweb")

# pour bd_modeles_record (adm)
# CONFIGURATION_PATH, repertoire racine sous lequel est cherche l'ensemble
# des fichiers de configuration (pas partout, seulement dans certains sous
# repertoires, cf code)
CONFIGURATION_PATH = DEPOT_PATH

# pour bd_modeles_record (adm)
# DATA_PATH, repertoire racine sous lequel est cherche l'ensemble des
# repertoires de donnees de simulation (pas partout, seulement dans
# certains sous repertoires, cf code)
DATA_PATH = DEPOT_PATH

RECORDWEB_VENDOR = os.path.join(ESPACE_WEB, "recordweb_vendor")

# path du project rwtool *****************************************************
# (correspond a RECORDWEB_HOME/rwtool)
SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
if SITE_ROOT not in sys.path:
    sys.path.insert(0, SITE_ROOT)

# RECORDWEB_HOME
# => acces aux librairies record et applications record
if RECORDWEB_HOME not in sys.path:
    sys.path.insert(0, RECORDWEB_HOME)

LOGGER.info("ESPACE_WEB  = %s", ESPACE_WEB)

#************************************************************************

#*****************************************************************************
# la BD des modeles record exploitee par l'outil_web_record
#*****************************************************************************

# emplacement et nom de la BD des modeles record exploitee par
# l'outil_web_record
BD_MODELES_RECORD = os.path.join(
    ESPACE_WEB,
    'bd_modeles_record_directory',
    'bd_modeles_record.db')
logging.info("pour info bd modeles record  = %s", BD_MODELES_RECORD)

#*****************************************************************************
# Configuration recueil de statistiques sur le site
#*****************************************************************************

REQUEST_TRAFFIC_MODULES = (
    'request.traffic.UniqueVisitor',
    'request.traffic.UniqueVisit',
    'request.traffic.Hit',
    'request.traffic.Search',
    'request.traffic.Error',
    'request.traffic.Error404',
)

#*****************************************************************************
# debug
#*****************************************************************************

if SERVEUR_DJANGO:
    DEBUG = True
else:
    DEBUG = False

ALLOWED_HOSTS = ['*']
TEMPLATE_DEBUG = DEBUG

#*****************************************************************************
# to email errors
#*****************************************************************************

# ADMIN for 500 errors
ADMINS = ()
if not SERVEUR_DJANGO:  # apache
    ADMINS = (
        ('rousse', 'nathalie.rousse@toulouse.inra.fr'),
    )

# MANAGERS for 404 errors
MANAGERS = ADMINS

if not SERVEUR_DJANGO:  # apache

    # to email 500 errors

    if MACHINE_PROD:  # (VM de production webrecord)
        SERVER_EMAIL = 'django-rwtool@webrecord.com'
    else:  # (VM d'essai webrecord_test avant mise en production)
        SERVER_EMAIL = 'django-rwtool@webrecordtest.com'

    # serveur smtp exim :
    EMAIL_HOST = 'smtp.toulouse.inra.fr'
    # EMAIL_PORT par defaut

    # to email 404 errors
    SEND_BROKEN_LINK_EMAILS = True

#*****************************************************************************

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BD_MODELES_RECORD,
    }
}

SITE_ID = 1


# (pour rafraichir/generer STATIC_ROOT : 'python manage.py collectstatic')
STATIC_ROOT = os.path.join(RECORDWEB_HOME, 'static')

STATIC_URL = '/static/'

ADMIN_MEDIA_PREFIX = '/static/admin/'


# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)


# Make this unique, and don't share it with anybody.
SECRET_KEY = 'n%@$ei5#-7cnx-zh1lk#g(9vqqqck9ct@1!x=f@5u57n9k6%fj'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    #     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.gzip.GZipMiddleware',
    # internationalisation traductions (doit etre apres SessionMiddleware)
    # (Autodetect user language (from browser) or use session defined language
    # (or fallback to project default)) : en commentaire pour que
    # LANGUAGE_CODE soit actif
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    # for django-request # a placer apres django.contrib.auth, avant
    # django.contrib.flatpages
    'request.middleware.RequestMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    # "django.core.context_processors.auth", # (django1.3)
    "django.contrib.auth.context_processors.auth",  # (django1.4)
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",  # internationalisation traductions
    "django.core.context_processors.media",
    "django.core.context_processors.debug",
    "django.core.context_processors.static",
)

ROOT_URLCONF = 'rwtool.urls'


TEMPLATE_ROOT = os.path.join(RECORDWEB_HOME, 'templates')
TEMPLATE_DIRS = (TEMPLATE_ROOT,
)

# Additional locations of static files
STATICFILES_DIRS = (
    os.path.join(TEMPLATE_ROOT, 'static'),
)




INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'django.contrib.admin',  # to enable the admin

    'rest_framework',  # web services

    'rest_framework_jwt',

    # Uncomment the next line for django-request statistics module :
    'request',

    # web services
    'record.webservices_record',  # utilisation webservices record


)

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
    ),
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.UnicodeJSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
        'rest_framework.renderers.XMLRenderer',
  )
}

# Configuration de l'authentification avec la méthode des tokens
JWT_AUTH = {
    'JWT_ENCODE_HANDLER':
    'rest_framework_jwt.utils.jwt_encode_handler',

    'JWT_DECODE_HANDLER':
    'rest_framework_jwt.utils.jwt_decode_handler',

    'JWT_PAYLOAD_HANDLER':
    'rest_framework_jwt.utils.jwt_payload_handler',

    'JWT_ALGORITHM': 'HS256',
    'JWT_VERIFY': True,
    'JWT_VERIFY_EXPIRATION': True,
    'JWT_LEEWAY': 0,
    # Durée de validité = 30 min.
    'JWT_EXPIRATION_DELTA': datetime.timedelta(seconds=1800)
}

SESSION_ENGINE = 'django.contrib.sessions.backends.file'


