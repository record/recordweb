function toggle_visibility(id) {
		var e = document.getElementById(id);
		if (e.style.display == 'block')
			e.style.display = 'none';
		else
			e.style.display = 'block';
	}
	

function auth() {
            var username = document.getElementById('username');
            if (!username.value) {
                username.style.border="2px solid red";
                return;
            }
            else{
                username.style.border = "";
            }
            var password = document.getElementById('password');
            if (!password.value){
                password.style.border = "2px solid red";
                return;
            }
            else{
                password.style.border = "";
            }
            
            if (!window.location.origin)
                window.location.origin = window.location.protocol+"//"+window.location.host;
            select = document.getElementById('authRespType');
            respType = select.options[select.selectedIndex].value;
            
            url = window.location.origin + '/api-token-auth/'
            var objJSON = {
                "username": username.value,
                "password": password.value
            };
            var strJSON = JSON.stringify(objJSON);
            var xhr = new XMLHttpRequest();
            xhr.open('POST', url, true);
            xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
            xhr.setRequestHeader('Accept', respType);
            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4 ) {
                    e = document.getElementById('authResponse');
                    e.style.display = 'block';
                    e = document.getElementById('authHideResp');
                    e.style.display = 'block';
                    respdiv = document.getElementById('authResp');
                    respdiv.innerHTML = '<pre class="json"><code>' + xhr.responseText + '</code></pre>';
                    codediv = document.getElementById('authRespCode');
                    codediv.innerHTML = '<pre>' + xhr.status + '</pre>';
                    headersdiv = document.getElementById('authRespHeaders');
                    headersdiv.innerHTML = '<pre>' + xhr.getAllResponseHeaders() + '</pre>';
                    hljs.highlightBlock(respdiv);
                }
            };
            // send the collected data as JSON
            xhr.send(strJSON);
            
        }
        
function getData(){
        culturePrincipale = document.getElementById('culturePrincipale').value;
        culturePrecedente = document.getElementById('culturePrecedente').value;
        cultureIntermediaire = document.getElementById('cultureIntermediaire').value;
        if (cultureIntermediaire == '')
            cultureIntermediaire = undefined;
        region = document.getElementById('region').value;
        objJSON = {
            "culturePrincipale": culturePrincipale,
            "culturePrecedente": culturePrecedente,
            "cultureIntermediaire": cultureIntermediaire,
            "region": region
        };
        strJSON = JSON.stringify(objJSON);
        return strJSON;
}
    
    function syntaxHighlight(json) {
        if (typeof json != 'string') {
            json = JSON.stringify(json, undefined, 2);
        }
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
            var cls = 'number';
            if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
            } else if (/true|false/.test(match)) {
            cls = 'boolean';
            } else if (/null/.test(match)) {
                cls = 'null';
            }
            return '<span class="' + cls + '">' + match + '</span>';
        });
    }    
    
    
    
        
function simuler() {
            data = getData();
            if (!window.location.origin)
                window.location.origin = window.location.protocol+"//"+window.location.host;
            select = document.getElementById('simRespType');
            respType = select.options[select.selectedIndex].value;
            url = window.location.origin + '/simulation/MicMac/'
           
            var xhr = new XMLHttpRequest();
            xhr.open('POST', url, true);
            xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
            xhr.setRequestHeader('Accept', respType);
            if(window.token != undefined)
                xhr.setRequestHeader('Authorization', 'JWT ' + token);
            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4 ) {
                    e = document.getElementById('report-pane');
                    e.style.display = 'none';
                    e = document.getElementById('simResponse');
                    e.style.display = 'block';
                    e = document.getElementById('simHideResp');
                    e.style.display = 'block';
                    respdiv = document.getElementById('simResp');
                    respdiv.innerHTML = '<pre class="json"><code>' + xhr.responseText + '</pre></code>';
                    codediv = document.getElementById('simRespCode');
                    codediv.innerHTML = '<pre>' + xhr.status + '</pre>';
                    headersdiv = document.getElementById('simRespHeaders');
                    headersdiv.innerHTML = '<pre>' + xhr.getAllResponseHeaders() + '</pre>';
                    hljs.highlightBlock(respdiv);
                }
            };
            // send the collected data as JSON
            xhr.send(strJSON);
            e = document.getElementById('report-pane');
            e.style.display = 'block';
        }
        
        
        
        function activateAuth(){
            if (document.getElementById('onoffswitch').checked){
                document.getElementById('onoffswitch').checked = false;
                window.token = undefined;
                return;
            }
            el = document.getElementById("login-window");
            el.style.display = "block";
        }
        
        function getToken(){
            var username = document.getElementById('loginusername');
            if (!username.value) {
                username.style.border="2px solid red";
                return;
            }
            else{
                username.style.border = "";
            }
            var password = document.getElementById('loginpassword');
            if (!password.value){
                password.style.border = "2px solid red";
                return;
            }
            else{
                password.style.border = "";
            }
            
            if (!window.location.origin)
                window.location.origin = window.location.protocol+"//"+window.location.host;
            select = document.getElementById('authRespType');
            respType = select.options[select.selectedIndex].value;
            
            url = window.location.origin + '/api-token-auth/'
            var objJSON = {
                "username": username.value,
                "password": password.value
            };
            var strJSON = JSON.stringify(objJSON);
            var xhr = new XMLHttpRequest();
            xhr.open('POST', url, true);
            xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
            xhr.setRequestHeader('Accept', respType);
            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4 ) {
                    if (xhr.status == 200){
                        window.token = JSON.parse(xhr.responseText).token;
                        document.getElementById('onoffswitch').checked = true;
                    } else 
                        alert('Erreur : ' + xhr.responseText);
                }
                toggle_visibility('login-window');
            };
            // send the collected data as JSON
            xhr.send(strJSON);
        }