# coding=UTF-8
#
#------------------------------------------------------------------------------
#
# Authors : Rabah Meradi, for RECORD platform, INRA.
#
#------------------------------------------------------------------------------
#
# meansrecordws - RECORD platform web services for MEANS platform
#
# Copyright (C) 2014-2015 INRA http://www.inra.fr
#
# This file is part of meansrecordws.
#
# meansrecordws is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# meansrecordws is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#

# script apache mod_wsgi django
	
import os
import sys
	
project=os.path.dirname(__file__)
app=os.path.dirname(project)
if project not in sys.path :
    sys.path.append(project)
if app not in sys.path :
    sys.path.append(app)

os.environ['DJANGO_SETTINGS_MODULE'] = 'rwtool.settings'

# Supprimer ces deux lignes si vous n'utilisez pas d'environnement virtuel
activate_this = os.path.expanduser("/home/meradi/.envs/wsenv/bin/activate_this.py")
execfile(activate_this, dict(__file__=activate_this))


import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()