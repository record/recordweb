Django==1.6.5
PyJWT==0.2.1
djangorestframework==2.3.13
djangorestframework-jwt==0.1.5
django-request==1.0.1