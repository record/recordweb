'''
script apache mod_wsgi django with vle
	
Author : Nathalie Rousse, INRA
	
Copyright (C) 2012 INRA
	
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
	
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
	
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import os
import sys

project=os.path.dirname(__file__)
app=os.path.dirname(project)
if project not in sys.path :
    sys.path.append(project)
if app not in sys.path :
    sys.path.append(app)

os.environ['DJANGO_SETTINGS_MODULE'] = 'rmlib.settings'
	
import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

