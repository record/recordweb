#-*- coding:utf-8 -*-

###############################################################################
# File conf_internationalisation.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
#
# CONFIGURATION
#
###############################################################################

from django.conf import settings

###############################################################################
#
# internationalisation
#
###############################################################################

class CONF_internationalisation(object) :

    print ""
    print ""
    print "CONF_internationalisation de recordweb/rwtool/configs "
    print ""
    print ""

    # Option avec/sans affichage du menu de choix du langage
    menu_langage_bd_modeles_record = settings.MENU_LANGAGE_BD_MODELES_RECORD # True, False

###############################################################################


