#-*- coding:utf-8 -*-

###############################################################################
# File admin_forms.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Authors :
# Nathalie Rousse, INRA RECORD team member,
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
#
# Formulaires d'administration des modeles record enregistres en bd
#
# Saisie (creation...) des modeles record (edition 'read write')
#
###############################################################################

from record.bd_modeles_record.models import ParDefautModeleRecord
from record.bd_modeles_record.models import ModeleRecord, MotDePasse, Individu
from record.bd_modeles_record.models import get_conf_repertoire_racine_configuration
from record.bd_modeles_record.models import get_conf_repertoire_racine_lots_pkgs
from record.bd_modeles_record.models import get_conf_repertoire_racine_data
from record.bd_modeles_record.models import getNomAbsoluRepertoireLotPkgs
from record.bd_modeles_record.models import getNomAbsoluRepertoireData
from record.bd_modeles_record.models import getNomAbsoluFichierConfigWebConf
from record.bd_modeles_record.models import getNomAbsoluFichierConfigWebApplis
from record.bd_modeles_record.models import getNomAbsoluFichierConfigDicos
from record.bd_modeles_record.models import defaultValueNomRepertoireData, getNomAbsoluRepertoireDataParDefaut

from django.db import models

from django import forms

import os.path
from record.utils.dirs_and_files import getListeRepertoiresNonCachesRep
from record.utils.dirs_and_files import getListeRepertoiresLargesNonCachesRep
from record.utils.dirs_and_files import verificationFichierOk, verificationLargeRepertoireOk

from record.bd_modeles_record.configs.conf_bd_modeles_record import CONF_bd_modeles_record

from django.utils.translation import ugettext as _

###############################################################################
#
# Des constantes
#
###############################################################################

# Valeur 'vide' d'une liste de choix
choix_vide = ("","---------")

###############################################################################
# Methodes relatives a la conf
# qui servent dans des criteres de recherche de repertoires/fichiers qui
# seront proposes dans des listes menus
###############################################################################

def get_conf_repertoire_config_web_conf() :
    return CONF_bd_modeles_record.repertoire_config_web_conf
def get_conf_repertoire_config_web_applis() :
    return CONF_bd_modeles_record.repertoire_config_web_applis
def get_conf_repertoire_config_dicos() :
    return CONF_bd_modeles_record.repertoire_config_dicos

###############################################################################
# FilePathFieldConfig... :
#
# Classes utilitaires pour les noms des fichiers vpz de configuration
# (nom_fichier_config_web_conf, nom_fichier_config_web_applis
# et nom_fichier_config_dicos) de ModeleRecord
#
###############################################################################

# FilePathField pour des noms de fichiers de configuration (partie commune)
class FilePathFieldConfig( models.FilePathField ) :

    # determine/construit et retourne liste_choix qui est la liste des paires
    # (nom relatif,nom relatif) associees aux fichiers vpz de configuration
    # (chemin relatif par rapport a self.path)
    def get_liste_choix( self ) :

        # recuperation de choices : paires (nom_absolu,nom_depuis_path)
        defaults = { 'path': self.path,
                     'match': self.match,
                     'recursive': self.recursive,
                     'form_class': forms.FilePathField, }
        obj= super(models.FilePathField, self).formfield(**defaults)
        choices = obj.choices

        # liste_choix : paires (nom_relatif,nom_relatif)
        liste_choix = []
        for (nom_absolu,nom_depuis_path) in choices :
            nom_relatif = nom_depuis_path[1:] # suppression debut '/'
            nom_texte = u"fichier" + " " + nom_relatif
            liste_choix.append( (nom_relatif,nom_texte) )

        return liste_choix

    # etant donne un nom relatif de fichier (nom relatif par rapport a
    # self.path) correspondant a choice=(nom_depuis_path,nom_depuis_path),
    # la methode estSousRepertoire determine si le fichier en question se
    # situe ou non directement sous un repertoire nomme nom_repertoire
    def estSousRepertoire( self, nom_repertoire, choice ) :
        cr = False # par defaut
        (nom_depuis_path,nom_depuis_path) = choice
        nom_absolu = os.path.join( self.path, nom_depuis_path )
        decoupage = nom_absolu.split("/")
        if len(decoupage) >= 2 :
            le_repertoire = decoupage[-2]
            if le_repertoire == nom_repertoire :
                cr = True
        return cr

# FilePathField pour des noms de fichiers de configuration web
# propres a la conf web
class FilePathFieldConfigWebConf( FilePathFieldConfig ) :

    # determine/construit et retourne liste_choix qui est la liste des paires
    # (nom,nom) avec nom : nom relatif fichier vpz de configuration
    # filtree relativement a configuration web propre a la conf web
    def get_liste_choix_noms_relatifs_fichiers( self ) : 

        choices = self.get_liste_choix()

        # ne garder que ceux situes sous repertoire repertoire_config_web_conf
        # ie dont nom absolu est de la forme */repertoire_config_web_conf/*.*
        # (le filtre par rapport a l'extension a deja eu lieu)
        liste_choix = [ choix_vide ]
        for choice in choices:
            if self.estSousRepertoire( get_conf_repertoire_config_web_conf(), choice ) :
                liste_choix.append( choice )
        #print "---------------------------------------"
        #print " CONFIG : ", liste_choix
        #print "---------------------------------------"
        return liste_choix

# FilePathField pour des noms de fichiers de configuration web
# propres aux applis web
class FilePathFieldConfigWebApplis( FilePathFieldConfig ) :

    # determine/construit et retourne liste_choix qui est la liste des paires
    # (nom,nom) avec nom : nom relatif fichier vpz de configuration
    # filtree relativement a configuration web propre aux applis web
    def get_liste_choix_noms_relatifs_fichiers( self ) : 

        choices = self.get_liste_choix()

        # ne garder que ceux situes sous repertoire repertoire_config_web_applis
        # ie dont nom absolu est de la forme */repertoire_config_web_applis/*.*
        # (le filtre par rapport a l'extension a deja eu lieu)
        liste_choix = [ choix_vide ]
        for choice in choices:
            if self.estSousRepertoire( get_conf_repertoire_config_web_applis(), choice ) :
                liste_choix.append( choice )
        return liste_choix

# FilePathField pour des noms de fichiers de configuration
# propres aux dictionnaires
class FilePathFieldConfigDicos( FilePathFieldConfig ) :

    # determine/construit et retourne liste_choix qui est la liste des paires
    # (nom,nom) avec nom : nom relatif fichier vpz de configuration
    # filtree relativement a configuration propre aux dictionnaires
    def get_liste_choix_noms_relatifs_fichiers( self ) : 

        choices = self.get_liste_choix()

        # ne garder que ceux situes sous repertoire repertoire_config_dicos
        # ie dont nom absolu est de la forme */repertoire_config_dicos/*.*
        # (le filtre par rapport a l'extension a deja eu lieu)
        liste_choix = [ choix_vide ]
        for choice in choices:
            if self.estSousRepertoire( get_conf_repertoire_config_dicos(), choice ) :
                liste_choix.append( choice )
        return liste_choix

###############################################################################
#
# Methodes relatives aux nom_repertoire_lot_pkgs et nom_pkg de ModeleRecord
#
###############################################################################

###############################################################################
# get_liste_choix_noms_relatifs_repertoires_lots_pkgs
# determine/construit et retourne liste_choix qui est la liste des paires
# correspondant aux noms relatifs de repertoire de lot de paquets vle
# (nom relatif depuis repertoire_racine_lots_pkgs).
# Les repertoires sont recherches sous
# repertoire_racine_lots_pkgs / version_vle (pour chaque version_vle de la
# liste_versions_vle )
###############################################################################
def get_liste_choix_noms_relatifs_repertoires_lots_pkgs( repertoire_racine_lots_pkgs, liste_versions_vle ) :

    liste_choix = []

    for version_vle in liste_versions_vle :

        # liste les repertoires (non caches) situes sous repertoire_racine_lots_pkgs / version_vle
        repertoire_version_vle = os.path.join( repertoire_racine_lots_pkgs, version_vle )
        liste_noms_lot_pkgs = getListeRepertoiresNonCachesRep( repertoire_version_vle )

        # liste_choix : paires (nom_relatif,nom_texte)
        sep = ",  "
        for nom_lot_pkgs in liste_noms_lot_pkgs :
            nom_texte = u"version" + " " + version_vle + sep + u"lot" + " " + nom_lot_pkgs
            nom_relatif = os.path.join( version_vle, nom_lot_pkgs )
            liste_choix.append( (nom_relatif,nom_texte) )

    return liste_choix

###############################################################################
# get_liste_choix_noms_pkg
# determine/construit et retourne liste_choix qui est la liste des paires
# correspondant aux noms de paquet vle.
# Les paquets vle sont recherches sous
# repertoire_racine_lots_pkgs / version_vle / lot_pkg (pour toutes versions
# de liste_versions_vle et tous lots de paquets vle trouves dessous)
###############################################################################
def get_liste_choix_noms_pkg( repertoire_racine_lots_pkgs, liste_versions_vle ) :

    liste_choix = []

    for version_vle in liste_versions_vle :

        # liste les repertoires (non caches) situes sous repertoire_racine_lots_pkgs / version_vle
        repertoire_version_vle = os.path.join( repertoire_racine_lots_pkgs, version_vle )
        liste_noms_lot_pkgs = getListeRepertoiresNonCachesRep( repertoire_version_vle )

        for nom_lot_pkgs in liste_noms_lot_pkgs :

            repertoire_lot_pkgs = os.path.join( repertoire_version_vle, nom_lot_pkgs )
            # liste les repertoires (non caches) situes sous repertoire_lot_pkgs
            liste_noms_pkg = getListeRepertoiresNonCachesRep( repertoire_lot_pkgs )

            for nom_pkg in liste_noms_pkg :

                # liste_choix : paires (nom_pkg,nom_texte)
                sep = ",  "
                nom_texte = u"version" + " " + version_vle + sep + u"lot" + " " + nom_lot_pkgs + sep + u"paquet" + " " + nom_pkg
                liste_choix.append( (nom_pkg,nom_texte) )

    #print "---------------------------------------"
    #print " NOMS_PKG : ", liste_choix
    #print "---------------------------------------"
    return liste_choix

###############################################################################
#
# Methodes relatives aux repertoires des fichiers de donnees de simulation
# ie a nom_repertoire_data de ModeleRecord
#
###############################################################################

###############################################################################
# Methodes relatives a la conf
# qui servent dans des criteres de recherche de repertoires/fichiers qui
# seront proposes dans des listes menus
###############################################################################
def get_conf_repertoire_data_base() :
    return CONF_bd_modeles_record.repertoire_data_base

###############################################################################
# get_liste_choix_noms_relatifs_repertoires_datas
# determine/construit et retourne liste_choix qui est la liste des paires
# correspondant aux noms relatifs de repertoire de donnees de simulation de
# paquets vle (nom relatif depuis repertoire_racine_data).
# Les repertoires sont recherches sous repertoire_racine_data, avec un nom
# contenant repertoire_data_base.
###############################################################################
def get_liste_choix_noms_relatifs_repertoires_datas( repertoire_racine_data, repertoire_data_base ) :

    # recherche sous repertoire_racine / repertoire_relatif
    def ajouter( liste_noms_relatifs, repertoire_racine, nom_base, repertoire_relatif="" ) :

        if repertoire_relatif == "" :
            repertoire_absolu = repertoire_racine
        else :
            repertoire_absolu = os.path.join( repertoire_racine, repertoire_relatif )
        liste_noms = getListeRepertoiresLargesNonCachesRep( repertoire_absolu )
        for nom in liste_noms : # candidat
            nom_relatif = os.path.join( repertoire_relatif, nom )
            if nom_base in nom : # elu
                liste_noms_relatifs.append( nom_relatif )
            ajouter( liste_noms_relatifs, repertoire_racine, nom_base, nom_relatif ) # recursivite

    # liste_noms_relatifs_data (chemin relatif par rapport a repertoire_racine_data)
    liste_noms_relatifs_data = []
    ajouter( liste_noms_relatifs_data, repertoire_racine_data, repertoire_data_base )

    # liste_choix : paires (nom_relatif,nom_texte)
    liste_choix = []
    for nom_relatif_data in liste_noms_relatifs_data :
        nom_texte = u"répertoire" + " " + nom_relatif_data
        liste_choix.append( (nom_relatif_data,nom_texte) )

    #print "---------------------------------------"
    #print " DATAS : ", liste_choix
    #print "---------------------------------------"
    return liste_choix

###############################################################################
#
# Formulaire d'administration des modeles record
#
# Saisie (creation...) des modeles record (edition 'read write')
#
###############################################################################
class ModeleRecordAdminForm(forms.ModelForm):

	# nom (translate)

    # description (translate)

    # pour nom_repertoire_lot_pkgs et nom_pkg
    path_lots_pkgs = get_conf_repertoire_racine_lots_pkgs()
    liste_versions_vle = CONF_bd_modeles_record.liste_versions_vle

    # nom_repertoire_lot_pkgs
    # (nom relatif a partir du repertoire racine path_lots_pkgs).
    # Les repertoires presentes/proposes a l'usr se situent sous un sous
    # repertoire liste_versions_vle du repertoire racine path_lots_pkgs.

    label_admin = ParDefautModeleRecord.label_nom_repertoire_lot_pkgs
    help_text = ParDefautModeleRecord.help_text_nom_repertoire_lot_pkgs + " " + _(u"Seuls sont proposés les répertoires situés sous le répertoire") + " " + path_lots_pkgs + ", " + _(u"plus exactement sous un de ses sous-répertoires de version.")

    liste_choix_lots_pkgs = get_liste_choix_noms_relatifs_repertoires_lots_pkgs( path_lots_pkgs, liste_versions_vle )
    liste_choix = [ choix_vide ]
    for v in liste_choix_lots_pkgs :
        liste_choix.append( v )
    nom_repertoire_lot_pkgs = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix )

    # nom_pkg
    label_admin = ParDefautModeleRecord.label_nom_pkg
    help_text = ParDefautModeleRecord.help_text_nom_pkg

    liste_choix_noms_pkg = get_liste_choix_noms_pkg( path_lots_pkgs, liste_versions_vle )
    liste_choix = [ choix_vide ]
    for v in liste_choix_noms_pkg :
        liste_choix.append( v )
    nom_pkg = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix )

    # pour nom_repertoire_data
    path_data = get_conf_repertoire_racine_data()
    data_base = get_conf_repertoire_data_base()

    ###########################################################################
    # nom_repertoire_data
    # (nom relatif a partir du repertoire racine path_data).
    # Vaut 'defaultValueNomRepertoireData' si 'repertoire par defaut' choisi.
    # Les repertoires presentes/proposes a l'usr se situent sous le repertoire
    # racine path_data, et repondent a d'autres criteres (cf data_base).
    ###########################################################################

    label_admin = ParDefautModeleRecord.label_nom_repertoire_data
    help_text = ParDefautModeleRecord.help_text_nom_repertoire_data + " " + _(u"Seuls sont proposés les répertoires situés sous le répertoire") + " " + path_data + " (" + _(u"plus exactement sous toute son arborescence") + ") " + _(u"et dont le nom contient") + " '" + data_base + "', " + _(u"ou bien le répertoire par défaut (sous-répertoire 'data' du paquet principal du modèle)") + "."

    liste_choix_data = get_liste_choix_noms_relatifs_repertoires_datas( path_data, data_base )
    liste_choix = [ ( defaultValueNomRepertoireData(), _(u"répertoire par défaut (sous-répertoire 'data' du paquet principal)") ) ]
    for v in liste_choix_data :
        liste_choix.append( v )
    nom_repertoire_data = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix, required=False )

    # mot_de_passe
    label_admin = ParDefautModeleRecord.label_mot_de_passe
    help_text = ParDefautModeleRecord.help_text_mot_de_passe + _(u" Associer à un modèle un des mots de passe existants ou en créer un nouveau")
    mot_de_passe = forms.ModelChoiceField(label=label_admin, help_text=help_text, queryset=MotDePasse.objects.all())

    ###########################################################################
    # Fichiers vpz de configuration : 
    # En l'etat actuel des choses, au niveau de l'outil_web_record, la seule
    # syntaxe imposee pour le nom des 3 fichiers vpz de configuration est
    # l'extension extension_config.
    # Les fichiers presentes/proposes a l'usr se situent sous le repertoire 
    # racine path_config et repondent a d'autres criteres (cf
    # get_conf_repertoire_config_web_conf()...)
    ###########################################################################

    max_length_config = ParDefautModeleRecord.max_length_config

    path_config = get_conf_repertoire_racine_configuration()
    extension_config = "vpz"
    match_config = ".*\." + extension_config + "$"

    # nom_fichier_config_web_conf (optionnel)
    # (nom relatif a partir du repertoire racine path_config)
    label_admin = ParDefautModeleRecord.label_nom_fichier_config_web_conf
    help_text= ParDefautModeleRecord.help_text_nom_fichier_config_web_conf + _(u" Seuls sont proposés les fichiers situés sous un répertoire") + " " + get_conf_repertoire_config_web_conf() + " " + _(u"de") + " " + path_config + "."
    file_path_field = FilePathFieldConfigWebConf( max_length=max_length_config, path=path_config, match=match_config, recursive=True )
    liste_choix = file_path_field.get_liste_choix_noms_relatifs_fichiers() 
    nom_fichier_config_web_conf = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix, required=False )

    # nom_fichier_config_web_applis (optionnel)
    # (nom relatif a partir du repertoire racine path_config)
    label_admin = ParDefautModeleRecord.label_nom_fichier_config_web_applis
    help_text= ParDefautModeleRecord.help_text_nom_fichier_config_web_applis + _(u" Seuls sont proposés les fichiers situés sous un répertoire") + " " + get_conf_repertoire_config_web_applis() + " " + _(u"de") + " " + path_config + "."
    file_path_field = FilePathFieldConfigWebApplis( max_length=max_length_config, path=path_config, match=match_config, recursive=True )
    liste_choix = file_path_field.get_liste_choix_noms_relatifs_fichiers() 
    nom_fichier_config_web_applis = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix, required=False ) 

    # nom_fichier_config_dicos (optionnel)
    # (nom relatif a partir du repertoire racine path_config)
    label_admin = ParDefautModeleRecord.label_nom_fichier_config_dicos
    help_text= ParDefautModeleRecord.help_text_nom_fichier_config_dicos + _(u" Seuls sont proposés les fichiers situés sous un répertoire") + " " + get_conf_repertoire_config_dicos() + " "+ _(u"de")+ " " + path_config + "."
    file_path_field = FilePathFieldConfigDicos( max_length=max_length_config, path=path_config, match=match_config, recursive=True )
    liste_choix = file_path_field.get_liste_choix_noms_relatifs_fichiers() 
    nom_fichier_config_dicos = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix, required=False )

	# responsable
    label_admin = ParDefautModeleRecord.label_responsable
    help_text = ParDefautModeleRecord.help_text_responsable + _(u" Associer à un modèle un des individus existants ou en créer un nouveau")
    responsable = forms.ModelChoiceField(label=label_admin, help_text=help_text, queryset=Individu.objects.all())

    # responsable_scientifique
    label_admin = ParDefautModeleRecord.label_responsable_scientifique
    help_text = ParDefautModeleRecord.help_text_responsable_scientifique + _(u" Associer à un modèle un des individus existants ou en créer un nouveau")
    responsable_scientifique = forms.ModelChoiceField(label=label_admin, help_text=help_text, queryset=Individu.objects.all())

    # responsable_informatique
    label_admin = ParDefautModeleRecord.label_responsable_informatique
    help_text = ParDefautModeleRecord.help_text_responsable_informatique + _(u" Associer à un modèle un des individus existants ou en créer un nouveau")
    responsable_informatique = forms.ModelChoiceField(label=label_admin, help_text=help_text, queryset=Individu.objects.all())

    # cr_reception (translate)

    # version_vle
    label_admin = ParDefautModeleRecord.label_version_vle
    help_text = ParDefautModeleRecord.help_text_version_vle
    liste_choix = [ choix_vide ]
    for v in CONF_bd_modeles_record.liste_versions_vle :
        liste_choix.append( (v,v) )
    version_vle = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix )

    class Meta:
        model = ModeleRecord

    ###########################################################################
    #
    # Methode de verification du ModeleRecord saisi
    #
    ###########################################################################
    def clean(self):

        def valeur( cleaned_data, key ) :
            v = None
            if key in cleaned_data :
                v = cleaned_data.get( key )
            return v

        cleaned_data = super(ModeleRecordAdminForm, self).clean()

        #nom = valeur( cleaned_data, 'nom' )
        #description = valeur( cleaned_data, 'description' )
        nom_repertoire_lot_pkgs = valeur( cleaned_data, 'nom_repertoire_lot_pkgs' )
        nom_pkg = valeur( cleaned_data, 'nom_pkg' )
        nom_repertoire_data = valeur( cleaned_data, 'nom_repertoire_data' )
        #mot_de_passe = valeur( cleaned_data, 'mot_de_passe' )
        nom_fichier_config_web_conf = valeur( cleaned_data, 'nom_fichier_config_web_conf' )
        nom_fichier_config_web_applis = valeur( cleaned_data, 'nom_fichier_config_web_applis' )
        nom_fichier_config_dicos = valeur( cleaned_data, 'nom_fichier_config_dicos' )
        #responsable = valeur( cleaned_data, 'responsable' )
        #responsable_scientifique = valeur( cleaned_data, 'responsable_scientifique' )
        #responsable_informatique = valeur( cleaned_data, 'responsable_informatique' )
        #cr_reception = valeur( cleaned_data, 'cr_reception' )
        version_vle = valeur( cleaned_data, 'version_vle' )

        # inutilise
        #instance = getattr(self, 'instance', None)
        #if instance and instance.id : modele_record = instance
        #else : modele_record = None

        # chemins absolus
        nom_absolu_repertoire_lot_pkgs = None
        nom_absolu_repertoire_data = None
        nom_absolu_fichier_config_web_conf = None
        nom_absolu_fichier_config_web_applis = None
        nom_absolu_fichier_config_dicos = None
        if nom_repertoire_lot_pkgs is not None : 
            nom_absolu_repertoire_lot_pkgs = getNomAbsoluRepertoireLotPkgs( nom_repertoire_lot_pkgs ) 
        if nom_repertoire_data is not None :
            nom_absolu_repertoire_data = getNomAbsoluRepertoireData( nom_repertoire_data ) 
        if nom_fichier_config_web_conf is not None : 
            nom_absolu_fichier_config_web_conf = getNomAbsoluFichierConfigWebConf( nom_fichier_config_web_conf ) 
        if nom_fichier_config_web_applis is not None : 
            nom_absolu_fichier_config_web_applis = getNomAbsoluFichierConfigWebApplis( nom_fichier_config_web_applis ) 
        if nom_fichier_config_dicos is not None : 
            nom_absolu_fichier_config_dicos = getNomAbsoluFichierConfigDicos( nom_fichier_config_dicos ) 

        # verification de version_vle par rapport a nom_repertoire_lot_pkgs
        if version_vle is not None and nom_repertoire_lot_pkgs is not None : 
            if version_vle not in nom_repertoire_lot_pkgs : 
                msg = _(u"incompatibilité entre") + " version_vle (" + version_vle + ") " + _(u"et") + " nom_repertoire_lot_pkgs (chemin relatif " + nom_repertoire_lot_pkgs + ")"
                raise forms.ValidationError( msg )

        # pour chacun de nom_fichier_config_web_conf,
        # nom_fichier_config_web_applis, nom_fichier_config_dicos 
        # verifier que le fichier existe (s'il a ete saisi)

        nom_fichier = nom_fichier_config_web_conf
        nom_absolu_fichier = nom_absolu_fichier_config_web_conf
        if nom_fichier is not None :
            if nom_fichier != "" : # nom_fichier (facultatif) a bien ete saisi
                if not verificationFichierOk( nom_absolu_fichier ) :
                    msg = _(u"problème de fichier de configuration") + " : "
                    msg = msg + _(u"le fichier") + " " + nom_absolu_fichier + " " + _(u"n'existe pas")
                    raise forms.ValidationError( msg )
        nom_fichier = nom_fichier_config_web_applis
        nom_absolu_fichier = nom_absolu_fichier_config_web_applis
        if nom_fichier is not None :
            if nom_fichier != "" : # nom_fichier (facultatif) a bien ete saisi
                if not verificationFichierOk( nom_absolu_fichier ) :
                    msg = _(u"problème de fichier de configuration") + " : "
                    msg = msg + _(u"le fichier") + " " + nom_absolu_fichier + " " + _(u"n'existe pas")
                    raise forms.ValidationError( msg )
        nom_fichier = nom_fichier_config_dicos 
        nom_absolu_fichier = nom_absolu_fichier_config_dicos 
        if nom_fichier is not None :
            if nom_fichier != "" : # nom_fichier (facultatif) a bien ete saisi
                if not verificationFichierOk( nom_absolu_fichier ) :
                    msg = _(u"problème de fichier de configuration") + " : "
                    msg = msg + _(u"le fichier") + " " + nom_absolu_fichier + " " + _(u"n'existe pas")
                    raise forms.ValidationError( msg )

        # verifier que le repertoire nom_repertoire_lot_pkgs existe
        if nom_repertoire_lot_pkgs is not None :
            if not verificationLargeRepertoireOk( nom_absolu_repertoire_lot_pkgs ) :
                msg = _(u"le répertoire") + " " + nom_absolu_repertoire_lot_pkgs + " " + _(u"n'existe pas")
                raise forms.ValidationError( msg )

        # verifier que le repertoire nom_pkg existe sous le repertoire
        # nom_repertoire_lot_pkgs 
        if nom_absolu_repertoire_lot_pkgs is not None and nom_pkg is not None : 
            nom_absolu_repertoire_pkg = os.path.join( nom_absolu_repertoire_lot_pkgs, nom_pkg ) 
            if not verificationLargeRepertoireOk( nom_absolu_repertoire_pkg ) :
                msg = _(u"incompatibilité entre") + " nom_pkg (" + nom_pkg + ") " + _(u"et") + " nom_repertoire_lot_pkgs (chemin relatif " + nom_repertoire_lot_pkgs + ")" + " : " 
                msg = msg + _(u"il n'existe pas de répertoire ") + nom_pkg + " " + _(u"sous le répertoire ") + nom_absolu_repertoire_lot_pkgs 
                raise forms.ValidationError( msg )

        # verifier que le repertoire nom_repertoire_data (ou le repertoire
        # par defaut si 'repertoire par defaut' choisi) existe
        if nom_repertoire_data is not None :

            if nom_repertoire_data == defaultValueNomRepertoireData() :
            # correspond au choix du repertoire par defaut

                if nom_repertoire_lot_pkgs is not None and nom_pkg is not None : 
                    nom_absolu_repertoire_data_default = getNomAbsoluRepertoireDataParDefaut( nom_repertoire_lot_pkgs, nom_pkg )
                    if not verificationLargeRepertoireOk( nom_absolu_repertoire_data_default ) :
                        msg = _(u"problème de répertoire par défaut des fichiers de données") + " : "
                        msg = msg + _(u"le répertoire par défaut (sous-répertoire 'data' du paquet principal)") + " " + nom_absolu_repertoire_data_default + " " + _(u"n'existe pas")
                        raise forms.ValidationError( msg )
            else :
                if not verificationLargeRepertoireOk( nom_absolu_repertoire_data ) :
                    msg = _(u"problème de répertoire des fichiers de données") + " : "
                    msg = msg + _(u"le répertoire") + " " + nom_absolu_repertoire_data + " " + _(u"n'existe pas")
                    raise forms.ValidationError( msg )

        return cleaned_data

###############################################################################

