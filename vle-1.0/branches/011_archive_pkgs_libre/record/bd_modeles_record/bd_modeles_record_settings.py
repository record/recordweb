#-*- coding:utf-8 -*-

# For django settings of projects that use bd_modeles_record applications.

###############################################################################

import os

# 'Verifie' le fichier f (existe et il s'agit bien d'un fichier)
def verificationFichierOk( f ) :
    verif = os.path.exists(f) and os.path.isfile(f)
    return verif

# 'Verifie' le repertoire rep : les liens symboliques sont acceptes
def verificationLargeRepertoireOk( rep ) :
    verif = os.path.exists(rep) and os.path.isdir(rep)
    return verif

class bd_modeles_record_settings(object) :

    def __init__(self, var_RECORDWEB_HOME,
                       var_VERSIONS_VLE,
                       var_LOTS_PKGS_PATH,
                       var_CONFIGURATION_PATH,
                       var_DATA_PATH,
                       var_RECORDWEB_VENDOR,
                       var_NOM_PAGE_LISTE_MODELES_RECORD,
                       var_ACTIONS ) :

        #######################################################################
        #
        # IN (from settings) : verification de declarations et existences
        #
        #######################################################################

        # RECORDWEB_HOME, repertoire racine (projets, applications, librairies python)
        RECORDWEB_HOME = var_RECORDWEB_HOME
        # verif
        rep = RECORDWEB_HOME
        if not verificationLargeRepertoireOk( rep ) :
            print "ERREUR !!! bd_modeles_record_settings.py : répertoire ", rep, " N'EXISTE PAS"
        rep = os.path.join( RECORDWEB_HOME, 'record' )
        if not verificationLargeRepertoireOk( rep ) :
            print "ERREUR !!! bd_modeles_record_settings.py : répertoire ", rep, " N'EXISTE PAS"

        # VERSIONS_VLE, liste des versions vle gerees/prises en compte
        VERSIONS_VLE = var_VERSIONS_VLE
        # verif
        if isinstance( VERSIONS_VLE, list ) :
            for v in VERSIONS_VLE : 
                if not isinstance( v, str ) :
                    print "ERREUR !!! bd_modeles_record_settings.py : VERSIONS_VLE N'EST PAS UNE LISTE DE str (cf ", v, " )"
        else :
            print "ERREUR !!! bd_modeles_record_settings.py : VERSIONS_VLE N'EST PAS UNE LISTE ( ", VERSIONS_VLE, " )"

        # depot des modeles record ####################################################
        # - LOTS_PKGS_PATH : lots des paquets vle (pouvant contenir des fichiers de
        #   configuration, contenant des repertoires data)
        # - CONFIGURATION_PATH : repertoire englobant l'ensemble des fichiers de
        #   configuration/personnalisation (ceux presents dans les paquets vle, et
        #   d'autres en plus)
        # - DATA_PATH : repertoire englobant l'ensemble des repertoires de donnees de
        #   simulation (ceux presents dans les paquets vle, et d'autres en plus)

        # repertoire racine sous lequel est cherche l'ensemble des lots des paquets vle des modeles record
        LOTS_PKGS_PATH = var_LOTS_PKGS_PATH
        # verif
        rep = LOTS_PKGS_PATH
        if not verificationLargeRepertoireOk( rep ) :
            print "ERREUR !!! bd_modeles_record_settings.py : répertoire ", rep, " N'EXISTE PAS"

        # repertoire racine sous lequel est cherche l'ensemble des fichiers de configuration (pas partout, seulement dans certains sous repertoires, cf code)
        CONFIGURATION_PATH = var_CONFIGURATION_PATH
        # verif
        rep = CONFIGURATION_PATH
        if not verificationLargeRepertoireOk( rep ) :
            print "ERREUR !!! bd_modeles_record_settings.py : répertoire ", rep, " N'EXISTE PAS"

        # repertoire racine sous lequel est cherche l'ensemble des repertoires de donnees de simulation (pas partout, seulement dans certains sous repertoires, cf code)
        DATA_PATH = var_DATA_PATH
        # verif
        rep = DATA_PATH
        if not verificationLargeRepertoireOk( rep ) :
            print "ERREUR !!! bd_modeles_record_settings.py : répertoire ", rep, " N'EXISTE PAS"

        # vendors #############################################################

        # repertoire des librairies, applications django externes (django-transmeta...)
        RECORDWEB_VENDOR = var_RECORDWEB_VENDOR
        # verif
        rep = RECORDWEB_VENDOR
        if not verificationLargeRepertoireOk( rep ) :
            print "ERREUR !!! bd_modeles_record_settings.py : répertoire ", rep, " N'EXISTE PAS"

        # configuration du fonctionnement de l'application ####################

        # nom de fichier
        NOM_PAGE_LISTE_MODELES_RECORD=var_NOM_PAGE_LISTE_MODELES_RECORD
        # verif
        nom = NOM_PAGE_LISTE_MODELES_RECORD
        if not isinstance( nom, str ) :
            print "ERREUR !!! bd_modeles_record_settings.py : NOM_PAGE_LISTE_MODELES_RECORD ", nom, " N'EST PAS UN str"
        elif nom == '' : # str
            print "AVERTISSEMENT !!! bd_modeles_record_settings.py : NOM_PAGE_LISTE_MODELES_RECORD n'est pas redefinie (c'est la valeur par defaut qui va servir)"

        # ACTIONS
        ACTIONS = var_ACTIONS
        # verif
        if isinstance( ACTIONS, list ) :
            for d in ACTIONS : 
                if not isinstance(d,dict) :
                    print "ERREUR !!! bd_modeles_record_settings.py : ACTIONS N'EST PAS UNE LISTE DE dict (cf ", d, " )"
                elif 'label' not in d.keys() or 'type' not in d.keys() :
                    print "ERREUR !!! bd_modeles_record_settings.py : ACTIONS CONTIENT UN dict sans cle 'label' et 'type'(cf ", d, " )"
                elif 'url' not in d.keys() : # 'url' optionnel
                    print "AVERTISSEMENT !!! bd_modeles_record_settings.py : ACTIONS CONTIENT UN dict sans cle 'url' (optionnel) (cf ", d, ") (c'est la valeur par defaut qui va servir si elle existe)"
        else :
            print "ERREUR !!! bd_modeles_record_settings.py : ACTIONS N'EST PAS UNE LISTE ( ", ACTIONS, " )"

        #######################################################################
        #
        # OUT (to settings) : elements de configuration propres a bd_modeles_record
        #
        #######################################################################

        #######################################################################
        # path de bd_modeles_record (correspond a RECORDWEB_HOME/bd_modeles_record)
        SITE_ROOT = os.path.realpath(os.path.dirname(__file__))

        # TEMPLATE_DIRS #######################################################

        # emplacement des templates
        # propres a projet bd_modeles_record : templates_bd_modeles_record 
        # publics : templates_public
        templates_bd_modeles_record = os.path.join( SITE_ROOT, 'templates' )
        templates_public = os.path.join( RECORDWEB_HOME, 'record', 'templates' )

        self.TEMPLATE_DIRS_FIRST = ( templates_bd_modeles_record,)
        self.TEMPLATE_DIRS_LAST = ( templates_public,)

        # LOCALE_PATHS ########################################################

        # emplacement translation file commun recordweb
        # (en amont de ceux de chaque projet)
        translation_recordweb = os.path.join( RECORDWEB_HOME, 'record', 'locale' )

        # directories where Django looks for translation files
        # (en plus des repertoires 'locale' de projet etc)
        # (du plus au moins prioritaire)
        #
        # Actuellement le propre dictionnaire de bd_modeles_record existe mais inusite
        #
        self.LOCALE_PATHS = ( translation_recordweb,)

        # STATICFILES_DIR #####################################################

        # emplacement des fichiers css et js
        # propres a l'outil_web_record : SITE_MEDIA_OUTIL_WEB_RECORD
        # publics : SITE_MEDIA_PUBLIC
        SITE_MEDIA_BD_MODELES_RECORD = os.path.join( SITE_ROOT, 'site_media' )
        SITE_MEDIA_PUBLIC = os.path.join( RECORDWEB_HOME, 'record', 'site_media' )

        # Additional locations of static files
        self.STATICFILES_DIRS_FIRST = (
            # STATICFILES_DIRS avec collectstatic (du + au - prioritaire )
            SITE_MEDIA_BD_MODELES_RECORD, # os.path.join( SITE_ROOT, 'site_media' )
        )
        self.STATICFILES_DIRS_LAST = (
            # STATICFILES_DIRS avec collectstatic (du + au - prioritaire )
            SITE_MEDIA_PUBLIC,
        )

###############################################################################

