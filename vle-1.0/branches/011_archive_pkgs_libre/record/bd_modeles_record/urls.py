
from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('record.bd_modeles_record.views',

    url(r'^$', 'les_modeles_record'),
    url(r'^les_modeles_record/$', 'les_modeles_record'),
    url(r'^bdrec/usr/les_modeles_record/$', 'les_modeles_record'),

    #old url(r'^un_modele_record_menu/(?P<modele_record_id>\d+)/$', 'un_modele_record_menu'),
    #old url(r'^bdrec/usr/un_modele_record_menu/(?P<modele_record_id>\d+)/$', 'un_modele_record_menu'),
    url(r'^un_modele_record_menu/$', 'un_modele_record_menu'),
    url(r'^bdrec/usr/un_modele_record_menu/$', 'un_modele_record_menu'),

    #old url(r'^un_modele_record_mdp/(?P<modele_record_id>\d+)/$', 'un_modele_record_mdp'),
    #old url(r'^bdrec/usr/un_modele_record_mdp/(?P<modele_record_id>\d+)/$', 'un_modele_record_mdp'),
    url(r'^un_modele_record_mdp/$', 'un_modele_record_mdp'),
    url(r'^bdrec/usr/un_modele_record_mdp/$', 'un_modele_record_mdp'),
)

