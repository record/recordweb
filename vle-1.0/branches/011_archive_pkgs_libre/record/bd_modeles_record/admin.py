#-*- coding:utf-8 -*-

###############################################################################
# File bd_modeles_record/admin.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Authors :
# Nathalie Rousse, INRA RECORD team member,
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
#
# Administration de la bd des modeles record
#
###############################################################################

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from record.bd_modeles_record.models import Individu, MotDePasse, ModeleRecord
from record.bd_modeles_record.models import estMotDePassePublic, enregistrerMotDePassePublic

from record.bd_modeles_record.forms.admin_forms import ModeleRecordAdminForm

###############################################################################
# Pour que certaines parties/rubriques administratives n'apparaissent pas
###############################################################################
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.contrib.auth.models import Group
# admin.site.unregister(User) # partie User (en commentaire pour voir les comptes administrateur)
admin.site.unregister(Group)  # partie Group
admin.site.unregister(Site)   # partie Site

from django.utils.translation import ugettext as _


###############################################################################
# Ajout/enregistrement du MotDePasse public s'il n'existe pas encore dans la
# bd des modeles record
###############################################################################

modele_public_absent = True # par defaut
liste_mots_de_passe = MotDePasse.objects.all()
for mot_de_passe in liste_mots_de_passe :
    if estMotDePassePublic( mot_de_passe ) :
        modele_public_absent = False
if modele_public_absent :
    enregistrerMotDePassePublic()

###############################################################################
#
# Administration MotDePasse
#
###############################################################################
class MotDePasseAdmin(UserAdmin):

    texte_description = _(u"Un mot de passe est associé à un ou plusieurs modèles privés. Les modèles publics ont pour mot de passe modele_public.")

    fieldsets = (

        ( _(u"MOT DE PASSE"), { 'description': texte_description,
                                'fields': () } ),

        ( None, { 'fields': ('username',
                             'password') } ),
    )

admin.site.register(MotDePasse, MotDePasseAdmin)

###############################################################################
#
# Administration Individu
#
###############################################################################
class IndividuAdmin(UserAdmin):

    texte_description = _(u"Un individu est une personne susceptible d\'endosser le rôle de responsable et/ou responsable scientifique et/ou responsable informatique par rapport à un ou plusieurs modèles")

    fieldsets = (

        ( _(u"INDIVIDU"), { 'description': texte_description,
                            'fields': () } ),

        ( None, { 'fields': ('username',
                             'first_name',
                             'last_name',
                             'email') } ),
    )

admin.site.register(Individu, IndividuAdmin)

###############################################################################
#
# Administration ModeleRecord
#
###############################################################################
class ModeleRecordAdmin(admin.ModelAdmin):

    form = ModeleRecordAdminForm

    list_display = ('nom', 'nom_pkg')

    texte_description = _(u"Les modèles sont ceux de la plate-forme Record qui sont mis à disposition au sein de l\'outil Web Record. Un modèle est caractérisé par son lot de paquets vle (l'ensemble des paquets vle requis pour son fonctionnement : paquets de dependance et paquet principal), son paquet principal (contenant ses fichiers vpz scénario de simulation) et un certain nombre d\'autres renseignements nécessaires à son exploitation. Il a un mot de passe privé ou alors son mot de passe est modele_public s\'il est public. Ses responsables sont des individus. Les 3 fichiers vpz de configuration servent à personnaliser la présentation du modèle (ses scénarios et leurs données) au sein de l\'outil Web Record.")

    #
    #from transmeta import canonical_fieldname
    # pour memo : methode canonical_fieldname
    # pour relation entre 'nom'... et 'nom_fr','nom_en'...
    #
    #def formfield_for_dbfield(self, db_field, **kwargs):
    #    field = super(ModeleRecordAdmin, self).formfield_for_dbfield(db_field, **kwargs)
    #    db_fieldname = canonical_fieldname(db_field)
    #
    #        print "db_field.name : ", db_field.name, "  ***  ", "db_fieldname : ", db_fieldname
    #
    #        if db_fieldname == 'description':
    #        # this applies to all description_* fields
    #        print "!!! description"
    #        #field.widget = MyCustomWidget()
    #    elif db_field.name == 'nom_en':
    #        # this applies only to body_es field
    #        print "!!! nom_en"
    #        ###field.widget = MyCustomWidget()
    #    return field
    #

    fieldsets = (

        ( _(u"MODELE RECORD"), { 'description': texte_description,
                                 'fields': () } ),

        ( None, { 'fields': ( 'nom_fr',
                              'nom_en',
                              'description_fr',
                              'description_en',
                              'cr_reception_fr',
                              'cr_reception_en' ) } ),

        ( _(u"CARACTERE PUBLIC/PRIVE"), { 'fields': ( 'mot_de_passe',) } ),

        ( _(u"RESPONSABLES"), { 'fields': ( 'responsable',
                              'responsable_scientifique',
                              'responsable_informatique',) } ),

        ( _(u"ASPECT VLE (PAQUETS...)"), { 'fields': ( 'nom_repertoire_lot_pkgs',
                              'nom_pkg',
                              'nom_repertoire_data',
                              'version_vle') } ),

        ( _(u"CONFIGURATION PERSONNALISATION (optionnel)"), { 'fields': ( 'nom_fichier_config_web_conf',
                              'nom_fichier_config_web_applis',
                              'nom_fichier_config_dicos') } ),
    )

    list_filter = [ 'nom_fr',
                    'nom_en',
                    #'nom_repertoire_lot_pkgs',
                    'nom_pkg',
                    #'nom_repertoire_data',
                    'version_vle',
                    #'nom_fichier_config_web_conf',
                    #'nom_fichier_config_web_applis',
                    #'nom_fichier_config_dicos',
                    'responsable',
                    'responsable_scientifique',
                    'responsable_informatique',
                    'mot_de_passe' ]

    search_fields = [ 'nom_fr',
                    'nom_en',
                    'nom_pkg',
                    'version_vle',
                    'responsable',
                    'responsable_scientifique',
                    'responsable_informatique' ]

admin.site.register(ModeleRecord, ModeleRecordAdmin)

###############################################################################

