#-*- coding:utf-8 -*-

###############################################################################
# File views.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
#
# View scn_selection
#
# Choix/selection d'un scenario de simulation (fichier vpz) parmi ceux du
# modele record considere.
#
# Fait partie du processus d'exploitation des scenarios par les utilisateurs,
# selon lequel il s'agit de choisir un scenario de simulation, puis de le
# simuler apres en avoir visualise et eventuellement modifie la configuration
# (parametrage), et enfin d'exploiter sous diverses formes les resultats de
# la simulation effectuee (les visualiser a l'ecran, telecharger...).
#
###############################################################################


from django.shortcuts import render_to_response

from record.bd_modeles_record.models import ModeleRecord as ModeleRecordBd
from record.bd_modeles_record.forms.user_forms import MotDePasseAskingForm
from record.bd_modeles_record.forms.user_forms import ModeleRecordFormComplet

from record.outil_web_record.models.modele_record.modele_record import ModeleRecordOn
from record.outil_web_record.models.espace_exploitation.espace_exploitation import EspaceExploitation

from record.outil_web_record.models.affichage_saisie.appel_page import NomsPages
from record.outil_web_record.models.affichage_saisie.appel_page import contexte_modele_record_mdp
from record.outil_web_record.models.affichage_saisie.appel_page import contexte_liste_scenarios

from record.utils import session
from configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

from django.utils.translation import ugettext as _

###############################################################################
#
# CHOIX/SELECTION SCENARIO
#
###############################################################################

###############################################################################
# liste_scenarios
#
# Point d'entree de l'outil_web_record : initialisations, nettoyage, 
# preparation et affichage page du choix d'un scenario de simulation (parmi
# ceux du modele record)
#
# - Si modele record prive non deverrouille (mot de passe pas encore saisi) : 
# avant de montrer la liste des scenarios du modele, il est demande a l'usr 
# de saisir le mot de passe du modele.
# - Si modele public ou modele prive deverrouille (mot de passe deja saisi) :
# il est affiche la liste des scenarios du modele dans laquelle l'usr peut
# selectionner celui de son choix.
#
###############################################################################
def liste_scenarios(request, modele_record_id):

    ###########################################################################
    # les informations en entree
    ###########################################################################

    modele_record_id = int(modele_record_id)
    modele_record_bd = ModeleRecordBd.objects.get(id=modele_record_id)

    formulaire_mot_de_passe = MotDePasseAskingForm(request.POST)

    ###########################################################################
    # determination de l'etat deverrouillage (avec maj de flashes)
    ###########################################################################

    # flashes, initialisation
    flashes = [] # aucun message d'erreur en flashes par defaut

    deverrouillage = False # par defaut
    if modele_record_bd.isPublic() :
        deverrouillage = True # modele public
    else : # modele prive
        if formulaire_mot_de_passe.is_valid(): # verifier mot de passe saisi
            if modele_record_bd.checkPassword( formulaire_mot_de_passe.get_valeur_saisie() ) :
                deverrouillage = True # bon mot de passe
            else : # mauvais mot de passe, le redemander
                deverrouillage = False
                flashes.append( _(u"mauvaise saisie") +", " + _(u"nouvel essai") )

    ###########################################################################
    # traitement (en fonction de deverrouillage)
    ###########################################################################

    if not deverrouillage : # demande du mot de passe
        c = contexte_modele_record_mdp(request, flashes, modele_record_id )
        return render_to_response( NomsPages.NOM_page_modele_record_mdp, c )

    else : # deverrouillage

        #######################################################################
        # Nettoyage
        #######################################################################

        # nettoyage relatif aux repertoires d'exploitation devenus obsoletes
        # (suppression, maj pkgs)
        EspaceExploitation.nettoyage()

        # session : suppression de l'eventuel scn precedent
        if session.existe(request, 'scn') :
            session.get(request, 'scn').supprimer() # pour aussi supprimer repertoire d'exploitation
        session.suppression_prealable(request, 'scn')


        #  ici !!!! definir VLE_HOME la 1ere fois !!!
        # il faut que ce soit avant 1er appel de Vle : ie ModeleRecordOn











        #######################################################################
        # des essais temporaires relatifs a VLE_HOME
        #######################################################################
        # dans code pyvle.cpp : vle::manager::init() n'est appele qu'au premier appel du constructeur de Vle, les modifications ulterieures de la variable d'environnement VLE_HOME ne sont pas prises en compte
        #######################################################################
        # essai not ok : tous les fichiers resultat (wwdm, 2CV, MilSol) se trouvent sous output du paquet MilSol (derniere creation/construction Vle3) ; taille des fichiers resultat de wwdm et 2CV : 0 octet

#       import os
#       print ""
#       print "EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE"
#       print ""
#       print " os.environ.get('VLE_HOME') = ", os.environ.get('VLE_HOME')
#       print ""
#       # from pyvle import Vle remplace par : a chaque session son pyvleX
#
#       print "exp_wwdm wwdm sous WXC"
#       print "os.environ['VLE_HOME'] = '/home/nrousse/WXC'" 
#       os.environ['VLE_HOME'] = '/home/nrousse/WXC' 
#       print "exp_wwdm = Vle( 'wwdm.vpz', 'wwdm' )"
#       from pyvle1 import Vle1
#       exp_wwdm = Vle1( 'wwdm.vpz', 'wwdm' )
#
#       print "exp_cv 2CV sous .vle"
#       print "os.environ['VLE_HOME'] = '/home/nrousse/.vle'" 
#       os.environ['VLE_HOME'] = "/home/nrousse/.vle" 
#       print "exp_cv = Vle( '2CV-parcelle.vpz', '2CV' )"
#       from pyvle2 import Vle2
#       exp_cv = Vle2( '2CV-parcelle.vpz', '2CV' )
#
#       print "exp_ms MilSol sous XCV"
#       print "os.environ['VLE_HOME'] = '/home/nrousse/XCV'" 
#       os.environ['VLE_HOME'] = "/home/nrousse/XCV" 
#       print "exp_ms = Vle( 'MilSol.vpz', 'MilSol' )"
#       from pyvle3 import Vle3
#       exp_ms = Vle3( 'MilSol.vpz', 'MilSol' )
#
#       print "SIMULATION wwdm sous WXC"
#       print "exp_wwdm.run()"
#       exp_wwdm.run()
#       print "... c fait"
#
#       print "SIMULATION MilSol sous XCV"
#       print "exp_ms.run()"
#       exp_ms.run()
#       print "... c fait"
#
#       print "SIMULATION 2CV sous .vle"
#       print "exp_cv.run()"
#       exp_cv.run()
#       print "... c fait"
#
#       print " os.environ.get('VLE_HOME') = ", os.environ.get('VLE_HOME')
#
#       print ""
#       print "EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE"
#
        #######################################################################
        #######################################################################


        #######################################################################
        # Initialisations
        #######################################################################

        flashes = [] # aucun message d'erreur en flashes par defaut

        # formulaire_modele_record (rappel des infos completes a titre d'information/memo)
        formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

        # le_modele_record
        le_modele_record = None # par defaut
        le_modele_record = ModeleRecordOn( modele_record_bd ) # tentative
        if not le_modele_record.estExploitable() : # echec
            msg_off = le_modele_record.exploitabilite.getMsgEtat()
            msg = _(u"Le modèle est considéré indisponible") + ", " + _(u"problème") + " : " + msg_off
            flashes.append( msg )
            le_modele_record = None # par defaut

        #######################################################################
        # Mise en session
        #######################################################################

        session.suppression_prealable(request, 'le_modele_record' ) # prealable
        if le_modele_record is not None :
            session.set(request, 'le_modele_record', le_modele_record)
            session.sauver(request)

        #######################################################################
        # Preparation du contexte d'affichage page du choix d'un scenario
        #######################################################################

        c = contexte_liste_scenarios(request, flashes, le_modele_record, formulaire_modele_record )
        return render_to_response( NomsPages.NOM_page_liste_scenarios, c )


###############################################################################
# scenario_selection
#
# La page du choix d'un scenario du modele record a deja ete affichee une 1ere
# fois (cf liste_scenarios). Les initialisations ont deja ete effectuees :
# le_modele_record est  enregistre en session ; s'il s'agit d'un modele prive,
# il a deja ete deverrouille (mot de passe saisi). Pas de reinitialisation.
#
# Preparation et affichage de la page du choix d'un scenario du modele record
#
###############################################################################
def scenario_selection(request) :

    ###########################################################################
    # les informations en entree
    ###########################################################################

    id_operation = None # par defaut
    if "id_operation" in request.POST.keys() :
        id_operation = request.POST["id_operation"]
    id_operation = int(id_operation)

    ###########################################################################
    # Nettoyage
    ###########################################################################

    # suppression de l'eventuel scn precedent
    if session.existe(request, 'scn') :
        session.get(request, 'scn').supprimer() # pour aussi supprimer repertoire d'exploitation
    session.suppression_prealable(request, 'scn')

    ###########################################################################
    # Initialisations
    ###########################################################################

    # aucun message d'erreur en flashes
    flashes = []

    # le_modele_record
    le_modele_record = session.get(request, 'le_modele_record')

    # formulaire_modele_record (rappel des infos completes a titre d'information/memo)
    modele_record_bd = ModeleRecordBd.objects.get(id=le_modele_record.id)
    formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

    ###########################################################################
    # Preparation du contexte d'affichage page du choix d'un scenario
    ###########################################################################

    c = contexte_liste_scenarios(request, flashes, le_modele_record, formulaire_modele_record )
    return render_to_response( NomsPages.NOM_page_liste_scenarios, c )

###############################################################################

