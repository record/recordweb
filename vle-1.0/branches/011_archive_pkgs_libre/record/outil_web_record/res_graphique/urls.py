from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('record.outil_web_record.res_graphique.views',

    url(r'^graphique_menu/$', 'graphique_menu'),
    url(r'^configuration_graphique/$', 'configuration_graphique'),
    url(r'^affichage_graphique/$', 'affichage_graphique'),
    url(r'^representation_graphique/$', 'representation_graphique'),
    url(r'^conservation_graphique/$', 'conservation_graphique'),

)

