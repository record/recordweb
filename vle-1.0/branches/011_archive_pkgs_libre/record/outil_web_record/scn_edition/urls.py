
from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('record.outil_web_record.scn_edition.views',

    url(r'^scenario_initialisation/$', 'scenario_initialisation'),
    url(r'^scenario_actualisation/$', 'scenario_actualisation'),

)

