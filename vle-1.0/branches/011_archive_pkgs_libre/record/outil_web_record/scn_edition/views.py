#-*- coding:utf-8 -*-

###############################################################################
# File views.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
# View scn_edition
#
# Edition d'un scenario (visualisation et eventuelle modification).
#
# Fait partie du processus d'exploitation des scenarios par les utilisateurs,
# selon lequel il s'agit de choisir un scenario (dans un paquet), puis de le
# simuler apres en avoir visualise et eventuellement modifie la configuration
# (parametrage), et enfin d'exploiter sous diverses formes les resultats de
# la simulation effectuee (les visualiser a l'ecran, telecharger...).
#
###############################################################################

import copy

from django.shortcuts import render_to_response

from record.bd_modeles_record.models import ModeleRecord as ModeleRecordBd
from record.bd_modeles_record.forms.user_forms import ModeleRecordFormComplet

from record.outil_web_record.models.scn.scn import Scn
from record.outil_web_record.models.definition.definition import Definition 
from record.outil_web_record.models.affichage_saisie.parametres_requete import ParametresRequete
from record.outil_web_record.models.affichage_saisie.choix_operations import ChoixOperations as CHX # noms des choix
from record.outil_web_record.models.affichage_saisie.appel_page import NomsPages, contexte_affichage_scenario, contexte_simulation_menu
from record.outil_web_record.models.erreurs_gerees.erreurs_gerees import ErreursGerees as ERR # noms des erreurs

from record.utils import session
from record.utils.erreur import Erreur # gestion des erreurs
from record.utils.erreur import get_message_exception
from configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

from django.utils.translation import ugettext as _

###############################################################################
#
# EDITION DES INFORMATIONS DU SCENARIO
# (VISUALISATION ET EVENTUELLES MODIFICATIONS)
#
###############################################################################

###############################################################################
# Le scenario est choisi (index_scenario_on de index_modele_record_on, ainsi
# que sa presentation/configuration web index_application_associee)
# Prise en compte du scenario choisi (initialisation)
# Preparation de sa visualisation
###############################################################################
def scenario_initialisation(request):

    ###########################################################################
    # les informations en entree, initialisations
    ###########################################################################

    modele_record_id = None # par defaut
    if "modele_record_id" in request.POST.keys() :
        modele_record_id = request.POST["modele_record_id"]

    # !!! modele_record_id ne sert pas ; cf le_modele_record en session

    index_scenario_on = None # par defaut
    if "index_scenario_on" in request.POST.keys() :
        index_scenario_on = request.POST["index_scenario_on"]

    index_application_associee = None # par defaut
    if "index_application_associee" in request.POST.keys() :
        index_application_associee = request.POST["index_application_associee"]

    modele_record_id = int(modele_record_id)
    index_scenario_on = int(index_scenario_on)
    index_application_associee = int(index_application_associee)

    #######################################################################
    # Prise en compte du scenario choisi (initialisation) : 
    # construction de scn mis a part scn.result
    #######################################################################

    le_modele_record = session.get(request, 'le_modele_record')
    scenario_vpz = le_modele_record.getScenarioVpz( index_scenario_on )
    application_associee = le_modele_record.getApplicationWeb( index_scenario_on, index_application_associee )

    infos_application_id = application_associee.getId()

    #traceChoix( scenario_vpz, infos_application_id ) # trace 

    scn = Scn( scenario_vpz, infos_application_id )

    # session.enlever(request, 'les_modeles_record') # les_modeles_record ne sert plus

    # scn enregistre dans session
    session.suppression_prealable(request, 'scn') # eventuel scn precedent
    session.set(request, 'scn', scn)
    session.sauver(request)

    # Note : avec la creation/construction de 'scn', il a ete cree un espace
    # d'exploitation dedie aux differentes operations qui vont par la suite
    # etre effectuees/demandees sur le scenario retenu. On travaille
    # dorenavant dans cet espace d'exploitation, sur scenario_vpz et non plus
    # scenario_vpz_d_origine.

    # le contenu du repertoire data pour si besoin/voulu faciliter la saisie
    contenu_repertoire_data = scn.espace_exploitation.getListeFichiersRepPkgData()

    # contenu_repertoire_data enregistre dans session
    session.suppression_prealable(request, 'contenu_repertoire_data') # eventuel contenu_repertoire_data precedent
    session.set(request, 'contenu_repertoire_data', contenu_repertoire_data)
    session.sauver(request)

    ###########################################################################
	# Preparation du PREMIER affichage du scenario choisi
	# (pour visualisation/modification)
    ###########################################################################

    # scn

    # formulaire_modele_record (rappel des infos completes a titre d'information/memo)
    modele_record_bd = ModeleRecordBd.objects.get(id=le_modele_record.id)
    formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

    # La gestion d'erreur : erreur_saisie_blocs inactif au 1er affichage,
    # aucun message d'erreur en flashes
    flashes = []

    c = contexte_affichage_scenario(request, flashes, scn, contenu_repertoire_data, formulaire_modele_record)

    # affichage du scenario (pour la 1ere fois)
    return render_to_response( NomsPages.NOM_page_affichage_scenario, c )



###############################################################################
# Le scenario a deja ete affiche une 1ere fois.
# Le traitement effectue varie en fonction de id_operation qui caracterise
# l'endroit depuis lequel scenario_actualisation est appele et la raison de
# l'appel.
# Si appel depuis affichage scenario : met a jour le scenario (sa definition)
# a partir de l'eventuelle saisie. Sinon (par exemple appel retour en
# arriere), il n'y avait pas saisie.
# En fonction de id_operation : soit affichage du menu de simulation et sa
# preparation (par exemple dans cas de choix_validation_finale et si les
# informations affichees/saisies du scenario ont bien ete recuperees)  ; soit
# affichage du scenario et sa preparation, avec ou sans erreur selon la
# maniere dont se sont deroules les traitements (un cas sans erreur :
# choix_validation_intermediaire et les informations affichees/saisies du
# scenario ont bien ete recuperees).
###############################################################################
def scenario_actualisation(request) :

    ###########################################################################
    # les informations en entree
    ###########################################################################

    id_operation = None # par defaut
    if "id_operation" in request.POST.keys() :
        id_operation = request.POST["id_operation"]
    id_operation = int(id_operation)

    # Initialisations

    erreur_saisie_blocs = Erreur( ERR.NOM_erreur_saisie_blocs ) # desactivee par defaut

    # Cas appel en retour arriere pour affichage scenario (en cours)
    if id_operation == CHX.VAL_operation_retour_edition :

        # supprimer eventuel scn.resultat precedent ?
        # session.get(request, 'scn').resultat = None

        nom_template = NomsPages.NOM_page_affichage_scenario

    # Cas appel en retour arriere pour affichage menu de simulation
    elif id_operation == CHX.VAL_operation_retour_simulation :

        nom_template = NomsPages.NOM_page_simulation_menu

    # Cas appel depuis page d'affichage scenario (quittee par une validation)
    elif id_operation == CHX.VAL_operation_validation :

        parametres_post = ParametresRequete( request.POST )


        #print "TMPTMPTMP TMPTMPTMP TMPTMPTMP TMPTMPTMP TMPTMPTMP"
        #for name in parametres_post.get_cles() :
        #    print name, " : ", parametres_post.get_valeurs(name)
        #print "TMPTMPTMP TMPTMPTMP TMPTMPTMP TMPTMPTMP TMPTMPTMP"


        # le type de validation choisie
        # (entre choix_validationIntermediaire et choix_validationFinale)
        choix_validation = CHX.get_choix_validation(parametres_post)

        # donnees/blocs saisis : Le traitement miseAjourBlocsSaisis est applique
        # sur blocs de definition_copie (copie temporaire), et ne sera repercute
        # dans session sur scn.definition.blocs qu'en cas de bon deroulement.
        definition_copie = Definition()
        scn = session.get(request, 'scn')
        definition_copie.blocs = copy.deepcopy( scn.definition.blocs )
        try :
            cr_ok = definition_copie.miseAjourBlocsSaisis( parametres_post )

        except Exception, e :
            cr_ok = False
            erreur_saisie_blocs.activer()
            #erreur_saisie_blocs.set_message( e.message )
            erreur_saisie_blocs.set_message( get_message_exception(e) )

        if cr_ok : # bon deroulement de miseAjourBlocsSaisis

            # prise en compte du traitement (maj)
            session.get(request, 'scn').definition.blocs = copy.deepcopy( definition_copie.blocs )
            session.sauver(request)


            #print "TMPTMPTMP TMPTMPTMP TMPTMPTMP TMPTMPTMP TMPTMPTMP"
            #for bloc in session.get(request, 'scn').definition.blocs :
            #    print bloc.infos_generales.titre, " : "
            #    for data in bloc.liste_datas :
            #        print data.values
            #print "TMPTMPTMP TMPTMPTMP TMPTMPTMP TMPTMPTMP TMPTMPTMP"


            # rq : contenu_repertoire_data inchange

            if choix_validation == CHX.NOM_choix_validationFinale :
                nom_template = NomsPages.NOM_page_simulation_menu
            else : # CHX.NOM_choix_validationIntermediaire et de plus par defaut
                nom_template = NomsPages.NOM_page_affichage_scenario
        else : # mauvais deroulement de miseAjourBlocsSaisis
            nom_template = NomsPages.NOM_page_affichage_scenario # avec erreur

    if nom_template == NomsPages.NOM_page_affichage_scenario :

        #######################################################################
        # Preparation du contexte d'affichage du scenario
        #######################################################################

        scn = session.get(request, 'scn')
        contenu_repertoire_data = session.get(request, 'contenu_repertoire_data')

        # formulaire_modele_record (rappel des infos completes a titre d'information/memo)
        le_modele_record = session.get(request, 'le_modele_record')
        modele_record_bd = ModeleRecordBd.objects.get(id=le_modele_record.id)
        formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

        # Liste messages d'erreur en flashes
        flashes = [] # par defaut
        if erreur_saisie_blocs.isActive() : # maj flashes selon erreur_saisie_blocs
            texte_erreur = _(u"Erreur") + " : " + _(u"Les informations du scénario en cours (scénario d\'origine après saisie ou non de modifications) n\'ont pas pu être prises en compte") + "."
            texte_message_erreur = erreur_saisie_blocs.get_message()
            texte_precision = _(u"Retour à l\'état précédent du scénario en cours. Autrement dit ses informations actuelles ignorent les modifications qui avaient éventuellement été apportées au scénario juste avant la dernière demande de validation (boutons \'Validation intermédiaire\' ou \'Validation finale\'), qui a échoué") + "."
            texte_et_apres = _(u"Effectuer éventuellement de nouvelles modifications sur les informations du scénario en cours avant de valider (boutons \'Validation intermédiaire\' ou \'Validation finale\')") + "."
            flashes.append( texte_erreur )
            flashes.append( texte_message_erreur )
            flashes.append( texte_precision )
            flashes.append( texte_et_apres )

        c = contexte_affichage_scenario(request, flashes, scn, contenu_repertoire_data, formulaire_modele_record)

    elif nom_template == NomsPages.NOM_page_simulation_menu :

        #######################################################################
        # Preparation du contexte d'affichage du menu de simulation
        #######################################################################

	    # Il s'agit du PREMIER affichage du menu de simulation
        scn = session.get(request, 'scn')

        # pour rappel des infos completes a titre d'information/memo
        le_modele_record = session.get(request, 'le_modele_record')

        # La gestion d'erreur : erreur_echec_simulation inactif au 1er
        # affichage, aucun message d'erreur en flashes
        flashes = []

        c = contexte_simulation_menu(request, flashes, scn, le_modele_record)

    else :
        c = {} # vide

    return render_to_response(nom_template, c)


###############################################################################
#
# Utilitaires
#
###############################################################################

# trace ecran du choix
# (modele record, scenario, application web de configuration)
def traceChoix( scenario_vpz, infos_application_id ) :
    t_ecr.trait()
    m = "Le scenario choisi est le scenario : " + str(scenario_vpz.nom_vpz)
    m = m + " du modele : " + str(scenario_vpz.paquet.nom) + " (paquet " + str(scenario_vpz.paquet.nom_pkg) + "),"
    m = m + " a presenter selon la configuration web : "
    t_ecr.msg(m)

    m = "fichier vpz de configuration du dictionnaire : "
    m = m + str( infos_application_id.getNomAbsoluFichierConfigDicos() )
    m = m + " ; "
    m = m + "fichier vpz de configuration web propre aux applis web : "
    m = m + str( infos_application_id.getNomAbsoluFichierConfigWebApplis() )
    m = m + " ; "
    m = m + "nom application : "
    m = m + str( infos_application_id.getApplicationName() )
    m = m + " ; "
    m = m + "type source : "
    m = m + str( infos_application_id.getTypeSourceConfWebScenario() )
    t_ecr.msg( m )


###############################################################################

