from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('record.outil_web_record.scn_selection.views',

    # 1) scn_selection
    url(r'^', include('record.outil_web_record.scn_selection.urls')),
    url(r'^owrec/usr/', include('record.outil_web_record.scn_selection.urls')),

    # 2) scn_edition
    url(r'^', include('record.outil_web_record.scn_edition.urls')),
    url(r'^owrec/usr/', include('record.outil_web_record.scn_edition.urls')),

    # 3) scn_simulation
    url(r'^', include('record.outil_web_record.scn_simulation.urls')),
    url(r'^owrec/usr/', include('record.outil_web_record.scn_simulation.urls')),

    # 4) res_telechargement, res_numerique, res_graphique

    url(r'^', include('record.outil_web_record.res_telechargement.urls')),
    url(r'^owrec/usr/', include('record.outil_web_record.res_telechargement.urls')),

    url(r'^', include('record.outil_web_record.res_numerique.urls')),
    url(r'^owrec/usr/', include('record.outil_web_record.res_numerique.urls')),

    url(r'^', include('record.outil_web_record.res_graphique.urls')),
    url(r'^owrec/usr/', include('record.outil_web_record.res_graphique.urls')),
)

