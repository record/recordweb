
from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('record.outil_web_record.res_telechargement.views',

    url(r'^telechargement_menu/$', 'telechargement_menu'),
    url(r'^telechargement/$', 'telechargement'),

)

