#-*- coding:utf-8 -*-

###############################################################################
# File conf_web_gestion.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

import copy

from record.outil_web_record.models.conf_web.conf_web import FichierConfigSource
from record.outil_web_record.models.conf_web.conf_web import SourceConfWebModeleRecord
from record.outil_web_record.models.conf_web.conf_web import TypeSourceConfWebScenario
from record.outil_web_record.models.conf_web.conf_web import SourceConfWebScenario
from record.outil_web_record.models.conf_web.conf_web_modele_record import ConfWebModeleRecord
from record.outil_web_record.models.conf_web.conf_web_scenario import ConfWebScenario

from record.utils.vle.pkg import Pkg

from record.utils.conf_web.web_vpz.exp_config_web import ExpConfigWebModeleRecord
from record.utils.conf_web.web_vpz.exp_config_web import ExpConfigWebScenario
from record.utils.conf_web.web_vpz.exp_config_web import ExpConfigDictionnaire

from record.outil_web_record.models.conf_web.infos_generales import InfosGeneralesAppli

from record.outil_web_record.configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

from django.utils.translation import ugettext as _

###############################################################################
#
# Classes et methodes d'interface pour la configuration web,
# concernant la configuration web de l'application Web (cf ConfWebScenario)
# et/ou la configuration web du modele record (cf ConfWebModeleRecord).
#
###############################################################################

###############################################################################
#
# Methode : get_conf_web_modele_record (couche d'appel de ConfWebModeleRecord)
#
# Construit ConfWebModeleRecord a partir des differentes sources/origines
# possibles, cf SourceConfWebModelerecord et TypeSourceConfWebModelerecord
# (en l'etat actuel : fichiers vpz de configuration ou par defaut).
# Retourne ConfWebModeleRecord construit.
#
# rep_pkg : nom du repertoire (chemin absolu) du paquet principal
#
###############################################################################
def get_conf_web_modele_record( rep_pkg, nom_absolu_fichier_config_web_conf, nom_absolu_fichier_config_web_applis, nom_absolu_fichier_config_dicos ) :

    ###########################################################################
    #
    # Prealables
    #
    ###########################################################################

    msgtrace = "" # initialisation

    ###########################################################################
    # les noms des fichiers vpz de configuration
    ###########################################################################

    cas_valide = False # par defaut
    if nom_absolu_fichier_config_web_conf is not None :
        nom = nom_absolu_fichier_config_web_conf.encode( 'ascii','replace' )
        fichierConfigWebConf = FichierConfigSource( nom )
        if fichierConfigWebConf.isValide() :
            nomAbsoluFichierConfigWebConf = fichierConfigWebConf.getNomAbsoluFichier()
            cas_valide = True
    if not cas_valide :
        nomAbsoluFichierConfigWebConf = FichierConfigSource.VAL_vide
        msgtrace = msgtrace + " ; " + _(u"fichier de config web conf") +" "+ _(u"invalide")

    cas_valide = False # par defaut
    if nom_absolu_fichier_config_web_applis is not None :
        nom = nom_absolu_fichier_config_web_applis.encode( 'ascii','replace' )
        fichierConfigWebApplis = FichierConfigSource( nom )
        if fichierConfigWebApplis.isValide() :
            nomAbsoluFichierConfigWebApplis = fichierConfigWebApplis.getNomAbsoluFichier()
            cas_valide = True
    if not cas_valide :
        nomAbsoluFichierConfigWebApplis = FichierConfigSource.VAL_vide
        msgtrace = msgtrace + " ; " + _(u"fichier de config web applis") +" "+ _(u"invalide")

    cas_valide = False # par defaut
    if nom_absolu_fichier_config_dicos is not None :
        nom = nom_absolu_fichier_config_dicos.encode( 'ascii','replace' )
        fichierConfigDicos = FichierConfigSource( nom )
        if fichierConfigDicos.isValide() :
            nomAbsoluFichierConfigDicos = fichierConfigDicos.getNomAbsoluFichier()
            cas_valide = True
    if not cas_valide :
        nomAbsoluFichierConfigDicos = FichierConfigSource.VAL_vide
        msgtrace = msgtrace + " ; " + _(u"fichier de config dicos") +" "+ _(u"invalide")

    ###########################################################################
    # les noms des scenarios (noms des fichiers vpz sans leur extension)
    # existants du modele/paquet
    ###########################################################################
    noms_scenarios_existants = Pkg.getListeVpzSansExtension( rep_pkg )

    ###########################################################################
    # indicateur configurerParDefaut pour configurer ou non par defaut :
    # - par defaut, configuration par defaut (configurerParDefaut vaut True) ;
    # - pas de configuration par defaut (configurerParDefaut mis a False) si
    #   autre tentative de configuration web ok/acceptee 
    ###########################################################################
    configurerParDefaut = True

    ###########################################################################
    #
    # Tentative de configuration web a partir fichier vpz de configuration
    #
    ###########################################################################

    ###########################################################################
    # prealable : construction de toutes_applications_infos_generales (les
    # InfosGeneralesAppli de toutes les applications existantes de la conf)
    # dont ExpConfigWebModeleRecord a besoin
    ###########################################################################
    toutes_applications_infos_generales = {} # par defaut
    if not FichierConfigSource.isValueVide( nomAbsoluFichierConfigWebApplis ) :
        # lecture/recuperation dans fichierConfigWebApplis
        exp_config_web_applis = ExpConfigWebScenario( nomAbsoluFichierConfigWebApplis )
        toutes_applications_infos_generales = exp_config_web_applis.lireToutesApplicationsInfosGenerales()
        msgtrace = msgtrace + " ; " + "appel de exp_config_web_applis.lireToutesApplicationsInfosGenerales()"

    ###########################################################################
    # tentative
    ###########################################################################
    if not FichierConfigSource.isValueVide( nomAbsoluFichierConfigWebConf ) :

        msgtrace = msgtrace + " ********* " + "debut tentative configuration a partir fichiers vpz de configuration " + " ********* " 

        # remarque : ExpConfigWebModeleRecord
        # lit dans nomAbsoluFichierConfigWebConf, ne lit pas dans
        # nomAbsoluFichierConfigWebApplis ni nomAbsoluFichierConfigDicos
        exp_config_web_conf = ExpConfigWebModeleRecord( nomAbsoluFichierConfigWebConf, noms_scenarios_existants, toutes_applications_infos_generales, nomAbsoluFichierConfigWebApplis, nomAbsoluFichierConfigDicos )
        source_conf_web_modele_record = SourceConfWebModeleRecord()
        source_conf_web_modele_record.initPourValWebVpz( exp_config_web_conf )
        conf_web_modele_record = ConfWebModeleRecord( source_conf_web_modele_record )

        if conf_web_modele_record.isInvalide() :
        # configuration non effective (not ok)
            # configurerParDefaut reste True
            msgtrace = msgtrace + " ; " + "conf_web_modele_record issu de ExpConfigWebModeleRecord invalide"
        else : # configuration (avant controle compatibilite) effective/ok 

            # controle de compatibilite entre le modele record (scenarios...)
            # et conf_web_modele_record
            (compatibilite,messageAssComp) = conf_web_modele_record.assuranceCompatibilite( noms_scenarios_existants )

            msgtrace = msgtrace + " ; " + "message en retour assuranceCompatibilite : " + str(messageAssComp) 

            if compatibilite :
            # configuration web a partir fichier vpz de configuration web ok/acceptee
                configurerParDefaut = False
            else : # incompatibilite
            # configuration non effective/ not ok suite a controle compatibilite
                # configurerParDefaut reste True
                msgtrace = msgtrace + " ; " + "incompatibilite"

    else : # le fichier vpz de configuration n'existe pas
        # configurerParDefaut reste True
        msgtrace = msgtrace + " ; " + "fichier "+nomAbsoluFichierConfigWebConf+" invalide"

    ###########################################################################
    #
    # Configuration web par defaut
    #
    ###########################################################################
    if configurerParDefaut :
    # correspond au cas d'echec de configuration web a partir fichier vpz de
    # configuration web (quand type_source vaut VAL_web_vpz),
    # ou bien a tout cas ou type_source differe de VAL_web_vpz (vaut
    # VAL_par_defaut ou VAL_vide ou autre)
    
        msgtrace = msgtrace + " ********* " + "debut tentative configuration par defaut " + " ********* " 

        source_conf_web_modele_record = SourceConfWebModeleRecord()
        source_conf_web_modele_record.initPourValParDefaut( noms_scenarios_existants )
        conf_web_modele_record = ConfWebModeleRecord( source_conf_web_modele_record )

        # preparation message d'erreur (si besoin)
        message_erreur = "Fin/sortie traitement get_conf_web_modele_record sans configuration web"

        if conf_web_modele_record.isInvalide() :
        # configuration non effective (not ok)

            # trace erreur (pas de configuration web)
            message = "configuration par defaut non effective (not ok) ; "
            t_err.message( "get_conf_web_modele_record : " + message + message_erreur )
            msgtrace = msgtrace + " ; " + message + message_erreur

        else : # configuration (avant controle compatibilite) effective/ok 

            # controle de compatibilite entre le modele record (ses
            # scenarios...) et conf_web_modele_record par defaut
            (compatibilite,messageAssComp) = conf_web_modele_record.assuranceCompatibilite( noms_scenarios_existants )

            msgtrace = msgtrace + " ; " + "message en retour assuranceCompatibilite : " + str(messageAssComp) 

            if compatibilite :
            # configuration web par defaut ok/acceptee
                pass
            else : # incompatibilite
            # configuration non effective/ not ok suite a controle compatibilite

                # trace erreur (pas de configuration web)
                message = "incompatibilite entre la configuration web par defaut et le modele ; "
                t_err.message( "get_conf_web_modele_record : " + message + message_erreur )
                msgtrace = msgtrace + " ; " + message + message_erreur

    t_ecr.message( msgtrace )
    t_ecr.message( "conf_web_modele_record TRACE : " )
    conf_web_modele_record.traceConfWebModeleRecord()

    return conf_web_modele_record

###############################################################################
#
# Methode : get_conf_web_scenario (couche d'appel de ConfWebScenario)
#
# Construit ConfWebScenario a partir des differentes sources/origines
# possibles, cf SourceConfWebScenario et TypeSourceConfWebScenario, cf
# infos_application_id.
#
# Construit ConfWebScenario a partir de :
# - infos_application_id (InfosApplicationId), les informations
#   d'identification de l'application web de configuration.
# - et exp_scenario (Exp relatif au scenario).
# Retourne ConfWebScenario construit.
#
# Remarque : exp_scenario sert pour la configuration web par defaut et pour
# le controle de compatibilite.
#
###############################################################################
def get_conf_web_scenario( infos_application_id, exp_scenario ):

    ###########################################################################
    # Prealables
    ###########################################################################

    msgtrace = "" # initialisation

    # type_source
    type_source = infos_application_id.getTypeSourceConfWebScenario()

    # indicateur pour (en fin de traitement) poursuivre le traitement une
    # fois conf_web_scenario produite (verifications...)
    verifier_conf_web_scenario_produite = False # par defaut

    # indicateur pour (en fin de traitement) construire et retourner 
    # application de cas d'erreur en cas de mauvais deroulement traitement
    retourner_appli_erreur = False # par defaut
    # pour completer infos_generales de l'application de cas d'erreur
    message_appli_erreur = "" # initialisation

    ###########################################################################
    # cas de probleme de format de type_source (valeur impossible/inattendue)
    ###########################################################################
    if not TypeSourceConfWebScenario.isValueTypeSourceConfWebScenario( type_source ) :
        retourner_appli_erreur = True
        message_appli_erreur = "-- application dont type_source a valeur impossible --"

    ###########################################################################
    # cas type_source valant VAL_par_defaut
    ###########################################################################
    elif TypeSourceConfWebScenario.isValueParDefaut( type_source ) :
    # tentative configuration web a partir de l'application par defaut

        msgtrace = msgtrace + " ********* " + "debut tentative configuration par defaut " + " ********* " 

        source_conf_web_scenario = SourceConfWebScenario()
        source_conf_web_scenario.initPourValParDefaut( exp_scenario )
        conf_web_scenario = ConfWebScenario( source_conf_web_scenario )

        verifier_conf_web_scenario_produite = True

    ###########################################################################
    # cas type_source valant VAL_par_defaut_avec_dico 
    ###########################################################################
    elif TypeSourceConfWebScenario.isValueParDefautAvecDico( type_source ) :

        #######################################################################
        # prealable : tentative de recuperation des dicos dans fichier vpz de
        # configuration du dictionnaire
        #######################################################################

        # identification du fichier vpz de configuration du dictionnaire
        nomAbsoluConfigDicos = infos_application_id.getNomAbsoluFichierConfigDicos()

        msgtrace = msgtrace + " ; " + "tentative de recuperation des dicos dans fichier vpz de configuration du dictionnaire, (pour documenter application par defaut)"
        msgtrace = msgtrace + " ; " + "pour memo le fichier vpz de configuration du dictionnaire : " + str(nomAbsoluConfigDicos)

        if not FichierConfigSource.isValueVide( nomAbsoluConfigDicos ) :
        # tentative configuration web a partir de l'application par defaut avec dico

            msgtrace = msgtrace + " ********* " + "debut tentative configuration par defaut avec dico" + " ********* " 

            exp_config_dicos = ExpConfigDictionnaire( nomAbsoluConfigDicos )
            source_conf_web_scenario = SourceConfWebScenario()
            source_conf_web_scenario.initPourValParDefautAvecDico( exp_scenario, exp_config_dicos )
            conf_web_scenario = ConfWebScenario( source_conf_web_scenario )

            verifier_conf_web_scenario_produite = True

        else : # reaction a 'absence' du dictionnaire
        # tentative configuration web a partir de l'application par defaut (sans dico)

            msgtrace = msgtrace + " ********* " + "debut tentative configuration par defaut (sans dico)" + " ********* " 

            source_conf_web_scenario = SourceConfWebScenario()
            source_conf_web_scenario.initPourValParDefaut( exp_scenario )
            conf_web_scenario = ConfWebScenario( source_conf_web_scenario )

            verifier_conf_web_scenario_produite = True

    ###########################################################################
    # cas type_source valant VAL_web_vpz
    ###########################################################################
    elif TypeSourceConfWebScenario.isValueWebVpz( type_source ) :
    # tentative de configuration web a partir fichier vpz de configuration web

        # identification du fichier vpz de configuration du dictionnaire
        nomAbsoluConfigDicos = infos_application_id.getNomAbsoluFichierConfigDicos()

        # identification du fichier vpz de configuration web propre aux applis web
        nomAbsoluConfigWebApplis = infos_application_id.getNomAbsoluFichierConfigWebApplis()

        # application_name : nom de l'application (en tant que parametre de
        # la condition des applications)
        application_name = infos_application_id.getApplicationName()

        msgtrace = msgtrace + " ********* " + "debut tentative configuration a partir fichiers vpz de configuration " + " ********* " 

        #######################################################################
        # prealable : exp_config_dicos dont initPourApplicationName a besoin
        #######################################################################
        if not FichierConfigSource.isValueVide( nomAbsoluConfigDicos ) :
            # lecture/recuperation dans fichierConfigDicos
            exp_config_dicos = ExpConfigDictionnaire( nomAbsoluConfigDicos )
        else :
            exp_config_dicos = None

        if not FichierConfigSource.isValueVide( nomAbsoluConfigWebApplis ) :

            exp_config_web_applis = ExpConfigWebScenario( nomAbsoluConfigWebApplis )
            exp_config_web_applis.initPourApplicationName( application_name, exp_config_dicos )
            source_conf_web_scenario = SourceConfWebScenario()
            source_conf_web_scenario.initPourValWebVpz( exp_config_web_applis )
            conf_web_scenario = ConfWebScenario( source_conf_web_scenario )

            verifier_conf_web_scenario_produite = True

        else : # le fichier vpz de configuration n'existe pas

            # configuration non effectuee
            retourner_appli_erreur = True
            message_appli_erreur = " -- le fichier vpz de configuration web propre a la conf web n'existe pas -- "

            msgtrace = msgtrace + " ; " + message_appli_erreur 

    ###########################################################################
    # cas VAL_vide
    ###########################################################################
    elif TypeSourceConfWebScenario.isValueVide( type_source ) :
    # configuration web correspondant a application vide

        infos_generales = InfosGeneralesAppli()
        infos_generales.setValeurs( "", "", "" ) # tout vide
        source_conf_web_scenario = SourceConfWebScenario()
        source_conf_web_scenario.initPourValVide( infos_generales )
        conf_web_scenario = ConfWebScenario( source_conf_web_scenario )

        verifier_conf_web_scenario_produite = True

    ###########################################################################
    # cas de probleme de format de type_source (valeur impossible/inattendue)
    ###########################################################################
    else : # cas type_source de valeur impossible/inattendue
        retourner_appli_erreur = True
        message_appli_erreur = "-- application dont type_source a valeur impossible --"

    ###########################################################################
    # poursuite traitement dans cas ou conf_web_scenario a ete produite
    ###########################################################################
    if verifier_conf_web_scenario_produite : # poursuite traitement

        # preparation message d'erreur (si besoin)
        message_erreur = "Fin/sortie traitement get_conf_web_scenario sans configuration web"

        if conf_web_scenario.isInvalide() :
        # configuration non effective (not ok)

            retourner_appli_erreur = True
            message_appli_erreur = " -- configuration non effective (not ok) -- "

            msgtrace = msgtrace + " ; " + message_appli_erreur + message_erreur 

            # trace erreur (pas de configuration web)
            t_err.message( "get_conf_web_scenario : " + message_appli_erreur + message_erreur )

        else : # configuration (avant controle compatibilite) effective/ok 

            # controle de compatibilite entre exp_scenario et conf_web_scenario
            (compatibilite,messageAssComp) = conf_web_scenario.assuranceCompatibilite( exp_scenario )

            msgtrace = msgtrace + " ; " + "message en retour assuranceCompatibilite : " + str(messageAssComp)

            if compatibilite : # configuration web ok/acceptee
                msgtrace = msgtrace + " ; " + "configuration ok/acceptee"

            else : # incompatibilite
            # configuration non effective/ not ok suite a controle compatibilite

                retourner_appli_erreur = True
                message_appli_erreur = " -- incompatibilite entre la configuration web et le scenario -- "

                msgtrace = msgtrace + " ; " + message_appli_erreur + message_erreur 

                # trace erreur (pas de configuration web)
                t_err.message( "get_conf_web_scenario : " + message_appli_erreur + message_erreur )
                t_err.msg( "pour memo message en retour assuranceCompatibilite : " + str(messageAssComp) )

    ###########################################################################
    # cas d'erreur
    ###########################################################################
    if retourner_appli_erreur :
    # configuration web correspondant a application de cas d'erreur

        infos_generales = InfosGeneralesAppli()
        infos_generales.setValeursCasDerreur() # par defaut
        description = infos_generales.getDescription() + " " + message_appli_erreur
        infos_generales.setDescription( description ) # complement

        source_conf_web_scenario = SourceConfWebScenario()
        source_conf_web_scenario.initPourValVide( infos_generales )
        conf_web_scenario = ConfWebScenario( source_conf_web_scenario )

    t_ecr.message( "get_conf_web_scenario : " + msgtrace )
    t_ecr.message( "conf_web_scenario TRACE : " )
    conf_web_scenario.traceConfWebScenario()

    return conf_web_scenario 

