#-*- coding:utf-8 -*-

###############################################################################
# File transcodage.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

###############################################################################
#
#
# Classes et methodes dediees a la configuration Web :
# des elements de transcodage d'informations
#
#
# Remarque : classes sur lesquelles s'appuient egalement exp_conf_web et
# definition pour les aspects communs (formats calques)
#
###############################################################################

###############################################################################
#
# Modele : TranscodageConfWeb
#
# Elements de transcodage d'informations de configuration relatives/communes
# a ConfWebScenario et ConfWebModeleRecord
#
# (objet dedie a ConfWebScenario et ConfWebModeleRecord)
#
###############################################################################
class TranscodageConfWeb( object ):

    CONST_valeur_vide = None

    @classmethod
    def valeurVide( cls, v ) :
        return ( v == cls.CONST_valeur_vide )

###############################################################################
#
# Modele : TranscodageConfWebScenario
#
# Elements de transcodage d'informations de configuration relatives/propres a
# ConfWebScenario
#
# (objet dedie a ConfWebScenario)
#
# Contient des constantes/valeurs 'normalisees' prises par des informations
# de ConfWebScenario, des methodes de transcodage
#
###############################################################################
class TranscodageConfWebScenario( object ):

    ###########################################################################
    # Constantes : des valeurs attribuees par defaut
    ###########################################################################
    # cf CONF_conf_web.TXTDEFAUT_...

    ###########################################################################
    # Constantes
    # valeurs 'normalisees' prises par des informations de ConfWebScenario
    ###########################################################################

    CONST_web_dimension_variable = 'variable'
    CONST_web_dimension_fixe     = 'fixe'

    CONST_web_mode_hidden        = 'hidden'
    CONST_web_mode_read_write    = 'read_write'
    CONST_web_mode_read_only     = 'read_only'

    CONST_vpz_type_exp_name      = 'type_exp_name'
    CONST_vpz_type_exp_duration  = 'type_exp_duration'
    CONST_vpz_type_exp_begin     = 'type_exp_begin'
    CONST_vpz_type_simu_seed     = 'type_simu_seed'
    CONST_vpz_type_plan_seed     = 'type_plan_seed'
    CONST_vpz_type_plan_number   = 'type_plan_number'
    CONST_vpz_type_condition_port = 'type_condition_port' 

    ###########################################################################
    # Methodes de transcodage qui retournent
    # valeur normalisee prise par certaines informations de ConfWebScenario
    ###########################################################################

    # retourne la valeur (normalisee) de web_dimension
    @classmethod
    def getStdWebDimension( cls, web_dimension ) :
        v = TranscodageConfWeb.CONST_valeur_vide # par defaut
        if web_dimension == 'variable' :
            v = cls.CONST_web_dimension_variable
        elif web_dimension == 'fixe' :
            v = cls.CONST_web_dimension_fixe
        return v

    # retourne la valeur (normalisee) de web_mode
    @classmethod
    def getStdWebMode( cls, web_mode ) :
        v = TranscodageConfWeb.CONST_valeur_vide # par defaut
        if web_mode == 'hidden' :
            v = cls.CONST_web_mode_hidden
        for val in ('read-write', 'read_write', 'readwrite', 'rw' ) : 
            if web_mode == val :
                v = cls.CONST_web_mode_read_write
        for val in ( 'read-only', 'read_only', 'readonly' ) :
            if web_mode == val :
                v = cls.CONST_web_mode_read_only
        return v

    # retourne la valeur (normalisee) de vpz_type
    @classmethod
    def getStdVpzType( cls, vpz_type ) :
        v = TranscodageConfWeb.CONST_valeur_vide # par defaut
        for val in ( 'type_exp_name', 'type-exp-name', 'typeexpname' ) :
            if vpz_type == val :
                v = cls.CONST_vpz_type_exp_name
        for val in ( 'type_exp_duration', 'type-exp-duration', 'typeexpduration' ) : 
            if vpz_type == val :
                v = cls.CONST_vpz_type_exp_duration
        for val in ( 'type_exp_begin', 'type-exp-begin', 'typeexpbegin' ) : 
            if vpz_type == val :
                v = cls.CONST_vpz_type_exp_begin
        for val in ( 'type_simu_seed', 'type-simu-seed', 'typesimuseed' ) : 
            if vpz_type == val :
                v = cls.CONST_vpz_type_simu_seed
        for val in ( 'type_plan_seed', 'type-plan-seed', 'typeplanseed' ) : 
            if vpz_type == val :
                v = cls.CONST_vpz_type_plan_seed
        for val in ( 'type_plan_number', 'type-plan-number', 'typeplannumber' ) : 
            if vpz_type == val :
                v = cls.CONST_vpz_type_plan_number
        for val in ( 'type_condition_port', 'type-condition-port', 'typeconditionport', 'type_cond_port', 'type-cond-port', 'typecondport' ) : 
            if vpz_type == val :
                v = cls.CONST_vpz_type_condition_port
        return v

    ###########################################################################
    # Methodes qui retournent la signification d'une information
    # La comparaison est faite sur sa valeur normalisee
    ###########################################################################

    # Valeurs de web_dimension 

    # determine si web_dimension est "variable" (ou equivalent)
    @classmethod
    def isVariableDimension(cls, web_dimension) :
        r = False # par defaut
        if cls.getStdWebDimension(web_dimension) == cls.CONST_web_dimension_variable :
            r = True
        return r

    # determine si web_dimension est "fixe" (ou equivalent)
    @classmethod
    def isFixeDimension(cls, web_dimension) :
        r = False # par defaut
        if cls.getStdWebDimension(web_dimension) == cls.CONST_web_dimension_fixe :
            r = True
        return r

    # Valeurs de web_mode 

    # determine si web_mode est "hidden" (ou equivalent)
    @classmethod
    def isHiddenMode(cls, web_mode) :
        r = False # par defaut
        if cls.getStdWebMode(web_mode) == cls.CONST_web_mode_hidden :
            r = True
        return r

    # determine si web_mode est "read_write" (ou equivalent)
    @classmethod
    def isReadWriteMode(cls, web_mode) :
        r = False # par defaut
        if cls.getStdWebMode(web_mode) == cls.CONST_web_mode_read_write :
            r = True
        return r

    # determine si web_mode est "read_only" (ou equivalent)
    @classmethod
    def isReadOnlyMode(cls, web_mode) :
        r = False # par defaut
        if cls.getStdWebMode(web_mode) == cls.CONST_web_mode_read_only :
            r = True
        return r

    # Valeurs de vpz_type 

    # determine si vpz_type est "type_exp_name" (ou equivalent)
    @classmethod
    def isTypeExpNameVpzType(cls, vpz_type) :
        r = False # par defaut
        if cls.getStdVpzType(vpz_type) == cls.CONST_vpz_type_exp_name :
            r = True
        return r

    # determine si vpz_type est "type_exp_duration" (ou equivalent)
    @classmethod
    def isTypeExpDurationVpzType(cls, vpz_type) :
        r = False # par defaut
        if cls.getStdVpzType(vpz_type) == cls.CONST_vpz_type_exp_duration :
            r = True
        return r

    # determine si vpz_type est "type_exp_begin" (ou equivalent)
    @classmethod
    def isTypeExpBeginVpzType(cls, vpz_type) :
        r = False # par defaut
        if cls.getStdVpzType(vpz_type) == cls.CONST_vpz_type_exp_begin :
            r = True
        return r

    # determine si vpz_type est "type_simu_seed" (ou equivalent)
    @classmethod
    def isTypeSimuSeedVpzType(cls, vpz_type) :
        r = False # par defaut
        if cls.getStdVpzType(vpz_type) == cls.CONST_vpz_type_simu_seed :
            r = True
        return r

    # determine si vpz_type est "type_plan_seed" (ou equivalent)
    @classmethod
    def isTypePlanSeedVpzType(cls, vpz_type) :
        r = False # par defaut
        if cls.getStdVpzType(vpz_type) == cls.CONST_vpz_type_plan_seed :
            r = True
        return r

    # determine si vpz_type est "type_plan_number" (ou equivalent)
    @classmethod
    def isTypePlanNumberVpzType(cls, vpz_type) :
        r = False # par defaut
        if cls.getStdVpzType(vpz_type) == cls.CONST_vpz_type_plan_number :
            r = True
        return r

    # determine si vpz_type est "type_condition_port" (ou equivalent)
    @classmethod
    def isTypeConditionPortVpzType(cls, vpz_type) :
        r = False # par defaut
        if cls.getStdVpzType(vpz_type) == cls.CONST_vpz_type_condition_port :
            r = True
        return r

