#-*- coding:utf-8 -*-

###############################################################################
# File infos_generales.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################


from record.outil_web_record.configs.conf_conf_web import CONF_conf_web
from record.outil_web_record.models.conf_web.transcodage import TranscodageConfWeb as tcw

from record.outil_web_record.configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

###############################################################################
#
#
# Classes et methodes dediees a la configuration web :
# des classes de composants de la ConfWebScenario ou la ConfWebModeleRecord
#
#
###############################################################################


###############################################################################
#
# Informations generales d'une entite web (application, page web, scenario...)
#
###############################################################################
class InfosGenerales(object) :

    def __init__( self ) :
        self.titre = None
        self.description = None
        self.help = None

    def set( self, titre, description, help ) :
        self.setTitre( titre )
        self.setDescription( description )
        self.setHelp( help )

    # retourne la valeur (normalisee) de titre
    def getTitre( self ) :
        titre = tcw.CONST_valeur_vide # par defaut
        v = self.titre
        if type(v) == str :
            titre = v
        return titre

    def setTitre( self, titre ) :
        self.titre = titre

    # retourne la valeur (normalisee) de description
    def getDescription( self ) :
        description = tcw.CONST_valeur_vide # par defaut
        v = self.description
        if type(v) == str :
            description = v
        return description

    def setDescription( self, description ) :
        self.description = description

    # retourne la valeur (normalisee) de help
    def getHelp( self ) :
        help = tcw.CONST_valeur_vide # par defaut
        v = self.help
        if type(v) == str :
            help = v
        return help

    def setHelp( self, help ) :
        self.help = help

    # Valeurs titre, description, help
    def setValeurs( self, titre, description, help ):

        if type(titre) != str :
            titre = tcw.CONST_valeur_vide
        if type(description) != str :
            description = tcw.CONST_valeur_vide
        if type(help) != str :
            help = tcw.CONST_valeur_vide
        self.set( titre, description, help )

###############################################################################
#
# Informations generales d'une application
#
###############################################################################
class InfosGeneralesAppli(InfosGenerales) :

    # Valeurs par defaut
    def setValeursDefaut( self ): # sert ?
        titre = CONF_conf_web.TXTDEFAUT_titre_appli
        description = CONF_conf_web.TXTDEFAUT_description_appli
        help = CONF_conf_web.TXTDEFAUT_help_appli
        self.set( titre, description, help )

    # Valeurs par defaut pour chaque champ considere individuellement
    def setValeursDefautUneParUne( self ): 

        titre_appli = self.getTitre()
        if tcw.valeurVide( titre_appli ) :
            titre_appli = CONF_conf_web.TXTDEFAUT_titre_appli
            self.setTitre( titre_appli ) 

        description_appli = self.getDescription()
        if tcw.valeurVide( description_appli ) :
            description_appli = CONF_conf_web.TXTDEFAUT_description_appli
            self.setDescription( description_appli )

        help_appli = self.getHelp()
        if tcw.valeurVide( help_appli ) :
            help_appli = CONF_conf_web.TXTDEFAUT_help_appli
            self.setHelp( help_appli )

    # Valeurs standard
    def setValeursStandard( self ):
        titre = CONF_conf_web.TXTSTANDARD_titre_appli
        description = CONF_conf_web.TXTSTANDARD_description_appli
        help = CONF_conf_web.TXTSTANDARD_help_appli
        self.set( titre, description, help )

    # Valeurs standard avec dico issue d'une configuration web
    def setValeursStandardAvecDico( self ):
        titre = CONF_conf_web.TXTSTANDARDAVECDICO_titre_appli
        description = CONF_conf_web.TXTSTANDARDAVECDICO_description_appli
        help = CONF_conf_web.TXTSTANDARDAVECDICO_help_appli
        self.set( titre, description, help )

    # Valeurs de cas d'erreur
    def setValeursCasDerreur( self ):
        titre = CONF_conf_web.TXTERREUR_titre_appli
        description = CONF_conf_web.TXTERREUR_description_appli
        help = CONF_conf_web.TXTERREUR_help_appli
        self.set( titre, description, help )

###############################################################################
#
# Informations generales d'une page web (de definition ou de resultat)
#
###############################################################################
class InfosGeneralesPageWeb(InfosGenerales) :

    #def __init__( self ):
        #InfosGenerales.__init__( self )

    # Valeurs par defaut
    def setValeursDefaut( self ): # sert ?
        titre = CONF_conf_web.TXTDEFAUT_titre_page_web
        description = CONF_conf_web.TXTDEFAUT_description_page_web
        help = CONF_conf_web.TXTDEFAUT_help_page_web
        self.set( titre, description, help )

    # Valeurs par defaut pour chaque champ considere individuellement
    def setValeursDefautUneParUne( self ): 

        titre = self.getTitre()
        if tcw.valeurVide( titre ) :
            titre = CONF_conf_web.TXTDEFAUT_titre_page_web
            self.setTitre(titre)

        description = self.getDescription()
        if tcw.valeurVide( description ) :
            description = CONF_conf_web.TXTDEFAUT_description_page_web
            self.setDescription(description)

        help = self.getHelp()
        if tcw.valeurVide( help ) :
            help = CONF_conf_web.TXTDEFAUT_help_page_web
            self.setHelp(help)

    # Valeurs standard page experience
    def setValeursStandardPageExperience( self ):
        titre = CONF_conf_web.TXTSTANDARD_titre_page_experience
        description = CONF_conf_web.TXTSTANDARD_description_page_experience
        help = CONF_conf_web.TXTSTANDARD_help_page_experience
        self.set( titre, description, help )

    # Valeurs standard page plan d'experience
    def setValeursStandardPagePlan( self ):
        titre = CONF_conf_web.TXTSTANDARD_titre_page_plan
        description = CONF_conf_web.TXTSTANDARD_description_page_plan
        help = CONF_conf_web.TXTSTANDARD_help_page_plan
        self.set( titre, description, help )

    # Valeurs standard page condition
    def setValeursStandardPageCondition( self ):
        # attention titre contient en fait le prefixe du titre qui sera donne a la page condition
        titre = CONF_conf_web.TXTSTANDARD_prefixe_titre_page_condition
        description = CONF_conf_web.TXTSTANDARD_description_page_condition
        help = CONF_conf_web.TXTSTANDARD_help_page_condition
        self.set( titre, description, help )

    # Valeurs standard page web res (view/vue)
    def setValeursStandardPageWebRes( self ):
        # attention titre contient en fait le prefixe du titre qui sera donne a la page web res
        titre = CONF_conf_web.TXTSTANDARD_prefixe_titre_page_view
        description = "Page correspondant à la vue (du scénario) "
        help = CONF_conf_web.TXTSTANDARD_help_page_view 
        self.set( titre, description, help )

###############################################################################
#
# Informations generales d'un scenario
#
###############################################################################
class InfosGeneralesScenario(InfosGenerales) :

    # Valeurs par defaut (nom_scenario sert dans le titre)
    def setValeursDefaut( self, nom_scenario ): # sert ?

        # attention : titre est la concatenation de
        # TXTDEFAUT_prefixe_titre_scenario et nom_scenario
        titre = CONF_conf_web.TXTDEFAUT_prefixe_titre_scenario
        titre = titre+' '+nom_scenario

        description = CONF_conf_web.TXTDEFAUT_description_scenario

        help = CONF_conf_web.TXTDEFAUT_help_scenario

        self.set( titre, description, help )

    # Valeurs par defaut pour chaque champ considere individuellement
    # (nom_scenario sert dans le titre)
    def setValeursDefautUneParUne( self, nom_scenario ): 

        titre_scenario = self.getTitre()
        if tcw.valeurVide( titre_scenario ) :
            # attention : titre est la concatenation de
            # TXTDEFAUT_prefixe_titre_scenario et nom_scenario
            titre_scenario = CONF_conf_web.TXTDEFAUT_prefixe_titre_scenario
            titre_scenario = titre_scenario+' '+nom_scenario
            self.setTitre( titre_scenario ) 

        description_scenario = self.getDescription()
        if tcw.valeurVide( description_scenario ) :
            description_scenario = CONF_conf_web.TXTDEFAUT_description_scenario
            self.setDescription( description_scenario )

        help_scenario = self.getHelp()
        if tcw.valeurVide( help_scenario ) :
            help_scenario = CONF_conf_web.TXTDEFAUT_help_scenario
            self.setHelp( help_scenario )

    # Valeurs standard (nom_scenario sert dans le titre)
    def setValeursStandard( self, nom_scenario ):

        # attention : titre est la concatenation de
        # TXTSTANDARD_prefixe_titre_scenario et nom_scenario
        titre = CONF_conf_web.TXTSTANDARD_prefixe_titre_scenario
        titre = titre+' '+nom_scenario

        description = CONF_conf_web.TXTSTANDARD_description_scenario

        help = CONF_conf_web.TXTSTANDARD_help_scenario

        self.set( titre, description, help )

