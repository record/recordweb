#-*- coding:utf-8 -*-

###############################################################################
# File modele_record.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Authors :
# Nathalie Rousse, INRA RECORD team member,
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

import os

from record.outil_web_record.models.espace_exploitation.espace_exploitation import EspaceExploitation

from record.outil_web_record.models.scenariovpz.scenariovpz import ScenarioVpz

from record.outil_web_record.models.conf_web.conf_web_gestion import get_conf_web_modele_record

from configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

from django.utils.translation import ugettext as _

###############################################################################
#
# Classes et methodes dediees a l'information du modele record tel
# qu'utilise/exploite dans l'outil_web_record (montres a l'usr...).
# Couche de filtres et complements (notamment complements relatifs a la
# configuration web) du modele record entre (de) la bd des
# modeles record et (vers) l'outil_web_record.
#
###############################################################################

###############################################################################
# Modele : ModeleRecordOn
#
# L'information d'un modele record tel qu'utilise/exploite dans
# l'outil_web_record. ModeleRecordOn sert a presenter le modele record dans
# l'outil_web_record (pour en choisir un, puis un de ses scenarios, puis une
# des configurations d'application web du scenario choisi).
#
# Les informations du ModeleRecordOn reprennent/recuperent un certain nombre
# d'informations du ModeleRecord d'origine (ModeleRecord dans la bd des
# modeles record) + en contiennent d'autres (la liste de ses scenarios
# noms_scenarios (fichiers vpz) tenant compte de la configuration web, et pour
# chaque scenario ses configurations d'application web associees/possibles).
#
# exploitabilite : indique si le ModeleRecordOn est exploitable ou non dans
# l'outil_web_record.
#
###############################################################################
class ModeleRecordOn(object):

    ###########################################################################
    # Construction a partir d'un ModeleRecord (ModeleRecord dans la bd des
    # modeles record)
    #
    # Determine si possible id.
    #
    # Effectue toute verifications necessaires, a partir desquelles est
    # determinee exploitabilite : verifie que tous les champs du
    # ModeleRecord sont renseignes dans bd (sauf facultatifs), que le
    # paquet nom_pkg existe ainsi que certains repertoires et fichiers...
    #
    # En cas d'exploitabilite, determine les autres champs d'information.
    #
    ###########################################################################
    def __init__( self, modele_record_bd ) :

        expl = Exploitabilite()
        self.id = None # par defaut

        if modele_record_bd is None : # pas de modele_record_bd 
            expl.rendreInexploitable( _(u"modèle inconnu") ) 

        else : # modele_record_bd existe

            if modele_record_bd.id is None :
                expl.rendreInexploitable( _(u"information id manquante") )
            else : # id existe
                self.id = modele_record_bd.id 

            ###################################################################
            # Premier niveau de verification :
            # verifie que tous les autres champs de modele_record_bd sont
            # renseignes dans bd (sauf facultatifs : les noms des fichiers
            # vpz de configuration nom_fichier_config_...)
            ###################################################################

            if modele_record_bd.nom is None or modele_record_bd.nom=="" : 
                expl.rendreInexploitable( _(u"information nom manquante") )
            if modele_record_bd.description is None or modele_record_bd.description=="" :
                expl.rendreInexploitable( _(u"information description manquante") )
            if modele_record_bd.nom_repertoire_lot_pkgs is None or modele_record_bd.nom_repertoire_lot_pkgs=="" :
                expl.rendreInexploitable( _(u"information nom_repertoire_lot_pkgs manquante") )
            if modele_record_bd.nom_pkg is None or modele_record_bd.nom_pkg=="" :
                expl.rendreInexploitable( _(u"information nom_pkg manquante") )
            if modele_record_bd.nom_repertoire_data is None :
            # la valeur "" n'est pas rejetee pour nom_repertoire_data
            # (si jamais elle etait definie comme valeur par defaut)
                expl.rendreInexploitable( _(u"information nom_repertoire_data manquante") )
            # mot_de_passe : rien
            # nom_fichier_config_web_conf non verifie (non obligatoire)
            # nom_fichier_config_web_applis non verifie (non obligatoire)
            # nom_fichier_config_dicos non verifie (non obligatoire)
            if modele_record_bd.responsable is None :
                expl.rendreInexploitable( _(u"information responsable manquante") )
            else : # verifie que responsable lui-meme estExploitable
                individu = IndividuOn( modele_record_bd.responsable )
                if not individu.estExploitable() :
                    msg = individu.exploitabilite.getMsgEtat()
                    expl.rendreInexploitable( msg )
            if modele_record_bd.responsable_scientifique is None :
                expl.rendreInexploitable( _(u"information responsable_scientifique manquante") )
            else : # verifie que responsable_scientifique lui-meme estExploitable
                individu = IndividuOn( modele_record_bd.responsable_scientifique )
                if not individu.estExploitable() :
                    msg = individu.exploitabilite.getMsgEtat()
                    expl.rendreInexploitable( msg )
            if modele_record_bd.responsable_informatique is None :
                expl.rendreInexploitable( _(u"information responsable_informatique manquante") )
            else : # verifie que responsable_informatique lui-meme estExploitable
                individu = IndividuOn( modele_record_bd.responsable_informatique )
                if not individu.estExploitable() :
                    msg = individu.exploitabilite.getMsgEtat()
                    expl.rendreInexploitable( msg )
            if modele_record_bd.cr_reception is None or modele_record_bd.cr_reception=="" :
                expl.rendreInexploitable( _(u"information cr_reception manquante") )
            if modele_record_bd.version_vle is None or modele_record_bd.version_vle=="" :
                expl.rendreInexploitable( _(u"information version_vle manquante") )

        self.exploitabilite = expl 

        #######################################################################
        # A ce stade, un premier niveau de verification a ete effectue.
        # Toute autre verification necessaire est maintenant effectuee, 
        # au fur et a mesure de preparation des champs d'information
        #######################################################################
        if self.estExploitable() :

            nom_absolu_repertoire_lot_pkgs = modele_record_bd.getNomAbsoluRepertoireLotPkgs()
            nom_absolu_repertoire_data = modele_record_bd.getNomAbsoluRepertoireData()
            nom_absolu_fichier_config_web_conf = modele_record_bd.getNomAbsoluFichierConfigWebConf()
            nom_absolu_fichier_config_web_applis = modele_record_bd.getNomAbsoluFichierConfigWebApplis()
            nom_absolu_fichier_config_dicos = modele_record_bd.getNomAbsoluFichierConfigDicos()

            # verifie existence repertoire nom_absolu_repertoire_lot_pkgs
            if not EspaceExploitation.repertoireExisteDeja( nom_absolu_repertoire_lot_pkgs ) :
                txt = _(u"le répertoire de lot du modèle") + " " + nom_absolu_repertoire_lot_pkgs +" "+ _(u"n'existe pas")
                expl.rendreInexploitable( txt ) 

            # verifie que le paquet/repertoire nom_pkg existe sous repertoire nom_repertoire_lot_pkgs
            nom_absolu_repertoire_pkg = os.path.join( nom_absolu_repertoire_lot_pkgs, modele_record_bd.nom_pkg ) 
            if not EspaceExploitation.repertoireExisteDeja( nom_absolu_repertoire_pkg ) :
                txt = _(u"le paquet") +" "+ modele_record_bd.nom_pkg + _(u"n'existe pas sous") +" "+ nom_absolu_repertoire_lot_pkgs 
                expl.rendreInexploitable( txt ) 

            # verifie existence repertoire nom_absolu_repertoire_data
            if modele_record_bd.aNomRepertoireDataParDefaut() :
                nom_absolu_repertoire_data = modele_record_bd.getNomAbsoluRepertoireDataParDefaut()
            else :
                nom_absolu_repertoire_data = modele_record_bd.getNomAbsoluRepertoireData()

            if not EspaceExploitation.repertoireExisteDeja( nom_absolu_repertoire_data ) :
                txt = _(u"le répertoire des données de simulation") +" "+ nom_absolu_repertoire_data + " " + _(u"n'existe pas")
                expl.rendreInexploitable( txt ) 

        #######################################################################
        # a ce stade, toutes les verifications necessaires ont ete
        # effectuees. affectation des champs d'information
        # attention : chemins absolus de repertoires/fichiers dans
        # ModeleRecordOn (alors que chemins relatifs dans ModeleRecord en bd)
        #######################################################################
        if self.estExploitable() :

            self.nom = modele_record_bd.nom 
            self.description = modele_record_bd.description 
            self.nom_repertoire_lot_pkgs = nom_absolu_repertoire_lot_pkgs
            self.nom_pkg = modele_record_bd.nom_pkg 
            self.nom_repertoire_data = nom_absolu_repertoire_data
            self.nom_fichier_config_web_conf = nom_absolu_fichier_config_web_conf
            self.nom_fichier_config_web_applis = nom_absolu_fichier_config_web_applis
            self.nom_fichier_config_dicos = nom_absolu_fichier_config_dicos
            self.responsable = IndividuOn( modele_record_bd.responsable )
            self.responsable_scientifique = IndividuOn( modele_record_bd.responsable_scientifique )
            self.responsable_informatique = IndividuOn( modele_record_bd.responsable_informatique )
            self.cr_reception = modele_record_bd.cr_reception 
            self.version_vle = modele_record_bd.version_vle 

            # liste des ScenarioOn, scenarios (fichiers vpz) disponibles du
            # modele/paquet (non pas ceux presents mais ceux issus/filtres
            # de/par la configuration web)

            # lecture de la configuration web
            rep_absolu_pkg = os.path.join( self.nom_repertoire_lot_pkgs, self.nom_pkg )
            conf_web_modele_record = get_conf_web_modele_record( rep_absolu_pkg, self.nom_fichier_config_web_conf, self.nom_fichier_config_web_applis, self.nom_fichier_config_dicos )

            scenarios_on = []
            for nom_scenario_on in conf_web_modele_record.noms_scenarios :
                infos_generales_scenario = conf_web_modele_record.infos_generales_scenarios[ nom_scenario_on ]
                applications_associees = conf_web_modele_record.applications_web_associees[ nom_scenario_on ]
                scenario_on = ScenarioOn( nom_scenario_on, infos_generales_scenario, applications_associees )

                scenarios_on.append( scenario_on )

            self.scenarios_on = scenarios_on
            self.conf_web_modele_record = conf_web_modele_record


    ###########################################################################
    # Construit et retourne le ScenarioVpz du ModeleRecordOn correspondant
    # a son scenario caracterise par l'identifiant
    # index_scenario_on (dans liste scenarios_on du ModeleRecordOn)
    ###########################################################################
    def getScenarioVpz( self, index_scenario_on ) :

        nom_paquet = self.nom
        description_modele_record = self.description
        nom_pkg = self.nom_pkg
        version_vle = self.version_vle
        nom_repertoire_lot_pkgs = self.nom_repertoire_lot_pkgs
        nom_repertoire_data = self.nom_repertoire_data

        scenario_on = self.scenarios_on[index_scenario_on]
        nom_vpz = scenario_on.nom_scenario + ".vpz"

        # infos_generales_initiales_scenario
        nom_scenario = scenario_on.nom_scenario
        conf_web_modele_record = self.conf_web_modele_record
        infos_generales_initiales_scenario = conf_web_modele_record.infos_generales_scenarios[nom_scenario]

        scenario_vpz = ScenarioVpz( nom_vpz, nom_paquet, description_modele_record, nom_pkg, version_vle, nom_repertoire_lot_pkgs, nom_repertoire_data, infos_generales_initiales_scenario )

        return scenario_vpz

    ###########################################################################
    # Retourne ApplicationWeb correspondant a une application associee a un
    # scenario ScenarioOn du ModeleRecordOn, caracterises par
    # leurs identifiants :
    # index_scenario_on (dans liste scenarios_on du ModeleRecordOn) et
    # index_application_associee (dans liste infos_applications_associees du
    # ScenarioOn)
    #
    # Attention : On utilise/s'appuie sur le fait que les applications se
    # trouvent exactement dans le meme ordre dans infos_applications_associees
    # de ScenarioOn et dans applications_web_associees[nom_scenario] de
    # ConfWebModeleRecord (ou sera recuperee ApplicationWeb)
    #
    ###########################################################################
    def getApplicationWeb( self, index_scenario_on, index_application_associee ) :

        scenario_on = self.scenarios_on[index_scenario_on]
        nom_scenario = scenario_on.nom_scenario

        conf_web_modele_record = self.conf_web_modele_record

        application_web = conf_web_modele_record.getApplicationWeb( nom_scenario, index_application_associee )

        return application_web

    ###########################################################################
    # Retourne l'etat d'exploitabilite du modele, selon les verifications
    # effectuees au moment de l'appel de la methode (au cours de son processus
    # de construction/verification, un modele peut commencer par etre
    # considere exploitable et par la suite devenir inexploitable).
    ###########################################################################
    def estExploitable( self ) :
        return self.exploitabilite.estExploitable()

###############################################################################
# Modele : ScenarioOn
#
# L'information d'un scenario tel qu'utilise/exploite dans
# l'outil_web_record : un scenario qui est propose pour exploitation (associe
# a un ModeleRecordOn).
#
# ScenarioOn sert a presenter le scenario et ses configurations d'application
# web (pour etre eventuellement choisi avec l'une d'entre elles).
#
# Les informations du ScenarioOn reprennent/recuperent dans nom_scenario et
# applications_associees (provenant de ConfWebModeleRecord) ce qui est utile
# a la presentation :
# - nom_scenario : le nom du scenario
# - infos_generales : les infos generales du scenario (description, titre...)
# - infos_applications_associees : la liste des infos_generales de ses
#   applications_associees.
#
# Attention : Les applications doivent etre rangees dans la liste resultat
# infos_applications_associees exactement dans l'ordre ou elles se trouvent
# dans la liste source applications_associees (cf traitement
# getApplicationAssociee).
#
###############################################################################
class ScenarioOn(object):

    ###########################################################################
    # Construction a partir de son nom_scenario, de ses infos generales 
    # infos_generales et de la liste de ses applications_associees
    ###########################################################################
    def __init__( self, nom_scenario, infos_generales, applications_associees ) :

        self.nom_scenario = nom_scenario 

        self.infos_generales = infos_generales 

        infos_applications_associees = []
        for application_associee in applications_associees :
            infos_application_associee = application_associee.getInformationsGenerales()
            infos_applications_associees.append( infos_application_associee )
        self.infos_applications_associees = infos_applications_associees 

###############################################################################
# Modele : IndividuOn
#
# L'information d'un individu tel qu'utilise/exploite dans l'outil_web_record.
# Reprend/recupere un certain nombre d'informations de Individu d'origine
# (IndividuOn dans la bd des modeles record) et en contient d'autres
# (exploitabilite...).
#
# IndividuOn sert a presenter dans l'outil_web_record un individu lie/relatif
# a un modele record.
#
# exploitabilite : indique si IndividuOn est exploitable ou non dans
# l'outil_web_record.
#
###############################################################################
class IndividuOn(object):

    ###########################################################################
    # Construction a partir d'un Individu (Individu de la bd des modeles
    # record).
    #
    # Determine si possible id.
    #
    # Effectue toutes verifications necessaires, a partir desquelles est
    # determinee exploitabilite : verifie que tous les champs de
    # Individu sont renseignes dans bd.
    #
    # En cas d'exploitabilite, determine les autres champs d'information.
    #
    ###########################################################################
    def __init__( self, individu_bd ) :

        expl = Exploitabilite()
        id = None # par defaut

        if individu_bd is None : # pas de individu_bd 
            expl.rendreInexploitable( _(u"individu inconnu") ) 

        else : # individu_bd existe

            if individu_bd.id is None :
                expl.rendreInexploitable( _(u"information id manquante") )
            else :
                id = individu_bd.id 

            # verifie que tous les autres champs de individu_bd sont
            # renseignes (tous sont exiges/obligatoires)
            if individu_bd.first_name is None or individu_bd.first_name=="" :
                expl.rendreInexploitable( _(u"information first_name manquante") )
            if individu_bd.last_name is None or individu_bd.last_name=="" :
                expl.rendreInexploitable( _(u"information last_name manquante") )
            if individu_bd.email is None or individu_bd.email=="" :
                expl.rendreInexploitable( _(u"information email manquante") )

        self.exploitabilite = expl
        self.id = id

        #######################################################################
        # a ce stade, toutes les verifications necessaires ont ete
        # effectuees. affectation des champs d'information
        #######################################################################
        if self.estExploitable() :
            self.first_name = individu_bd.first_name 
            self.last_name = individu_bd.last_name 
            self.email = individu_bd.email 

    ###########################################################################
    # Retourne l'etat d'exploitabilite de IndividuOn
    ###########################################################################
    def estExploitable( self ) :
        return self.exploitabilite.estExploitable()


###############################################################################
# Exploitabilite decrit le caractere exploitable ou non dans
# l'outil_web_record d'une information donnee.
#
# etat_exploitable : booleen qui indique si l'information consideree (ie a
# laquelle Exploitabilite est associe) est exploitable ou non dans
# l'outil_web_record.
#
# msg_etat : informations sur les raisons/explications de l'etat de
# l'information consideree.
#
###############################################################################
class Exploitabilite(object) :

    ###########################################################################
    # Construction par defaut
    ###########################################################################
    def __init__( self ) :
        self.etat_exploitable = True # par defaut
        self.msg_etat = "" # par defaut

    ###########################################################################
    # met etat_exploitable a False et complete msg_etat avec txt
    ###########################################################################
    def rendreInexploitable( self, txt ) :
        self.etat_exploitable = False
        self.msg_etat = self.msg_etat + " -- " + txt

    ###########################################################################
    # Retourne true si exploitabilite (ie etat_exploitable True)
    ###########################################################################
    def estExploitable( self ) :
        return self.etat_exploitable

    ###########################################################################
    # Retourne msg_etat
    ###########################################################################
    def getMsgEtat( self ) :
        return self.msg_etat
