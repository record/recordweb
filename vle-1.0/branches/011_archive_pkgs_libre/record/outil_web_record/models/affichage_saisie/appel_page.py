#-*- coding:utf-8 -*-

###############################################################################
# File appel_page.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
#
# Classes et methodes dediees a l'appel de pages
#
###############################################################################

from django.core.context_processors import csrf

from django import forms

from record.outil_web_record.models.affichage_saisie.choix_operations import ChoixOperations as CHX # noms des choix

from record.bd_modeles_record.models import ModeleRecord as ModeleRecordBd
from record.bd_modeles_record.forms.user_forms import MotDePasseAskingForm
from record.bd_modeles_record.forms.user_forms import ModeleRecordFormPartiel
from record.bd_modeles_record.forms.user_forms import ModeleRecordFormComplet

###############################################################################
# Modele : NomsPages
#
# Des constantes (les noms des pages)
#
###############################################################################
class NomsPages(object) :

    NOM_page_modele_record_mdp = 'scn_selection/modele_record_mdp.html'

    NOM_page_liste_scenarios = 'scn_selection/liste_scenarios.html'

    NOM_page_affichage_scenario = 'scn_edition/scenario.html'

    NOM_page_simulation_menu = 'scn_simulation/simulation_menu.html'

    NOM_page_restitution_menu = 'res_menu/restitution_menu.html'

    NOM_page_telechargement_menu = 'res_telechargement/telechargement_menu.html'

    NOM_page_affichage_numerique = 'res_numerique/affichage_numerique.html'

    # menu de choix du type de representation graphique :
    NOM_page_graphique_menu = 'res_graphique/graphique_menu.html'

    # pages de configuration representation graphique (choix des donnees representees...)
    NOM_page_configuration_graphiqueY = 'res_graphique/configuration_graphiqueY.html'
    NOM_page_configuration_graphiqueXY = 'res_graphique/configuration_graphiqueXY.html'

    NOM_page_affichage_graphique = 'res_graphique/affichage_graphique.html'

    NOM_page_fin = 'the_end.html'

###############################################################################
#
# Des methodes de constitution de contextes d'appel de pages
#
###############################################################################

###############################################################################
# Methode contexte_modele_record_mdp : constitue et retourne le contexte de
# la page NOM_page_modele_record_mdp 
###############################################################################
def contexte_modele_record_mdp(request, flashes, modele_record_id ) :

    # Formulaire de saisie du mot de passe (du modele record prive)
    formulaire_mot_de_passe = MotDePasseAskingForm()
    # informations complementaires (a faire suivre)
    formulaire_mot_de_passe.modele_record_id = modele_record_id
            
    # rappel des infos partielles a titre d'information/memo
    modele_record_bd = ModeleRecordBd.objects.get(id=modele_record_id)
    formulaire_modele_record = ModeleRecordFormPartiel(instance=modele_record_bd)

    c = {} # le contexte

    c['formulaire_mot_de_passe'] = formulaire_mot_de_passe
    c['formulaire_modele_record'] = formulaire_modele_record
    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

###############################################################################
# Methode contexte_liste_scenarios : constitue et retourne le contexte de
# la page NOM_page_liste_scenarios 
###############################################################################
def contexte_liste_scenarios(request, flashes, le_modele_record, formulaire_modele_record) :

    # pour memo : request.LANGUAGE_CODE 

    c = {}
    if le_modele_record is not None :
        c['le_modele_record'] = le_modele_record
    # else : cas d'un modele record inexploitable/indisponible (cf flashes)
    c['formulaire_modele_record'] = formulaire_modele_record
    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

###############################################################################
# Methode contexte_affichage_scenario : constitue et retourne le contexte de
# la page d'affichage du scenario
###############################################################################
def contexte_affichage_scenario(request, flashes, scn, contenu_repertoire_data, formulaire_modele_record) :

    c = {}

    # operations proposees : apres avoir eventuellement modifie le scenario,
    # l'usr quittera la page par choix_validationIntermediaire ou
    # choix_validationFinale (ou retour en arriere)
    c['choix_validationIntermediaire'] = CHX.NOM_choix_validationIntermediaire
    c['choix_validationFinale'] = CHX.NOM_choix_validationFinale

    # autres constantes a transmettre pour identifier les operations
    c['operation_validation'] = CHX.VAL_operation_validation
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo

    c['scenario'] = scn.scenario_vpz
    c['definition'] = scn.definition
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine
    c['contenu_repertoire_data'] = contenu_repertoire_data
    c['formulaire_modele_record'] = formulaire_modele_record
    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

###############################################################################
# Methode contexte_simulation_menu : constitue et retourne le contexte de
# la page NOM_page_simulation_menu
###############################################################################
def contexte_simulation_menu(request, flashes, scn, le_modele_record) :

    # formulaire_modele_record (rappel des infos completes a titre d'information/memo)
    modele_record_bd = ModeleRecordBd.objects.get(id=le_modele_record.id)
    formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

    c = {}

    # les 3 types de simulation sont actifs/proposes
    c['choix_simulationSimple'] = CHX.VAL_choix_simulationSimple
    c['choix_simulationMultipleModeLineaire'] = CHX.VAL_choix_simulationMultipleModeLineaire
    # notes_de_developpement appel_page.py, contexte_simulation_menu :
    # simulationMultipleModeTotal est temporairement desactive (en attendant 
    # suppression definitive de cette option/traitement)
    #c['choix_simulationMultipleModeTotal'] = CHX.VAL_choix_simulationMultipleModeTotal
    # remplace par 
    c['choix_simulationMultipleModeTotal'] = CHX.VAL_choix_a_cacher

    # autres constantes a transmettre pour identifier les operations
    c['choix_a_cacher'] = CHX.VAL_choix_a_cacher # valeur de comparaison
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo
    c['operation_retour_edition'] = CHX.VAL_operation_retour_edition

    c['scenario'] = scn.scenario_vpz
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine
    c['definition'] = scn.definition
    c['formulaire_modele_record'] = formulaire_modele_record

    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

###############################################################################
# Methode contexte_restitution_menu : constitue et retourne le contexte de
# la page NOM_page_restitution_menu
###############################################################################
def contexte_restitution_menu(request, flashes, scn, le_modele_record) :

    # formulaire_modele_record (rappel des infos completes a titre d'information/memo)
    modele_record_bd = ModeleRecordBd.objects.get(id=le_modele_record.id)
    formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

    c = {}

    # les types de restitution : choix_num est actif/propose ou non
    # en fonction de type_simulation
    c['choix_graph'] = CHX.VAL_choix_graph
    c['choix_telechgt'] = CHX.VAL_choix_telechgt
    type_simulation = scn.resultat.type_simulation
    if type_simulation == "mono_simulation" :
        c['choix_num'] = CHX.VAL_choix_num
    else : # le choix 'affichage numerique' n'apparait pas dans le menu
        c['choix_num'] = CHX.VAL_choix_a_cacher

    # autres constantes a transmettre pour identifier les operations
    c['choix_a_cacher'] = CHX.VAL_choix_a_cacher # valeur de comparaison
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo
    c['operation_retour_edition'] = CHX.VAL_operation_retour_edition
    c['operation_retour_simulation'] = CHX.VAL_operation_retour_simulation

    c['scenario'] = scn.scenario_vpz
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine
    c['definition'] = scn.definition
    c['formulaire_modele_record'] = formulaire_modele_record

    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

###############################################################################
# Methode contexte_telechargement_menu : constitue et retourne le contexte de
# la page NOM_page_telechargement_menu
###############################################################################
def contexte_telechargement_menu(request, flashes, scn, le_modele_record) :

    # formulaire_modele_record (rappel des infos completes a titre d'information/memo)
    modele_record_bd = ModeleRecordBd.objects.get(id=le_modele_record.id)
    formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

    c = {}

    # les 2 types de telechargement
    # (choix_telechgtDernierFichier est propose/actif sous condition)

    c['choix_telechgtDossier'] = CHX.VAL_choix_telechgtDossier

    # nom du dernier fichier de conservation d'une representation graphique
    # S'il n'existe pas (valeur None), alors il ne faut pas proposer le
    # telechargement correspondant)
    nom = scn.resultat.getNomDernierFichierRepresentationGraphique()
    choix_telechgtDernierFichier = CHX.VAL_choix_telechgtDernierFichier # par defaut
    if nom == None :
        nom = ""
        choix_telechgtDernierFichier = CHX.VAL_choix_a_cacher 
    c['choix_telechgtDernierFichier'] = choix_telechgtDernierFichier
    c['nom_dernier_fichier_rg'] = nom

    # autres constantes a transmettre pour identifier les operations
    c['choix_a_cacher'] = CHX.VAL_choix_a_cacher # valeur de comparaison
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo
    c['operation_retour_edition'] = CHX.VAL_operation_retour_edition
    c['operation_retour_simulation'] = CHX.VAL_operation_retour_simulation
    c['operation_retour_restitution'] = CHX.VAL_operation_retour_restitution

    c['scenario'] = scn.scenario_vpz
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine
    c['definition'] = scn.definition
    c['formulaire_modele_record'] = formulaire_modele_record

    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

###############################################################################
# Methode contexte_affichage_numerique : constitue et retourne le contexte de
# la page d'affichage numerique
###############################################################################
def contexte_affichage_numerique(request, flashes, scn, le_modele_record) :

    # formulaire_modele_record (rappel des infos completes a titre d'information/memo)
    modele_record_bd = ModeleRecordBd.objects.get(id=le_modele_record.id)
    formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

    # les tableaux (avec tri) des variables en lignes (deja prets) :
    # 'type_capture_vars' (toutes vues confondues) et 'vue_vars' (par vue)

    ###########################################################################
    # un tableau des variables en colonnes (prepare/construit ici) :
    # 'tableau_vars'
    ###########################################################################
    tableau_vars = []
    nomvar_time = 'time' # donnee a placer en 1er
    tableau_vars.append( [ 'nom variable', 'type', 'timestep', 'diminutif' ] ) # entete

    for typage,dict_nom_valeurs in scn.resultat.res.type_capture_vars.iteritems() :

        if nomvar_time in dict_nom_valeurs.keys() : # time en 1er
            nomvar = nomvar_time
            donneesvar = dict_nom_valeurs[ nomvar ]
            ligne=[ nomvar, typage.type, typage.timestep, donneesvar.diminutif ]
            for valeur in donneesvar.valeurs :
                ligne.append( valeur )
            tableau_vars.append(ligne)

        for nomvar,donneesvar in dict_nom_valeurs.iteritems() : # autres vars
            if nomvar != nomvar_time :
                ligne=[ nomvar, typage.type, typage.timestep, donneesvar.diminutif ]
                for valeur in donneesvar.valeurs :
                    ligne.append( valeur )
                tableau_vars.append(ligne)
            # else : time deja pris en compte

    # transposee (en remplacant None par "")
    tableau_vars = map(lambda *row: list(row), *tableau_vars)
    for i,l in enumerate(tableau_vars) :
        for j,e in enumerate(l) :
            if e is None :
                tableau_vars[i][j]=""

    ###########################################################################

    c = {}

    # autres constantes a transmettre pour identifier les operations
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo
    c['operation_retour_edition'] = CHX.VAL_operation_retour_edition
    c['operation_retour_simulation'] = CHX.VAL_operation_retour_simulation
    c['operation_retour_restitution'] = CHX.VAL_operation_retour_restitution

    c['scenario'] = scn.scenario_vpz
    c['type_capture_vars'] = scn.resultat.res.type_capture_vars
    c['vue_vars'] = scn.resultat.res.vue_vars
    c['tableau_vars'] = tableau_vars
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine
    c['definition'] = scn.definition
    c['formulaire_modele_record'] = formulaire_modele_record

    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

###############################################################################
# Methode contexte_graphique_menu : constitue et retourne le contexte de
# la page NOM_page_graphique_menu
###############################################################################
def contexte_graphique_menu(request, flashes, scn, le_modele_record) :

    # formulaire_modele_record (rappel des infos completes a titre d'information/memo)
    modele_record_bd = ModeleRecordBd.objects.get(id=le_modele_record.id)
    formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

    c = {}

    # les 2 types de representations graphiques : les 2 choix sont
    # proposes/actifs sans condition, quel que soit le type de simulation 
    c['choix_graphXY'] = CHX.VAL_choix_graphXY
    c['choix_graphY'] = CHX.VAL_choix_graphY

    # autres constantes a transmettre pour identifier les operations
    c['choix_a_cacher'] = CHX.VAL_choix_a_cacher # valeur de comparaison
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo
    c['operation_retour_edition'] = CHX.VAL_operation_retour_edition
    c['operation_retour_simulation'] = CHX.VAL_operation_retour_simulation
    c['operation_retour_restitution'] = CHX.VAL_operation_retour_restitution

    c['scenario'] = scn.scenario_vpz
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine
    c['definition'] = scn.definition
    c['formulaire_modele_record'] = formulaire_modele_record

    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

###############################################################################
# Methode contexte_configuration_graphique : constitue et retourne le contexte
# de la page NOM_page_configuration_graphiqueY ou
# NOM_page_configuration_graphiqueXY 
# (rq : le parametre choix_graph transmis vaut choix_graphY ou choix_graphXY)
###############################################################################
def contexte_configuration_graphique(request, flashes, scn, le_modele_record, type_simulation, type_capture_vars, choix_graph ) :

    # formulaire_modele_record (rappel des infos completes a titre d'information/memo)
    modele_record_bd = ModeleRecordBd.objects.get(id=le_modele_record.id)
    formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

    c = {}

    # le type de simulation :
    c['type_simulation'] = type_simulation

    # les resultats :
    c['type_capture_vars'] = type_capture_vars

    # le type de representation graphique choisi/en cours (a faire suivre) :
    c['choix_graph'] = choix_graph

    # autres constantes a transmettre pour identifier les operations
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo
    c['operation_retour_edition'] = CHX.VAL_operation_retour_edition
    c['operation_retour_simulation'] = CHX.VAL_operation_retour_simulation
    c['operation_retour_restitution'] = CHX.VAL_operation_retour_restitution

    c['scenario'] = scn.scenario_vpz
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine
    c['definition'] = scn.definition
    c['formulaire_modele_record'] = formulaire_modele_record

    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

###############################################################################
# Methode contexte_affichage_graphique : constitue et retourne le contexte
# de la page NOM_page_affichage_graphique
###############################################################################
def contexte_affichage_graphique(request, flashes, scn, le_modele_record ) :

    # formulaire_modele_record (rappel des infos completes a titre d'information/memo)
    modele_record_bd = ModeleRecordBd.objects.get(id=le_modele_record.id)
    formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

    c = {}

    # liste des fichiers existants sous rep_restitutions
    liste_fichiers = scn.espace_exploitation.getListeFichiersRepRestitutions()
    if len(liste_fichiers) == 0 : # liste vide 
        # artifice pour permettre verification presence de liste_fichiers
        liste_fichiers = [ "aucun" ]
    c['liste_fichiers'] = liste_fichiers

    # autres constantes a transmettre pour identifier les operations
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo
    c['operation_retour_edition'] = CHX.VAL_operation_retour_edition
    c['operation_retour_simulation'] = CHX.VAL_operation_retour_simulation
    c['operation_retour_restitution'] = CHX.VAL_operation_retour_restitution

    c['scenario'] = scn.scenario_vpz
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine
    c['definition'] = scn.definition
    c['formulaire_modele_record'] = formulaire_modele_record

    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

