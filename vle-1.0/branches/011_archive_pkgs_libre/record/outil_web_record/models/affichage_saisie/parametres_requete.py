#-*- coding:utf-8 -*-

###############################################################################
# File parametres_requete.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from django.http import QueryDict

#from configs.conf_trace import CONF_trace
#from record.utils.trace import TraceEcran, TraceErreur

###############################################################################
#
# Classes et methodes dediees a gestion d'affichage et saisie de donnees
#
###############################################################################


###############################################################################
# Modele : ParametresRequete
#
# Classe dediee a la gestion des parametres d'une requete
#
# Les parametres issus d'une requete (request.POST, request.GET) sont au
# format QueryDict : { name : v }, avec v : liste de valeurs [v1,v2,v3...].
#
# ParametresRequete herite de (peut etre geree comme un) QueryDict.
#
# Dans le dict params, les informations (name, v1, v2, v3...) ont ete
# encodees. 
#
# Les traitements (contient_name, get_cles, get_valeurs...) portent sur des
# informations encodees (en fait exploitent params), qu'il s'agisse des
# valeurs d'entree (name...) comme des valeurs retournees ([v1,v2...]...).
#
###############################################################################
class ParametresRequete( QueryDict ) :

    ###########################################################################
    # Construction a partir de parametres_QueryDict 
    ###########################################################################
    def __init__(self, parametres_QueryDict ) :

        self.update( parametres_QueryDict )

        # encodage des informations dans dict params complet correspondant
        # ({ name : v } avec v : liste de valeurs [v1,v2,v3...])
        params = {}
        for k,values in parametres_QueryDict.iterlists() :

            k_encode = k.encode() # cle encodee

            # liste des valeurs encodees
            values_encode = []
            for v in values :
                values_encode.append( v.encode() )

            # prise en compte dans params
            params[ k_encode ] = values_encode

        self.params = params

    ###########################################################################
    # Methode contient_name
    # Retourne True si ParametresRequete (plus exactement params) contient
    # un element de cle/name name 
    ###########################################################################
    def contient_name(self, name) :
        return ( name in self.params.keys() )

    ###########################################################################
    # Methode get_valeurs
    # Retourne la liste des valeurs [v1,v2,v3...] de l'element de cle/name
    # name (de params)
    ###########################################################################
    def get_cles(self) :
        return ( self.params.keys() )

    ###########################################################################
    # Methode get_valeurs
    # Retourne la liste des valeurs [v1,v2,v3...] de l'element de cle/name
    # name (de params) 
    ###########################################################################
    def get_valeurs(self, name) :
        return ( self.params[name] )

    ###########################################################################
    # Methode get_valeur
    # Retourne la 1ere valeur v1 de la liste des valeurs [v1,v2,v3...] de
    # l'element de cle/name name (de params) 
    ###########################################################################
    def get_valeur(self, name) :
        return ( self.get_valeurs(name)[0] )

