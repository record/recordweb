#-*- coding:utf-8 -*-

###############################################################################
# File scenariovpz.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Authors :
# Nathalie Rousse, INRA RECORD team member,
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
# Modele : ScenarioVpz
#
# Les informations d'identification/caracterisation du fichier vpz (scenario)
# qui a ete selectionne dans un paquet (modele record) pour etre simule.
#
# infos_generales_initiales_scenario sont les infos_generales qui ont ete 
# definies/donnees pour le scenario lorsqu'il se trouvait dans son etat
# initial. Le scenario est susceptible d'etre modifie dans la suite des
# traitements tandis que infos_generales_initiales_scenario est invariable.
#
###############################################################################
class ScenarioVpz(object):

    ###########################################################################
    # Construction 
    ###########################################################################
    def __init__(self, nom_vpz, nom_paquet, description_modele_record, nom_pkg, version_vle, nom_repertoire_lot_pkgs, nom_repertoire_data,infos_generales_initiales_scenario ) :

        # le paquet dont il fait partie
        self.paquet = Paquet(nom_paquet, description_modele_record, nom_pkg, version_vle, nom_repertoire_lot_pkgs, nom_repertoire_data)

        # le vpz
        self.nom_vpz = nom_vpz

        # InfosGeneralesScenario
        self.infos_generales_initiales_scenario = infos_generales_initiales_scenario

    def get_paquet(self):
        return self.paquet

    def get_nom_paquet(self):
        return self.paquet.nom
    def get_description_paquet(self):
        return self.paquet.description_modele_record
    def get_nom_pkg(self):
        return self.paquet.nom_pkg
    def get_version_vle(self):
        return self.paquet.version_vle
    def get_nom_repertoire_lot_pkgs(self):
        return self.paquet.nom_repertoire_lot_pkgs
    def get_nom_repertoire_data(self):
        return self.paquet.nom_repertoire_data

    def get_nom_vpz(self):
        return self.nom_vpz

    def set_nom_vpz(self, nom_vpz):
        self.nom_vpz = nom_vpz

    def get_infos_generales_initiales_scenario(self) : 
        return self.infos_generales_initiales_scenario

###############################################################################
# Modele : Paquet (utilise par ScenarioVpz)
#
# Les informations du modele record pour son utilisation (visualisation,
# simulation...) dans l'outil_web_record
#
# Ces informations proviennent du ModeleRecord correspondant :
# - dont sont gardees : informations techniques (nom, version_vle...)
# - a priori non gardees/conservees : informations de gestion (mot_de_passe,
#   responsables, cr_reception...)
#
# Paquet est construit a partir du ModeleRecord pour le scenario choisi.
# Paquet est l'objet traite une fois le scenario choisi dans le modele record.
#
# Rq : (1) ModeleRecord (dans la bd)
#  --> (2) LesModelesRecord (presentation des modeles record dans
#          l'outil_web_record, pour choix du modele et du scenario
#  --> (3) Paquet : construit par rapport au scenario choisi. Paquet est 
#          l'objet traite une fois le scenario choisi dans le modele record.
#
###############################################################################
class Paquet(object):

    def __init__(self, nom, description_modele_record, nom_pkg, version_vle, nom_repertoire_lot_pkgs, nom_repertoire_data):

	    # nom du modele 
        self.nom = nom

	    # description du modele record
        self.description_modele_record = description_modele_record

        # nom effectif du paquet principal du modele record
        # en tant que paquet vle
        self.nom_pkg = nom_pkg

	    # version vle sous laquelle le paquet a ete receptionne
        self.version_vle = version_vle

        # nom (absolu) du repertoire contenant notamment le paquet principal,
        # en fait regroupant les paquets constituant le modele record
        # (le paquet (principal) et tous les paquets de dependance)
        self.nom_repertoire_lot_pkgs = nom_repertoire_lot_pkgs

        # nom (absolu) du repertoire des fichiers des donnees de simulation
        self.nom_repertoire_data = nom_repertoire_data

###############################################################################

