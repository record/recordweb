( function($){

jQuery.fn.mon_infobulle = function(options) {

    var element = document.createElement("div");

    $(element).addClass(options.infobullecss).hide();

    document.body.appendChild(element);

    return this.each(function() {
        $(this).hover(function() {
            $(element).show();
            $(element).html($(this).attr("texte_bulle"));
            $(this).mousemove(function(e) {
                $(element).css({ "position": "absolute",

                                 /* a partir */
                                 /* ordonnee position souris */
                                 "top": e.pageY + options.offsetY,
                                 /* abscisse origine ecran */
                                 "left": 0 + options.offsetX

                                 /* a partir de la position souris
                                 "top": e.pageY + options.offsetY,
                                 "left": e.pageX + options.offsetX */

                                 /* a partir de l'origine ecran
                                 "top": 0 + options.offsetY,
                                 "left": 0 + options.offsetX */
                });
            });
        },
	function() {
            $(element).hide()
        });
    });
};

} )(jQuery)
