#-*- coding:utf-8 -*-

# For django settings of projects that use record applications (common part).

###############################################################################

import os

RECORDWEB_RECORD_DIR = os.path.abspath(os.path.dirname(__file__))

# TEMPLATE_DIRS ###############################################################

TEMPLATE_DIRS_RECORD = ( os.path.join(RECORDWEB_RECORD_DIR,'templates'), ) 

# STATICFILES_DIR #############################################################

STATICFILES_DIRS_RECORD= ( os.path.join(RECORDWEB_RECORD_DIR,'site_media'), )

###############################################################################

print "[record/record_settings.py] pour info variables (settings) :"
print "[record/record_settings.py] RECORDWEB_RECORD_DIR = ", RECORDWEB_RECORD_DIR
print "[record/record_settings.py] TEMPLATE_DIRS_RECORD = ", TEMPLATE_DIRS_RECORD
print "[record/record_settings.py] STATICFILES_DIRS_RECORD= ", STATICFILES_DIRS_RECORD

