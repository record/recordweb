#-*- coding:utf-8 -*-

###############################################################################
# File dirs_and_files.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
#
# Methodes concernant la manipulation de repertoires/fichiers
#
###############################################################################

import os
import os.path

from record.utils.cmdes_systeme import executerCommandeSysteme

try:
    from configs.conf_trace import CONF_trace
except ImportError:
    from record.utils.configs.conf_trace import CONF_trace

from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)


###############################################################################
# 'Verifie' le fichier f (existe et il s'agit bien d'un fichier)
###############################################################################
def verificationFichierOk( f ) :
    verif = os.path.exists(f) and os.path.isfile(f)
    return verif

###############################################################################
# 'Verifie' le repertoire rep : les liens symboliques ne sont pas acceptes
###############################################################################
def verificationRepertoireOk( rep ) :
    verif = os.path.exists(rep) and os.path.isdir(rep) and not os.path.islink(rep)
    return verif

###############################################################################
# Determine si repertoire est un lien
###############################################################################
def isLienRepertoire( rep ) :
    return ( os.path.islink(rep) )

###############################################################################
# 'Verifie' le repertoire rep : les liens symboliques sont acceptes
###############################################################################
def verificationLargeRepertoireOk( rep ) :
    verif = os.path.exists(rep) and os.path.isdir(rep)
    return verif

###############################################################################
# Determine si nom est un nom de repertoire ou de fichier cache (ie avec
# prefixe '.')
###############################################################################
def isFichierOuRepertoireCache( nom ) :
    verif = nom.startswith('.')
    return verif

###############################################################################
# Fournit la liste des fichiers existant sous repertoire rep
###############################################################################
def getListeFichiersRep( rep ) :
    liste = list()
    if verificationRepertoireOk( rep ) :
        liste = os.listdir(  rep  )
        liste = [ e.encode() for e in liste ] 
        liste = [ f for f in liste if verificationFichierOk( os.path.join(rep,f) ) ] 
    return liste

###############################################################################
# Fournit la liste des fichiers existant sous repertoire rep qui ne sont
# pas des fichiers caches (ie prefixes '.')
###############################################################################
def getListeFichiersNonCachesRep( rep ) :
    liste = list()
    liste_noms_absolus = []
    if verificationRepertoireOk( rep ) :
        liste = os.listdir(  rep  )
        liste = [ e.encode() for e in liste ] 
        for f in liste :
            if verificationFichierOk( os.path.join(rep,f) ) and not isFichierOuRepertoireCache(f) :
                liste_noms_absolus.append( f )
    return liste_noms_absolus

###############################################################################
# Fournit la liste des repertoires existant sous repertoire rep
###############################################################################
def getListeRepertoiresRep( rep ) :
    liste = list()
    if verificationRepertoireOk( rep ) :
        liste = os.listdir(  rep  )
        liste = [ e.encode() for e in liste ] 
        liste = [ f for f in liste if verificationRepertoireOk( os.path.join(rep,f) ) ] 
    return liste

###############################################################################
# Fournit la liste des repertoires existant sous repertoire rep qui ne sont
# pas des repertoires caches (ie prefixes '.')
###############################################################################
def getListeRepertoiresNonCachesRep( rep ) :
    liste = list()
    liste_noms_absolus = []
    if verificationRepertoireOk( rep ) :
        liste = os.listdir(  rep  )
        liste = [ e.encode() for e in liste ] 
        for f in liste :
            if verificationRepertoireOk( os.path.join(rep,f) ) and not isFichierOuRepertoireCache(f) :
                liste_noms_absolus.append( f )
    return liste_noms_absolus

###############################################################################
# Fournit la liste des repertoires existant sous repertoire rep qui ne sont
# pas des repertoires caches (ie prefixes '.'), les liens symboliques sont
# acceptes/pris en compte
###############################################################################
def getListeRepertoiresLargesNonCachesRep( rep ) :
    liste = list()
    liste_noms_absolus = []
    if verificationLargeRepertoireOk( rep ) :
        liste = os.listdir(  rep  )
        liste = [ e.encode() for e in liste ] 
        for f in liste :
            if verificationLargeRepertoireOk( os.path.join(rep,f) ) and not isFichierOuRepertoireCache(f) :
                liste_noms_absolus.append( f )
    return liste_noms_absolus

###############################################################################
# Suppression du lien rep
###############################################################################
def supprimerLien( rep ) :
    if isLienRepertoire( rep ) :
        commande = 'rm -f '+rep
        executerCommandeSysteme( commande )
    else :
        t_err.trait()
        t_err.message( "supprimerLien : répertoire " + str(rep) + " N'EST PAS UN LIEN, n'a pas été supprimé" )

###############################################################################
# Suppression du repertoire rep
###############################################################################
def supprimerRepertoire( rep ) :
    if verificationRepertoireOk( rep ) :
        commande = 'rm -fr ' + rep
        executerCommandeSysteme( commande )
    else :
        t_err.trait()
        t_err.message( "supprimerRepertoire : répertoire " + str(rep) + " NOT OK, n'a pas été supprimé" )

###############################################################################
# Suppression tout contenu du repertoire rep
###############################################################################
def viderRepertoire( rep ) :
    if verificationRepertoireOk( rep ) :
        commande = 'rm -fr ' + rep + '/*'
        executerCommandeSysteme( commande )
    else :
        t_err.trait()
        t_err.message( "viderRepertoire : répertoire " + str(rep) + " NOT OK, n'a pas été vidé" )

###############################################################################
# Creer repertoire rep
###############################################################################
def creerRepertoire( rep ) :
    if verificationRepertoireOk( rep ) :
        t_ecr.trait()
        t_ecr.message( "creerRepertoire : répertoire " + str(rep) + " EXISTE DEJA, n'a pas été créé" )
    else :
        commande = 'mkdir ' + rep
        executerCommandeSysteme( commande )

###############################################################################
# Creer lien 
###############################################################################
def creerLien( rep_a, rep_b ) :

    if verificationLargeRepertoireOk( rep_b ) : # existant
        t_ecr.trait()
        t_ecr.message( "creerLien : répertoire " + str(rep_b) + " EXISTE DEJA, le lien n'a pas été créé" )
    else :
        commande = 'ln -s '+rep_a+' '+rep_b
        executerCommandeSysteme( commande )

###############################################################################

