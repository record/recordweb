#-*- coding:utf-8 -*-

###############################################################################
# File session.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
# 
# Methodes relatives a la session
#
###############################################################################

# sauvegarde session
def sauver(request) :
    request.session.modified = True

# efface session
def raz(request) :
    request.session.clear()

# cle existe dans session
def existe(request, cle) :
    return cle in request.session.keys()

# enleve cle de session
def enlever(request, cle) :
    del( request.session[cle] )

# retourne cle de session
def get(request, cle) :
    return request.session[cle]

# ecrit val dans cle de session
def set(request, cle, val) :
    request.session[cle] = val

# supprime l'eventuelle cle
def suppression_prealable(request, cle) :
    if existe(request, cle ) :
        enlever(request, cle )
        sauver(request)

