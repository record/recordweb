#-*- coding:utf-8 -*-

###############################################################################
# File pkg.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

import os

from record.utils.dirs_and_files import getListeFichiersNonCachesRep, getListeRepertoiresNonCachesRep

try:
    from configs.conf_trace import CONF_trace
except ImportError:
    from record.utils.configs.conf_trace import CONF_trace

from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

###############################################################################
#
# Methodes relatives a la structure/organisation d'un paquet vle a partir
# de/connaissant son repertoire (qui peut se situer ailleurs que sous le
# repertoire 'pkgs' de vle)
#
###############################################################################

###############################################################################
# Pkg
#
# Methodes concernant la structure d'un paquet vle
#
###############################################################################
class Pkg(object):

    # Retourne le repertoire 'data' du paquet a partir de son repertoire
    @classmethod
    def getRepertoirePaquetData( cls, rep_pkg ) :
        return ( os.path.join( rep_pkg, 'data' ) )

    # Retourne le repertoire 'lib' du paquet a partir de son repertoire
    @classmethod
    def getRepertoirePaquetLib( cls, rep_pkg ) :
        return ( os.path.join( rep_pkg, 'lib' ) )

    # Retourne le repertoire 'exp' du paquet a partir de son repertoire
    @classmethod
    def getRepertoirePaquetExp( cls, rep_pkg ) :
        return ( os.path.join( rep_pkg, 'exp' ) )

    # Retourne le repertoire 'output' du paquet a partir de son repertoire
    @classmethod
    def getRepertoirePaquetOutput( cls, rep_pkg ) :
        return ( os.path.join( rep_pkg, 'output' ) )

    # Retourne la liste des noms de fichiers vpz (avec leur extension)
    # existants pour le paquet caracterise/identifie par son repertoire rep_pkg 
    @classmethod
    def getListeVpzAvecExtension( cls, rep_pkg ) :
        rep_pkg_exp = cls.getRepertoirePaquetExp( rep_pkg )
        liste = getListeFichiersNonCachesRep( rep_pkg_exp )
        liste_vpz_avec_extension = [ f for f in liste if f.endswith('.vpz') ] 
        return liste_vpz_avec_extension

    # Retourne la liste des noms de fichiers vpz (sans leur extension)
    # existants pour le paquet caracterise/identifie par son repertoire rep_pkg 
    @classmethod
    def getListeVpzSansExtension( cls, rep_pkg ) :
        liste_vpz_avec_extension = cls.getListeVpzAvecExtension( rep_pkg )
        liste_vpz_sans_extension = [ f.replace('.vpz','') for f in liste_vpz_avec_extension ] 
        return liste_vpz_sans_extension

###############################################################################

