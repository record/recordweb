#-*- coding:utf-8 -*-

###############################################################################
# File exp_config_web.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Authors :
# Nathalie Rousse, INRA RECORD team member,
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

import copy

from record.utils.vle.exp import Exp

from record.outil_web_record.models.conf_web.transcodage import TranscodageConfWeb as tcw
from record.outil_web_record.models.conf_web.transcodage import TranscodageConfWebScenario as tcws

from record.outil_web_record.models.conf_web.conf_web_modele_record import ConfWebModeleRecord
from record.outil_web_record.models.conf_web.conf_web_modele_record import ApplicationWeb

from record.outil_web_record.models.conf_web.conf_web_scenario import ConfWebScenario
from record.outil_web_record.models.conf_web.conf_web_scenario import PageWebDef, PageWebRes

from record.outil_web_record.models.conf_web.infos_donnee import DocDonnee, InfosDonneeDef

from record.outil_web_record.models.conf_web.infos_generales import InfosGeneralesScenario
from record.outil_web_record.models.conf_web.infos_generales import InfosGeneralesAppli

try:
    from configs.conf_trace import CONF_trace
except ImportError:
    from record.utils.configs.conf_trace import CONF_trace

from record.utils.trace import TraceEcran, TraceErreur 

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

###############################################################################
#
# Interface avec pyvle concernant un fichier vpz de configuration (pas du
# repertoire exp).
#
# Interface entre le fichier vpz de configuration d'une part et d'autre part
# part ConfWebModeleRecord, ConfWebScenario...
#
###############################################################################

###############################################################################
#
# Modele ExpConfig et ses classes filles 
# ExpConfigWebModeleRecord, ExpConfigWebScenario, ExpConfigDictionnaire
#
# Objet de manipulation d'un fichier vpz de configuration
#
# Constructions a partir du contenu du fichier vpz de configuration, selon les
# cas (de classe fille) : ConfWebModeleRecord ou ConfWebScenario ou ...
#
    # zzz traiter cas ou condition_applications (...etc) n'existe pas

###############################################################################
class ExpConfig( Exp ):

    ###########################################################################
    # Initialisation/construction a partir du nom du fichier vpz (chemin
    # absolu) nom_vpz,
    ###########################################################################
    def __init__( self, nom_vpz ):
        Exp.__init__( self, nom_vpz )

    ###########################################################################
    #
    # Transcodage : retourne valeur de certaines informations de ExpConfig
    #
    ###########################################################################

    # retourne la valeur de la cle du dict objet, avec valeur
    # CONST_valeur_vide par defaut
    @classmethod
    def getValeur( cls, objet, cle ) :
        v = tcw.CONST_valeur_vide # par defaut
        if cle in objet.keys() :
            v = objet[cle]
        return v

    # retourne la valeur de titre du dict objet
    @classmethod
    def getTitre( cls, objet ) :
        return cls.getValeur( objet, 'titre' )

    # retourne la valeur de description du dict objet
    @classmethod
    def getDescription( cls, objet ) :
        return cls.getValeur( objet, 'description' )

    # retourne la valeur de help du dict objet
    @classmethod
    def getHelp( cls, objet ) :
        return cls.getValeur( objet, 'help' )

    # retourne la valeur de doc_dico_prefere de donnee
    @classmethod
    def getDocDicoPrefere( cls, donnee ) :
        return cls.getValeur( donnee, 'doc_dico_prefere' )

    # retourne la valeur de vpz_view_name de donnee
    @classmethod
    def getVpzViewName( cls, donnee ) :
        return cls.getValeur( donnee, 'vpz_view_name' )

    # retourne la valeur de nom_frenchname de donnee
    @classmethod
    def getNomFrenchname( cls, donnee ) :
        return cls.getValeur( donnee, 'nom_frenchname' )

    # retourne la valeur de nom_englishname de donnee
    @classmethod
    def getNomEnglishname( cls, donnee ) :
        return cls.getValeur( donnee, 'nom_englishname' )

    # retourne la valeur de description de donnee
    @classmethod
    def getDescription( cls, donnee ) :
        return cls.getValeur( donnee, 'description' )

    # retourne la valeur de unite de donnee
    @classmethod
    def getUnite( cls, donnee ) :
        return cls.getValeur( donnee, 'unite' )

    # retourne la valeur de val_max de donnee
    @classmethod
    def getValMax( cls, donnee ) :
        return cls.getValeur( donnee, 'val_max' )

    # retourne la valeur de val_min de donnee
    @classmethod
    def getValMin( cls, donnee ) :
        return cls.getValeur( donnee, 'val_min' )

    # retourne la valeur de val_recommandee de donnee
    @classmethod
    def getValRecommandee( cls, donnee ) :
        return cls.getValeur( donnee, 'val_recommandee' )

    # retourne la valeur de web_dimension de donnee
    @classmethod
    def getWebDimension( cls, donnee ) :
        return cls.getValeur( donnee, 'web_dimension' )

    # retourne la valeur de web_mode de donnee
    @classmethod
    def getWebMode( cls, donnee ) :
        return cls.getValeur( donnee, 'web_mode' )

    # retourne la valeur de web_name de la donnee
    @classmethod
    def getWebName( cls, donnee ) :
        return cls.getValeur( donnee, 'web_name' )

    # retourne la valeur de vpz_type de donnee
    @classmethod
    def getVpzType( cls, donnee ) :
        return cls.getValeur( donnee, 'vpz_type' )

    # retourne la valeur de vpz_cond_name de donnee
    @classmethod
    def getVpzCondName( cls, donnee ) :
        return cls.getValeur( donnee, 'vpz_cond_name' )

    # retourne la valeur de vpz_port_name de donnee
    @classmethod
    def getVpzPortName( cls, donnee ) :
        return cls.getValeur( donnee, 'vpz_port_name' )

    ###########################################################################
    # Documentation d'une donnee d'une page web 
    # cf le dict correspondant a la cle 'doc' cote ExpConfig
    # (dont la cle 'dico' n'est pas recuperee),
    # cf DocDonnee cote ConfWebScenario
    ###########################################################################

    # maj a jour DocDonnee a partir des champs valides/existants de exp_doc
    @classmethod
    def ecrireExpDocDansDocDonnee( cls, exp_doc, doc ) :

        v = cls.getNomFrenchname(exp_doc)
        if not tcw.valeurVide(v) :
            doc.setNomFrenchname( v )
        v = cls.getNomEnglishname(exp_doc)
        if not tcw.valeurVide(v) :
            doc.setNomEnglishname( v )
        v = cls.getDescription(exp_doc)
        if not tcw.valeurVide(v) :
            doc.setDescription( v )
        v = cls.getUnite(exp_doc)
        if not tcw.valeurVide(v) :
            doc.setUnite( v )
        v = cls.getValMax(exp_doc)
        if not tcw.valeurVide(v) :
            doc.setValMax( v )
        v = cls.getValMin(exp_doc)
        if not tcw.valeurVide(v) :
            doc.setValMin( v )
        v = cls.getValRecommandee(exp_doc)
        if not tcw.valeurVide(v) :
            doc.setValRecommandee( v )
        v = cls.getHelp(exp_doc)
        if not tcw.valeurVide(v) :
            doc.setHelp( v )

    # retourne la valeur de la cle dans le dict 'doc' du dict donnee,
    # avec valeur CONST_valeur_vide par defaut
    @classmethod
    def get_docValeur( cls, donnee, cle ) :
        v = tcw.CONST_valeur_vide # par defaut
        if 'doc' in donnee.keys() :
            if cle in donnee['doc'].keys() :
                v = donnee['doc'][cle]
        return v

    # pour donnee (d'une page web ou d'un dictionnaire),
    # retourne la valeur de nom_frenchname de doc de donnee
    @classmethod
    def get_docNomFrenchname( cls, donnee ) :
        return cls.get_docValeur( donnee, 'nom_frenchname' )

    # pour donnee (d'une page web ou d'un dictionnaire),
    # retourne la valeur de nom_englishname de doc de donnee
    @classmethod
    def get_docNomEnglishname( cls, donnee ) :
        return cls.get_docValeur( donnee, 'nom_englishname' )

    # pour donnee (d'une page web ou d'un dictionnaire),
    # retourne la valeur de description de doc de donnee
    @classmethod
    def get_docDescription( cls, donnee ) :
        return cls.get_docValeur( donnee, 'description' )

    # pour donnee (d'une page web ou d'un dictionnaire),
    # retourne la valeur de unite de doc de donnee
    @classmethod
    def get_docUnite( cls, donnee ) :
        return cls.get_docValeur( donnee, 'unite' )

    # pour donnee (d'une page web ou d'un dictionnaire),
    # retourne la valeur de val_max de doc de donnee
    @classmethod
    def get_docValMax( cls, donnee ) :
        return cls.get_docValeur( donnee, 'val_max' )

    # pour donnee (d'une page web ou d'un dictionnaire),
    # retourne la valeur de val_min de doc de donnee
    @classmethod
    def get_docValMin( cls, donnee ) :
        return cls.get_docValeur( donnee, 'val_min' )

    # pour donnee (d'une page web ou d'un dictionnaire),
    # retourne la valeur de val_recommandee de doc de donnee
    @classmethod
    def get_docValRecommandee( cls, donnee ) :
        return cls.get_docValeur( donnee, 'val_recommandee' )

    # pour donnee (d'une page web ou d'un dictionnaire),
    # retourne la valeur de help de doc de donnee
    @classmethod
    def get_docHelp( cls, donnee ) :
        return cls.get_docValeur( donnee, 'help' )

###############################################################################
#
# Modele ExpConfigWebModeleRecord
#
# Classe fille de ExpConfigWeb, relative/dediee a ConfWebModeleRecord
#
# Interface entre le fichier vpz de configuration d'une part et d'autre
# part ConfWebModeleRecord.
#
# Les verifications de format ne sont pas toutes faites dans
# ExpConfigWebModeleRecord, de preference localisees/remontees au niveau de
# ConfWebModeleRecord.
#
###############################################################################
class ExpConfigWebModeleRecord( ExpConfig ) :

    ###########################################################################
    #
    # Constantes propres au format de la configuration web sous forme de
    # fichier vpz : partie propre a la conf web
    # et methodes associees (d'exploitation)
    #
    ###########################################################################

    ###########################################################################
    # l'etat
    ###########################################################################

    # la condition de l'etat
    CONST_condition_etat = 'etat'

    # L'etat contient les informations (ports) a_appliquer et mode_application
    CONST_etat_port_a_appliquer      = 'a_appliquer'
    CONST_etat_port_mode_application = 'mode_application'

    ###########################################################################
    # les scenarios
    ###########################################################################

    # la condition des scenarios
    CONST_condition_scenarios = 'scenarios'

    # La condition des scenarios contient les informations (ports)
    # scenarios_caches et scenarios_montres
    CONST_scenarios_port_scenarios_montres = 'scenarios_montres'
    CONST_scenarios_port_scenarios_caches  = 'scenarios_caches'

    # Un scenario (ie un element de la liste des scenarios_caches ou de la
    # liste des scenarios_montres) est caracterise par une map/dict dont les
    # cles sont
    CONST_scenario_cle_nom_scenario = 'nom_scenario'
    CONST_scenario_cle_applications_associees = 'applications_associees'
    CONST_scenario_cle_infos_generales = 'infos_generales'

    ###########################################################################
    # Constantes valeurs 'normalisees' prises par certaines informations 
    ###########################################################################
    
    # valeurs 'normalisees' prises par mode_application
    CONST_mode_application_maximum = 'maximum'
    CONST_mode_application_minimum = 'minimum'

    ###########################################################################
    # Methodes associees a
    # l'information mode_application (port de condition etat)
    ###########################################################################

    # Retourne valeur normalisee prise par mode_application
    @classmethod
    def getStdModeApplication( cls, mode_application ) :
        v = tcw.CONST_valeur_vide # par defaut
        for val in ( 'maximum', 'Maximum', 'max', 'Max', 'maxi', 'Maxi' ) : 
            if mode_application == val :
                v = cls.CONST_mode_application_maximum
        for val in ( 'minimum', 'Minimum', 'min', 'Min', 'mini', 'Mini' ) : 
            if mode_application == val :
                v = cls.CONST_mode_application_minimum
        return v

    # Methodes qui retournent la signification de mode_application
    # (la comparaison est faite sur sa valeur normalisee)

    # determine si mode_application est "maximum" (ou equivalent)
    @classmethod
    def isMaximumModeApplication(cls, mode_application) :
        r = False # par defaut
        if cls.getStdModeApplication(mode_application) == cls.CONST_mode_application_maximum :
            r = True
        return r

    # determine si mode_application est "minimum" (ou equivalent)
    @classmethod
    def isMinimumModeApplication(cls, mode_application) :
        r = False # par defaut
        if cls.getStdModeApplication(mode_application) == cls.CONST_mode_application_minimum :
            r = True
        return r

    ###########################################################################
    #
    # Construction de ConfWebModeleRecord a partir du nom
    # nom_vpz_config_web_conf du fichier vpz de configuration web (chemin
    # absolu) propre a la conf web.
    # Pour les autres parametres, voir initConfWebModeleRecord
    #
    ###########################################################################
    def __init__( self, nom_vpz_config_web_conf, noms_scenarios_existants, toutes_applications_infos_generales, nom_vpz_config_web_applis, nom_vpz_config_dicos ) :

        ExpConfig.__init__( self, nom_vpz_config_web_conf )

        cr_ok = self.initConfWebModeleRecord( noms_scenarios_existants, toutes_applications_infos_generales, nom_vpz_config_web_applis, nom_vpz_config_dicos )
        if cr_ok != True :
            self.configuration_web = None

    ###########################################################################
    #
    # Methode initConfWebModeleRecord
    #
    # Construit configuration_web (ConfWebModeleRecord) a partir de :
    # - la liste noms_scenarios_existants des noms des scenarios existants
    # - la liste toutes_applications_infos_generales des
    #   InfosGeneralesAppli de toutes les applications existantes de la conf
    # - nom_vpz_config_web_applis : le nom du fichier vpz de configuration web
    #   (chemin absolu) propre aux applis web
    # - nom_vpz_config_dicos : le nom du fichier vpz de configuration 
    #   (chemin absolu) du dictionnaire
    #
    # Le traitement lit dans le fichier vpz de configuration web les
    # informations dont il a besoin, les verifie (format) et interprete (prend
    # certaines decisions en s'appuyant notamment sur
    # noms_scenarios_existants) pour construire ConfWebModeleRecord.
    #
    # toutes_applications_infos_generales et nom_vpz_config_web_applis et
    # nom_vpz_config_dicos sont utilises pour les applications web associees
    # ApplicationWeb (partie InfosGeneralesAppli et InfosApplicationId).
    #
    # nom_vpz_config_dicos est aussi utilise pour/si
    # ajouterApplicationWebParDefautAvecDico.
    #
    ###########################################################################
    def initConfWebModeleRecord( self, noms_scenarios_existants, toutes_applications_infos_generales, nom_vpz_config_web_applis, nom_vpz_config_dicos ) :

        cr_ok = True # par defaut

        # definition/initialisation de self.configuration_web au fur et a  
        # mesure de la lecture de rubrique/partie de la configuration

        t_ecr.message( "ExpConfigWebModeleRecord, initConfWebModeleRecord : declaration/creation de ConfWebModeleRecord")
        self.configuration_web = ConfWebModeleRecord() # vide (mais valide)
        self.configuration_web.initRaz() # precaution (a priori inutile)
        
        # Partie etat (a_appliquer, mode_application)
        mode_application = self.lireModeApplication()
        a_appliquer = self.lireAappliquer( mode_application )

        if not a_appliquer : # fin
            self.configuration_web = None

        else : # (pour)suite exploration du fichier vpz

            # Partie scenarios (scenarios_caches, scenarios_montres),
            # la liste des noms des scenarios disponibles/a montrer
            scenarios_names = self.lireScenariosNames( mode_application, noms_scenarios_existants )

            # Parties infos_generales et applications_associees
            # (dans scenarios_montres ou par defaut)
            # de chaque nom_scenario de scenarios_names
            (infos_generales_scenarios, applications_web_associees) = self.lireInfosGeneralesEtApplicationsAssociees( scenarios_names, mode_application, toutes_applications_infos_generales, nom_vpz_config_web_applis, nom_vpz_config_dicos )

            # En fonction de mode_application, l'application par defaut
            # avec dico est ou non ajoutee dans toutes les listes
            # applications_web_associees[nom_scenario]
            if self.isMaximumModeApplication(mode_application) :
                self.configuration_web.ajouterApplicationWebParDefautAvecDico( applications_web_associees, nom_vpz_config_dicos ) 

            # self.configuration_web
            self.configuration_web.noms_scenarios = scenarios_names
            self.configuration_web.infos_generales_scenarios = infos_generales_scenarios
            self.configuration_web.applications_web_associees = applications_web_associees

        return cr_ok

    ###########################################################################
    #
    # Methodes relatives a l'information a_appliquer (port de etat)
    #
    ###########################################################################

    ###########################################################################
    # Lit (dans exp) la valeur de a_appliquer,
    # la standardise et met par defaut dans un etat signifiant,
    # et la retourne.
    # En cas d'absence/insignifiance, a_appliquer est force a True
    # (respectivt False) si mode_application "maximum" (respectivt "minimum")
    ###########################################################################
    def lireAappliquer( self, mode_application ) :

        condition = self.CONST_condition_etat
        port = self.CONST_etat_port_a_appliquer

        a_appliquer = tcw.CONST_valeur_vide # par defaut

        if condition in self.listConditions() : 
            if port in self.listConditionPorts(condition) :
                a_appliquer = self.getConditionPortValues( condition, port )

        # standardisation
        if type( a_appliquer ) != bool :
            a_appliquer = tcw.CONST_valeur_vide

        # verifier valeur 'signifiante',
        # reaction sinon : valeur True ou False selon mode_application
        if tcw.valeurVide( a_appliquer ) :
            if self.isMinimumModeApplication(mode_application) :
                a_appliquer = False
            else : # Maximum
                a_appliquer = True

        return a_appliquer

    ###########################################################################
    #
    # Methodes relatives a l'information mode_application (port de etat)
    #
    ###########################################################################

    ###########################################################################
    # Lit (dans exp) la valeur de mode_application,
    # la standardise et met par defaut dans un etat signifiant,
    # et la retourne
    # En cas d'absence/insignifiance, mode_application est force a valeur
    # CONST_mode_application_maximum
    ###########################################################################
    def lireModeApplication( self ) :

        condition = self.CONST_condition_etat
        port = self.CONST_etat_port_mode_application

        mode_application = tcw.CONST_valeur_vide # par defaut

        if condition in self.listConditions() : 
            if port in self.listConditionPorts(condition) :
                mode_application = self.getConditionPortValues( condition, port )

        # standardisation
        if type( mode_application ) == str :
            mode_application = self.getStdModeApplication( mode_application )
        else :
            mode_application = tcw.CONST_valeur_vide

        # verifier valeur 'signifiante', 
        # reaction sinon : valeur CONST_mode_application_maximum
        if tcw.valeurVide( mode_application ) :
            mode_application = self.CONST_mode_application_maximum

        return mode_application

    ###########################################################################
    #
    # Methodes relatives a l'information (cle) nom_scenario
    # (condition scenarios)
    #
    ###########################################################################

    ###########################################################################
    # Construit et retourne la liste des noms des scenarios
    # gardes/retenus/disponibles/a montrer.
    # Pour cela, lit (dans exp) les listes scenarios_caches et
    # scenarios_montres (plus exactement l'information nom_scenario de chaque
    # element, pas applications_associees) et les interprete, en s'appuyant
    # notamment sur mode_application et noms_scenarios_existants
    ###########################################################################
    def lireScenariosNames( self, mode_application, noms_scenarios_existants ) :

        #######################################################################
        # determination de scenarios_montres_names et scenarios_caches_names 
        # a partir de scenarios_montres et scenarios_caches (si (ports)
        # lus/existants dans condition)
        #######################################################################

        scenarios_montres_names = tcw.CONST_valeur_vide # par defaut
        scenarios_caches_names = tcw.CONST_valeur_vide # par defaut

        condition = self.CONST_condition_scenarios 

        # indicateurs d'existence (dans exp) de scenarios_montres, 
        # scenarios_caches (en tant que ports de condition)
        scenarios_montres_existait = False # par defaut
        scenarios_caches_existait = False # par defaut

        if condition in self.listConditions() : # condition existe

            # scenarios_montres_names
            port = self.CONST_scenarios_port_scenarios_montres 
            if port in self.listConditionPorts(condition) :

                scenarios_montres_existait = True

                scenarios_montres = self.getConditionSetValue( condition, port )
                scenarios_montres_names = self.getScenariosNames( scenarios_montres )

                # standardisation
                self.standardiserListeScenariosNames( scenarios_montres_names )

            # scenarios_caches_names
            port = self.CONST_scenarios_port_scenarios_caches
            if port in self.listConditionPorts(condition) :

                scenarios_caches_existait = True

                scenarios_caches = self.getConditionSetValue( condition, port )
                scenarios_caches_names = self.getScenariosNames( scenarios_caches )

                # standardisation
                self.standardiserListeScenariosNames( scenarios_caches_names )

        #######################################################################
        # reaction pour determiner par defaut scenarios_montres_names et
        # scenarios_caches_names en cas d'inexistence de scenarios_montres,
        # scenarios_caches (reaction fonction de mode_application
        # et noms_scenarios_existants)
        #######################################################################

        if not scenarios_montres_existait :
            if self.isMinimumModeApplication(mode_application) :
                scenarios_montres_names = [] # liste vide
            else : # mode maximum
                # montrer tous les scenarios existants, sauf ceux se trouvant
                # eventuellement dans scenarios_caches_names
                if scenarios_caches_existait :
                    scenarios_montres_names = [] # init
                    for nom_scenario in noms_scenarios_existants :
                        if nom_scenario not in scenarios_caches_names :
                            scenarios_montres_names.append( nom_scenario )
                else :
                    scenarios_montres_names = copy.deepcopy( noms_scenarios_existants )

        if not scenarios_caches_existait :
            if self.isMinimumModeApplication(mode_application) :
                # cacher tous les scenarios existants, sauf ceux se trouvant
                # eventuellement dans scenarios_montres_names
                if scenarios_montres_existait :
                    scenarios_caches_names = [] # init
                    for nom_scenario in noms_scenarios_existants :
                        if nom_scenario not in scenarios_montres_names :
                            scenarios_caches_names.append( nom_scenario )
                else :
                    scenarios_caches_names = copy.deepcopy( noms_scenarios_existants )
            else : # mode maximum
                scenarios_caches_names = [] # liste vide

        # standardisation
        self.standardiserListeScenariosNames( scenarios_montres_names )
        self.standardiserListeScenariosNames( scenarios_caches_names )

        #######################################################################
        # Gestion d'eventuels conflits entre scenarios_montres et
        # scenarios_caches dans le cas ou, tous les 2 existant
        # contiennent de memes nom_scenario : nom_scenario est enleve d'une
        # des 2 liste scenarios_montres_names, scenarios_caches_names (selon
        # mode_application)
        #######################################################################
        if scenarios_montres_existait and scenarios_caches_existait :
            photo_scenarios_montres_names = copy.deepcopy(scenarios_montres_names)
            for nom_scenario in photo_scenarios_montres_names :
                if nom_scenario in scenarios_caches_names :
                    if self.isMinimumModeApplication(mode_application) :
                        # supprime de scenarios_montres_names
                        scenarios_montres_names.remove( nom_scenario )
                    else : # mode maximum
                        # supprime de scenarios_caches_names
                        scenarios_caches_names.remove( nom_scenario )

        #######################################################################
        # Suppression dans scenarios_montres_names des nom_scenario de 
        # scenarios_caches_names (cf cas ou scenarios_montres_names,
        # scenarios_caches_names crees par defaut)
        #######################################################################
        scenarios_names = copy.deepcopy(scenarios_montres_names) # par defaut
        for nom_scenario in scenarios_montres_names :
            if nom_scenario in scenarios_caches_names :
                scenarios_names.remove( nom_scenario )

        return scenarios_names

    ###########################################################################
    # Standardise la liste de scenarios_names, verifie valeur 'signifiante',
    # reaction sinon : element non str supprime/enleve de liste, liste vide...
    ###########################################################################
    @classmethod
    def standardiserListeScenariosNames( cls, scenarios_names ) :

        if tcw.valeurVide( scenarios_names ) :
            scenarios_names = []

        if type(scenarios_names) != list :
            scenarios_names = []

        photo_scenarios_names = copy.deepcopy(scenarios_names)
        for nom_scenario in photo_scenarios_names :
            if type(nom_scenario) != str :
                scenarios_names.remove( nom_scenario )

    ###########################################################################
    # construit et retourne scenarios_names, la liste des nom_scenario trouves
    # dans scenarios (qui est une liste de map/dict contenant chacun une
    # valeur (str) nom_scenario pour la cle_nom_scenario)
    ###########################################################################
    def getScenariosNames( self, scenarios ) :
        scenarios_names = [] # par defaut
        for scenario in scenarios :
            nom_scenario = self.getValeur( scenario, self.CONST_scenario_cle_nom_scenario )
            if not tcw.valeurVide( nom_scenario ) :
                if type(nom_scenario) == str :
                    scenarios_names.append( nom_scenario )
        return scenarios_names

    ###########################################################################
    #
    # Methodes relatives/communes aux informations (cle) infos_generales et
    # (cle) applications_associees
    # (condition scenarios)
    #
    ###########################################################################

    ###########################################################################
    # Methode lireInfosGeneralesEtApplicationsAssociees
    #
    # Construit et retourne infos_generales_scenarios et
    # applications_web_associees qui sont 2 map/dict definies ainsi :
    # pour tout nom_scenario de scenarios_names,
    # infos_generales_scenarios[ nom_scenario ] = InfosGeneralesScenario de
    # nom_scenario.
    # applications_web_associees[ nom_scenario ] = liste des ApplicationWeb de
    # nom_scenario.
    #
    # Partie applications_web_associees :
    #
    # Pour un scenario nom_scenario present dans scenarios_montres mais sans
    # applications_associees, la liste
    # applications_web_associees[ nom_scenario ] est construite par defaut
    # (selon mode_application, toutes_applications_infos_generales).
    # Pour un scenario nom_scenario absent de scenarios_montres (qui se trouve
    # dans scenarios_names suite a des reactions par defaut anterieures), la
    # liste applications_web_associees[ nom_scenario ] est definie vide.
    # Remarque : Pour un scenario nom_scenario present dans scenarios_montres
    # avec une liste applications_associees presente et vide, la liste
    # applications_web_associees[ nom_scenario ] est/reste vide.
    #
    # nom_vpz_config_web_applis et nom_vpz_config_dicos sont utilises dans la
    # construction de ApplicationWeb
    #
    ###########################################################################
    def lireInfosGeneralesEtApplicationsAssociees( self, scenarios_names, mode_application, toutes_applications_infos_generales, nom_vpz_config_web_applis, nom_vpz_config_dicos ) :

        # lectures dans scenarios_montres
        tous_applications_names = toutes_applications_infos_generales.keys()
        (scenarios_montres_infos_generales_scenarios, scenarios_montres_applications_associees_names) = self.lireScenariosMontres( mode_application, tous_applications_names )

        # construction infos_generales_scenarios
        infos_generales_scenarios = self.creerLesInfosGeneralesScenarios( scenarios_names, scenarios_montres_infos_generales_scenarios )

        # construction applications_web_associees
        applications_web_associees = self.creerLesApplicationsWebAssociees( scenarios_names, toutes_applications_infos_generales, scenarios_montres_applications_associees_names, nom_vpz_config_web_applis, nom_vpz_config_dicos )

        return (infos_generales_scenarios, applications_web_associees) 

    ###########################################################################
    #
    # Pour chaque nom_scenario de scenarios_montres, lit (dans exp) dans
    # scenarios_montres : (i) ses infos_generales pour construire
    # scenarios_montres_infos_generales_scenarios[nom_scenario] ;
    # (ii) la liste de ses applications_associees et l'interprete ou (si non
    # trouvee) la definit par defaut (s'appuie notamment sur mode_application,
    # tous_applications_names) pour construire
    # scenarios_montres_applications_associees_names[nom_scenario].
    #
    # Retourne la paire de dict/map
    # (scenarios_montres_infos_generales_scenarios,
    # scenarios_montres_applications_associees_names).
    #
    ###########################################################################
    def lireScenariosMontres( self, mode_application, tous_applications_names ) :

        # Cherche/lit (dans exp) scenarios_montres (dans la condition des
        # scenarios). Pour memo, scenarios_montres est une liste de map/dict
        # contenant chacun : (i) une valeur (str) nom_scenario pour la
        # cle_nom_scenario ; (ii) une valeur (liste de valeurs (str)
        # nom_application) applications_associees pour la
        # cle_applications_associees ; (iii) une valeur (dict/map)
        # infos_generales pour la cle_infos_generales.
        scenarios_montres = tcw.CONST_valeur_vide # par defaut
        condition = self.CONST_condition_scenarios 
        if condition in self.listConditions() : # condition existe
            port = self.CONST_scenarios_port_scenarios_montres 
            if port in self.listConditionPorts(condition) : # scenarios_montres existe
                scenarios_montres = self.getConditionSetValue( condition, port )

        # a partir de scenarios_montres (+ interpretation/reaction par defaut)
        # construit scenarios_montres_infos_generales_scenarios,
        # scenarios_montres_applications_associees_names

        scenarios_montres_infos_generales_scenarios = {} # par defaut
        scenarios_montres_applications_associees_names = {} # par defaut

        if not tcw.valeurVide( scenarios_montres ) :
        # la liste scenarios_montres existe

            for scenario in scenarios_montres :
                # lecture de ses champs/cles nom_scenario et 
                # infos_generales et applications_associees
                nom_scenario = self.getValeur( scenario, self.CONST_scenario_cle_nom_scenario )
                infos_generales = self.getValeur( scenario, self.CONST_scenario_cle_infos_generales )
                applications_associees_names = self.getValeur( scenario, self.CONST_scenario_cle_applications_associees )

                # verification de nom_scenario (en tant que str)
                if not tcw.valeurVide( nom_scenario ) :
                    if type(nom_scenario) == str :
                    # nom_scenario existe/ok, # determination de ses
                    # scenarios_montres_infos_generales_scenarios
                    # et applications_associees_names :

                        # partie scenarios_montres_infos_generales_scenarios
                        infos_generales_scenario = self.initialisationScenarioInfosGenerales( infos_generales ) 
                        scenarios_montres_infos_generales_scenarios[ nom_scenario ] = infos_generales_scenario

                        # partie scenarios_montres_applications_associees_names
                        applications_associees_names = self.modifierApplicationsAssocieesNames( applications_associees_names, mode_application, tous_applications_names ) 
                        scenarios_montres_applications_associees_names[ nom_scenario ] = applications_associees_names

                    # else : (nom_scenario mauvais format), rien n'est fait
                # else : (nom_scenario inexistant), rien n'est fait

        # else : (scenarios_montres inexistant)
            # scenarios_montres_applications_associees_names est vide

        return (scenarios_montres_infos_generales_scenarios, scenarios_montres_applications_associees_names)

    ###########################################################################
    #
    # Methodes propres a l'information (cle) applications_associees
    # (condition scenarios)
    #
    ###########################################################################

    ###########################################################################
    # modifierApplicationsAssocieesNames
    ###########################################################################
    def modifierApplicationsAssocieesNames( self, applications_associees_names, mode_application, tous_applications_names ) :

        # standardisation, verification de
        # applications_associees_names (en tant que list de str)
        if not tcw.valeurVide( applications_associees_names ) :
        # applications_associees existait dans
        # scenarios_montres pour scenario

            if type(applications_associees_names) == list :
            # applications_associees_names existe/est une liste
                # standardisation : les elements non str sont
                # enleves de applications_associees_names
                photo_applications_associees_names = copy.deepcopy(applications_associees_names)
                for application in photo_applications_associees_names :
                    if type(application) != str :
                        applications_associees_names.remove( application )
            else : # applications_associees_names mauvais format
                # reaction :
                applications_associees_names = [] # liste vide

        else : # tcw.valeurVide( applications_associees_names )
        # applications_associees n'existait pas dans
        # scenarios_montres pour scenario

            # reaction par defaut
            if self.isMinimumModeApplication(mode_application) :
                applications_associees_names = [] # liste vide
            else : # mode maximum
                # rentrer toutes les applications existantes
                applications_associees_names = copy.deepcopy( tous_applications_names )

        return applications_associees_names

    ###########################################################################
    #
    # Construit applications_web_associees (dict/map dont les cles sont les
    # nom_scenario de scenarios_names et les champs/valeurs leur liste
    # d'ApplicationWeb) a partir de scenarios_names,
    # scenarios_montres_applications_associees_names et des informations des
    # applications provenant de la condition des applications.
    #
    # Retourne applications_web_associees
    #
    ###########################################################################
    def creerLesApplicationsWebAssociees( self, scenarios_names, toutes_applications_infos_generales, scenarios_montres_applications_associees_names, nom_vpz_config_web_applis, nom_vpz_config_dicos ) :

        applications_web_associees = {} # par defaut

        tous_applications_names = toutes_applications_infos_generales.keys()

        for nom_scenario in scenarios_names :

            # liste des ApplicationWeb associees a nom_scenario
            applications_associees = [] # par defaut

            if nom_scenario in scenarios_montres_applications_associees_names.keys() :
            # nom_scenario present dans scenarios_montres,
            # recuperation de ses applications associees

                applications_associees_names = scenarios_montres_applications_associees_names[ nom_scenario ]
                for application_name in applications_associees_names :

                    if application_name in tous_applications_names :
                        application_infos_generales = toutes_applications_infos_generales[ application_name ]
                        application_associee = ApplicationWeb()
                        application_associee.setValeursApplicationWebVpz( application_infos_generales, nom_vpz_config_web_applis, application_name, nom_vpz_config_dicos )
                        applications_associees.append( application_associee )

                    # else : (application_name inconnue)
                        # pass

            # else : ( nom_scenario absent de scenarios_montres)
                # applications_associees reste vide

            applications_web_associees[ nom_scenario ] = applications_associees

        return applications_web_associees

    ###########################################################################
    #
    # Methodes propres a l'information (cle) infos_generales
    # (condition scenarios)
    #
    ###########################################################################

    ###########################################################################
    # initialisation de infos_generales_scenario a partir de infos_generales
    # issu de scenarios_montres
    ###########################################################################
    def initialisationScenarioInfosGenerales( self, infos_generales ) :

        # zzzz ajouter controles de format

        infos_generales_scenario = InfosGeneralesScenario() # par defaut

        # verification de infos_generales (en tant que map/dict)
        if not tcw.valeurVide( infos_generales ) :
        # infos_generales existait dans scenarios_montres pour scenario

            if type(infos_generales) == dict :
            # infos_generales existe/est une map/dict

                titre = self.getTitre( infos_generales )
                description = self.getDescription( infos_generales )
                help = self.getHelp( infos_generales )
                infos_generales_scenario.setValeurs( titre, description, help )

            #else : # infos_generales mauvais format
        #else : infos_generales n'existait pas dans scenarios_montres pour scenario

        return infos_generales_scenario

    ###########################################################################
    #
    # Construit infos_generales_scenarios (dict/map dont les cles sont les
    # nom_scenario de scenarios_names et les champs/valeurs leur 
    # InfosGeneralesScenario) a partir de scenarios_names et
    # scenarios_montres_infos_generales_scenarios.
    #
    # Retourne infos_generales_scenarios
    #
    ###########################################################################
    def creerLesInfosGeneralesScenarios( self, scenarios_names, scenarios_montres_infos_generales_scenarios ) :

        infos_generales_scenarios = {} # par defaut

        for nom_scenario in scenarios_names :

            if nom_scenario in scenarios_montres_infos_generales_scenarios.keys() :
            # nom_scenario present dans scenarios_montres,
            # recuperation de ses infos_generales
                infos_generales_scenario = scenarios_montres_infos_generales_scenarios[ nom_scenario ]

            else : # nom_scenario absent de scenarios_montres
                # infos_generales_scenario cree mais vide
                infos_generales_scenario = InfosGeneralesScenario()

            infos_generales_scenarios[ nom_scenario ] = infos_generales_scenario

        return infos_generales_scenarios

###############################################################################
#
# Modele ExpConfigWebScenario
#
# Classe fille de ExpConfigWeb, relative/dediee a ConfWebScenario
#
# Interface entre le fichier vpz de configuration web propre aux applis web
# d'une part et d'autre part ConfWebScenario.
#
# Les verifications de format ne sont pas toutes faites dans
# ExpConfigWebScenario, de preference localisees/remontees au niveau de 
# ConfWebScenario.
#
###############################################################################
class ExpConfigWebScenario( ExpConfig ) :

    ###########################################################################
    #
    # Constantes propres au format de la configuration web sous forme de
    # fichier vpz : partie propre aux applis web
    #
    ###########################################################################

    ###########################################################################
    # les applications
    ###########################################################################

    # la condition des applications
    CONST_condition_applications = 'applications'

    # Une application (ie un port de la condition des applications) contient
    # des map/dict dont chaque cle renseigne un theme de configuration
    CONST_appli_cle_infos_generales = 'infos_generales'
    CONST_appli_cle_dicos           = 'dicos'
    CONST_appli_cle_pages_def       = 'pages_def'
    CONST_appli_cle_pages_res       = 'pages_res'

    ###########################################################################
    # les pages web (def, res)
    ###########################################################################

    # la condition des pages web de definition
    CONST_condition_pages_web_def = 'pages_def'

    # Une page web def (ie un port de la condition des pages web def) contient
    # une map/dict dont les cles sont :
    CONST_page_def_cle_infos_generales = 'infos_generales'
    CONST_page_def_cle_donnees         = 'donnees'

    # la condition des pages res de definition
    CONST_condition_pages_web_res = 'pages_res'

    # Une page web res (ie un port de la condition des pages web res) contient
    # une map/dict dont les cles sont :
    CONST_page_res_cle_infos_generales = 'infos_generales'
    CONST_page_res_cle_donnees         = 'donnees'

    ###########################################################################
    # les valeurs donnees a web_name 
    # dans le cas ou il prend valeur issue de doc
    ###########################################################################
    CONST_frenchname_de_doc = 'frenchname' # prend valeur nom_frenchname de doc
    CONST_englishname_de_doc = 'englishname' # prend valeur nom_englishname de doc

    # Constantes valeurs 'normalisees' prises par certaines informations :
    # Voir TranscodageConfWebScenario pour des valeurs 'normalisees'
    # prises par des informations 'calquees sur' ConfWebScenario

    ###########################################################################
    #
    # Construction/initialisation a partir du nom du fichier vpz (chemin
    # absolu) nom_vpz de la configuration web propre aux applis web.
    #
    ###########################################################################
    def __init__( self, nom_vpz ):
        ExpConfig.__init__( self, nom_vpz )
        self.configuration_web = None

    ###########################################################################
    #
    # Methode initPourApplicationName
    #
    # Construction/initialisation de ConfWebScenario (self.configuration_web)
    # pour le cas d'une application application_name du fichier vpz de
    # configuration web propre aux applis web (presente dedans)
    #
    # Construction a partir de application_name (parametre/port de la
    # condition des applications)
    # et du ExpConfigDictionnaire exp_config_dicos pour y lire/recuperer
    # dictionnaire utilises pour la documentation des donnees
    #
    ###########################################################################
    def initPourApplicationName( self, application_name, exp_config_dicos=None ):

        cr_ok = True # par defaut

        # definition/initialisation de self.configuration_web au fur et a  
        # mesure de la lecture de chaque rubrique/partie de la configuration

        self.configuration_web = ConfWebScenario() # vide (mais valide)
        self.configuration_web.initRaz() # precaution (a priori inutile)
        
        #######################################################################
        # Lecture (dans exp) des informations de l'application
        # application_name : infos_generales, dicos, pages_def, pages_res
        #######################################################################

        dict_infos_appli = self.lireInformationsAppli( application_name )
        infos_generales = dict_infos_appli[ self.CONST_appli_cle_infos_generales ] 
        dicos = dict_infos_appli[ self.CONST_appli_cle_dicos ] 
        pages_def = dict_infos_appli[ self.CONST_appli_cle_pages_def ]
        pages_res = dict_infos_appli[ self.CONST_appli_cle_pages_res ] 

        #######################################################################
        #
        # Partie informations generales application
        #
        #######################################################################

        # initialisation de self.configuration_web.infos_generales
        self.initialisationAppliInfosGenerales( infos_generales )

        #######################################################################
        #
        # Partie dictionnaire 
        # qui servira a documenter les pages web (def et res)
        # il n'est garde ici du dictionnaire complet (correspondant a
        # exp_config_dicos) que la partie correspondant a dico_names
        #
        #######################################################################

        # la liste ordonnee des noms des dicos de l'application 
        # application_name
        dico_names = dicos

        if exp_config_dicos != None :

            # la liste des noms de tous les dictionnaires de la conf
            tous_dictionnaires_names = exp_config_dicos.getDictionnairesNames()

            # verifier que dico_names est inclu dans tous_dictionnaires_names
            # reaction sinon : suppression dans dico_names des noms absents dans
            # tous_dictionnaires_names
            photo_dico_names = copy.deepcopy(dico_names)
            for nom in photo_dico_names :
                if nom not in tous_dictionnaires_names :
                    dico_names.remove(nom)
                    t_err.message( "ExpConfigWeb, le dictionnaire suivant est enleve car inexistant : " + nom )

            # Le dictionnaire est un dict de dicos
            dictionnaire = exp_config_dicos.lirePartieDictionnaire( dico_names )

        else : # exp_config_dicos == None

            # reaction
            dico_names = [] # vider dico_names
            dictionnaire = {} # creer dictionnaire vide

        #######################################################################
        #
        # Partie pages web de definition
        #
        #######################################################################

        #######################################################################
        # pages_web_def_brutes
        #######################################################################

        # la liste des noms des pages web def de l'application
        # application_name
        page_def_names = pages_def # valeur initiale

        # la liste des noms de toutes les pages web def de la conf

        # condition_pages_web_def : nom de la condition dediee aux pages web def
        condition_pages_web_def = self.CONST_condition_pages_web_def

        # La liste des noms de TOUTES les pages web def DE LA CONF (non ordonnee)
        toutes_pages_web_def_names = self.lirePagesWebNames( condition_pages_web_def )

        # verifier que page_def_names est inclu dans toutes_pages_web_def_names
        # reaction : suppression dans page_def_names des noms absents dans
        # toutes_pages_web_def_names
        photo_page_def_names = copy.deepcopy(page_def_names)
        for nom in photo_page_def_names :
            if nom not in toutes_pages_web_def_names :
                page_def_names.remove(nom)
                t_err.message( "ExpConfigWeb, la page de definition suivante est enlevee car inexistante : " + nom )

        # Les pages web de definition brutes de l'application application_name :
        # format d'origine, sans y avoir traite la documentation
        pages_web_def_brutes = self.lireConfPagesWeb( condition_pages_web_def, page_def_names )

        #######################################################################
        # self.configuration_web.pages_web_def
        # a partir de pages_web_def_brutes, dico_names, dictionnaire
        # (mise au format PageWebDef et documentation des pages web def)
        #######################################################################
        self.initialisationPagesWebDef( pages_web_def_brutes, dico_names, dictionnaire )

        # supprimer de page_def_names celles qui n'ont pas ete gardees dans
        # self.configuration_web.pages_web_def
        names = self.configuration_web.pages_web_def.keys() 
        photo_page_def_names = copy.deepcopy(page_def_names)
        for nom in photo_page_def_names :
            if nom not in names :
                page_def_names.remove(nom)

        #######################################################################
        # self.configuration_web.pages_web_def_names, la liste des noms des 
        # pages web def de l'application application_name,
        # a partir de page_def_names
        #######################################################################
        self.configuration_web.pages_web_def_names = page_def_names

        #######################################################################
        #
        # Partie pages web de resultat
        #
        #######################################################################

        #######################################################################
        # pages_web_res_brutes
        #######################################################################

        # la liste des noms des pages web res de l'application application_name
        page_res_names = pages_res # valeur initiale

        # la liste des noms de toutes les pages web res de la conf

        # condition_pages_web_res : nom de la condition dediee aux pages web res
        condition_pages_web_res = self.CONST_condition_pages_web_res

        # La liste des noms de TOUTES les pages web res DE LA CONF (non ordonnee)
        toutes_pages_web_res_names = self.lirePagesWebNames( condition_pages_web_res )

        # verifier que page_res_names est inclu dans toutes_pages_web_res_names
        # reaction : suppression dans page_res_names des noms absents dans toutes_pages_web_res_names
        photo_page_res_names = copy.deepcopy(page_res_names)
        for nom in photo_page_res_names :
            if nom not in toutes_pages_web_res_names :
                page_res_names.remove(nom)
                t_err.message( "ExpConfigWeb, la page de resultat suivante est enlevee car inexistante : " + nom )

        # Les pages web de resultat brutes de l'application application_name :
        # format d'origine, sans y avoir traite la documentation
        pages_web_res_brutes = self.lireConfPagesWeb( condition_pages_web_res, page_res_names )

        #######################################################################
        # self.configuration_web.pages_web_res
        # a partir de pages_web_res_brutes, dictionnaire
        # (mise au format PageWebRes et documentation des pages web res)
        #######################################################################
        self.initialisationPagesWebRes( pages_web_res_brutes, dictionnaire )

        # supprimer de page_res_names celles qui n'ont pas ete gardees dans
        # self.configuration_web.pages_web_res
        names = self.configuration_web.pages_web_res.keys() 
        photo_page_res_names = copy.deepcopy(page_res_names)
        for nom in photo_page_res_names :
            if nom not in names :
                page_res_names.remove(nom)

        #######################################################################
        # self.configuration_web.pages_web_res_names, la liste des noms des
        # pages web res de l'application application_name,
        # a partir de page_res_names
        #######################################################################
        self.configuration_web.pages_web_res_names = page_res_names 

        if cr_ok != True :
            self.configuration_web = None

        return cr_ok

    ###########################################################################
    #
    # Initialisation partie informations generales de l'application
    #
    ###########################################################################

    ###########################################################################
    # Definit self.configuration_web.infos_generales a partir de infos_generales
    ###########################################################################
    def initialisationAppliInfosGenerales( self, infos_generales ) :

        # zzzz ajouter controles de format
        # verifier que infos_generales : map/dict

        # self.configuration_web.infos_generales 
        titre = self.getTitre( infos_generales )
        description = self.getDescription( infos_generales )
        help = self.getHelp( infos_generales )
        self.configuration_web.infos_generales = InfosGeneralesAppli()
        self.configuration_web.infos_generales.setValeurs( titre, description, help )

    ###########################################################################
    #
    # initialisation partie pages web de definition
    # et partie pages web de resultat
    #
    ###########################################################################

    ###########################################################################
    # fonction commune aux pages web def et res
    # Lecture (dans exp) de la liste des noms des pages web
    # c'est la liste des ports de la condition condition_pages_web
    # a ce niveau, la liste n'est pas ordonnee
    ###########################################################################
    def lirePagesWebNames( self, condition_pages_web ) :
        pages_web_names = self.listConditionPorts(condition_pages_web) 

        # zzzz ajouter controles de format
        # verifier que la condition condition_pages_web existe

        return pages_web_names

    ###########################################################################
    # fonction commune aux pages web def et res
    # Lecture (dans exp) de la configuration des pages web
    # a ce niveau, la documentation n'a pas ete traitee
    ###########################################################################
    def lireConfPagesWeb( self, condition_pages_web, pages_web_names ) :
        pages_web = {}
        for page_web_name in pages_web_names :
            pages_web[page_web_name] = self.getConditionSetValue(condition_pages_web,page_web_name)

        # zzzz ajouter controles de format
        # verifier que chaque port (qui correspond/definit une page web) :
        # contient liste de map/dict
        # +? verifier que la premiere map (generalites sur la page) est
        # particuliere par rapport aux suivantes (les donnees de la page) ?

        return pages_web

    ###########################################################################
    # Construction/creation de la documentation DocDonnee d'une donnee d'une
    # page web def
    ###########################################################################
    def docDef( self, id_par_defaut_pour_dico, dico_names, dictionnaire, doc_dicos, doc_locale ) :

        #######################################################################
        # Prealable : definition/determination de dico_prefere et name pour 
        # eventuellement recuperer infos de documentation dans 
        # donnee/element (parametre) name de dico (condition) dico_prefere.
        # dico_prefere et name sont lus dans doc_dicos, avec comme valeur 
        # par defaut id_par_defaut_pour_dico pour name (si name non trouve
        # dans doc_dicos)
        #######################################################################
        dico_prefere = None # par defaut
        name = None # par defaut
        if doc_dicos != None :
            if 'dico_prefere' in doc_dicos.keys() :
                dico_prefere = doc_dicos['dico_prefere']
            if 'name' in doc_dicos.keys() :
                name = doc_dicos['name']
            else :
                name = id_par_defaut_pour_dico

        doc = DocDonnee()

        #######################################################################
        # on parcourt les maj de la moins a la plus prioritaire :
        #######################################################################

        # maj de doc par rapport a id_par_defaut_pour_dico qui serait trouve
        # dans dictionnaire (le 1er trouve en consultant les dicos selon leur
        # liste ordonnee)
        if not tcw.valeurVide( id_par_defaut_pour_dico ) :
            liste_sources = []
            for dico_name in dico_names :
                dico = dictionnaire[dico_name]
                if id_par_defaut_pour_dico in dico.keys() :
                    liste_sources.append( dico[id_par_defaut_pour_dico] ) 
            if len(liste_sources) >= 1 :
                source = liste_sources[0]
                self.ecrireExpDocDansDocDonnee( source, doc )

        # puis si name a ete trouve dans doc_dicos :
        # name issu de doc_dicos prioritaire sur id_par_defaut_pour_dico =>
        # maj de doc par rapport a name qui serait trouve dans dictionnaire
        # (le 1er trouve en consultant les dicos selon leur liste ordonnee)
        if name != None and name != id_par_defaut_pour_dico :
            liste_sources = []
            for dico_name in dico_names :
                dico = dictionnaire[dico_name]
                if name in dico.keys() :
                    liste_sources.append( dico[name] ) 
            if len(liste_sources) >= 1 :
                source = liste_sources[0]
                self.ecrireExpDocDansDocDonnee( source, doc )

        # puis si dico_prefere a ete trouve dans doc_dicos (name existe dans 
        # ce cas : a ete trouve dans doc_dicos ou mis par defaut a
        # id_par_defaut_pour_dico) :
        # dico_prefere prioritaire parmi tous les dicos =>
        # maj de doc par rapport a name qui serait trouve dans dico_prefere
        if dico_prefere != None and name != None :
            if not tcw.valeurVide( name ) :
                if dico_prefere in dictionnaire.keys() :
                    dico = dictionnaire[dico_prefere]
                    if name in dico.keys() :
                        source = dico[name]
                        self.ecrireExpDocDansDocDonnee( source, doc )

        # puis si doc_locale existe : doc_locale prioritaire sur tout =>
        # maj de doc par rapport a doc_locale
        if doc_locale != None :
            source = doc_locale
            self.ecrireExpDocDansDocDonnee( source, doc )

        #
        # zzzz ajouter controles de format ?
        #

        return doc

    ###########################################################################
    # Fonction de mise a jour de web_name dans infos_donnee_def si vaut
    # 'frenchname' ou 'englishname' (recuperer alors sa valeur dans doc)
    ###########################################################################
    def maj_web_name( self, infos_donnee_def ) :

        web_name = infos_donnee_def.getWebName()

        if web_name == self.CONST_frenchname_de_doc :
            # a recuperer dans nom_frenchname si existe
            web_name = infos_donnee_def.doc.getNomFrenchname()
            infos_donnee_def.setWebName( web_name ) # maj

        elif web_name == self.CONST_englishname_de_doc :
            # a recuperer dans nom_englishname si existe
            web_name = infos_donnee_def.doc.getNomEnglishname()
            infos_donnee_def.setWebName( web_name ) # maj

    ###########################################################################
    # self.configuration_web.pages_web_def a partir de 
    # pages_web_def_brutes, dico_names, dictionnaire :
    # - Mise au format PageWebDef 
    # - Documentation des pages web def, a partir du dictionnaire et
    #   doc_locale, selon doc_priorite
    # - Mise a jour de web_name si vaut 'frenchname' ou 'englishname' (valeur
    #   alors recupere dans doc)
    ###########################################################################
    def initialisationPagesWebDef( self, pages_web_def_brutes, dico_names, dictionnaire ):

        for ( nom_page, page_web_brute ) in pages_web_def_brutes.iteritems() :

            # normalement page_web_brute est un singleton (de type dict)
            if len(page_web_brute) < 1 : # page occultee
                t_err.message( "initialisationPagesWebDef, la page de definition suivante est enlevee/ignoree car sans renseignement (ni donnees, ni informations generales) : " + nom_page )
            else :
                if len(page_web_brute) > 1 : # cas particulier
                    # seule la 1ere valeur/information de la page sera prise en consideration (valeurs suivantes ignorees)
                    t_err.message( "initialisationPagesWebDef, la page de definition suivante n'est pas au format, elle contient des informations qui seront ignorees : " + nom_page )

                # parcours des donnees de page_web_brute
                # pour les rentrer documentees dans page_web_def

                page_web_def = PageWebDef() # la page

                # informations de page_web_brute
                p = page_web_brute[0]
                infos_generales = p[ self.CONST_page_def_cle_infos_generales ]
                liste_donnees_brutes = p[ self.CONST_page_def_cle_donnees ]

                # infos_generales
                titre = self.getTitre(infos_generales)
                description = self.getDescription(infos_generales)
                help = self.getHelp(infos_generales)
                page_web_def.infos_generales.setValeurs( titre, description, help )

                # liste_donnees_brutes
                for donnee_brute in liste_donnees_brutes :

                    # donnee a formater/documenter

                    infos_donnee_def = InfosDonneeDef()

                    # infos gardees de donnee_brute
                    # reorganisees (en dict thematiques)

                    web_dimension = self.getWebDimension( donnee_brute )
                    web_name = self.getWebName( donnee_brute )
                    web_mode = self.getWebMode( donnee_brute )
                    infos_donnee_def.setWebConfValeurs( web_dimension, web_name, web_mode )

                    vpz_type = self.getVpzType( donnee_brute )
                    if tcws.isTypeConditionPortVpzType(vpz_type) :

                        # condition et port
                        vpz_cond_name = self.getVpzCondName( donnee_brute )
                        vpz_port_name = self.getVpzPortName( donnee_brute )
                        infos_donnee_def.setVpzIdValeurs( vpz_type, vpz_cond_name, vpz_port_name )
                    else : 
                        infos_donnee_def.setVpzIdValeurs( vpz_type )

                    # ajout documentation creee ici
                    if 'doc_dicos' in donnee_brute.keys() :
                        doc_dicos = donnee_brute['doc_dicos']
                    else :
                        doc_dicos = None
                    if 'doc_locale' in donnee_brute.keys() :
                        doc_locale = donnee_brute['doc_locale']
                    else :
                        doc_locale = None
                    id_pour_dico = infos_donnee_def.getIdParDefaut()
                    infos_donnee_def.doc = self.docDef( id_pour_dico, dico_names, dictionnaire, doc_dicos, doc_locale )

                    #print "--------------------------------------------------------"
                    #print "trace temporaire exp_config_web, ExpConfigWebScenario  :"
                    #print ""
                    #print "infos_donnee_def.doc.nom_frenchname = ", infos_donnee_def.doc.nom_frenchname 
                    #print "infos_donnee_def.doc.nom_englishname = ", infos_donnee_def.doc.nom_englishname 
                    #print "infos_donnee_def.doc.description = ", infos_donnee_def.doc.description 
                    #print "infos_donnee_def.doc.unite = ", infos_donnee_def.doc.unite 
                    #print "infos_donnee_def.doc.val_max = ", infos_donnee_def.doc.val_max 
                    #print "infos_donnee_def.doc.val_min = ", infos_donnee_def.doc.val_min 
                    #print "infos_donnee_def.doc.val_recommandee = ", infos_donnee_def.doc.val_recommandee 
                    #print "infos_donnee_def.doc.help = ", infos_donnee_def.doc.help 

                    #print "web dimension :" + str(infos_donnee_def.getWebDimension())
                    #print "web mode : " + str(infos_donnee_def.getWebMode())
                    #print "vpz type : " + str(infos_donnee_def.getVpzType())
                    #print "vpz cond et port names : " + str(infos_donnee_def.getVpzCondName()) + " , " + str(infos_donnee_def.getVpzPortName()) 
                    #print "web name (juste avant maj_web_name) : " + str(infos_donnee_def.getWebName())
                    #print "--------------------------------------------------------"


                    # maintenant que la doc est construite/connue
                    # maj de web_name si vaut 'frenchname' ou 'englishname'
                    self.maj_web_name( infos_donnee_def )


                    print "web name (juste apres maj_web_name) : " + str(infos_donnee_def.getWebName())


                    page_web_def.liste_donnees.append( infos_donnee_def )

                self.configuration_web.pages_web_def[nom_page] = page_web_def

        # zzzz ajouter controles de format ?


    ###########################################################################
    # Constitution du dict des donnees documentees d'une page web res
    # Ces donnees sont issues du dico prefere
    ###########################################################################
    def docRes( self, doc_dico_prefere, dictionnaire ) :

        donnees_documentees = {}

        if doc_dico_prefere in dictionnaire.keys() :

            dico = dictionnaire[ doc_dico_prefere ]

            for (nom_donnee,doc_issue_de_dico) in dico.iteritems() :
                doc_donnee = DocDonnee()
                self.ecrireExpDocDansDocDonnee( doc_issue_de_dico, doc_donnee )
                donnees_documentees[ nom_donnee ] = doc_donnee

        return donnees_documentees

    ###########################################################################
    # self.configuration_web.pages_web_res a partir de 
    # pages_web_res_brutes, dictionnaire :
    # - Mise au format PageWebRes 
    # - Documentation des pages web res a partir doc_dico_prefere et dictionnaire
    # Une page web res correspond a une unique vue view
    ###########################################################################
    def initialisationPagesWebRes( self, pages_web_res_brutes, dictionnaire ):

        for ( nom_page, page_web_brute ) in pages_web_res_brutes.iteritems() :

            # normalement page_web_brute est un singleton (de type dict)
            if len(page_web_brute) < 1 : # page occultee
                t_err.message( "initialisationPagesWebRes, la page de resultat suivante est enlevee/ignoree car sans renseignement (ni donnees, ni informations generales) : " + nom_page )
            else :
                if len(page_web_brute) > 1 : # cas particulier
                    # seule la 1ere valeur/information de la page sera prise en consideration (valeurs suivantes ignorees)
                    t_err.message( "initialisationPagesWebRes, la page de resultat suivante n'est pas au format, elle contient des informations qui seront ignorees : " + nom_page )

                # parcours des donnees de page_web_brute
                # pour les rentrer documentees dans page_web_res

                page_web_res = PageWebRes() # la page

                # informations de page_web_brute
                p = page_web_brute[0]
                infos_generales = p[ self.CONST_page_res_cle_infos_generales ]
                liste_donnees_brutes = p[ self.CONST_page_res_cle_donnees ]

                # infos_generales
                titre = self.getTitre(infos_generales)
                description = self.getDescription(infos_generales)
                help = self.getHelp(infos_generales)
                page_web_res.infos_generales.setValeurs( titre, description, help )

                # liste_donnees_brutes
                # est normalement un singleton (ou vide) correspondant au
                # dict de l'info propre a l'unique vue view correspondant 
                # a la page (nom de la view et son dico prefere)
                if len( liste_donnees_brutes ) < 1 : # est vide
                    pass
                else :
                    donnee_brute = liste_donnees_brutes[0]
    
                    doc_dico_prefere = self.getDocDicoPrefere( donnee_brute )
                    vpz_view_name = self.getVpzViewName( donnee_brute )

                    # on ne garde que le nom de la vue view
                    page_web_res.vpz_view_name = vpz_view_name

                    # ajout du dict des donnees documentees issues de doc_dico_prefere
                    # donnees est un dict, peut-etre vide
                    donnees_documentees = self.docRes( doc_dico_prefere, dictionnaire )
                    page_web_res.donnees_documentees = donnees_documentees

                self.configuration_web.pages_web_res[nom_page] = page_web_res

            # zzzz ajouter controles de format ?

    ###########################################################################
    #
    # Methodes relatives a l'information applications (condition)
    #
    ###########################################################################

    ###########################################################################
    # Methode lireToutesApplicationsInfosGenerales
    #
    # Lit (dans exp) les infos_generales de toutes les applications,
    # construit et retourne le dict des InfosGeneralesAppli associees
    #
    ###########################################################################
    def lireToutesApplicationsInfosGenerales( self ) :

        applications_infos_generales = {}

        # Partie applications_names,
        # la liste des noms de toutes les applications web de la conf
        tous_applications_names = self.lireApplicationsNames()

        for application_name in tous_applications_names :

            # Lecture (dans exp, dans la condition des applications), des
            # informations de l'application application_name pour y recuperer
            # infos_generales 
            dict_infos_appli = self.lireInformationsAppli( application_name )
            infos_generales = dict_infos_appli[ self.CONST_appli_cle_infos_generales ] 
            titre = self.getTitre( infos_generales )
            description = self.getDescription( infos_generales )
            help = self.getHelp( infos_generales )

            # construction de application_infos_generales a partir de
            # infos_generales lu
            application_infos_generales = InfosGeneralesAppli()
            application_infos_generales.setValeurs( titre, description, help )

            applications_infos_generales[ application_name ] = application_infos_generales

        return applications_infos_generales

    ###########################################################################
    # Lit (dans exp) la liste (non ordonnee) des noms de toutes les
    # applications web de la conf (c'est la liste des ports de la condition
    # des applications), la standardise et met par defaut dans un etat
    # signifiant, et la retourne
    ###########################################################################
    def lireApplicationsNames( self ) :
        condition = self.CONST_condition_applications 

        applications_names = tcw.CONST_valeur_vide # par defaut

        if condition in self.listConditions() : 
            applications_names = self.listConditionPorts(condition) 

        # verifier etat 'signifiant', reaction sinon : valeur liste vide
        if tcw.valeurVide( applications_names ) :
            applications_names = []

        return applications_names 

    ###########################################################################
    #
    # L'information d'une application (dans la condition des applications)
    #
    ###########################################################################

    ###########################################################################
    # Lit (dans exp) les informations de l'application application_name (dans
    # la condition des applications), 
    # les regroupe dans un dict unique dict_infos_appli,
    # retourne dict_infos_appli
    ###########################################################################
    def lireInformationsAppli( self, application_name ) :

        condition = self.CONST_condition_applications 
        port = application_name 

        # liste_infos_appli : la liste des values de port de condition
        # liste_infos_appli est une liste de dict a cle unique
        # qui vont etre regroupes dans dict unique dict_infos_appli
        liste_infos_appli = self.getConditionSetValue( condition, port )
        dict_infos_appli = {}
        for dict_infos in liste_infos_appli :
            dict_infos_appli.update( dict_infos )

        return dict_infos_appli


###############################################################################
#
# Modele ExpConfigDictionnaire
#
# Classe fille de ExpConfigWeb, relative/dediee au dictionnaire
#
###############################################################################
class ExpConfigDictionnaire( ExpConfig ) :

    ###########################################################################
    # Constantes propres au format de la configuration sous forme de
    # fichier vpz : partie du dictionnaire
    ###########################################################################
    # aucune

    ###########################################################################
    #
    # Construction/initialisation a partir du nom du fichier vpz (chemin
    # absolu) nom_vpz de la configuration du dictionnaire.
    #
    # La liste dictionnaires_names des noms de tous les dictionnaires est
    # construite/produite. Par contre le contenu des dictionnaires n'est pas
    # lu a la construction.
    #
    ###########################################################################
    def __init__( self, nom_vpz ):

        ExpConfig.__init__( self, nom_vpz )

        # la liste des noms de tous les dictionnaires
        self.dictionnaires_names = self.lireDictionnairesNames()

        # dictionnaire (tous les dictionnaires) n'est pas gere en attribut de
        # la classe ExpConfigDictionnaire mais en tant que valeur de retour de
        # la methode lireToutDictionnaire, afin de n'etre construit/produit
        # que si besoin (plutot que systematiquement)
        # self.dictionnaire = self.lireToutDictionnaire()

    ###########################################################################
    #
    # Methode getDictionnairesNames
    #
    # Retourne dictionnaires_names qui a ete produit/lu lors de la
    # construction (cf __init__ de ExpConfigDictionnaire)
    #
    ###########################################################################
    def getDictionnairesNames( self ) :
        return self.dictionnaires_names

    ###########################################################################
    # Methode lireDictionnairesNames
    # Lit (dans exp) la liste (non ordonnee) des noms de tous les
    # dictionnaires de la conf (c'est la liste des conditions),
    # (la standardise et met par defaut dans un etat signifiant)
    # et la retourne.
    # En cas d'absence/insignifiance, la liste est vide.
    ###########################################################################
    def lireDictionnairesNames( self ) :
        dictionnaires_names = self.listConditions() 
        return dictionnaires_names

    ###########################################################################
    # Methode lirePartieDictionnaire
    # Lit (dans exp) les dicos presents dans dico_names (liste de noms de dicos)
    # Construit et retourne dictionnaire qui est un dict/map
    # de cle nom_dico et valeur dico (dictionnaire[nom_dico]=dico)
    # ou dico correspond aux informations de la condition nom_dico 
    # lues (dans exp) + d'autres informations (cf cle 'nom_dico')
    # et format transforme (en dict)
    # Remarque : les dicos ne peuvent pas etre tous regroupes en un seul
    # car 2 peuvent contenir un meme nom (la cle nom n'est pas unique).
    # Une donnee du dictionnaire est identifiable dans la totalite du
    # dictionnaire par (donnee['dico'],donnee['id'])
    ###########################################################################
    def lirePartieDictionnaire( self, dico_names ) :
    
        dictionnaire = {}

        for dico_name in dico_names :

            if dico_name in self.dictionnaires_names :

                dico = {} # initialisation dico de dico_name

                # liste des noms des donnees du dico dico_name (ie liste des
                # ports de la condition dico_name)
                noms_donnees = self.listConditionPorts( dico_name ) 

                for nom_donnee in noms_donnees :

                    # lecture/constitution de dict_donnee_dico :
                    # set_donnee_dico est la liste des values du port
                    # nom_donnee de la condition dico_name ; c'est a priori
                    # un singleton dict (a cles multiples) mais il est tenu
                    # compte du cas ou ce serait une liste de dict :
                    # regroupement des dict dans dict unique dict_donnee_dico
                    dict_donnee_dico = {}
                    set_donnee_dico = self.getConditionSetValue( dico_name, nom_donnee )
                    for dict_partiel in set_donnee_dico :
                        if type(dict_partiel) == dict :
                            dict_donnee_dico.update( dict_partiel )

                    # ajout des cles 'dico' et 'id' (informant sur origine, 
                    # procurant un identifiant unique dans le dictionnaire)
                    dict_donnee_dico['dico'] = dico_name
                    dict_donnee_dico['id'] = nom_donnee

                    dico[nom_donnee] = dict_donnee_dico

                dictionnaire[ dico_name ] = dico

        return dictionnaire

    ###########################################################################
    # Methode lireDictionnaireComplet
    # Lit (dans exp) tous les dicos presents
    # (pour le format du dictionnaire retourne, voir lirePartieDictionnaire)
    ###########################################################################
    def lireToutDictionnaire( self ) :
        dictionnaire = self.lirePartieDictionnaire( self.dictionnaires_names )
        return dictionnaire

    ###########################################################################
    # Methode documenterPagesWeb
    #
    # Complete/documente pages_web_def et pages_web_res (de ConfWebScenario)
    # a partir du dictionnaire (tous les dicos)
    #
    ###########################################################################
    def documenterPagesWeb( self, pages_web_def, pages_web_res ) :

        # la liste des noms de tous les dictionnaires de la conf
        tous_dictionnaires_names = self.dictionnaires_names 

        # Le dictionnaire est un dict de dicos
        dictionnaire = self.lireToutDictionnaire()

        # parcours des donnees des pages_web_def
        for page_web_def in pages_web_def.values() :
            for infos_donnee_def in page_web_def.liste_donnees :

                id_pour_dico = infos_donnee_def.getIdParDefaut()
                if not tcw.valeurVide( id_pour_dico ) :

                    # recherche de id_pour_dico dans dictionnaire (le 1er
                    # trouve en consultant les dicos selon leur liste ordonnee)
                    liste_sources = []
                    for dico_name in tous_dictionnaires_names :
                        dico = dictionnaire[ dico_name ]
                        if id_pour_dico in dico.keys() :
                            liste_sources.append( dico[id_pour_dico] ) 

                    if len(liste_sources) >= 1 : # au moins 1 trouve
                        source = liste_sources[0] # le 1er trouve

                        # creation/construction de la documentation DocDonnee
                        doc = DocDonnee()
                        self.ecrireExpDocDansDocDonnee( source, doc )

                        # affectation a infos_donnee_def
                        infos_donnee_def.doc = doc

        # parcours des donnees des pages_web_res
        # !!! a ecrire
