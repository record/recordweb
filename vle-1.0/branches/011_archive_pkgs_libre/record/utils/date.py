#-*- coding:utf-8 -*-

###############################################################################
# File date.py
#
# Application Web RECORD nom_appli_web_record_a_definir
#
# Author : Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2011 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
# Sources des traitements de conversion entre julian day et gregorian date et
# time : 'Python scripts for astronomy',
# URL http://www.atnf.csiro.au/people/Enno.Middelberg/python/python.html,
# gd2jd.py : Converts a gregorian date and time to julian date.
# jd2gd.py : Converts julian date to gregorian date and time.
# Voir en fin de fichier la copie de ce code d'origine
#
###############################################################################

import math, sys, string

###############################################################################
# 
# Classe dediee a une date geree selon plusieurs formats :
# julian day, gregorian date et time
#
###############################################################################
class DateMultiFormat( object ) :

    ###########################################################################
    # Construction
    ###########################################################################
    def __init__(self) :

        # gestion julian
        self.jd = "" # chaine de caracteres correspondant a un float (julian day)

        # gestion gregorian "yyyy-mm-dd" et time "hh:mm:ss" 
        # (avec separateurs adequats)
        self.dd = ""   # chaine de caracteres correspondant a "dd"
        self.mm = ""   # chaine de caracteres correspondant a "mm"
        self.yyyy = "" # chaine de caracteres correspondant a "yyyy"
        self.hh = ""   # chaine de caractere correspondant a "hh" 
        self.min = ""  # chaine de caractere correspondant a "mm" 
        self.sec = ""  # chaine de caractere correspondant a "ss" 

        # configuration des formats : les separateurs
        self.separateur_yyyymmdd = "-"
        self.separateur_hhmmss = ":"

    ###########################################################################
    # Initialisation a partir de julian day
    # julian_day : chaine de caracteres 
    ###########################################################################
    def initialiser_julian( self, julian_day ) :

        # Affectation julian 
        self.jd = julian_day

        # Maj gregorian (conversion julian to gregorian)
        self.rafraichir_gregorian()

    ###########################################################################
    # Initialisation a partir de gregorian date yyyymmdd (et time hhmmss)
    # yyyymmdd : chaine de caracteres "yyyy-mm-dd"
    # hhmmss : chaine de caracteres "hh:mm:ss" optionnel
    # (avec separateurs adequats)
    ###########################################################################
    def initialiser_gregorian( self, yyyymmdd, hhmmss=None ) :

        date=string.split( yyyymmdd, self.separateur_yyyymmdd )

        if hhmmss == None:
            hhmmss = "12"+self.separateur_hhmmss+"00"+self.separateur_hhmmss+"00" # par defaut

        time=string.split( hhmmss, self.separateur_hhmmss )

        # Affectation gregorian date et time

        self.dd = date[2]
        self.mm = date[1]
        self.yyyy = date[0]

        self.hh = time[0]
        self.min = time[1]
        self.sec = time[2]

        # Maj julian (conversion gregorian to julian)
        self.rafraichir_julian()

    ###########################################################################
    # gregorian date et time intouches, rafraichissement julian day a partir
    # gregorian date et time :
    # Conversion gregorian date and time to julian day
    ###########################################################################
    def rafraichir_julian( self ) :

        dd=int( self.dd )
        mm=int( self.mm )
        yyyy=int( self.yyyy )

        hh=float( self.hh )
        min=float( self.min )
        sec=float( self.sec )

        UT=hh+min/60+sec/3600

        if (100*yyyy+mm-190002.5)>0:
            sig=1
        else:
            sig=-1

        JD = 367*yyyy - int(7*(yyyy+int((mm+9)/12))/4) + int(275*mm/9) + dd + 1721013.5 + UT/24 - 0.5*sig +0.5

        self.jd = str(JD)

        # affichage ecran
        # self.affichage_ecran()

    ###########################################################################
    # julian day intouche, rafraichissement gregorian date et time a partir
    # de julian day :
    # Conversion julian day to gregorian date and time.
    ###########################################################################
    def rafraichir_gregorian( self ) :

        jd=float( self.jd )
        jd=jd+0.5
        Z=int(jd)
        F=jd-Z
        alpha=int((Z-1867216.25)/36524.25)
        A=Z + 1 + alpha - int(alpha/4)

        B = A + 1524
        C = int( (B-122.1)/365.25)
        D = int( 365.25*C )
        E = int( (B-D)/30.6001 )

        dd = B - D - int(30.6001*E) + F

        if E<13.5:
            mm=E-1
        if E>13.5:
            mm=E-13
    
        if mm>2.5:
            yyyy=C-4716
        if mm<2.5:
            yyyy=C-4715

        h=int((dd-int(dd))*24)
        min=int((((dd-int(dd))*24)-h)*60)
        sec=int(86400*(dd-int(dd))-h*3600-min*60)

        self.dd = string.zfill(int(dd),2)
        self.mm = string.zfill(int(mm),2)
        self.yyyy = string.zfill(int(yyyy),4)
        self.hh = string.zfill(h,2)
        self.min = string.zfill(min,2)
        self.sec = string.zfill(sec,2)

        # affichage ecran
        # self.affichage_ecran()

    ###########################################################################
    #
    # Retourne date selon differents formats (chaines de caracteres)
    #
    ###########################################################################

    # julian day 
    def get_julian_day( self ) :
        return self.jd

    # gregorian date "yyyy-mm-dd" (avec separateur adequat)
    def get_gregorian_date( self ) :
        yyyymmdd = self.yyyy +self.separateur_yyyymmdd+ self.mm +self.separateur_yyyymmdd+ self.dd
        return yyyymmdd

    def get_dd( self ) :
        return self.dd
    def get_mm( self ) :
        return self.mm
    def get_yyyy( self ) :
        return self.yyyy

    # time "hh:mm:ss" (avec separateur adequat)
    def get_time( self ) :
        hhmmss = self.hh +self.separateur_hhmmss+ self.min +self.separateur_hhmmss+ self.sec
        return hhmmss

    def get_hh( self ) :
        return self.hh
    def get_min( self ) :
        return self.min
    def get_sec( self ) :
        return self.sec

    ###########################################################################
    # affichage ecran
    ###########################################################################
    def affichage_ecran( self ) :
        print "julian : " + self.get_julian_day()
        print "gregorian : " + self.get_gregorian_date() +"  "+ self.get_time()

###############################################################################
# 
# Methodes de gestion de dates (conversions...)
#
###############################################################################

###############################################################################
# Converts julian date to gregorian date and time.
# From jd2gd.py written by Enno Middelberg 2002
# Pour julian_day="2452720" calcule/retourne 21 mars 2003
###############################################################################
def jd2gd( julian_day ) : # julian_day : chaine de caracteres

    jd=float( julian_day )
    jd=jd+0.5
    Z=int(jd)
    F=jd-Z
    alpha=int((Z-1867216.25)/36524.25)
    A=Z + 1 + alpha - int(alpha/4)

    B = A + 1524
    C = int( (B-122.1)/365.25)
    D = int( 365.25*C )
    E = int( (B-D)/30.6001 )

    dd = B - D - int(30.6001*E) + F

    if E<13.5:
        mm=E-1
    if E>13.5:
        mm=E-13

    if mm>2.5:
        yyyy=C-4716
    if mm<2.5:
        yyyy=C-4715

    h=int((dd-int(dd))*24)
    min=int((((dd-int(dd))*24)-h)*60)
    sec=86400*(dd-int(dd))-h*3600-min*60

    # affichage ecran
    months=["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    print "julian day " + julian_day + " => gregorian : " + months[mm-1] + " %i, %i, " % (dd, yyyy), 
    print string.zfill(h,2) + ":" + string.zfill(min,2) + ":" + string.zfill(sec,2) + " UTC"

###############################################################################
# Converts a gregorian date and time to julian date.
# From gd2jd.py written by Enno Middelberg 2002
# Pour ddmmyyyy="21.03.2003", hhmmss="hh:mm:ss.ssss" optionnel, calcule/retourne "2452720"
###############################################################################
def gd2jd( ddmmyyyy, hhmmss=None ) :

    # gregorian_date : ddmmyyyy, hhmmss
    # ddmmyyyy chaine de caracteres "dd.mm.yyyy"
    # hhmmss chaine de caracteres "hh:mm:ss.ssss" optionnel

    date=string.split(ddmmyyyy, ".")
    dd=int(date[0])
    mm=int(date[1])
    yyyy=int(date[2])


    if hhmmss!=None:
        time=string.split(hhmmss, ":")
        hh=float(time[0])
        min=float(time[1])
        sec=float(time[2])
    else:
        hh=12.0
        min=0.0
        sec=0.0

    UT=hh+min/60+sec/3600

    if (100*yyyy+mm-190002.5)>0:
        sig=1
    else:
        sig=-1

    JD = 367*yyyy - int(7*(yyyy+int((mm+9)/12))/4) + int(275*mm/9) + dd + 1721013.5 + UT/24 - 0.5*sig +0.5

    # affichage ecran
    months=["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

    print "gregorian : " + months[mm-1] + " %i, %i, %i:%i:%i UT => julian day %f" % (dd, yyyy, hh, min, sec, JD),


###############################################################################
# Sources
#
# La suite est la copie du code d'origine :
#
# URL http://www.atnf.csiro.au/people/Enno.Middelberg/python/python.html,
# Python scripts for astronomy,
###############################################################################

###############################################################################
# jd2gd.py : Converts julian date to gregorian date and time.
###############################################################################
#
#!/usr/bin/python
#
# task to convert a list of julian dates to gregorian dates
# description at http://mathforum.org/library/drmath/view/51907.html
# Original algorithm in Jean Meeus, "Astronomical Formulae for Calculators"
#
#import math, sys, string
#
#if len(sys.argv)==1:
#    print "\n Task to convert a list of julian dates to gregorian dates."
#    print " Written by Enno Middelberg 2002"
#    print "\n Usage: jd2gd.py [-f] date1 date2 date3 ...\n"
#    sys.exit()
#else:
#    print "\n Task to convert a list of julian dates to gregorian dates."
#    print " Written by Enno Middelberg 2002\n"
#    
#
#for x in sys.argv[1:]:
#    try:
#	    jd=float(x)
#    except ValueError:
#	    continue
#    jd=jd+0.5
#    Z=int(jd)
#    F=jd-Z
#    alpha=int((Z-1867216.25)/36524.25)
#    A=Z + 1 + alpha - int(alpha/4)
#
#    B = A + 1524
#    C = int( (B-122.1)/365.25)
#    D = int( 365.25*C )
#    E = int( (B-D)/30.6001 )
#
#    dd = B - D - int(30.6001*E) + F
#
#    if E<13.5:
#	    mm=E-1
#
#    if E>13.5:
#	    mm=E-13
#
#    if mm>2.5:
#	    yyyy=C-4716
#
#    if mm<2.5:
#	    yyyy=C-4715
#
#    months=["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
#    daylist=[31,28,31,30,31,30,31,31,30,31,30,31]
#    daylist2=[31,29,31,30,31,30,31,31,30,31,30,31]
#
#    h=int((dd-int(dd))*24)
#    min=int((((dd-int(dd))*24)-h)*60)
#    sec=86400*(dd-int(dd))-h*3600-min*60
#
#    # Now calculate the fractional year. Do we have a leap year?
#    if (yyyy%4 != 0):
#	    days=daylist2
#    elif (yyyy%400 == 0):
#	    days=daylist2
#    elif (yyyy%100 == 0):
#	    days=daylist
#    else:
#	    days=daylist2
#
#    if "-f" in sys.argv:
#	    daysum=0
#	    for y in range(mm-1):
#	        daysum=daysum+days[y]
#	    daysum=daysum+dd-1
#
#	    if days[1]==29:
#	        fracyear=yyyy+daysum/366
#	    else:
#	        fracyear=yyyy+daysum/365
#	    print x+" = "+`fracyear`
#    else:
#	    print x+" = "+months[mm-1]+" %i, %i, " % (dd, yyyy), 
#	    print string.zfill(h,2)+":"+string.zfill(min,2)+":"+string.zfill(sec,2)+" UTC"
#
#print
#

###############################################################################
# gd2jd.py : Converts a gregorian date and time to julian date.
###############################################################################
#
#!/usr/bin/python
#
# converts a gregorian date to julian date
# expects one or two arguments, first is date in dd.mm.yyyy,
# second optional is time in hh:mm:ss. If time is omitted,
# 12:00:00 is assumed
#
#import math, sys, string

#if len(sys.argv)==1:
#    print "\n gd2jd.py converts a gregorian date to julian date."
#    print "\n Usage: gd2jd.py dd.mm.yyyy [hh:mm:ss.ssss]\n"
#    sys.exit()
#
#date=string.split(sys.argv[1], ".")
#dd=int(date[0])
#mm=int(date[1])
#yyyy=int(date[2])
#
#
#if len(sys.argv)==3:
#    time=string.split(sys.argv[2], ":")
#    hh=float(time[0])
#    min=float(time[1])
#    sec=float(time[2])
#else:
#    hh=12.0
#    min=0.0
#    sec=0.0
#
#UT=hh+min/60+sec/3600
#
#print "UT="+`UT`
#
#total_seconds=hh*3600+min*60+sec
#fracday=total_seconds/86400
#
#print "Fractional day: %f" % fracday
## print dd,mm,yyyy, hh,min,sec, UT
#
#if (100*yyyy+mm-190002.5)>0:
#    sig=1
#else:
#    sig=-1
#
#JD = 367*yyyy - int(7*(yyyy+int((mm+9)/12))/4) + int(275*mm/9) + dd + 1721013.5 + UT/24 - 0.5*sig +0.5
#
#months=["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
#
#print "\n"+months[mm-1]+" %i, %i, %i:%i:%i UT = JD %f" % (dd, yyyy, hh, min, sec, JD),
#
## Now calculate the fractional year. Do we have a leap year?
#daylist=[31,28,31,30,31,30,31,31,30,31,30,31]
#daylist2=[31,29,31,30,31,30,31,31,30,31,30,31]
#if (yyyy%4 != 0):
#    days=daylist2
#elif (yyyy%400 == 0):
#    days=daylist2
#elif (yyyy%100 == 0):
#    days=daylist
#else:
#    days=daylist2
#
#daysum=0
#for y in range(mm-1):
#    daysum=daysum+days[y]
#daysum=daysum+dd-1+UT/24
#
#if days[1]==29:
#    fracyear=yyyy+daysum/366
#else:
#    fracyear=yyyy+daysum/365
#print " = " + `fracyear`+"\n"
#

