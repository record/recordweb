## @file documentation.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File documentation.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#******************************************************************************
## @package documentation
# Documentation recordweb
#
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# Voir toutes les classes du module recordweb.documentation.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#******************************************************************************
## Environnement informatique
#
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# L'outil web record est heberge sur une machine virtuelle sous Debian 6.
# 
# Configuration de base :
# - Serveur Apache2 + paquets apache2-mpm-prefork, libapache2-mod-wsgi, libapache2-mod-python
# - Python 2.6
# 
# Logiciels requis :
# - framework django (1.3.1)
# - vle, pyvle (1.0.3)
# - librairies python : sqlite 3, python-matplotlib, python-imaging...
# - applications django : django-transmeta-readonly (translations), django-form-utils et django-floppyforms (forms), django-fiber (cms), django-south (schema and data migrations for django project), django-request (statistics module for django)...
# - autres installations : doxygen (generation de documentation a partir du code source), graphviz (generation de graphes : graphes d'appel, de dependances...), exim (serveur smtp, pour email errors)...
# 
# Certains modeles record peuvent requerir d'autres installations (par exemple GDAL pour gengiscan, FORTRAN pour NativeStics...).
# 
# Utilitaires divers :
# sudo, subversion, iceweasel (Web browser based on Firefox)...
# 
# Cette presentation n'est pas exhaustive. Pour l'information complete : voir le fichier install_serveur.txt inclus dans le code source recordweb (recordweb/specifique_serveur/install_serveur.txt).
# 
# Procedure d'INSTALL sur serveur de prod : voir le fichier install_serveur.txt inclus dans le code source recordweb (recordweb/specifique_serveur/install_serveur.txt).
# 
#------------------------------------------------------------------------------
# 
# Complements - informations sur l'environnement informatique :
# 
# - Django, framework open-source de développement web en Python :
#   - https://www.djangoproject.com
#   - http://www.django-fr.org
#   - https://wiki.koumbit.net/Django
#   - http://www.djangopackages.com, http://www.djangopackages.com/grid
# 
#------------------------------------------------------------------------------
# 
#******************************************************************************
class I: pass


#******************************************************************************
## Fonctionnalites - elements de specification et conception.
#
#******************************************************************************
class II:

 #*****************************************************************************
 ## Modele record en BD
 #
 #*****************************************************************************
 # 
 #-----------------------------------------------------------------------------
 # 
 # Un modele record mis a disposition au sein de l'outil web record est caracterise par son lot de paquets vle (l'ensemble des paquets vle requis pour son fonctionnement : paquets de dependance et paquet principal), son paquet principal (contenant ses fichiers vpz scenario de simulation) et un certain nombre d'autres renseignements necessaires a son exploitation (son responsable, sa version vle...).
 # 
 # Caractere prive/public d'un modele record en BD : \n
 # Pour acceder au sein de l'outil web record a un modele record declare prive en BD, il faut saisir son mot de passe, qui est distribue par son responsable aux personnes de son choix.
 # 
 #-----------------------------------------------------------------------------
 #
 #*****************************************************************************
 def A(): pass


 #*****************************************************************************
 ## BD
 #
 #*****************************************************************************
 # 
 # -------------------------------------------------------------------------------
 # 
 # La BD : base de connaissance des modeles record, associee a un espace depot des modeles record.
 # 
 # Les modeles record proposes a l'usr sont ceux enregistres en BD (un modele record existant dans le depot n'est propose qu'a condition d'etre enregistre en BD).
 # 
 # Pas de stockage en BD des travaux de simulation, pas d'identification/enregistrement en BD des utilisateurs, pas d'espace de travail dedie aux utilisateurs.
 # 
 # Recours/alternatives dans l'outil web record :
 # - fonction/operation 'telecharger les resultats' : permet a l'utilisateur de conserver ses resultats chez lui.
 # - fonctionnalites permettant a l'utilisateur de travailler avec ses propres fichiers :
 #   - telecharger son propre fichier data
 #   - telecharger son propre scenario de simulation (fichier vpz), par exemple issu du dossier produit lors d'une operation precedente.
 # 
 # BD contient :
 # - modeles record
 # - mots de passe : mots de passe des modeles record declares prives
 # - individus : les responsables des modeles (dont ceux qui ont la charge de distribuer mot de passe)
 # 
 # Un meme modele record peut etre enregistre plusieurs fois en BD : avec differentes configurations/personnalisations, en versions privees/publiques, ce qui permet de proposer des variantes adaptees au public auquel elles s'adressent. 
 # 
 #-----------------------------------------------------------------------------
 # 
 # Solution actuelle : SQLite (moteur de base de donnees relationnelle accessible par le langage SQL, bd stockee dans un fichier).
 # 
 # Il est envisage de passer au serveur de base de donnee PostgreSQL.
 # 
 # + Utilisation de django-south en cours de developpement (migration de BD)
 # 
 #-----------------------------------------------------------------------------
 # 
 # Mode de gestion de session : using file-based sessions
 # 
 #-----------------------------------------------------------------------------
 #
 #*****************************************************************************
 def B(): pass
 
 #*****************************************************************************
 ## Depot des modeles record
 #
 #*****************************************************************************
 #
 #-----------------------------------------------------------------------------
 #
 # Les modeles record exploites par l'outil web record se trouvent dans un depot : repertoire recordweb_depot.
 # 
 # recordweb_depot contient :
 # - pkgs_recordweb : les paquets vle des modeles record exploites par
 #   l'outil_web_record (code + compilation).
 # - configurations_recordweb : des fichiers de configuration (personnalisation)
 #   supplementaires.
 # - datas_recordweb : des fichiers de donnees de simulation supplementaires.
 # 
 #-----------------------------------------------------------------------------
 #
 # Arborescence de recordweb_depot :
 # 
 # recordweb_depot contient :
 #
 # - configurations_recordweb
 #
 # - datas_recordweb
 #
 # - pkgs_recordweb contient :
 #   - vle-0.8.9 : ...
 #   - vle-1.0 :
 #     - lot_wwdm (a) : wwdm, meteo
 #     - pkgs_2CV : 2CV, meteo
 #     - projet_bilan_hydrique : bilan_hydrique, commun, lecture_fichier
 #     - modele_sunfloV1 : sunfloV1, commun, lecture_fichier, sunfloV1_bio,_itk,...
 #     - ...
 #   - vle-1.1 : ...
 # 
 # (a) repertoire d'un modele record regroupant/contenant tous ses paquets vle (compiles) : son paquet vle principal (celui qui contient les fichiers vpz) et tous ses paquets vle de dependance dans leur version/etat utilisee par le modele record.
 # 
 #-----------------------------------------------------------------------------
 #
 #*****************************************************************************
 def C(): pass
 
 
 #*****************************************************************************
 ## Version vle des modeles record
 #
 #*****************************************************************************
 # 
 #-----------------------------------------------------------------------------
 # 
 # Pour l'instant une seule (vle-1.0).
 # 
 # Prevoir une machine virtuelle par version vle prise en compte.
 # 
 #-----------------------------------------------------------------------------
 #
 #*****************************************************************************
 def D(): pass
 
 #*****************************************************************************
 ## Personnalisation, configuration web
 #
 #*****************************************************************************
 #
 #-----------------------------------------------------------------------------
 #
 # Une configuration peut etre associee a un modele record afin de personnaliser sa presentation dans l'outil. Il faut pour cela la renseigner lors de l'enregistrement du modele dans la bd des modeles record (champs de la partie 'Configuration personnalisation').
 # 
 # Concernant la maniere dont les informations d'un fichier scenario de simulation sont presentees, la representation standard (a defaut de configuration) est basee exclusivement sur les informations trouvees dans le fichier .vpz (les rubriques correspondent aux conditions, les intitules aux noms des parametres...). La configuration permet de choisir/redefinir les rubriques et la repartition des parametres dans celles-ci, de choisir pour chaque parametre de le cacher ou de le montrer en mode de lecture et/ou ecriture, d'en redefinir l'intitule et de lui ajouter des renseignements (unite, valeurs min et max, description...).
 # 
 # La configuration permet egalement de choisir les fichiers vpz qui sont montres et ceux qui doivent rester caches.
 # 
 # Illustrations : voir dans "flexibilite, variantes" sous recordweb.documentation.V "Preparation du modele record par son responsable".
 # 
 # La fonctionnalite personnalisation est actuellement mise en oeuvre sous forme de fichiers xml (vpz) ; decoupage en 3 fichiers : dicos, appli web, conf web.
 # 
 # Le repertoire configurations_recordweb du depot des modeles record recordweb_depot est dedie aux fichiers de configurations web mais ceux-ci peuvent aussi se trouver directement dans le modele record (repertoire meta pour dicos et repertoire web pour appli web, conf web).
 # 
 #-----------------------------------------------------------------------------
 # 
 # Etat du developpement de la fonctionnalite personnalisation : impasse totale faite sur la personnalisation des views (les sorties). Forme definitive de mise en oeuvre non definie.
 #
 #-----------------------------------------------------------------------------
 #
 #*****************************************************************************
 def E(): pass
 
 
 #*****************************************************************************
 ## Repertoire des donnees de simulation
 #
 #*****************************************************************************
 #
 #-----------------------------------------------------------------------------
 #
 # Par defaut, le repertoire des fichiers de donnees de simulation pris en consideration dans l'outil web record pour un modele record est le repertoire data de son paquet principal.
 # 
 # Il est possible d'en changer, pour un autre repertoire dont le chemin doit alors etre renseigne lors de l'enregistrement du modele dans la bd des modeles record (champ 'repertoire des fichiers de donnees').
 # 
 # Le repertoire datas_recordweb du depot des modeles record recordweb_depot est dedie a ces repertoires complementaires de donnees de simulation mais ceux-ci peuvent aussi se trouver ailleurs dans le depot, notamment directement dans le modele record.
 #
 #-----------------------------------------------------------------------------
 #
 #*****************************************************************************
 def F(): pass
 
 #*****************************************************************************
 ## Langues
 #
 #*****************************************************************************
 # 
 #-----------------------------------------------------------------------------
 # 
 # Version francaise, version anglaise
 # 
 # Architecture des dictionnaires de traduction : un dictionnaire commun record et un dictionnaire propre a chaque application (au sens django du terme)
 # 
 # Actuellement le seul dictionnaire utilise/renseigne est le dictionnaire commun record (les dictionnaires propres a chaque application existent mais inusites)
 #
 #-----------------------------------------------------------------------------
 # 
 # django-admin makemessages, compilemessages
 # et fichiers dictionnaires locale/en,fr.../LC_MESSAGES/django.po
 # 
 #-----------------------------------------------------------------------------
 # 
 #*****************************************************************************
 def G(): pass
 
 #*****************************************************************************
 ## Statistiques
 #
 #*****************************************************************************
 #
 #-----------------------------------------------------------------------------
 # 
 # Des statistiques sur frequentation-visites du site
 # 
 # Accessibilite : dans la partie admin : request / overview
 # 
 # Help (interpretation) : "Guide to Web Server Analysis" http://www.webalizer.org/simpleton.html
 # 
 #-----------------------------------------------------------------------------
 # 
 # Utilisation django-request (a statistics module for django)
 # 
 #-----------------------------------------------------------------------------
 #
 #*****************************************************************************
 def H(): pass
 
 #*****************************************************************************
 ## Gestion des erreurs 
 #
 #*****************************************************************************
 # 
 #-----------------------------------------------------------------------------
 # 
 # Dans le code, levee d'un certain nombre de cas d'erreurs donnant lieu a l'affichage de messages en page web (cf variable flashes) ; gestion d'exceptions, dans des phases comme le traitement de simulation.
 # 
 # En developpement : \n
 # settings.DEBUG = True : messages d'erreur en page web
 # 
 # En fonctionnement de production (DEBUG=False) : \n
 # transmission des messages d'erreur par e-mail (erreurs de type 500 et 404) \n
 # cf settings.ADMIN,MANAGER ... \n
 # Serveur smtp : exim (smtp.toulouse.inra.fr) (configuration : http://www.ivorde.ro/EXIM_4_relay_to_smarthost_How_to_route_all_mail_except_local_domain-105.html)
 # 
 #-----------------------------------------------------------------------------
 #
 #*****************************************************************************
 def J(): pass
 
 
 #*****************************************************************************
 ## Deroulement de l'exploitation d'un modele
 #
 #*****************************************************************************
 #
 #-----------------------------------------------------------------------------
 #
 # Chaque usr (session,connexion) possede sa propre variable d'environnement VLE_HOME et son propre repertoire VLE_HOME correspondant. Une fois que l'usr a choisi un modele record puis un de ses scenarios de simulation (et une de ses configurations/cas d'utilisation), un environnement d'exploitation associe temporaire est construit dans le sous repertoire pkgs de son repertoire VLE_HOME ; y sont deroulees ses simulations et stockees temporairement ses resultats d'exploitation (cf dossier de telechargement, representations graphiques...).
 #
 #-----------------------------------------------------------------------------
 #
 #*****************************************************************************
 def K(): pass
 
 #*****************************************************************************
 ## Structure/architecture du code
 #
 #*****************************************************************************
 # 
 #-----------------------------------------------------------------------------
 # 
 #   - recordweb : code de l'outil web record, de la BD des modeles record et du
 #     site d'information
 #   - recordweb_depot : depot de l'outil_web_record
 #   - recordweb_vendor : code externe utilise/appele
 #   - bd_modeles_record_directory : la BD des modeles record
 # 
 #-----------------------------------------------------------------------------
 # 
 # recordweb contient :
 # 
 #   - record : librairies python et applications (au sens django du terme)
 # 
 #   - specifique_serveur : partie propre a l'installation sur serveur de prod
 # 
 #   et les projets (au sens django du terme) :
 #   - rmlib : applications web permettant d'utiliser (observer) et
 #     d'administrer (ajouts, suppressions, modifications) la BD des modeles
 #     record.
 #   - rwtool : applications web permettant d'utiliser l'outil web record, et
 #     de plus (en 'option') d'administrer la BD des modeles record.
 #   - rwsite : application web contenant le site d'information et permettant
 #     d'utiliser l'outil web record et d'administrer la BD des modeles record
 #     (en faisant reference a l'url des projets respectifs rwtool, rmlib).
 # 
 # Les projets d'exploitation sont rwsite et rwtool (rwtool est reference par
 # rwsite).
 # Rq : rmlib n'est pas indispensable a partir du moment ou le projet rwtool
 # est configure pour permettre d'administrer la BD des modeles record.
 # 
 #-----------------------------------------------------------------------------
 # 
 # record contient :
 # 
 #   - utils : code python (independant de django) partage/commun record
 #     (utilitaires, interfaces...)
 # 
 #   - templates : templates partages/communs record
 # 
 #   - forms : formulaires partages/communs record
 # 
 #   - site_media : fichiers statiques (css, js) partages/communs record
 # 
 #   - locale : dictionnaire (translation) partage/commun record
 # 
 #   et les applications (au sens django du terme) :
 #   - bd_modeles_record : application dediee a la BD des modeles record
 #   - outil_web_record : application dediee a l'outil web record
 # 
 #-----------------------------------------------------------------------------
 # 
 # Chaque application (au sens django du terme) contient :
 # 
 #   - sa configuration django : urls.py, configs
 # 
 #   - models : la partie MODELE de son architecture MVC
 # 
 #   - un groupe de repertoires :  la partie CONTROLEUR de son architecture MVC
 #     (chacun de ces repertoires contient urls.py et views.py)
 # 
 #   - templates : la partie VUE de son architecture MVC 
 # 
 #   - forms : ses formulaires propres
 # 
 #   - site_media : ses fichiers statiques (css, js) propres
 # 
 #   - locale : dictionnaire (translation) propre
 # 
 #   - utils : code python (independant de django) propre (utilitaires...)
 # 
 #-----------------------------------------------------------------------------
 # 
 # Chaque projet (au sens django du terme) contient :
 # 
 # - sa configuration django : settings.py, urls.py et configs
 # 
 #-----------------------------------------------------------------------------
 #
 #*****************************************************************************
 def L(): pass
 
 #*****************************************************************************
 ## Performances
 #
 #*****************************************************************************
 # 
 #-----------------------------------------------------------------------------
 # 
 # Performances BD :
 # 
 # Robustesse : risque de perte de donnees avec SQLite en cas de crash.
 # 
 # Il est envisage de passer au serveur de base de donnee PostgreSQL.
 # 
 #-----------------------------------------------------------------------------
 # 
 # Moteur vle :
 # 
 # Pour l'instant un seul moteur de calcul vle (une seule machine virtuelle) sur lequel tous les utilisateurs (toutes les simulations) sont en concurrence.
 # 
 # Solution actuelle en cas de surcharge (trop de ralentissement) : interruption par intervention manuelle au niveau du serveur.
 # 
 # Une solution plus performante serait d'apporter une plus grande puissance de calcul en gerant plusieurs moteurs de calcul vle (architecture multi-machines).
 # 
 #-----------------------------------------------------------------------------
 #
 #*****************************************************************************
 def M(): pass
 
 #*****************************************************************************
 ## Fonctionnalites futures possibles (en fonction orientation donnee a l'outil)
 #
 #*****************************************************************************
 #
 #-----------------------------------------------------------------------------
 #
 # Metadonnees : version operationnelle/finalisee de configuration/personnalisation
 #
 #-----------------------------------------------------------------------------
 #
 # Ajout de traitements d'analyse et exploration numeriques des modeles (analyse de sensibilite, optimisations...) programmes sous R (via Python interface to R) ou Python
 #
 #-----------------------------------------------------------------------------
 #
 #*****************************************************************************
 def N(): pass
   

#******************************************************************************
## Criteres de choix (choix techniques, de conception)
#
#******************************************************************************
#
#------------------------------------------------------------------------------
# 
# Coherence avec choix vle, interface gvle (pour faciliter l'appropriation par ceux qui en sont familiers)
# 
#------------------------------------------------------------------------------
# 
#******************************************************************************
class III: pass


#******************************************************************************
## Vide (sans objet)
#
#******************************************************************************
class IV: pass


#******************************************************************************
## Preparation par son responsable (modelisateur, proprietaire) de la livraison d'un modele record dans la bd et le depot des modeles record pour exploitation par l'outil web record
#
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# Conditions d'acceptation du modele record dans la bd des modeles record et le depot des modeles, pour exploitation dans l'outil web record :
# 
# Pour pouvoir etre exploite au sein de l'outil web record, un modele record doit repondre a un certain nombre de conditions, qu'il s'agira de renseigner lors de son enregistrement dans la bd des modeles record. Par exemple ses responsables doivent etre identifies, son bon fonctionnement sous Linux doit etre verifie/garanti, sa procedure d'installation doit etre renseignee (pour pouvoir reproduire son installation dans le depot de l'outil Web)...
# 
# Avant de permettre aux utilisateurs l'acces a un modele, l'outil web record determine si le modele repond ou non aux conditions requises, en interpretant les champs d'information du modele dans la bd (par exemple la condition necessaire d'autorisation d'exploitation du modele dans l'outil Web Record est directement deduite du champ 'Accord d'exploitation').
# 
#------------------------------------------------------------------------------
# 
# Choix du niveau d'acces (modele prive/public) :
# 
# Le responsable aura a choisir entre un statut public ou prive pour son modele record depose au sein de l'outil web record.
# 
# Si le modele record est prive, il lui est associe un mot de passe qui sera demande a tout utilisateur qui le selectionne dans l'outil web record avant de pouvoir y acceder. Le mot de passe est remis au responsable qui pourra s'il le souhaite le distribuer aux personnes de son choix.
# 
# Un modele de statut public est accessible a tout utilisateur de l'outil web record, dans le sens ou il peut en visualiser, modifier et simuler les scenarios de simulation, acceder aux fichiers des donnees de simulation, telecharger des restitutions de resultats. Les informations de restitution telechargeables sont composees de fichier vpz de scenarios de simulation, de fichiers de donnees de simulation et de fichiers de sorties de simulation. Il ne s'agit pas d'une distribution du modele record en tant que logiciel, qui reste entierement du ressort de son responsable. Notamment, le code source du modele record n'est pas accessible au sein de l'outil web record (ni en telechargement ni en visualisation dans les pages web).
# 
#------------------------------------------------------------------------------
# 
# Comportement/fonctionnement par defaut :
# 
# Le modele record est livre sous forme de paquet(s) vle : son paquet principal (qui contient les fichiers vpz scenarios de simulation) et ses eventuels paquets de dependances.
# 
# Par defaut, l'outil web record propose pour le modele record l'ensemble des fichiers vpz scenarios de simulation trouves dans le repertoire exp de son paquet principal et donne acces (presence dans le dossier de restitution telechargeable) a l'ensemble des fichiers de donnees de simulation trouves dans le repertoire data de son paquet principal.
# 
# Si le responsable ne souhaite pas montrer dans l'outil web record certains fichiers vpz ou fichiers de donnees, il peut les supprimer de la version livree. Toutefois d'autres manieres de faire sont proposees (voir ci-dessous) qui ont pour avantage de ne pas modifier le paquet existant.
# 
# Par defaut, l'outil web record presente les informations d'un fichier scenario de simulation en regroupant les parametres du modele dans des rubriques correspondant chacune a une 'condition vle' du modele, plus une rubrique d'informations generales (debut et duree de simulation...).  
# 
#------------------------------------------------------------------------------
# 
# Fichiers de configuration web :
# 
# Le responsable a la possibilite de personnaliser la presentation de son modele record dans l'outil web record, au moyen de fichiers de configuration web (fichiers xml) : voir recordweb.documentation.II.E "Personnalisation, configuration web".
# 
# La configuration lui permet de modifier la presentation des informations de chaque fichier scenario de simulation et aussi de choisir les fichiers vpz qui seront montres et ceux a cacher.
# 
# Ainsi, le responsable peut construire/preparer la configuration web de son choix et livrer les fichiers xml correspondants soit en plus de son modele record soit directement inseres dans son paquet.
# 
#------------------------------------------------------------------------------
# 
# Choix des informations laissees visibles :
# 
# Fichiers vpz scenarios de simulation : comme vu ci-dessus, le responsable qui ne veut montrer dans l'outil web record que certains fichiers vpz de son modele record peut soit supprimer de la version livree les fichiers vpz a cacher, soit passer par des fichiers de configuration web.
# 
# Fichiers de donnees de simulation : comme vu ci-dessus, le responsable qui ne veut montrer dans l'outil web record que certains fichiers de donnees de son modele record peut supprimer de la version livree les fichiers de donnees a cacher. Il a aussi la possibilite de livrer un nouveau repertoire de fichiers de donnees que l'outil web record utilisera a la place de celui pris par defaut (voir recordweb.documentation.II.F "Repertoire des donnees de simulation"). Ce repertoire complementaire peut etre livre soit en plus du modele record soit directement insere dedans.
# 
#------------------------------------------------------------------------------
# 
# Flexibilite,  variantes :
# 
# Un meme modele record peut etre enregistre plusieurs fois en BD : avec differentes configurations/personnalisations, en versions privees/versions publiques. Ceci permet de proposer des variantes adaptees au public auquel elles s'adressent. 
# 
# Exemples :
# - la presentation 'gestion itk' du modele sunflo ne donne acces qu'aux operations de conduite.
# - la version publique du modele sunflo ne donne acces qu'aux fichiers vpz correspondant a la variete Melody et a certains fichiers de donnees meteo, tandis que les versions privees montrent plus de varietes et de fichiers de donnees meteo.
# - la presentation 'application sol' du modele bilan hydrique ne montre que des parametres sol.
# - la presentation 'application pour non experts' du modele bilan hydrique montre certains parametres plante reserves aux experts mais ne permet pas de les modifier.
# - documentation des parametres du modele bilan hydrique.
# 
#------------------------------------------------------------------------------
# 
#******************************************************************************
class V: pass

#******************************************************************************

