#!/bin/bash

###
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
# Copyright (C) 2011-2012 INRA http://www.inra.fr
###

# Script de construction (a partir de install_projet.txt) du fichier texte genere_install_demonstrateur_prive.txt contenant les instructions d'installation du projet web demonstrateur_prive

# !!! cree/modifie fichier temporaire tmp_file.txt

echo "construction (a partir de install_projet.txt) du fichier texte genere_install_demonstrateur_prive.txt contenant les instructions d'installation du projet web demonstrateur_prive"

grep '#r' install_projet.txt > genere_install_demonstrateur_prive.txt
sed 's/#[#w]#[#u]#[#r]|//' genere_install_demonstrateur_prive.txt > tmp_file.txt
cat tmp_file.txt > genere_install_demonstrateur_prive.txt
sed 's/nom_du_projet_web/demonstrateur_prive/g' genere_install_demonstrateur_prive.txt > tmp_file.txt
cat tmp_file.txt > genere_install_demonstrateur_prive.txt

