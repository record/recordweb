###############################################################################
#
#                   JOURNAL DES MISES EN PRODUCTION
#
# developpement local -> essai sur VM locale webrecord_test -> sur VM de prod
#
###############################################################################

                            ###################################################
                            # A reproduire sur VM de prod webrecord : 
                            # effacer lot_azodynwheat de /opt/recordweb_depot
                            # et enlever le modele de la BD
                            ###################################################

###############################################################################
# 17/07/14 : les operations effectuees sur VM webrecord
###############################################################################

Demonstrateur_public :
- reactivation du choix simulation multiple mode total (croisee).

###############################################################################
# 04/07/14 : les operations effectuees sur VM webrecord
###############################################################################

Demonstrateur_public :
- Le modele xpest (lot_xpest) est installe. Il s'agit de l'etat initial en cours de mise au point avec Jean-Noel Aubertot (fichiers vpz et personnalisation).

###############################################################################
# 09/04/14 : les operations effectuees sur VM webrecord
###############################################################################

Demonstrateur_prive :
- Ajout compte 'means'.
- Le modele micmac (lot_micmac) est active (acces sous condition).
Demonstrateur_public :
- Le modele micmac (lot_micmac) est active (acces sous condition).

###############################################################################
# 18/03/14 : les operations effectuees sur VM webrecord
###############################################################################

Ajout du lot lot_cnmip (modele ceres+stics) qui vient d'etre ajoute au depot
de modeles.

Ajoute uniquement sur demonstrateur_public.

!!! Le modele lot_cnmip ne fonctionne pas, reste a mettre au point.

---------------------------- 

Cette modification/ajout a ete effectee pour les besoins du Workshop
'Model Intercomparison for agricultural GHG emissions' , Paris, March 19-21 

###############################################################################
# 14/03/14 : ajout A LA MAIN d'un code
             qui n'a pas ete livre sur la branche trunk !!!

Ajout uniquement dans demonstrateur_public.

Le code ajoute ici a la main a ete ecrit et verifie sur la branche en cours
de developpement des web services qui correspond a branches/020_wsdev.

--- Description/contenu de ce code :
CORRECTION de la methode 'def produireGraphique(self)' dans
record/utils/graphique.py : ax.legend( les_legendes, loc='best', ncol=1)

###############################################################################
# 14/03/14 : ajout A LA MAIN d'un code
             qui n'a pas ete livre sur la branche trunk !!!


        !!! LAISSER tel que ce code marque 'MARS20124' ? !!!


Ajout uniquement dans demonstrateur_public.

Le code ajoute ici a la main a ete ecrit et verifie sur la branche en cours
de developpement des web services qui correspond a branches/020_wsdev.

--- Description/contenu de ce code :

- Code marque 'MARS2014' : construction/enregistrement d'un EVENTUEL fichier
  de trace graphique supplementaire, dans lequel ajout de mean, max et min :
  - ajout de la methode 'def produireGraphiqueMore(self)'
    dans record/utils/graphique.py 
  - et ajout de son appel dans record/outil_web_record/res_graphique/views.py

---------------------------- 

Cette modification/ajout a ete effectee pour les besoins du Workshop
'Model Intercomparison for agricultural GHG emissions' , Paris, March 19-21 

###############################################################################

###############################################################################
# 25/02/14 : les operations effectuees sur VM webrecord
###############################################################################

Ajout du lot du modele LotkaVolterra qui vient d'etre ajoute au depot de modeles

###############################################################################
# 03/01/14 : les operations effectuees sur VM webrecord
###############################################################################

Livraison de recordweb (recordweb/trunk/recordweb) suite a une correction (fichier espace_exploitation.py) en cours d'essai/verification de la livraison precedente. Il s'agit de la revision 282 qui servira d'origine (puisqu'a priori ok) a la creation de la branche recordweb/branches/wsdev d'ajout/developpement des web services.
Seul demonstrateur_public est mis a jour selon recordweb livre ici (recordweb/trunk/recordweb).

Ensuite on travaillera/developpera la branche recordweb/branches/wsdev en laissant intact le code recordweb/trunk/recordweb correspondant a demonstrateur_public. demonstrateur_prive, dedie aux essais des web services, sera mis a jour selon la branche recordweb/branches/wsdev.

###############################################################################
# 02/01/14 : les operations effectuees sur VM webrecord
###############################################################################

Livraison de recordweb_vendor (pour ajout/installation de djangorestframework destine aux web services).

Livraison de recordweb (recordweb/trunk/recordweb). Il s'agit de la revision 278 qui servira d'origine a priori (si ok) a la creation de la branche recordweb/branches/wsdev d'ajout/developpement des web services.
Seul demonstrateur_public est mis a jour selon recordweb livre ici (recordweb/trunk/recordweb).

Ensuite on travaillera/developpera la branche recordweb/branches/wsdev en laissant intact le code recordweb/trunk/recordweb correspondant a demonstrateur_public. demonstrateur_prive, dedie aux essais des web services, sera mis a jour selon la branche recordweb/branches/wsdev.

###############################################################################
# 13/12/13 : les operations effectuees sur VM webrecord
###############################################################################

Livraison de recordweb_depot pour en recuperer lot_micmac  (le reste, ie les autres lots, laisse en l'etat).

** Rapport installation de lot_micmac sur MV webrecord :
- vle -P DynamicsStics test : 3 erreurs (erreur pour tests 13,14,15, pas d'erreur pour tests 1 a 12)
- Par contre tous les vpz de exp 'passent' (deroulement simulation jusqu'a la fin), mis a part les 2 *_spatial_*.vpz (vle -P DynamicsStics ...vpz)
- Tous les 6 vpz de MicMac-Design 'passent' (deroulement simulation jusqu'a la fin) (vle -P MicMac-Design ...vpz)

###############################################################################
# 29/10/2013 : les operations effectuees sur VM webrecord
###############################################################################

Livraison de recordweb (le reste est a jour).

Version avec ".encode('utf8')" pour permettre lecture (pas saisie) de caracteres speciaux (accents...) dans champs string (ne vaut pas pour champs composes : dict, list)

###############################################################################
# 11/09/2013 : les operations effectuees sur VM webrecord
###############################################################################

Livraison de recordweb (le reste est a jour).

Version ou :
- les champs de saisie des datas d'un bloc d'un scenario de simulation sont des textarea (=> facilite saisie et de plus 'Add' est maintenant OK pour les champs set et map),
- correction du bug selon lequel 'X' supprimait meme une unique valeur.

###############################################################################
# 18/01/2013 : les operations effectuees sur VM webrecord
###############################################################################

Livraison de recordweb (le reste est a jour).

###############################################################################
# 18/01/2013 : les operations effectuees sur VM webrecordtest
###############################################################################

Livraison de tout, notamment mise a jour de recordweb_vendor.

###############################################################################
# 03/01/2013 : les operations effectuees sur VM webrecord (VM webrecordtest indisponible)
###############################################################################

Livraison nouvelle bd_modeles_record (champs accord_exploitation, fonctionne_sous_linux, pour_installation, caracteristiques, url...), nouvelles fonctionnalites : choix/envoi d'un nouveau fichier vpz, d'un nouveau fichier de donnees, saisie donnee de type date, formulaires par categories...

Puis livraison de 'recordweb/record/outil_web_record/forms' tout seul (correction de l'erreur de traduction).

###############################################################################
# 16/11/2012 : les operations effectuees sur VM webrecordtest puis webrecord
###############################################################################

livraison de quelques modifications settings et install (generation doxygen, url maintenant que le nom de domaine webrecord existe...)

###############################################################################
# 13/11/2012 : les operations effectuees sur VM webrecord
###############################################################################

livraison du nouveau modele weed (personnalisation + code du test)
declare dans demonstrateur_prive et demonstrateur_public

###############################################################################
# 05/11/2012 : les operations effectuees sur VM webrecord
###############################################################################
Poursuite essai avec emploi de VLE_HOME (et non plus lien pkgs) : 
Reinstallation recordweb/record

Bilan de l'essai : a l'air de fonctionner

 Pour memo :
 - description ancienne 'anomalie' :
   usr1 --> _VLE_HOME__user__pid1
   usr2 --> _VLE_HOME__user__pid2
   usr1 choisit edite et simule modeleA :
        --> _VLE_HOME__user__pid1/pkgs/__simu_modeleA/exp,...,output
   usr2 choisit edite et simule modeleB
        --> _VLE_HOME__user__pid2/pkgs/__simu_modeleB/exp,...,output
   il est aussi cree le repertoire _VLE_HOME__user__pid1/pkgs/__simu_modeleB/output (vide)
   (cf 'scenario_actualisation', cf /var/log/apache2/error.log.anomalie5nov)
 - correction : une seule requete par processus (MaxRequestsPerChild=1)

###############################################################################
# 29/10/2012 : les operations effectuees sur VM webrecord
###############################################################################

Essai avec emploi de VLE_HOME (et non plus lien pkgs) : 
en cours d'install, maj default_rwsite_demonstrateur_prive (port)

Reinstallation recordweb

###############################################################################
# 26/10/2012 : les operations effectuees sur VM locale webrecordtest 
###############################################################################
livraison recordweb/record nouvelle version autour de VLE_HOME (cf initialiserControleur) ... ca a l'air de marcher... (mais concurrence non verifiee)
Reinstallation recordweb

###############################################################################
# 25/10/2012 : les operations effectuees sur VM locale webrecordtest 
###############################################################################
Essai avec emploi de VLE_HOME (et non plus lien pkgs) : not ok sur VM webrecordtest (... cf nb de processes ???)
Reinstallation recordweb

###############################################################################
# 19/10/2012 : les operations effectuees sur VM locale webrecordtest 
###############################################################################

Un essai a ete effectue d'installation du projet web nom_du_projet_web (ses recordweb, recordweb_depot et bd_modeles_record sous /opt/nom_du_projet_web) en suivant la procedure install_projet_web.txt : essai OK sous reserve d'avoir au prealable donne dans les fichiers 'default_...' une valeur correcte aux 3 ports d'installation (xxxx, yyyy et zzzz dans le code).

Puis installation OK demonstrateur_prive et demonstrateur_public, en modifiant valeur ports default_rwsite_demonstrateur_prive,_public (valeur definitive au lieu de 80 dans le code) :
- les recordweb, recordweb_depot et bd_modeles_record de/sous /opt/demonstrateur_prive
- les recordweb, recordweb_depot et bd_modeles_record de/sous /opt/demonstrateur_public

###############################################################################
# 16/10/2012 : les operations effectuees sur VM prod webrecord 
###############################################################################
Reinstallation recordweb (version avec demonstrateur_prive et demonstrateur_public)
Reinstallation bd_modeles_record (contient ModuloSTICS)

###############################################################################
# 16/10/2012 : les operations effectuees sur VM locale webrecord_test 
###############################################################################

Reinstallation PARTIELLE recordweb_depot :
livraison configurations_web, datas_recordweb et lot_stics, lot_weed (que compilation lot_stics et lot_weed, pas de recompilations des autres)

Reinstallation recordweb (version avec demonstrateur_prive et demonstrateur_public)

Reinstallation bd_modeles_record (contient ModuloSTICS)

Installation gfortran

###############################################################################
# 15/10/2012 : les operations effectuees sur VM prod webrecord 
###############################################################################

Reinstallation PARTIELLE recordweb_depot :
livraison configurations_web, datas_recordweb et lot_stics, lot_weed (que compilation lot_stics et lot_weed, pas de recompilations des autres)

Reinstallation bd_modeles_record (contient ModuloSTICS)

Installation gfortran

###############################################################################
# 26/09/2012 : les operations effectuees sur VM locale webrecord_test 
# ont ete reproduites sur VM prod webrecord 
# Les operations effectuees :
###############################################################################
(cf recordweb/specifique_serveur/NOTES.txt)

-------------------------------------------------------------------------------

Reinstallation recordweb

-------------------------------------------------------------------------------

Principalement alimentation des pages du site rwsite : document 'Finalites', .tar.gz exemple d'appel de R sous Python

*******************************************************************************
###############################################################################

###############################################################################
# 07/09/2012 : Essai sur VM locale webrecord_test 
# 11/09/2012 : les operations ont ete reproduites sur VM prod webrecord 
# Les operations effectuees :
###############################################################################
(cf recordweb/specifique_serveur/NOTES.txt)

-------------------------------------------------------------------------------

Reinstallation recordweb

-------------------------------------------------------------------------------

rwsite, urls.py : api/v2 (en plus de api/v1)

record/utils : configs/conf_trace.py pour permettre utilisations record/utils hors projets django et prise en compte dans code record/utils

(+ mineur : loc='best' au lieu de 0 dans graphique.py)

Maintenant que OK : change port 8080 de rwtool en 24002

ajout dans rwsite de representation_graphique.py

-------------------------------------------------------------------------------
###############################################################################

###############################################################################
# 06/09/2012 : modifications/maj mineures directement effectuees/livrees
# sur VM prod webrecord 
# Les operations effectuees :
###############################################################################
Reinstallation recordweb
###############################################################################

###############################################################################
# 31/08/2012 : les operations effectuees courant aout 2012 sur 
# VM locale webrecord_test ont ete reproduites sur VM prod webrecord 
# Les operations effectuees :
###############################################################################
(cf recordweb/specifique_serveur/NOTES.txt)

-------------------------------------------------------------------------------
Nouveau code source :

Reinstallation recordweb
Reinstallation recordweb_vendor

Pas reinstallation bd_modeles_record_directory # (!!!+ suppression azodyn ble)
Pas reinstallation recordweb_depot (... pas de recompilation des modeles record)

*******************************************************************************
###############################################################################

###############################################################################
# 31/08/2012 : Essais courants aout 2012 sur VM locale webrecord_test 
# Les operations effectuees :
###############################################################################
-------------------------------------------------------------------------------
31 aout 2012 - en cours sur VM locale webrecord_test :
essai derniere version de recordweb : OK ; avant mise en prod
-------------------------------------------------------------------------------
10 aout 2012 - en cours sur VM locale webrecord_test :
installation du rwsite a base de django-fiber
    ... /media/ : ok
    renommer les URLs dans rwsite/settings.py
    nettoyer django-transmeta (cf maintenant : python setup.py install)
-------------------------------------------------------------------------------
2 aout 2012 - effectue sur VM locale webrecord_test :
Essais dans Stock : faire tourner simultanement plusieurs projets django sur le serveur
2 aout 2012 - effectue sur VM locale webrecord_test, a ete pris en compte/propage dans code svn :
=> a ete pris en compte/propage dans code svn

Auparavant, sauvegarde version avant essais : Stock.miseEnProdDe120727
Jete : avant derniere version Stock.miseEnProdDe120720

*******************************************************************************
###############################################################################

###############################################################################
# 27/07/2012 : Essai sur VM locale webrecord_test 
# 27/07/2012 : les operations ont ete reproduites sur VM prod webrecord 
# Les operations effectuees :
###############################################################################
(cf recordweb/specifique_serveur/NOTES.txt)

-------------------------------------------------------------------------------
Nouveau code source :

Reinstallation recordweb
Reinstallation bd_modeles_record_directory # (!!!+ suppression azodyn ble)

Pas reinstallation recordweb_vendor
Pas reinstallation recordweb_depot (... pas de recompilation des modeles record)

*******************************************************************************
###############################################################################

###############################################################################
# 20/07/2012 : Essai sur VM locale webrecord_test 
# 20/07/2012 : les operations ont ete reproduites sur VM prod webrecord 
# Les operations effectuees :
###############################################################################
(cf recordweb/specifique_serveur/NOTES.txt)

-------------------------------------------------------------------------------
Nouveau code source :

Reinstallation recordweb
Reinstallation bd_modeles_record_directory # (!!!+ suppression azodyn ble)

Pas reinstallation recordweb_vendor
Pas reinstallation recordweb_depot (... pas de recompilation des modeles record)

*******************************************************************************
###############################################################################

###############################################################################
# 17/07/2012 : Essai sur VM locale webrecord_test 
# 19/07/2012 : les operations ont ete reproduites sur VM prod webrecord 
# Les operations effectuees :
###############################################################################
(cf recordweb/specifique_serveur/NOTES.txt)

-------------------------------------------------------------------------------
Nouvelles installations :

Installation graphviz (generation de graphes)
Installation django-request (statistics module for django)
Installation exim (serveur smtp, pour email errors)

-------------------------------------------------------------------------------
Nouveau code source :

Reinstallation recordweb
Reinstallation recordweb_vendor
Reinstallation bd_modeles_record_directory # (!!!+ suppression azodyn ble)

Pas reinstallation recordweb_depot (... pas de recompilation des modeles record)

*******************************************************************************
###############################################################################


