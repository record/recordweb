#-*- coding:utf-8 -*-

## @file record/bd_modeles_record/admin.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File bd_modeles_record/admin.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# Administration de la bd des modeles record
#
#*****************************************************************************

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from record.bd_modeles_record.models import Individu, MotDePasse, ModeleRecord
from record.bd_modeles_record.models import estMotDePassePublic, enregistrerMotDePassePublic

from record.bd_modeles_record.forms.admin_forms import ModeleRecordAdminForm

#*****************************************************************************
# Pour que certaines parties/rubriques administratives n'apparaissent pas
#*****************************************************************************
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.contrib.auth.models import Group
# admin.site.unregister(User) # partie User (en commentaire pour voir les comptes administrateur)
admin.site.unregister(Group)  # partie Group
admin.site.unregister(Site)   # partie Site

from django.utils.translation import ugettext as _


#*****************************************************************************
# Ajout/enregistrement du MotDePasse public s'il n'existe pas encore dans la
# bd des modeles record
#*****************************************************************************

if MotDePasse.objects.exists() :
    modele_public_absent = True # par defaut
    liste_mots_de_passe = MotDePasse.objects.all()
    for mot_de_passe in liste_mots_de_passe :
        if estMotDePassePublic( mot_de_passe ) :
            modele_public_absent = False
    if modele_public_absent :
        enregistrerMotDePassePublic()

##..
#*****************************************************************************\n
#
# Administration MotDePasse
#
#*****************************************************************************
class MotDePasseAdmin(UserAdmin):

    texte_description = _(u"Un mot de passe est associé à un ou plusieurs modèles privés. Les modèles publics ont pour mot de passe modele_public.")

    fieldsets = (

        ( _(u"MOT DE PASSE"), { 'description': texte_description,
                                'fields': () } ),

        ( None, { 'fields': ('username',
                             'password') } ),
    )

admin.site.register(MotDePasse, MotDePasseAdmin)

##..
#*****************************************************************************\n
#
# Administration Individu
#
#*****************************************************************************
class IndividuAdmin(UserAdmin):

    texte_description = _(u"Un individu est une personne susceptible d'endosser le rôle de responsable et/ou responsable scientifique et/ou responsable informatique par rapport à un ou plusieurs modèles")

    fieldsets = (

        ( _(u"INDIVIDU"), { 'description': texte_description,
                            'fields': () } ),

        ( None, { 'fields': ('username',
                             'first_name',
                             'last_name',
                             'email') } ),
    )

admin.site.register(Individu, IndividuAdmin)

##..
#*****************************************************************************\n
#
# Administration ModeleRecord
#
#*****************************************************************************
class ModeleRecordAdmin(admin.ModelAdmin):

    def show_url(self, obj):
        return '<a href="%s">%s</a>' % (obj.url, obj.url)
    show_url.allow_tags = True
    show_url.short_description = "lien"

    form = ModeleRecordAdminForm

    #list_display = ('nom', 'nom_pkg' )
    list_display = ('nom', 'nom_pkg', 'show_url' )

    texte_description = _(u"Il s'agit des modèles de la plate-forme Record qui sont mis à disposition au sein de l'outil Web Record. Un modèle est caractérisé par les paquets vle qui le constituent et un certain nombre d'autres renseignements nécessaires à son exploitation. Il peut être en accès privé ou public.")

    fieldsets = (

        ( _(u"MODELE RECORD"), { 'description': texte_description,
                            'fields': () } ),

        ( _(u"IDENTITE"), { #'description': _(u"Identité"),
                            'fields': (
                                'nom_fr',
                                'nom_en',
                                'description_fr',
                                'description_en',
                                'url',
                            ) } ),

        ( _(u"RESPONSABLES"), { 'description': _(u"Les 3 responsables du modèle sont des 'Individus' de la base."),
                            'fields': (
                                'responsable',
                                'responsable_scientifique',
                                'responsable_informatique',
                            ) } ),

        ( _(u"LIVRAISON OUTIL WEB"), { 'description': _(u"Informations concernant l'étape de livraison du modèle pour l'outil Web (renseignements sur l'état dans lequel le modèle est livré, sa procédure d'installation...)"),
                            'fields': (
                                'cr_reception_fr',
                                'cr_reception_en',
                                'fonctionne_sous_linux',
                                'sans_trace_ecran',
                                'pour_installation_fr',
                                'pour_installation_en',
                                'accord_exploitation',
                            ) } ),
    
        ( _(u"ASPECT VLE (PAQUETS...)"), {
                            'description': _(u"Eléments d'identification du modèle dans le dépôt de modèles de l'outil Web Record : aspect VLE (paquets...), fichiers de données. Un modèle est caractérisé par son lot de paquets vle (l'ensemble des paquets vle requis pour son fonctionnement : paquets de dépendance et paquet principal) et son paquet principal (contenant ses fichiers vpz scénario de simulation) dont le répertoire des données ('data') peut être remplacé par un répertoire spécifique."),
                            'fields': (
                                'nom_repertoire_lot_pkgs',
                                'nom_pkg',
                                'nom_repertoire_data',
                                'version_vle',
                            ) } ),

        ( _(u"CONFIGURATION PERSONNALISATION (optionnel)"), {
                            'description': _(u"Eléments d'identification du modèle dans le dépôt de modèles de l'outil Web Record : aspect configuration/personnalisation (optionnel). Il est possible de personnaliser la présentation du modèle (ses scénarios et leurs données) au sein de l'outil Web Record. Actuellement cette personnalisation est effectuée au moyen de 3 fichiers vpz de configuration."),
                            'fields': (
                                'nom_fichier_config_web_conf',
                                'nom_fichier_config_web_applis',
                                'nom_fichier_config_dicos',
                            ) } ),

        ( _(u"CONDITIONS LIVRAISON OUTIL WEB"), {
            'description': _(u"Conditions de dépôt/livraison du modèle dans l'outil Web : choix de le rendre public ou privé (le mot de passe du modèle est un 'Mot de passe' de la base), de l'activer ou non, détermination d'un certain nombre de caractéristiques qui pourront être utilisées pour présenter les modèles par groupes/rubriques)"),
                            'fields': (
                                'mot_de_passe',
                                'modele_active',
                                'caracteristique1',
                                'caracteristique2',
                                'caracteristique3',
                                'caracteristique4',
                                'caracteristique5',
                                'caracteristique6',
                                'caracteristique7',
                                'caracteristique8',
                                'caracteristique9',
                                'caracteristique10',
                                'caracteristique11',
                                'caracteristique12',
                                'caracteristique13',
                                'caracteristique14',
                                'caracteristique15',
                                'caracteristique16',
                            ) } ),
    )

    list_filter = [ 'nom_fr',
                    'nom_en',
                    'nom_pkg',
                    'version_vle',
                    'responsable',
                    'responsable_scientifique',
                    'responsable_informatique',
                    'mot_de_passe' ]

    search_fields = [ 'nom_fr',
                    'nom_en',
                    'nom_pkg',
                    'version_vle',
                    'responsable',
                    'responsable_scientifique',
                    'responsable_informatique' ]

admin.site.register(ModeleRecord, ModeleRecordAdmin)

#*****************************************************************************

