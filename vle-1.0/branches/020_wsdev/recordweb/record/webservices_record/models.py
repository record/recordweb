from django.db import models

#from utils import WS_LesModeles, WS_GlobalContext, WS_ScnContent, WS_SimuResult
#from utils import WS_GlobalContext, WS_ScnContent

from record.bd_modeles_record.models import ModeleRecord as ModeleRecordBd
from record.bd_modeles_record.models import MotDePasse, Individu

from record.outil_web_record.models.modele_record.modele_record import ModeleRecordOn
from record.outil_web_record.models.conf_web.conf_web_gestion import get_conf_web_modele_record
import os

from models_ws.help_texts import help_texts

from django.utils.translation import ugettext as _ # !!!


#******************************************************************************
# ESSAI EN COURS !!!!!!!!!

from pygments.lexers import get_all_lexers
from pygments.styles import get_all_styles
from pygments.lexers import get_lexer_by_name
from pygments.formatters.html import HtmlFormatter
from pygments import highlight
LEXERS = [item for item in get_all_lexers() if item[1]]
LANGUAGE_CHOICES = sorted([(item[1][0], item[0]) for item in LEXERS])
STYLE_CHOICES = sorted((item, item) for item in get_all_styles())

class SSnippet(models.Model):
    created  = models.DateTimeField(auto_now_add=True)
    title    = models.CharField(max_length=100, blank=True, default='')
    code     = models.TextField()
    linenos  = models.BooleanField(default=False)
    language = models.CharField(choices=LANGUAGE_CHOICES,
                                default='python',
                                max_length=100)
    style    = models.CharField(choices=STYLE_CHOICES,
                                default='friendly',
                                max_length=100)
    owner = models.ForeignKey('auth.User', related_name='snippets')
    highlighted = models.TextField()

    def save(self, *args, **kwargs):
        # Use the `pygments` library to create a highlighted HTML
        # representation of the code snippet.
        lexer = get_lexer_by_name(self.language)
        linenos = self.linenos and 'table' or False
        options = self.title and {'title': self.title} or {}
        formatter = HtmlFormatter(style=self.style, linenos=linenos,
                                  full=True, **options)
        self.highlighted = highlight(self.code, lexer, formatter)
        super(SSnippet, self).save(*args, **kwargs)

    class Meta:
        ordering = ('created',)

#******************************************************************************


#******************************************************************************
# Help
#******************************************************************************

class HelpText(models.Model):

    subject = models.CharField( max_length=100, blank=False, default=None )
    
    class Meta:
        pass

    def __unicode__(self):
        return '%s' % (self.subject)

    @classmethod
    def buildAllHelpText( cls ) :
        for subject,text in help_texts.iteritems() :
            help_text = HelpText( subject=subject )
            help_text.save()
            for line_text in text :
                help_text.addLine( line_text )
            help_text.save()

    def addLine( self, line_text ) :
        line = Line( line=line_text )
        line.save()
        line.helptexts.add( self )

    @classmethod
    def forceBuildAllHelpText( cls ) :
        Line.objects.all().delete()
        HelpText.objects.all().delete()
        cls.buildAllHelpText()

    def hasSubject( self, subject ) :
        return ( self.subject == subject )

    @classmethod
    def existsSubject( cls, subject ) :
        liste = HelpText.objects.all()
        for o in liste :
            if o.hasSubject( subject ) :
                return True
        return False

class Line(models.Model):

    line = models.TextField( blank=False, default=None )
    helptexts = models.ManyToManyField( HelpText, related_name='content' )

    def __unicode__(self):
        return self.line

    class Meta :
        pass

#******************************************************************************
# Model and its scenarios list (in order to choose one of them)
#
# application_associee :
# For the moment the applications_associees list is not proposed/treated. !!!
# Yet the application_associee is used as a filter !!!
#
#******************************************************************************
class WsModel(models.Model):

    #*************************************************************************
    # champs calques sur ceux de class ModeleRecord(models.Model)

    nom = models.CharField( max_length=10000, blank=True )
    description = models.TextField( blank=True )
    modele_record_id = models.IntegerField( blank=True )
    #url = models.CharField( blank=True )
    nom_repertoire_lot_pkgs = models.CharField( max_length=10000, blank=True )
    nom_pkg = models.CharField( max_length=10000, blank=True )
    nom_repertoire_data = models.CharField( max_length=10000, blank=True )

    #mot_de_passe = models.ForeignKey( MotDePasse )
    #mot_de_passe.null = True
    #mot_de_passe.blank = False # obligatoire (mot_de_passe 'modele_public' pour un modele public) (normalement obligatoire)

    modele_active = models.CharField( max_length=10000, blank=True )
    #nom_fichier_config_web_conf = models.CharField( max_length=10000, blank=True )
    #nom_fichier_config_web_applis = models.CharField( max_length=10000, blank=True )
    #nom_fichier_config_dicos = models.CharField( max_length=10000, blank=True )

    #responsable = models.ForeignKey(Individu, related_name=_(u"responsable") )
    responsable = models.ForeignKey(Individu, related_name='responsabilities' ) # IndividuOn shuntee
    responsable.null = True
    responsable.blank = False # obligatoire (normalement obligatoire)

    #responsable_scientifique = models.ForeignKey(Individu, related_name=_(u"responsable_scientifique") )
    responsable_scientifique = models.ForeignKey(Individu, related_name='scientific_responsabilities' ) # IndividuOn shuntee
    responsable_scientifique.null = True 
    responsable_scientifique.blank = False # obligatoire (normalement obligatoire)

    #responsable_informatique = models.ForeignKey(Individu, related_name=_(u"responsable_informatique") )
    responsable_informatique = models.ForeignKey(Individu, related_name='software_responsabilities' ) # IndividuOn shuntee
    responsable_informatique.null = True 
    responsable_informatique.blank = False # obligatoire (normalement obligatoire)

    cr_reception = models.TextField( blank=True )
    fonctionne_sous_linux = models.CharField( max_length=10000, blank=True )
    sans_trace_ecran = models.CharField( max_length=10000, blank=True )
    pour_installation = models.TextField( blank=True )
    accord_exploitation = models.CharField( max_length=10000, blank=True )
    version_vle = models.CharField( max_length=10000, blank=True )

    #*************************************************************************

    # scenarios

    def init(self, scncontent, *args, **kwargs):
        pass


    # construction des WsModele a partir des ModeleRecordBd existant en bd
    # et enregistrement en bd
    @classmethod
    def buildAll( cls ):

#       lesmodeles = dict()
#
#       liste_modeles_record_bd = ModeleRecordBd.objects.all()
#       for modele_record_bd in liste_modeles_record_bd :
#
#           le_modele_record = ModeleRecordOn( modele_record_bd )
#           if le_modele_record.estExploitable() :
#
#               idmod = le_modele_record.id
#               lemodele = dict()
#               lemodele[ "nompkg" ] = le_modele_record.nom_pkg 
#               lemodele[ "nommodelerecord" ] = le_modele_record.nom
#               lemodele[ "nomrepertoiredata" ] = le_modele_record.nom_repertoire_data
#
#               scenarios = dict()
#               for idscn,scenario_on in enumerate(le_modele_record.scenarios_on) :
#                   lescn = dict()
#                   lescn[ "nomscenarioavecvpz" ] = scenario_on.infos_scenario_form.nom_scenario_avec_vpz 
#                   lescn[ "titrescenario" ] = scenario_on.infos_scenario_form.titre_scenario
#                   scenarios[ idscn ] = lescn
#               lemodele[ "scenarios" ] = scenarios
#
#               lesmodeles[ idmod ] = lemodele
#
#           else : # modele inexploitable ignore
#               report.append( _(u"unavailable model ignored") )
#
#       data = lesmodeles
#
#       #########################

        liste_modeles_record_bd = ModeleRecordBd.objects.all()
        for modele_record_bd in liste_modeles_record_bd :

            le_modele_record = ModeleRecordOn( modele_record_bd )
            if le_modele_record.estExploitable() :

                nom = le_modele_record.nom 
                description = le_modele_record.description
                modele_record_id = le_modele_record.id
                nom_repertoire_lot_pkgs = le_modele_record.nom_repertoire_lot_pkgs
                nom_pkg = le_modele_record.nom_pkg
                nom_repertoire_data = le_modele_record.nom_repertoire_data
                modele_active = le_modele_record.modele_active 
                nom_fichier_config_web_conf = le_modele_record.nom_fichier_config_web_conf 
                nom_fichier_config_web_applis = le_modele_record.nom_fichier_config_web_applis 
                nom_fichier_config_dicos = le_modele_record.nom_fichier_config_dicos 
                cr_reception = le_modele_record.cr_reception
                fonctionne_sous_linux = le_modele_record.fonctionne_sous_linux
                sans_trace_ecran = le_modele_record.sans_trace_ecran
                pour_installation = le_modele_record.pour_installation
                accord_exploitation = le_modele_record.accord_exploitation
                version_vle = le_modele_record.version_vle
                wsmodel = WsModel( nom=nom,
                                 description=description,
                                 modele_record_id=modele_record_id,
                                 nom_repertoire_lot_pkgs=nom_repertoire_lot_pkgs,
                                 nom_pkg=nom_pkg,
                                 nom_repertoire_data=nom_repertoire_data,
                                 modele_active=modele_active,
                                 #nom_fichier_config_web_conf=nom_fichier_config_web_conf ,
                                 #nom_fichier_config_web_applis=nom_fichier_config_web_applis ,
                                 #nom_fichier_config_dicos=nom_fichier_config_dicos ,
                                 cr_reception=cr_reception,
                                 fonctionne_sous_linux=fonctionne_sous_linux,
                                 sans_trace_ecran=sans_trace_ecran,
                                 pour_installation=pour_installation,
                                 accord_exploitation=accord_exploitation,
                                 version_vle=version_vle )
                wsmodel.save()

# !!!
#               self.modele_record_bd = modele_record_bd ???
#               self.le_modele_record = le_modele_record ???
#
#               self.responsable = le_modele_record.responsable
#                 # = IndividuOn( modele_record_bd.responsable )
#               self.responsable_scientifique = le_modele_record.responsable_scientifique
#                 # = IndividuOn( modele_record_bd.responsable_scientifique )
#               self.responsable_informatique = le_modele_record.responsable_informatique
#                 # = IndividuOn( modele_record_bd.responsable_informatique )
                responsable = Individu.objects.get( id=le_modele_record.responsable.id )
                wsmodel.responsable = responsable
                responsable_scientifique = Individu.objects.get( id=le_modele_record.responsable_scientifique.id )
                wsmodel.responsable_scientifique = responsable_scientifique
                responsable_informatique = Individu.objects.get( id=le_modele_record.responsable_informatique.id )
                wsmodel.responsable_informatique = responsable_informatique
                wsmodel.save()
#   
#               # liste des ScenarioOn, scenarios (fichiers vpz) disponibles du
#               # modele/paquet (non pas ceux presents mais ceux issus/filtres
#               # de/par la configuration web)
#
#               # lecture de la configuration web
                rep_absolu_pkg = os.path.join( le_modele_record.nom_repertoire_lot_pkgs, le_modele_record.nom_pkg )
                conf_web_modele_record = get_conf_web_modele_record( rep_absolu_pkg, le_modele_record.nom_fichier_config_web_conf, le_modele_record.nom_fichier_config_web_applis, le_modele_record.nom_fichier_config_dicos )
#
#               scenarios_on = []
#               for nom_scenario_on in conf_web_modele_record.noms_scenarios :
                for nom_scenario in conf_web_modele_record.noms_scenarios :
                    wsscenario = WsModelScenario( nom_scenario=nom_scenario, wsmodel=wsmodel )
                    wsscenario.save()
#                   infos_generales_scenario = conf_web_modele_record.infos_generales_scenarios[ nom_scenario_on ]
#                   applications_associees = conf_web_modele_record.applications_web_associees[ nom_scenario_on ]
#                   scenario_on = ScenarioOn( nom_scenario_on, infos_generales_scenario, applications_associees )
#                   scenarios_on.append( scenario_on )
#               self.scenarios_on = scenarios_on
#               self.conf_web_modele_record = conf_web_modele_record
            wsmodel.save()

class WsModelScenario(models.Model):

    nom_scenario = models.CharField( max_length=10000, blank=False )
    # ...etc !!!

    wsmodel = models.ForeignKey( WsModel, related_name='wsmodelscenarios')

    def __unicode__(self):
        return self.nom_scenario

    class Meta :
        pass


#******************************************************************************
# Documentation - metadonnees
#******************************************************************************

# !!!DOC
#class WsDoc(models.Model):
    # iii pass

class WsDocParameter(models.Model):

    typevalue = models.CharField( max_length=100 )
    unit = models.CharField( max_length=100 )
    max = models.CharField( max_length=100 )
    min = models.CharField( max_length=100 )
    recommanded = models.CharField( max_length=100 )
                        
#   def __unicode__(self):
#       return u"unit:%s, max value:%s, min value:%s, recommanded value:%s, type value:%s" % ( self.unit, self.max, self.min, self.recommanded, self.typevalue )

#   class Meta :
#       pass

#******************************************************************************
# Scenario
#******************************************************************************

# scenario 
class WsScenario(models.Model):

    experimentname_isactive = models.BooleanField(default=False)
    experimentname = models.CharField(max_length=100, blank=True, default='VVV DE MODELS')

    duration_isactive = models.BooleanField(default=False)
    duration = models.FloatField( blank=True )

    seed_isactive = models.BooleanField(default=False)
    seed = models.FloatField( blank=True )

    replicaseed_isactive = models.BooleanField(default=False)
    replicaseed = models.FloatField( blank=True )

    replicanumber_isactive = models.BooleanField(default=False)
    replicanumber = models.FloatField( blank=True )

    begin_isactive = models.BooleanField(default=False)
    begin = models.CharField( max_length=100, blank=True ) # !!! time, date

    # parameters

    parameterslist = models.TextField( blank=True ) # dediee saisie
    
    #nnn cr_ok = models.BooleanField( )
    #nnn report = models.TextField( )
    #nnn help = models.TextField( )
    #nnn is_public_model = models.BooleanField( )
    #nnn vle_home = models.CharField( max_length=100 )
    #nnn idmod = models.IntegerField( )
    #nnn idscn = models.IntegerField( )

    #def __init__(self, scncontent=None, *args, **kwargs):
    #    super(WsScenario, self).__init__(*args, **kwargs)
    #    if scncontent is not None :
    #        self.init( scncontent=scncontent )


    def init(self, scncontent, *args, **kwargs):
        print "\nWsScenario.init(scncontent)"

        experimentname_isactive = False
        duration_isactive = False
        seed_isactive = False
        replicaseed_isactive = False
        replicanumber_isactive = False
        begin_isactive  = False

        experimentname = "VVV init WsScenario"
        duration = 999.9
        seed = 999.9
        replicaseed = 999.9
        replicanumber = 999.9
        begin = "VVV init WsScenario"

        if scncontent is not None :
            if scncontent.cr_ok :
                keys = scncontent.data.keys() 

                if 'experimentname' in keys :
                    experimentname_isactive = True
                    experimentname = scncontent.data[ 'experimentname' ]
                if 'duration' in keys :
                    duration_isactive = True
                    duration = scncontent.data[ 'duration' ]
                if 'seed' in keys :
                    seed_isactive = True
                    seed = scncontent.data[ 'seed' ]
                if 'replicaseed' in keys :
                    replicaseed_isactive = True
                    replicaseed = scncontent.data[ 'replicaseed' ]
                if 'replicanumber' in keys :
                    replicanumber_isactive = True
                    replicanumber = scncontent.data[ 'replicanumber' ]
                if 'begin' in keys :
                    begin_isactive = True
                    begin = scncontent.data[ 'begin' ]

        self.experimentname_isactive = experimentname_isactive
        self.experimentname = experimentname
        self.duration_isactive = duration_isactive
        self.duration = duration
        self.seed_isactive = seed_isactive
        self.seed = seed
        self.replicaseed_isactive = replicaseed_isactive
        self.replicaseed = replicaseed
        self.replicanumber_isactive = replicanumber_isactive
        self.replicanumber = replicanumber
        self.begin_isactive = begin_isactive
        self.begin = begin

        self.parameterslist = []

    def addParameters( self, scncontent ):
        if scncontent is not None :
            if scncontent.cr_ok :

                metadatas = None
                if 'metadata' in scncontent.data.keys() :
                    metadatas = scncontent.data[ 'metadata' ]
                #print "\nWsScenario.addParameters(scncontent), metadatas : ", metadatas # !!!

                if 'parameters' in scncontent.data.keys() :
                    parameters = scncontent.data[ 'parameters' ]
                    for (cond,e) in parameters.iteritems() :
                        for (port,v) in e.iteritems() :

                            # creation metadata mal placee !!!
                            metadata = metadatas[cond][port] # +? if not exists...
                            typevalue = metadata['typevalue']
                            unit = metadata['unit']
                            max = metadata['max']
                            min = metadata['min']
                            recommanded = metadata['recommanded']
                            doc = WsDocParameter( typevalue=typevalue, unit=unit, max=max, min=min, recommanded=recommanded )
                            doc.save()

                            parameter = WsParameter( condname=cond, portname=port, values=v, scenario=self, metadata=doc )
                            parameter.save()
                            self.parameterslist.append( { "condname":parameter.condname,
                                                          "portname":parameter.portname,
                                                          "values":parameter.values } )

    def get_fields_actifs(self): 

        def mayadd( v, isactive, fieldnames) :
            if isactive :
                fieldnames = fieldnames + ( v, )
            return fieldnames

        fieldnames = tuple()

        # considering '_isactive' value
        fieldnames = mayadd( 'experimentname', self.experimentname_isactive, fieldnames )
        fieldnames = mayadd( 'duration', self.duration_isactive, fieldnames )
        fieldnames = mayadd( 'seed', self.seed_isactive, fieldnames )
        fieldnames = mayadd( 'replicaseed', self.replicaseed_isactive, fieldnames )
        fieldnames = mayadd( 'replicanumber', self.replicanumber_isactive, fieldnames )
        fieldnames = mayadd( 'begin', self.begin_isactive, fieldnames )

        # doesn't include 'parameters', 'parameterslist'
        # doesn't include 'result'

        return fieldnames

    def get_fields_shown(self, fieldnames={}):
        # selects the fields of fieldnames that exist as self attributes
        fields_shown = list()
        for fieldname in fieldnames :
            if getattr(self, fieldname, None) is not None :
                fields_shown.append( fieldname )
        return tuple(fields_shown)

    def __unicode__(self):
        return u"Scenario id : %s, experimentname : %s (isactive:%s), duration : %f (isactive:%s), seed : %f (isactive:%s), replicaseed : %f (isactive:%s), replicanumber : %f (isactive:%s), begin : %s (isactive:%s)" % ( self.id, self.experimentname, self.experimentname_isactive, self.duration, self.duration_isactive, self.seed, self.seed_isactive, self.replicaseed, self.replicaseed_isactive, self.replicanumber, self.replicanumber_isactive, self.begin, self.begin_isactive )

    class Meta:
        pass

class WsParameter(models.Model):

    condname = models.CharField( max_length=100 )
    portname = models.CharField( max_length=100 )
    values = models.CharField( max_length=100 )
    scenario = models.ForeignKey( WsScenario, related_name='parameters')
    metadata = models.ForeignKey( WsDocParameter, related_name='datas') # !!! relation ne va pas rester de ce type

    def __unicode__(self):
        #return u"Parameter id %s, condname %s, portname %s, values %s" % ( self.id, self.condname, self.portname, self.values )
        return u"condname:'%s', portname:'%s', values:'%s'" % ( self.condname, self.portname, self.values )

    class Meta:
        pass


#******************************************************************************
# Resultat de simulation
#******************************************************************************

class WsResult(models.Model):

    # resultat
    result = models.TextField( blank=True, default=None )

    # scenario (sera affiche/restitue ou non selon cas/demandes)
    scenario = models.ForeignKey( WsScenario, related_name='result') # !!! relation ne va pas rester comme ca

    #nnn cr_ok = models.BooleanField( blank=True, default=None )
    #nnn report = models.TextField( blank=True, default=None )
    #nnn help = models.TextField( blank=True, default=None )

    #def __init__(self, *args, **kwargs):
    #    super(WsResult, self).__init__(*args, **kwargs)

    def init(self, simuresult ):
        result = None # default 
        if simuresult is not None :
            if simuresult.cr_ok :
                result = simuresult.data
        self.result = result

    def __unicode__(self):
        return u"Result id : %s, result : %s" % ( self.id, self.result )

    class Meta:
        pass

#******************************************************************************



##############################################################################





#******************************************************************************
#
#    Ce qui suit est ancien, pour des/en tant qu'exemple (a jeter a priori)
#
#******************************************************************************


#******************************************************************************
# des extraits de l'ancienne classe
class vvvScenario(models.Model):

    #created  = models.DateTimeField(auto_now_add=True)
    #title    = models.CharField( max_length=100, blank=True, default=None )
    # a jeter !!!
    #code     = models.TextField(default="bla bla bla")
    #linenos  = models.BooleanField(default=False)
    #language = models.CharField(choices=LANGUAGE_CHOICES,
    #                            default='python',
    #                            max_length=100)

    experimentname = models.CharField( max_length=100 )
    duration = models.FloatField( )

    cr_ok = models.BooleanField( )
    report = models.TextField( )
    help = models.TextField( )
    #nnn is_public_model = models.BooleanField( )
    #nnn vle_home = models.CharField( max_length=100 )

    #nnn idmod = models.IntegerField( )
    #nnn idscn = models.IntegerField( )

#   def __init__(self, scncontent, *args, **kwargs):
#       super(vvvScenario, self).__init__(*args, **kwargs)

    def init(self, scncontent, *args, **kwargs):

        # default values
        self.cr_ok = None
        self.report = None
        self.help = None

        self.experimentname = None
        self.duration = None

        if scncontent is not None :

            cr_ok = scncontent.cr_ok
            report = scncontent.report
            help = scncontent.help

            # data
            if scncontent.cr_ok :

                keys = scncontent.data.keys() 

                if 'experimentname' in keys :
                    experimentname = scncontent.data[ 'experimentname' ]
                else : # !!! a revoir
                    experimentname = "a revoir !!!!!!"

                if 'duration' in keys :
                    duration = scncontent.data[ 'duration' ]
                else : # !!! a revoir
                    duration = 3333

        self.cr_ok = cr_ok 
        self.report = report
        self.help = help
        #nnn self.is_public_model = ws_globalcontext.is_public_model
        #nnn self.vle_home = vle_home
        #nnn self.idmod = idmod
        #nnn self.idscn = idscn

        if experimentname is not None :
            self.experimentname = experimentname 
        if duration is not None :
            self.duration = duration 


#******************************************************************************
# Content a mettre en place ?

# champs simples !!!
class Content(models.Model):

    # pour l'instant champs simples !!! a modifier ...
    data = models.CharField(max_length=100, blank=True, default='VALEUR DATA PAR DEFAUT')
    #data = vvvScenario()
    #!!! en cours
    #data = models.ForeignKey(vvvScenario)

    report = models.TextField()
    help = models.TextField()

#******************************************************************************
#
#from pygments.lexers import get_all_lexers
#from pygments.styles import get_all_styles
#LEXERS = [item for item in get_all_lexers() if item[1]]
#LANGUAGE_CHOICES = sorted([(item[1][0], item[0]) for item in LEXERS])
#STYLE_CHOICES = sorted((item, item) for item in get_all_styles())
#class Snippet(models.Model):
#    created  = models.DateTimeField(auto_now_add=True)
#    title    = models.CharField(max_length=100, blank=True, default='')
#    code     = models.TextField()
#    linenos  = models.BooleanField(default=False)
#    language = models.CharField(choices=LANGUAGE_CHOICES,
#                                default='python',
#                                max_length=100)
#    style    = models.CharField(choices=STYLE_CHOICES,
#                                default='friendly',
#                                max_length=100)
#    class Meta:
#        ordering = ('created',)
#
#******************************************************************************

#   hasattr(object, name)

#   delattr(object, name)
#   delattr(x, 'foobar') is equivalent to del x.foobar
#       delattr(self, 'experimentname')

#   setattr(object, name, value)
#   setattr(x, 'foobar', 123) is equivalent to x.foobar = 123.

#******************************************************************************

##############################################################################

