#-*- coding:utf-8 -*-


from django.utils.translation import ugettext as _


# key subject, value liste des lignes du texte
help_texts = {
        
'Mod' : [
    _(u"** The 'Mod' get command shows all the models.") ,
    _(u"** Choice of one model and one of its scenarios : ") ,
    " - " + _(u"The XXX commands relative to one specific scenario of one specific model will look like 'XXX/idmod/idscn'."),
    " - " + _(u"idmod is the model 'id', given in the models list, reminded for each scenario in 'wsmodel'."),
    " - " + _(u"idscn is the scenario 'id', given in the 'wsmodelscenarios' list."),
    _(u"** Warnings : if the model is private, it won't be possible to see its content, neither to simulate it.") ,
    ],

# ScnDoc/idmod/idscn : returns the complete information of the scenario
# No post.
# idmod, idscn correspond with WsModel, WsModelScenario ; see Mod to know them.

# ScnDoc/idmod/idscn : returns the complete information of the scenario
# identified by (idmod,idscn), in its original state. The information
# contains the documentation (see 'metadata').
# No post.
# idmod, idscn correspond with WsModel, WsModelScenario ; see Mod to know them.


# identified by (idmod,idscn), in its original state. The information
# contains the documentation (see 'metadata').
# No post.
# idmod, idscn correspond with WsModel, WsModelScenario ; see Mod to know them.

'ScnFull' : [
    _(u"The 'ScnFull/idmod/idscn' get command returns the complete information (including documentation) of the scenario identified by idmod and idscn, in its original state." ),
    ],

'id' : [
    _(u"** Model and scenario identification :") ,
    _(u"** The selected model and its selected scenario are identified by idmod and idscn. To know idmod and idscn, see 'Mod'.") ,
    ],

'help_menu_scn' : [
    _(u"VA BOUGER !!!") ,
    _(u"** Experiment menu :") ,
    _(u"GET Scn/idmod/idscn : returns the model scenario content") ,
    " - " + _(u"POST Scn/idmod/idscn or POST Scn with keys { 'idmod':value, 'idvpz':value } :") ,
    "   " + _(u"returns the model scenario content once modified according to the optional keys from a scenario content.") ,
    ],

'help_menu_run' : [
    _(u"VA BOUGER !!!") ,
    _(u"** Simulation menu :") ,
    " - " + _(u"GET Res/idmod/idscn : returns the results of the scenario simulation") ,
    " - " + _(u"POST Res/idmod/idscn or POST Res with keys { 'idmod':value, 'idvpz':value } : ") ,
    "   " + _(u"returns the results of the scenario simulation once modified according to the optional keys from a scenario content ('duration', 'begin', 'parameters'...).") ,
    "   " + _(u"optional key 'simumode': value 'multi' for a multisimulation, value 'mono' for a monosimulation.") ,
    "   " + _(u"optional key 'returnscncontent': value True to receive the content of the simulated scenario, value False not to.") ,
    ],



'Nohelp' : [
    _(u"No help for this subject") ,
    ],
        
}

