#-*- coding:utf-8 -*-

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse
 
from utils import WS_GlobalContext, WS_ScnContent, WS_SimuResult
from utils import OLD___WS_LesModeles

#!!! from models import Content 
#!!! from serializers import ContentSerializer
from models import HelpText
from serializers import HelpTextSerializer

from django.contrib.auth.models import User
from serializers import UserSerializer

from record.bd_modeles_record.models import Individu
from serializers import IndividuSerializerLight, IndividuSerializerFull

from models import WsModel
from serializers import ModelSerializerLight, ModelSerializerFull
from serializers import DbModelSerializerLight, DbModelSerializerFull

from models import WsModelScenario
from serializers import ModelScenarioSerializerLight, ModelScenarioSerializerFull

from models import WsScenario
from serializers import ScenarioSerializerFull, ScenarioSerializerLight
from serializers import ScenarioSerializer_Form, ScenarioSerializer_NoForm
#from models import WsParameter
from models import WsResult

from record.outil_web_record.utils.commun_views import initialiserControleur

from record.utils import session
 
from django.utils.translation import ugettext as _

import os

#******************************************************************************
#
#                                   Menus
#
#******************************************************************************

#------------------------------------------------------------------------------

@api_view(('GET',))
def home( request, format=None ):
    '''Main menu relative to the models that are stored into the database : \n
    - 'Models' to see the models information, \n
    - 'Simulation' to simulate models (get the results of one simulation) '''

    return Response({
        'Models        ': reverse('models', request=request, format=format),
        'Simulation    ': reverse('simulation', request=request, format=format),
        'Administration': reverse('administration', request=request, format=format),
        'Others tmp    ': reverse('menu_others', request=request, format=format),
    })

#------------------------------------------------------------------------------

@api_view(('GET',))
def models( request, format=None ):
    '''List of the models that are stored into the database'''
    return Response({
        'Models list ': reverse('dbmodel-list', request=request, format=format),
    })

#------------------------------------------------------------------------------

@api_view(('GET',))
def simulation( request, format=None ):
    '''
    - 'Models list' : to browse through the models and their scenarios. To know the id of each model scenario. \n
    The first step is to identify which scenario of which model is going to be simulated. The model scenario is identified by its id. Once identified the model scenario id : \n
    - 'Scn/id/' to edit the scenario content.
    - 'ScnFull/id/' for full content.
    - 'Res/id/' to get results of the scenario simulation.
    - 'ResFull/id' for the scenario content in addition.
    '''

    return Response({
        'Models list': reverse('wsmodel-list', request=request, format=format),
    })

#------------------------------------------------------------------------------

@api_view(('GET',))
def administration( request, format=None ):
    return Response({

        'To re-build the models list': [ reverse('ws-restart', request=request, format=format),

            "Re-build and save into the database the models (WsModel) that will be taken into account",
            "development notes : must be reserved to admin",
        ],

        'Accounts': [ reverse('user-list', request=request, format=format),
            "Returns the accounts.",
            "An account corresponds with a person (model owner, developper...) or is dedicated to limit the access to some models.",
            "development notes : must be reserved to admin",
        ],

        'Individus': [ reverse('individu-list', request=request, format=format),
            "Returns the individus who are persons assuming one or several roles (responsable, responsable_scientifique, responsable_informatique...) towards one or several models.",
            "development notes : must be reserved to admin",
        ],

    })

#------------------------------------------------------------------------------
# temporaire (a virer) !!!

@api_view(('GET',))
def menu_others( request, format=None ):
    return Response({
       
#       'The models scenarios list': [ reverse('wsmodelscenario-list', request=request, format=format),
#           "help : Returns the models scenarios (WsModelScenario) list !!!!!!!!!!!! A NE PAS LAISSER !!!!",
#       ],

        'The models list': [ reverse('wsmodel-list', request=request, format=format),
            "help : Returns the models (WsModel) list (with for each one its scenarios list in order to choose one of them)",
        ],

        'To re-build the models list': [ reverse('ws-restart', request=request, format=format),

            "help : Re-build and save into the database the models (WsModel) that will be taken into account",
            "development notes : must be reserved to admin",
        ],

        'Accounts': [ reverse('user-list', request=request, format=format),
            "help : Returns the accounts. An account corresponds with a person (model owner, developper...) or is dedicated to limit the access to some models.",
            "development notes : must be reserved to admin",
        ],

        'Some help': reverse('help-list', request=request, format=format),

        'snippets': reverse('snippet-list', request=request, format=format),
    })

#------------------------------------------------------------------------------
# essai 'snippet' temporaire (a virer) !!!

from models import SSnippet
from serializers import SSnippetSerializer
from rest_framework import permissions
from rest_framework import renderers
##from snippets.permissions import IsOwnerOrReadOnly

class SnippetHighlight(generics.GenericAPIView):
    queryset = SSnippet.objects.all()
    renderer_classes = (renderers.StaticHTMLRenderer,)

    def get(self, request, *args, **kwargs):
        snippet = self.get_object()
        return Response(snippet.highlighted)


class SnippetList(generics.ListCreateAPIView):

    queryset = SSnippet.objects.all()
    serializer_class = SSnippetSerializer
    #permission_classes = (permissions.IsAuthenticatedOrReadOnly,
    #                      IsOwnerOrReadOnly,)

    def pre_save(self, obj):
        obj.owner = self.request.user

class SnippetDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = SSnippet.objects.all()
    serializer_class = SSnippetSerializer
    #permission_classes = (permissions.IsAuthenticatedOrReadOnly,
    #                      IsOwnerOrReadOnly,)

    def pre_save(self, obj):
        obj.owner = self.request.user


#******************************************************************************
#
#
#                  !!!!!!!!!        A AFAIRE         !!!!!!!!!
#
#
# Homogeneiser traitement erreurs, exceptions (raise ...)
#
# voir marques '!!!'
#
# Nettoyage des modeles en BD  (... juste avant return Response() ?)
#
#
#******************************************************************************


#******************************************************************************
#
#                                   Help
#
#******************************************************************************

#------------------------------------------------------------------------------
# Help
# Help/xxx with xxx=value of a subject
#------------------------------------------------------------------------------
class Help(generics.ListAPIView):

    def __init__(self, *args, **kwargs):
        HelpText.forceBuildAllHelpText() # prealable !!! a deplacer ???
        self.subject = 'Nohelp' # default
        super(Help, self).__init__(*args, **kwargs)

    def get_queryset( self ) :

        if self.subject == 'All' :
            queryset = HelpText.objects.all()
        elif self.subject == 'Mod' :
            queryset = HelpText.objects.all().filter( subject='Mod' )

        elif self.subject == 'ScnFull' :
            queryset = HelpText.objects.all().filter( subject__in=('ScnFull','id') )

        elif self.subject == 'Scn' :
            queryset = HelpText.objects.all().filter( subject='help_menu_scn' )
        elif self.subject == 'Res' :
            queryset = HelpText.objects.all().filter( subject='help_menu_run' )
        elif self.subject == 'Sim' :
            queryset = HelpText.objects.all().filter( subject__in=('help_menu_scn','help_menu_run') )
        elif self.subject == 'help_menu_scn' :
            queryset = HelpText.objects.all().filter( subject='help_menu_scn' )

        else : # 'Nohelp' and default
            queryset = HelpText.objects.all().filter( subject='Nohelp' )
        return queryset

    serializer_class = HelpTextSerializer

    def get(self, request, subject=None, *args, **kwargs) :
        if subject is None :
            subject = 'All'
        self.subject = subject
        return self.list(request, *args, **kwargs)


#******************************************************************************
#
#                     Database information and management
#
#******************************************************************************

#------------------------------------------------------------------------------
# WsRestart :
# - the models (WsModel) that will be taken into account are re-built and
#   saved into the database.
# - returns an execution report (restart ok/not ok).
#------------------------------------------------------------------------------
class WsRestart( APIView ):

    def get(self, request ):

        # WsModel objects are deleted and rebuilt into the database
        # up to the ModeleRecordBd ones
        WsModelScenario.objects.all().delete()
        WsModel.objects.all().delete()
        initialiserControleur(request) # has to be done before WsModel.buildAll()
        WsModel.buildAll()

        report = [ _( u"Try to re-build and save into the database the models (WsModel) that will be taken into account..." ) ]
        if True : # execution ok
            thestatus = status.HTTP_200_OK
            report.append( _( u"...Execution has succeeded." ) )
            report.append( _( u"The models (WsModel) that will be taken into account have been re-built and saved into the database." ) )
        else : # execution not ok
            thestatus = status.HTTP_400_BAD_REQUEST
            report.append( _( u"...Execution has failed." ) )

        content = { 'report':report } # , 'help':'...' }
        return Response( content, status=thestatus )

#------------------------------------------------------------------------------
# database information : WsModel &Co

class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class IndividuList(generics.ListAPIView):
    queryset = Individu.objects.all()
    serializer_class = IndividuSerializerLight

class IndividuDetail(generics.RetrieveAPIView):
    queryset = Individu.objects.all()
    serializer_class = IndividuSerializerFull

class DbModelList(generics.ListCreateAPIView):
#class DbModelList(generics.ListAPIView):
    queryset = WsModel.objects.all()
    serializer_class = DbModelSerializerLight

class DbModelDetail( generics.RetrieveUpdateAPIView): # ok !
#class DbModelDetail(generics.RetrieveAPIView):
    queryset = WsModel.objects.all()
    serializer_class = DbModelSerializerFull

#******************************************************************************
#
#                       The models and their scenarios
#
#******************************************************************************

#------------------------------------------------------------------------------
# WsModel : the models list with for each one its scenarios list (in order to
# choose one of them).
#------------------------------------------------------------------------------

#class WsModelList(generics.ListCreateAPIView):
class WsModelList(generics.ListAPIView):
    queryset = WsModel.objects.all()
    serializer_class = ModelSerializerLight

#class WsModelDetail( generics.RetrieveUpdateAPIView): # ok !
class WsModelDetail(generics.RetrieveAPIView):
    queryset = WsModel.objects.all()
    serializer_class = ModelSerializerFull

#class WsModelScenarioList(generics.ListAPIView):
#    queryset = WsModelScenario.objects.all()
#    serializer_class = ModelScenarioSerializerLight

class WsModelScenarioDetail(generics.RetrieveAPIView):
    queryset = WsModelScenario.objects.all()
    serializer_class = ModelScenarioSerializerFull


#------------------------------------------------------------------------------
# Common

# get_context returns
# the basic information of the scenario identified by its id idscn.
# Scenario in its original state.
# idscn corresponds with WsModelScenario (idmod is deduced from its WsModel).
def get_context(request, idscn ) :

    # vle_home
    initialiserControleur(request)
    VLE_HOME = session.get(request, 'VLE_HOME')

    # default values
    ws_globalcontext = None
    ws_scncontent = None
    cr_ok = False

    ws_globalcontext = WS_GlobalContext( VLE_HOME, idscn )
    if ws_globalcontext.cr_ok :
        ws_scncontent = WS_ScnContent( ws_globalcontext.scn )
        if ws_scncontent.cr_ok :
            cr_ok = True
    return ( ws_scncontent, ws_globalcontext, cr_ok )
 
def id_defined( idscn ) : 
    id_defined = False # default
    if (idscn is not None) :
        id_defined = True
    return id_defined

def create_scenario( scncontent ) :
    scenario = WsScenario()
    scenario.init( scncontent=scncontent )
    scenario.save()
    scenario.addParameters( scncontent=scncontent )
    scenario.save()
    #print "\n\n Parameters :"
    #for parameter in WsParameter.objects.all() : 
    #    print parameter
    #print "\n\n Scenarios :"
    #for scenario in WsScenario.objects.all() : 
    #    print scenario
    #    for parameter in scenario.parameters.all() : 
    #        print parameter
    #    print ""
    return scenario

def create_result( simuresult, scenario ):
    result = WsResult( scenario=scenario )
    result.init( simuresult=simuresult )
    result.save()
    return result

#******************************************************************************
#
#                               A scenario
#
#******************************************************************************

#------------------------------------------------------------------------------
# ScnFull/idscn GET : returns the complete information of the scenario
# identified by its id idscn. Scenario in its original state. The information
# contains the documentation (see 'metadata').
#
# No POST.
#
# idscn corresponds with WsModelScenario (idmod is deduced from its WsModel) ; see Mod to know them.
#------------------------------------------------------------------------------
class ScnFull( generics.RetrieveAPIView ) :

    def get(self, request, id=None ):
        idscn = id
        thestatus = status.HTTP_400_BAD_REQUEST # default
        if id_defined( idscn ) :
            ( ws_scncontent, ws_globalcontext, cr_ok ) = get_context( request, idscn )
            if cr_ok :
                scenario = create_scenario( ws_scncontent )
                thestatus = status.HTTP_200_OK # !!! forced (not treated)
                f = scenario.get_fields_actifs() + ('parameters',)
                fields_shown = scenario.get_fields_shown( fieldnames=f )
                serializer = ScenarioSerializerFull( scenario, fields=fields_shown )
                return Response(serializer.data, status=thestatus )
            else :
                if not ws_globalcontext.is_public_model :
                    thestatus = status.HTTP_401_UNAUTHORIZED
        return Response( status=thestatus )

#------------------------------------------------------------------------------
# Scn/idscn GET :
# - Returns the information of the scenario identified by its id idscn. Scenario in its original state. The information without the documentation.
# - Form to modify the scenario information ('duration','parameters'...).
#
# Scn/idscn POST : returns the information of the scenario identified by its id idscn. Scenario whose original state has been modified according to posted scenario information ('duration'...). The information without the documentation.
#
# idscn corresponds with WsModelScenario (idmod is deduced from its WsModel) ; see Mod to know them.
#------------------------------------------------------------------------------
class Scn( generics.RetrieveUpdateAPIView ) :

    def get_serializer_class(self):
        if self.with_form :
            return ScenarioSerializer_Form
        else :
            return ScenarioSerializer_NoForm

    def __init__(self, *args, **kwargs):
        self.with_form = False
        super(Scn, self).__init__(*args, **kwargs)

    def get(self, request, id=None ):
        idscn = id
        self.with_form = True
        thestatus = status.HTTP_400_BAD_REQUEST # default
        if id_defined( idscn ) :
            ( ws_scncontent, ws_globalcontext, cr_ok ) = get_context( request, idscn )
            if cr_ok :
                scenario = create_scenario( ws_scncontent )
                thestatus = status.HTTP_200_OK # !!! forced (not treated)
                f = scenario.get_fields_actifs() + ('parameters',)
                fields_shown = scenario.get_fields_shown( fieldnames=f )
                serializer = ScenarioSerializerLight( scenario, fields=fields_shown )
                return Response(serializer.data, status=thestatus )
            else :
                if not ws_globalcontext.is_public_model :
                    thestatus = status.HTTP_401_UNAUTHORIZED
        return Response(status=thestatus ) # report, help...!

    def post(self, request, id=None, format=None):
        idscn = id
        self.with_form = False
        thestatus = status.HTTP_400_BAD_REQUEST # default

        # !!! ajouter cas idscn dans post ???
        # keys = request.DATA.keys()
        # if ('idscn' in keys) :
        #     idscn = request.DATA['idscn']
        #     id_defined = True
        if id_defined( idscn ) :
            ( ws_scncontent, ws_globalcontext, cr_ok ) = get_context( request, idscn )
            if cr_ok :
                scenario = create_scenario( ws_scncontent )
                thestatus = status.HTTP_200_OK # !!! forced (not treated)
                f = scenario.get_fields_actifs() + ('parameters',)
                fields_shown = scenario.get_fields_shown( fieldnames=f )
                serializer = ScenarioSerializerLight( scenario, fields=fields_shown )
                #print "duration value : ", serializer.data.get('duration')

                maj_serializer = ScenarioSerializer_Form( scenario, data=request.DATA )
                if maj_serializer.is_valid() :
                    maj_serializer.save()
                    serializer.copie( maj_serializer, fields=fields_shown ) 
                    return Response(serializer.data, status=thestatus )
                else :
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST) # !!! a revoir
            else :
                if not ws_globalcontext.is_public_model :
                    thestatus = status.HTTP_401_UNAUTHORIZED
        return Response(status=thestatus ) # report, help...!

#******************************************************************************
#
#                               A scenario simulation
#
#******************************************************************************

#------------------------------------------------------------------------------
# Res/idscn GET :
# - Returns the simulation result of the scenario identified by its id idscn. Scenario in its original state.
# - Form to modify the scenario information ('duration','parameters'...) to be simulated.
#
# Res/idscn POST : returns the simulation result of the scenario identified by its id idscn. Scenario whose original state has been modified according to posted scenario information ('duration'...).
#
# idscn corresponds with WsModelScenario (idmod is deduced from its WsModel) ; see Mod to know them.
#
# ResFull/idscn : idem Res/idscn excepted that the scenario information is also returned (in addition with the simulation result). The ResFull class inherits from the Res class which takes 'only_result' into account.
#
#------------------------------------------------------------------------------

class Res( generics.RetrieveUpdateAPIView ) :

    def get_serializer_class(self):
        if self.with_form :
            return ScenarioSerializer_Form
        else :
            return ScenarioSerializer_NoForm

    def __init__(self, *args, **kwargs):
        self.with_form = False
        self.only_result = True # value for Res
        super(Res, self).__init__(*args, **kwargs)

    def get(self, request, id=None ):
        idscn = id
        self.with_form = True
        thestatus = status.HTTP_400_BAD_REQUEST # default
        if id_defined( idscn ) :
            ( ws_scncontent, ws_globalcontext, cr_ok ) = get_context( request, idscn )
            if cr_ok :
                scenario = create_scenario( ws_scncontent )
                # scenario.cr_ok !!! forced (not treated)
                ws_simuresult = WS_SimuResult( ws_globalcontext.scn )
                if ws_simuresult.cr_ok :
                    ws_simuresult.run()
                    if ws_simuresult.cr_ok :
                        thestatus = status.HTTP_200_OK
                        result = create_result( simuresult=ws_simuresult, scenario=scenario )
                        if self.only_result :
                            f = ('result',)
                        else :
                            f = scenario.get_fields_actifs() + ('parameters', 'result')
                        fields_shown = scenario.get_fields_shown( fieldnames=f )
                        serializer = ScenarioSerializerLight( scenario, fields=fields_shown )
                        return Response(serializer.data, status=thestatus )
                    #else : !!!
                #else : !!!
            else :
                if not ws_globalcontext.is_public_model :
                    thestatus = status.HTTP_401_UNAUTHORIZED
        return Response(status=thestatus ) # report, help...!

    def post(self, request, id=None ):
        idscn = id
        #keys = request.DATA.keys()
        #pour l'instant simumode 'non gere' : choix de sa valeur non propose, simulation avec systematiquement valeur par defaut de simumode !!!
        self.with_form = False
        thestatus = status.HTTP_400_BAD_REQUEST # default

        # !!! ajouter cas idscn dans post ???...
        if id_defined( idscn ) :
            ( ws_scncontent, ws_globalcontext, cr_ok ) = get_context( request, idscn )
            if cr_ok :
                scenario = create_scenario( ws_scncontent )
                # scenario.cr_ok !!! forced (not treated)
                # maj scenario selon post
                f = scenario.get_fields_actifs() + ('parameters',)
                fields_shown = scenario.get_fields_shown( fieldnames=f )
                serializer = ScenarioSerializerLight( scenario, fields=fields_shown )
                maj_serializer = ScenarioSerializer_Form( scenario, data=request.DATA )

                if maj_serializer.is_valid() :
                    maj_serializer.save()
                    serializer.copie( maj_serializer, fields=fields_shown ) 
                    ws_simuresult = WS_SimuResult( ws_globalcontext.scn )
                    if ws_simuresult.cr_ok :
                        try :
                            ( cr_ok, texte_rapport ) = ws_simuresult.maj( scenario )
                        except Exception, e :
                            message_erreur = get_message_exception(e)
                            print "\n message_erreur : ", message_erreur # !!!
                            return Response( {'report':message_erreur}, status=status.HTTP_400_BAD_REQUEST) # !!! a revoir
                        else : # pas d'exception lors deroulement de maj, continuer
                            if not cr_ok : # autres erreurs lors deroulement de maj
                                message_erreur = texte_rapport 
                                print "\n message_erreur : ", message_erreur # !!!
                                return Response( {'report':message_erreur}, status=status.HTTP_400_BAD_REQUEST) # !!! a revoir
                            else : # bon deroulement de maj, continuer

                                ws_simuresult.run()
                                if ws_simuresult.cr_ok :
                                    thestatus = status.HTTP_200_OK
                                    result = create_result( simuresult=ws_simuresult, scenario=scenario )
                                    if self.only_result :
                                        f = ('result',)
                                    else :
                                        f = scenario.get_fields_actifs() + ('parameters', 'result',)
                                    fields_shown = scenario.get_fields_shown( fieldnames=f )
                                    serializer = ScenarioSerializerLight( scenario, fields=fields_shown )
                                    return Response(serializer.data, status=thestatus )
                                else : # !!! a revoir
                                    return Response( {'report':self.report}, status=status.HTTP_400_BAD_REQUEST) # !!! a revoir

                else : # scenario saisie not ok
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST) # !!! a revoir
            else :
                if not ws_globalcontext.is_public_model :
                    thestatus = status.HTTP_401_UNAUTHORIZED
        return Response(status=thestatus ) # report, help...!

class ResFull( Res ):
    def __init__(self, *args, **kwargs):
        super(ResFull, self).__init__(*args, **kwargs)
        self.only_result = False

#******************************************************************************

#...    else :
#           msg = _(u"undefined id (idscn)" )
#           report = [ msg ]
#           help_msg = _(u"idscn must be defined in url (Res/idscn/) or in post { 'idscn':value }" )
#           help = [ help_msg ]
#           content = { 'report':report, 'help':help }
#           return Response( content, status=status.HTTP_400_BAD_REQUEST )

#******************************************************************************


