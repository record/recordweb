## @file record/webservices_record/urls.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File urls.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

from django.conf.urls.defaults import patterns, include, url
from rest_framework.urlpatterns import format_suffix_patterns
import views #from views import Scn, Res # *

from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = patterns('record.webservices_record.views',

    #--------------------------------------------------------------------------
    # Menus

    url(r'^$',                'home' ),
    url(r'^models/$',         'models',         name='models' ),
    url(r'^simulation/$',     'simulation',     name='simulation' ),
    url(r'^administration/$', 'administration', name='administration' ),
    url(r'^menu_others/$',    'menu_others',    name='menu_others' ),

    #--------------------------------------------------------------------------
    # Database information and management

    url(r'^DbMod/$', views.DbModelList.as_view(), name='dbmodel-list'),
    url(r'^DbMod/(?P<pk>[0-9]+)/$', views.DbModelDetail.as_view(), name='dbmodel-detail'),

    url(r'^WsRestart/$', views.WsRestart.as_view(), name='ws-restart'),

    # Accounts (users)
    url(r'^User/$', views.UserList.as_view(), name='user-list'),
    url(r'^User/(?P<pk>[0-9]+)/$', views.UserDetail.as_view(), name='user-detail'),

    # Individus
    url(r'^Individu/$', views.IndividuList.as_view(), name='individu-list'),
    url(r'^Individu/(?P<pk>[0-9]+)/$', views.IndividuDetail.as_view(), name='individu-detail'),

    # MotDePasse ...

    #--------------------------------------------------------------------------

    # Help
    url(r'^wsrec/Help/$', views.Help.as_view(), name='help-list'),
    url(      r'^Help/$', views.Help.as_view(), name='help-list'),
    url(r'^wsrec/Help/(?P<subject>[a-z,A-Z,0-9,_]+)/$', views.Help.as_view(), name='help-detail'),
    url(      r'^Help/(?P<subject>[a-z,A-Z,0-9,_]+)/$', views.Help.as_view(), name='help-detail'),

    #--------------------------------------------------------------------------
    # Models and their scenarios

    # navigation to identify the scenario id, select the scenario
    url(r'^Mod/$', views.WsModelList.as_view(), name='wsmodel-list'),
    url(r'^Mod/(?P<pk>[0-9]+)/$', views.WsModelDetail.as_view(), name='wsmodel-detail'),
    url(r'^ModScn/(?P<pk>[0-9]+)/$', views.WsModelScenarioDetail.as_view(), name='wsmodelscenario-detail'),

    # For ScnFull, Scn, Res, ResFull, the id is the WsModelScenario's one

    # ScnFull (Full for Doc)
    url(r'^wsrec/ScnFull/(?P<id>[0-9]+)/$', views.ScnFull.as_view(), name='scnfull-detail'),
    url(      r'^ScnFull/(?P<id>[0-9]+)/$', views.ScnFull.as_view(), name='scnfull-detail'),

    # Scn
    url(r'^wsrec/Scn/(?P<id>[0-9]+)/$', views.Scn.as_view(), name='scn-detail'),
    url(      r'^Scn/(?P<id>[0-9]+)/$', views.Scn.as_view(), name='scn-detail'),

    # ResFull (Full for Scenario)
    url(r'^wsrec/ResFull/(?P<id>[0-9]+)/$', views.ResFull.as_view(), name='resfull-detail'),
    url(      r'^ResFull/(?P<id>[0-9]+)/$', views.ResFull.as_view(), name='resfull-detail'),

    # Res
    url(r'^wsrec/Res/(?P<id>[0-9]+)/$', views.Res.as_view(), name='res-detail'),
    url(      r'^Res/(?P<id>[0-9]+)/$', views.Res.as_view(), name='res-detail'),

    # ...

    # essai a virer !!!
    url(r'^ssnippets/$', views.SnippetList.as_view(), name='snippet-list'),
    url(r'^ssnippets/(?P<pk>[0-9]+)/$', views.SnippetDetail.as_view(), name='snippet-detail'),
    url(r'^ssnippets/(?P<pk>[0-9]+)/highlight/$', views.SnippetHighlight.as_view(), name='snippet-highlight'),
    #url(r'^uusers/$', views.UserList.as_view(), name='user-list'),
    #url(r'^uusers/(?P<pk>[0-9]+)/$', views.UserDetail.as_view(), name='user-detail'),

    # ...

)

#urlpatterns = format_suffix_patterns(urlpatterns) #!!!!



# ESSAI EN COURS !!!!!!!!!
#
#from django.conf.urls.defaults import patterns, include, url
#from rest_framework.urlpatterns import format_suffix_patterns
#from snippets import views
#
#
# API endpoints
##urlpatterns = format_suffix_patterns(patterns('snippets.views',
#    url(r'^$', 'api_root'),
#    url(r'^snippets/$',
#        views.SnippetList.as_view(),
#        name='snippet-list'),
#    url(r'^snippets/(?P<pk>[0-9]+)/$',
#        views.SnippetDetail.as_view(),
#        name='snippet-detail'),
#    url(r'^snippets/(?P<pk>[0-9]+)/highlight/$',
#        views.SnippetHighlight.as_view(),
#        name='snippet-highlight'),
#    url(r'^users/$',
#        views.UserList.as_view(),
#        name='user-list'),
#    url(r'^users/(?P<pk>[0-9]+)/$',
#        views.UserDetail.as_view(),
#        name='user-detail')
#))
#
## Login and logout views for the browsable API
#urlpatterns += patterns('',    
#    url(r'^api-auth/', include('rest_framework.urls',
#                               namespace='rest_framework')),
#)
#


