#-*- coding:utf-8 -*-

from record.bd_modeles_record.models import ModeleRecord as ModeleRecordBd
from record.outil_web_record.models.modele_record.modele_record import ModeleRecordOn
from record.outil_web_record.models.scenariovpz.scenariovpz import ScenarioVpz

from record.outil_web_record.models.espace_exploitation.espace_exploitation import EspaceExploitation

from models import WsModel, WsModelScenario

from record.outil_web_record.models.scn.scn import Scn
from record.outil_web_record.models.resultat.resultat import Resultat

from record.outil_web_record.models.erreurs_gerees.erreurs_gerees import ErreursGerees as ERR # noms des erreurs
from record.utils.erreur import Erreur # gestion des erreurs
from record.utils.erreur import get_message_exception
from record.utils.vle.exp import Exp

from record.outil_web_record.models.conf_web.transcodage import TranscodageConfWebScenario as tcws

from django.utils.translation import ugettext as _

#******************************************************************************
#
# Traitements faisant l'interface entre des objets de l'outil web record
# outil_web_record et bd_modeles_record (ModeleRecordBd, ModeleRecordOn, Scn,
# Exp...) et ceux de webservices_record (WsModel, WsModelScenario)
#
# Certaines fonctions sont (re)ecrites pour etre adaptees au nouveau contexte
# d'appel (E/S) : list_from_str, getScenarioVpz, getApplicationWeb...
#
#******************************************************************************

from ast import literal_eval

# extraction list (de str v)
# issu/extrait/adapte de def extraire_list(cls, v, prefixe, suffixe) :
def list_from_str( v ) :
    conversion_ok = False # par defaut
    val = None # par defaut
    try:
        c = literal_eval( v ) # contenu du str
        # extraction ok, s'assurer du format
        try:
            val = c
            if isinstance(val,list) :
                conversion_ok = True
        except : pass
    except : pass
    return (conversion_ok,val)

#******************************************************************************
# le_modele_record, scn, espace exploitation ... Il s'agit du scenario dans
# son etat d'origine (tel que dans depot, non modifie)
# Rq1 : construction scn mis a part scn.result
# Rq2 : scn.definition/blocs servira a creer ws_scncontent qui lui meme
#       servira a maj directement exp (pas de maj de definition.blocs a
#       partir de ws_scncontent)
#******************************************************************************
class WS_GlobalContext( object ):

    def __init__( self, VLE_HOME, idscn ) : # ex ( self, VLE_HOME, idmod, idscn )

        cr_ok = True # default
        report =  list() # default
        help_msgs = list() # default

        le_modele_record = None # default
        scn = None # default

        # nettoyage relatif aux repertoires d'exploitation devenus obsoletes
        # (suppression de repertoires VLE_HOME propres aux sessions/connexions)
        EspaceExploitation.nettoyage()

        # les entrees
        ###modele_record_id = int(idmod)
        ###index_scenario_on = int(idscn)

        wsmodelscenario_id = int(idscn)
        wsmodelscenario = WsModelScenario.objects.get(id=wsmodelscenario_id)

        #wsmodel_id = int(idmod) # est remplace par
        wsmodel_id = wsmodelscenario.wsmodel.id
        wsmodel = WsModel.objects.get(id=wsmodel_id)

        index_application_associee = 0 # !!! pour l'instant non choisie par usr

        # le_modele_record
        ###modele_record_bd = ModeleRecordBd.objects.get(id=int(idmod))
        modele_record_bd = ModeleRecordBd.objects.get(id=wsmodel.modele_record_id)
        le_modele_record = None # par defaut
        is_public_model = True # default

        if modele_record_bd.isPublic() : # modele public
            le_modele_record = ModeleRecordOn( modele_record_bd ) # tentative
            if not le_modele_record.estExploitable() : # echec
                msg_off = le_modele_record.exploitabilite.getMsgEtat()
                msg = _(u"Le modèle est considéré indisponible") + ", " + _(u"problème") + " : " + msg_off
                report.append( msg )
                le_modele_record = None
        else : # modele prive,  pour l'instant non traite (acces non donne)
            is_public_model = False
            msg = "modele acces prive" # _(u"modèle accès privé") 
            report.append( msg )

        if le_modele_record is not None :

            #******************************************************************
            # le scenario et son application associee
            #******************************************************************
            # le scenario est un scenario existant
            # index_scenario_on et index_application_associee existent
            #
            # !!! impasses
            # sur cas de nouveau fichier scenario de simulation (fichier vpz) :
            #     un_new_fichier_scenario_envoye, nom_new_fichier_scenario...)
            # sur cas de nouveau fichier de donnees a ajouter au repertoire data du
            #     modele : un_new_fichier_data_envoye, nom_new_fichier_data...)
            # ... recuperation des fichiers envoyes ...
            #

            ###scenario_vpz = le_modele_record.getScenarioVpz( index_scenario_on )
            scenario_vpz = self.getScenarioVpz( le_modele_record, wsmodelscenario )

            ###application_associee = le_modele_record.getApplicationWeb( index_scenario_on, index_application_associee )
            application_associee = self.getApplicationWeb( le_modele_record, wsmodelscenario, index_application_associee )

            infos_application_id = application_associee.getId()

            # Construction de scn (mis a part scn.result)
            scn = Scn( VLE_HOME, scenario_vpz, infos_application_id )

            # Note : avec la creation/construction de scn, il a ete cree un espace
            # d'exploitation dedie aux differentes operations qui vont par la suite
            # etre effectuees/demandees sur le scenario retenu. On travaille
            # dorenavant dans cet espace d'exploitation, sur scenario_vpz et non plus
            # scenario_vpz_d_origine.

        else : # le_modele_record is None
            cr_ok = False

        help_msgs.append( "...???..." )

        self.cr_ok = cr_ok
        self.report = report
        self.help = help_msgs

        self.le_modele_record = le_modele_record
        self.scn = scn
        self.is_public_model = is_public_model 

    ### remplace ModeleRecordOn.getScenarioVpz( index_scenario_on )
    @classmethod
    def getScenarioVpz( cls, le_modele_record, wsmodelscenario ) :

        nom_paquet = le_modele_record.nom
        description_modele_record = le_modele_record.description
        nom_pkg = le_modele_record.nom_pkg
        version_vle = le_modele_record.version_vle
        nom_repertoire_lot_pkgs = le_modele_record.nom_repertoire_lot_pkgs
        nom_repertoire_data = le_modele_record.nom_repertoire_data

        nom_vpz = wsmodelscenario.nom_scenario + ".vpz"

        # infos_generales_initiales_scenario
        nom_scenario = wsmodelscenario.nom_scenario
        conf_web_modele_record = le_modele_record.conf_web_modele_record
        infos_generales_initiales_scenario = conf_web_modele_record.infos_generales_scenarios[nom_scenario]

        scenario_vpz = ScenarioVpz( nom_vpz, nom_paquet, description_modele_record, nom_pkg, version_vle, nom_repertoire_lot_pkgs, nom_repertoire_data, infos_generales_initiales_scenario )

        return scenario_vpz 

    ### remplace ModeleRecordOn.getApplicationWeb( index_scenario_on, index_application_associee )
    @classmethod
    def getApplicationWeb( cls, le_modele_record, wsmodelscenario, index_application_associee ) :
        nom_scenario = wsmodelscenario.nom_scenario
        conf_web_modele_record = le_modele_record.conf_web_modele_record
        application_web = conf_web_modele_record.getApplicationWeb( nom_scenario, index_application_associee )
        return application_web

#******************************************************************************
#
# Donnees envoyees/recues par webservices (construction, mise en forme...)
#
#******************************************************************************

msg = " - " + _(u"idmod = model key in 'data'")
msgs_id = [ msg ]
msg = " - " +_(u"idscn = scenario key in 'scenarios'")
msgs_id.append( msg )

# help_msgs_scns OK a jeter
msg = _(u"** Warnings :")
help_msgs_scns = [ msg ]
msg = " - " +_(u" If the model is private, it won't be possible to see its content, neither to simulate it.")
help_msgs_scns.append( msg )
msg = _(u"** Id : ")
help_msgs_scns.append( msg )
for m in msgs_id :
    help_msgs_scns.append( m )

# scncontent
msg_scncontent = _(u"{ 'experimentname':value , 'duration':value , 'begin':[ time, dateheure ] , 'seed':value , 'replicaseed':value , 'replicanumber':value , 'parameters':{ nom condition : { nom port : [ val, val, ... ] } } }")
msgs_scncontent_keys = [ "experimentname", "duration", "begin", "seed", "replicaseed", "replicanumber", "parameters" ]
msg = _(u"** Scenario content : ")
help_msgs_scncontent = [ msg ]
help_msgs_scncontent.append( msg_scncontent )
msg = _(u"** Scenario content keys: ") 
help_msgs_scncontent.append( msg )
for m in msgs_scncontent_keys :
    help_msgs_scncontent.append( m )

# help_msgs_menu_scn OK a jeter
msg = _(u"** Experiment menu :")
help_msgs_menu_scn = [ msg ]
msg = " - " + _(u"GET Scn/idmod/idscn : returns the model scenario content")
help_msgs_menu_scn.append( msg )
msg = " - " + _(u"POST Scn/idmod/idscn or POST Scn with keys { 'idmod':value, 'idvpz':value } :")
help_msgs_menu_scn.append( msg )
msg = "   " + _(u"returns the model scenario content once modified according to the optional keys from a scenario content.")
help_msgs_menu_scn.append( msg )

# help_msgs_menu_run OK a ajeter
msg = _(u"** Simulation menu :")
help_msgs_menu_run = [ msg ]
msg = " - " + _(u"GET Res/idmod/idscn : returns the results of the scenario simulation")
help_msgs_menu_run.append( msg )
msg = " - " + _(u"POST Res/idmod/idscn or POST Res with keys { 'idmod':value, 'idvpz':value } : ")
help_msgs_menu_run.append( msg )
msg = "   " + _(u"returns the results of the scenario simulation once modified according to the optional keys from a scenario content ('duration', 'begin', 'parameters'...).")
help_msgs_menu_run.append( msg )
msg = "   " + _(u"optional key 'simumode': value 'multi' for a multisimulation, value 'mono' for a monosimulation.")
help_msgs_menu_run.append( msg )
msg = "   " + _(u"optional key 'returnscncontent': value True to receive the content of the simulated scenario, value False not to.")
help_msgs_menu_run.append( msg )


#------------------------------------------------------------------------------
# Liste des modeles et scenarios
#
# data :
#
# { "idmod" : { "nommodelerecord":...
#               "nompkg":...
#               "nomrepertoiredata":...
#               "scenarios" : { "idscn" : { "nomscenarioavecvpz":...
#                                           "titrescenario":...  }  }  }  }
#
# !!! pour l'instant on ne propose pas la liste des applications associees !!!
# !!! (l'application associee est cependant utilisee en guise de filtrage) !!!
#
#------------------------------------------------------------------------------
class OLD___WS_LesModeles( object ):

    def __init__( self ):

        cr_ok = True # default
        report =  list() 
        help_msgs = list()
        lesmodeles = dict()

        liste_modeles_record_bd = ModeleRecordBd.objects.all()
        for modele_record_bd in liste_modeles_record_bd :

            le_modele_record = ModeleRecordOn( modele_record_bd )
            if le_modele_record.estExploitable() :

                idmod = le_modele_record.id
                lemodele = dict()
                lemodele[ "nompkg" ] = le_modele_record.nom_pkg 
                lemodele[ "nommodelerecord" ] = le_modele_record.nom
                lemodele[ "nomrepertoiredata" ] = le_modele_record.nom_repertoire_data

                scenarios = dict()
                for idscn,scenario_on in enumerate(le_modele_record.scenarios_on) :
                    lescn = dict()
                    lescn[ "nomscenarioavecvpz" ] = scenario_on.infos_scenario_form.nom_scenario_avec_vpz 
                    lescn[ "titrescenario" ] = scenario_on.infos_scenario_form.titre_scenario
                    scenarios[ idscn ] = lescn
                lemodele[ "scenarios" ] = scenarios

                lesmodeles[ idmod ] = lemodele

            else : # modele inexploitable ignore
                report.append( _(u"unavailable model ignored") )

        help_msgs.append( help_msgs_scns )
        help_msgs.append( help_msgs_menu_scn )
        help_msgs.append( help_msgs_menu_run )
        help_msgs.append( help_msgs_scncontent )

        self.data = lesmodeles
        self.cr_ok = cr_ok
        self.report = report
        self.help = help_msgs

    def INUSITE___get_pkg_name( self, idmod ) :
        lesmodeles = self.data
        return ( lesmodeles[idmod]["nompkg"] )

    def INUSITE___get_vpz_name( self, idmod, idscn ) :
        lemodele = self.data[idmod]
        return ( lemodele[idscn]["nomscenarioavecvpz"] )


#------------------------------------------------------------------------------
# Contenu d'un scenario presente/propose pour information/modification
#
# data :
#
# { "experimentname" : ...
#   "duration" : ...
#   "begin" : ... !!!
#   "seed" : ...
#   "replicaseed" : ...
#   "replicanumber" : ...
#   "parameters" : { nom condition : { nom port : [ values ] } }
# }
#
# scn.definition.blocs -> scncontent ...(-> exp) : la source d'informations
# est scn.definition.blocs. De son cote, scncontent servira a maj directement
# exp (pas de maj de definition.blocs a partir de scncontent).
#
# Utilisation/prise en compte partielle de la personnalisation :
# - Les parametres sont classes dans scncontent selon les conditions d'origine,
#   l'organisation en blocs est interpretee en tant que filtrage de donnees.
# - En l'etat actuel, une donnee de type CONST_web_mode_read_only n'est pas
#   restituee. !!!
#
#------------------------------------------------------------------------------
class WS_ScnContent( object ):

    def __init__( self, scn ) :

        cr_ok = True # default
        report =  list()
        help_msgs = list()
        scncontent = dict()
        parameters = dict()
        metadata = dict() # for parameters (classified by conditions and ports)

        for bloc in scn.definition.blocs :

            for donnee_def in bloc.liste_datas :

                # maj scncontent a partir de donnee_def.values

                web_mode = donnee_def.getWebMode()

                if tcws.isReadWriteMode(web_mode) :

                    vpz_type = donnee_def.getVpzType()
                    if tcws.isTypeExpNameVpzType(vpz_type) :
                        scncontent[ 'experimentname' ] = donnee_def.values[0] 

                    elif tcws.isTypeExpDurationVpzType(vpz_type) :
                        scncontent[ 'duration' ] = donnee_def.values[0]

                    elif tcws.isTypeExpBeginVpzType(vpz_type) : # !!!
                        # scncontent[ 'begin' ] = donnee_def.values !!!
                        scncontent[ 'begin' ] = [ donnee_def.values[0] ] # time
                        date_heure = donnee_def.values[1] + "--" + donnee_def.values[2]
                        scncontent[ 'begin' ].append( date_heure )

                    elif tcws.isTypeSimuSeedVpzType(vpz_type) :
                        scncontent[ 'seed' ] = donnee_def.values[0]

                    elif tcws.isTypePlanSeedVpzType(vpz_type) :
                        scncontent[ 'replicaseed' ] = donnee_def.values[0]

                    elif tcws.isTypePlanNumberVpzType(vpz_type) :
                        scncontent[ 'replicanumber' ] = donnee_def.values[0]

                    elif tcws.isTypeConditionPortVpzType(vpz_type) :
                        cond = donnee_def.getVpzCondName()
                        port = donnee_def.getVpzPortName()
                        values = donnee_def.values
                        meta = { 'typevalue':donnee_def.type_value,
                                 'unit':donnee_def.infos_donnee_def.doc.unite,
                                 'max':donnee_def.infos_donnee_def.doc.val_max,
                                 'min':donnee_def.infos_donnee_def.doc.val_min,
                                 'recommanded': donnee_def.infos_donnee_def.doc.val_recommandee
                                 }

                        if cond in parameters.keys() : # existe deja
                            parameters[cond][port] = values
                            metadata[cond][port] = meta
                        else :
                            parameters[cond] = { port : values }
                            metadata[cond] = { port : meta }

                    # else : rien

                # else CONST_web_mode_hidden : rien

                # else CONST_web_mode_read_only : rien # !!! attention le usr ne le sait pas !!!
                elif tcws.isReadOnlyMode(web_mode) :
                    msg = _(u"read only data hidden !!!")
                    report.append( msg )

        scncontent[ 'parameters' ] = parameters
        scncontent[ 'metadata' ] = metadata

        help_msgs.append( help_msgs_menu_scn )
        help_msgs.append( help_msgs_menu_run )
        help_msgs.append( help_msgs_scncontent )

        self.data = scncontent
        self.cr_ok = cr_ok
        self.report = report
        self.help = help_msgs


#------------------------------------------------------------------------------
# Resultat de la simulation d'un scenario
#
# data : pour l'instant il s'agit de scn.resultat en l'etat (plus exactement
#        results_par_vues) => a 'transformer' dans WsSimuResult ?
#
# exp cree a partir de scn, puis maj selon scenario (WsScenario).
#
#------------------------------------------------------------------------------

class WS_SimuResult( object ):

    def __init__( self, scn ) :

        # exp correspond a scenario_vpz, avec prise en compte des 
        # valeurs de definition
        # exp est construit relativement au paquet pour les besoins 
        # de l'operation de simulation (cf data, lib...)
        exp = Exp( scn.scenario_vpz.nom_vpz, scn.scenario_vpz.get_nom_pkg() )

        self.exp = exp
        self.scn = scn

        self.data = None

        self.report =  list()
        self.help = list()
        self.cr_ok = True

    # Mise a jour de exp selon scenario
    def maj( self, scenario ):

        cr_ok = True # default
        texte_rapport = ""

        if scenario.experimentname_isactive :
            experimentname = scenario.experimentname
            ###v = str( scncontentdata['experimentname'] )
            v = experimentname.encode()
            self.exp.setName( v )

        if scenario.duration_isactive :
            duration = scenario.duration
            #v = float( scncontentdata['duration'] )
            v = duration
            self.exp.setDuration( v )

        if scenario.begin_isactive :
            (conversion_ok, begin) = list_from_str( scenario.begin )
            if conversion_ok :
                # !!! pour l'instant prise en compte uniquement de
                # !!! valeur time (date_heure ignoree)

                #time = scncontentdata['begin'][0]
                #date_heure = scncontentdata['begin'][1] # !!! ignoree
                #v = float( time )
    
                time = begin[0]
                date_heure = begin[1] # !!! ignoree
                print "Pour memo, begin[1] ignoree, ie date_heure : ", date_heure
                texte_rapport = texte_rapport + " -- " + "Pour memo, begin[1] ignoree (ie date,heure)."
                v = time
        
                self.exp.setBegin( v )
    
        if scenario.seed_isactive :
            seed = scenario.seed
            #v = float( scncontentdata['seed'] )
            v = seed
            self.exp.setSeed( v )
    
        if scenario.replicaseed_isactive :
            replicaseed = scenario.replicaseed
            #v = int( scncontentdata['replicaseed'] )
            v = replicaseed
            self.exp.setReplicaSeed( v )

        if scenario.replicanumber_isactive :
            replicanumber = scenario.replicanumber
            #v = int( scncontentdata['replicanumber'] )
            v = replicanumber
            self.exp.setReplicaNumber( v )
 
        for parameter in scenario.parameters.all() : 

            txt_exception = ""

            cond = parameter.condname
            cond = cond.encode()
            port = parameter.portname
            port = port.encode()
            type_value = parameter.metadata.typevalue
            type_value = type_value.encode() # ???
            (conversion_ok, values) = list_from_str( parameter.values )

            if conversion_ok :

                self.exp.clearConditionPort(cond, port) # effacement prealable
                for i,val in enumerate(values) :
                    ###cr = self.exp.tenterEcrireValeur( cond, port, v, type_value )
                    cr = True # default
                    if self.exp.is_type_value_not_nul( type_value ) :
                        try :
                            if type_value == self.exp.TYPE_VALUE_BOOLEAN :
                                self.exp.addBooleanCondition(cond, port, val)
                            elif type_value == self.exp.TYPE_VALUE_INTEGER :
                                self.exp.addIntegerCondition(cond, port, val)
                            elif type_value == self.exp.TYPE_VALUE_DOUBLE :
                                self.exp.addRealCondition(cond, port, val)
                            elif type_value == self.exp.TYPE_VALUE_STRING :
                                self.exp.addStringCondition(cond, port, val)
                            elif type_value == self.exp.TYPE_VALUE_MAP :
                                self.exp.addMapCondition(cond, port, val)
                            elif type_value == self.exp.TYPE_VALUE_SET :
                                self.exp.addSetCondition(cond, port, val)
                            elif type_value == self.exp.TYPE_VALUE_MATRIX :
                                self.exp.addMatrixCondition(cond, port, val)
                            elif type_value == self.exp.TYPE_VALUE_TABLE :
                                self.exp.addTableCondition(cond, port, val)
                            elif type_value == self.exp.TYPE_VALUE_TUPLE :
                                self.exp.addTupleCondition(cond, port, val)
                            elif type_value == self.exp.TYPE_VALUE_XMLTYPE :
                                self.exp.addXMLCondition(cond, port, val)
    
                        except Exception, e :
                            diag = "echec de format, " + e.message
                            txt_exception = txt_exception + " -- " + diag
                            cr = False
                            print "tenterEcrireValeur, ", diag
                                                 
                    else : # notamment TYPE_VALUE_NIL
                        diag = "echec de type_value" 
                        txt_exception = txt_exception + " -- " + diag
                        cr = False
                        print "tenterEcrireValeur, ", diag

                    if not cr : 
                        cr_ok = False
                        msg = "echec ecriture : cond: " + str(cond) + " port: " + str(port)
                        msg = msg + " v: " + str(v) + " type_value: " + type_value + " i: " + str(i)
                        print msg
                    else :
                        msg = "ecriture : cond: " + str(cond) + " port: " + str(port)
                        msg = msg + " val: " + str(val) + " type_value: " + type_value + " i: " + str(i)
                        print msg
                    texte_rapport = texte_rapport + " -- " + msg    

            else : # not conversion_ok 
                cr_ok = False
                msg = "echec format values " + str(values) + "."
                texte_rapport = texte_rapport + " -- " + msg    

        return ( cr_ok, texte_rapport )


    def run( self, simumode="multi" ) :

        # Initialisations
        erreur_echec_simulation = Erreur( ERR.NOM_erreur_echec_simulation ) # desactivee par defaut
        message_erreur = "" # par defaut

        cr_ok = True # default

        if simumode == "multi" : # cas simulation multiple
            type_simulation = "multi_simulation"
            mode_combinaison = "mode_lineaire"
        else : # "mono" and default # cas simulation simple
            type_simulation = "mono_simulation"
            mode_combinaison = ""

        # objet resultat de simulation
        # depend du type de simulation (simple ou multiple)
        resultat = Resultat( type_simulation, mode_combinaison )

        #**************************************************************
        # Lance la simulation (2 fois) : (1) simulation et sauvegarde 
        # fichiers en preparation de la demande de telechargement des
        # resultats, (2) simulation dans le but d'en afficher les
        # resultats (representations graphiques...)
        #**************************************************************
        try:
            resultat.simuler( self.exp, self.scn.espace_exploitation )
        except Exception, e:

            cr_ok = False
            message_erreur = message_erreur + " *** " + get_message_exception(e)
            print "\n\n\n message_erreur : ", message_erreur # !!!
            self.report.append( message_erreur )

            self.cr_ok = cr_ok

        else : # bon deroulement de simuler, continuer
        # (simulation sans levee d'erreur)
            self.scn.resultat = resultat

        if not cr_ok : # echec de simulation
        # (au niveau de definition.prendreEnCompteDans ou resultat.simuler)
            # maj erreur_echec_simulation
            erreur_echec_simulation.activer()
            erreur_echec_simulation.set_message( message_erreur )

        # Liste messages d'erreur en report
        if erreur_echec_simulation.isActive() : # maj report selon erreur_echec_simulation
            cr_ok = False
            msg = _(u"Echec simulation : ") + erreur_echec_simulation.get_message()
            print "\n\n\n msg : ", msg # !!!
            self.report.append( msg )
        else :
            cr_ok = True
            msg = _(u"Simulation done")
            print "\n\n\n msg : ", msg # !!!
            self.report.append( msg )

        if cr_ok :
            self.data = self.scn.resultat.res.results_par_vues
    
        self.cr_ok = cr_ok
    
        self.help.append( help_msgs_menu_scn )
        self.help.append( help_msgs_menu_run )
        self.help.append( help_msgs_scncontent )


#******************************************************************************

