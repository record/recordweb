from django.forms import widgets
from rest_framework import serializers

from models import HelpText, Line
from models import WsModel, WsModelScenario
from models import WsScenario
from models import WsParameter, WsDocParameter
from models import WsResult

from django.contrib.auth.models import User
from record.bd_modeles_record.models import Individu #, MotDePasse

from models import Content # !!!

from utils import list_from_str

#******************************************************************************
# Common

class WsSerializer(serializers.ModelSerializer):

    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        fields = kwargs.pop('fields', None)

        # Instantiate the superclass normally
        super(WsSerializer, self).__init__(*args, **kwargs)

        if fields:
            # Drop any fields that are not specified in the 'fields' argument
            allowed = set(fields)
            existing = set(self.fields.keys())
            for field_name in existing - allowed:
                self.fields.pop(field_name)

#******************************************************************************
# Help !!! a revoir

class HelpTextSerializer(WsSerializer):
    content = serializers.RelatedField(many=True)
    class Meta :
        model = HelpText
        fields = ( 'subject', 'content' )
        depth = 1

class LineSerializer(WsSerializer):
    class Meta:
        model = Line
        fields = ('line', 'helptexts')

#******************************************************************************
# version database : WsModel &Co

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User

# class MotDePasseSerializer...( UserSerializer ): ... !!!

class IndividuSerializerLight( UserSerializer ):
#class IndividuSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Individu
        fields = ( 'username', 'url', )
        view_name = 'individu-detail'

class IndividuSerializerFull( UserSerializer ):
#class IndividuSerializer(serializers.HyperlinkedModelSerializer):
    responsabilities = serializers.HyperlinkedRelatedField(many=True, view_name='dbmodel-detail')
    scientific_responsabilities = serializers.HyperlinkedRelatedField(many=True, view_name='dbmodel-detail')
    software_responsabilities = serializers.HyperlinkedRelatedField(many=True, view_name='dbmodel-detail')
    class Meta:
        model = Individu
        depth = 2

class DbModelSerializerLight( serializers.HyperlinkedModelSerializer ):
    class Meta :
        model = WsModel
        fields = ( 'nom', 'url', 'nom_pkg' )
        view_name = 'dbmodel-detail'

class DbModelSerializerFull(WsSerializer):
    # without wsmodelscenarios (information dedicated to web)
    responsable = serializers.HyperlinkedRelatedField( view_name='individu-detail' )
    responsable_scientifique = serializers.HyperlinkedRelatedField( view_name='individu-detail' )
    responsable_informatique = serializers.HyperlinkedRelatedField( view_name='individu-detail' )
    class Meta :
        model = WsModel
        depth = 2

#******************************************************************************
# ESSAI EN COURS !!!!!!!!!!!!!!!!

#from django.forms import widgets
#from rest_framework import serializers
from models import SSnippet, LANGUAGE_CHOICES, STYLE_CHOICES
#from django.contrib.auth.models import User

class SSnippetSerializer(serializers.HyperlinkedModelSerializer):
    owner = serializers.Field(source='owner.username')
    highlight = serializers.HyperlinkedIdentityField(view_name='snippet-highlight', format='html')

    class Meta:
        model = SSnippet
        fields = ('url', 'highlight', 'owner',
                  'title', 'code', 'linenos', 'language', 'style')

#******************************************************************************
# WsModel and WsModelScenario

#------------------------------------------------------------------------------
# WsModelScenario

class ModelScenarioSerializerLight( serializers.HyperlinkedModelSerializer ):
    class Meta:
        model = WsModelScenario
        fields = ( 'nom_scenario', 'url', )

class ModelScenarioSerializerFull(WsSerializer):

    wsmodel = serializers.HyperlinkedRelatedField(view_name='wsmodel-detail')

    # menu
    menu_Scn = serializers.HyperlinkedIdentityField( view_name='scn-detail', lookup_field='id' )
    menu_ScnFull = serializers.HyperlinkedIdentityField( view_name='scnfull-detail', lookup_field='id' )
    menu_Res = serializers.HyperlinkedIdentityField( view_name='res-detail', lookup_field='id' )
    menu_ResFull = serializers.HyperlinkedIdentityField( view_name='resfull-detail', lookup_field='id' )

    class Meta:
        model = WsModelScenario

        # pour ordonner
        fields = ( "id", "nom_scenario", "wsmodel",
                   "menu_Scn", "menu_ScnFull", "menu_Res", "menu_ResFull",
                 )
        #depth=2

#------------------------------------------------------------------------------
# WsModel

class ModelSerializerLight( serializers.HyperlinkedModelSerializer ):
    wsmodelscenarios = ModelScenarioSerializerLight(many=True)
    class Meta :
        model = WsModel
        fields = ( 'nom', 'url', 'nom_pkg', 'wsmodelscenarios' )

class ModelSerializerFull(WsSerializer):
    #wsmodelscenarios = ModelScenarioSerializerFull(many=True)
    wsmodelscenarios = serializers.HyperlinkedRelatedField(many=True, view_name='wsmodelscenario-detail' )

    #essai tmp
    champessai_description_html = serializers.CharField(source='description', read_only=True, widget=widgets.Textarea, max_length=100000)
    def transform_champessai_description_html(self, obj, value):
        from django.contrib.markup.templatetags.markup import markdown
        return markdown(value)

    class Meta :
        model = WsModel
        #fields = ( ,,,)
        depth = 2

#******************************************************************************
# WsParameter and WsDocParameter

class DocParameterSerializer(WsSerializer):
    class Meta:
        model = WsDocParameter
        fields = ('unit', 'min', 'max','recommanded','typevalue')

class ParameterSerializerFull(WsSerializer):
    metadata = DocParameterSerializer()
    class Meta:
        model = WsParameter
        fields = ('condname', 'portname', 'values', 'metadata',)

class ParameterSerializerLight(WsSerializer):
    class Meta:
        model = WsParameter
        fields = ('condname', 'portname', 'values',)

#class ParameterSerializerBrut(WsSerializer):
#    metadata = serializers.RelatedField()
#    class Meta:
#        model = WsParameter

#******************************************************************************
# WsResult

class ResultSerializerFull(WsSerializer):
    class Meta:
        model = WsResult
        fields = ('result', 'scenario')
        depth = 2

class ResultSerializerLight(WsSerializer):
    class Meta:
        model = WsResult
        fields = ('result',)


#******************************************************************************
# WsScenario

# Common
class ScenarioSerializer(WsSerializer):

    def restore_object(self, attrs, instance=None):

        print "ScenarioSerializer.restore_object(), attrs : ", attrs
        if instance: # Update existing instance
            print "ScenarioSerializer.restore_object(), Update existing instance"

            if instance.experimentname_isactive :
                instance.experimentname = attrs.get( 'experimentname', instance.experimentname )
            if instance.duration_isactive :
                instance.duration = attrs.get( 'duration', instance.duration )
            if instance.seed_isactive :
                instance.seed = attrs.get( 'seed', instance.seed )
            if instance.replicaseed_isactive :
                instance.replicaseed = attrs.get( 'replicaseed', instance.replicaseed )
            if instance.replicanumber_isactive :
                instance.replicanumber = attrs.get( 'replicanumber', instance.replicanumber )
            if instance.begin_isactive :
                instance.begin = attrs.get( 'begin', instance.begin )

            p_attrs = attrs.get( 'parameterslist', None )
            if p_attrs is not None :
                (conversion_ok, parameterslist) = list_from_str( p_attrs )
                if conversion_ok :
                    for p in parameterslist :
                        parameter = instance.parameters.get( condname=p['condname'], portname=p['portname'] ) 
                        parameter.values = p['values']
                        parameter.save()

            print "ScenarioSerializer.restore_object(), instance : ", instance
            return instance

        # Create new instance
        print "ScenarioSerializer.restore_object(), Create new instance"
        return WsScenario(**attrs) # !!!?

    def validate(self, attrs):
    # interpretation de isactive en terme de saisie
    # - si 'champ_isactive' vaut False alors valeur de 'champ' non prise en
    #   consideration
    # - si 'champ_isactive' n'existe pas ou vaut True alors valeur de 'champ'
    #   prise en consideration (...mais ne sera effectivement prise en compte
    #   que si scenario.champ_isactive est True)

        def isOk( v, isactive ) :
            cr_ok = True
            if isactive is False : # as if unset
                cr_ok = False
            else : # 'isactive' absent ou True
                if v is None : # 'v' absent
                    cr_ok = False
            return cr_ok

        def supprimer( v, attrs ) :
            if v in attrs.keys() :
                attrs.__delitem__( v )

        print "__class__ : ", self.__class__
        print "validate, attrs (en entree) : ", attrs

        v = attrs.get( 'experimentname', None )
        isactive = attrs.get( 'experimentname_isactive', None )
        if not isOk( v, isactive ) : # ignored
            supprimer ( 'experimentname', attrs )
        supprimer ( 'experimentname_isactive', attrs )

        v = attrs.get( 'duration', None )
        isactive = attrs.get( 'duration_isactive', None )
        if not isOk( v, isactive ) :
            supprimer( 'duration', attrs )
        supprimer( 'duration_isactive', attrs )

        v = attrs.get( 'seed', None )
        isactive = attrs.get( 'seed_isactive', None )
        if not isOk( v, isactive ) :
            supprimer( 'seed', attrs )
        supprimer( 'seed_isactive', attrs )

        v = attrs.get( 'replicaseed', None )
        isactive = attrs.get( 'replicaseed_isactive', None )
        if not isOk( v, isactive ) :
            supprimer( 'replicaseed', attrs )
        supprimer( 'replicaseed_isactive', attrs )

        v = attrs.get( 'replicanumber', None )
        isactive = attrs.get( 'replicanumber_isactive', None )
        if not isOk( v, isactive ) :
            supprimer( 'replicanumber', attrs )
        supprimer( 'replicanumber_isactive', attrs )

        v = attrs.get( 'begin', None )
        isactive = attrs.get( 'begin_isactive', None )
        if not isOk( v, isactive ) :
            supprimer( 'begin', attrs )
        supprimer( 'begin_isactive', attrs )

        #...if cas d'erreur :
        #...    raise serializers.ValidationError("text explaining the error case...")
        #print "validate, attrs (en sortie) : ", attrs
        return attrs

    #def rendre_readonly_form(self):
    #    for field in self.fields.values() :
    #        field.widget.attrs['readonly']= True # 'readonly'

#------------------------------------------------------------------------------

class ScenarioSerializerFull(ScenarioSerializer):
    parameters = ParameterSerializerFull(many=True)
    result = ResultSerializerFull()
    class Meta :
        model = WsScenario
        depth = 2

class ScenarioSerializerLight(ScenarioSerializer):
    parameters = ParameterSerializerLight(many=True)
    result = ResultSerializerLight()

    def copie(self, another_serializer, fields ) :
        print "ScenarioSerializerLight.copie des champs : ", fields
        for field in fields :
            setattr( self, field, another_serializer.data.get(field) )

    class Meta :
        model = WsScenario
        depth = 2

class ScenarioSerializer_Form(ScenarioSerializer):
    class Meta :
        model = WsScenario
        depth = 2

class ScenarioSerializer_NoForm(ScenarioSerializer):

#   def __init__(self, *args, **kwargs):
#       super(ScenarioSerializer, self).__init__(*args, **kwargs)
#       print "ScenarioSerializer_NoForm.__init__, self.fields.keys() : ", self.fields.keys()

    class Meta :
        model = WsScenario
        read_only_fields = ( # all the fields
                'experimentname_isactive', 'experimentname',
                'duration_isactive', 'duration',
                'seed_isactive', 'seed',
                'replicaseed_isactive', 'replicaseed',
                'replicanumber_isactive', 'replicanumber',
                'begin_isactive', 'begin', 
                'parameterslist', )
                # 'parameters', 'result'
        depth = 2

#******************************************************************************

###############################################################################






#******************************************************************************
#
#    Ce qui suit est ancien, pour des/en tant qu'exemple (a jeter a priori)
#
#******************************************************************************

#******************************************************************************
# extraits ancienne classe

class vvvScenarioSerializer(WsSerializer):

    yyyparameters = serializers.RelatedField(many=True)
    #un_parametre = ParameterSerializer( source="un_parametre" , required="False")
    #!!!xxx un_parametre = ParameterSerializer( required="False")

    #experimentname = serializers.CharField( source='experimentname' ) 
    #duration = serializers.FloatField( source='duration' )
    #parameters = serializers.CharField( source='parameters', widget=widgets.Textarea, max_length=100000 )
    #metadata = serializers.CharField( source='metadata', widget=widgets.Textarea, max_length=100000, read_only=True )
    #cr_ok = serializers.BooleanField( source='cr_ok', read_only=True ) # read-only
    #report = serializers.CharField( source='report', widget=widgets.Textarea, max_length=100000, read_only=True ) # read-only
    #help = serializers.CharField( source='help', widget=widgets.Textarea, max_length=100000, read_only=True ) # read-only
    #nnn is_public_model = serializers.BooleanField( source='is_public_model', read_only=True ) # read-only
    #nnn vle_home = serializers.CharField( source='vle_home', read_only=True ) # read-only
    #nnn idmod = serializers.IntegerField( source='idmod', read_only=True ) # read-only
    #nnn idscn = serializers.IntegerField( source='idscn', read_only=True ) # read-only

    #pk = serializers.Field()  # Note: `Field` is an untyped read-only field.
    #!!! pour memo title = serializers.CharField(source='experimentname', required=False, max_length=100)
    # title = serializers.CharField(required=False, max_length=100)
    #title = serializers.Field() # read-only
    #code = serializers.CharField(widget=widgets.Textarea, max_length=100000)
    #linenos = serializers.BooleanField(required=False)
    #language = serializers.ChoiceField(choices=LANGUAGE_CHOICES, default='python')

    def xxx_rendre_readonly_form(self): # cpacakifaut... a garder pour data readonly
        print "rendre_readonly_form", self.fields
        for field in self.fields.values() :
            field.widget.attrs['readonly']= True # 'readonly'

    # Validation methods !!! a ecrire
    def validate_title(self, attrs, source): # !!!
        value = attrs[source]
        if False : #True :
            raise serializers.ValidationError(value+"JE VEUX UN AUTRE TITRE")
        return attrs

#******************************************************************************
# extraits ancienne classe

class SimuSerializer( WsSerializer ):

    def __init__(self, *args, **kwargs):

        # Instantiate the superclass normally
        super(SimuSerializer, self).__init__(*args, **kwargs)

#       field_names = set(self.Meta.fields_form)
#       print "field_names : ", field_names
#       for field_name,field in self.fields.iteritems() :
#           if field_name in field_names :
#               print " pseudo desactivation de field_name : ", field_name
#               field.widget.attrs['readonly']= True # 'readonly' (pseudo desactive)


#******************************************************************************

# Commun a tous controlers ?!!!
class ContentSerializer(serializers.Serializer):

    #pk = serializers.Field()  # Note: `Field` is an untyped read-only field.
    # ... idmod, idscn ... ?!!!

    # pour l'instant champs simples !!! a serializer ...
    data = serializers.CharField( required=False,
                                  max_length=100)
    # !!! en cours
    #data = vvvScenarioSerializer()
    report = serializers.CharField( required=False,
                                  widget=widgets.Textarea,
                                  max_length=100000)
    help = serializers.CharField( required=False,
                                  widget=widgets.Textarea,
                                  max_length=100000)

    def restore_object(self, attrs, instance=None):
        """
        Create or update a new content instance, given a dictionary
        of deserialized field values.

        (if this method was not defined, then deserializing
        data would simply return a dictionary of items)
        """

        if instance:

            # Update existing instance
            instance.data = attrs.get('data', instance.data)
            instance.report = attrs.get('report', instance.report)
            instance.help = attrs.get('help', instance.help)
            return instance

        # Create new instance
        return Content(**attrs)

#******************************************************************************

#
# OR
#
# class SnippetSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Snippet
#         fields = ('id', 'title', 'code', 'linenos', 'language', 'style')

#******************************************************************************

