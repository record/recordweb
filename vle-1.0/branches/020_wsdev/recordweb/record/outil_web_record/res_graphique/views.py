#-*- coding:utf-8 -*-

## @file record/outil_web_record/res_graphique/views.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File views.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
# View res_graphique
#
# Restitution/observation des resultats de la simulation effectuee d'un
# scenario. Parmi les differentes formes de restitution existantes, il s'agit 
# ici de restitution sous forme d'affichage graphique (representations
# graphiques). Deux possibilites :
# - representation graphique ou tous les traces ont la meme var en abscisse 
# - representation graphique ou tous les traces n'ont pas forcement la meme
#   var en abscisse 
# Les 2 possibilites sont implementees dans les cas de simulation simple et
# de simulation multiple.
#
# Fait partie du processus d'exploitation des scenarios par les utilisateurs,
# selon lequel il s'agit de choisir un scenario (d'un modele record), puis de
# le simuler apres en avoir visualise et eventuellement modifie la configuration
# (parametrage), et enfin d'exploiter sous diverses formes les resultats de
# la simulation effectuee (les visualiser a l'ecran, telecharger...).
#
#*****************************************************************************

from django.http import HttpResponse
from django.shortcuts import render_to_response

from record.outil_web_record.models.affichage_saisie.parametres_requete import ParametresRequete
from record.outil_web_record.models.affichage_saisie.choix_operations import ChoixOperations as CHX # noms des choix
from record.outil_web_record.models.affichage_saisie.appel_page import NomsPages, contexte_graphique_menu, contexte_configuration_graphique, contexte_affichage_graphique
from record.outil_web_record.models.affichage_saisie.affichage_saisie import transformationArtificielle, definir_Y, definir_XY
from record.outil_web_record.models.erreurs_gerees.erreurs_gerees import ErreursGerees as ERR # noms des erreurs

from record.outil_web_record.utils.commun_views import initialiserControleur

from record.utils import session
from record.utils.erreur import Erreur # gestion des erreurs
from configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

import threading
imageThreadLock = threading.Lock() # make sure methods for graphics do not overwrite each other

from django.utils.translation import ugettext as _

#*****************************************************************************
#
# AFFICHAGES/REPRESENTATIONS GRAPHIQUES DE RESULTAT DE SIMULATION DU SCENARIO 
#
#*****************************************************************************

##..
#*****************************************************************************\n
#
# Affichage du menu d'affichage graphique et sa preparation
#
#*****************************************************************************
def graphique_menu(request) :
    t_ecr.trait()
    t_ecr.message("(graphique_menu:) appel de initialiserControleur")
    initialiserControleur(request)

    #*************************************************************************
    # Preparation du contexte du menu d'affichage graphique
    #*************************************************************************

    scn = session.get(request, 'scn')

    le_modele_record = session.get(request, 'le_modele_record')

    flashes = [] # pas de flashes
    c = contexte_graphique_menu(request, flashes, scn, le_modele_record, formulaire_de_type_ctg=True )

    return render_to_response( NomsPages.NOM_page_graphique_menu, c)

##..
#*****************************************************************************\n
#
# Une simulation du scenario choisi a ete effectuee (scn.resultat existe). \n
# Le traitement effectue varie en fonction de id_operation qui caracterise
# l'endroit depuis lequel configuration_graphique est appele et la raison de
# l'appel (le type de representation graphique choisi est
# type_de_representation=id_operation). \n
# Si erreur decelee en cours de traitement, alors affichage menu d'affichage
# graphique avec erreur.
# Sinon : preparation et affichage page de configuration graphique.
#
#*****************************************************************************
def configuration_graphique(request) :
    t_ecr.trait()
    t_ecr.message("(configuration_graphique:) appel de initialiserControleur")
    initialiserControleur(request)

    #*************************************************************************
    # les informations en entree
    #*************************************************************************

    id_operation = None # par defaut
    if "id_operation" in request.POST.keys() :
        id_operation = request.POST["id_operation"]
    id_operation = int(id_operation)

    # Initialisations

    erreur_configuration_graphique = Erreur( ERR.NOM_erreur_configuration_graphique ) # desactivee par defaut

    cas_d_erreur = False # par defaut
    message_erreur = "" # par defaut

    #*************************************************************************
    # Cas de choix d'affichage/representation graphique :
	# Preparation et affichage page de configuration graphique.
    # Le traitement depend du type de simulation (simple, multiple)
    # et du type de representation (choix_graphY, choix_graphXY).
    #*************************************************************************
    if CHX.is_choix_graphique( id_operation ) :

        # Initialisations

        type_de_representation = id_operation
        scn = session.get(request, 'scn')
        type_simulation = scn.resultat.type_simulation

        if type_simulation == "multi_simulation" : # cas "multi_simulation" 
            type_capture_vars = scn.resultat.res.type_capture_vars
        else : # cas "mono_simulation" et de plus par defaut
            type_capture_vars = scn.resultat.res.type_capture_vars
            type_capture_vars = transformationArtificielle( type_capture_vars )

        if type_de_representation == CHX.VAL_choix_graphY :
            choix_graph = CHX.VAL_choix_graphY
            nom_template = NomsPages.NOM_page_configuration_graphiqueY 
        elif type_de_representation == CHX.VAL_choix_graphXY :
            choix_graph = CHX.VAL_choix_graphXY
            nom_template = NomsPages.NOM_page_configuration_graphiqueXY

    #*************************************************************************
    # Cas d'erreur (ne devrait pas arriver)
    #*************************************************************************
    else : # affichage du menu d'affichage graphique avec erreur
        cas_d_erreur = True
        message_erreur = message_erreur + " *** " + _(u"type de représentation graphique demandé inconnu")
        nom_template = NomsPages.NOM_page_graphique_menu 

    if cas_d_erreur : # echec affichage graphique
    # (quel que soit l'endroit/nature d'echec)
        # maj erreur_configuration_graphique
        erreur_configuration_graphique.activer()
        erreur_configuration_graphique.set_message( message_erreur )

    #*************************************************************************
    # Preparation du contexte d'appel
    #*************************************************************************

    # Partie commune 

    scn = session.get(request, 'scn')

    le_modele_record = session.get(request, 'le_modele_record')

    # Liste messages d'erreur en flashes
    flashes = [] # par defaut
    if erreur_configuration_graphique.isActive() : # maj flashes selon erreur_configuration_graphique
        texte_erreur = _(u"Erreur : l\'affichage graphique a échoué") + "."
        texte_message_erreur = _(u"Message d'erreur") + " : " + erreur_configuration_graphique.get_message() + "."
        flashes.append( texte_erreur )
        flashes.append( texte_message_erreur )

    # Preparation du contexte de page de configuration graphique
    # (page_configuration_graphiqueY ou page_configuration_graphiqueXY 
    # selon cas)
    if nom_template == NomsPages.NOM_page_configuration_graphiqueY or nom_template == NomsPages.NOM_page_configuration_graphiqueXY  :
        c = contexte_configuration_graphique(request, flashes, scn, le_modele_record, type_simulation, type_capture_vars, choix_graph, formulaire_de_type_ctg=True )

    # Preparation du contexte du menu d'affichage graphique
    elif nom_template == NomsPages.NOM_page_graphique_menu :
        c = contexte_graphique_menu(request, flashes, scn, le_modele_record, formulaire_de_type_ctg=True )

    else :
        c = {} # vide

    return render_to_response(nom_template, c)

##..
#*****************************************************************************\n
#
# Une simulation du scenario choisi a ete effectuee (scn.resultat existe). \n
# La configuration (choix des donnees a representer...) de representation
# graphique a ete choisie. \n
# Le traitement effectue varie en fonction de id_operation qui caracterise
# l'endroit depuis lequel affichage_graphique est appele et la raison de
# l'appel (le type de representation graphique choisi est
# type_de_representation=id_operation). \n
# Tout d'abord traitement de prise en compte de la configuration de la
# representation graphique. Ensuite preparation affichage graphique puis
# affichage graphique, sauf si erreur decelee en cours de traitement, auquel
# cas preparation et affichage page de configuration graphique ou page
# graphique menu (selon erreur) avec erreur.
#
#*****************************************************************************
def affichage_graphique(request):
    t_ecr.trait()
    t_ecr.message("(affichage_graphique:) appel de initialiserControleur")
    initialiserControleur(request)

    #*************************************************************************
    # les informations en entree
    #*************************************************************************

    id_operation = None # par defaut
    if "id_operation" in request.POST.keys() :
        id_operation = request.POST["id_operation"]
    id_operation = int(id_operation)

    # Initialisations

    erreur_affichage_graphique = Erreur( ERR.NOM_erreur_affichage_graphique ) # desactivee par defaut

    cas_d_erreur = False # par defaut
    message_erreur = "" # par defaut

    #*************************************************************************
    # Cas d'affichage de representation graphique a partir de sa configuration
    # (donnees a representer...) : prise en compte configuration, construction
    # graphique, preparation affichage graphique
    #*************************************************************************
    if CHX.is_choix_graphique( id_operation ) :

        # Initialisations

        type_de_representation = id_operation
        scn = session.get(request, 'scn')
        type_simulation = scn.resultat.type_simulation

        parametres_post = ParametresRequete( request.POST )

        titre_du_trace = parametres_post.get_valeur( "titre" ) # iiiii ajouter gestion du cas ou il n'existe pas

        #*********************************************************************
        # liste_XY : liste des couples (X,Y) a tracer
        # le decodage de parametres_post depend du type de simulation
        # et du type de representation
        #*********************************************************************
        liste_XY = []
        cr_ok = False # par defaut

        if type_de_representation == CHX.VAL_choix_graphY :
            cr_ok = definir_Y( liste_XY, parametres_post, type_simulation )
        elif type_de_representation == CHX.VAL_choix_graphXY :
            cr_ok = definir_XY( liste_XY, parametres_post, type_simulation )

        if not cr_ok : # mauvais deroulement

            cas_d_erreur = True
            texte_erreur = "aucun trace (X,Y) n'a été sélectionné pour la représentation graphique"
            message_erreur = message_erreur + " *** " + texte_erreur
            texte_et_apres = "Sélectionner au moins un couple (X,Y), c\'est à dire dans le tableau la variable en abscisse et au moins une variable en ordonnée avant de \'Valider les choix\'"
            message_erreur = message_erreur + " *** " + texte_et_apres

            # page de configuration graphique (depend de type_de_representation)
            if type_de_representation == CHX.VAL_choix_graphY :
                nom_template = NomsPages.NOM_page_configuration_graphiqueY 
            elif type_de_representation == CHX.VAL_choix_graphXY :
                nom_template = NomsPages.NOM_page_configuration_graphiqueXY

        else : # bon deroulement de la prise en compte configuration
            # poursuite du traitement (construction du graphique, 
            # preparation affichage graphique)

            # indicateur pour la construction des legendes
            if type_de_representation == CHX.VAL_choix_graphY :
                memeAbscisse = True
            elif type_de_representation == CHX.VAL_choix_graphXY :
                memeAbscisse = False

            session.get(request, 'scn').resultat.res.setRepresentationGraphique( liste_XY, titre_du_trace, memeAbscisse )
            session.sauver(request)

            # page d'affichage graphique
            nom_template = NomsPages.NOM_page_affichage_graphique 

    #*************************************************************************
    # Cas d'erreur (ne devrait pas arriver)
    #*************************************************************************
    else : # affichage page du choix des donnees representees avec erreur
        cas_d_erreur = True
        message_erreur = message_erreur + " *** " + "type de représentation graphique demandé inconnu"

        # page graphique menu
        nom_template = NomsPages.NOM_page_graphique_menu 

    if cas_d_erreur : # echec affichage graphique
    # (quel que soit l'endroit/nature d'echec)
        # maj erreur_affichage_graphique
        erreur_affichage_graphique.activer()
        erreur_affichage_graphique.set_message( message_erreur )

    #*************************************************************************
    # Preparation du contexte d'appel
    #*************************************************************************

    # Partie commune 

    scn = session.get(request, 'scn')

    le_modele_record = session.get(request, 'le_modele_record')

    # Liste messages d'erreur en flashes
    flashes = [] # par defaut
    if erreur_affichage_graphique.isActive() : # maj flashes selon erreur_affichage_graphique
        texte_erreur = "Erreur : l\'affichage graphique a échoué."
        texte_message_erreur = "Message d'erreur : " + erreur_affichage_graphique.get_message() + "."
        flashes.append( texte_erreur )
        flashes.append( texte_message_erreur )

    # Preparation du contexte d'affichage/representation graphique
    if nom_template == NomsPages.NOM_page_affichage_graphique :
        c = contexte_affichage_graphique(request, flashes, scn, le_modele_record, formulaire_de_type_ctg=True )

    # Preparation du contexte de page de configuration graphique
    # (page_configuration_graphiqueY ou page_configuration_graphiqueXY selon cas)
    elif nom_template == NomsPages.NOM_page_configuration_graphiqueY or nom_template == NomsPages.NOM_page_configuration_graphiqueXY  :

        choix_graph = type_de_representation

        if type_simulation == "multi_simulation" : # cas "multi_simulation" 
            type_capture_vars = scn.resultat.res.type_capture_vars
        else : # cas "mono_simulation" et de plus par defaut
            type_capture_vars = scn.resultat.res.type_capture_vars
            type_capture_vars = transformationArtificielle( type_capture_vars )

        c = contexte_configuration_graphique(request, flashes, scn, le_modele_record, type_simulation, type_capture_vars, choix_graph, formulaire_de_type_ctg=True )

    # Preparation du contexte du menu d'affichage graphique
    elif nom_template == NomsPages.NOM_page_graphique_menu :
        c = contexte_graphique_menu(request, flashes, scn, le_modele_record, formulaire_de_type_ctg=True )

    else :
        c = {} # vide

    return render_to_response(nom_template, c)

##..
#*****************************************************************************\n
#
# Une simulation du scenario choisi a ete effectuee (scn.resultat existe). \n
# La configuration (choix des donnees a representer...) de representation
# graphique a ete choisie. La representation graphique a ete construite
# (scn.resultat.res.rg existe). \n
# Renvoie l'image de la representation graphique
#
#*****************************************************************************
def representation_graphique(request):
    t_ecr.trait()
    t_ecr.message("(representation_graphique:) appel de initialiserControleur")
    initialiserControleur(request)

    # imageThreadLock to prevent threads from writing over each other's graphics
    # we don't want different threads to write on each other's canvases,
    # so make sure we have a new one
    global imageThreadLock
    imageThreadLock.acquire() # lock graphics

    buffer = session.get(request, 'scn').resultat.afficherRepresentationGraphique()
    
    imageThreadLock.release() # unlock graphics

    response = HttpResponse( buffer.getvalue(), content_type='image/png' )
    # response['Content-Disposition'] = str( 'inline; filename=graph' )

    return response

##..
#*****************************************************************************\n
#
# Une simulation du scenario choisi a ete effectuee (scn.resultat existe). \n 
# La configuration (choix des donnees a representer...) de representation
# graphique a ete choisie. La representation graphique a ete construite
# (scn.resultat.res.rg existe). \n
# Enregistre le fichier de la derniere representation graphique (en cours) \n 
# Preparation et affichage graphique (de la representation graphique en cours)
#
#*****************************************************************************
def conservation_graphique(request):
    t_ecr.trait()
    t_ecr.message("(conservation_graphique:) appel de initialiserControleur")
    initialiserControleur(request)

    parametres_post = ParametresRequete( request.POST )

    nom = parametres_post.get_valeur( "nomfichier" ) # iiiii ajouter gestion du cas ou il n'existe pas

    repertoire = session.get(request, 'scn').espace_exploitation.preparerRepertoireExploitationRestitutions()
    nom_fichier_absolu = repertoire +'/'+ nom
    session.get(request, 'scn').resultat.enregistrerRepresentationGraphique( nom_fichier_absolu )


    #debut_MARS2014
    #*************************************************************************
    # Construction d'un eventuel trace graphique supplementaire, dans lequel
    # ajout de mean, max et min :
    # - a condition qu'il s'agisse d'un cas ou toutes les courbes a tracer
    #   ont la meme abscisse
    # - Quand l'utilisateur demande a 'conserver la representation graphique
    #   sous forme de fichier', ce graphique supplementaire est produit et
    #   enregistre dans un fichier dont le nom est derive du nom du trace 
    #   donne par l'utilisateur (pas d'affichage ecran)

    from record.utils import graphique
    from record.utils.graphique import Graphique
    import os
    filename,fileextension = os.path.splitext( nom_fichier_absolu )
    othername = filename + "__more__" + fileextension
    image = session.get(request, 'scn').resultat.res.rg.produireGraphiqueMore()
    if image is not None :
        graphique.getImageFile( image, othername )

    #*************************************************************************
    #fin_MARS2014


    session.get(request, 'scn').resultat.memoriserNomDernierFichierRepresentationGraphique( nom )
    session.sauver(request)

    #*************************************************************************
    # Preparation du contexte d'affichage/representation graphique
    #*************************************************************************

    scn = session.get(request, 'scn')

    le_modele_record = session.get(request, 'le_modele_record')

    # Message en flashes
    message = "Fichier " + nom + " conservé."
    flashes = [ message ]

    c = contexte_affichage_graphique(request, flashes, scn, le_modele_record, formulaire_de_type_ctg=True  )

    return render_to_response(NomsPages.NOM_page_affichage_graphique, c)

#*****************************************************************************

