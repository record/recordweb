#-*- coding:utf-8 -*-

## @file record/outil_web_record/configs/conf_vle.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File conf_vle.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

from django.conf import settings

#*****************************************************************************
#
# CONFIGURATION
#
#*****************************************************************************

##..
#*****************************************************************************\n
#
# vle
#
#*****************************************************************************
class CONF_vle(object) :

    ## vle_home est la valeur initiale de la variable d'environnement
    # VLE_HOME (donnee lors du settings).
    # les repertoires VLE_HOME attribues a chaque session/connexion sont
    # construits sous vle_home
    vle_home = settings.VLE_HOME

#*****************************************************************************

