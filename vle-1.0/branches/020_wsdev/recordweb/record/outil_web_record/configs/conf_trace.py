#-*- coding:utf-8 -*-

## @file record/outil_web_record/configs/conf_trace.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File conf_trace.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# CONFIGURATION
#
#*****************************************************************************

from django.conf import settings

##..
#*****************************************************************************\n
#
# trace (a l'ecran)
#
#*****************************************************************************
class CONF_trace(object) :

    # Option avec ou sans affichage des traces a l'ecran
    trace_ecran = True    # True # False # True # False # True

    # Option avec ou sans affichage des traces erreur (a l'ecran)
    trace_erreur = True   # True # False # True # False # True

#*****************************************************************************

