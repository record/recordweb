#-*- coding:utf-8 -*-

## @file record/outil_web_record/configs/conf_conf_web.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File conf_conf_web.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# CONFIGURATION
#
#*****************************************************************************

#*****************************************************************************
#
# configuration web
#
#*****************************************************************************

##..
#*****************************************************************************\n
#
# Constantes : des valeurs attribuees par defaut a des informations affichees
#
#*****************************************************************************
class CONF_conf_web(object) :

    # Valeurs s'il n'est pas donne/applique de configuration web 

    TXTSTANDARD_prefixe_titre_scenario = "Scénario"
    TXTSTANDARD_description_scenario = ""
    TXTSTANDARD_help_scenario = ""

    TXTSTANDARD_titre_appli = "Application standard"
    TXTSTANDARD_description_appli = "Application standard, sans personnalisation"
    TXTSTANDARD_help_appli = "pas de configuration web donnée/appliquée"

    TXTSTANDARD_titre_page_experience = "Informations générales (début de simulation, durée de simulation...)"
    TXTSTANDARD_description_page_experience = "Page d'informations sur l'expérience"
    TXTSTANDARD_help_page_experience = "Correspond sous gvle aux informations des zones 'Experiment' et 'Simulation' de la page 'Project'"

    TXTSTANDARD_titre_page_plan = "Gestion de l'alea pour les simulations multiples"
    TXTSTANDARD_description_page_plan = "Page de configuration de l'alea du plan d'expérience pour le cas de simulation multiple."
    TXTSTANDARD_help_page_plan = "Une simulation multiple pourra être effectuée selon un plan d'expérience en mode linéaire (combinaison linéaire de chaque index des valeurs), pas en mode total (produit cartésien de chaque valeur). Correspond sous gvle aux informations de la zone 'Plan' de la page 'Project'"

    TXTSTANDARD_prefixe_titre_page_condition = "Conditions expérimentales (paramètres et/ou variables d'entrée)"
    TXTSTANDARD_description_page_condition = ""
    TXTSTANDARD_help_page_condition = "Correspond aux paramètres (ports) de la condition du modèle de même nom"
    TXTSTANDARD_prefixe_web_name_page_condition = " "

    TXTSTANDARD_prefixe_titre_page_view = "Vue (sorties) "
    TXTSTANDARD_description_page_view = ""
    TXTSTANDARD_help_page_view = ""

    # cas particulier sans configuration web sauf pour dictionnaire issu
    # d'une configuration web, adjoint a l'application standard
    TXTSTANDARDAVECDICO_titre_appli = "Application standard (avec dictionnaire)"
    TXTSTANDARDAVECDICO_description_appli = "Application standard, sans personnalisation (sauf dictionnaire)"
    TXTSTANDARDAVECDICO_help_appli = "pas de configuration web donnée/appliquée, sauf pour dictionnaire dont provient la documentation des données"

    # Valeurs de forcage si non renseignement dans configuration web donnee/appliquee 

    TXTDEFAUT_prefixe_titre_scenario = "Scénario"
    TXTDEFAUT_description_scenario = ""
    TXTDEFAUT_help_scenario = ""

    TXTDEFAUT_titre_appli = "Application sans nom"
    TXTDEFAUT_description_appli = ""
    TXTDEFAUT_help_appli = ""

    # page web def ou page web res
    TXTDEFAUT_titre_page_web = "Page sans titre"
    TXTDEFAUT_description_page_web = ""
    TXTDEFAUT_help_page_web = ""

    TXTDEFAUT_web_name = "Donnée sans nom"
    TXTDEFAUT_web_name_donnee_exp_name = "Nom de l'expérience"
    TXTDEFAUT_web_name_donnee_exp_duration = "Durée de simulation"
    TXTDEFAUT_web_name_donnee_exp_begin = "Début de simulation"
    TXTDEFAUT_web_name_donnee_simu_seed = "Graine de simulation"
    TXTDEFAUT_web_name_donnee_plan_seed = "Graine de génération des graines de simulation"
    TXTDEFAUT_web_name_donnee_plan_number = "Nombre de répliquas de chaque combinaison"

    # Valeurs des id de recherche par defaut d'une donnee (InfosDonneeDef) 
    # dans un dictionnaire
    TXTDEFAUT_id_donnee_exp_name = "experiment_name"
    TXTDEFAUT_id_donnee_exp_duration = "Exp_Duration"
    TXTDEFAUT_id_donnee_exp_begin = "Exp_Begin"
    TXTDEFAUT_id_donnee_simu_seed = "Simu_Seed"
    TXTDEFAUT_id_donnee_plan_seed = "Plan_Seed"
    TXTDEFAUT_id_donnee_plan_number = "Plan_Number"

    # Valeurs de cas d'erreur

    TXTERREUR_titre_appli = "Situation d'erreur"
    TXTERREUR_description_appli = "Application vide/non applicable"
    TXTERREUR_help_appli = "pour un autre essai dans un autre cas de (modèle, scénario, application) : 'Retour au choix du scenario'"

#*****************************************************************************

