#-*- coding:utf-8 -*-

## @file record/outil_web_record/utils/commun_views.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File commun_views.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# Traitements communs aux/a des controleurs (views.py) de outil_web_record
#
#*****************************************************************************

import os

#from django.shortcuts import render_to_response

from record.outil_web_record.models.espace_exploitation.espace_exploitation import EspaceExploitation

from record.utils import session
from record.utils.dirs_and_files import verificationRepertoireOk, creerRepertoire
from configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

from django.utils.translation import ugettext as _


##..
#*****************************************************************************\n
# initialiserEnvironnementVle
#
# Fonction d'initialisation de l'environnement de VLE
#
# La variable d'environnement VLE_HOME a pour valeur initiale
# CONF_vle.vle_home (donnee lors du settings). Il lui est ensuite donne une
# valeur propre a la session qui est memorisee dans la session ('VLE_HOME')
# et dont le repertoire correspondant est cree s'il n'existait pas encore.
# Cette valeur est 'transmise' a VLE (cf setEnvironnementVle), ce apres quoi
# la valeur de VLE_HOME prise en consideration par VLE dans le process
# considere est session.get(request,'VLE_HOME') (et non pas
# os.environ.get('VLE_HOME') si jamais la variable d'environnement VLE_HOME
# du process changeait). C'est cette valeur ('VLE_HOME') memorisee en session
# qui est retransmise/rappelee a VLE (cf setEnvironnementVle) a chaque appel
# de initialiserEnvironnementVle.
#
#*****************************************************************************
def initialiserEnvironnementVle(request) :

    session.sauver(request)
    if not session.existe(request, 'VLE_HOME') :

        # determination nom_repertoire_VLE_HOME
        ident = request.session.session_key ###...pas ident = str(os.getpid())
        nom_repertoire_VLE_HOME = EspaceExploitation.determinerNomRepertoireVleHome( ident )

        if not verificationRepertoireOk( nom_repertoire_VLE_HOME ) : # n'existe pas
            creerRepertoire( nom_repertoire_VLE_HOME )

        # memorisation en session
        session.set(request, 'VLE_HOME', nom_repertoire_VLE_HOME)
        session.sauver(request)

    EspaceExploitation.setEnvironnementVle( session.get(request,'VLE_HOME') )


##..
#*****************************************************************************\n
# initialiserEnvironnementUpload
#
# Fonction d'initialisation de l'environnement de gestion des fichiers
# envoyes par l'utilisatuer (nom_repertoire_upload...).
#
#*****************************************************************************
def initialiserEnvironnementUpload(request) :
    if session.existe(request, 'VLE_HOME') :
        EspaceExploitation.setEnvironnementUpload( session.get(request,'VLE_HOME') )

##..
#*****************************************************************************\n
# initialiserControleur
#
# Fonction d'initialisation appelee par chaque controleur (chaque def de
# chaque views.py)
#
#*****************************************************************************
def initialiserControleur(request):
    initialiserEnvironnementVle(request)
    initialiserEnvironnementUpload(request)

#*****************************************************************************

