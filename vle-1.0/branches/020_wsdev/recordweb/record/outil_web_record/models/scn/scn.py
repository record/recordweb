#-*- coding:utf-8 -*-

## @file record/outil_web_record/models/scn/scn.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File scn.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

from record.outil_web_record.models.scenariovpz.scenariovpz import ScenarioVpz

from record.outil_web_record.models.definition.definition import Definition
from record.outil_web_record.models.conf_web.conf_web_gestion import get_conf_web_scenario
from record.outil_web_record.models.espace_exploitation.espace_exploitation import EspaceExploitation

from record.utils.dirs_and_files import verificationFichierOk

from record.utils.vle.exp import Exp

from configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

from django.utils.translation import ugettext as _

##..
#*****************************************************************************\n
# Modele : Scn
#
# Une exploitation d'un scenario par un utilisateur :
# visualisation, manipulation, simulation...
#
#*****************************************************************************
class Scn( object ):

    ## Construction/creation a partir du scenario (ScenarioVpz) et des
    # informations d'identification de son application associee de
    # configuration web (InfosApplicationId). \n
    # VLE_HOME est le repertoire VLE_HOME propre a la session/connexion. \n
    # Le booleen casDeFichierScenarioTelecharge sert a signaler qu'il s'agit
    # d'un nouveau fichier vpz du scenario qui a ete telecharge.
    def __init__( self, VLE_HOME, scenario_vpz, infos_application_id, casDeFichierScenarioTelecharge=False ) :

        nom_vpz = scenario_vpz.get_nom_vpz()
        nom_paquet = scenario_vpz.get_nom_paquet()
        description_modele_record = scenario_vpz.get_description_paquet()
        nom_pkg = scenario_vpz.get_nom_pkg()
        version_vle = scenario_vpz.get_version_vle()
        nom_repertoire_lot_pkgs = scenario_vpz.get_nom_repertoire_lot_pkgs()
        nom_repertoire_data = scenario_vpz.get_nom_repertoire_data()
        infos_generales_initiales_scenario = scenario_vpz.get_infos_generales_initiales_scenario()

        self.scenario_vpz_d_origine = None  # ScenarioVpz 
        self.scenario_vpz = None            # ScenarioVpz 
        self.definition = None              # Definition
        self.resultat = None                # Resultat de simulation
        self.espace_exploitation = None     # espace d'exploitation
        self.configuration_web = None       # configuration appli web 

        #*********************************************************************
        # scenario_vpz_d_origine
        #*********************************************************************

        self.scenario_vpz_d_origine = ScenarioVpz( nom_vpz, nom_paquet, description_modele_record, nom_pkg, version_vle, nom_repertoire_lot_pkgs, nom_repertoire_data, infos_generales_initiales_scenario )

        #*********************************************************************
        # espace_exploitation
        #*********************************************************************

        # nom_repertoire_lot_pkgs d'exploitation (il s'agit du sous repertoire
        # pkgs du repertoire VLE_HOME propre a la session/connexion)
        nom_repertoire_lot_pkgs = EspaceExploitation.getNomRepertoireExploitation( VLE_HOME )

        # nom_pkg d'exploitation (sous nom_repertoire_lot_pkgs d'exploitation)
        nom_pkg = EspaceExploitation.choisirNomSousRepertoireExploitation( EspaceExploitation.getBasePkgExploitation(), nom_repertoire_lot_pkgs, nom_pkg )

        # creation de l'espace d'exploitation
        self.espace_exploitation = EspaceExploitation( VLE_HOME, self.scenario_vpz_d_origine, nom_repertoire_lot_pkgs, nom_pkg, casDeFichierScenarioTelecharge )

        #*********************************************************************
        # scenario_vpz
        #*********************************************************************

        # changements dans scenario_vpz (relatif a espace_exploitation) par
        # rapport a scenario_vpz_d_origine :
        nom_repertoire_lot_pkgs = self.espace_exploitation.nom_repertoire_lot_pkgs
        # nom_repertoire_data inchange
        nom_pkg = self.espace_exploitation.nom_pkg
        nom_paquet = 'issu de ' + self.scenario_vpz_d_origine.get_nom_paquet()
        description_modele_record = description_modele_record + _(u"(il s'agit de la description du modele dans son etat initial)")
        nom_vpz = self.espace_exploitation.nom_vpz
        # copy.deepcopy de infos_generales_initiales_scenario ?

        self.scenario_vpz = ScenarioVpz( nom_vpz, nom_paquet, description_modele_record, nom_pkg, version_vle, nom_repertoire_lot_pkgs, nom_repertoire_data, infos_generales_initiales_scenario )

        #*********************************************************************
        # configuration_web
        #*********************************************************************

        # prealable : exp_scenario, Exp relatif a self.scenario_vpz
        nom_absolu_vpz = self.espace_exploitation.getNomAbsoluFichierVpz( self.scenario_vpz.nom_vpz )
        exp_scenario = Exp( nom_absolu_vpz )

        self.configuration_web = get_conf_web_scenario( infos_application_id, exp_scenario )

        #*********************************************************************
        # definition
        #*********************************************************************

        # initialisation de l'objet de definition du scenario

        # deja fait plus haut : exp_scenario = Exp( nom_absolu_vpz )

        self.definition = Definition( exp_scenario, self.configuration_web )

    ## Destruction/suppression
    def supprimer( self ) :
        self.espace_exploitation.supprimerRepExploitation()
        self.espace_exploitation.viderNomRepertoireLotPkgs()
        del self
