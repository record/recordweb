#-*- coding:utf-8 -*-

## @file record/bd_modeles_record/forms/user_forms.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File user_forms.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# Formulaires et Informations pour affichages dans pages html
#
#*****************************************************************************

from django import forms
import floppyforms as forms
from form_utils.forms import BetterForm # pour formulaire par categories

from record.outil_web_record.models.conf_web.infos_generales import InfosGeneralesAppli
from record.outil_web_record.models.conf_web.infos_generales import InfosGeneralesScenario

from record.outil_web_record.forms.infos_generales_forms import InfosGeneralesForm
from record.outil_web_record.forms.commun_forms import setFieldsConfigurationInfosListeScenarios

from django.utils.translation import ugettext as _
from transmeta import TransMeta

##..
#*****************************************************************************\n
#
# Information de presentation du scenario fictif qui permet (en derniere
# ligne de liste des scenarios) d'envoyer/choisir un nouveau scenario autre 
# que variante d'un scenario existant
#
#*****************************************************************************
class InformationScenarioFictif(object):

    ##..
    #*************************************************************************\n
    # Construction constante
    #\n*************************************************************************
    def __init__( self ) :

        nom_scenario = _(u"nouveau scénario")
        
        titre = "Nouveau scénario de simulation, a priori sans lien avec les scénarios existants"
        description = _(u"Cette dernière ligne est consacrée à l'envoi d'un nouveau scénario de simulation (fichier vpz) a priori sans lien avec les scénarios existants (listés ci-dessus)." )
        help = _(u"Si jamais le nouveau scénario est une variante d'un scénario existant, alors il est possible de donner le nouveau scénario sur la ligne du scénario existant, et ainsi de choisir un cas d'utilisation particulier." )

        #*********************************************************************
        # infos_generales et infos_generales_form
        # (a priori self.infos_generales ne sert plus, remplace par 
        # self.infos_generales_form)
        #*********************************************************************
        infos_generales = InfosGeneralesScenario()
        infos_generales.set( titre, description, help )
        # self.infos_generales = infos_generales
        self.infos_generales_form = InfosScenarioFictifForm( nom_scenario, infos_generales ) #iiiiiiiii

        #*********************************************************************
        # infos_application_associee et infos_application_associee_form
        # (a priori self.infos_application_associee ne sert plus, remplace
        # par self.infos_application_associee_form)
        #*********************************************************************

        # partie generale de l'unique application proposee : il s'agit de
        # l'application standard (par defaut)
        infos_generales_application = InfosGeneralesAppli()
        infos_generales_application.setValeursStandard()

        #self.infos_application_associee = infos_generales_application
        self.infos_application_associee_form = InfosApplicationAssocieeScenarioFictifForm( infos_generales_application )

##..
#*****************************************************************************\n
#
# Formulaire associe au scenario fictif relatif a InformationScenarioFictif
# (nouveau scenario).
#
# Formulaire construit a partir de nom_scenario et InfosGenerales
#
#*****************************************************************************
class InfosScenarioFictifForm( InfosGeneralesForm ):

    def __init__(self, nom_scenario, infos_generales, *args, **kwargs):

        super(InfosScenarioFictifForm, self).__init__( infos_generales, *args, **kwargs)

        # nom_scenario
        self.nom_scenario = nom_scenario

        # titre_scenario
        self.titre_scenario = infos_generales.titre

        # tous les champs read-only et autre configuration
        setFieldsConfigurationInfosListeScenarios( self.fields )

    class Meta :

        #*********************************************************************
        # tout dans un seul encadre
        #*********************************************************************
        fieldsets = (
        ( "SCENARIO_FICTIF", { #'description': _(u""),

                     'fields': (
                                'titre',
                                'description',
                                'help',
                               ),

                     'legend': _(u"Nouveau scénario"),
                   } ),
        )

##..
#*****************************************************************************\n
#
# Formulaire associe a infos_application_associee de scenario fictif
# InformationScenarioFictif (nouveau scenario)
#
# Formulaire construit a partir de InfosGenerales
#
#*****************************************************************************
class InfosApplicationAssocieeScenarioFictifForm( InfosGeneralesForm ):

    def __init__(self, infos_generales, *args, **kwargs):

        super(InfosApplicationAssocieeScenarioFictifForm, self).__init__(infos_generales, *args, **kwargs)

        # nom_application
        self.nom_application = infos_generales.titre

        # tous les champs read-only et autre configuration
        setFieldsConfigurationInfosListeScenarios( self.fields )


##..
#*****************************************************************************\n
#
# Formulaire UpLoadFile
#
#*****************************************************************************
#class UploadFileForm(forms.Form):
#    file  = forms.FileField()

class FormulaireFichierScenario(forms.Form):
    new_fichier_scenario = forms.FileField( label=_(u'Fichier')+' ' , required=False)
    new_titre_scenario = forms.CharField(max_length=50, label=_(u'Titre (optionnel)')+' ' , required=False)
    new_description_scenario = forms.CharField(max_length=50, label=_(u'Description (optionnel)')+' ' , required=False)

class FormulaireFichierData(forms.Form):
    new_fichier_data = forms.FileField( label=_(u'Fichier')+' ' , required=False)

#*****************************************************************************

