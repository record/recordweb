#-*- coding:utf-8 -*-

## @file record/outil_web_record/models/affichage_saisie/appel_page.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File appel_page.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# Classes et methodes dediees a l'appel de pages
#
#*****************************************************************************

from django.core.context_processors import csrf

from django import forms

from record.outil_web_record.models.affichage_saisie.choix_operations import ChoixOperations as CHX # noms des choix
from record.outil_web_record.models.affichage_saisie.affichage_saisie import DateSaisie

from record.outil_web_record.forms.infos_generales_forms import InfosApplicationForm
from record.outil_web_record.forms.infos_generales_forms import InfosScenarioForm

from record.outil_web_record.models.affichage_saisie.forms import FormulaireFichierScenario, FormulaireFichierData
from record.outil_web_record.models.affichage_saisie.forms import InformationScenarioFictif

from record.bd_modeles_record.models import ModeleRecord as ModeleRecordBd

from record.bd_modeles_record.forms.forms import MotDePasseAskingForm

# le choix entre formulaire 'classique' (user_forms) et formulaire par
# categories (user_forms_ctg) est fait au niveau de chaque traitement (en
# fonction du parametre formulaire_de_type_ctg d'appel)
#from record.bd_modeles_record.forms.user_forms import ModeleRecordFormComplet # formulaire 'classique'
#from record.bd_modeles_record.forms.user_forms_ctg import ModeleRecordFormComplet # formulaire par categories
#from record.bd_modeles_record.forms.user_forms import ModeleRecordFormPartiel # formulaire 'classique'
#from record.bd_modeles_record.forms.user_forms_ctg import ModeleRecordFormPartiel # formulaire par categories

from record.utils.dirs_and_files import getListeFichiersNonCachesRep


##..
#*****************************************************************************\n
# Modele : NomsPages
#
# Des constantes (les noms des pages)
#
#*****************************************************************************
class NomsPages(object) :

    NOM_page_modele_record_mdp = 'scn_selection/modele_record_mdp.html'

    NOM_page_liste_scenarios = 'scn_selection/liste_scenarios.html'

    NOM_page_affichage_scenario = 'scn_edition/scenario.html'

    NOM_page_simulation_menu = 'scn_simulation/simulation_menu.html'

    NOM_page_restitution_menu = 'res_menu/restitution_menu.html'

    NOM_page_telechargement_menu = 'res_telechargement/telechargement_menu.html'

    NOM_page_affichage_numerique = 'res_numerique/affichage_numerique.html'

    # menu de choix du type de representation graphique :
    NOM_page_graphique_menu = 'res_graphique/graphique_menu.html'

    # pages de configuration representation graphique (choix des donnees representees...)
    NOM_page_configuration_graphiqueY = 'res_graphique/configuration_graphiqueY.html'
    NOM_page_configuration_graphiqueXY = 'res_graphique/configuration_graphiqueXY.html'

    NOM_page_affichage_graphique = 'res_graphique/affichage_graphique.html'

    NOM_page_fin = 'the_end.html'

#*****************************************************************************
#
# Des methodes de constitution de contextes d'appel de pages
#
#*****************************************************************************

##..
#*****************************************************************************\n
# Methode contexte_modele_record_mdp : constitue et retourne le contexte de
# la page NOM_page_modele_record_mdp . \n
# formulaire_de_type_ctg est False pour cas de formulaire 'classique', \n
# et True pour cas de formulaire ou les informations sont presentees selon des
# categories.
#\n*****************************************************************************
def contexte_modele_record_mdp(request, flashes, modele_record_id, formulaire_de_type_ctg=False) :

    # Formulaire de saisie du mot de passe (du modele record prive)
    formulaire_mot_de_passe = MotDePasseAskingForm()
    # informations complementaires (a faire suivre)
    formulaire_mot_de_passe.modele_record_id = modele_record_id
            
    # rappel des infos partielles a titre d'information/memo
    modele_record_bd = ModeleRecordBd.objects.get(id=modele_record_id)
    if formulaire_de_type_ctg : # formulaire par categories
        from record.bd_modeles_record.forms.user_forms_ctg import ModeleRecordFormPartiel
    else : # formulaire 'classique'
        from record.bd_modeles_record.forms.user_forms import ModeleRecordFormPartiel
    formulaire_modele_record = ModeleRecordFormPartiel(instance=modele_record_bd)

    c = {} # le contexte

    c['formulaire_mot_de_passe'] = formulaire_mot_de_passe
    c['formulaire_modele_record'] = formulaire_modele_record
    if formulaire_de_type_ctg :
        c['formulaire_de_type_ctg'] = "cas de formulaire (informations partielles) ou les informations sont presentees selon des categories"
    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

##..
#*****************************************************************************\n
# Methode contexte_liste_scenarios : constitue et retourne le contexte de
# la page NOM_page_liste_scenarios \n
# formulaire_de_type_ctg est False pour cas de formulaire 'classique', \n
# et True pour cas de formulaire ou les informations sont presentees selon des
# categories.
#\n*****************************************************************************
def contexte_liste_scenarios(request, flashes, le_modele_record, nom_repertoire_data, formulaire_modele_record, formulaire_de_type_ctg=False) :

    # pour memo : request.LANGUAGE_CODE 

    c = {}

    if le_modele_record is not None :
        c['le_modele_record'] = le_modele_record
    # else : cas d'un modele record inexploitable/indisponible (cf flashes)

    c['formulaire_modele_record'] = formulaire_modele_record
    if formulaire_de_type_ctg :
        c['formulaire_de_type_ctg'] = "cas de formulaire ou les informations sont presentees selon des categories"

    contenu_repertoire_data = None
    if nom_repertoire_data is not None :
        contenu_repertoire_data = getListeFichiersNonCachesRep( nom_repertoire_data )
    c['contenu_repertoire_data'] = contenu_repertoire_data

    c['le_scenario_fictif'] = InformationScenarioFictif()
    c['formulaire_fichier_scenario'] = FormulaireFichierScenario()
    c['formulaire_fichier_data'] = FormulaireFichierData()

    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

##..
#*****************************************************************************\n
# Methode contexte_affichage_scenario : constitue et retourne le contexte de
# la page d'affichage du scenario
# formulaire_de_type_ctg est False pour cas de formulaire 'classique', \n
# et True pour cas de formulaire ou les informations sont presentees selon des
# categories.
#\n*****************************************************************************
def contexte_affichage_scenario(request, flashes, scn, nom_repertoire_data, formulaire_modele_record, formulaire_de_type_ctg=False) :

    formulaire_infos_scenario = InfosScenarioForm( scn.scenario_vpz, scn.scenario_vpz_d_origine )
    formulaire_infos_application = InfosApplicationForm( scn.definition.infos_generales )

    c = {}

    # operations proposees : apres avoir eventuellement modifie le scenario,
    # l'usr quittera la page par choix_validationIntermediaire ou
    # choix_validationFinale (ou retour en arriere)
    c['choix_validationIntermediaire'] = CHX.NOM_choix_validationIntermediaire
    c['choix_validationFinale'] = CHX.NOM_choix_validationFinale

    # autres constantes a transmettre pour identifier les operations
    c['operation_validation'] = CHX.VAL_operation_validation
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo

    # constante utilisee dans saisie date (donnee "type_exp_begin")
    c['valeur_obsolete_saisie_date'] = DateSaisie.getValeurObsolete()

    c['scenario'] = scn.scenario_vpz
    c['definition'] = scn.definition
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine

    contenu_repertoire_data = None
    if nom_repertoire_data is not None :
        contenu_repertoire_data = getListeFichiersNonCachesRep( nom_repertoire_data )
    c['contenu_repertoire_data'] = contenu_repertoire_data

    c['formulaire_modele_record'] = formulaire_modele_record
    if formulaire_de_type_ctg :
        c['formulaire_de_type_ctg'] = "cas de formulaire ou les informations sont presentees selon des categories"

    c['formulaire_infos_scenario'] = formulaire_infos_scenario
    c['formulaire_infos_application'] = formulaire_infos_application


# debut essai temporaire (formulaire et date)
# Le code :
#   from django import forms
#   from datetime import date
#   class DateForm(forms.Form):
#       w = forms.DateInput(format = '%Y-%m-%d')
#       mydate = forms.DateField( initial=date.today(), widget=w, input_formats=('%Y-%m-%d',)) 
#       mydate.widget.attrs['class']= "datepicker"
#   c['DATE_FORM'] = DateForm()
#
# avec .html :
#   {% if DATE_FORM %}
#       <form action="/owrec/usr/liste_scenarios/333/", method="post">
#           {% csrf_token %}
#           {{ DATE_FORM }}
#       </form>
#   {% endif %}
# fin essai temporaire (formulaire et date)


    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

##..
#*****************************************************************************\n
# Methode contexte_simulation_menu : constitue et retourne le contexte de
# la page NOM_page_simulation_menu
# formulaire_de_type_ctg est False pour cas de formulaire 'classique', \n
# et True pour cas de formulaire ou les informations sont presentees selon des
# categories.
#\n*****************************************************************************
def contexte_simulation_menu(request, flashes, scn, le_modele_record, formulaire_de_type_ctg=False) :

    # formulaire_modele_record (rappel des infos completes a titre d'information/memo)
    modele_record_bd = ModeleRecordBd.objects.get(id=le_modele_record.id)

    if formulaire_de_type_ctg : # formulaire par categories
        from record.bd_modeles_record.forms.user_forms_ctg import ModeleRecordFormComplet
    else : # formulaire 'classique'
        from record.bd_modeles_record.forms.user_forms import ModeleRecordFormComplet
    formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

    formulaire_infos_scenario = InfosScenarioForm( scn.scenario_vpz, scn.scenario_vpz_d_origine )
    formulaire_infos_application = InfosApplicationForm( scn.definition.infos_generales )

    c = {}

    # les 3 types de simulation sont actifs/proposes
    c['choix_simulationSimple'] = CHX.VAL_choix_simulationSimple
    c['choix_simulationMultipleModeLineaire'] = CHX.VAL_choix_simulationMultipleModeLineaire
    # notes_de_developpement appel_page.py, contexte_simulation_menu :
    # simulationMultipleModeTotal (a venir : 
    # suppression definitive de cette option/traitement)
    c['choix_simulationMultipleModeTotal'] = CHX.VAL_choix_simulationMultipleModeTotal
    #c['choix_simulationMultipleModeTotal'] = CHX.VAL_choix_a_cacher

    # autres constantes a transmettre pour identifier les operations
    c['choix_a_cacher'] = CHX.VAL_choix_a_cacher # valeur de comparaison
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo
    c['operation_retour_edition'] = CHX.VAL_operation_retour_edition

    c['scenario'] = scn.scenario_vpz
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine
    c['definition'] = scn.definition
    c['formulaire_modele_record'] = formulaire_modele_record
    if formulaire_de_type_ctg :
        c['formulaire_de_type_ctg'] = "cas de formulaire ou les informations sont presentees selon des categories"
    c['formulaire_infos_scenario'] = formulaire_infos_scenario
    c['formulaire_infos_application'] = formulaire_infos_application

    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

##..
#*****************************************************************************\n
# Methode contexte_restitution_menu : constitue et retourne le contexte de
# la page NOM_page_restitution_menu
# formulaire_de_type_ctg est False pour cas de formulaire 'classique', \n
# et True pour cas de formulaire ou les informations sont presentees selon des
# categories.
#\n*****************************************************************************
def contexte_restitution_menu(request, flashes, scn, le_modele_record, formulaire_de_type_ctg=False) :

    # formulaire_modele_record (rappel des infos completes a titre d'information/memo)
    modele_record_bd = ModeleRecordBd.objects.get(id=le_modele_record.id)

    if formulaire_de_type_ctg : # formulaire par categories
        from record.bd_modeles_record.forms.user_forms_ctg import ModeleRecordFormComplet
    else : # formulaire 'classique'
        from record.bd_modeles_record.forms.user_forms import ModeleRecordFormComplet
    formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

    formulaire_infos_scenario = InfosScenarioForm( scn.scenario_vpz, scn.scenario_vpz_d_origine )
    formulaire_infos_application = InfosApplicationForm( scn.definition.infos_generales )

    c = {}

    # les types de restitution : choix_num est actif/propose ou non
    # en fonction de type_simulation
    c['choix_graph'] = CHX.VAL_choix_graph
    c['choix_telechgt'] = CHX.VAL_choix_telechgt
    type_simulation = scn.resultat.type_simulation
    if type_simulation == "mono_simulation" :
        c['choix_num'] = CHX.VAL_choix_num
    else : # le choix 'affichage numerique' n'apparait pas dans le menu
        c['choix_num'] = CHX.VAL_choix_a_cacher

    # autres constantes a transmettre pour identifier les operations
    c['choix_a_cacher'] = CHX.VAL_choix_a_cacher # valeur de comparaison
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo
    c['operation_retour_edition'] = CHX.VAL_operation_retour_edition
    c['operation_retour_simulation'] = CHX.VAL_operation_retour_simulation

    c['scenario'] = scn.scenario_vpz
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine
    c['definition'] = scn.definition
    c['formulaire_modele_record'] = formulaire_modele_record
    if formulaire_de_type_ctg :
        c['formulaire_de_type_ctg'] = "cas de formulaire ou les informations sont presentees selon des categories"
    c['formulaire_infos_scenario'] = formulaire_infos_scenario
    c['formulaire_infos_application'] = formulaire_infos_application

    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

##..
#*****************************************************************************\n
# Methode contexte_telechargement_menu : constitue et retourne le contexte de
# la page NOM_page_telechargement_menu
# formulaire_de_type_ctg est False pour cas de formulaire 'classique', \n
# et True pour cas de formulaire ou les informations sont presentees selon des
# categories.
#\n*****************************************************************************
def contexte_telechargement_menu(request, flashes, scn, le_modele_record, formulaire_de_type_ctg=False) :

    # formulaire_modele_record (rappel des infos completes a titre d'information/memo)
    modele_record_bd = ModeleRecordBd.objects.get(id=le_modele_record.id)

    if formulaire_de_type_ctg : # formulaire par categories
        from record.bd_modeles_record.forms.user_forms_ctg import ModeleRecordFormComplet
    else : # formulaire 'classique'
        from record.bd_modeles_record.forms.user_forms import ModeleRecordFormComplet
    formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

    formulaire_infos_scenario = InfosScenarioForm( scn.scenario_vpz, scn.scenario_vpz_d_origine )
    formulaire_infos_application = InfosApplicationForm( scn.definition.infos_generales )

    c = {}

    # les 2 types de telechargement
    # (choix_telechgtDernierFichier est propose/actif sous condition)

    c['choix_telechgtDossier'] = CHX.VAL_choix_telechgtDossier

    # nom du dernier fichier de conservation d'une representation graphique
    # S'il n'existe pas (valeur None), alors il ne faut pas proposer le
    # telechargement correspondant)
    nom = scn.resultat.getNomDernierFichierRepresentationGraphique()
    choix_telechgtDernierFichier = CHX.VAL_choix_telechgtDernierFichier # par defaut
    if nom == None :
        nom = ""
        choix_telechgtDernierFichier = CHX.VAL_choix_a_cacher 
    c['choix_telechgtDernierFichier'] = choix_telechgtDernierFichier
    c['nom_dernier_fichier_rg'] = nom

    # autres constantes a transmettre pour identifier les operations
    c['choix_a_cacher'] = CHX.VAL_choix_a_cacher # valeur de comparaison
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo
    c['operation_retour_edition'] = CHX.VAL_operation_retour_edition
    c['operation_retour_simulation'] = CHX.VAL_operation_retour_simulation
    c['operation_retour_restitution'] = CHX.VAL_operation_retour_restitution

    c['scenario'] = scn.scenario_vpz
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine
    c['definition'] = scn.definition
    c['formulaire_modele_record'] = formulaire_modele_record
    if formulaire_de_type_ctg :
        c['formulaire_de_type_ctg'] = "cas de formulaire ou les informations sont presentees selon des categories"
    c['formulaire_infos_scenario'] = formulaire_infos_scenario
    c['formulaire_infos_application'] = formulaire_infos_application

    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

##..
#*****************************************************************************\n
# Methode contexte_affichage_numerique : constitue et retourne le contexte de
# la page d'affichage numerique
# formulaire_de_type_ctg est False pour cas de formulaire 'classique', \n
# et True pour cas de formulaire ou les informations sont presentees selon des
# categories.
#\n*****************************************************************************
def contexte_affichage_numerique(request, flashes, scn, le_modele_record, formulaire_de_type_ctg=False) :

    # formulaire_modele_record (rappel des infos completes a titre d'information/memo)
    modele_record_bd = ModeleRecordBd.objects.get(id=le_modele_record.id)

    if formulaire_de_type_ctg : # formulaire par categories
        from record.bd_modeles_record.forms.user_forms_ctg import ModeleRecordFormComplet
    else : # formulaire 'classique'
        from record.bd_modeles_record.forms.user_forms import ModeleRecordFormComplet
    formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

    formulaire_infos_scenario = InfosScenarioForm( scn.scenario_vpz, scn.scenario_vpz_d_origine )
    formulaire_infos_application = InfosApplicationForm( scn.definition.infos_generales )

    # les tableaux (avec tri) des variables en lignes (deja prets) :
    # 'type_capture_vars' (toutes vues confondues) et 'vue_vars' (par vue)

    #*************************************************************************
    # un tableau des variables en colonnes (prepare/construit ici) :
    # 'tableau_vars'
    #*************************************************************************
    tableau_vars = []
    nomvar_time = 'time' # donnee a placer en 1er
    tableau_vars.append( [ 'nom variable', 'type', 'timestep', 'diminutif' ] ) # entete

    for typage,dict_nom_valeurs in scn.resultat.res.type_capture_vars.iteritems() :

        if nomvar_time in dict_nom_valeurs.keys() : # time en 1er
            nomvar = nomvar_time
            donneesvar = dict_nom_valeurs[ nomvar ]
            ligne=[ nomvar, typage.type, typage.timestep, donneesvar.diminutif ]
            for valeur in donneesvar.valeurs :
                ligne.append( valeur )
            tableau_vars.append(ligne)

        for nomvar,donneesvar in dict_nom_valeurs.iteritems() : # autres vars
            if nomvar != nomvar_time :
                ligne=[ nomvar, typage.type, typage.timestep, donneesvar.diminutif ]
                for valeur in donneesvar.valeurs :
                    ligne.append( valeur )
                tableau_vars.append(ligne)
            # else : time deja pris en compte

    # transposee (en remplacant None par "")
    tableau_vars = map(lambda *row: list(row), *tableau_vars)
    for i,l in enumerate(tableau_vars) :
        for j,e in enumerate(l) :
            if e is None :
                tableau_vars[i][j]=""

    #*************************************************************************

    c = {}

    # autres constantes a transmettre pour identifier les operations
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo
    c['operation_retour_edition'] = CHX.VAL_operation_retour_edition
    c['operation_retour_simulation'] = CHX.VAL_operation_retour_simulation
    c['operation_retour_restitution'] = CHX.VAL_operation_retour_restitution

    c['scenario'] = scn.scenario_vpz
    c['type_capture_vars'] = scn.resultat.res.type_capture_vars
    c['vue_vars'] = scn.resultat.res.vue_vars
    c['tableau_vars'] = tableau_vars
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine
    c['definition'] = scn.definition
    c['formulaire_modele_record'] = formulaire_modele_record
    if formulaire_de_type_ctg :
        c['formulaire_de_type_ctg'] = "cas de formulaire ou les informations sont presentees selon des categories"
    c['formulaire_infos_scenario'] = formulaire_infos_scenario
    c['formulaire_infos_application'] = formulaire_infos_application

    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

##..
#*****************************************************************************\n
# Methode contexte_graphique_menu : constitue et retourne le contexte de
# la page NOM_page_graphique_menu
# formulaire_de_type_ctg est False pour cas de formulaire 'classique', \n
# et True pour cas de formulaire ou les informations sont presentees selon des
# categories.
#\n*****************************************************************************
def contexte_graphique_menu(request, flashes, scn, le_modele_record, formulaire_de_type_ctg=False) :

    # formulaire_modele_record (rappel des infos completes a titre d'information/memo)
    modele_record_bd = ModeleRecordBd.objects.get(id=le_modele_record.id)

    if formulaire_de_type_ctg : # formulaire par categories
        from record.bd_modeles_record.forms.user_forms_ctg import ModeleRecordFormComplet
    else : # formulaire 'classique'
        from record.bd_modeles_record.forms.user_forms import ModeleRecordFormComplet
    formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

    formulaire_infos_scenario = InfosScenarioForm( scn.scenario_vpz, scn.scenario_vpz_d_origine )
    formulaire_infos_application = InfosApplicationForm( scn.definition.infos_generales )

    c = {}

    # les 2 types de representations graphiques : les 2 choix sont
    # proposes/actifs sans condition, quel que soit le type de simulation 
    c['choix_graphXY'] = CHX.VAL_choix_graphXY
    c['choix_graphY'] = CHX.VAL_choix_graphY

    # autres constantes a transmettre pour identifier les operations
    c['choix_a_cacher'] = CHX.VAL_choix_a_cacher # valeur de comparaison
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo
    c['operation_retour_edition'] = CHX.VAL_operation_retour_edition
    c['operation_retour_simulation'] = CHX.VAL_operation_retour_simulation
    c['operation_retour_restitution'] = CHX.VAL_operation_retour_restitution

    c['scenario'] = scn.scenario_vpz
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine
    c['definition'] = scn.definition
    c['formulaire_modele_record'] = formulaire_modele_record
    if formulaire_de_type_ctg :
        c['formulaire_de_type_ctg'] = "cas de formulaire ou les informations sont presentees selon des categories"
    c['formulaire_infos_scenario'] = formulaire_infos_scenario
    c['formulaire_infos_application'] = formulaire_infos_application

    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

##..
#*****************************************************************************\n
# Methode contexte_configuration_graphique : constitue et retourne le contexte
# de la page NOM_page_configuration_graphiqueY ou
# NOM_page_configuration_graphiqueXY 
# (rq : le parametre choix_graph transmis vaut choix_graphY ou choix_graphXY)
# formulaire_de_type_ctg est False pour cas de formulaire 'classique', \n
# et True pour cas de formulaire ou les informations sont presentees selon des
# categories.
#\n*****************************************************************************
def contexte_configuration_graphique(request, flashes, scn, le_modele_record, type_simulation, type_capture_vars, choix_graph, formulaire_de_type_ctg=False) :

    # formulaire_modele_record (rappel des infos completes a titre d'information/memo)
    modele_record_bd = ModeleRecordBd.objects.get(id=le_modele_record.id)

    if formulaire_de_type_ctg : # formulaire par categories
        from record.bd_modeles_record.forms.user_forms_ctg import ModeleRecordFormComplet
    else : # formulaire 'classique'
        from record.bd_modeles_record.forms.user_forms import ModeleRecordFormComplet
    formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

    formulaire_infos_scenario = InfosScenarioForm( scn.scenario_vpz, scn.scenario_vpz_d_origine )
    formulaire_infos_application = InfosApplicationForm( scn.definition.infos_generales )

    c = {}

    # le type de simulation :
    c['type_simulation'] = type_simulation

    # les resultats :
    c['type_capture_vars'] = type_capture_vars

    # le type de representation graphique choisi/en cours (a faire suivre) :
    c['choix_graph'] = choix_graph

    # autres constantes a transmettre pour identifier les operations
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo
    c['operation_retour_edition'] = CHX.VAL_operation_retour_edition
    c['operation_retour_simulation'] = CHX.VAL_operation_retour_simulation
    c['operation_retour_restitution'] = CHX.VAL_operation_retour_restitution

    c['scenario'] = scn.scenario_vpz
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine
    c['definition'] = scn.definition
    c['formulaire_modele_record'] = formulaire_modele_record
    if formulaire_de_type_ctg :
        c['formulaire_de_type_ctg'] = "cas de formulaire ou les informations sont presentees selon des categories"
    c['formulaire_infos_scenario'] = formulaire_infos_scenario
    c['formulaire_infos_application'] = formulaire_infos_application

    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

##..
#*****************************************************************************\n
# Methode contexte_affichage_graphique : constitue et retourne le contexte
# de la page NOM_page_affichage_graphique
# formulaire_de_type_ctg est False pour cas de formulaire 'classique', \n
# et True pour cas de formulaire ou les informations sont presentees selon des
# categories.
#\n*****************************************************************************
def contexte_affichage_graphique(request, flashes, scn, le_modele_record, formulaire_de_type_ctg=False) :

    # formulaire_modele_record (rappel des infos completes a titre d'information/memo)
    modele_record_bd = ModeleRecordBd.objects.get(id=le_modele_record.id)

    if formulaire_de_type_ctg : # formulaire par categories
        from record.bd_modeles_record.forms.user_forms_ctg import ModeleRecordFormComplet
    else : # formulaire 'classique'
        from record.bd_modeles_record.forms.user_forms import ModeleRecordFormComplet
    formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

    formulaire_infos_scenario = InfosScenarioForm( scn.scenario_vpz, scn.scenario_vpz_d_origine )
    formulaire_infos_application = InfosApplicationForm( scn.definition.infos_generales )

    c = {}

    # liste des fichiers existants sous rep_restitutions
    liste_fichiers = scn.espace_exploitation.getListeFichiersRepRestitutions()
    if len(liste_fichiers) == 0 : # liste vide 
        # artifice pour permettre verification presence de liste_fichiers
        liste_fichiers = [ "aucun" ]
    c['liste_fichiers'] = liste_fichiers

    # autres constantes a transmettre pour identifier les operations
    c['operation_retour_selection'] = CHX.VAL_operation_selection_memo
    c['operation_retour_edition'] = CHX.VAL_operation_retour_edition
    c['operation_retour_simulation'] = CHX.VAL_operation_retour_simulation
    c['operation_retour_restitution'] = CHX.VAL_operation_retour_restitution

    c['scenario'] = scn.scenario_vpz
    c['scenario_d_origine'] = scn.scenario_vpz_d_origine
    c['definition'] = scn.definition
    c['formulaire_modele_record'] = formulaire_modele_record
    if formulaire_de_type_ctg :
        c['formulaire_de_type_ctg'] = "cas de formulaire ou les informations sont presentees selon des categories"
    c['formulaire_infos_scenario'] = formulaire_infos_scenario
    c['formulaire_infos_application'] = formulaire_infos_application

    c['flashes'] = flashes # optionnel
    c.update(csrf(request))

    return c

#*****************************************************************************

