#-*- coding:utf-8 -*-

## @file record/outil_web_record/models/conf_web/conf_web.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File conf_web.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

from record.utils.dirs_and_files import verificationFichierOk

from record.outil_web_record.configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)


#*****************************************************************************
#
#
# Classes et methodes d'interface pour la configuration web,
# concernant la configuration web de l'application Web (cf ConfWebScenario)
# et/ou la configuration web du modele record (cf ConfWebModeleRecord).
#
#
#*****************************************************************************


##..
#*****************************************************************************\n
# Modele : FichierConfigSource
#
# Classe dediee a l'identification d'un fichier vpz de configuration
# (configuration du dictionnaire, configuration web propre a la conf web,
# propre aux applis web)
#
#*****************************************************************************
class FichierConfigSource( object ):

    VAL_vide=""

    def __init__(self, nom=None) :
        self.nom_absolu_fichier = self.VAL_vide # par defaut
        if nom != None :
            self.setNomAbsoluFichier( nom )

    def getNomAbsoluFichier( self ):
        return self.nom_absolu_fichier

    ## avec verification de format (mais pas d'existence du fichier)
    # reaction : VAL_vide
    def setNomAbsoluFichier( self, nom ) :
        nom_formate = self.VAL_vide # par defaut
        if type(nom) == str :
            if nom.endswith('.vpz') and nom.startswith('/') : # laisser condition nom.startswith('/') ?
                nom_formate = nom

        self.nom_absolu_fichier = nom_formate

    #def getNomRelatif( self ) : # nom du fichier (radical.vpz) sans le chemin
        #decoupage = self.nom_absolu_fichier.split( '/' )
        #return decoupage[-1]

    #def getChemin( self ) :
        # ... si besoin

    def nomAbsoluFichierIsValueVide( self ) :
        return ( self.nom_absolu_fichier == self.VAL_vide )

    ## verifie existence du fichier
    def isValide( self ) :
        is_valide = False # par defaut
        if self.nom_absolu_fichier != None :
            if not self.nomAbsoluFichierIsValueVide() :
                if verificationFichierOk( self.nom_absolu_fichier ) :
                    is_valide = True
        return is_valide

    ## Retourne True si v correspond a valeur VAL_vide
    @classmethod
    def isValueVide( cls, v ) :
        return ( v == cls.VAL_vide )

##..
#*****************************************************************************\n
# Modele : TypeSourceConfWeb
#
# Classe de definition des valeurs gerees/susceptibles d'etre prises par 
# type_source de SourceConfWeb
#
#*****************************************************************************
class TypeSourceConfWeb( object ):

    #*************************************************************************
    # definition des valeurs possibles 
    #*************************************************************************

    # configuration 'vide' (voir classes filles)
    VAL_vide=""

    # configuration par defaut
    VAL_par_defaut="par_defaut"

    # configuration a partir de fichier vpz de configuration web
    VAL_web_vpz="web_vpz"

    ##..
    #*************************************************************************\n
    # Retourne True si v correspond a valeur VAL_vide
    #\n*************************************************************************
    @classmethod
    def isValueVide( cls, v ) :
        return ( v == cls.VAL_vide )

    ##..
    #*************************************************************************\n
    # Retourne True si v correspond a valeur VAL_par_defaut 
    #\n*************************************************************************
    @classmethod
    def isValueParDefaut( cls, v ) :
        return ( v == cls.VAL_par_defaut )

    ##..
    #*************************************************************************\n
    # Retourne True si v correspond a valeur VAL_web_vpz 
    #\n*************************************************************************
    @classmethod
    def isValueWebVpz( cls, v ) :
        return ( v == cls.VAL_web_vpz )


##..
#*****************************************************************************\n
# Modele : TypeSourceConfWebModeleRecord
#
# Classe de definition des valeurs gerees/susceptibles d'etre prises par 
# type_source de SourceConfWebModeleRecord
#
#*****************************************************************************
class TypeSourceConfWebModeleRecord( TypeSourceConfWeb ):

    ##..
    #*************************************************************************\n
    # Retourne True si v correspond a une des valeurs possibles de 
    # TypeSourceConfWebModeleRecord
    #\n*************************************************************************
    @classmethod
    def isValueTypeSourceConfWebModeleRecord( cls, v ) :
        if v in ( cls.VAL_vide, cls.VAL_par_defaut, cls.VAL_web_vpz ) :
            return True
        else :
            return False

##..
#*****************************************************************************\n
# Modele : TypeSourceConfWebScenario
#
# Classe de definition des valeurs gerees/susceptibles d'etre prises par 
# type_source de SourceConfWebScenario
#
#*****************************************************************************
class TypeSourceConfWebScenario( TypeSourceConfWeb ):

    #*************************************************************************
    # definition des valeurs possibles (en plus de celles de TypeSourceConfWeb)
    #*************************************************************************

    # ConfWebScenario est configure par defaut + dictionnaire (information de
    # documentation de donnees) issu d'un fichier vpz de configuration web
    VAL_par_defaut_avec_dico="par_defaut_avec_dico"

    ##..
    #*************************************************************************\n
    # Retourne True si v correspond a valeur VAL_par_defaut_avec_dico 
    #\n*************************************************************************
    @classmethod
    def isValueParDefautAvecDico( cls, v ) :
        return ( v == cls.VAL_par_defaut_avec_dico )

    ##..
    #*************************************************************************\n
    # Retourne True si v correspond a une des valeurs possibles de 
    # TypeSourceConfWebScenario
    #\n*************************************************************************
    @classmethod
    def isValueTypeSourceConfWebScenario( cls, v ) :
        if v in ( cls.VAL_vide, cls.VAL_par_defaut, cls.VAL_web_vpz, cls.VAL_par_defaut_avec_dico ) :
            return True
        else :
            return False

##..
#*****************************************************************************\n
# Modele : SourceConfWeb
#
# Classe regroupant les informations a partir desquelles est construite/creee
# une ConfWebScenario ou ConfWebModeleRecord (selon cas de classe fille)
#
#*****************************************************************************
class SourceConfWeb( object ):

    def __init__( self ) :

        # selon type_source, les autres attributs (cf classes filles) sont
        # ou non signifiants
        self.type_source = TypeSourceConfWeb.VAL_vide # par defaut

    def getTypeSource( self ) :
        return self.type_source

##..
#*****************************************************************************\n
# Modele : SourceConfWebModeleRecord
#
# Classe regroupant les informations a partir desquelles est construite/creee
# une ConfWebModeleRecord
#
#*****************************************************************************
class SourceConfWebModeleRecord( SourceConfWeb ):

    ##..
    #*************************************************************************\n
    # Construction globale
    #\n*************************************************************************
    def __init__( self ) :

        SourceConfWeb.__init__( self )

        #*********************************************************************
        # type_source VAL_web_vpz
        # cas de configuration web a partir fichier vpz de configuration web
        # propre a la conf web
        #*********************************************************************

        # ExpConfigWebModeleRecord correspondant au fichier vpz de
        # configuration web propre a la conf web
        self.exp_config_web_conf = None

        #*********************************************************************
        # type_source VAL_par_defaut 
        # cas de configuration web par defaut
        #*********************************************************************

        # la liste des noms des scenarios existants du modele record (noms
        # des fichiers vpz sans leur extension)
        self.noms_scenarios_existants = None 

    #*************************************************************************
    # Specificites d'initialisation
    #*************************************************************************

    def initPourValWebVpz( self, exp_config_web_conf ) :
        self.type_source = TypeSourceConfWebModeleRecord.VAL_web_vpz
        self.exp_config_web_conf = exp_config_web_conf

    def initPourValParDefaut( self, noms_scenarios_existants ) : 
        self.type_source = TypeSourceConfWebModeleRecord.VAL_par_defaut
        self.noms_scenarios_existants = noms_scenarios_existants 

##..
#*****************************************************************************\n
# Modele : SourceConfWebScenario
#
# Classe regroupant les informations a partir desquelles est construite/creee
# une ConfWebScenario
#
#*****************************************************************************
class SourceConfWebScenario( SourceConfWeb ):

    ##..
    #*************************************************************************\n
    # Construction globale
    #\n*************************************************************************
    def __init__( self ) :

        SourceConfWeb.__init__( self )

        #*********************************************************************
        # type_source VAL_par_defaut
        # cas de configuration web a partir de l'application par defaut
        #*********************************************************************

        # Exp correspondant au fichier vpz du scenario
        self.exp_scenario = None 

        #*********************************************************************
        # type_source VAL_par_defaut_avec_dico 
        # cas de configuration web a partir de l'application par defaut
        # et d'une configuration de dictionnaire
        #*********************************************************************

        # Exp correspondant au fichier vpz du scenario
        self.exp_scenario = None 

        # ExpConfigDictionnaire correspondant au fichier vpz de configuration
        # du dictionnaire
        self.exp_config_dicos = None

        #*********************************************************************
        # type_source VAL_web_vpz
        # cas de configuration web a partir fichier vpz de configuration web
        # propre aux applis web
        #*********************************************************************

        # ExpConfigWebScenario correspondant au fichier vpz de configuration
        # web propre aux applis web
        self.exp_config_web_applis = None

        #*********************************************************************
        # type_source VAL_vide
        # cas de configuration web correspondant a application vide
        #*********************************************************************

        # InfosGeneralesAppli a affecter a une application 'vide'
        self.infos_generales_appli = None

    #*************************************************************************
    # Specificites d'initialisation
    #*************************************************************************

    def initPourValParDefaut( self, exp_scenario ) :
        self.type_source = TypeSourceConfWebScenario.VAL_par_defaut
        self.exp_scenario = exp_scenario 

    def initPourValParDefautAvecDico( self, exp_scenario, exp_config_dicos ) :
        self.type_source = TypeSourceConfWebScenario.VAL_par_defaut_avec_dico
        self.exp_scenario = exp_scenario 
        self.exp_config_dicos = exp_config_dicos 

    def initPourValWebVpz( self, exp_config_web_applis ) :
        self.type_source = TypeSourceConfWebScenario.VAL_web_vpz
        self.exp_config_web_applis = exp_config_web_applis 

    def initPourValVide( self, infos_generales_appli ) :
        self.type_source = TypeSourceConfWebScenario.VAL_vide
        self.infos_generales_appli = infos_generales_appli

#*****************************************************************************

