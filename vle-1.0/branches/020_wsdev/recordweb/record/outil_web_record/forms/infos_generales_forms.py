#-*- coding:utf-8 -*-

## @file record/outil_web_record/forms/infos_generales_forms.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File infos_generales_forms.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

import floppyforms as forms
from form_utils.forms import BetterForm # pour formulaire par categories

from record.forms.commun_forms import setFieldsConfigurationDefautReadOnly
from record.outil_web_record.forms.commun_forms import setFieldsConfigurationInfosLigneRappel

from record.outil_web_record.configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

from django.utils.translation import ugettext as _

from transmeta import TransMeta

#*****************************************************************************
#
#
# Formulaires associes/relatifs a des classes de infos_generales
#
#
#*****************************************************************************

##..
#*****************************************************************************\n
#
# Formulaire associe a InfosGenerales
#
#*****************************************************************************
class InfosGeneralesForm(BetterForm):

    def __init__(self, infos_generales, *args, **kwargs):

        super(InfosGeneralesForm, self).__init__(*args, **kwargs)

        # titre
        self.fields['titre'] = forms.CharField( label=_(u"Titre"), help_text=_(u"titre"), initial=infos_generales.titre )
    
        # description
        self.fields['description'] = forms.CharField( label=_(u"Description"), help_text=_(u"description"),
                widget=forms.Textarea(attrs={'rows':4, 'cols':100}),
                initial=infos_generales.description )
    
        # help
        self.fields['help'] = forms.CharField( label=_(u"Help"), help_text=_(u"help"),
                widget=forms.Textarea(attrs={'rows':4, 'cols':100}),
                initial=infos_generales.help )

        # configuration des champs
        setFieldsConfigurationDefautReadOnly( self.fields )

    class Meta :

        #*********************************************************************
        # tout dans un seul encadre sans nom
        #*********************************************************************
        fieldsets = (

        ( None, { #'description': _(u""),
                    'fields': ('titre',
                               'description',
                               'help',
                              ),
                } ),
        )

        row_attrs = {'titre': {'style': 'background: yellow'}}

##..
#*****************************************************************************\n
#
# Formulaire associe a InfosGeneralesAppli
#
#*****************************************************************************
class InfosGeneralesAppliFormA(InfosGeneralesForm):

    class Meta :

        #*********************************************************************
        # tout dans un seul encadre avec un nom
        #*********************************************************************
        fieldsets = (

        ( "APPLI", { #'description': _(u""),

                     'fields': ('titre',
                                'description',
                                'help',
                               ),

                     'legend': _(u"Application"),
                   } ),
        )

##..
#*****************************************************************************\n
#
# Formulaire d'information concernant une application (cas d'utilisation en
# cours).
# Le formulaire est construit a partir de InfosGeneralesAppli de
# l'application.
#
#*****************************************************************************
class InfosApplicationForm(InfosGeneralesForm):
#class InfosApplicationForm(InfosGeneralesAppliFormA):

    def __init__(self, infos_generales, *args, **kwargs):

        super(InfosApplicationForm, self).__init__(infos_generales, *args, **kwargs)

        # configuration des champs
        setFieldsConfigurationInfosLigneRappel( self.fields )


##..
#*****************************************************************************\n
#
# Formulaire d'information concernant un scenario (scenario de simulation en
# cours).
# Le formulaire est construit a partir des ScenarioVpz du scenario et de son
# scenario_d_origine.
#
#*****************************************************************************
class InfosScenarioForm(BetterForm):

    def __init__(self, scenario, scenario_d_origine, *args, **kwargs):

        super(InfosScenarioForm, self).__init__(*args, **kwargs)

        # nom_scenario_d_origine
        self.fields['nom_scenario_d_origine'] = forms.CharField( label=_(u"Nom du scénario d'origine (en tant que fichier vpz)"), help_text=_(u"nom du scénario d'origine dont le scénario en cours est issu (nom en tant que fichier vpz)"), initial=scenario_d_origine.nom_vpz )

        # nom_modele
        self.fields['nom_modele'] = forms.CharField( label=_(u"Nom du modèle"), help_text=_(u"nom du modèle dont le scénario d'origine fait partie"), initial=scenario_d_origine.paquet.nom )

        # nom_modele_sous_pkgs
        self.fields['nom_modele_sous_pkgs'] = forms.CharField( label=_(u"Nom sous pkgs"), help_text=_(u"nom sous pkgs du modèle dont le scénario d'origine fait partie"), initial=scenario_d_origine.paquet.nom_pkg )

        # description du modele
        self.fields['description_modele'] = forms.CharField( label=_(u"Description"), help_text=_(u"description du modèle dont le scénario d'origine fait partie"), 
                widget=forms.Textarea(attrs={'rows':4, 'cols':100}),
                initial=scenario_d_origine.paquet.description_modele_record )

        # titre, description et help du scenario dans son etat initial
        formulaire_infos_initiales_scenario = InfosGeneralesForm( scenario.infos_generales_initiales_scenario )
        self.fields['titre_initial'] = formulaire_infos_initiales_scenario.fields['titre']
        self.fields['description_initiale'] = formulaire_infos_initiales_scenario.fields['description']
        self.fields['help_initial'] = formulaire_infos_initiales_scenario.fields['help']

        # configuration des champs
        setFieldsConfigurationInfosLigneRappel( self.fields )

    class Meta :

        #*********************************************************************
        # plusieurs encadres
        #*********************************************************************
        fieldsets = (

        ( "SCENARIO_D_ORIGINE", { 'description': _(u"scénario d'origine dont le scénario en cours est issu"),
                    'fields': ( 'nom_scenario_d_origine',
                              ),
                     'legend': _(u"Le scénario d'origine"),
                } ),

        ( "MODELE", { 'description': _(u"modèle dont le scénario d'origine fait partie"),
                    'fields': ('nom_modele',
                               'nom_modele_sous_pkgs',
                               'description_modele',
                              ),

                     'legend': _(u"Le modèle"),
                } ),

        ( "ETAT_INITIAL", { 'description': _(u"Les informations suivantes correspondent au scénario en cours lorsqu'il était dans son état initial, avant toute éventuelle modification"),
                    'fields': ('titre_initial',
                               'description_initiale',
                               'help_initial',
                              ),

                     'legend': _(u"Etat initial"),
                } ),
        )

        row_attrs = {'titre': {'style': 'background: yellow'}}

#*****************************************************************************

