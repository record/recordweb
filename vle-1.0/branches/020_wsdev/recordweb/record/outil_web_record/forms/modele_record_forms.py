#-*- coding:utf-8 -*-

## @file record/outil_web_record/forms/modele_record_forms.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File modele_record_forms.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

import floppyforms as forms
from form_utils.forms import BetterForm # pour formulaire par categories

from record.outil_web_record.forms.infos_generales_forms import InfosGeneralesForm
from record.outil_web_record.forms.commun_forms import setFieldsConfigurationInfosListeScenarios

from record.outil_web_record.configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

from django.utils.translation import ugettext as _

from transmeta import TransMeta

#*****************************************************************************
#
#
# Formulaires associes/relatifs a des classes de modele_record
#
#
#*****************************************************************************

##..
#*****************************************************************************\n
#
# Formulaire associe au scenario relatif a ScenarioOn
#
# Formulaire construit a partir de nom_scenario et InfosGenerales
#
#*****************************************************************************
class InfosScenarioOnForm(InfosGeneralesForm):

    def __init__(self, nom_scenario, infos_generales, *args, **kwargs):

        super(InfosScenarioOnForm, self).__init__(infos_generales, *args, **kwargs)

        # nom_scenario
        #self.nom_scenario = nom_scenario

        # nom_scenario_avec_vpz
        self.nom_scenario_avec_vpz = nom_scenario+".vpz"

        # titre_scenario
        self.titre_scenario = infos_generales.titre

        # nom
        self.fields['nom'] = forms.CharField( label=_(u"Nom en tant que fichier vpz"), help_text=_(u"Nom en tant que fichier vpz"), initial=self.nom_scenario_avec_vpz )
    
        # configuration des champs
        setFieldsConfigurationInfosListeScenarios( self.fields )

    class Meta :

        #*********************************************************************
        # tout dans un seul encadre sans nom
        #*********************************************************************
        fieldsets = (

        ( None, { #'description': _(u""),
                    'fields': ('nom',
                               'titre',
                               'description',
                               'help',
                              ),
                } ),
        )

##..
#*****************************************************************************\n
#
# Formulaire associe a infos_application_associee relatif a ScenarioOn
#
# Formulaire construit a partir de InfosGenerales
#
#*****************************************************************************
class InfosApplicationAssocieeScenarioOnForm( InfosGeneralesForm ):

    def __init__(self, infos_generales, *args, **kwargs):

        super(InfosApplicationAssocieeScenarioOnForm, self).__init__(infos_generales, *args, **kwargs)

        # nom_application
        self.nom_application = infos_generales.titre

        # tous les champs read-only et autre configuration
        setFieldsConfigurationInfosListeScenarios( self.fields )

#*****************************************************************************

