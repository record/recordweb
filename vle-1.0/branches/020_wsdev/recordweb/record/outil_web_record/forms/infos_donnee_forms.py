#-*- coding:utf-8 -*-

## @file record/outil_web_record/forms/infos_donnee_forms.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File infos_donnee_forms.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

import floppyforms as forms
from form_utils.forms import BetterForm # pour formulaire par categories

from record.forms.commun_forms import setFieldsConfigurationDefautReadOnly

from record.outil_web_record.configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

from django.utils.translation import ugettext as _

from transmeta import TransMeta

#*****************************************************************************
#
#
# Formulaires associes/relatifs a des classes de infos_donnee
#
#
#*****************************************************************************

##..
#*****************************************************************************\n
#
# Formulaire associe a DocDonnee
#
# Formulaire construit a partir de DocDonnee
#
#*****************************************************************************
class DocDonneeForm(BetterForm):

    def __init__(self, doc_donnee, *args, **kwargs):

        super(DocDonneeForm, self).__init__(*args, **kwargs)

        self.fields['nom_frenchname'] = forms.CharField( label=_(u"Nom français"),
                help_text=_(u"Nom français"), 
                initial=doc_donnee.nom_frenchname,
                required=False)

        self.fields['nom_englishname'] = forms.CharField( label=_(u"Nom anglais"),
                help_text=_(u"Nom anglais"), 
                initial=doc_donnee.nom_englishname,
                required=False)

        self.fields['description'] = forms.CharField( label=_(u"Description"),
                help_text=_(u"Description"), 
                widget=forms.Textarea(attrs={'rows':4, 'cols':100}),
                initial=doc_donnee.description,
                required=False)

        self.fields['unite'] = forms.CharField( label=_(u"Unité"),
                help_text=_(u"Unité"), 
                initial=doc_donnee.unite,
                required=False)

        self.fields['val_max'] = forms.CharField( label=_(u"Valeur max"),
                help_text=_(u"Valeur max"), 
                initial=doc_donnee.val_max,
                required=False)

        self.fields['val_min'] = forms.CharField( label=_(u"Valeur min"),
                help_text=_(u"Valeur min"), 
                initial=doc_donnee.val_min,
                required=False)

        self.fields['val_recommandee'] = forms.CharField( label=_(u"Valeur recommandée"),
                help_text=_(u"Valeur recommandée"), 
                initial=doc_donnee.val_recommandee,
                required=False)

        self.fields['help'] = forms.CharField( label=_(u"Help"),
                help_text=_(u"Help"), 
                widget=forms.Textarea(attrs={'rows':4, 'cols':100}),
                initial=doc_donnee.help,
                required=False)

        # configuration des champs
        setFieldsConfigurationDefautReadOnly( self.fields )

    class Meta :

        #*********************************************************************
        # tout dans un seul encadre sans nom
        #*********************************************************************
        fieldsets = (

            ( None, { #'description': _(u""),
                        'fields': (
                              'nom_frenchname',
                              'nom_englishname',
                              'description',
                              'unite',
                              'val_max',
                              'val_min',
                              'val_recommandee',
                              'help',
                              ),
                    } ),
        )

##..
#*****************************************************************************\n
#
# Formulaire associe a InfosWebConf
#
# Formulaire construit a partir de InfosWebConf
#
#*****************************************************************************
class InfosWebConfForm(BetterForm):

    def __init__(self, infos_web_conf, *args, **kwargs):

        super(InfosWebConfForm, self).__init__(*args, **kwargs)

        # web_name
        self.fields['web_name'] = forms.CharField( label=_(u"Intitulé"),
                help_text=_(u"Intitulé"), 
                initial=infos_web_conf.web_name,
                required=False)

        # web_dimension
        self.fields['web_dimension'] = forms.CharField( label=_(u"Dimension"),
                help_text=_(u"Possibilité ('variable') ou non ('fixe') d'ajouter des valeurs à la donnée"), 
                initial=infos_web_conf.web_dimension,
                required=False)

        # web_mode
        self.fields['web_mode'] = forms.CharField( label=_(u"Mode"),
                help_text=_(u"Autorisations en termes de lecture et d'écriture"), 
                initial=infos_web_conf.web_mode,
                required=False)

        # configuration des champs
        setFieldsConfigurationDefautReadOnly( self.fields )

    class Meta :

        #*********************************************************************
        # tout dans un seul encadre sans nom
        #*********************************************************************
        fieldsets = (

            ( "WEB_CONF", { #'description': _(u""),
                        'fields': (
                              'web_name',
                              'web_dimension',
                              'web_mode',
                              ),
                         'legend': _(u"Caractéristiques d'édition de la donnée au niveau de la page web"),
                    } ),
        )

##..
#*****************************************************************************\n
#
# Formulaire associe a InfosVpzId
#
# Formulaire construit a partir de InfosVpzId
#
#*****************************************************************************
class InfosVpzIdForm(BetterForm):

    def __init__(self, infos_vpz_id, *args, **kwargs):

        super(InfosVpzIdForm, self).__init__(*args, **kwargs)

        # vpz_type
        self.fields['vpz_type'] = forms.CharField( label=_(u"Type"),
                help_text=_(u"le type détermine la nature de la donnée dans le fichier vpz"),
                initial=infos_vpz_id.vpz_type,
                required=False)

        # vpz_cond_name et vpz_port_name
        texte_cond = "/" # par defaut
        texte_port = "/" # par defaut
        if infos_vpz_id.vpz_type == "type_condition_port" :
            texte_cond = infos_vpz_id.vpz_cond_name
            texte_port = infos_vpz_id.vpz_port_name
        self.fields['vpz_cond_name'] = forms.CharField( label=_(u"Nom condition"),
                help_text=_(u"Nom de la condition dont fait partie le paramètre dans le fichier vpz"), 
                initial=texte_cond,
                required=False)
        self.fields['vpz_port_name'] = forms.CharField( label=_(u"Nom port"),
                help_text=_(u"Nom du port (de la condition) correspondant au paramètre dans le fichier vpz"), 
                initial=texte_port,
                required=False)

        # configuration des champs
        setFieldsConfigurationDefautReadOnly( self.fields )

    class Meta :

        #*********************************************************************
        # tout dans un seul encadre sans nom
        #*********************************************************************
        fieldsets = (

            ( "VPZ_ID", { #'description': _(u""),
                        'fields': (
                              'vpz_type',
                              'vpz_cond_name',
                              'vpz_port_name',
                              ),
                         'legend': _(u"Caractéristiques (correspondance) de la donnée au niveau du fichier vpz"),
                    } ),
        )

##..
#*****************************************************************************\n
#
# Formulaire associe a InfosDonneeDef
#
# Formulaire construit a partir de InfosDonneeDef
#
#*****************************************************************************
class InfosDonneeDefForm(BetterForm):

    def __init__(self, infos_donnee_def, *args, **kwargs):

        super(InfosDonneeDefForm, self).__init__(*args, **kwargs)

        # a partir de infos_donnee_def.web_conf
        infos_web_conf_form = InfosWebConfForm( infos_donnee_def.web_conf )
        for k,f in infos_web_conf_form.fields.iteritems() :
            self.fields[k] = f

        # plus web_dimension_donnee web_mode_donnee web_name_donnee
        self.web_dimension_donnee = infos_donnee_def.web_conf.web_dimension
        self.web_mode_donnee = infos_donnee_def.web_conf.web_mode
        self.web_name_donnee = infos_donnee_def.web_conf.web_name

        # a partir de infos_donnee_def.doc
        donnee_doc_form = DocDonneeForm( infos_donnee_def.doc )
        for k,f in donnee_doc_form.fields.iteritems() :
            self.fields[k] = f

        # a partir de infos_donnee_def.vpz_id
        infos_vpz_id_form = InfosVpzIdForm( infos_donnee_def.vpz_id )
        for k,f in infos_vpz_id_form.fields.iteritems() :
            self.fields[k] = f

        # plus vpz_type_donnee
        self.vpz_type_donnee = infos_donnee_def.vpz_id.vpz_type

        # configuration des champs
        setFieldsConfigurationDefautReadOnly( self.fields )

    class Meta :

        #*********************************************************************
        # tout dans un seul encadre sans nom
        #*********************************************************************
        fieldsets = (

            ( None, { #'description': _(u""),
                        'fields': (
                              'web_name',

                              'nom_frenchname',
                              'nom_englishname',
                              'description',
                              'unite',
                              'val_max',
                              'val_min',
                              'val_recommandee',
                              'help',
                              ),
                    } ),

            ( "POUR_MEMO_VPZ", { #'description': _(u""),
                        'fields': (
                              'vpz_type',
                              'vpz_cond_name',
                              'vpz_port_name',
                              ),
                         'legend': _(u"Correspondance au niveau du fichier vpz"),
                    } ),

            ( "POUR_MEMO_WEB", { #'description': _(u""),
                        'fields': (
                              'web_dimension',
                              'web_mode',
                              ),
                         'legend': _(u"Caractéristiques d'édition"),
                    } ),
        )

#*****************************************************************************

