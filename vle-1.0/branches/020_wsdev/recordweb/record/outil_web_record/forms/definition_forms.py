#-*- coding:utf-8 -*-

## @file record/outil_web_record/forms/definition_forms.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File definition_forms.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

import floppyforms as forms
from form_utils.forms import BetterForm # pour formulaire par categories

from record.outil_web_record.forms.infos_generales_forms import InfosGeneralesForm
from record.outil_web_record.forms.commun_forms import setFieldsConfigurationInfosScenario
from record.forms.commun_forms import setFieldsConfigurationDefautReadOnly

from record.outil_web_record.forms.infos_donnee_forms import InfosDonneeDefForm

from record.outil_web_record.configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

from django.utils.translation import ugettext as _

from transmeta import TransMeta

#*****************************************************************************
#
#
# Formulaires associes/relatifs a des classes de definition
#
#
#*****************************************************************************
##..
#*****************************************************************************\n
#
# Formulaire associe a informations generales de Bloc
#
# Formulaire construit a partir de InfosGenerales
#
#*****************************************************************************
class InfosBlocForm(InfosGeneralesForm):

    def __init__(self, infos_generales, *args, **kwargs):

        super(InfosBlocForm, self).__init__(infos_generales, *args, **kwargs)

        # titre_bloc
        self.titre_bloc = infos_generales.titre
    
        # configuration des champs
        setFieldsConfigurationInfosScenario( self.fields )

#*****************************************************************************\n
#
# Formulaire associe a DonneeDef
#
# Formulaire construit a partir de infos_donnee_def
#
#*****************************************************************************
class DonneeDefForm(InfosDonneeDefForm): #iii pour l'instant (... cf aussi self.values...)

    def __init__(self, infos_donnee_def, *args, **kwargs):

        super(DonneeDefForm, self).__init__(infos_donnee_def, *args, **kwargs)

        # configuration des champs
        setFieldsConfigurationInfosScenario( self.fields )

#*****************************************************************************

