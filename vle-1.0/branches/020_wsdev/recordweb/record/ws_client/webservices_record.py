#-*- coding:utf-8 -*-

#******************************************************************************
#
#                  Methods in Python language to call
#
#     web services of the django application record/webservices_record
#
#******************************************************************************

import sys
sys.path.append( '/home/nrousse/workspace_svn/recordweb/record' )
from ws_client.utils import *

sep0 = "----------------------------------------"
sep1 = "    ------------------------------------"
sep2 = "        --------------------------------"

#******************************************************************************
# Utils
#******************************************************************************

#------------------------------------------------------------------------------
# Print of webservices_record specific objects

def print_help( helptexts ) :
    print sep0 #------------------------------------
    print "                 HELP"
    print sep0 #------------------------------------
    for h in helptexts :
        print ""
        #print "*** ", h['subject'], ":"
        for t in h['content'] : 
            print t
    print ""
    print sep0 #------------------------------------

def print_models_and_scenarios( lesmodeles ) :

    print sep0 #------------------------------------
    print "     MODELS AND THEIR SCENARIOS"
    print sep0 #------------------------------------
    print "Models :", "\n"
    for m in lesmodeles :
        print sep0 #------------------------------------
        print "Model :", "\n"
        for k,v in m.iteritems() :
            print ""
            if k=='wsmodelscenarios' :
                print "Model scenarios :", "\n"
                print sep1 #---------------------------
                for s in v :
                    for sk, sv in s.iteritems() :
                        print "   ", sk, " : ", sv
                    print sep1 #---------------------------
            else :
                print k, " : ", v
    print ""
    print sep0 #------------------------------------

def print_scenario( scenario ) :

    print sep0 #------------------------------------
    print "            THE SCENARIO"
    print sep0 #------------------------------------
    print ""
    for k,v in scenario.iteritems() :
        if k=='parameters' :
            print k, ":", "\n"
            for p in v :
                print sep1 #---------------------------
                print "    Parameter :", "\n"
                for pk, pv in p.iteritems() :
                    if pk=='metadata' :
                        print "   ", pk, " : "
                        for dk,dv in pv.iteritems() :
                            print "      ", dk, ":", dv
                    else :
                        print "   ", pk, " : ", pv
                print ""
            print sep1 #---------------------------

        elif k=='result' :
            pass # at the end
        else :
            print k, ":", v, "\n"

    # 'result' at the end
    k = 'result'
    if k in scenario.keys() :
        v = scenario[k]
        print k, ":", "\n"

        # v is a list with a single element
        # this single element e is a dict with a single key 'result'
        r = v[0]
        (conversion_ok, result_ij) = tuple_from_str( r['result'] )
        for i,result_j in enumerate(result_ij) :
            for j,result in enumerate(result_j) :
                print sep1 #---------------------------
                print "    (",i,",",j,") :", "\n"
                for viewname,viewoutputs in result.iteritems() :
                    print sep2 #---------------------------
                    print "        View ", viewname, " :", "\n"
                    for vk,vv in viewoutputs.iteritems() :
                        print "        Output", vk, " : ", vv
                    print ""
                print sep2 #---------------------------
            print sep1 #---------------------------
        print ""

    print ""
    print sep0 #------------------------------------

#******************************************************************************
#
#                                  GET
#
#******************************************************************************

#******************************************************************************
# Web services about Help
#******************************************************************************

#------------------------------------------------------------------------------
# get http://127.0.0.1:24000/wsrec/Help/
# get http://127.0.0.1:24000/wsrec/Help/xxx
def getHelp( wsurl, subject=None, printall=False ) :

    if subject is None :
        url = "%sHelp/" % wsurl
    else :
        url = "%sHelp/%s/" % (wsurl,subject)
    helptexts_str = get_command( url )
    (conversion_ok, helptexts) = list_from_str( helptexts_str )

    if printall :
        print sep0 #------------------------------------
        print "web service GET", url, "..."
        print sep0 #------------------------------------
        print_brut_result( helptexts_str )
        print sep0 #------------------------------------
        print_conversion_result( conversion_ok, helptexts )
        print sep0 #------------------------------------
    print ""
    print_help( helptexts )
    
#******************************************************************************
# Web services about Models
#******************************************************************************

#------------------------------------------------------------------------------
# get http://127.0.0.1:24000/wsrec/Mod/
def getMod( wsurl, printall=False ) :

    url = "%sMod/" % wsurl
    lesmodeles_str = get_command( url )
    (conversion_ok, lesmodeles) = list_from_str( lesmodeles_str )

    if printall :
        print sep0 #------------------------------------
        print "web service GET", url, "..."
        print sep0 #------------------------------------
        print_brut_result( lesmodeles_str )
        print sep0 #------------------------------------
        print_conversion_result( conversion_ok, lesmodeles )
        print sep0 #------------------------------------
    print ""
    print_models_and_scenarios( lesmodeles )

#******************************************************************************
# Web services about Scenario
#******************************************************************************

#------------------------------------------------------------------------------
# get http://127.0.0.1:24000/wsrec/ScnFull/idmod/idscn/
def getScnFull( wsurl, idmod, idscn, printall=False ) :

    url = "%sScnFull/%s/%s/" % (wsurl,idmod,idscn)
    scenario_str = get_command( url )
    (conversion_ok, scenario) = dict_from_str( scenario_str )

    if printall :
        print sep0 #------------------------------------
        print "web service GET", url, "..."
        print sep0 #------------------------------------
        print_brut_result( scenario_str )
        print sep0 #------------------------------------
        print_conversion_result( conversion_ok, scenario )
        print sep0 #------------------------------------
    print ""
    print_scenario( scenario )

#------------------------------------------------------------------------------
# get http://127.0.0.1:24000/wsrec/Scn/idmod/idscn/
def getScn( wsurl, idmod, idscn, printall=False ) :

    url = "%sScn/%s/%s/" % (wsurl,idmod,idscn)
    scenario_str = get_command( url )
    (conversion_ok, scenario) = dict_from_str( scenario_str )

    if printall :
        print sep0 #------------------------------------
        print "web service GET", url, "..."
        print sep0 #------------------------------------
        print_brut_result( scenario_str )
        print sep0 #------------------------------------
        print_conversion_result( conversion_ok, scenario )
        print sep0 #------------------------------------
    print ""
    print_scenario( scenario )
    
#******************************************************************************
# Web services about Simulation
#******************************************************************************

#------------------------------------------------------------------------------
# get http://127.0.0.1:24000/wsrec/Res/idmod/idscn/
def getRes( wsurl, idmod, idscn, printall=False ) :

    url = "%sRes/%s/%s/" % (wsurl,idmod,idscn)
    scenario_str = get_command( url )
    (conversion_ok, scenario) = dict_from_str( scenario_str )

    if printall :
        print sep0 #------------------------------------
        print "web service GET", url, "..."
        print sep0 #------------------------------------
        print_brut_result( scenario_str )
        print sep0 #------------------------------------
        print_conversion_result( conversion_ok, scenario )
        print sep0 #------------------------------------
    print ""
    print_scenario( scenario )
    
#------------------------------------------------------------------------------
# get http://127.0.0.1:24000/wsrec/ResFull/idmod/idscn/
def getResFull( wsurl, idmod, idscn, printall=False ) :

    url = "%sResFull/%s/%s/" % (wsurl,idmod,idscn)
    scenario_str = get_command( url )
    (conversion_ok, scenario) = dict_from_str( scenario_str )

    if printall :
        print sep0 #------------------------------------
        print "web service GET", url, "..."
        print sep0 #------------------------------------
        print_brut_result( scenario_str )
        print sep0 #------------------------------------
        print_conversion_result( conversion_ok, scenario )
        print sep0 #------------------------------------
    print ""
    print_scenario( scenario )
    
#******************************************************************************
#
#                                  POST
#
#******************************************************************************

#******************************************************************************
# Web services about Scenario
#******************************************************************************

#------------------------------------------------------------------------------
# post http://127.0.0.1:24000/wsrec/ScnFull/idmod/idscn/
def postScnFull( wsurl, idmod, idscn, inputdata={}, printall=False ) :

    url = "%sScnFull/%s/%s/" % (wsurl,idmod,idscn)
    scenario_str = post_command( url, inputdata )
    (conversion_ok, scenario) = dict_from_str( scenario_str )

    if printall :
        print sep0 #------------------------------------
        print "web service POST", url, "..."
        print sep0 #------------------------------------
        print_brut_result( scenario_str )
        print sep0 #------------------------------------
        print_conversion_result( conversion_ok, scenario )
        print sep0 #------------------------------------
    print ""
    print_scenario( scenario )

#------------------------------------------------------------------------------
# post http://127.0.0.1:24000/wsrec/Scn/idmod/idscn/
def postScn( wsurl, idmod, idscn, inputdata={}, printall=False ) :

    url = "%sScn/%s/%s/" % (wsurl,idmod,idscn)
    scenario_str = post_command( url, inputdata )
    (conversion_ok, scenario) = dict_from_str( scenario_str )

    if printall :
        print sep0 #------------------------------------
        print "web service POST", url, "..."
        print sep0 #------------------------------------
        print_brut_result( scenario_str )
        print sep0 #------------------------------------
        print_conversion_result( conversion_ok, scenario )
        print sep0 #------------------------------------
    print ""
    print_scenario( scenario )
    
#------------------------------------------------------------------------------
    
#******************************************************************************
# Web services about Simulation
#******************************************************************************

#------------------------------------------------------------------------------
# post http://127.0.0.1:24000/wsrec/Res/idmod/idscn/
def postRes( wsurl, idmod, idscn, inputdata={}, printall=False ) :

    url = "%sRes/%s/%s/" % (wsurl,idmod,idscn)
    scenario_str = post_command( url, inputdata )
    (conversion_ok, scenario) = dict_from_str( scenario_str )

    if printall :
        print sep0 #------------------------------------
        print "web service POST", url, "..."
        print sep0 #------------------------------------
        print_brut_result( scenario_str )
        print sep0 #------------------------------------
        print_conversion_result( conversion_ok, scenario )
        print sep0 #------------------------------------
    print ""
    print_scenario( scenario )
    
#------------------------------------------------------------------------------
# post http://127.0.0.1:24000/wsrec/ResFull/idmod/idscn/
def postResFull( wsurl, idmod, idscn, inputdata={}, printall=False ) :

    url = "%sResFull/%s/%s/" % (wsurl,idmod,idscn)
    scenario_str = post_command( url, inputdata )
    (conversion_ok, scenario) = dict_from_str( scenario_str )

    if printall :
        print sep0 #------------------------------------
        print "web service POST", url, "..."
        print sep0 #------------------------------------
        print_brut_result( scenario_str )
        print sep0 #------------------------------------
        print_conversion_result( conversion_ok, scenario )
        print sep0 #------------------------------------
    print ""
    print_scenario( scenario )

#******************************************************************************

