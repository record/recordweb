#!/bin/bash

#******************************************************************************
#
#                        Appels bases sur curl
#
#     de web services de l'application django record/webservices_record
#
#******************************************************************************

#******************************************************************************
#
#                                 GET
#
#******************************************************************************

#******************************************************************************
#                          Help
#******************************************************************************

# get http://127.0.0.1:24000/wsrec/Help/
curl http://127.0.0.1:24000/wsrec/Help/

# get http://127.0.0.1:24000/wsrec/Help/xxx/
curl http://127.0.0.1:24000/wsrec/Help/Mod/

#******************************************************************************
#                          Models
#******************************************************************************

# get http://127.0.0.1:24000/wsrec/Mod/
curl http://127.0.0.1:24000/wsrec/Mod/

#******************************************************************************
#                         Scenario
#******************************************************************************

# get http://127.0.0.1:24000/wsrec/ScnFull/idmod/idscn/
curl http://127.0.0.1:24000/wsrec/ScnFull/2/4/

# get http://127.0.0.1:24000/wsrec/Scn/idmod/idscn/
curl http://127.0.0.1:24000/wsrec/Scn/2/4/

#******************************************************************************
#                           Simulation
#******************************************************************************

# get http://127.0.0.1:24000/wsrec/Res/idmod/idscn/
curl http://127.0.0.1:24000/wsrec/Res/2/4/

# get http://127.0.0.1:24000/wsrec/ResFull/idmod/idscn/
curl http://127.0.0.1:24000/wsrec/ResFull/2/4/

#******************************************************************************

#******************************************************************************
#
#                                 POST
#
#******************************************************************************

#'{
#"duration":4.0,
#"duration_isactive":true,
#"experimentname":"experiment_number_three",
#"experimentname_isactive":true,
#"parameterslist" : [ { "condname":"COND_Weed", "portname":"beta_0", "values":"[0.21,0.22]" }, { "condname":"COND_Weed", "portname":"chsi_1", "values":"[0.47]" } ]
#}'

#******************************************************************************
#                         Scenario
#******************************************************************************

# post http://127.0.0.1:24000/wsrec/Scn/idmod/idscn/
curl -X POST http://127.0.0.1:24000/wsrec/Scn/2/4/ -d '{ "duration":4.0, "duration_isactive":true, "experimentname":"experiment_number_three", "experimentname_isactive":true, "parameterslist" : [ { "condname":"COND_Weed", "portname":"beta_0", "values":"[0.21,0.22]" }, { "condname":"COND_Weed", "portname":"chsi_1", "values":"[0.47]" } ] }' -H "Content-Type: application/json"


#******************************************************************************
#                           Simulation
#******************************************************************************

# post http://127.0.0.1:24000/wsrec/Res/idmod/idscn/
curl -X POST http://127.0.0.1:24000/wsrec/Res/2/4/ -d '{ "duration":4.0, "duration_isactive":true, "experimentname":"experiment_number_three", "experimentname_isactive":true, "parameterslist" : [ { "condname":"COND_Weed", "portname":"beta_0", "values":"[0.21,0.22]" }, { "condname":"COND_Weed", "portname":"chsi_1", "values":"[0.47]" } ] }' -H "Content-Type: application/json"

# post http://127.0.0.1:24000/wsrec/ResFull/idmod/idscn/
curl -X POST http://127.0.0.1:24000/wsrec/ResFull/2/4/ -d '{ "duration":4.0, "duration_isactive":true, "experimentname":"experiment_number_three", "experimentname_isactive":true, "parameterslist" : [ { "condname":"COND_Weed", "portname":"beta_0", "values":"[0.21,0.22]" }, { "condname":"COND_Weed", "portname":"chsi_1", "values":"[0.47]" } ] }' -H "Content-Type: application/json"

#******************************************************************************

