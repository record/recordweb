#-*- coding:utf-8 -*-

#******************************************************************************
#
#                      Client side of web services
#
#                    Utils for code of python clients
#
#******************************************************************************

#******************************************************************************
# Conversions
#******************************************************************************

from ast import literal_eval

# extraction list (de str v)
# issu/extrait/adapte de def extraire_list(cls, v, prefixe, suffixe) :
def list_from_str( v ) :
    conversion_ok = False # par defaut
    val = None # par defaut
    try:
        c = literal_eval( v ) # contenu du str
        # extraction ok, s'assurer du format
        try:
            val = c
            if isinstance(val,list) :
                conversion_ok = True
        except : pass
    except : pass
    return (conversion_ok,val)

# extraction dict (de str v)
def dict_from_str( v ) :
    conversion_ok = False # par defaut
    val = None # par defaut
    try:
        c = literal_eval( v ) # contenu du str
        # extraction ok, s'assurer du format
        try:
            val = c
            if isinstance(val,dict) :
                conversion_ok = True
        except : pass
    except : pass
    return (conversion_ok,val)

# extraction tuple (de str v)
def tuple_from_str( v ) :
    conversion_ok = False # par defaut
    val = None # par defaut
    try:
        c = literal_eval( v ) # contenu du str
        # extraction ok, s'assurer du format
        try:
            val = c
            if isinstance(val,tuple) :
                conversion_ok = True
        except : pass
    except : pass
    return (conversion_ok,val)

#******************************************************************************
# PYCURL
#******************************************************************************

import pycurl
import cStringIO
import json

# Calls 'get url' and returns the returned content
def get_command( url ) :

    buf = cStringIO.StringIO()
    c = pycurl.Curl()

    c.setopt(c.URL, url )

    c.setopt(c.WRITEFUNCTION, buf.write)
    c.perform()
    buf_str = buf.getvalue()
    buf.close()
    return buf_str

# Calls 'post url' and returns the returned content
def post_command( url, inputdata ) :

    buf = cStringIO.StringIO()
    c = pycurl.Curl()

    c.setopt(c.POST, 1 )
    c.setopt(c.HTTPHEADER, ['Content-Type: application/json'] )

    c.setopt(c.URL, url )

    json_inputdata = json.dumps( inputdata )
    c.setopt(c.POSTFIELDS, json_inputdata )

    c.setopt(c.WRITEFUNCTION, buf.write)
    c.perform()
    buf_str = buf.getvalue()
    buf.close()
    return buf_str

#******************************************************************************
# Print (general objects)
#******************************************************************************

def print_brut_result( buf_str ):
    print "Received content (brut) :"
    print buf_str

def print_conversion_result( conversion_ok, buf ):
    if conversion_ok :
        print "Received content after conversion (conversion OK) :"
        print buf
    else :
        print "Conversion of received content NOT OK"

#******************************************************************************

