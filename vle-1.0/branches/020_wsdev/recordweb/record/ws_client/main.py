#-*- coding:utf-8 -*-

#******************************************************************************
#
#                        Appels en langage Python
#
#     de web services de l'application django record/webservices_record
#
#******************************************************************************

import sys
sys.path.append( '/home/nrousse/workspace_svn/recordweb/record' )
from ws_client.webservices_record import *

wsurl = "http://127.0.0.1:24000/wsrec/"

#******************************************************************************
#
#                                  GET
#
#******************************************************************************

#******************************************************************************
#                          Help
#******************************************************************************

# get http://127.0.0.1:24000/wsrec/Help/
getHelp( wsurl=wsurl )
getHelp( wsurl=wsurl, printall=True )

# get http://127.0.0.1:24000/wsrec/Help/xxx
getHelp( wsurl=wsurl, subject='Mod')
getHelp( wsurl=wsurl, subject='Mod', printall=True )

#******************************************************************************
#                          Models
#******************************************************************************

# get http://127.0.0.1:24000/wsrec/Mod/
getMod( wsurl=wsurl )
getMod( wsurl=wsurl, printall=True )

#******************************************************************************
#                         Scenario
#******************************************************************************

# get http://127.0.0.1:24000/wsrec/ScnFull/idmod/idscn/
getScnFull( wsurl=wsurl, idmod=2, idscn=4 )
getScnFull( wsurl=wsurl, idmod=2, idscn=4, printall=True )

# get http://127.0.0.1:24000/wsrec/Scn/idmod/idscn/
getScn( wsurl=wsurl, idmod=2, idscn=4 )
getScn( wsurl=wsurl, idmod=2, idscn=4, printall=True )

#******************************************************************************
#                           Simulation
#******************************************************************************

# get http://127.0.0.1:24000/wsrec/Res/idmod/idscn/
getRes( wsurl=wsurl, idmod=2, idscn=4 )
getRes( wsurl=wsurl, idmod=2, idscn=4, printall=True )

# get http://127.0.0.1:24000/wsrec/ResFull/idmod/idscn/
getResFull( wsurl=wsurl, idmod=2, idscn=4 )
getResFull( wsurl=wsurl, idmod=2, idscn=4, printall=True )

#******************************************************************************


#******************************************************************************
#
#                                  POST
#
#******************************************************************************

inputdata = dict()

inputdata[ 'duration' ] = 4.0
inputdata[ 'duration_isactive' ] = True

inputdata[ 'experimentname' ] = "experiment_number_three"
inputdata[ 'experimentname_isactive' ] = True

parameterslist = list()

p = { 'condname' : "COND_Weed", 'portname' : "beta_0" }
p['values'] = "[0.21,0.22]"
parameterslist.append( p )

p = { 'condname' : "COND_Weed", 'portname' : "chsi_1" }
p['values'] = "[0.47]"
parameterslist.append( p )

inputdata[ "parameterslist" ] = parameterslist

#******************************************************************************
#                         Scenario
#******************************************************************************

# post http://127.0.0.1:24000/wsrec/ScnFull/idmod/idscn/
postScnFull( wsurl=wsurl, idmod=2, idscn=4, inputdata=inputdata )
postScnFull( wsurl=wsurl, idmod=2, idscn=4, inputdata=inputdata, printall=True )

# post http://127.0.0.1:24000/wsrec/Scn/idmod/idscn/
postScn( wsurl=wsurl, idmod=2, idscn=4, inputdata=inputdata )
postScn( wsurl=wsurl, idmod=2, idscn=4, inputdata=inputdata, printall=True )

#******************************************************************************
#                           Simulation
#******************************************************************************

# post http://127.0.0.1:24000/wsrec/Res/idmod/idscn/
postRes( wsurl=wsurl, idmod=2, idscn=4, inputdata=inputdata )
postRes( wsurl=wsurl, idmod=2, idscn=4, inputdata=inputdata, printall=True )

# post http://127.0.0.1:24000/wsrec/ResFull/idmod/idscn/
postResFull( wsurl=wsurl, idmod=2, idscn=4, inputdata=inputdata )
postResFull( wsurl=wsurl, idmod=2, idscn=4, inputdata=inputdata, printall=True )

#******************************************************************************

