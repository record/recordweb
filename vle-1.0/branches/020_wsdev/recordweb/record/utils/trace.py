#-*- coding:utf-8 -*-

## @file record/utils/trace.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File trace.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
# 
# Classes relatives aux traces
#
#*****************************************************************************

##..
#*****************************************************************************\n
# classe mere, declinee en TraceEcran et TraceErreur
#\n*****************************************************************************
class Trace(object) :

    def __init__( self, avecTrace, entete ):
        self.avecTrace = avecTrace
        self.entete = entete

    ## constitue et retourne l'information qui servira dans l'entete informative
    def get_information( self, file, name ) :
        e = '['+file+","+name+"]"
        return e

    #*************************************************************************
    # affichages avec entete informative
    #*************************************************************************

    def message(self, m) : # de m
        if self.avecTrace :
            print self.entete, m

    def messages(self, liste) : # d'une liste (une ligne par element de la liste)
        if self.avecTrace :
            print self.entete
            for m in liste : print m

    #*************************************************************************
    # affichages bruts (sans entete)
    #*************************************************************************

    def msg(self, m) : # de m
        if self.avecTrace :
            print m
    
    def msgs(self, liste) : # d'une liste (une ligne par element de la liste)
        if self.avecTrace :
            for m in liste : print m
    
    ##..
    #*************************************************************************\n
    # affichage entete informative
    #\n*************************************************************************
    def entete_informative(self) :
        self.msg( self.entete + ":" )
    
    ##..
    #*************************************************************************\n
    # affiche un trait
    #\n*************************************************************************
    def trait(self) :
        self.msg("-----------------------------------------------------------------------------")
    
    ##..
    #*************************************************************************\n
    # affiche un saut de ligne
    #\n*************************************************************************
    def saut_de_ligne(self) :
        self.msg("")

##..
#*****************************************************************************\n
# Classe TraceEcran : trace a l'ecran
#
# config : classe contenant attribut de configuration trace_ecran
#
#*****************************************************************************
class TraceEcran(Trace) :

    def __init__( self, file, name, config ) :
        avecTrace = config.trace_ecran
        entete = self.get_information( file, name )
        Trace.__init__( self, avecTrace, entete )

##..
#*****************************************************************************\n
# Classe TraceErreur : trace erreur a l'ecran
#
# config : classe contenant attribut de configuration trace_erreur
#
#*****************************************************************************
class TraceErreur(Trace) :

    def __init__( self, file, name, config ) :
        avecTrace = config.trace_erreur
        entete = self.get_information( file, name ) + "[ERREUR !!! ]"
        Trace.__init__( self, avecTrace, entete )

#*****************************************************************************

