#-*- coding:utf-8 -*-

## @file record/utils/dirs_and_files.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File dirs_and_files.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# Methodes concernant la manipulation de repertoires/fichiers
#
#*****************************************************************************

import os
import os.path

from record.utils.cmdes_systeme import executerCommandeSysteme

try:
    from configs.conf_trace import CONF_trace
except ImportError:
    from record.utils.configs.conf_trace import CONF_trace

from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)


##..
#*****************************************************************************\n
# 'Verifie' le fichier f (existe et il s'agit bien d'un fichier)
#\n*****************************************************************************
def verificationFichierOk( f ) :
    verif = os.path.exists(f) and os.path.isfile(f)
    return verif

##..
#*****************************************************************************\n
# 'Verifie' le repertoire rep : les liens symboliques ne sont pas acceptes
#\n*****************************************************************************
def verificationRepertoireOk( rep ) :
    verif = os.path.exists(rep) and os.path.isdir(rep) and not os.path.islink(rep)
    return verif

##..
#*****************************************************************************\n
# Determine si repertoire est un lien
#\n*****************************************************************************
def isLienRepertoire( rep ) :
    return ( os.path.islink(rep) )

##..
#*****************************************************************************\n
# 'Verifie' le repertoire rep : les liens symboliques sont acceptes
#\n*****************************************************************************
def verificationLargeRepertoireOk( rep ) :
    verif = os.path.exists(rep) and os.path.isdir(rep)
    return verif

##..
#*****************************************************************************\n
# Determine si nom est un nom de repertoire ou de fichier cache (ie avec
# prefixe '.')
#\n*****************************************************************************
def isFichierOuRepertoireCache( nom ) :
    verif = nom.startswith('.')
    return verif

##..
#*****************************************************************************\n
# Fournit la liste des fichiers existant sous repertoire rep
#
# Traitement (permissif) cas d'erreur : une erreur sur un repertoire/fichier
# est non bloquante (reaction : le repertoire/fichier en question est ignore
# + un message ecran d'avertissement).
#
#\n*****************************************************************************
def getListeFichiersRep( rep ) :
    debutmsg = "AVERTISSEMENT !!! getListeFichiersRep : dans repertoire " +rep+", "
    liste_noms = []
    if verificationRepertoireOk( rep ) :
        liste = os.listdir(  rep  )
        for element in liste :
            try :
                f = element.encode('utf8')
                if verificationFichierOk( os.path.join(rep,f) ) :
                    liste_noms.append( f )
            except Exception, e :
                diag = "echec : " + str(e) + " => " + element + " ignored"
                t_ecr.saut_de_ligne()
                t_ecr.message( debutmsg + diag )
                t_ecr.saut_de_ligne()
    return liste_noms

##..
#*****************************************************************************\n
# Fournit la liste des fichiers existant sous repertoire rep qui ne sont
# pas des fichiers caches (ie prefixes '.')
#
# Traitement (permissif) cas d'erreur : une erreur sur un repertoire/fichier
# est non bloquante (reaction : le repertoire/fichier en question est ignore
# + un message ecran d'avertissement).
#
#\n*****************************************************************************
def getListeFichiersNonCachesRep( rep ) :
    debutmsg = "AVERTISSEMENT !!! getListeFichiersNonCachesRep : dans repertoire " +rep+", "
    liste_noms_absolus = []
    if verificationRepertoireOk( rep ) :
        liste = os.listdir(  rep  )
        for element in liste :
            try :
                f = element.encode('utf8')
                if verificationFichierOk( os.path.join(rep,f) ) and not isFichierOuRepertoireCache(f) :
                    liste_noms_absolus.append( f )
            except Exception, e :
                diag = "echec : " + str(e) + " => " + element + " ignored"
                t_ecr.saut_de_ligne()
                t_ecr.message( debutmsg + diag )
                t_ecr.saut_de_ligne()
    return liste_noms_absolus

##..
#*****************************************************************************\n
# Fournit la liste des repertoires existant sous repertoire rep
#
# Traitement (permissif) cas d'erreur : une erreur sur un repertoire/fichier
# est non bloquante (reaction : le repertoire/fichier en question est ignore
# + un message ecran d'avertissement).
#
#\n*****************************************************************************
def getListeRepertoiresRep( rep ) :
    debutmsg = "AVERTISSEMENT !!! getListeRepertoiresRep : dans repertoire " +rep+", "
    liste_noms = []
    if verificationRepertoireOk( rep ) :
        liste = os.listdir(  rep  )
        for element in liste :
            try :
                f = element.encode('utf8')
                if verificationRepertoireOk( os.path.join(rep,f) ) :
                    liste_noms.append( f )
            except Exception, e :
                diag = "echec : " + str(e) + " => " + element + " ignored"
                t_ecr.saut_de_ligne()
                t_ecr.message( debutmsg + diag )
                t_ecr.saut_de_ligne()
    return liste_noms

##..
#*****************************************************************************\n
# Fournit la liste des repertoires existant sous repertoire rep qui ne sont
# pas des repertoires caches (ie prefixes '.')
#
# Traitement (permissif) cas d'erreur : une erreur sur un repertoire/fichier
# est non bloquante (reaction : le repertoire/fichier en question est ignore
# + un message ecran d'avertissement).
#
#\n*****************************************************************************
def getListeRepertoiresNonCachesRep( rep ) :
    debutmsg = "AVERTISSEMENT !!! getListeRepertoiresNonCachesRep : dans repertoire " +rep+", "
    liste_noms_absolus = []
    if verificationRepertoireOk( rep ) :
        liste = os.listdir(  rep  )
        for element in liste :
            try :
                f = element.encode('utf8')
                if verificationRepertoireOk( os.path.join(rep,f) ) and not isFichierOuRepertoireCache(f) :
                    liste_noms_absolus.append( f )
            except Exception, e :
                diag = "echec : " + str(e) + " => " + element + " ignored"
                t_ecr.saut_de_ligne()
                t_ecr.message( debutmsg + diag )
                t_ecr.saut_de_ligne()
    return liste_noms_absolus


##..
#*****************************************************************************\n
# Fournit la liste des repertoires existant sous repertoire rep qui ne sont
# pas des repertoires caches (ie prefixes '.'), les liens symboliques sont
# acceptes/pris en compte
#
# Traitement (permissif) cas d'erreur : une erreur sur un repertoire/fichier
# est non bloquante (reaction : le repertoire/fichier en question est ignore
# + un message ecran d'avertissement).
#
#\n*****************************************************************************
def getListeRepertoiresLargesNonCachesRep( rep ) :
    debutmsg = "AVERTISSEMENT !!! getListeRepertoiresLargesNonCachesRep : dans repertoire " +rep+", "
    liste_noms_absolus = []
    if verificationLargeRepertoireOk( rep ) :
        liste = os.listdir(  rep  )
        for element in liste :
            try :
                f = element.encode('utf8')
                if verificationLargeRepertoireOk( os.path.join(rep,f) ) and not isFichierOuRepertoireCache(f) :
                    liste_noms_absolus.append( f )
            except Exception, e :
                #diag = "echec : " + e.message + " => " + element + " ignored"
                diag = "echec : " + str(e) + " => " + element + " ignored"
                t_ecr.saut_de_ligne()
                t_ecr.message( debutmsg + diag )
                t_ecr.saut_de_ligne()
    return liste_noms_absolus


##..
#*****************************************************************************\n
# Suppression du lien rep
#\n*****************************************************************************
def supprimerLien( rep ) :
    if isLienRepertoire( rep ) :
        commande = 'rm -f '+rep
        executerCommandeSysteme( commande )
    else :
        t_err.trait()
        t_err.message( "supprimerLien : repertoire " + str(rep) + " N'EST PAS UN LIEN, n'a pas ete supprime" )

##..
#*****************************************************************************\n
# Suppression du repertoire rep
#\n*****************************************************************************
def supprimerRepertoire( rep ) :
    if verificationRepertoireOk( rep ) :
        commande = 'rm -fr ' + rep
        executerCommandeSysteme( commande )
    else :
        t_err.trait()
        t_err.message( "supprimerRepertoire : repertoire " + str(rep) + " NOT OK, n'a pas ete supprime" )

##..
#*****************************************************************************\n
# Suppression tout contenu du repertoire rep
#\n*****************************************************************************
def viderRepertoire( rep ) :
    if verificationRepertoireOk( rep ) :
        commande = 'rm -fr ' + rep + '/*'
        executerCommandeSysteme( commande )
    else :
        t_err.trait()
        t_err.message( "viderRepertoire : repertoire " + str(rep) + " NOT OK, n'a pas ete vide" )

##..
#*****************************************************************************\n
# Creer repertoire rep
#\n*****************************************************************************
def creerRepertoire( rep ) :
    if verificationRepertoireOk( rep ) :
        t_ecr.trait()
        t_ecr.message( "creerRepertoire : repertoire " + str(rep) + " EXISTE DEJA, n'a pas ete cree" )
    else :
        commande = 'mkdir ' + rep
        executerCommandeSysteme( commande )

##..
#*****************************************************************************\n
# Creer lien 
#\n*****************************************************************************
def creerLien( rep_a, rep_b ) :

    if verificationLargeRepertoireOk( rep_b ) : # existant
        t_ecr.trait()
        t_ecr.message( "creerLien : repertoire " + str(rep_b) + " EXISTE DEJA, le lien n'a pas ete cree" )
    else :
        commande = 'ln -s '+rep_a+' '+rep_b
        executerCommandeSysteme( commande )

#*****************************************************************************

