#!/bin/bash

###
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
# Copyright (C) 2011-2012 INRA http://www.inra.fr
###

# Script de construction (a partir de install_projet.txt) du fichier texte genere_install_nom_du_projet_web.txt contenant les instructions d'installation du projet web nom_du_projet_web

# !!! cree/modifie fichier temporaire tmp_file.txt

echo "construction (a partir de install_projet.txt) du fichier texte genere_install_nom_du_projet_web.txt contenant les instructions d'installation du projet web nom_du_projet_web"

grep '#w' install_projet.txt > genere_install_nom_du_projet_web.txt
sed 's/#[#w]#[#u]#[#r]|//' genere_install_nom_du_projet_web.txt > tmp_file.txt
cat tmp_file.txt > genere_install_nom_du_projet_web.txt

