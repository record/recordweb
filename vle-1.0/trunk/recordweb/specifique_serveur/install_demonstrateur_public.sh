#!/bin/bash

###
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
# Copyright (C) 2011-2012 INRA http://www.inra.fr
###

# Script de construction (a partir de install_projet.txt) du fichier texte genere_install_demonstrateur_public.txt contenant les instructions d'installation du projet web demonstrateur_public

# !!! cree/modifie fichier temporaire tmp_file.txt

echo "construction (a partir de install_projet.txt) du fichier texte genere_install_demonstrateur_public.txt contenant les instructions d'installation du projet web demonstrateur_public"

grep '#u' install_projet.txt > genere_install_demonstrateur_public.txt
sed 's/#[#w]#[#u]#[#r]|//' genere_install_demonstrateur_public.txt > tmp_file.txt
cat tmp_file.txt > genere_install_demonstrateur_public.txt
sed 's/nom_du_projet_web/demonstrateur_public/g' genere_install_demonstrateur_public.txt > tmp_file.txt
cat tmp_file.txt > genere_install_demonstrateur_public.txt

