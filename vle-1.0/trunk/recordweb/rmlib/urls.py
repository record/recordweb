
## @file rmlib/urls.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File urls.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

from django.conf.urls.defaults import *

from django.conf import settings

# to enable the admin:
from django.contrib import admin
admin.autodiscover()

#*****************************************************************************

urlpatterns = patterns('',

    # internationalisation traductions
    (r'^i18n/', include('django.conf.urls.i18n')),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        # static files
        (r'^site_media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT }),
    )

urlpatterns += patterns('',

    #*************************************************************************
    #
    # partie administration
    #
    #*************************************************************************

    url(r'^rmlib/adm/',   include(admin.site.urls)),
    url(r'^rmlib/admin/', include(admin.site.urls)), # synonyme

    #*************************************************************************
    #
    # partie utilisation
    #
    #*************************************************************************

    #*************************************************************************
    # par ordre chronologique d'appel :
    #*************************************************************************

    # 1) les_modeles_record
    url(r'^rmlib/usr/', include('record.bd_modeles_record.urls')), 
    url(r'^bdrec/usr/', include('record.bd_modeles_record.urls')), 

    url(r'^', include('record.bd_modeles_record.urls')), 

    #*************************************************************************
)

