#-*- coding:utf-8 -*-

## @file record/forms/commun_forms.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File commun_forms.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

from record.outil_web_record.configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

from django.utils.translation import ugettext as _

from transmeta import TransMeta

#*****************************************************************************
#
#
# Des traitements/donnees communs aux formulaires
#
#
#*****************************************************************************

#import floppyforms as forms
#from form_utils.forms import BetterForm # pour formulaire par categories

##..
#*****************************************************************************\n
#
# Des donnees communes aux formulaires
#
#*****************************************************************************
class CommunForms(object):
    pass


##..
#*****************************************************************************\n
# Methode setFieldsConfigurationReadOnlyA
#
# Une configuration des champs de fields relatif a formulaire BetterForm
#
#*****************************************************************************
def setFieldsConfigurationReadOnlyA( fields ):

    # tous les champs read-only et autre configuration
    for field in fields.values() :
        field.widget.attrs['readonly']= True # 'readonly'
        field.widget.attrs['size']= 80
        field.widget.attrs['style']= "background-color:transparent; border:0; color:blue; font-size:11pt; font-family:arial,sans-serif"

#*****************************************************************************

##..
#*****************************************************************************\n
# Methode setFieldsConfigurationDefautReadOnly
#
# Configuration des champs de fields relatif a formulaire BetterForm
# par defaut dans ce de read-only
#
#*****************************************************************************
def setFieldsConfigurationDefautReadOnly( fields ):

    # tous les champs read-only et autre configuration
    for field in fields.values() :
        field.widget.attrs['readonly']= True # 'readonly'
        field.widget.attrs['size']= 80
        field.widget.attrs['style']= "background-color:transparent; border:0; color:blue;"

#*****************************************************************************

