��    D      <  a   \      �     �     �            )   &  *   P     {     �  	   �     �     �     �     �     �     �     �     �               &     B     N     g     �  2   �     �     �     �     �          0     I     `     c     f     �  "   �  !   �     �      �     	     2	     R	     l	  )   �	  -   �	  !   �	  .   
  .   3
  !   b
     �
     �
  
   �
  	   �
     �
  )   �
     �
          #     0  .   B  	   q     {     �     �  $   �     �  @  �     4     N     ^     j  +   p  +   �  
   �  
   �     �     �     �                       
   "     -     ?     L     c     }     �     �     �  &   �     �     
               )     B     Z     q     t     x     �      �     �     �     �          1     O     h  '   �  +   �     �  (   �  *   !     L     l     x     �     �     �     �     �     �     �       %        <     D     P     e     |     �        @   !              '   
         6   8   %         ;          <       B                                       #   7            9                     2         -              3   A   "   .             *          	           5   =       1       ?   /              ,   C   >      $   D       0                :       (           +   )           &      4    Affichages numériques Compte rendu de réception Description Erreur Erreur : l'affichage graphique a échoué Erreur : l'affichage numérique a échoué INDIVIDU Individu Individus MODELE RECORD MOT DE PASSE Menu  Message d'erreur Mot de passe Nom Nom du modèle Nom du paquet principal RESPONSABLES Record Web - bd modeles Représentations graphiques Responsable Responsable informatique Responsable scientifique Retour au choix du scénario Retour au menu de simulation du scénario en cours Simulation simple Téléchargements Valeur Version vle de réception configuration dictionnaire configuration web applis configuration web conf de et il n'existe pas de répertoire  individu inconnu information cr_reception manquante information description manquante information email manquante information first_name manquante information id manquante information last_name manquante information nom manquante information nom_pkg manquante information nom_repertoire_data manquante information nom_repertoire_lot_pkgs manquante information responsable manquante information responsable_informatique manquante information responsable_scientifique manquante information version_vle manquante invalide la simulation a échoué le fichier le paquet le répertoire le répertoire des données de simulation le téléchargement a échoué modèle inconnu n'existe pas n'existe pas sous pour le répertoire 'data' du paquet principal problème responsable responsable_informatique responsable_scientifique répertoire des fichiers de données sous le répertoire  Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-01-03 17:47+0100
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Numerical representations Delivery report Description Error Error : the graphical representation failed Error : the numerical representation failed INDIVIDUAL Individual Individuals RECORD MODEL PASSWORD Menu  Error message Password Name Model name Main package name RESPONSIBLES Record Web - models db Graphical representations Responsible Computer responsible Scientific responsible Back to the scenario choice Back to the simulation scenario choice Simple simulation Downloadings Value Vle version dictionary configuration web appli configuration web conf configuration of and No directory exists  unknown individual cr_reception information missing description information missing email information missing first_name information missing id information missing last_name information missing name information missing nom_pkg information missing nom_repertoire_data information missing nom_repertoire_lot_pkgs information missing responsible information missing computer responsible information missing scientific responsible information missing version_vle information missing unavailable the simulation failed the file the package the directory the simulation data directory The downloading failed unknown model doesn't exist doesn't exist under for the main package 'data' directory problem responsible computer_responsible scientific_responsible datas files directory under the directory 