#-*- coding:utf-8 -*-

## @file record/utils/graphique.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File graphique.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

import pylab
import PIL.Image
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from cStringIO import StringIO

##..
#*****************************************************************************\n
# Graphique
#
# L'affichage/representation graphique de donnees
#
# Graphique de type "courbes" :
# trace de plusieurs courbes Y(X) sur un meme graphique
#
#*****************************************************************************
class Graphique(object):

    ##..
    #*************************************************************************\n
    # Construction
    #\n*************************************************************************
    def __init__( self ):

        #*********************************************************************
        # Type/nature du graphique
        #*********************************************************************
        # le seul type de graphique traite est "courbes"
        self.type_graphique = "courbes" # par defaut

        #*********************************************************************
        # Configuration/informations du graphique
        #*********************************************************************

        # liste des couples (X,Y) avec X et Y du type (nom,valeurs) 
        self.les_XY = None

        self.titre = None

        self.legendeX = None     # sur axe X
        self.legendeY = None     # sur axe Y
        self.les_legendes = None # dans encadre

        #*********************************************************************
        # Pour l'apparence des graphiques
        #*********************************************************************
        self.styles = [ '-', # ligne continue
                         '--', #tirets
                         '-.', #points-tirets
                         ':' ] #pointilles
        self.couleurs = [ 'g', # vert
                           'r', # rouge
                           'k', # noir
                           'b', # bleu
                           'c', # cyan
                           'm' ] # magenta
        self.un_style_couleur = (self.styles[0],self.couleurs[0])

    ##..
    #*************************************************************************\n
    # Gestion de l'apparence des graphiques
    #\n*************************************************************************
    def get_style_couleur( self ) :

        (s,c) = self.un_style_couleur # le dernier utilise

        # incrementation
        i_s = self.styles.index(s) + 1
        if i_s == len(self.styles) :
            i_s = 0
        i_c = self.couleurs.index(c) + 1
        if i_c == len(self.couleurs) :
            i_c = 0

        self.un_style_couleur = ( self.styles[i_s], self.couleurs[i_c] )
        return ( self.un_style_couleur )

    #*************************************************************************
    #
    # Methodes set d'initialisation de Graphique
    #
    #*************************************************************************

    def setTitre( self, titre ) :
        self.titre = titre

    def setLegendes( self, legendeX, legendeY, les_legendes ) :
        self.legendeX = legendeX
        self.legendeY = legendeY
        self.les_legendes = les_legendes

    def setLesXY( self, les_XY ) :
        self.les_XY = les_XY

    ##..
    #*************************************************************************\n
    # Methode produireGraphique
    #
    # Produit le graphique (retourne son image)
    #
    #*************************************************************************
    def produireGraphique( self ) :

        pylab.close()

        # creation d'un figure pour les graphiques
        fig = Figure(figsize=(12, 6),facecolor='#FFFFFF')
        canvas = FigureCanvas(fig)
    
        ax = fig.add_subplot(111,axisbg='#FFFFFF')
        width = 0.35

        # graphique

        les_legendes = self.les_legendes

        les_p = list()
        for ( (nomX,valeursX), (nomY,valeursY) ) in self.les_XY :
            (s,c) = self.get_style_couleur()
            p = ax.plot(valeursX, valeursY, color=c, linestyle=s, linewidth=1)
            les_p.append(p)

        ax.set_ylabel( self.legendeY )
        ax.set_xlabel( self.legendeX )

        ax.set_title( self.titre )

        ax.grid(True)

        ax.legend( les_p, self.les_legendes, loc='best', ncol=1)

        canvas.draw()

        imageSize = canvas.get_width_height()
        imageRgb = canvas.tostring_rgb()
        pilImage = PIL.Image.fromstring("RGB", imageSize, imageRgb)

        return pilImage

##..
#*****************************************************************************\n
# Methode getImageBuffer
#
# Restitution image sous forme de buffer
# pour trace/affichage ecran
#
#*****************************************************************************
def getImageBuffer( image ) :
    buffer = StringIO()
    image.save(buffer, "PNG")
    return buffer

##..
#*****************************************************************************\n
# Methode getImageFile
#
# Restitution image sous forme de fichier \n
# nom_fichier : nom absolu, dont l'extension determine le format (pdf, png..)
#
#*****************************************************************************
def getImageFile( image, nom_fichier ) :
    image.save( nom_fichier )

#*****************************************************************************

