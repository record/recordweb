#-*- coding:utf-8 -*-

## @file record/utils/vle/exp.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File exp.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

import libpyvle

from ast import literal_eval

from pyvle import Vle, VleMatrix
from pyvle import VleTable, VleTuple, VleMatrix, VleXML
from pyvle import VlePackage

try:
    from configs.conf_trace import CONF_trace
except ImportError:
    from record.utils.configs.conf_trace import CONF_trace

from record.utils.trace import TraceEcran, TraceErreur 

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)


#*****************************************************************************
#
# Interface avec pyvle (class Vle, (class VlePackage))
#
#*****************************************************************************

##..
#*****************************************************************************\n
#
# Methode d'initialisation de VLE par prise en compte de la variable
# d'environnement VLE_HOME (il s'agit d'appeler vle::manager::init).
#
# Cette methode n'aura d'effet que s'il s'agit de la 1ere invocation de pyvle
# (Vle, VlePackage) pour le process.
# Pour plus de details, voir code de pyvle.cpp :
#    static bool thread_init = false;
#    if (!thread_init) { vle::manager::init(); thread_init = true; }
#
#*****************************************************************************
def initialiserVle() :
    VlePackage.getInstalledPackages()

##..
#*****************************************************************************\n
# Exp
#
# Objet de manipulation du fichier vpz (definition, simulation)
#
#*****************************************************************************
class Exp( Vle ):

    ##..
    #*************************************************************************\n
    # Construction
    #\n*************************************************************************
    def __init__( self, nom_vpz, nom_pkg='' ):

        if nom_pkg == '':
        # construction a partir du nom du fichier vpz (chemin absolu) nom_vpz
            Vle.__init__( self, nom_vpz.encode('utf8') )

        else:
        # construction a partir du nom du fichier vpz (chemin relatif) nom_vpz
        # et du nom du paquet vle
            Vle.__init__( self, nom_vpz.encode('utf8'), nom_pkg.encode('utf8') )

    #*************************************************************************
    # Construction a partir du nom du fichier vpz et du paquet
    #*************************************************************************
    #def __init__( self, nom_vpz, nom_paquet ) :
    #    Vle.__init__( self, nom_vpz, nom_paquet )

    ##..
    #*************************************************************************\n
    # Retourne la liste de toutes les conditions, qu'elles soient ou non
    # actives (ie pointees/utilisees par au moins un modele atomic)
    #\n*************************************************************************
    def get_liste_conditions(self):
        return self.listConditions()

    ##..
    #*************************************************************************\n
    # Retourne la liste des conditions actives
    # ie pointees/utilisees par au moins un modele atomic
    #\n*************************************************************************
    def get_liste_conditions_actives(self):

        liste_conditions_actives = list()

        # parcours des dynamics (actives ou non,
        # ie pointees/utilisees ou non par modele(s) atomic(s))
        for dyn in self.listDynamics() :
            # liste des modeles atomics qui utilisent/pointent dyn
            liste_ma = self.listDynamicModels(dyn)
            for ma in liste_ma :
                # liste des conditions pointees/utilisees par ma
                liste_conditions_ma = self.listAtomicModelConditions(ma)
                for cond in liste_conditions_ma :
                    if cond not in liste_conditions_actives :
                        liste_conditions_actives.append( cond )

        return liste_conditions_actives

    ##..
    #*************************************************************************\n
    #
    # Retourne ports, la liste des ports de chaque condition 
    #
    # for cond in self.listConditions() :
    #     ports[cond] : liste des ports de la condition cond
    #
    #*************************************************************************
    def get_liste_ports_par_condition(self):
        ports = [self.listConditionPorts(cond) for cond in self.listConditions()]
        return ports

    ##..
    #*************************************************************************\n
    #
    # Retourne la liste ports_values
    #
    # ports_values est la liste des ( port, la liste des values du port ) de
    # chaque condition
    #
    # Remarque : la 'liste des values du port' comporte plusieurs elements
    # dans le cas d'un port de multi-simulation et un element unique sinon
    #
    # for cond in self.listConditions() :
    #     ports_values[cond] :
    #     liste des ( port, liste des values de port ) de cond
    #
    #*************************************************************************
    def get_liste_ports_setvalues(self):

        ports_values = [ [ ( port, self.getConditionSetValue(cond,port) ) for p,port in enumerate( self.listConditionPorts(cond) ) ] for c,cond in enumerate(self.listConditions()) ]

        # trace (nb : affichage n'a pas ete verifiee)
        #t_ecr.message( "get_liste_ports_setvalues :" )
        #for c,cond in enumerate(self.listConditions()) :
            #m = "ports_values[" + cond + "] : " + str( ports_values[c] )
            #t_ecr.msg(m)
        #t_ecr.saut_de_ligne()

        return ports_values

    ##..
    #*************************************************************************\n
    #
    # Retourne ports_values, la liste des (port,values) de chaque condition 
    #
    # Remarque : values est une liste dans le cas d'un port de
    # multi-simulation et un element unique sinon
    #
    # for cond in self.listConditions() :
    #     ports_values[cond] : liste des (port,values) de la condition cond
    #
    #*************************************************************************
    def get_liste_ports_values(self):

        # condition par condition : port, values
        ports_values = [ [ (port,self.getConditionPortValues(cond,port)) for port in self.listConditionPorts(cond)] for cond in self.listConditions()]
        return ports_values

    #*************************************************************************
    #
    # Methodes relatives au 'type_value'
    #
    # Cette partie de code (is_type_value, is_type_value_not_nul,
    # get_type_value...) fonctionne avec pyvle-1.0.3, ne fonctionne pas avec
    # pyvle-1.0.1 (fonctionne avec pyvle-1.0.2 + certains patches).
    #
    # Avec pyvle-1.0.3 (pyvle-1.0.2 + patches) :
    #
    #   Correspondance entre
    #   (A) les types choisis a la saisie (sous gvle) et
    #   (B) les types retournes par get_type_value (cf ceux retournes par 
    #       getConditionValueType (de pyvle))
    #   (C) leur renommage (valeurs constantes associees) ici
    #   (D) le type vle::value::Value::XXXX correspondant
    #   (e) le type vle python d'une valeur correspondant a type_value (c'est
    #       le type attendu pour la valeur d'appel de addXxxxCondition) :
    #
    #   (A)      (B)      (C)                 (D)       (e)
    #   Boolean  boolean  TYPE_VALUE_BOOLEAN  BOOLEAN   bool
    #   Double   double   TYPE_VALUE_DOUBLE   DOUBLE    float
    #   Integer  integer  TYPE_VALUE_INTEGER  INTEGER   int
    #   String   string   TYPE_VALUE_STRING   STRING    str
    #   Map      map      TYPE_VALUE_MAP      MAP       dict (with str keys)  
    #   Set      set      TYPE_VALUE_SET      SET       list
    #   Table    table    TYPE_VALUE_TABLE    TABLE     VleTable (embeds list
    #                                                        of list of float)
    #   Tuple    tuple    TYPE_VALUE_TUPLE    TUPLE     VleTuple (embeds list 
    #                                                                of float) 
    #   Matrix   matrix   TYPE_VALUE_MATRIX   MATRIX    VleMatrix (embeds list 
    #                                                                 of list)
    #            xml      TYPE_VALUE_XMLTYPE  XMLTYPE   VleXML (embeds str) 
    #   Null     none     TYPE_VALUE_NIL      NIL 
    #   (tout autre : none)
    #
    #
    # Avec pyvle-1.0.2 sans patch, les valeurs retournees par
    # getConditionValueType sont :
    # boolean (pour Boolean choisi/saisi sous gvle)
    # double (pour Double choisi/saisi sous gvle)
    # integer (pour Integer choisi/saisi sous gvle)
    # string (pour String choisi/saisi sous gvle)
    # map (pour Map choisi/saisi sous gvle)
    # set (pour Set choisi/saisi sous gvle)
    # none (pour Table, Tuple, Xml choisis/saisis sous gvle)
    # ??? (pour Matrix, Null choisis/saisis sous gvle)
    # !!! comment a terminer !!!
    #
    # Pour memo avec pyvle-1.0.1, les valeurs retournees par
    # getConditionValueType sont :
    # boolean (pour Boolean choisi/saisi sous gvle)
    # double (pour Double choisi/saisi sous gvle)
    # integer (pour Integer choisi/saisi sous gvle)
    # string (pour String choisi/saisi sous gvle)
    # map (pour Map choisi/saisi sous gvle)
    # set (pour Set choisi/saisi sous gvle)
    # none (pour Table, Tuple, Matrix, Xml choisis/saisis sous gvle)
    #
    #*************************************************************************

    # Valeurs retournees par get_type_value
    TYPE_VALUE_DOUBLE   = 'double'
    TYPE_VALUE_INTEGER  = 'integer'
    TYPE_VALUE_STRING   = 'string'
    TYPE_VALUE_BOOLEAN  = 'boolean'
    TYPE_VALUE_MAP      = 'map'
    TYPE_VALUE_SET      = 'set'
    TYPE_VALUE_TUPLE    = 'tuple'
    TYPE_VALUE_TABLE    = 'table'
    TYPE_VALUE_XMLTYPE  = 'xml'
    TYPE_VALUE_MATRIX   = 'matrix'
    TYPE_VALUE_NIL      = 'none'

    @classmethod
    def is_type_value( cls, v ) :
        return ( v in ( cls.TYPE_VALUE_DOUBLE, cls.TYPE_VALUE_INTEGER, cls.TYPE_VALUE_STRING, cls.TYPE_VALUE_BOOLEAN, cls.TYPE_VALUE_MAP, cls.TYPE_VALUE_SET, cls.TYPE_VALUE_TUPLE, cls.TYPE_VALUE_TABLE, cls.TYPE_VALUE_XMLTYPE, cls.TYPE_VALUE_MATRIX, cls.TYPE_VALUE_NIL ) )

    @classmethod
    def is_type_value_not_nul( cls, v ) :
        return ( cls.is_type_value(v) and ( v != cls.TYPE_VALUE_NIL ) )

    ##..
    #*************************************************************************\n
    # get_type_value retourne le type de la 1ere value du port de la condition
    #\n*************************************************************************
    def get_type_value(self, condition, port ):
        type_value = self.getConditionValueType(condition, port, 0 ) # type du premier element
        return type_value

    #*************************************************************************
    #
    # Utilitaires pour conversions et verifications
    #
    # Cette partie de code a ete ecrite pour pyvle-1.0.3 (pyvle-1.0.2 + patches)
    #
    #*************************************************************************

    #*************************************************************************
    # Verifie que str s est de la forme du type voulu (list, VleTuple...)
    #*************************************************************************

    @classmethod
    def is_str_list(cls, s ) :
        cr = False # par defaut
        if isinstance( s, str ) :
            if s.startswith('[') and s.endswith(']') :
                cr = True
        return cr

    @classmethod
    def is_str_dict(cls, s ) :
        cr = False # par defaut
        if isinstance( s, str ) :
            if s.startswith('{') and s.endswith('}') :
                cr = True
        return cr

    # generique (commun)
    @classmethod
    def is_str_Vle(cls, s, prefixe, suffixe ) :
        cr = False # par defaut
        if isinstance( s, str ) :
            if s.startswith(prefixe) and s.endswith(suffixe) :
                cr = True
        return cr

    @classmethod
    def is_str_VleTuple(cls, s ) :
        prefixe = '<VleTuple('
        suffixe = ')>'
        return cls.is_str_Vle( s, prefixe, suffixe )

    @classmethod
    def is_str_VleTable(cls, s ) :
        prefixe = '<VleTable('
        suffixe = ')>'
        return cls.is_str_Vle( s, prefixe, suffixe )

    @classmethod
    def is_str_VleMatrix(cls, s ) :
        prefixe = '<VleMatrix('
        suffixe = ')>'
        return cls.is_str_Vle( s, prefixe, suffixe )

    @classmethod
    def is_str_VleXML(cls, s ) :
        prefixe = '<VleXML('
        suffixe = ')>'
        return cls.is_str_Vle( s, prefixe, suffixe )

    ##..
    #*************************************************************************\n
    # Essaie d'enlever prefixe et suffixe du str v puis d'en 'extraire' list
    #\n*************************************************************************
    @classmethod
    def extraire_list(cls, v, prefixe, suffixe) :
        conversion_ok = False # par defaut
        val = None # par defaut
        if v.startswith(prefixe) and v.endswith(suffixe) :
            v = v[len(prefixe):-len(suffixe)]
            # extraction list (de str v)
            try:
                c = literal_eval( v ) # contenu du str
                # extraction ok, s'assurer du format
                try:
                    val = c
                    if isinstance(val,list) :
                        conversion_ok = True
                except : pass
            except : pass
        return (conversion_ok,val)

    ##..
    #*************************************************************************\n
    # Essaie d'enlever prefixe et suffixe du str
    #\n*************************************************************************
    @classmethod
    def extraire_str(cls, v, prefixe, suffixe) :
        conversion_ok = False # par defaut
        val = None # par defaut
        if v.startswith(prefixe) and v.endswith(suffixe) :
            val = v[len(prefixe):-len(suffixe)]
            conversion_ok = True
        return (conversion_ok,val)

    #*************************************************************************
    # Methodes de conversion de valeur v saisie (str) en type vle python (e)
    # des verifications/controles sont effectues, en amont/complement de 
    # celles qui seront effectuees apres dans/par addXXXCondition (cf try)
    #*************************************************************************

    @classmethod
    def conversion_VleTable(cls, v) :

        prefixe = '<VleTable('
        suffixe = ')>'
        (conversion_ok,value) = cls.extraire_list(v,prefixe,suffixe)

        if conversion_ok :
            # VleTable (embeds list of list of float)
            cvalue = []
            for value2 in value:
                if isinstance(value2,list) :
                    cvalue2 = []
                    for v in value2 :
                        if isinstance(v,float):
                            cvalue2.append(v)
                        elif isinstance(v,int) or isinstance(v,long) : # reel saisi sous forme d'entier
                            cvalue2.append( float(v) ) # cast
                        else :
                            conversion_ok = False
                    cvalue.append( cvalue2 )
                else :
                    conversion_ok = False

        if conversion_ok :
            val = VleTable(cvalue)
        else :
            val = None

        return (conversion_ok,val)

    @classmethod
    def conversion_VleTuple(cls, v) :

        prefixe = '<VleTuple('
        suffixe = ')>'
        (conversion_ok,value) = cls.extraire_list(v,prefixe,suffixe)

        if conversion_ok :
            # format VleTuple (embeds list of float)
            cvalue = []
            for v in value:
                if isinstance(v,float):
                    cvalue.append(v)
                elif isinstance(v,int) or isinstance(v,long) : # reel saisi sous forme d'entier
                    cvalue.append( float(v) )
                else :
                    conversion_ok = False

        if conversion_ok :
            val = VleTuple(cvalue)
        else :
            val = None

        return (conversion_ok,val)

    @classmethod
    def conversion_VleXML(cls, v) :

        prefixe = '<VleXML('
        suffixe = ')>'
        (conversion_ok,value) = cls.extraire_str(v,prefixe,suffixe)

        # VleXML (embeds str), pas de controle/verification

        if conversion_ok :
            val = VleXML(value)
        else :
            val = None

        return (conversion_ok,val)

    ##..
    #*************************************************************************\n
    # Verifie que v est un dict dont toutes les cles sont str
    #\n*************************************************************************
    @classmethod
    def is_dict_with_str_keys(cls, v) :
        cr = True # par defaut
        if isinstance(v,dict) :
            # dict (with str keys)
            for k in v.keys() :
                if not isinstance(k,str) :
                    cr = False
        else :
            cr = False
        return cr

    #*************************************************************************
    #
    # tenterEcrireValeur
    #
    # Cette methode depend de la version de pyvle
    #
    #*************************************************************************

    ##..
    #*************************************************************************\n
    # tenterEcrireValeur
    #
    # Version correspondant a pyvle-1.0.3 (pyvle-1.0.2 + patches).
    # Cette version ne fonctionne pas avec pyvle-1.0.1.
    #
    # Traitement fonction de type_value
    # Tentative d'ecriture de v dans le port (de cond) : ajout de v (valeur
    # typee, cf formatage) a la liste des elements/valeurs du port
    # Retourne True si ecriture effective/reussie et sinon False
    # raise exception en cas d'echec
    #
    # notes_de_developpement exp.py :
    # Attention, le traitement presuppose qu'on interdit dans tout texte les
    # chaines de caractere '<Vle' et ')>'
    #
    #*************************************************************************
    def tenterEcrireValeur( self, cond, port, v, type_value ) :
            motcodevle = '__motcodevle__' # constante

            cr_ok = True # par defaut
            # debut de txt_exception 
            txt_exception = "tenterEcrireValeur(cond:"+cond+",port:"+port+",v:"+v+",type_value:"+type_value+"): "

            #*****************************************************************
            #
            # conversion de v (str saisi) en val de type vle python (e)
            # (destine appel de addXxxxCondition)
            # formatage, verifications/controles en amont/complement de ce
            # qui est verifie apres, dans/par addXxxxCondition (cf try)
            # (le type de val : bool, float...)
            #
            #*****************************************************************

            val = None # par defaut
    
            #*****************************************************************
            # extraction (de str v)
            #*****************************************************************
            try: # essai
                c = literal_eval( v ) # contenu du str
                # extraction ok, s'assurer du format
                try:
                    val = c
                except (SyntaxError): # echec de conversion
                    diag = "echec de conversion"
                    txt_exception = txt_exception + " -- " + diag
                    cr_ok = False
                    t_err.message( "tenterEcrireValeur, " + diag )

            except (ValueError, SyntaxError): # cas str
                val = v

            #*****************************************************************
            # Cas de 'faux' str, qui est reste str car mal/non converti :
            # formatage lie a '<VleXxxx(...)>' (dans cas VleXxxx, list, dict)
            #*****************************************************************
            if cr_ok :
                #if val != None and
                if isinstance( val, str ) :

                    cas_traite = '' # par defaut
                    if self.is_str_list( val ) :
                        cas_traite = 'cas_list' # cas traite plus bas
                    elif self.is_str_dict( val ) :
                        cas_traite = 'cas_dict' # cas traite plus bas

                    #*********************************************************
                    # '<VleTable(...)>' correspondant au cas
                    # VleTable (embeds list of list of float)
                    #*********************************************************
                    elif self.is_str_VleTable( val ) :
                        (conversion_ok,val) = self.conversion_VleTable(val)
                        if not conversion_ok :
                            diag = "echec de conversion VleTable"
                            txt_exception = txt_exception + " -- " + diag
                            cr_ok = False
                            t_err.message( "tenterEcrireValeur, " + diag )

                    #*********************************************************
                    # '<VleTuple(...)>' correspondant au cas
                    # VleTuple (embeds list of float) 
                    #*********************************************************
                    elif self.is_str_VleTuple( val ) :
                        (conversion_ok,val) = self.conversion_VleTuple(val)
                        if not conversion_ok :
                            diag = "echec de conversion VleTuple"
                            txt_exception = txt_exception + " -- " + diag
                            cr_ok = False
                            t_err.message( "tenterEcrireValeur, " + diag )

                    #*********************************************************
                    # '<VleXML(...)>' correspondant au cas
                    # VleXML (embeds str) 
                    #*********************************************************
                    elif self.is_str_VleXML( val ) :
                        (conversion_ok,val) = self.conversion_VleXML(val)
                        if not conversion_ok :
                            diag = "echec de conversion VleXML"
                            txt_exception = txt_exception + " -- " + diag
                            cr_ok = False
                            t_err.message( "tenterEcrireValeur, " + diag )

                    #*********************************************************
                    # '<Vlematrix(...)>' correspondant au cas
                    # VleMatrix (embeds list of list)
                    #*********************************************************
                    elif self.is_str_VleMatrix( val ) :
                        # val est traite sans '<VleMatrix(' et ')>' qui lui
                        # seront 'rajoutes' apres/a la fin
                        prefixe = '<VleMatrix('
                        suffixe = ')>'
                        val = val[len(prefixe):-len(suffixe)]
                        cas_traite = 'cas_VleMatrix'

                    #*********************************************************
                    # cas ou peuvent etre contenus des elements de type
                    # VleXxxx (list, dict, VleMatrix)
                    #*********************************************************
                    if (cas_traite=='cas_list') or (cas_traite=='cas_dict') or (cas_traite=='cas_VleMatrix') :


                        # les parties/morceaux du  type '<VleXxxx ... )>'
                        # (avec Xxxx = Tuple ou Table ou Matrix ou XML)
                        # sont extraits de val (avant litteral_eval pour
                        # permettre conversion de val en list ou dict) et
                        # sont stockes dans stock_temporaire (pour
                        # reintegration dans value en tant que VleXxxx
                        # (et non plus str))
                        stock_temporaire = []
                        continuer = True
                        while continuer :
                            # sousChaine de forme '<Vle...)>'
                            prefixe = '<Vle'
                            suffixe = ')>'
                            pos1 = val.find( prefixe )
                            pos2 = val.find( suffixe )
                            if (pos1!=-1) and (pos2!=-1) and (pos1<pos2) :
                                pos2 = pos2 + len( suffixe )
                                sousChaine = val[pos1:pos2]
                                sousChaine = sousChaine[1:-1] # suppression '<' et '>'
                                stock_temporaire.append( eval(sousChaine) ) # eval : de str a VleXxxx # !!! eval
                                # sousChaine est remplace dans val par motcodevle
                                # (qui sert de reperage pour reintegration future) 
                                val = val[0:pos1] + "\'" + motcodevle + "\'" + val[pos2:]
                            else : 
                                continuer = False
                            stock_temporaire.reverse() # pour futur depilement
       
                        # extraction de value (du str val)
                        value = None
                        try: # essai
                            c = literal_eval( val ) # contenu du str
                            # extraction ok, s'assurer du format
                            try:
                                value = c
                            except (SyntaxError): # echec de conversion
                                pass
                        except (ValueError, SyntaxError): # cas str
                            value = val # reste str
        
                        # si conversion ok, alors
                        # reintegration des elements de stock_temporaire
                        # dans value en tant que VleXxxx (et non plus str)
                        if (cas_traite=='cas_list') :
                            if isinstance( value, list ) :
                                value_list = []
                                for v in value :
                                    if v == motcodevle :
                                        value_list.append(stock_temporaire.pop())
                                    else :
                                        value_list.append(v)
                                val = value_list
                            else :
                                diag = "echec de conversion (cas de list)"
                                txt_exception = txt_exception + " -- " + diag
                                cr_ok = False
                                t_err.message( "tenterEcrireValeur, " + diag )
    
                        elif (cas_traite=='cas_dict') :
                            if isinstance( value, dict ) :
                                value_dict = {}
                                for (k,v) in value.iteritems() :
                                    if v == motcodevle :
                                        value_dict[k] = stock_temporaire.pop()
                                    else :
                                        value_dict[k] = v
                                val = value_dict
                            else :   
                                diag = "echec de conversion (cas de dict)"
                                txt_exception = txt_exception + " -- " + diag
                                cr_ok = False
                                t_err.message( "tenterEcrireValeur, " + diag )
        
                        if (cas_traite=='cas_VleMatrix') :
                            conversion_ok = False # par defaut
                            if isinstance( value, list ) :
                                conversion_ok = true # pour l'instant
                                value_list = []
                                for v in value :
                                    if v == motcodevle :
                                        value_list.append(stock_temporaire.pop())
                                    else :
                                        value_list.append(v)

                                # poursuite verifications propres a VleMatrix
                                # VleMatrix (embeds list of list)
                                for value2 in value_list :
                                    if not isinstance(value2,list) :
                                        conversion_ok = False

                                if conversion_ok :
                                    val = VleMatrix(value_list)

                            if not conversion_ok :
                                diag = "echec de conversion VleMatrix"
                                txt_exception = txt_exception + " -- " + diag
                                cr_ok = False
                                t_err.message( "tenterEcrireValeur, " + diag )

            #*****************************************************************
            # cas TYPE_VALUE_MAP : dict (with str keys)
            # controle format keys (str)
            #*****************************************************************
            if cr_ok :
                if val != None :
    
                    if type_value == self.TYPE_VALUE_MAP :
                        if not self.is_dict_with_str_keys(val) :
    
                            diag = "echec de format dict/map : la cle doit etre un str"
                            txt_exception = txt_exception + " -- " + diag
                            cr_ok = False
                            t_err.message( "tenterEcrireValeur, " + diag )


            #*****************************************************************
            # cas de reel saisi sous forme d'entier, cas d'entier saisi sous
            # forme de reel : forcage du type
            #*****************************************************************
            if cr_ok :
                if val != None :
                    if type_value == self.TYPE_VALUE_INTEGER :
                        if isinstance(val, float) or isinstance(val, long) :
                            val = int(val)
                    if type_value == self.TYPE_VALUE_DOUBLE :
                        if isinstance(val, int) or isinstance(val, long) :
                            val = float(val)

            #*****************************************************************
            #
            # ajout de valeur val typee (appel de addXXXCondition)
            #
            #*****************************************************************
            if cr_ok :
                if val != None :

                    if self.is_type_value_not_nul( type_value ) :

                        try :
                            if type_value == self.TYPE_VALUE_BOOLEAN :
                                self.addBooleanCondition(cond, port, val)
                            elif type_value == self.TYPE_VALUE_INTEGER :
                                self.addIntegerCondition(cond, port, val)
                            elif type_value == self.TYPE_VALUE_DOUBLE :
                                self.addRealCondition(cond, port, val)
                            elif type_value == self.TYPE_VALUE_STRING :
                                self.addStringCondition(cond, port, val)
                            elif type_value == self.TYPE_VALUE_MAP :
                                self.addMapCondition(cond, port, val)
                            elif type_value == self.TYPE_VALUE_SET :
                                self.addSetCondition(cond, port, val)
                            elif type_value == self.TYPE_VALUE_MATRIX :
                                self.addMatrixCondition(cond, port, val)
                            elif type_value == self.TYPE_VALUE_TABLE :
                                self.addTableCondition(cond, port, val)
                            elif type_value == self.TYPE_VALUE_TUPLE :
                                self.addTupleCondition(cond, port, val)
                            elif type_value == self.TYPE_VALUE_XMLTYPE :
                                self.addXMLCondition(cond, port, val)

                        except Exception, e :
                            diag = "echec de format, " + e.message
                            txt_exception = txt_exception + " -- " + diag
                            cr_ok = False
                            t_err.message( "tenterEcrireValeur, " + diag )

                    else : # notamment TYPE_VALUE_NIL
                        diag = "echec de type_value" 
                        txt_exception = txt_exception + " -- " + diag
                        cr_ok = False
                        t_err.message( "tenterEcrireValeur, " + diag )

                else :
                    diag = "echec valeur None"
                    txt_exception = txt_exception + " -- " + diag
                    cr_ok = False
                    t_err.message( "tenterEcrireValeur, " + diag )

            # else : cas (not cr_ok) deja 'vu/traite'
    
            if not cr_ok :
                raise Exception( txt_exception )

            return cr_ok


    ##..
    #*************************************************************************\n
    # tenterEcrireValeur
    #
    # Version obsolete, correspondant a pyvle-1.0.1.
    #
    # Cette version fonctionne egalement dans certains cas avec
    # pyvle-1.0.2 + patches mais pas dans tous (cf types VleTable...) et est
    # alors moins performante que la version de tenterEcrireValeur adaptee
    # a pyvle-1.0.3 (plus de controles...)
    #
    # traitement fonction de type_value
    #
    # Tentative d'ecriture de v dans le port (de cond) :
    # - ajout (non type) de v a la liste des elements/valeurs du port,
    # - puis ecrasement (type) du ieme element avec v.
    #
    # Les types 'set' et 'map' sont traites comme des cas particuliers
    # (exceptions) car la methode
    # setConditionValue(cond, port, v, type_value, i) ne peut pas leur
    # etre appliquee.
    #
    # Retourne True si ecriture effective/reussie et sinon False
    #
    # raise exception en cas d'echec
    #
    #*************************************************************************
    def pyvle101_tenterEcrireValeur( self, cond, port, v, type_value, i) :

            cr_ok = True # par defaut
            # debut de txt_exception 
            txt_exception = "tenterEcrireValeur(cond:"+cond+",port:"+port+",v:"+v+",type_value:"+type_value+", i:"+str(i)+ "): "

            # cas d'ajout puis ecrasement avec valeur typee
            if type_value in ('boolean', 'integer', 'double', 'string') :
                self.addValueCondition(cond, port, v)
                self.setConditionValue(cond, port, v, type_value, i)

            # cas non securise (si mauvais type) d'ajout seulement (sans ecrasement)
            elif type_value in ('map', 'set' ) :
    
                val = None # par defaut
                try: # essai extraction/conversion
                    c = literal_eval( v ) # contenu du str
                    # extraction ok, s'assurer du format
                    try:
                        val = c
                    except (SyntaxError): # echec de conversion
                        diag = "echec de conversion"
                        txt_exception = txt_exception + " -- " + diag
                        cr_ok = False
                        t_err.message( "tenterEcrireValeur, " + diag )
                except (ValueError, SyntaxError): # cas str
                    diag = "echec - cas str"
                    txt_exception = txt_exception + " -- " + diag
                    cr_ok = False
                    t_err.message( "tenterEcrireValeur, " + diag )

                # verification du type
                if cr_ok :
                    if val != None :
                        if type_value=='map' and not isinstance(val, dict) :
                            diag = "echec de type/format (dict attendu), type(val):" + type(val)
                            txt_exception = txt_exception + " -- " + diag
                            cr_ok = False
                            t_err.message( "tenterEcrireValeur, " + diag )
                        elif type_value=='set' and not isinstance(val, list) :
                            diag = "echec de type/format (list attendu), type(val):" + type(val)
                            txt_exception = txt_exception + " -- " + diag
                            cr_ok = False
                            t_err.message( "tenterEcrireValeur, " + diag )
        
                if cr_ok :
                    if val != None :
                        self.addValueCondition(cond, port, val)

            else : # notamment type_value 'none'
                diag = "echec de type_value"
                txt_exception = txt_exception + " -- " + diag
                cr_ok = False
                t_err.message( "tenterEcrireValeur, " + diag )

            if not cr_ok :
                raise Exception( txt_exception )

            return cr_ok

    ##..
    #*************************************************************************\n
    # Methode get_types_capture
    #
    # Construit typesCapture, le type de capture des vues :
    # dict ( vue : ( type, timestep ) )
    #
    #*************************************************************************
    def get_types_capture( self ):
        typesCapture = dict()
        for vue in self.listViews() :
            typesCapture[vue] = ( self.getViewType(vue), self.getViewTimeStep(vue) )
        return typesCapture

    ##..
    #*************************************************************************\n
    #
    # Lance la simulation en tant que simulation simple (avec run)
    # et retourne le resultat de la simulation simple.
    #
    # Le resultat : il s'agit des variables observees vue par vue.
    #
    # Format du resultat : dict ( vue : dict( nomvar : valeurs ) )
    #
    # Exemple de cle 'vue' : 'vueContraintes' \n
    # Exemple de cle 'nomvar' : 'sunfloV1,...contrainte_azote.FNIRUE'
    #
    #*************************************************************************
    def mono_run(self):
        r = None # par defaut
        r = self.run()
        return r

    ##..
    #*************************************************************************\n
    #
    # Lance la simulation en tant que simulation multiple (avec runManager) 
    # et retourne le resultat de la simulation multiple (plan d'experience).
    #
    # Le resultat : il s'agit, experience par experience, des variables
    # observees vue par vue.
    #
    # Format du resultat :
    # tuple de tuple (correspondant aux 2 indices de l'experience) de
    # dict ( vue : dict( nomvar : valeurs ) )
    #
    # Les 2 indices de l'experience/simulation sont ceux de son
    # identification parmi l'ensemble des simulations composant le plan
    # d'experience. Exemple '-0-0'.
    #
    # Exemple de cle 'vue' : 'vueContraintes' \n
    # Exemple de cle 'nomvar' : 'sunfloV1,...contrainte_azote.FNIRUE'
    #
    #*************************************************************************
    def multi_run(self):
        r = None # par defaut
        r = self.runManager()
        return r

    ##..
    #*************************************************************************\n
    # Passe toutes les vues existantes en mode 'storage'
    #\n*************************************************************************
    def setAllStorage(self):

        # Passe la vue view en mode 'storage'
        def setStorage( view ):
            output = self.getViewOutput(view)
            self.setOutputPlugin(output, '', 'local', 'storage')

        for view in self.listViews():
            setStorage(view)

    ##..
    #*************************************************************************\n
    # Lance la simulation en tant que simulation simple (avec run)
    # apres avoir passe toutes les vues en mode 'storage'
    # et retourne le resultat de la simulation simple (format dict attendu)
    #\n*************************************************************************
    def mono_runModeStorage(self):

        self.setAllStorage()

        res = self.mono_run()

        if not isinstance(res, dict):
            txt_exception = "simulation simple en mode storage, erreur de format du resultat retourne "
            txt_exception = txt_exception + str(type(res))
            txt_exception = txt_exception + " (attendu dict)"
            txt_exception = txt_exception + " ; resultat retourne : " + str(res)
            raise Exception( txt_exception )

        return res

    ##..
    #*************************************************************************\n
    # Lance la simulation en tant que simulation multiple (avec runManager) 
    # apres avoir passe toutes les vues en mode 'storage'
    # et retourne le resultat de la simulation multiple (plan d'experience)
    # (format tuple attendu) (plus exactement tuple de tuple de dict)
    #\n*************************************************************************
    def multi_runModeStorage(self):

        self.setAllStorage()

        res = self.multi_run()

        if not isinstance(res, tuple):
            txt_exception = "simulation multiple en mode storage, erreur de format du resultat retourne "
            txt_exception = txt_exception + str(type(res))
            txt_exception = txt_exception + " (attendu tuple)"
            txt_exception = txt_exception + " ; resultat retourne : " + str(res)
            raise Exception( txt_exception )

        return res

#*****************************************************************************

