#-*- coding:utf-8 -*-

## @file record/outil_web_record/scn_edition/views.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File views.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
# View scn_edition
#
# Edition d'un scenario (visualisation et eventuelle modification).
#
# Fait partie du processus d'exploitation des scenarios par les utilisateurs,
# selon lequel il s'agit de choisir un scenario (dans un paquet), puis de le
# simuler apres en avoir visualise et eventuellement modifie la configuration
# (parametrage), et enfin d'exploiter sous diverses formes les resultats de
# la simulation effectuee (les visualiser a l'ecran, telecharger...).
#
#*****************************************************************************

import copy

from django.shortcuts import render_to_response

from record.bd_modeles_record.models import ModeleRecord as ModeleRecordBd
#from record.bd_modeles_record.forms.user_forms import ModeleRecordFormComplet # formulaire 'classique'
from record.bd_modeles_record.forms.user_forms_ctg import ModeleRecordFormComplet # formulaire par categories

from record.outil_web_record.models.scn.scn import Scn
from record.outil_web_record.models.definition.definition import Definition 
from record.outil_web_record.models.affichage_saisie.parametres_requete import ParametresRequete
from record.outil_web_record.models.affichage_saisie.choix_operations import ChoixOperations as CHX # noms des choix
from record.outil_web_record.models.affichage_saisie.appel_page import NomsPages, contexte_affichage_scenario, contexte_simulation_menu, contexte_liste_scenarios
from record.outil_web_record.models.erreurs_gerees.erreurs_gerees import ErreursGerees as ERR # noms des erreurs
from record.outil_web_record.models.espace_exploitation.espace_exploitation import EspaceExploitation
from record.outil_web_record.models.conf_web.conf_web_modele_record import InfosApplicationId

from record.outil_web_record.utils.commun_views import initialiserControleur

from record.utils import session
from record.utils.erreur import Erreur # gestion des erreurs
from record.utils.erreur import get_message_exception
from configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

from django.utils.translation import ugettext as _

#*****************************************************************************
#
# EDITION DES INFORMATIONS DU SCENARIO
# (VISUALISATION ET EVENTUELLES MODIFICATIONS)
#
#*****************************************************************************

##..
#*****************************************************************************\n
#
# Le scenario est choisi. \n
#
# Le scenario choisi est un scenario existant ou un nouveau fichier envoye.
#
# Cas general ou le scenario choisi est un scenario existant
# (un_new_fichier_scenario_envoye=False) :
# - Le scenario est choisi (index_scenario_on de index_modele_record_on,
#   ainsi que sa presentation/configuration web index_application_associee).
#
# Cas particulier ou le scenario choisi est un nouveau fichier qui est envoye
# en tant que variante d'un scenario existant
# (un_new_fichier_scenario_envoye=True et
# cas_d_un_new_fichier_scenario_independant=False) :
# - index_modele_record_on est transmis. Le modele correspond a
#   index_modele_record_on.
# - index_scenario_on est transmis. Le fichier scenario a prendre en
#   consideration est le nouveau fichier envoye (avec son titre et sa
#   description) et non pas celui correspondant a index_scenario_on.
# - index_application_associee est transmis. La configuration web a prendre
#   en consideration est index_application_associee se referant a
#   index_scenario_on.
#
# Cas particulier ou le scenario choisi est un nouveau fichier qui est
# envoye sans lien avec un scenario existant
# (un_new_fichier_scenario_envoye=True et
# cas_d_un_new_fichier_scenario_independant=True) :
# - index_modele_record_on est transmis. Le modele correspond a
#   index_modele_record_on.
# - index_scenario_on n'est pas transmis. Le fichier scenario a prendre en
#   consideration est le nouveau fichier envoye (avec son titre et sa
#   description).
# - index_application_associee n'est pas transmis. La configuration web a
#   prendre en consideration est systematiquement l'application standard
#   (par defaut)
#
# Un nouveau fichier de donnees peut avoir ete envoye pour etre ajoute au
# repertoire data du modele.
#
# Prise en compte du scenario choisi (initialisation) \n
# Preparation de sa visualisation
#
#*****************************************************************************
def scenario_initialisation(request):
    t_ecr.trait()
    t_ecr.message("(scenario_initialisation:) appel de initialiserControleur")
    initialiserControleur(request)

    #*************************************************************************
    #
    # les informations en entree, initialisations
    #
    #*************************************************************************

    modele_record_id = None # par defaut
    if "modele_record_id" in request.POST.keys() :
        modele_record_id = request.POST["modele_record_id"]

    index_scenario_on = None # par defaut
    if "index_scenario_on" in request.POST.keys() :
        index_scenario_on = request.POST["index_scenario_on"]

    index_application_associee = None # par defaut
    if "index_application_associee" in request.POST.keys() :
        index_application_associee = request.POST["index_application_associee"]

    # !!! modele_record_id ne sert pas ; cf le_modele_record en session
    if modele_record_id is not None :
        modele_record_id = int(modele_record_id)

    if index_scenario_on is not None :
        index_scenario_on = int(index_scenario_on)
    if index_application_associee is not None :
        index_application_associee = int(index_application_associee)

    # Initialisation relative au cas de nouveau fichier scenario de
    # simulation (fichier vpz)
    un_new_fichier_scenario_envoye = False # par defaut
    nom_new_fichier_scenario = None # par defaut
    cas_d_un_new_fichier_scenario_independant = False # par defaut
    if ( index_scenario_on is None ) and ( index_application_associee is None ) :
        cas_d_un_new_fichier_scenario_independant = True

    # Initialisation relative au cas de nouveau fichier de donnees a ajouter
    # au repertoire data du modele
    un_new_fichier_data_envoye = False # par defaut
    nom_new_fichier_data = None # par defaut

    # Titre du nouveau scenario (optionnel) 
    new_titre_scenario = None # par defaut
    if "new_titre_scenario" in request.POST.keys() :
        new_titre_scenario = request.POST["new_titre_scenario"]
    if new_titre_scenario == "" : # "" considere comme vide (non renseigne)
        new_titre_scenario = None

    # Description du nouveau scenario (optionnel)
    new_description_scenario = None # par defaut
    if "new_description_scenario" in request.POST.keys() :
        new_description_scenario = request.POST["new_description_scenario"]
    if new_description_scenario == "" : # "" considere comme vide (non renseigne)
        new_description_scenario = None

    #*************************************************************************
    # recuperation des fichiers envoyes
    #*************************************************************************

    # repertoire VLE_HOME propre a la session/connexion (pour EspaceExploitation)
    VLE_HOME = session.get(request, 'VLE_HOME')

    # Nouveau fichier de donnees a ajouter au repertoire data du modele
    new_fichier_data = None # par defaut
    if "new_fichier_data" in request.FILES.keys() :
        new_fichier_data = request.FILES['new_fichier_data']
        nom_new_fichier_data = new_fichier_data.name
        EspaceExploitation.telecharger( VLE_HOME, new_fichier_data, nom_new_fichier_data )
        un_new_fichier_data_envoye = True

    # Nouveau fichier scenario de simulation (fichier vpz)
    new_fichier_scenario = None # par defaut
    if "new_fichier_scenario" in request.FILES.keys() :
        new_fichier_scenario = request.FILES['new_fichier_scenario']
        nom_new_fichier_scenario = new_fichier_scenario.name
        EspaceExploitation.telecharger( VLE_HOME, new_fichier_scenario, nom_new_fichier_scenario )
        un_new_fichier_scenario_envoye = True

    # !!! a ecrire
    # Nouveau dossier de donnees en remplacement du repertoire data du modele
    # livre sous forme de tar.gz
    un_new_dossier_data_envoye = False # par defaut
    # untar ...

    #*************************************************************************
    #print "\n\n\n\n\n"
    #print " new_titre_scenario :", new_titre_scenario 
    #print " new_description_scenario :", new_description_scenario
    #print ""
    #print " modele_record_id : ", modele_record_id
    #print " index_scenario_on : ", index_scenario_on
    #print " index_application_associee : ", index_application_associee
    #print ""
    #print " un_new_fichier_scenario_envoye : ", un_new_fichier_scenario_envoye
    #print " nom_new_fichier_scenario : ", nom_new_fichier_scenario
    #print " cas_d_un_new_fichier_scenario_independant : ", cas_d_un_new_fichier_scenario_independant
    #print ""
    #print " un_new_fichier_data_envoye : ", un_new_fichier_data_envoye
    #print " nom_new_fichier_data : ", nom_new_fichier_data
    #print ""
    #print " variable d'environnement VLE_HOME : ", session.get(request, 'VLE_HOME')
    #print "\n\n\n\n\n"
    #*************************************************************************


    #*************************************************************************
    # Prise en compte du scenario choisi (initialisation) : 
    # construction de scn mis a part scn.result
    #*************************************************************************
    traitement_effectue = False # par defaut
    message_erreur = "" # par defaut

    le_modele_record = session.get(request, 'le_modele_record')

    if cas_d_un_new_fichier_scenario_independant :

        if un_new_fichier_scenario_envoye :
        # le scenario choisi est un nouveau fichier sans lien avec un
        # scenario existant
        # - index_modele_record_on est transmis. Le modele correspond a
        #   index_modele_record_on.
        # - index_scenario_on n'est pas transmis. Le fichier scenario a
        #   prendre en consideration est le nouveau fichier envoye (avec son
        #   titre et sa description).
        # - index_application_associee n'est pas transmis. La configuration
        #   web a prendre en consideration est systematiquement l'application
        #   standard (par defaut)

            # scenario_vpz
            scenario_vpz = le_modele_record.getNouveauScenarioVpz( nom_new_fichier_scenario )
            if new_titre_scenario is not None : # a ete renseigne
                scenario_vpz.set_titre_infos_generales_initiales_scenario( new_titre_scenario )
            if new_description_scenario is not None : # a ete renseigne
                scenario_vpz.set_description_infos_generales_initiales_scenario( new_description_scenario )

            # infos_application_id
            infos_application_id = InfosApplicationId()
            infos_application_id.setValeursApplicationParDefaut()
        
            #traceChoix( scenario_vpz, infos_application_id ) # trace 
        
            # repertoire VLE_HOME propre a la session/connexion (pour EspaceExploitation)
            VLE_HOME = session.get(request, 'VLE_HOME')

            casDeFichierScenarioTelecharge=True
            try :
                scn = Scn( VLE_HOME, scenario_vpz, infos_application_id, casDeFichierScenarioTelecharge )
                traitement_effectue = True
            except Exception, e :
                diag = _(u"erreur de mise en place du scenario : ") + str(e) 
                message_erreur = message_erreur + " *** " + diag
                traitement_effectue = False

        else : # not un_new_fichier_scenario_envoye
            message_erreur = message_erreur + " *** " + _(u"erreur de saisie du nouveau fichier scenario (sans lien avec un scenario existant)")
            traitement_effectue = False

    else : # index_scenario_on et index_application_associee existent
    # le scenario choisi est un scenario existant ou le scenario choisi est un nouveau fichier variante d'un scenario existant

        if un_new_fichier_scenario_envoye :
        # le scenario choisi est un nouveau fichier variante d'un scenario
        # existant
        # - index_modele_record_on est transmis. Le modele correspond a
        #   index_modele_record_on.
        # - index_scenario_on est transmis. Le fichier scenario a prendre en
        #   consideration est le nouveau fichier envoye (avec son titre et sa
        #   description) et non pas celui correspondant a index_scenario_on.
        # - index_application_associee est transmis. La configuration web a
        #   prendre en consideration est index_application_associee se
        #   referant a index_scenario_on.

            # '_zero' correspond au
            # scenario existant dont le scenario d'interet est une variante
            application_associee_zero = le_modele_record.getApplicationWeb( index_scenario_on, index_application_associee )
            infos_application_id_zero = application_associee_zero.getId()
            scenario_on_zero = le_modele_record.scenarios_on[index_scenario_on]
            nom_scenario_zero = scenario_on_zero.nom_scenario

            # scenario_vpz
            scenario_vpz = le_modele_record.getNouveauScenarioVpz( nom_new_fichier_scenario, nom_scenario_zero )
            if new_titre_scenario is not None : # a ete renseigne
                scenario_vpz.set_titre_infos_generales_initiales_scenario( new_titre_scenario )
            if new_description_scenario is not None : # a ete renseigne
                scenario_vpz.set_description_infos_generales_initiales_scenario( new_description_scenario )

            # infos_application_id
            infos_application_id = infos_application_id_zero 

            #traceChoix( scenario_vpz, infos_application_id ) # trace 
        
            # repertoire VLE_HOME propre a la session/connexion (pour EspaceExploitation)
            VLE_HOME = session.get(request, 'VLE_HOME')

            casDeFichierScenarioTelecharge=True
            try :
                scn = Scn( VLE_HOME, scenario_vpz, infos_application_id, casDeFichierScenarioTelecharge )
                traitement_effectue = True
            except Exception, e :
                diag = _(u"erreur de mise en place du scenario : ") + str(e) 
                message_erreur = message_erreur + " *** " + diag
                traitement_effectue = False

        else : # not un_new_fichier_scenario_envoye
        # le scenario choisi est un scenario existant
        # - Le scenario est choisi (index_scenario_on de
        #   index_modele_record_on, ainsi que sa presentation/configuration
        #   web index_application_associee)

            scenario_vpz = le_modele_record.getScenarioVpz( index_scenario_on )
            application_associee = le_modele_record.getApplicationWeb( index_scenario_on, index_application_associee )

            infos_application_id = application_associee.getId()
        
            #traceChoix( scenario_vpz, infos_application_id ) # trace 
        
            # repertoire VLE_HOME propre a la session/connexion (pour EspaceExploitation)
            VLE_HOME = session.get(request, 'VLE_HOME')

            try :
                scn = Scn( VLE_HOME, scenario_vpz, infos_application_id )
                traitement_effectue = True
            except Exception, e :
                diag = _(u"erreur de mise en place du scenario : ") + str(e) 
                message_erreur = message_erreur + " *** " + diag
                traitement_effectue = False

    if traitement_effectue : # deroulement OK
        if un_new_dossier_data_envoye :
            pass #... le mettre a la place de  scn.espace_exploitation repertoire data ...
        if un_new_fichier_data_envoye :
            scn.espace_exploitation.transfererFichierDataTelecharge( nom_new_fichier_data )

    if traitement_effectue : # deroulement OK

        # scn enregistre dans session
        session.suppression_prealable(request, 'scn') # eventuel scn precedent
        session.set(request, 'scn', scn)
        session.sauver(request)

        # Note : avec la creation/construction de 'scn', il a ete cree un espace
        # d'exploitation dedie aux differentes operations qui vont par la suite
        # etre effectuees/demandees sur le scenario retenu. On travaille
        # dorenavant dans cet espace d'exploitation, sur scenario_vpz et non plus
        # scenario_vpz_d_origine.

        # le nom du repertoire data pour en afficher le contenu pour si besoin/voulu faciliter la saisie
        nom_repertoire_data = scn.espace_exploitation.getRepPkgData()
    
        #*********************************************************************
	    # Preparation du PREMIER affichage du scenario choisi
	    # (pour visualisation/modification)
        #*********************************************************************

        #*********************************************************************
        # Initialisations
        #*********************************************************************

        # scn
    
        # formulaire_modele_record (rappel des infos completes a titre
        # d'information/memo)
        modele_record_bd = ModeleRecordBd.objects.get(id=le_modele_record.id)
        formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)
    
        # La gestion d'erreur : erreur_saisie_blocs inactif au 1er affichage,
        # aucun message d'erreur en flashes
        flashes = []
    
        c = contexte_affichage_scenario(request, flashes, scn, nom_repertoire_data, formulaire_modele_record, formulaire_de_type_ctg=True ) # formulaire par categories

        # affichage du scenario (pour la 1ere fois)
        return render_to_response( NomsPages.NOM_page_affichage_scenario, c )

    else : # (not traitement_effectue) deroulement not OK

    #*************************************************************************
	# Retour en erreur a la page du choix d'un scenario du modele record
    #*************************************************************************

        #*********************************************************************
        # Initialisations
        #*********************************************************************

        # message d'erreur en flashes rendant compte
        # de not traitement_effectue
        message = _(u"Les choix effectués n'ont pas pu être pris en compte" ) + message_erreur
        flashes = [ message ]

        # le_modele_record
        le_modele_record = session.get(request, 'le_modele_record')

        # le nom du repertoire data du modele pour information sur son contenu
        # (rq : scn n'existe pas encore)
        nom_repertoire_data = None
        if le_modele_record is not None :
            nom_repertoire_data = le_modele_record.getNomRepertoireData()

        # formulaire_modele_record (rappel des infos completes a titre
        # d'information/memo)
        modele_record_bd = ModeleRecordBd.objects.get(id=le_modele_record.id)
        formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

        #*********************************************************************
        # Preparation du contexte d'affichage page du choix d'un scenario
        #*********************************************************************

        c = contexte_liste_scenarios(request, flashes, le_modele_record, nom_repertoire_data, formulaire_modele_record, formulaire_de_type_ctg=True ) # formulaire par categories

        return render_to_response( NomsPages.NOM_page_liste_scenarios, c )

##..
#*****************************************************************************\n
#
# Le scenario a deja ete affiche une 1ere fois. \n
# Le traitement effectue varie en fonction de id_operation qui caracterise
# l'endroit depuis lequel scenario_actualisation est appele et la raison de
# l'appel. \n
# Si appel depuis affichage scenario : met a jour le scenario (sa definition)
# a partir de l'eventuelle saisie. Sinon (par exemple appel retour en
# arriere), il n'y avait pas saisie. \n
# En fonction de id_operation : soit affichage du menu de simulation et sa
# preparation (par exemple dans cas de choix_validation_finale et si les
# informations affichees/saisies du scenario ont bien ete recuperees)  ; soit
# affichage du scenario et sa preparation, avec ou sans erreur selon la
# maniere dont se sont deroules les traitements (un cas sans erreur :
# choix_validation_intermediaire et les informations affichees/saisies du
# scenario ont bien ete recuperees).
#
#*****************************************************************************
def scenario_actualisation(request) :
    t_ecr.trait()
    t_ecr.message("(scenario_actualisation:) appel de initialiserControleur")
    initialiserControleur(request)

    #*************************************************************************
    # les informations en entree
    #*************************************************************************

    id_operation = None # par defaut
    if "id_operation" in request.POST.keys() :
        id_operation = request.POST["id_operation"]
    id_operation = int(id_operation)

    # Initialisations

    erreur_saisie_blocs = Erreur( ERR.NOM_erreur_saisie_blocs ) # desactivee par defaut

    # Cas appel en retour arriere pour affichage scenario (en cours)
    if id_operation == CHX.VAL_operation_retour_edition :

        # supprimer eventuel scn.resultat precedent ?
        # session.get(request, 'scn').resultat = None

        nom_template = NomsPages.NOM_page_affichage_scenario

    # Cas appel en retour arriere pour affichage menu de simulation
    elif id_operation == CHX.VAL_operation_retour_simulation :

        nom_template = NomsPages.NOM_page_simulation_menu

    # Cas appel depuis page d'affichage scenario (quittee par une validation)
    elif id_operation == CHX.VAL_operation_validation :

        # le traitement ParametresRequete convient car les input indesirables
        # (qui posent probleme) sont filtres au niveau html (cf submit et
        # class a_occulter)
        parametres_post = ParametresRequete( request.POST )

        #*********************************************************************
        #print ""
        #print "trace parametres_post : "
        #print parametres_post
        #print ""
        #from record.outil_web_record.models.affichage_saisie.affichage_saisie import DonneeSaisie
        #pliste = []
        #for k in parametres_post.get_cles() :
        #    v = parametres_post.get_valeurs( k )
        #    d = DonneeSaisie( k, v )
        #    if d.name.formatBlocVarOk() :
        #        pliste.append( d.decouper() )
        #for d in pliste :
        #    print d['indbloc'], "/", d['inddata'], ":", d['valeurs']
        #print ""
        #*********************************************************************

        # sans filtrage des input au niveau html, le traitement
        # ParametresRequeteBloc est necessaire (traitement ou certaines cles
        # specifiques sont ignorees) en remplacement de ParametresRequete
        #from record.outil_web_record.models.affichage_saisie.parametres_requete import ParametresRequeteBloc
        #parametres_post = ParametresRequeteBloc( request.POST )

        # le type de validation choisie
        # (entre choix_validationIntermediaire et choix_validationFinale)
        choix_validation = CHX.get_choix_validation(parametres_post)

        # donnees/blocs saisis : Le traitement miseAjourBlocsSaisis est applique
        # sur blocs de definition_copie (copie temporaire), et ne sera repercute
        # dans session sur scn.definition.blocs qu'en cas de bon deroulement.
        definition_copie = Definition()
        scn = session.get(request, 'scn')
        definition_copie.blocs = copy.deepcopy( scn.definition.blocs )
        try :
            cr_ok = definition_copie.miseAjourBlocsSaisis( parametres_post )

        except Exception, e :
            cr_ok = False
            erreur_saisie_blocs.activer()
            #erreur_saisie_blocs.set_message( e.message )
            erreur_saisie_blocs.set_message( get_message_exception(e) )

        if cr_ok : # bon deroulement de miseAjourBlocsSaisis

            # prise en compte du traitement (maj)
            session.get(request, 'scn').definition.blocs = copy.deepcopy( definition_copie.blocs )
            session.sauver(request)

            if choix_validation == CHX.NOM_choix_validationFinale :
                nom_template = NomsPages.NOM_page_simulation_menu
            else : # CHX.NOM_choix_validationIntermediaire et de plus par defaut
                nom_template = NomsPages.NOM_page_affichage_scenario
        else : # mauvais deroulement de miseAjourBlocsSaisis
            nom_template = NomsPages.NOM_page_affichage_scenario # avec erreur

    if nom_template == NomsPages.NOM_page_affichage_scenario :

        #*********************************************************************
        # Preparation du contexte d'affichage du scenario
        #*********************************************************************

        scn = session.get(request, 'scn')
        nom_repertoire_data = scn.espace_exploitation.getRepPkgData()

        # formulaire_modele_record (rappel des infos completes a titre d'information/memo)
        le_modele_record = session.get(request, 'le_modele_record')
        modele_record_bd = ModeleRecordBd.objects.get(id=le_modele_record.id)
        formulaire_modele_record = ModeleRecordFormComplet(instance=modele_record_bd)

        # Liste messages d'erreur en flashes
        flashes = [] # par defaut
        if erreur_saisie_blocs.isActive() : # maj flashes selon erreur_saisie_blocs
            texte_erreur = _(u"Erreur") + " : " + _(u"Les informations du scénario en cours (scénario d\'origine après saisie ou non de modifications) n\'ont pas pu être prises en compte") + "."
            texte_message_erreur = erreur_saisie_blocs.get_message()
            texte_precision = _(u"Retour à l\'état précédent du scénario en cours. Autrement dit ses informations actuelles ignorent les modifications qui avaient éventuellement été apportées au scénario juste avant la dernière demande de validation (boutons \'Validation intermédiaire\' ou \'Validation finale\'), qui a échoué") + "."
            texte_et_apres = _(u"Effectuer éventuellement de nouvelles modifications sur les informations du scénario en cours avant de valider (boutons \'Validation intermédiaire\' ou \'Validation finale\')") + "."
            flashes.append( texte_erreur )
            flashes.append( texte_message_erreur )
            flashes.append( texte_precision )
            flashes.append( texte_et_apres )

        c = contexte_affichage_scenario(request, flashes, scn, nom_repertoire_data, formulaire_modele_record, formulaire_de_type_ctg=True ) # formulaire par categories

    elif nom_template == NomsPages.NOM_page_simulation_menu :

        #*********************************************************************
        # Preparation du contexte d'affichage du menu de simulation
        #*********************************************************************

	    # Il s'agit du PREMIER affichage du menu de simulation
        scn = session.get(request, 'scn')

        # pour rappel des infos completes a titre d'information/memo
        le_modele_record = session.get(request, 'le_modele_record')

        # La gestion d'erreur : erreur_echec_simulation inactif au 1er
        # affichage, aucun message d'erreur en flashes
        flashes = []

        c = contexte_simulation_menu(request, flashes, scn, le_modele_record, formulaire_de_type_ctg=True )

    else :
        c = {} # vide

    return render_to_response(nom_template, c)


#*****************************************************************************
#
# Utilitaires
#
#*****************************************************************************

## trace ecran du choix
# (modele record, scenario, application web de configuration)
def traceChoix( scenario_vpz, infos_application_id ) :
    t_ecr.trait()
    m = "Le scenario choisi est le scenario : " + str(scenario_vpz.nom_vpz)
    m = m + " du modele : " + str(scenario_vpz.paquet.nom) + " (paquet " + str(scenario_vpz.paquet.nom_pkg) + "),"
    m = m + " a presenter selon la configuration web : "
    t_ecr.msg(m)

    m = "fichier vpz de configuration du dictionnaire : "
    m = m + str( infos_application_id.getNomAbsoluFichierConfigDicos() )
    m = m + " ; "
    m = m + "fichier vpz de configuration web propre aux applis web : "
    m = m + str( infos_application_id.getNomAbsoluFichierConfigWebApplis() )
    m = m + " ; "
    m = m + "nom application : "
    m = m + str( infos_application_id.getApplicationName() )
    m = m + " ; "
    m = m + "type source : "
    m = m + str( infos_application_id.getTypeSourceConfWebScenario() )
    t_ecr.msg( m )

#*****************************************************************************

