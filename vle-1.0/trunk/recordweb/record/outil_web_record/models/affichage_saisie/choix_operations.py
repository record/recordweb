#-*- coding:utf-8 -*-

## @file record/outil_web_record/models/affichage_saisie/choix_operations.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File choix_operations.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# Classes pour gestion d'affichage et saisie
#
#*****************************************************************************

##..
#*****************************************************************************\n
# Modele : ChoixOperations
#
# Choix d'operation proposes a l'utilisateur
#
# Des constantes (les noms des choix proposes...)
#
# La gestion des choix d'operations proposes a l'utilisateur s'appuie sur des
# constantes. Un choix a proposer dans une page html lui est transmis dans le
# contexte d'appel. Propositions conditionnelles : le choix a pour valeur
# VAL_choix_a_cacher dans le contexte transmis a la page pour qu'il ne soit
# pas propose/affiche (la page cache le choix en question).
#
#*****************************************************************************
class ChoixOperations(object) :

    # Valeur donnee a un choix (transmis a une page) qui doit etre 
    # cache/invisible
    VAL_choix_a_cacher = 'choix_a_cacher'

    #*************************************************************************
    # Choix du type de validation (dans page d'affichage scenario)
    #*************************************************************************

    # transcodage du choix du type de validation
    NOM_choix_validationIntermediaire = 'choix_validationIntermediaire'
    NOM_choix_validationFinale = 'choix_validationFinale'

    ## retourne la valeur du choix selectionne (transmise dans
    # ParametresRequete params)
    @classmethod
    def get_choix_validation( cls, params ) :
        if params.contient_name( cls.NOM_choix_validationFinale ) :
            choix_validation = cls.NOM_choix_validationFinale
        else : # choix_validationIntermediaire et de plus par defaut
            choix_validation = cls.NOM_choix_validationIntermediaire
        return choix_validation

    #*************************************************************************
    # Valeurs prises par id_operation de scenario_actualisation
    # L'identifiant id_operation dedie a scenario_actualisation caracterise
    # l'endroit depuis lequel scenario_actualisation est appele et la raison
    # de l'appel.
    #*************************************************************************

    # appel depuis page d'affichage scenario (quittee par une validation)
    VAL_operation_validation = 21

    # appel en retour arriere pour affichage scenario (en cours)
    VAL_operation_retour_edition = 22

    # appel en retour arriere pour affichage menu de simulation
    VAL_operation_retour_simulation = 23

    #*************************************************************************
    # Valeurs prises par id_operation de scenario_selection
    # L'identifiant id_operation dedie a scenario_selection caracterise
    # l'endroit depuis lequel scenario_selection est appele et la raison
    # de l'appel.
    #*************************************************************************

    # appel pour selection scenario avec raz/oubli des eventuels
    # deverouillages existants
    VAL_operation_selection_raz = 0

    # appel pour selection scenario avec memorisation des eventuels
    # deverouillages existants
    VAL_operation_selection_memo = 1

    #*************************************************************************
    # Valeurs prises par id_operation de scenario_simulation
    # L'identifiant id_operation dedie a scenario_simulation caracterise
    # l'endroit depuis lequel scenario_simulation est appele et la raison de
    # l'appel (simulation ou retour arriere).
    #*************************************************************************

    # appel en retour arriere pour affichage menu de restitution
    VAL_operation_retour_restitution = 31

    # appel depuis page du menu de simulation (quittee par un choix de
    # simulation) :
    #*************************************************************************
    # Choix du type de simulation (dans page menu de simulation)
    #*************************************************************************

    # transcodage du choix du type de simulation
    VAL_choix_simulationSimple = 40
    VAL_choix_simulationMultipleModeLineaire = 41
    VAL_choix_simulationMultipleModeTotal = 42

    ## Retourne True si id_operation correspond a un des choix de simulation
    # et False sinon
    @classmethod
    def is_choix_simulation( cls, id_operation ) :
        cr = False # par defaut
        if id_operation == cls.VAL_choix_simulationSimple :
            cr = True
        elif id_operation == cls.VAL_choix_simulationMultipleModeLineaire  :
            cr = True
        elif id_operation == cls.VAL_choix_simulationMultipleModeTotal :
            cr = True
        return cr

    ## Retourne (type_simulation,mode_combinaison) correspondant au choix de
    # simulation. \n
    # Les valeurs prises par type_simulation et mode_combinaison
    # correspondent a Resultat.type_simulation et Resultat.mode_combinaison
    @classmethod
    def get_simulation_type_et_mode( cls, choix_simulation ) :
        if choix_simulation == cls.VAL_choix_simulationMultipleModeTotal :
            type_simulation = "multi_simulation"
            mode_combinaison = "mode_total"
        elif choix_simulation == cls.VAL_choix_simulationMultipleModeLineaire  :
            type_simulation = "multi_simulation"
            mode_combinaison = "mode_lineaire"
        else : # cls.VAL_choix_simulationSimple et de plus par defaut
            type_simulation = "mono_simulation"
            mode_combinaison = "" # sans objet
        return (type_simulation, mode_combinaison)

    #*************************************************************************
    #
    # Choix du type de restitution
    #
    #*************************************************************************

    # transcodage du choix du type de restitution :

    #*************************************************************************
    # representations graphiques
    #*************************************************************************

    VAL_choix_graphY = 51
    VAL_choix_graphXY = 52

    # VAL_choix_graph pour ensemble des representations graphiques
    # (est utilise par menu de restitution pour cacher/montrer)
    VAL_choix_graph = 50

    ## Retourne True si id_operation correspond a un des choix
    # d'affichage/representation graphique et False sinon
    @classmethod
    def is_choix_graphique( cls, id_operation ) :
        cr = False # par defaut
        if id_operation == cls.VAL_choix_graphY :
            cr = True
        elif id_operation == cls.VAL_choix_graphXY :
            cr = True
        return cr

    #*************************************************************************
    # telechargements (dans page menu de telechargement)
    #*************************************************************************

    VAL_choix_telechgtDossier = 61
    VAL_choix_telechgtDernierFichier = 62

    # VAL_choix_telechgt pour ensemble des telechargements
    # (est utilise par menu de restitution pour cacher/montrer)
    VAL_choix_telechgt = 60

    ## Retourne True si id_operation correspond a un des choix de 
    # telechargement et False sinon
    @classmethod
    def is_choix_telechargement( cls, id_operation ) :
        cr = False # par defaut
        if id_operation == cls.VAL_choix_telechgtDossier :
            cr = True
        elif id_operation == cls.VAL_choix_telechgtDernierFichier :
            cr = True
        return cr

    #*************************************************************************
    # affichages numeriques
    #*************************************************************************
    VAL_choix_num = 70

#*****************************************************************************

