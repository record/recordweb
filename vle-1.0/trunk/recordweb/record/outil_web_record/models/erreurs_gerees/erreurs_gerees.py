#-*- coding:utf-8 -*-

## @file record/outil_web_record/models/erreurs_gerees/erreurs_gerees.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File erreurs_gerees.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

##..
#*****************************************************************************\n
#
# Modele : les erreurs gerees
#
# Des constantes : les noms des erreurs gerees dans l'outil_web_record
#
# Les erreurs sont gerees selon la classe Erreurs.
#
#*****************************************************************************
class ErreursGerees(object) :

    #*************************************************************************
    # erreur_saisie_blocs :
    # Indique le cas d'erreur de recuperation des valeurs qui ont ete saisies
    # pour les donnees des blocs (affichage scenario).
    #*************************************************************************
    NOM_erreur_saisie_blocs = 'erreur_saisie_blocs'

    #*************************************************************************
    # erreur_echec_simulation :
    # Indique le cas d'une erreur de simulation (scenario_simulation).
    #*************************************************************************
    NOM_erreur_echec_simulation = 'erreur_echec_simulation'

    #*************************************************************************
    # erreur_configuration_graphique :
    # Indique le cas d'erreur ou le type d'affichage/representation graphique
    # demande n'a pas ete reconnu.
    # Pourra servir de plus si erreur levee en cours de traitement de
    # configuration du graphique (ce qui n'est pas le cas pour l'instant)
    #*************************************************************************
    NOM_erreur_configuration_graphique = 'erreur_configuration_graphique' 
 
    #*************************************************************************
    # erreur_affichage_graphique :
    # Indique le cas d'erreur ou le type d'affichage/representation graphique
    # demande n'a pas ete reconnu.
    # Sert de plus si erreur levee en cours de traitement d'affichage
    # graphique : cas ou aucun trace (X,Y) n'a ete selectionne pour un
    # affichage graphique.
    #*************************************************************************
    NOM_erreur_affichage_graphique = 'erreur_affichage_graphique' 

    #*************************************************************************
    # erreur_telechargement :
    # Indique le cas d'erreur ou le type de telechargement demande n'a pas
    # ete reconnu.
    # Pourra servir de plus si erreur levee en cours de traitement de
    # telechargement (ce qui n'est pas le cas pour l'instant)
    #*************************************************************************
    NOM_erreur_telechargement = 'erreur_telechargement' 

    #*************************************************************************
    # erreur_affichage_numerique :
    # Indique le cas d'erreur ou l'affichage numerique a ete demande alors
    # qu'il ne s'agit pas d'une simulation simple.
    # Pourra servir de plus si erreur levee en cours de traitement d'affichage
    # numerique (ce qui n'est pas le cas pour l'instant)
    #*************************************************************************
    NOM_erreur_affichage_numerique = 'erreur_affichage_numerique' 

#*****************************************************************************

