#-*- coding:utf-8 -*-

## @file record/outil_web_record/forms/commun_forms.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File commun_forms.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

from record.forms.commun_forms import setFieldsConfigurationReadOnlyA
from record.forms.commun_forms import setFieldsConfigurationDefautReadOnly

from record.outil_web_record.configs.conf_trace import CONF_trace
from record.utils.trace import TraceEcran, TraceErreur

# pour traces
t_ecr = TraceEcran(__file__,__name__,CONF_trace)
t_err = TraceErreur(__file__,__name__,CONF_trace)

from django.utils.translation import ugettext as _

from transmeta import TransMeta

#*****************************************************************************
#
#
# Des traitements/donnees communs aux formulaires
#
#
#*****************************************************************************

#import floppyforms as forms
#from form_utils.forms import BetterForm # pour formulaire par categories

##..
#*****************************************************************************\n
# Methode setFieldsConfigurationInfosLigneRappel
#
# Configuration des champs de fields relatif a formulaire BetterForm
# pour les 'pour info' dans la ligne des rappels
# (modele record, scenario et application)
#
#*****************************************************************************
def setFieldsConfigurationInfosLigneRappel( fields ):
    setFieldsConfigurationReadOnlyA( fields )

##..
#*****************************************************************************\n
# Methode setFieldsConfigurationInfosListeScenarios
#
# Configuration des champs de fields relatif a formulaire BetterForm
# pour les 'pour info' dans la liste de presentation des scenarios 
# (scenario et application)
#
#*****************************************************************************
def setFieldsConfigurationInfosListeScenarios( fields ):
    setFieldsConfigurationReadOnlyA( fields )

##..
#*****************************************************************************\n
# Methode setFieldsConfigurationInfosScenario
#
# Configuration des champs de fields relatif a formulaire BetterForm
# pour les 'pour info' dans la page de presentation d'un scenario 
# (bloc et donnees)
#
#*****************************************************************************
def setFieldsConfigurationInfosScenario( fields ):
    setFieldsConfigurationReadOnlyA( fields )

#*****************************************************************************
