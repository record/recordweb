#-*- coding:utf-8 -*-

## @file record/outil_web_record/outil_web_record_settings.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File outil_web_record_settings.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# For django settings of projects that use outil_web_record applications.
#
#*****************************************************************************

import os

## 'Verifie' le repertoire rep : les liens symboliques sont acceptes
def verificationLargeRepertoireOk( rep ) :
    verif = os.path.exists(rep) and os.path.isdir(rep)
    return verif

class outil_web_record_settings(object) :

    def __init__(self, var_RECORDWEB_HOME,
                       var_VLE_HOME,
                       var_RECORDWEB_VENDOR ) :

        #*********************************************************************
        #
        # IN (from settings) : verification de declarations et existences
        #
        #*********************************************************************

        # RECORDWEB_HOME, repertoire racine (projets, applications, librairies python)
        RECORDWEB_HOME = var_RECORDWEB_HOME
        # verif
        rep = RECORDWEB_HOME
        if not verificationLargeRepertoireOk( rep ) :
            print "ERREUR !!! outil_web_record_settings.py : répertoire ", rep, " N'EXISTE PAS"
        rep = os.path.join( RECORDWEB_HOME, 'record' )
        if not verificationLargeRepertoireOk( rep ) :
            print "ERREUR !!! outil_web_record_settings.py : répertoire ", rep, " N'EXISTE PAS"

        # VLE_HOME, ${HOME}/.vle, repertoire home de vle (plus exactement : valeur initiale donnee a la variable d'environnement VLE_HOME, repertoire sous lequel sont construits les repertoires VLE_HOME attribues a chaque session/connexion)
        VLE_HOME = var_VLE_HOME
        # verif
        rep = VLE_HOME
        if not verificationLargeRepertoireOk( rep ) :
            print "ERREUR !!! outil_web_record_settings.py : répertoire ", rep, " N'EXISTE PAS"

        # vendors ************************************************************

        # repertoire des librairies, applications django externes (django-transmeta...)
        RECORDWEB_VENDOR = var_RECORDWEB_VENDOR
        # verif
        rep = RECORDWEB_VENDOR
        if not verificationLargeRepertoireOk( rep ) :
            print "ERREUR !!! outil_web_record_settings.py : répertoire ", rep, " N'EXISTE PAS"

        #*********************************************************************
        #
        # OUT (to settings) :
        # elements de configuration propres a outil_web_record
        #
        #*********************************************************************

        #*********************************************************************
        # path de outil_web_record (correspond a RECORDWEB_HOME/outil_web_record)
        SITE_ROOT = os.path.realpath(os.path.dirname(__file__))

        # TEMPLATE_DIRS ******************************************************

        # emplacement des templates
        # propres a projet outil_web_record : templates_outil_web_record 
        # publics : templates_public
        templates_outil_web_record = os.path.join( SITE_ROOT, 'templates' )
        templates_public = os.path.join( RECORDWEB_HOME, 'record', 'templates' )

        self.TEMPLATE_DIRS_FIRST = ( templates_outil_web_record,)
        self.TEMPLATE_DIRS_LAST = ( templates_public,)

        # LOCALE_PATHS *******************************************************

        # emplacement translation file commun recordweb
        # (en amont de ceux de chaque projet)
        translation_recordweb = os.path.join( RECORDWEB_HOME, 'record', 'locale' )

        # directories where Django looks for translation files
        # (en plus des repertoires 'locale' de projet etc)
        # (du plus au moins prioritaire)
        #
        # Actuellement le propre dictionnaire de outil_web_record existe mais inusite
        #
        self.LOCALE_PATHS = ( translation_recordweb,)

        # STATICFILES_DIR ****************************************************

        # emplacement des fichiers css et js
        # propres a l'outil_web_record : SITE_MEDIA_OUTIL_WEB_RECORD
        # publics : SITE_MEDIA_PUBLIC
        SITE_MEDIA_OUTIL_WEB_RECORD = os.path.join( SITE_ROOT, 'site_media' )
        SITE_MEDIA_PUBLIC = os.path.join( RECORDWEB_HOME, 'record', 'site_media' )

        # Additional locations of static files
        self.STATICFILES_DIRS_FIRST = (
            # STATICFILES_DIRS avec collectstatic (du + au - prioritaire )
            SITE_MEDIA_OUTIL_WEB_RECORD, # os.path.join( SITE_ROOT, 'site_media' )
        )
        self.STATICFILES_DIRS_LAST = (
            # STATICFILES_DIRS avec collectstatic (du + au - prioritaire )
            SITE_MEDIA_PUBLIC,
        )

#*****************************************************************************

