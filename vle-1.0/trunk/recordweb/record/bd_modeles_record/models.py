#-*- coding:utf-8 -*-

## @file record/bd_modeles_record/models.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File bd_modeles_record/models.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# Le modele record enregistre en bd (ModeleRecord ...)
#
#*****************************************************************************

import os # pour getNomRepertoireDataDefault

from django.db import models
from django.contrib.auth.models import User

from django.utils.translation import ugettext as _

from transmeta import TransMeta

from record.utils.vle.pkg import Pkg

from record.bd_modeles_record.configs.conf_bd_modeles_record import CONF_bd_modeles_record

##..
#*****************************************************************************\n
# MotDePasse
#
# Chaque ModeleRecord prive est associe a un MotDePasse mot_de_passe. \n
# Un meme MotDePasse peut servir pour plusieurs ModeleRecord prives. \n
# Le mot_de_passe 'modele_public' est reserve/attribue a tout ModeleRecord
# public ; il doit exister par defaut dans la bd.
#
# MotDePasse est derive de User. Seuls certains champs de User sont
# exploites/geres ('username', 'password' ; cf MotDePasseAdmin).
#
#*****************************************************************************
class MotDePasse(User):

    class Meta :
        verbose_name = _(u"Mot de passe de Modèle(s)")
        verbose_name_plural = _(u"Mots de passe de Modèle(s)")

    def __unicode__(self):
        return 'mot de passe : %s' % (self.username)

#*****************************************************************************
# Constantes et methodes relatives au mot de passe public
#*****************************************************************************

mot_de_passe_public_username = 'modele_public'
mot_de_passe_public_password = 'public'

## construit et enregistre dans la bd le mot de passe public
def enregistrerMotDePassePublic():
    mot_de_passe_public = MotDePasse( username=mot_de_passe_public_username, password=mot_de_passe_public_password )
    mot_de_passe_public.save()

## determine si le MotDePasse mot_de_passe est ou non public
# (selon critere base sur username).
# retourne True pour public, False pour prive, None pour etat inconnu
def estMotDePassePublic( mot_de_passe ):
    cr_is_public = None # pour etat inconnu, par defaut
    if mot_de_passe is not None :
        if mot_de_passe.username == mot_de_passe_public_username :
            cr_is_public = True
        else :
            cr_is_public = False

    return cr_is_public

##..
#*****************************************************************************\n
# Individu
#
# Les responsable, responsable_scientifique et responsable_informatique des
# ModeleRecord sont des Individu. \n
# Un meme Individu peut jouer differents roles vis-a-vis de differents
# ModeleRecord.
#
# Individu est derive de User. Seuls certains champs de User sont
# exploites/geres ('username', 'first_name', 'last_name', 'email' ;
# cf IndividuAdmin).
#
#*****************************************************************************
class Individu(User):

    class Meta :
        verbose_name = _(u"Individu")
        verbose_name_plural = _(u"Individus")

    def __unicode__(self):
        return 'individu : %s' % (self.username)

#*****************************************************************************
# Methodes utiles au traitement de champs du ModeleRecord qui sont des noms 
# de repertoires/fichiers definis en tant que chemin relatif par rapport a un
# repertoire racine. Methodes egalement utilisees ailleurs
# (ParDefautModeleRecord, ModeleRecord...Form)
# Champs concernes :
# nom_repertoire_lot_pkgs, nom_repertoire_data,
# nom_fichier_config_web_conf, nom_fichier_config_web_applis,
# nom_fichier_config_dicos.
#*****************************************************************************

## get_conf_repertoire_racine_configuration retourne le repertoire racine
# (chemin absolu) a partir duquel sont donnes les chemins relatifs
# nom_fichier_config_web_conf, nom_fichier_config_web_applis et
# nom_fichier_config_dicos
def get_conf_repertoire_racine_configuration() :
    return CONF_bd_modeles_record.repertoire_racine_configuration

## get_conf_repertoire_racine_lots_pkgs retourne le repertoire racine (chemin 
# absolu) a partir duquel est donne le chemin relatif nom_repertoire_lot_pkgs
def get_conf_repertoire_racine_lots_pkgs() :
    return CONF_bd_modeles_record.repertoire_racine_lots_pkgs

## get_conf_repertoire_racine_data retourne le repertoire racine (chemin
# absolu) a partir duquel est donne le chemin relatif nom_repertoire_data
def get_conf_repertoire_racine_data() :
    return CONF_bd_modeles_record.repertoire_racine_data

# Methodes determinant et retournant le nom absolu de repertoire/fichier
# correspondant au nom relatif de repertoire/fichier.
# retourne None pour entree None ou ""

def getNomAbsoluRepertoireLotPkgs( nom_repertoire_lot_pkgs ) :
    rep = None # par defaut
    if (nom_repertoire_lot_pkgs is not None) and (nom_repertoire_lot_pkgs != "") :
        rep = os.path.join( get_conf_repertoire_racine_lots_pkgs(), nom_repertoire_lot_pkgs )
    return rep
def getNomAbsoluRepertoireData( nom_repertoire_data ) :
    rep = None # par defaut
    if (nom_repertoire_data is not None) and (nom_repertoire_data != "") :
        rep = os.path.join( get_conf_repertoire_racine_data(), nom_repertoire_data )
    return rep
def getNomAbsoluFichierConfigWebConf( nom_fichier_config_web_conf ) :
    f = None # par defaut
    if (nom_fichier_config_web_conf is not None) and (nom_fichier_config_web_conf != "") :
        f = os.path.join( get_conf_repertoire_racine_configuration(), nom_fichier_config_web_conf )
    return f
def getNomAbsoluFichierConfigWebApplis( nom_fichier_config_web_applis ) :
    f = None # par defaut
    if (nom_fichier_config_web_applis is not None) and (nom_fichier_config_web_applis != "") :
        f = os.path.join( get_conf_repertoire_racine_configuration(), nom_fichier_config_web_applis )
    return f
def getNomAbsoluFichierConfigDicos( nom_fichier_config_dicos ) :
    f = None # par defaut
    if (nom_fichier_config_dicos is not None) and (nom_fichier_config_dicos != "") :
        f = os.path.join( get_conf_repertoire_racine_configuration(), nom_fichier_config_dicos )
    return f

#*****************************************************************************
# Methodes utiles au traitement de nom_repertoire_data du ModeleRecord
# egalement utilisees ailleurs (ParDefautModeleRecord, ModeleRecord...Form)
#*****************************************************************************

## retourne la valeur de nom_repertoire_data correspondant a demande du
# repertoire par defaut
def defaultValueNomRepertoireData() :
    return ( "default" ) # ne doit pas valoir ""

## determine et retourne le nom (chemin absolu) du repertoire des donnees de
# simulation par defaut. Il s'agit du repertoire 'data' du paquet principal, 
# caracterise par le nom du paquet principal nom_pkg et son emplacement
# nom_repertoire_lot_pkgs (chemin relatif).
# rq : il existe aussi (dans ModeleRecord)
# getNomAbsoluRepertoireDataParDefaut(self)
def getNomAbsoluRepertoireDataParDefaut( nom_repertoire_lot_pkgs, nom_pkg ) :
    nom_absolu_repertoire_lot_pkgs = getNomAbsoluRepertoireLotPkgs( nom_repertoire_lot_pkgs )
    repertoire_absolu_paquet = os.path.join( nom_absolu_repertoire_lot_pkgs, nom_pkg )
    return ( Pkg.getRepertoirePaquetData( repertoire_absolu_paquet ) )

#*****************************************************************************
# Methodes utiles au traitement de champs du ModeleRecord prenant leurs 
# valeurs parmi 3 (ou moins) alternatives :
# vrai/affirmatif/oui, faux/negatif/non, indetermine
# egalement utilisees ailleurs (ParDefautModeleRecord, ModeleRecord...Form)
#*****************************************************************************

## retourne la valeur du choix vrai/affirmatif/oui
def choixVrai() :
    return ( "oui" )

## retourne la valeur du choix faux/negatif/non
def choixFaux() :
    return ( "non" )

## retourne la valeur du choix indetermine
def choixIndetermine() :
    return ( "indetermine" )

##..
#*****************************************************************************\n
# ParDefautModeleRecord : classe utilitaire relative a ModeleRecord
#
# Classe contenant/definissant des constantes de configuration de champs de
# ModeleRecord (valeurs par defaut de label, help_text...) servant a
# ModeleRecord et egalement susceptibles de servir a des ModeleRecord_xx_Form
#
#*****************************************************************************
class ParDefautModeleRecord(object):

	# nom
    label_nom = _(u"Nom")
    help_text_nom = _(u"Nom donné au modèle dans la base")
    max_length_nom = 200

	# description
    label_description = _(u"Description")
    help_text_description = _(u"Description du modèle")
    max_length_description = 500

    # url
    label_url = _(u"Plus d'informations")
    max_length_url = 200
    help_text_url = _(u"Lien vers une adresse internet donnant plus d'informations sur le modèle")

    # nom_repertoire_lot_pkgs
    label_nom_repertoire_lot_pkgs = _(u"lot des paquets vle")
    max_length_nom_repertoire_lot_pkgs = 500
    help_text_nom_repertoire_lot_pkgs = _(u"Nom (relatif) du répertoire contenant l'ensemble des paquets vle du modèle (nom libre).") + " " + _(u"Nom relatif depuis le répertoire") + " " + get_conf_repertoire_racine_lots_pkgs() + "."

    # nom_repertoire_data
    label_nom_repertoire_data = _(u"répertoire des fichiers de données")
    max_length_nom_repertoire_data = 500
    help_text_nom_repertoire_data = _(u"Nom (relatif) du répertoire contenant les fichiers de données de simulation du modèle (nom libre).") + " " + _(u"Nom relatif depuis le répertoire") + " " + get_conf_repertoire_racine_data() + "."
    help_text_nom_repertoire_data = help_text_nom_repertoire_data + " " + _(u"Valeur") + " '" + defaultValueNomRepertoireData() + "' " + _(u"pour le répertoire 'data' du paquet principal")

    # nom_pkg
    label_nom_pkg = _(u"Nom du paquet principal")
    max_length_nom_pkg = 200
    help_text_nom_pkg = _(u"Nom du paquet principal (project, sous pkgs) : celui qui contient les fichiers vpz scénario de simulation du modèle")

    # mot_de_passe
    label_mot_de_passe = _(u"Mot de passe")
    help_text_mot_de_passe = _(u"Mot de passe du modèle ('modele_public' pour un modèle public).")

    # modele_active
    label_modele_active = _(u"Modèle activé")
    max_length_modele_active = 200
    help_text_modele_active = _(u"Indique si le modèle doit être activé/proposé ou non dans l'outil Web Record ; un modèle qui est désactivé est présent dans la base mais non proposé par/au niveau de l'outil Web Record")

    texte_provision = _(u"(provision, signification à définir ultérieurement)")

    # caracteristique1
    label_caracteristique1 = _(u"Caractéristique n°1")
    max_length_caracteristique1 = 200
    help_text_caracteristique1 = _(u"Modèle de culture (domaine végétal)")

    # caracteristique2
    label_caracteristique2 = _(u"Caractéristique n°2")
    max_length_caracteristique2 = 200
    help_text_caracteristique2 = _(u"Modèle de système de culture (domaine végétal)")

    # caracteristique3
    label_caracteristique3 = _(u"Caractéristique n°3")
    max_length_caracteristique3 = 200
    help_text_caracteristique3 = _(u"Modèle de système de production (domaine végétal)")

    # caracteristique4
    label_caracteristique4 = _(u"Caractéristique n°4")
    max_length_caracteristique4 = 200
    help_text_caracteristique4 = _(u"Modèle à l'échelle d'un territoire (domaine végétal)")

    # caracteristique5
    label_caracteristique5 = _(u"Caractéristique n°5")
    max_length_caracteristique5 = 200
    help_text_caracteristique5 = _(u"Modèle architecture de plante ou bioagresseur (domaine végétal)")

    # caracteristique6
    label_caracteristique6 = _(u"Caractéristique n°6")
    max_length_caracteristique6 = 200
    help_text_caracteristique6 = _(u"Modèle animal (domaine animal)")

    # caracteristique7
    label_caracteristique7 = _(u"Caractéristique n°7")
    max_length_caracteristique7 = 200
    help_text_caracteristique7 = _(u"Modèle troupeau (domaine animal)")

    # caracteristique8
    label_caracteristique8 = _(u"Caractéristique n°8")
    max_length_caracteristique8 = 200
    help_text_caracteristique8 = _(u"Modèle de système de production (domaine animal)")

    # caracteristique9
    label_caracteristique9 = _(u"Caractéristique n°9")
    max_length_caracteristique9 = 200
    help_text_caracteristique9 = _(u"Modèle à l'échelle d'un territoire (domaine animal)")

    # caracteristique10
    label_caracteristique10 = _(u"Caractéristique n°10")
    max_length_caracteristique10 = 200
    help_text_caracteristique10 = _(u"Modèle utilitaire")

    # caracteristique11
    label_caracteristique11 = _(u"Caractéristique n°11")
    max_length_caracteristique11 = 200
    help_text_caracteristique11 = _(u"Modèle exemple")

    # caracteristique12
    label_caracteristique12 = _(u"Caractéristique n°12")
    max_length_caracteristique12 = 200
    help_text_caracteristique12 = texte_provision

    # caracteristique13
    label_caracteristique13 = _(u"Caractéristique n°13")
    max_length_caracteristique13 = 200
    help_text_caracteristique13 = texte_provision

    # caracteristique14
    label_caracteristique14 = _(u"Caractéristique n°14")
    max_length_caracteristique14 = 200
    help_text_caracteristique14 = texte_provision

    # caracteristique15
    label_caracteristique15 = _(u"Caractéristique n°15")
    max_length_caracteristique15 = 200
    help_text_caracteristique15 = texte_provision

    # caracteristique16
    label_caracteristique16 = _(u"Modèle réservé aux essais")
    max_length_caracteristique16 = 200
    help_text_caracteristique16 = _(u"Caractéristique (n°16) activée pour un modèle utilisé en cours de développement dans le cadre des essais et de la mise au point, qui ne doit pas apparaitre dans la version définitive mise en production")

    # fichiers vpz de configuration

    max_length_config = 500 # commun

    label_nom_fichier_config_web_conf = _(u"configuration web conf")
    help_text_nom_fichier_config_web_conf = _(u"Nom (relatif) du fichier vpz de configuration web propre à la conf web. Souvent nommé web_conf.vpz mais sans obligation (nom libre).") + " " + _(u"Nom relatif depuis le répertoire") + " " + get_conf_repertoire_racine_configuration() + "."

    label_nom_fichier_config_web_applis = _(u"configuration web applis")
    help_text_nom_fichier_config_web_applis = _(u"Nom (relatif) du fichier vpz de configuration web propre aux applis web. Souvent nommé web_applis.vpz mais sans obligation (nom libre).") + " " + _(u"Nom relatif depuis le répertoire") + " " + get_conf_repertoire_racine_configuration() + "."
    
    label_nom_fichier_config_dicos = _(u"configuration dictionnaire")
    help_text_nom_fichier_config_dicos = _(u"Nom (relatif) du fichier vpz de configuration propre au dictionnaire. Souvent nommé dicos.vpz mais sans obligation (nom libre).") + " " + _(u"Nom relatif depuis le répertoire") + " " + get_conf_repertoire_racine_configuration() + "."

    # responsables

    label_responsable = _(u"Responsable")
    help_text_responsable = _(u"Correspondant principal. Dans le cas de modèle privé, il distribue le mot de passe aux personnes de son choix.")

    label_responsable_scientifique = _(u"Responsable scientifique")
    help_text_responsable_scientifique = _(u"Correspondant pour les questions scientifiques")

    label_responsable_informatique = _(u"Responsable informatique")
    help_text_responsable_informatique = _(u"Correspondant pour les questions informatiques")

    # cr_reception
    label_cr_reception = _(u"Compte rendu de réception")
    max_length_cr_reception = 500
    help_text_cr_reception = _(u"Rapport de livraison du modèle (vérifications effectuées à la livraison, notes...)")

    # fonctionne_sous_linux
    label_fonctionne_sous_linux = _(u"Fonctionnement OK sous linux")
    max_length_fonctionne_sous_linux = 200
    help_text_fonctionne_sous_linux = _(u"Rend compte de la vérification du bon fonctionnement du modèle sous linux")

    # sans_trace_ecran
    label_sans_trace_ecran = _(u"Version sans trace écran")
    max_length_sans_trace_ecran = 200
    help_text_sans_trace_ecran = _(u"Indique si la version du modèle livrée/déposée effectue ou non des affichages/traces à l'écran")

    # pour_installation
    label_pour_installation = _(u"Informations d'installation")
    max_length_pour_installation = 500
    help_text_pour_installation = _(u"Informations d'installation du modèle à prendre en compte pour le rentrer dans le dépôt de l'outil Web : procédure, mention des librairies requises (par exemple GDAL pour Gengiscan, gfortran pour NativeStics) ...")

    # accord_exploitation
    label_accord_exploitation = _(u"Accord d'exploitation")
    max_length_accord_exploitation = 200
    help_text_accord_exploitation = _(u"Autorisation d'exploitation du modèle dans l'outil Web Record")

	# version_vle
    label_version_vle = _(u"Version vle de réception")
    max_length_version_vle = 200
    help_text_version_vle = _(u"Version vle sous laquelle le modèle a été livré")

##..
#*****************************************************************************\n
# ModeleRecord
#
# Les ModeleRecord sont les modeles record enregistres en bd, mis a
# disposition au sein de l'outil Web Record
#
# Un ModeleRecord comprend le nom de son pkg vle associe et d'autres
# renseignements necessaires a son exploitation : nom de son responsable,
# version vle, mot de passe (mot de passe prive ou 'modele_public') ...
# Un ModeleRecord ne contient pas la liste (dynamique) de ses scenarios
# (fichiers vpz).
#
# Note : la plupart des champs d'un ModeleRecord sont obligatoires mais
# certains sont optionnels (les fichiers vpz de configuration
# nom_fichier_config_xxx). Des verifications sont faites au sein de l'outil
# Web record pour/avant de considerer que le ModeleRecord peut etre
# choisi/exploite dans l'outil Web Record.
#
# Note : certains champs d'un ModeleRecord normalement obligatoires sont en
# fait rendus optionnels afin de faciliter la saisie des informations en bd.
# Par consequent un ModeleRecord incomplet peut exister dans la bd, par
# contre il ne sera pas propose/exploite au sein de l'outil Web record (cf
# verifications au niveau de l'outil).
#
#*****************************************************************************
class ModeleRecord(models.Model):

    __metaclass__ = TransMeta

    #*************************************************************************
    #
    #
    # Les champs du ModeleRecord
    #
    #
    #*************************************************************************

    #*************************************************************************
    #
    # Identite
    #
    #*************************************************************************

    #*************************************************************************
	# nom du modele 
    #*************************************************************************
    nom = models.CharField( ParDefautModeleRecord.label_nom, max_length=ParDefautModeleRecord.max_length_nom )
    nom.help_text = ParDefautModeleRecord.help_text_nom
    nom.blank = False # obligatoire (normalement obligatoire)

    #*************************************************************************
	# description du modele 
    #*************************************************************************
    description = models.TextField( ParDefautModeleRecord.label_description, max_length=ParDefautModeleRecord.max_length_description )
    description.help_text = ParDefautModeleRecord.help_text_description
    #description.blank = False # obligatoire (normalement obligatoire)
    description.blank = True # optionnel (rendu optionnel pour faciliter saisie)

    #*************************************************************************
    # lien internet
    # Ce champ est traite de maniere particuliere (presentation sous forme
    # de lien) car nomme 'url'
    #*************************************************************************
    url = models.CharField( ParDefautModeleRecord.label_url, max_length=ParDefautModeleRecord.max_length_url )
    url.help_text = ParDefautModeleRecord.help_text_url
    url.blank = True # optionnel (reellement optionnel)

    #*************************************************************************
    # et autres champs du modele (cf bibliotheque de modeles record) :
    # statut/etat de validation du modele...
    #*************************************************************************

    #*************************************************************************
    #
    # Correspondance avec/relation a pkgs vle
    #
    #*************************************************************************

    #*************************************************************************
    # nom (relatif) du repertoire regroupant les paquets constituant le modele
    # (le paquet principal, les paquets de dependance).
    # nom relatif depuis 'get_conf_repertoire_racine_lots_pkgs'
    #*************************************************************************
    nom_repertoire_lot_pkgs = models.CharField( ParDefautModeleRecord.label_nom_repertoire_lot_pkgs, max_length=ParDefautModeleRecord.max_length_nom_repertoire_lot_pkgs )
    nom_repertoire_lot_pkgs.help_text = ParDefautModeleRecord.help_text_nom_repertoire_lot_pkgs
    nom_repertoire_lot_pkgs.blank = False # obligatoire (normalement obligatoire)

    #*************************************************************************
    # nom effectif du paquet sous pkgs (paquet principal du modele)
    #*************************************************************************
    nom_pkg = models.CharField( ParDefautModeleRecord.label_nom_pkg, max_length=ParDefautModeleRecord.max_length_nom_pkg )
    nom_pkg.help_text = ParDefautModeleRecord.help_text_nom_pkg
    nom_pkg.blank = False # obligatoire (normalement obligatoire)

    #*************************************************************************
    # nom (relatif) du repertoire contenant les fichiers de donnees de
    # simulation (valeur a donner par defaut : chemin du sous-repertoire
    # data du paquet principal).
    # nom relatif depuis 'get_conf_repertoire_racine_data'
    #*************************************************************************
    nom_repertoire_data = models.CharField( ParDefautModeleRecord.label_nom_repertoire_data, max_length=ParDefautModeleRecord.max_length_nom_repertoire_data )
    nom_repertoire_data.help_text = ParDefautModeleRecord.help_text_nom_repertoire_data
    nom_repertoire_data.blank = True # optionnel (reellement optionnel) 

    #*************************************************************************
    # 
    # Conditions de depot dans l'outil Web
    # 
    #*************************************************************************

    #*************************************************************************
    # accessibilite (modele prive/public)
	# mot de passe du paquet (cf acces prive)
    #*************************************************************************
    mot_de_passe = models.ForeignKey( MotDePasse )
    mot_de_passe.help_text = ParDefautModeleRecord.help_text_mot_de_passe
    mot_de_passe.null = True
    mot_de_passe.blank = False # obligatoire (mot_de_passe 'modele_public' pour un modele public) (normalement obligatoire)

    #*************************************************************************
    # caracterisques du modele ( oui / non )
    # une caracteristique pourra etre utilisee par exemple dans des
    # classifications (presentation des modeles par groupes/rubriques)
    #*************************************************************************

    #*************************************************************************
    # activation/desativation
    # le modele desactive est present dans la bd des modeles record mais
    # n'est pas propose par l'outil web record
    #*************************************************************************
    modele_active = models.CharField( ParDefautModeleRecord.label_modele_active, max_length=ParDefautModeleRecord.max_length_modele_active )
    modele_active.help_text = ParDefautModeleRecord.help_text_modele_active
    modele_active.blank = False # obligatoire (normalement obligatoire)

    # caracteristique1
    caracteristique1 = models.CharField( ParDefautModeleRecord.label_caracteristique1, max_length=ParDefautModeleRecord.max_length_caracteristique1 )
    caracteristique1.help_text = ParDefautModeleRecord.help_text_caracteristique1
    caracteristique1.blank = False # obligatoire (normalement obligatoire)

    # caracteristique2
    caracteristique2 = models.CharField( ParDefautModeleRecord.label_caracteristique2, max_length=ParDefautModeleRecord.max_length_caracteristique2 )
    caracteristique2.help_text = ParDefautModeleRecord.help_text_caracteristique2
    caracteristique2.blank = False # obligatoire (normalement obligatoire)

    # caracteristique3
    caracteristique3 = models.CharField( ParDefautModeleRecord.label_caracteristique3, max_length=ParDefautModeleRecord.max_length_caracteristique3 )
    caracteristique3.help_text = ParDefautModeleRecord.help_text_caracteristique3
    caracteristique3.blank = False # obligatoire (normalement obligatoire)

    # caracteristique4
    caracteristique4 = models.CharField( ParDefautModeleRecord.label_caracteristique4, max_length=ParDefautModeleRecord.max_length_caracteristique4 )
    caracteristique4.help_text = ParDefautModeleRecord.help_text_caracteristique4
    caracteristique4.blank = False # obligatoire (normalement obligatoire)

    # caracteristique5
    caracteristique5 = models.CharField( ParDefautModeleRecord.label_caracteristique5, max_length=ParDefautModeleRecord.max_length_caracteristique5 )
    caracteristique5.help_text = ParDefautModeleRecord.help_text_caracteristique5
    caracteristique5.blank = False # obligatoire (normalement obligatoire)

    # caracteristique6
    caracteristique6 = models.CharField( ParDefautModeleRecord.label_caracteristique6, max_length=ParDefautModeleRecord.max_length_caracteristique6 )
    caracteristique6.help_text = ParDefautModeleRecord.help_text_caracteristique6
    caracteristique6.blank = False # obligatoire (normalement obligatoire)

    # caracteristique7
    caracteristique7 = models.CharField( ParDefautModeleRecord.label_caracteristique7, max_length=ParDefautModeleRecord.max_length_caracteristique7 )
    caracteristique7.help_text = ParDefautModeleRecord.help_text_caracteristique7
    caracteristique7.blank = False # obligatoire (normalement obligatoire)

    # caracteristique8
    caracteristique8 = models.CharField( ParDefautModeleRecord.label_caracteristique8, max_length=ParDefautModeleRecord.max_length_caracteristique8 )
    caracteristique8.help_text = ParDefautModeleRecord.help_text_caracteristique8
    caracteristique8.blank = False # obligatoire (normalement obligatoire)

    # caracteristique9
    caracteristique9 = models.CharField( ParDefautModeleRecord.label_caracteristique9, max_length=ParDefautModeleRecord.max_length_caracteristique9 )
    caracteristique9.help_text = ParDefautModeleRecord.help_text_caracteristique9
    caracteristique9.blank = False # obligatoire (normalement obligatoire)

    # caracteristique10
    caracteristique10 = models.CharField( ParDefautModeleRecord.label_caracteristique10, max_length=ParDefautModeleRecord.max_length_caracteristique10 )
    caracteristique10.help_text = ParDefautModeleRecord.help_text_caracteristique10
    caracteristique10.blank = False # obligatoire (normalement obligatoire)

    # caracteristique11
    caracteristique11 = models.CharField( ParDefautModeleRecord.label_caracteristique11, max_length=ParDefautModeleRecord.max_length_caracteristique11 )
    caracteristique11.help_text = ParDefautModeleRecord.help_text_caracteristique11
    caracteristique11.blank = False # obligatoire (normalement obligatoire)

    # caracteristique12
    caracteristique12 = models.CharField( ParDefautModeleRecord.label_caracteristique12, max_length=ParDefautModeleRecord.max_length_caracteristique12 )
    caracteristique12.help_text = ParDefautModeleRecord.help_text_caracteristique12
    caracteristique12.blank = False # obligatoire (normalement obligatoire)

    # caracteristique13
    caracteristique13 = models.CharField( ParDefautModeleRecord.label_caracteristique13, max_length=ParDefautModeleRecord.max_length_caracteristique13 )
    caracteristique13.help_text = ParDefautModeleRecord.help_text_caracteristique13
    caracteristique13.blank = False # obligatoire (normalement obligatoire)

    # caracteristique14
    caracteristique14 = models.CharField( ParDefautModeleRecord.label_caracteristique14, max_length=ParDefautModeleRecord.max_length_caracteristique14 )
    caracteristique14.help_text = ParDefautModeleRecord.help_text_caracteristique14
    caracteristique14.blank = False # obligatoire (normalement obligatoire)

    # caracteristique15
    caracteristique15 = models.CharField( ParDefautModeleRecord.label_caracteristique15, max_length=ParDefautModeleRecord.max_length_caracteristique15 )
    caracteristique15.help_text = ParDefautModeleRecord.help_text_caracteristique15
    caracteristique15.blank = False # obligatoire (normalement obligatoire)

    # caracteristique16
    caracteristique16 = models.CharField( ParDefautModeleRecord.label_caracteristique16, max_length=ParDefautModeleRecord.max_length_caracteristique16 )
    caracteristique16.help_text = ParDefautModeleRecord.help_text_caracteristique16
    caracteristique16.blank = False # obligatoire (normalement obligatoire)

    #*************************************************************************
    #
    # Configuration/personnalisation dans le depot de l'outil Web
    #
    # Fichiers vpz de configuration : 
    # Au niveau de l'outil_web_record, il est attendu concernant le nom
    # (absolu) des 3 fichiers vpz de configuration de correspondre a des 
    # fichiers au format vpz.
    # Voir ModeleRecordAdminForm pour les autres contraintes imposees dans le
    # cadre de la saisie/enregistrement en bd de ces noms de fichiers.
    #
    #*************************************************************************

    # nom (relatif) du fichier vpz de configuration web propre a la conf web
    # nom relatif depuis 'get_conf_repertoire_racine_configuration'
    nom_fichier_config_web_conf = models.CharField( ParDefautModeleRecord.label_nom_fichier_config_web_conf, max_length=ParDefautModeleRecord.max_length_config )
    nom_fichier_config_web_conf.help_text = ParDefautModeleRecord.help_text_nom_fichier_config_web_conf
    nom_fichier_config_web_conf.blank = True # optionnel (reellement optionnel) 

    # nom (relatif) du fichier vpz de configuration web propre aux applis web
    # nom relatif depuis 'get_conf_repertoire_racine_configuration'
    nom_fichier_config_web_applis = models.CharField( ParDefautModeleRecord.label_nom_fichier_config_web_applis, max_length=ParDefautModeleRecord.max_length_config )
    nom_fichier_config_web_applis.help_text = ParDefautModeleRecord.help_text_nom_fichier_config_web_applis
    nom_fichier_config_web_applis.blank = True # optionnel (reellement optionnel)
    
    # nom (relatif) du fichier vpz de configuration propre au dictionnaire
    # nom relatif depuis 'get_conf_repertoire_racine_configuration'
    nom_fichier_config_dicos = models.CharField( ParDefautModeleRecord.label_nom_fichier_config_dicos, max_length=ParDefautModeleRecord.max_length_config )
    nom_fichier_config_dicos.help_text = ParDefautModeleRecord.help_text_nom_fichier_config_dicos
    nom_fichier_config_dicos.blank = True # optionnel (reellement optionnel)

    #*************************************************************************
    #
    # Responsables (individus)
    #
    #*************************************************************************

	# responsable du modele record
	# qui en est le correspondant pour administrateur(s),
	# qui en distribue le mot de passe aux personnes de son choix.
    responsable = models.ForeignKey(Individu, related_name=_(u"responsable") )
    responsable.help_text = ParDefautModeleRecord.help_text_responsable
    responsable.null = True
    responsable.blank = False # obligatoire (normalement obligatoire)

    # responsable scientifique
    responsable_scientifique = models.ForeignKey(Individu, related_name=_(u"responsable_scientifique") )
    responsable_scientifique.help_text = ParDefautModeleRecord.help_text_responsable_scientifique
    responsable_scientifique.null = True 
    responsable_scientifique.blank = False # obligatoire (normalement obligatoire)

    # responsable informatique
    responsable_informatique = models.ForeignKey(Individu, related_name=_(u"responsable_informatique") )
    responsable_informatique.help_text = ParDefautModeleRecord.help_text_responsable_informatique
    responsable_informatique.null = True 
    responsable_informatique.blank = False # obligatoire (normalement obligatoire)

    #*************************************************************************
    #
    # Livraison pour l'outil Web
    #
    #*************************************************************************

    #*************************************************************************
    # compte-rendu de reception/recette, rapport de livraison du modele dans
    # l'outil web record (verifications effectuees...)
    #*************************************************************************

    #texte_default = "... " + _(u"à renseigner") +  " ..." + "\n"
    cr_reception = models.TextField( ParDefautModeleRecord.label_cr_reception, max_length=ParDefautModeleRecord.max_length_cr_reception ) # , default=texte_default ) vide par defaut

    cr_reception.help_text = ParDefautModeleRecord.help_text_cr_reception
    #cr_reception.blank = False # obligatoire (normalement obligatoire)
    cr_reception.blank = True # optionnel (rendu optionnel pour faciliter saisie)

    #*************************************************************************
    # indique si le modele fonctionne sous linux ( ne sait pas / oui / non )
    #*************************************************************************
    fonctionne_sous_linux = models.CharField( ParDefautModeleRecord.label_fonctionne_sous_linux, max_length=ParDefautModeleRecord.max_length_fonctionne_sous_linux )
    fonctionne_sous_linux.help_text = ParDefautModeleRecord.help_text_fonctionne_sous_linux
    fonctionne_sous_linux.blank = False # obligatoire (normalement obligatoire)

    #*************************************************************************
    # indique si la version du modele livree/deposee est sans affichage/trace
    # ecran ( ne sait pas / oui / non )
    #*************************************************************************
    sans_trace_ecran = models.CharField( ParDefautModeleRecord.label_sans_trace_ecran, max_length=ParDefautModeleRecord.max_length_sans_trace_ecran )
    sans_trace_ecran.help_text = ParDefautModeleRecord.help_text_sans_trace_ecran
    sans_trace_ecran.blank = False # obligatoire (normalement obligatoire)

    #*************************************************************************
    # Informations d'installation du modele : procedure, mention des
    # librairies requises (par exemple GDAL pour Gengiscan, gfortran pour
    # NativeStics) ...
    #*************************************************************************
    #texte_default = "... " + "\n"
    #texte_default = texte_default + _(u"Librairies requises : ...") + "\n"
    pour_installation = models.TextField( ParDefautModeleRecord.label_pour_installation, max_length=ParDefautModeleRecord.max_length_pour_installation ) # , default=texte_default ) vide par defaut
    pour_installation.help_text = ParDefautModeleRecord.help_text_pour_installation
    #pour_installation.blank = False # obligatoire (normalement obligatoire)
    pour_installation.blank = True # optionnel (rendu optionnel pour faciliter saisie)

    #*************************************************************************
    # autorisation/accord de depot/exploitation du modele dans l'outil web
    # record
    #*************************************************************************
    accord_exploitation = models.CharField( ParDefautModeleRecord.label_accord_exploitation, max_length=ParDefautModeleRecord.max_length_accord_exploitation )
    accord_exploitation.help_text = ParDefautModeleRecord.help_text_accord_exploitation
    accord_exploitation.blank = False # obligatoire (normalement obligatoire)

    #*************************************************************************
	# version vle sous laquelle le paquet a ete receptionne
    #*************************************************************************
    version_vle = models.CharField( ParDefautModeleRecord.label_version_vle, max_length=ParDefautModeleRecord.max_length_version_vle )
    version_vle.help_text = ParDefautModeleRecord.help_text_version_vle
    version_vle.blank = False # obligatoire (normalement obligatoire)

    #*************************************************************************

    class Meta :
        verbose_name = _(u"Modèle")
        verbose_name_plural = _(u"Modèles")

        translate = ( 'nom', 'description', 'cr_reception', 'pour_installation' )

    def __unicode__(self):
        return 'modele %s (pkg %s)' % (self.nom, self.nom_pkg)

    ##..
    #*************************************************************************\n
    # isPublic determine si le ModeleRecord est public.
    #\n*************************************************************************
    def isPublic(self):
        return ( estMotDePassePublic(self.mot_de_passe) )

    ##..
    #*************************************************************************\n
    # checkPassword determine si mdp correspond ou non au mot de passe du
    # ModeleRecord. Retourne True si identite.
    #\n*************************************************************************
    def checkPassword(self, mdp):
        return ( self.mot_de_passe.check_password(mdp) )

    #*************************************************************************
    # Methodes relatives a nom_repertoire_data
    # Valeur/cas particulier du repertoire par defaut :
    # nom_repertoire_data vaut 'defaultValueNomRepertoireData' dans le cas de
    # demande du repertoire par defaut. Dans ce cas, le nom (absolu) de
    # repertoire correspondant est getNomAbsoluRepertoireDataParDefaut.
    # (voir certaines de ces methodes en dehors de la classe ModeleRecord)
    #*************************************************************************

    ## determine et retourne le nom (chemin absolu) du repertoire des donnees
    # de simulation par defaut. Il s'agit du repertoire 'data' du paquet
    # principal.
    # rq : il existe aussi (hors classe)
    # getNomAbsoluRepertoireDataParDefaut(nom_repertoire_lot_pkgs,nom_pkg)
    def getNomAbsoluRepertoireDataParDefaut( self ) :
        return getNomAbsoluRepertoireDataParDefaut( self.nom_repertoire_lot_pkgs, self.nom_pkg )

    ## retourne True si nom_repertoire_data correspond a demande du repertoire
    # par defaut
    def aNomRepertoireDataParDefaut(self) :
        return (self.nom_repertoire_data == defaultValueNomRepertoireData() )


    #*************************************************************************
    # Methodes determinant et retournant le nom absolu de repertoire/fichier
    # correspondant a un champ du type nom relatif de repertoire/fichier
    # Champs concernes :
    # nom_repertoire_lot_pkgs, nom_repertoire_data,
    # nom_fichier_config_web_conf, nom_fichier_config_web_applis,
    # nom_fichier_config_dicos.
    #*************************************************************************

    def getNomAbsoluRepertoireLotPkgs(self) :
        return getNomAbsoluRepertoireLotPkgs( self.nom_repertoire_lot_pkgs )

    def getNomAbsoluRepertoireData(self) :
        return getNomAbsoluRepertoireData( self.nom_repertoire_data )

    def getNomAbsoluFichierConfigWebConf(self) :
        return getNomAbsoluFichierConfigWebConf( self.nom_fichier_config_web_conf )

    def getNomAbsoluFichierConfigWebApplis(self) :
        return getNomAbsoluFichierConfigWebApplis( self.nom_fichier_config_web_applis )

    def getNomAbsoluFichierConfigDicos(self) :
        return getNomAbsoluFichierConfigDicos( self.nom_fichier_config_dicos )

    #*************************************************************************
    #
    # Methodes relatives aux caracterisques du modele ( oui / non )
    #
    #*************************************************************************

    ##..
    #*************************************************************************\n
    # isModeleActive determine si le ModeleRecord est active.
    #\n*************************************************************************
    def isModeleActive(self):
        return ( self.modele_active == choixVrai() )

    ##..
    #*************************************************************************\n
    # isCaracteristiquei (i=1..16) determine si le ModeleRecord presente la
    # caracteristiquei
    #\n*************************************************************************
    def isCaracteristique1(self) :
        return ( self.caracteristique1 == choixVrai() )
    def isCaracteristique2(self) :
        return ( self.caracteristique2 == choixVrai() )
    def isCaracteristique3(self) :
        return ( self.caracteristique3 == choixVrai() )
    def isCaracteristique4(self) :
        return ( self.caracteristique4 == choixVrai() )
    def isCaracteristique5(self) :
        return ( self.caracteristique5 == choixVrai() )
    def isCaracteristique6(self) :
        return ( self.caracteristique6 == choixVrai() )
    def isCaracteristique7(self) :
        return ( self.caracteristique7 == choixVrai() )
    def isCaracteristique8(self) :
        return ( self.caracteristique8 == choixVrai() )
    def isCaracteristique9(self) :
        return ( self.caracteristique9 == choixVrai() )
    def isCaracteristique10(self) :
        return ( self.caracteristique10 == choixVrai() )
    def isCaracteristique11(self) :
        return ( self.caracteristique11 == choixVrai() )
    def isCaracteristique12(self) :
        return ( self.caracteristique12 == choixVrai() )
    def isCaracteristique13(self) :
        return ( self.caracteristique13 == choixVrai() )
    def isCaracteristique14(self) :
        return ( self.caracteristique14 == choixVrai() )
    def isCaracteristique15(self) :
        return ( self.caracteristique15 == choixVrai() )
    def isCaracteristique16(self) :
        return ( self.caracteristique16 == choixVrai() )

    #*************************************************************************
    #
    # Methodes relatives a des informations ( ne sait pas / oui / non ) du
    # modele concernant sa livraison pour l'outil Web
    #
    #*************************************************************************

    #*************************************************************************
    # traitements relatifs a fonctionne_sous_linux
    #*************************************************************************
    def fonctionnementSousLinuxIndetermine(self):
        return ( self.fonctionne_sous_linux == choixIndetermine() )
    def fonctionnementSousLinuxOkOuIndetermine(self):
        return ( ( self.fonctionne_sous_linux == choixVrai() ) or self.fonctionnementSousLinuxIndetermine() )
    def fonctionnementSousLinuxNotOkOuIndetermine(self):
        return ( ( self.fonctionne_sous_linux == choixFaux() ) or self.fonctionnementSousLinuxIndetermine() )

    #*************************************************************************
    # traitements relatifs a sans_trace_ecran
    #*************************************************************************
    def sansTraceEcranIndetermine(self):
        return ( self.sans_trace_ecran == choixIndetermine() )
    def sansTraceEcranVraiOuIndetermine(self):
        return ( ( self.sans_trace_ecran == choixVrai() ) or self.sansTraceEcranIndetermine() )
    def sansTraceEcranFauxOuIndetermine(self):
        return ( ( self.sans_trace_ecran == choixFaux() ) or self.sansTraceEcranIndetermine() )

    ##..
    #*************************************************************************\n
    # aAccordExploitation determine si le ModeleRecord possede/a obtenu son
    # autorisation/accord pour depot/exploitation dans l'outil web record
    #\n*************************************************************************
    def aAccordExploitation(self):
        return ( self.accord_exploitation == choixVrai() )

#*****************************************************************************

