#-*- coding:utf-8 -*-

## @file record/bd_modeles_record/forms/forms.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File forms.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# Formulaires relatifs a l'utilisation des modeles record enregistres en bd
#
#*****************************************************************************

from record.bd_modeles_record.models import ParDefautModeleRecord
from record.bd_modeles_record.models import ModeleRecord, Individu, MotDePasse
from record.bd_modeles_record.models import estMotDePassePublic

from django import forms
from django.forms import ModelForm

from form_utils.forms import BetterModelForm # pour formulaire par categories

from django.utils.translation import ugettext as _

from transmeta import TransMeta


#*****************************************************************************
#
# Divers
#
#*****************************************************************************

##..
#*****************************************************************************\n
#
# Formulaire de saisie d'un mot de passe \n
# (a comparer à ModeleRecord.mot_de_passe.password,
#  cf ModeleRecord.checkPassword) \n
# (a comparer à MotDePasse.password)
#
#*****************************************************************************
class MotDePasseAskingForm(forms.Form):
    mdp_saisi = forms.CharField( label=_(u'Mot de passe')+' ', widget=forms.PasswordInput(render_value=False) )

    def get_valeur_saisie(self) :
        cleaned_data = super( MotDePasseAskingForm, self).clean()
        return cleaned_data.get('mdp_saisi')

#*****************************************************************************

