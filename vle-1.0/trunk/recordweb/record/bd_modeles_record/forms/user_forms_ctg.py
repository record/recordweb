#-*- coding:utf-8 -*-

## @file record/bd_modeles_record/forms/user_forms_ctg.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File user_forms_ctg.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# Formulaires d'utilisation des modeles record enregistres en bd
#
# Observation des modeles record (edition 'read only')
#
# Dans ces formulaires, les informations sont presentees dans des categories
#
#*****************************************************************************

from record.bd_modeles_record.models import ParDefautModeleRecord
from record.bd_modeles_record.models import ModeleRecord, Individu, MotDePasse
from record.bd_modeles_record.models import estMotDePassePublic

from django import forms
from django.forms import ModelForm

from form_utils.forms import BetterModelForm # pour formulaire par categories

from record.forms.commun_forms import setFieldsConfigurationReadOnlyA

from django.utils.translation import ugettext as _

from transmeta import TransMeta


#*****************************************************************************
#
# Formulaire d'utilisation de Individu
#
#*****************************************************************************

##..
#*****************************************************************************\n
# Classe mere commune relative a formulaire d'utilisation de Individu
# ou les informations sont presentees dans des categories \n
#*****************************************************************************
class IndividuFormCtg(BetterModelForm):

    def __init__(self, *args, **kwargs):

        super(IndividuFormCtg, self).__init__(*args, **kwargs)

        instance = getattr(self, 'instance', None)
        if instance :
            individu = instance
        else : 
            individu = None

        if individu is not None :
            self.fields['username'] = forms.CharField( initial=individu.username)
            self.fields['first_name'] = forms.CharField( initial=individu.first_name)
            self.fields['last_name'] = forms.CharField( initial=individu.last_name)
            self.fields['email'] = forms.CharField( initial=individu.email)
        else : # reaction a changer ?
            self.fields['username'] = forms.CharField( initial="")
            self.fields['first_name'] = forms.CharField( initial="")
            self.fields['last_name'] = forms.CharField( initial="")
            self.fields['email'] = forms.CharField( initial="")

        # configuration des champs
        setFieldsConfigurationReadOnlyA( self.fields )

    class Meta :
        model = Individu

##..
#*****************************************************************************\n
# Un formulaire pour informations de Individu
# presentees selon certaines categories \n
#*****************************************************************************
class IndividuFormCtgA(IndividuFormCtg):

    def __init__(self, *args, **kwargs):
        super(IndividuFormCtgA, self).__init__(*args, **kwargs)

    class Meta :

        texte_description = _(u"Un individu est une personne susceptible d\'endosser le rôle de responsable et/ou responsable scientifique et/ou responsable informatique par rapport à un ou plusieurs modèles")

        fieldsets = (

        ( _(u"INDIVIDU"), { 'description': texte_description,
                             'fields': (),
                             'legend': _(u"INDIVIDU"),
                             } ),

        ( None, { 'fields': ('username',
                             'first_name',
                             'last_name',
                             'email') } ),
        )

        row_attrs = {'first_name': {'first_name': 'background: blue'}}


##..
#*****************************************************************************\n
# Un formulaire pour informations de Individu
# presentees selon certaines categories \n
#*****************************************************************************
class IndividuFormCtgB(IndividuFormCtg):

    def __init__(self, *args, **kwargs):
        super(IndividuFormCtgB, self).__init__(*args, **kwargs)

    class Meta :

        fieldsets = (

        ( None, { 'fields': ('username',
                             'first_name',
                             'last_name',
                             'email') } ),
        )

        row_attrs = {'first_name': {'first_name': 'background: blue'}}

#*****************************************************************************
#
# Formulaire d'utilisation de ModeleRecord
#
#*****************************************************************************

##..
#*****************************************************************************\n
# Classe mere commune relative a formulaire d'utilisation de ModeleRecord
# ou les informations sont presentees dans des categories \n
#*****************************************************************************
class ModeleRecordFormCtg(BetterModelForm):

    def __init__(self, *args, **kwargs):

        super(ModeleRecordFormCtg, self).__init__(*args, **kwargs)

        instance = getattr(self, 'instance', None)
        if instance and instance.id:
            modele_record = instance
            self.modele_record_id = modele_record.id
        else : 
            modele_record = None
            self.modele_record_id = None

        #*********************************************************************
        # valeurs initiales
        #*********************************************************************
        if modele_record is not None :

            initial_nom = modele_record.nom
            initial_description = modele_record.description
            initial_url = modele_record.url
            initial_nom_repertoire_lot_pkgs = modele_record.nom_repertoire_lot_pkgs
            initial_nom_pkg = modele_record.nom_pkg
            initial_nom_repertoire_data = modele_record.nom_repertoire_data
            initial_nom_fichier_config_web_conf = modele_record.nom_fichier_config_web_conf
            initial_nom_fichier_config_web_applis = modele_record.nom_fichier_config_web_applis
            initial_nom_fichier_config_dicos = modele_record.nom_fichier_config_dicos
            initial_cr_reception = modele_record.cr_reception
            initial_fonctionne_sous_linux = modele_record.fonctionne_sous_linux
            initial_sans_trace_ecran = modele_record.sans_trace_ecran
            initial_pour_installation = modele_record.pour_installation
            initial_accord_exploitation = modele_record.accord_exploitation
            initial_version_vle = modele_record.version_vle
            initial_modele_active = modele_record.modele_active
            initial_caracteristique1 = modele_record.caracteristique1
            initial_caracteristique2 = modele_record.caracteristique2
            initial_caracteristique3 = modele_record.caracteristique3
            initial_caracteristique4 = modele_record.caracteristique4
            initial_caracteristique5 = modele_record.caracteristique5
            initial_caracteristique6 = modele_record.caracteristique6
            initial_caracteristique7 = modele_record.caracteristique7
            initial_caracteristique8 = modele_record.caracteristique8
            initial_caracteristique9 = modele_record.caracteristique9
            initial_caracteristique10 = modele_record.caracteristique10
            initial_caracteristique11 = modele_record.caracteristique11
            initial_caracteristique12 = modele_record.caracteristique12
            initial_caracteristique13 = modele_record.caracteristique13
            initial_caracteristique14 = modele_record.caracteristique14
            initial_caracteristique15 = modele_record.caracteristique15
            initial_caracteristique16 = modele_record.caracteristique16

            initial_type_acces= "" # par defaut
            if estMotDePassePublic( modele_record.mot_de_passe ) is not None :
                if estMotDePassePublic( modele_record.mot_de_passe ) :
                    initial_type_acces= _(u"accès public")
                else :
                    initial_type_acces = _(u"accès sous condition")

        else : # reaction a changer ?

            initial_nom = ""
            initial_description = ""
            initial_url = ""
            initial_nom_repertoire_lot_pkgs = ""
            initial_nom_pkg = ""
            initial_nom_repertoire_data = ""
            initial_nom_fichier_config_web_conf = ""
            initial_nom_fichier_config_web_applis = ""
            initial_nom_fichier_config_dicos = ""
            initial_cr_reception = ""
            initial_fonctionne_sous_linux = ""
            initial_sans_trace_ecran = ""
            initial_pour_installation = ""
            initial_accord_exploitation = ""
            initial_version_vle = ""
            initial_modele_active = ""
            initial_caracteristique1 = ""
            initial_caracteristique2 = ""
            initial_caracteristique3 = ""
            initial_caracteristique4 = ""
            initial_caracteristique5 = ""
            initial_caracteristique6 = ""
            initial_caracteristique7 = ""
            initial_caracteristique8 = ""
            initial_caracteristique9 = ""
            initial_caracteristique10 = ""
            initial_caracteristique11 = ""
            initial_caracteristique12 = ""
            initial_caracteristique13 = ""
            initial_caracteristique14 = ""
            initial_caracteristique15 = ""
            initial_caracteristique16 = ""
            initial_type_acces = ""

        #*********************************************************************

        # nom (translate)
        label_user = ParDefautModeleRecord.label_nom
        help_text = ParDefautModeleRecord.help_text_nom
        self.fields['nom'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_nom)

        # description (translate)
        label_user = ParDefautModeleRecord.label_description
        help_text = ParDefautModeleRecord.help_text_description
        max_length = ParDefautModeleRecord.max_length_description
        self.fields['description'] = forms.CharField( label=label_user,
                help_text=help_text,
                widget=forms.Textarea(attrs={'rows':4, 'cols':60}),
                initial=initial_description)

        # url
        label_user = ParDefautModeleRecord.label_url
        help_text = ParDefautModeleRecord.help_text_url
        self.fields['url'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_url)

        # nom_repertoire_lot_pkgs
        label_user = ParDefautModeleRecord.label_nom_repertoire_lot_pkgs
        help_text = ParDefautModeleRecord.help_text_nom_repertoire_lot_pkgs
        self.fields['nom_repertoire_lot_pkgs'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_nom_repertoire_lot_pkgs)

        # nom_pkg
        label_user = ParDefautModeleRecord.label_nom_pkg
        help_text = ParDefautModeleRecord.help_text_nom_pkg
        self.fields['nom_pkg'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_nom_pkg)

        # nom_repertoire_data
        label_user = ParDefautModeleRecord.label_nom_repertoire_data
        help_text = ParDefautModeleRecord.help_text_nom_repertoire_data
        self.fields['nom_repertoire_data'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_nom_repertoire_data)

        # nom_fichier_config_web_conf (optionnel)
        label_user = ParDefautModeleRecord.label_nom_fichier_config_web_conf
        help_text = ParDefautModeleRecord.help_text_nom_fichier_config_web_conf
        self.fields['nom_fichier_config_web_conf'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_nom_fichier_config_web_conf)

        # nom_fichier_config_web_applis (optionnel)
        label_user = ParDefautModeleRecord.label_nom_fichier_config_web_applis
        help_text = ParDefautModeleRecord.help_text_nom_fichier_config_web_applis
        self.fields['nom_fichier_config_web_applis'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_nom_fichier_config_web_applis)

        # nom_fichier_config_dicos (optionnel)
        label_user = ParDefautModeleRecord.label_nom_fichier_config_dicos
        help_text = ParDefautModeleRecord.help_text_nom_fichier_config_dicos
        self.fields['nom_fichier_config_dicos'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_nom_fichier_config_dicos)

        # self.formulaires et self.formulaires_names servent a traiter les
        # champs ne correspondant pas a un seul field mais plusieurs (en
        # l'occurence les responsables) : pour un field dont field.name fait
        # partie des formulaires_names, le formulaire est
        # formulaires[field.name] et nom pas field
        self.formulaires = {}
        self.formulaires_names = []

        # responsable (suite du traitement dans classe fille)
        label_user = ParDefautModeleRecord.label_responsable
        help_text = ParDefautModeleRecord.help_text_responsable
        self.fields['responsable'] = forms.CharField( label=label_user,
                help_text=help_text) # fictif
        self.formulaires['responsable'] = IndividuFormCtg() # transitoire
        self.formulaires_names.append('responsable')

        # responsable_scientifique (suite du traitement dans classe fille)
        label_user = ParDefautModeleRecord.label_responsable_scientifique
        help_text = ParDefautModeleRecord.help_text_responsable_scientifique
        self.fields['responsable_scientifique'] = forms.CharField( label=label_user,
                help_text=help_text) # fictif
        self.formulaires['responsable_scientifique'] = IndividuFormCtg() # transitoire
        self.formulaires_names.append( 'responsable_scientifique')

        # responsable_informatique (suite du traitement dans classe fille)
        label_user = ParDefautModeleRecord.label_responsable_informatique
        help_text = ParDefautModeleRecord.help_text_responsable_informatique
        self.fields['responsable_informatique'] = forms.CharField( label=label_user,
                help_text=help_text) # fictif
        self.formulaires['responsable_informatique'] = IndividuFormCtg() # transitoire
        self.formulaires_names.append( 'responsable_informatique')
    
        # cr_reception (translate)
        label_user = ParDefautModeleRecord.label_cr_reception
        help_text = ParDefautModeleRecord.help_text_cr_reception
        self.fields['cr_reception'] = forms.CharField( label=label_user,
                help_text=help_text,
                widget=forms.Textarea(attrs={'rows':4, 'cols':60}),
                initial=initial_cr_reception)

        # fonctionne_sous_linux
        label_user = ParDefautModeleRecord.label_fonctionne_sous_linux
        help_text = ParDefautModeleRecord.help_text_fonctionne_sous_linux
        self.fields['fonctionne_sous_linux'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_fonctionne_sous_linux)

        # sans_trace_ecran
        label_user = ParDefautModeleRecord.label_sans_trace_ecran
        help_text = ParDefautModeleRecord.help_text_sans_trace_ecran
        self.fields['sans_trace_ecran'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_sans_trace_ecran)

        # pour_installation (translate)
        label_user = ParDefautModeleRecord.label_pour_installation
        help_text = ParDefautModeleRecord.help_text_pour_installation
        self.fields['pour_installation'] = forms.CharField( label=label_user,
                help_text=help_text,
                widget=forms.Textarea(attrs={'rows':4, 'cols':60}),
                initial=initial_pour_installation)

        # accord_exploitation
        label_user = ParDefautModeleRecord.label_accord_exploitation
        help_text = ParDefautModeleRecord.help_text_accord_exploitation
        self.fields['accord_exploitation'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_accord_exploitation)
    
        # version_vle
        label_user = ParDefautModeleRecord.label_version_vle
        help_text = ParDefautModeleRecord.help_text_version_vle
        self.fields['version_vle'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_version_vle)
    
        # modele_active
        label_user = ParDefautModeleRecord.label_modele_active
        help_text = ParDefautModeleRecord.help_text_modele_active
        self.fields['modele_active'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_modele_active)
    
        # caracteristique1
        label_user = ParDefautModeleRecord.label_caracteristique1
        help_text = ParDefautModeleRecord.help_text_caracteristique1
        self.fields['caracteristique1'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_caracteristique1)
    
        # caracteristique2
        label_user = ParDefautModeleRecord.label_caracteristique2
        help_text = ParDefautModeleRecord.help_text_caracteristique2
        self.fields['caracteristique2'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_caracteristique2)

        # caracteristique3
        label_user = ParDefautModeleRecord.label_caracteristique3
        help_text = ParDefautModeleRecord.help_text_caracteristique3
        self.fields['caracteristique3'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_caracteristique3)
 
        # caracteristique4
        label_user = ParDefautModeleRecord.label_caracteristique4
        help_text = ParDefautModeleRecord.help_text_caracteristique4
        self.fields['caracteristique4'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_caracteristique4)
    
        # caracteristique5
        label_user = ParDefautModeleRecord.label_caracteristique5
        help_text = ParDefautModeleRecord.help_text_caracteristique5
        self.fields['caracteristique5'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_caracteristique5)
    
        # caracteristique6
        label_user = ParDefautModeleRecord.label_caracteristique6
        help_text = ParDefautModeleRecord.help_text_caracteristique6
        self.fields['caracteristique6'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_caracteristique6)
    
        # caracteristique7
        label_user = ParDefautModeleRecord.label_caracteristique7
        help_text = ParDefautModeleRecord.help_text_caracteristique7
        self.fields['caracteristique7'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_caracteristique7)
    
        # caracteristique8
        label_user = ParDefautModeleRecord.label_caracteristique8
        help_text = ParDefautModeleRecord.help_text_caracteristique8
        self.fields['caracteristique8'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_caracteristique8)

        # caracteristique9
        label_user = ParDefautModeleRecord.label_caracteristique9
        help_text = ParDefautModeleRecord.help_text_caracteristique9
        self.fields['caracteristique9'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_caracteristique9)

        # caracteristique10
        label_user = ParDefautModeleRecord.label_caracteristique10
        help_text = ParDefautModeleRecord.help_text_caracteristique10
        self.fields['caracteristique10'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_caracteristique10)

        # caracteristique11
        label_user = ParDefautModeleRecord.label_caracteristique11
        help_text = ParDefautModeleRecord.help_text_caracteristique11
        self.fields['caracteristique11'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_caracteristique11)
    
        # caracteristique12
        label_user = ParDefautModeleRecord.label_caracteristique12
        help_text = ParDefautModeleRecord.help_text_caracteristique12
        self.fields['caracteristique12'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_caracteristique12)
    
        # caracteristique13
        label_user = ParDefautModeleRecord.label_caracteristique13
        help_text = ParDefautModeleRecord.help_text_caracteristique13
        self.fields['caracteristique13'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_caracteristique13)

        # caracteristique14
        label_user = ParDefautModeleRecord.label_caracteristique14
        help_text = ParDefautModeleRecord.help_text_caracteristique14
        self.fields['caracteristique14'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_caracteristique14)

        # caracteristique15
        label_user = ParDefautModeleRecord.label_caracteristique15
        help_text = ParDefautModeleRecord.help_text_caracteristique15
        self.fields['caracteristique15'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_caracteristique15)

        # caracteristique16
        label_user = ParDefautModeleRecord.label_caracteristique16
        help_text = ParDefautModeleRecord.help_text_caracteristique16
        self.fields['caracteristique16'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_caracteristique16)
    
        # cas particulier de mot_de_passe
        # traduit/interprete dans champ nouveau type_acces
        label_user = _(u"Type d'accès")
        help_text = _(u"Tant que son type d'accès n'est pas renseigné, un modèle reste inaccessible (il n'a pas de mot de passe, même s'il est dit ailleurs en 'accès sous condition'). Lorsque son type d'accès est renseigné, un modèle en 'accès sous condition' est accessible une fois saisi son mot de passe (obtenu auprès de son responsable), tandis qu'un modèle en 'accès public' est accessible sans condition.")
        self.fields['type_acces'] = forms.CharField( label=label_user,
                help_text=help_text,
                initial=initial_type_acces)

        #*********************************************************************

        # configuration des champs
        setFieldsConfigurationReadOnlyA( self.fields )

    class Meta:
        model = ModeleRecord

##..
#*****************************************************************************\n
# Un formulaire pour informations de ModeleRecord
# presentees selon certaines categories \n
#*****************************************************************************
class ModeleRecordFormCtgA(ModeleRecordFormCtg):

    def __init__(self, *args, **kwargs):

        super(ModeleRecordFormCtgA, self).__init__(*args, **kwargs)

        # mise a jour self.formulaires
        instance = getattr(self, 'instance', None)
        if instance and instance.id:
            modele_record = instance
        else : 
            modele_record = None

        if  modele_record is not None :

            if modele_record.responsable is not None :
                self.formulaires['responsable'] = IndividuFormCtgA(instance=modele_record.responsable)
            else :
                self.formulaires['responsable'] = None
            if modele_record.responsable_scientifique is not None :
                self.formulaires['responsable_scientifique'] = IndividuFormCtgA(instance=modele_record.responsable_scientifique)
            else :
                self.formulaires['responsable_scientifique'] = None
            if modele_record.responsable_informatique is not None :
                self.formulaires['responsable_informatique'] = IndividuFormCtgA(instance=modele_record.responsable_informatique)
            else :
                self.formulaires['responsable_informatique'] = None
        else :
            self.formulaires['responsable'] = None
            self.formulaires['responsable_scientifique'] = None
            self.formulaires['responsable_informatique'] = None

    class Meta:
        texte_description = _(u"Tous les champs du modèle ne sont pas affichés.")

        fieldsets = (

        ( _(u"MODELE RECORD"), { 'description': texte_description,
                                 'fields': (),
                                 'legend': _(u"MODELE RECORD"),
                                 } ),

        ( None, { 'fields': ( 'nom',
                              'description',
                              'url',
                              'type_acces',
                              'cr_reception',) } ),

        ( _(u"RESPONSABLES"), { 'fields': ( 'responsable',
                              'responsable_scientifique',
                              'responsable_informatique',) } ),

        ( _(u"ASPECT VLE (PAQUETS...)"), { 'fields': ( 'nom_repertoire_lot_pkgs',
                              'nom_pkg',
                              'nom_repertoire_data',
                              'version_vle') } ),

        ( _(u"CONFIGURATION PERSONNALISATION (optionnel)"), { 'fields': ( 'nom_fichier_config_web_conf',
                              'nom_fichier_config_web_applis',
                              'nom_fichier_config_dicos') } ),
        )

        row_attrs = {'nom': {'style': 'background: #f00'}}

##..
#*****************************************************************************\n
# Un formulaire pour informations de ModeleRecord
# presentees selon certaines categories \n
#*****************************************************************************
class ModeleRecordFormCtgB(ModeleRecordFormCtg):

    def __init__(self, *args, **kwargs):

        super(ModeleRecordFormCtgB, self).__init__(*args, **kwargs)

        # mise a jour self.formulaires
        instance = getattr(self, 'instance', None)
        if instance and instance.id:
            modele_record = instance
        else : 
            modele_record = None

        if  modele_record is not None :

            if modele_record.responsable is not None :
                self.formulaires['responsable'] = IndividuFormCtgB(instance=modele_record.responsable)
            else :
                self.formulaires['responsable'] = None
            if modele_record.responsable_scientifique is not None :
                self.formulaires['responsable_scientifique'] = IndividuFormCtgB(instance=modele_record.responsable_scientifique)
            else :
                self.formulaires['responsable_scientifique'] = None
            if modele_record.responsable_informatique is not None :
                self.formulaires['responsable_informatique'] = IndividuFormCtgB(instance=modele_record.responsable_informatique)
            else :
                self.formulaires['responsable_informatique'] = None
        else :
            self.formulaires['responsable'] = None
            self.formulaires['responsable_scientifique'] = None
            self.formulaires['responsable_informatique'] = None

    class Meta:

        fieldsets = (

        #*********************************************************************
#       ( "ENTETE", {      'description': _(u"Les renseignements du modèle concernent d'une part son identité propre et d'autre part son exploitation au sein de l'outil Web."),
#                      'fields': (),
#                       #'legend': _(u""),
#                     } ),

        #*********************************************************************
        ( "IDENTITE", { 'description': "identité",

                        'fields': ( 'nom',
                                    'description',
                                    'url',
                                    'responsable',
                                    'responsable_scientifique',
                                    'responsable_informatique',
                                  ),

                        'legend': _(u"Identité "),
                      } ),

        #*********************************************************************
        ( "LIVRAISON_OUTIL_WEB", { 'description': _(u"Livraison pour l'outil Web"),

                        'fields': ( 'cr_reception',
                                    'fonctionne_sous_linux',
                                    'sans_trace_ecran',
                                    'pour_installation',
                                    'accord_exploitation',
                                  ),

                        'legend': _(u"Livraison pour l'outil Web"),
                      } ),

        #*********************************************************************
        ( "CONDITIONS_LIVRAISON_OUTIL_WEB", { 'description': _(u"Conditions de dépôt dans l'outil Web"),

                        'fields': ( 'type_acces',
                                    'modele_active', # presence/proposition dans l'outil 
                                    'caracteristique1',
                                    'caracteristique2',
                                    'caracteristique3',
                                    'caracteristique4',
                                    'caracteristique5',
                                    'caracteristique6',
                                    'caracteristique7',
                                    'caracteristique8',
                                    'caracteristique9',
                                    'caracteristique10',
                                    'caracteristique11',
                                    'caracteristique12',
                                    'caracteristique13',
                                    'caracteristique14',
                                    'caracteristique15',
                                    'caracteristique16',
                        ),

                        'legend': _(u"Conditions de dépôt dans l'outil Web"),
                      } ),

        #*********************************************************************
        ( "ASPECT_VLE", { 'description': _(u"Eléments d'identification du modèle dans le dépôt de modèles de l'outil Web Record : aspect VLE (paquets...)"),

                        'fields': ( 'nom_repertoire_lot_pkgs',
                                    'nom_pkg',
                                    'version_vle',
                        ),
                        'legend': _(u"Aspect VLE (paquets...) dans le dépôt de l'outil Web"),
                      } ),

        #*********************************************************************
        ( "REPERTOIRE_DATA", { 'description': _(u"Eléments d'identification du modèle dans le dépôt de modèles de l'outil Web Record : les fichiers de données"),

                        'fields': ( 'nom_repertoire_data',
                        ),

                        'legend': _(u"Fichiers de données dans le dépôt de l'outil Web"),
                      } ),

        #*********************************************************************
        ( "CONFIGURATION_PERSONNALISATION", { 'description': _(u"Eléments d'identification du modèle dans le dépôt de modèles de l'outil Web Record : la configuration/personnalisation (optionnel)"),

                        'fields': ( 'nom_fichier_config_web_conf',
                                    'nom_fichier_config_web_applis',
                                    'nom_fichier_config_dicos',
                        ),

                        'legend': _(u"Configuration/personnalisation dans le dépôt de l'outil Web"),
                      } ),

        )

        #*********************************************************************

        row_attrs = {'nom': {'style': 'background: #f00'}}

##..
#*****************************************************************************\n
#
# Formulaire pour informations completes de ModeleRecord 
# (montrees pour un modele public ou prive apres saisie de son mot de passe) \n
# Utilisation : dans la page de presentation d'un modele
#
#*****************************************************************************
class ModeleRecordFormComplet( ModeleRecordFormCtgB ) :
    pass

##..
#*****************************************************************************\n
#
# Formulaire pour informations partielles de ModeleRecord
# (montrees que le modele soit public ou prive) \n
# En l'etat actuel, pas moins d'informations dans ModeleRecordFormPartiel que
# dans ModeleRecordFormComplet \n
# Utilisation : dans la page de demande du mot de passe d'un modele
#
#*****************************************************************************
class ModeleRecordFormPartiel(ModeleRecordFormCtgB):
    pass


#*****************************************************************************

