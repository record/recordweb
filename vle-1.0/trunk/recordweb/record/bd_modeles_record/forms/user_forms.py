#-*- coding:utf-8 -*-

## @file record/bd_modeles_record/forms/user_forms.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File user_forms.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# Formulaires d'utilisation des modeles record enregistres en bd
#
# Observation des modeles record (edition 'read only')
#
#*****************************************************************************

from record.bd_modeles_record.models import ParDefautModeleRecord
from record.bd_modeles_record.models import ModeleRecord, Individu, MotDePasse
from record.bd_modeles_record.models import estMotDePassePublic

from django import forms
from django.forms import ModelForm

from form_utils.forms import BetterModelForm # pour formulaire par categories

from django.utils.translation import ugettext as _

from transmeta import TransMeta


#*****************************************************************************
#
# Formulaire d'utilisation de Individu
#
#*****************************************************************************

##..
#*****************************************************************************\n
#
# Classe mere commune \n
# (self.fields.keyOrder est defini/configure dans classe fille)
#
#*****************************************************************************
class IndividuForm(ModelForm):

    def __init__(self, *args, **kwargs):

        super(IndividuForm, self).__init__(*args, **kwargs)

        # tous les champs read-only et autre configuration
        for field in self.fields.values() :
            field.widget.attrs['readonly']='readonly'
            field.widget.attrs['style']= "background-color:white;" # "background-color:white; color:blue;"

    class Meta :
        model = Individu

##..
#*****************************************************************************\n
#
# Formulaire pour informations partielles de Individu
#
#*****************************************************************************
class IndividuFormPartiel(IndividuForm):

    def __init__(self, *args, **kwargs):
        super(IndividuFormPartiel, self).__init__(*args, **kwargs)

        self.fields.keyOrder = [ 
                                'first_name', 
                                'last_name', 
                                'email' 
                               ]
    class Meta :
        model = Individu

##..
#*****************************************************************************\n
#
# Formulaire pour informations completes de Individu
#
#*****************************************************************************
class IndividuFormComplet(IndividuForm):

    def __init__(self, *args, **kwargs):
        super(IndividuFormComplet, self).__init__(*args, **kwargs)

        self.fields.keyOrder = [ 
                                'first_name', 
                                'last_name', 
                                'email' 
                               ]
    class Meta :
        model = Individu

#*****************************************************************************
#
# Formulaire d'utilisation de ModeleRecord
#
#*****************************************************************************

##..
#*****************************************************************************\n
#
# Classe mere commune
#
# Liste des champs de ModeleRecordForm : 
# - 'nom', 'nom_en', 'nom_fr'
# - 'description', 'description_en', 'description_fr'
# - 'url'
# - 'nom_repertoire_lot_pkgs'
# - 'nom_pkg'
# - 'nom_repertoire_data'
# - 'mot_de_passe'
# - 'nom_fichier_config_web_conf'
# - 'nom_fichier_config_web_applis'
# - 'nom_fichier_config_dicos'
# - 'responsable'
# - 'responsable_scientifique'
# - 'responsable_informatique'
# - 'cr_reception', 'cr_reception_en', 'cr_reception_fr'
# - 'fonctionne_sous_linux'
# - 'sans_trace_ecran'
# - 'pour_installation', 'pour_installation_en', 'pour_installation_fr'
# - 'accord_exploitation'
# - 'version_vle'
# - 'modele_active'
# - 'caracteristique1' ... 'caracteristique16'
# - 'type_acces' (supplementaire par rapport a ModeleRecord)
#
# formulaires et formulaires_names servent a traiter les champs ne
# correspondant pas a un seul field mais plusieurs
#
# (self.fields.keyOrder est defini/configure dans classe fille)
# ancien a jeter (self.fields.keyOrder et exclude sont definis/configures
# dans classe fille)
#
#*****************************************************************************
class ModeleRecordForm(ModelForm):

    def __init__(self, *args, **kwargs):

        super(ModeleRecordForm, self).__init__(*args, **kwargs)

        instance = getattr(self, 'instance', None)
        if instance and instance.id:
            modele_record = instance
            self.modele_record_id = modele_record.id
        else : 
            modele_record = None
            self.modele_record_id = None

        # nom (translate)
        label_user = ParDefautModeleRecord.label_nom
        help_text = ParDefautModeleRecord.help_text_nom
        self.fields['nom'] = forms.CharField( label=label_user, help_text=help_text,
                initial=modele_record.nom)

        # description (translate)
        label_user = ParDefautModeleRecord.label_description
        help_text = ParDefautModeleRecord.help_text_description
        max_length = ParDefautModeleRecord.max_length_description
        self.fields['description'] = forms.CharField( label=label_user, help_text=help_text,
                widget=forms.Textarea(attrs={'rows':4, 'cols':60}),
                initial=modele_record.description)

        # self.formulaires et self.formulaires_names servent a traiter les
        # champs ne correspondant pas a un seul field mais plusieurs (en
        # l'occurence les responsables) : pour un field dont field.name fait
        # partie des formulaires_names, le formulaire est
        # formulaires[field.name] et nom pas field
        self.formulaires = {}
        self.formulaires_names = []

	    # responsable (suite du traitement dans classe fille)
        self.formulaires['responsable'] = IndividuForm() # transitoire
        self.formulaires_names.append('responsable')

        # responsable_scientifique (suite du traitement dans classe fille)
        self.formulaires['responsable_scientifique'] = IndividuForm() # transitoire
        self.formulaires_names.append( 'responsable_scientifique')

        # responsable_informatique (suite du traitement dans classe fille)
        self.formulaires['responsable_informatique'] = IndividuForm() # transitoire
        self.formulaires_names.append( 'responsable_informatique')
    
        # cr_reception (translate)
        label_user = ParDefautModeleRecord.label_cr_reception
        help_text = ParDefautModeleRecord.help_text_cr_reception
        self.fields['cr_reception'] = forms.CharField( label=label_user, help_text=help_text,
                widget=forms.Textarea(attrs={'rows':4, 'cols':60}),
                initial=modele_record.cr_reception)

        # pour_installation (translate)
        label_user = ParDefautModeleRecord.label_pour_installation
        help_text = ParDefautModeleRecord.help_text_pour_installation
        self.fields['pour_installation'] = forms.CharField( label=label_user, help_text=help_text,
                widget=forms.Textarea(attrs={'rows':4, 'cols':60}),
                initial=modele_record.pour_installation)

        # cas particulier de mot_de_passe
        # traduit/interprete dans champ nouveau type_acces
        label_user = _(u"Type d'accès")
        help_text = _(u"Un modèle en 'accès sous condition' est accessible une fois saisi son mot de passe (obtenu auprès de son responsable). Un modèle en 'accès public' est accessible sans condition.")
        if estMotDePassePublic( modele_record.mot_de_passe ) :
            valeur = _(u"accès public")
        else :
            valeur = _(u"accès sous condition")
        self.fields['type_acces'] = forms.CharField( label=label_user, help_text=help_text,
                initial=valeur)

        # tous les champs read-only et autre configuration
        for field in self.fields.values() :
            field.widget.attrs['readonly']= True # 'readonly'
            ###field.widget.attrs['size']= 80
            field.widget.attrs['style']= "background-color:white;" # "background-color:white; color:blue;"

    class Meta:
        model = ModeleRecord

        #*********************************************************************
        # les champs montres sont determines/choisis dans self.fields.keyOrder
        # (defini/configure dans classe fille)
        # en amont duquel 'exclude' impose des restrictions/interdictions
        #*********************************************************************

        # tous les 'translate' sont caches, le mot_de_passe est cache
        exclude = ( 'nom_fr', 'nom_en',
                    'description_fr', 'description_en',
                    'cr_reception_fr', 'cr_reception_en',
                    'pour_installation_fr', 'pour_installation_en',
                    'mot_de_passe' )

##..
#*****************************************************************************\n
#
# Formulaire de caracteristiques principales du ModeleRecord
#
#*****************************************************************************
class ModeleRecordFormCaracteristiques(ModeleRecordForm):

    def __init__(self, *args, **kwargs):

        super(ModeleRecordFormCaracteristiques, self).__init__(*args, **kwargs)

        #*********************************************************************
        # determination/choix des champs montres (dans l'ordre d'apparition)
        # parmi les champs de ModeleRecordFormCaracteristiques, ie de
        # ModeleRecordForm (voir liste des champs dans ModeleRecordForm)
        # voir 'exclude' au sujet de restrictions/interdictions en amont
        #*********************************************************************
        self.fields.keyOrder = [
            'nom_pkg',
            'type_acces',
            ]

        # effacement des help_text de tous les champs
        for field in self.fields.values() :
            field.help_text = ""

##..
#*****************************************************************************\n
#
# Formulaire pour informations resumees de ModeleRecord
# (montrees que le modele soit public ou prive) \n
# Utilisation : dans la page de presentation de tous les modeles
#
#*****************************************************************************
class ModeleRecordFormResume(ModeleRecordForm):

    def __init__(self, *args, **kwargs):

        super(ModeleRecordFormResume, self).__init__(*args, **kwargs)

        # mise a jour self.formulaires
        instance = getattr(self, 'instance', None)
        if instance and instance.id:
            modele_record = instance
        else : 
            modele_record = None
        self.formulaires['responsable'] = IndividuFormPartiel(instance=modele_record.responsable)
        self.formulaires['responsable_scientifique'] = IndividuFormPartiel(instance=modele_record.responsable_scientifique)
        self.formulaires['responsable_informatique'] = IndividuFormPartiel(instance=modele_record.responsable_informatique)

        # nouveau champ formulaire_caracteristiques correspondant a
        # regroupement de caracteristiques principales de ModeleRecord,
        # et son formulaire associe
        label_user = _(u"Autres caractéristiques")
        help_text = _(u"Caractéristiques principales du modèle")
        self.fields['formulaire_caracteristiques'] = forms.CharField(label=label_user, help_text=help_text) # transitoire
        self.formulaires['formulaire_caracteristiques'] = ModeleRecordFormCaracteristiques(instance=modele_record)
        self.formulaires_names.append('formulaire_caracteristiques')

        #*********************************************************************
        # determination/choix des champs montres (dans l'ordre d'apparition)
        # parmi les champs de ModeleRecordFormPartiel, ie champ
        # formulaire_caracteristiques + ceux de ModeleRecordForm
        # (voir liste des champs dans ModeleRecordForm)
        # voir 'exclude' au sujet de restrictions/interdictions en amont
        #*********************************************************************
        # champs a de/commenter selon souhait de montrer ou non
        self.fields.keyOrder = [
            'nom', #'nom_en', #'nom_fr',
            'description', #'description_en', #'description_fr',
            'url',
            #'nom_repertoire_lot_pkgs',
            #'nom_pkg',
            #'nom_repertoire_data',
            #'mot_de_passe', n'est jamais montre
            #'nom_fichier_config_web_conf',
            #'nom_fichier_config_web_applis',
            #'nom_fichier_config_dicos',
            #'responsable',
            #'responsable_scientifique',
            #'responsable_informatique',
            #'cr_reception', #'cr_reception_en', #'cr_reception_fr',
            #'fonctionne_sous_linux'
            #'sans_trace_ecran'
            #'pour_installation', #'pour_installation_en', #'pour_installation_fr'
            #'accord_exploitation'
            #'version_vle',
            #'modele_active'
            #'caracteristique1' ... 'caracteristique16'
            #'type_acces',
            'formulaire_caracteristiques',
            ]


##..
#*****************************************************************************\n
#
# Formulaire pour informations partielles de ModeleRecord
# (montrees que le modele soit public ou prive) \n
# En l'etat actuel, pas moins d'informations dans ModeleRecordFormPartiel que
# dans ModeleRecordFormComplet \n
# Utilisation : dans la page de demande du mot de passe d'un modele
#
#*****************************************************************************
class ModeleRecordFormPartiel(ModeleRecordForm):

    def __init__(self, *args, **kwargs):

        super(ModeleRecordFormPartiel, self).__init__(*args, **kwargs)

        # mise a jour self.formulaires
        instance = getattr(self, 'instance', None)
        if instance and instance.id:
            modele_record = instance
        else : 
            modele_record = None
        self.formulaires['responsable'] = IndividuFormPartiel(instance=modele_record.responsable)
        self.formulaires['responsable_scientifique'] = IndividuFormPartiel(instance=modele_record.responsable_scientifique)
        self.formulaires['responsable_informatique'] = IndividuFormPartiel(instance=modele_record.responsable_informatique)

        #*********************************************************************
        # determination/choix des champs montres (dans l'ordre d'apparition)
        # parmi les champs de ModeleRecordFormComplet, ie ceux de
        # ModeleRecordForm (voir liste des champs dans ModeleRecordForm)
        # voir 'exclude' au sujet de restrictions/interdictions en amont
        #*********************************************************************
        # champs a de/commenter selon souhait de montrer ou non
        self.fields.keyOrder = [
            'nom', #'nom_en', #'nom_fr',
            'description', #'description_en', #'description_fr',
            'url',
            'nom_pkg',
            'type_acces',
            'nom_repertoire_lot_pkgs',
            'nom_repertoire_data',
            #'mot_de_passe', n'est jamais montre
            'nom_fichier_config_web_conf',
            'nom_fichier_config_web_applis',
            'nom_fichier_config_dicos',
            'responsable',
            'responsable_scientifique',
            'responsable_informatique',
            'cr_reception', #'cr_reception_en', #'cr_reception_fr',
            'fonctionne_sous_linux',
            'sans_trace_ecran',
            'pour_installation', #'pour_installation_en', #'pour_installation_fr'
            'accord_exploitation',
            'version_vle',
            'modele_active',
            'caracteristique1','caracteristique2','caracteristique3','caracteristique4','caracteristique5','caracteristique6','caracteristique7','caracteristique8','caracteristique9','caracteristique10','caracteristique11','caracteristique12','caracteristique13','caracteristique14','caracteristique15','caracteristique16',
            ]

##..
#*****************************************************************************\n
#
# Formulaire pour informations completes de ModeleRecord 
# (montrees pour un modele public ou prive apres saisie de son mot de passe) \n
# Utilisation : dans la page de presentation d'un modele
#
#*****************************************************************************
class ModeleRecordFormComplet(ModeleRecordForm):

    def __init__(self, *args, **kwargs):

        super(ModeleRecordFormComplet, self).__init__(*args, **kwargs)

        # mise a jour self.formulaires
        instance = getattr(self, 'instance', None)
        if instance and instance.id:
            modele_record = instance
        else : 
            modele_record = None
        self.formulaires['responsable'] = IndividuFormComplet(instance=modele_record.responsable)
        self.formulaires['responsable_scientifique'] = IndividuFormComplet(instance=modele_record.responsable_scientifique)
        self.formulaires['responsable_informatique'] = IndividuFormComplet(instance=modele_record.responsable_informatique)

        #*********************************************************************
        # determination/choix des champs montres (dans l'ordre d'apparition)
        # parmi les champs de ModeleRecordFormComplet, ie ceux de
        # ModeleRecordForm (voir liste des champs dans ModeleRecordForm)
        # voir 'exclude' au sujet de restrictions/interdictions en amont
        #*********************************************************************
        # champs a de/commenter selon souhait de montrer ou non
        self.fields.keyOrder = [
            'nom', #'nom_en', #'nom_fr',
            'description', #'description_en', #'description_fr',
            'url',
            'nom_pkg',
            'type_acces',
            'nom_repertoire_lot_pkgs',
            'nom_repertoire_data',
            #'mot_de_passe', n'est jamais montre
            'nom_fichier_config_web_conf',
            'nom_fichier_config_web_applis',
            'nom_fichier_config_dicos',
            'responsable',
            'responsable_scientifique',
            'responsable_informatique',
            'cr_reception', #'cr_reception_en', #'cr_reception_fr',
            'fonctionne_sous_linux',
            'sans_trace_ecran',
            'pour_installation', #'pour_installation_en', #'pour_installation_fr'
            'accord_exploitation',
            'version_vle',
            'modele_active',
            'caracteristique1','caracteristique2','caracteristique3','caracteristique4','caracteristique5','caracteristique6','caracteristique7','caracteristique8','caracteristique9','caracteristique10','caracteristique11','caracteristique12','caracteristique13','caracteristique14','caracteristique15','caracteristique16',
            ]

#*****************************************************************************

