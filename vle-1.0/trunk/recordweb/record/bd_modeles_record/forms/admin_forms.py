#-*- coding:utf-8 -*-

## @file record/bd_modeles_record/forms/admin_forms.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File admin_forms.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# Formulaires d'administration des modeles record enregistres en bd
#
# Saisie (creation...) des modeles record (edition 'read write')
#
#*****************************************************************************

from record.bd_modeles_record.models import ParDefautModeleRecord
from record.bd_modeles_record.models import ModeleRecord, MotDePasse, Individu
from record.bd_modeles_record.models import get_conf_repertoire_racine_configuration
from record.bd_modeles_record.models import get_conf_repertoire_racine_lots_pkgs
from record.bd_modeles_record.models import get_conf_repertoire_racine_data
from record.bd_modeles_record.models import getNomAbsoluRepertoireLotPkgs
from record.bd_modeles_record.models import getNomAbsoluRepertoireData
from record.bd_modeles_record.models import getNomAbsoluFichierConfigWebConf
from record.bd_modeles_record.models import getNomAbsoluFichierConfigWebApplis
from record.bd_modeles_record.models import getNomAbsoluFichierConfigDicos
from record.bd_modeles_record.models import defaultValueNomRepertoireData, getNomAbsoluRepertoireDataParDefaut
from record.bd_modeles_record.models import choixVrai, choixFaux, choixIndetermine

from django.db import models

from django import forms

import os.path
from record.utils.dirs_and_files import getListeRepertoiresNonCachesRep
from record.utils.dirs_and_files import getListeRepertoiresLargesNonCachesRep
from record.utils.dirs_and_files import verificationFichierOk, verificationLargeRepertoireOk

from record.bd_modeles_record.configs.conf_bd_modeles_record import CONF_bd_modeles_record

from django.utils.translation import ugettext as _

#*****************************************************************************
#
# Des constantes
#
#*****************************************************************************

## Valeur 'vide' d'une liste de choix
choix_vide = ("","---------")

liste_choix_non_oui = [ ( choixFaux(), "non" ),
                        ( choixVrai(), "oui" ) ]
liste_choix_nesaitpas_non_oui = [ ( choixIndetermine(), "?" ),
                                  ( choixFaux(), "non" ),
                                  ( choixVrai(), "oui" ) ]

#*****************************************************************************
# Methodes relatives a la conf
# qui servent dans des criteres de recherche de repertoires/fichiers qui
# seront proposes dans des listes menus
#*****************************************************************************

def get_conf_repertoire_config_web_conf() :
    return CONF_bd_modeles_record.repertoire_config_web_conf
def get_conf_repertoire_config_web_applis() :
    return CONF_bd_modeles_record.repertoire_config_web_applis
def get_conf_repertoire_config_dicos() :
    return CONF_bd_modeles_record.repertoire_config_dicos

#*****************************************************************************
# FilePathFieldConfig... :
#
# Classes utilitaires pour les noms des fichiers vpz de configuration
# (nom_fichier_config_web_conf, nom_fichier_config_web_applis
# et nom_fichier_config_dicos) de ModeleRecord
#
#*****************************************************************************

## FilePathField pour des noms de fichiers de configuration (partie commune)
class FilePathFieldConfig( models.FilePathField ) :

    ## determine/construit et retourne liste_choix qui est la liste des paires
    # (nom relatif,nom relatif) associees aux fichiers vpz de configuration
    # (chemin relatif par rapport a self.path).
    #
    # Traitement (permissif) cas d'erreur : une erreur sur un fichier est non
    # bloquante, reaction : le fichier en question est ignore + un message
    # ecran d'avertissement.
    #
    def get_liste_choix( self ) :

        debutmsg = "AVERTISSEMENT !!! FilePathFieldConfig::get_liste_choix : "

        # recuperation de choices : paires (nom_absolu,nom_depuis_path)
        defaults = { 'path': self.path,
                     'match': self.match,
                     'recursive': self.recursive,
                     'form_class': forms.FilePathField, }
        obj= super(models.FilePathField, self).formfield(**defaults)
        choices = obj.choices
        # liste_choix : paires (nom_relatif,nom_relatif)
        liste_choix = []
        for (nom_absolu,nom_depuis_path) in choices :
            nom_relatif = nom_depuis_path[1:] # suppression debut '/'
            try :
                nom_texte = u"fichier" + " " + nom_relatif
            except Exception, e :
                diag = "echec : " + str(e) + " => " + nom_relatif + " ignored"
                print ""
                print debutmsg, diag
                print ""
                # nom_relatif ignored
            else :
                liste_choix.append( (nom_relatif,nom_texte) )
        return liste_choix

    ## etant donne un nom relatif de fichier (nom relatif par rapport a
    # self.path) correspondant a choice=(nom_depuis_path,nom_depuis_path),
    # la methode estSousRepertoire determine si le fichier en question se
    # situe ou non directement sous un repertoire nomme nom_repertoire
    def estSousRepertoire( self, nom_repertoire, choice ) :
        cr = False # par defaut
        (nom_depuis_path,nom_depuis_path) = choice
        nom_absolu = os.path.join( self.path, nom_depuis_path )
        decoupage = nom_absolu.split("/")
        if len(decoupage) >= 2 :
            le_repertoire = decoupage[-2]
            if le_repertoire == nom_repertoire :
                cr = True
        return cr

## FilePathField pour des noms de fichiers de configuration web
# propres a la conf web
class FilePathFieldConfigWebConf( FilePathFieldConfig ) :

    ## determine/construit et retourne liste_choix qui est la liste des paires
    # (nom,nom) avec nom : nom relatif fichier vpz de configuration
    # filtree relativement a configuration web propre a la conf web
    def get_liste_choix_noms_relatifs_fichiers( self ) : 

        choices = self.get_liste_choix()

        # ne garder que ceux situes sous repertoire repertoire_config_web_conf
        # ie dont nom absolu est de la forme */repertoire_config_web_conf/*.*
        # (le filtre par rapport a l'extension a deja eu lieu)
        liste_choix = [ choix_vide ]
        for choice in choices:
            if self.estSousRepertoire( get_conf_repertoire_config_web_conf(), choice ) :
                liste_choix.append( choice )
        #print "---------------------------------------"
        #print " CONFIG : ", liste_choix
        #print "---------------------------------------"
        return liste_choix

## FilePathField pour des noms de fichiers de configuration web
# propres aux applis web
class FilePathFieldConfigWebApplis( FilePathFieldConfig ) :

    ## determine/construit et retourne liste_choix qui est la liste des paires
    # (nom,nom) avec nom : nom relatif fichier vpz de configuration
    # filtree relativement a configuration web propre aux applis web
    def get_liste_choix_noms_relatifs_fichiers( self ) : 

        choices = self.get_liste_choix()

        # ne garder que ceux situes sous repertoire repertoire_config_web_applis
        # ie dont nom absolu est de la forme */repertoire_config_web_applis/*.*
        # (le filtre par rapport a l'extension a deja eu lieu)
        liste_choix = [ choix_vide ]
        for choice in choices:
            if self.estSousRepertoire( get_conf_repertoire_config_web_applis(), choice ) :
                liste_choix.append( choice )
        return liste_choix

## FilePathField pour des noms de fichiers de configuration
# propres aux dictionnaires
class FilePathFieldConfigDicos( FilePathFieldConfig ) :

    ## determine/construit et retourne liste_choix qui est la liste des paires
    # (nom,nom) avec nom : nom relatif fichier vpz de configuration
    # filtree relativement a configuration propre aux dictionnaires
    def get_liste_choix_noms_relatifs_fichiers( self ) : 

        choices = self.get_liste_choix()

        # ne garder que ceux situes sous repertoire repertoire_config_dicos
        # ie dont nom absolu est de la forme */repertoire_config_dicos/*.*
        # (le filtre par rapport a l'extension a deja eu lieu)
        liste_choix = [ choix_vide ]
        for choice in choices:
            if self.estSousRepertoire( get_conf_repertoire_config_dicos(), choice ) :
                liste_choix.append( choice )
        return liste_choix

#*****************************************************************************
#
# Methodes relatives aux nom_repertoire_lot_pkgs et nom_pkg de ModeleRecord
#
#*****************************************************************************

##..
#*****************************************************************************\n
#
# get_liste_choix_noms_relatifs_repertoires_lots_pkgs
# determine/construit et retourne liste_choix qui est la liste des paires
# correspondant aux noms relatifs de repertoire de lot de paquets vle
# (nom relatif depuis repertoire_racine_lots_pkgs). \n
# Les repertoires sont recherches sous
# repertoire_racine_lots_pkgs / version_vle (pour chaque version_vle de la
# liste_versions_vle )
#
#*****************************************************************************
def get_liste_choix_noms_relatifs_repertoires_lots_pkgs( repertoire_racine_lots_pkgs, liste_versions_vle ) :

    liste_choix = []

    for version_vle in liste_versions_vle :

        # liste les repertoires (non caches) situes sous repertoire_racine_lots_pkgs / version_vle
        repertoire_version_vle = os.path.join( repertoire_racine_lots_pkgs, version_vle )
        liste_noms_lot_pkgs = getListeRepertoiresNonCachesRep( repertoire_version_vle )

        # liste_choix : paires (nom_relatif,nom_texte)
        sep = ",  "
        for nom_lot_pkgs in liste_noms_lot_pkgs :
            nom_texte = u"version" + " " + version_vle + sep + u"lot" + " " + nom_lot_pkgs
            nom_relatif = os.path.join( version_vle, nom_lot_pkgs )
            liste_choix.append( (nom_relatif,nom_texte) )

    return liste_choix

##..
#*****************************************************************************\n
#
# get_liste_choix_noms_pkg
# determine/construit et retourne liste_choix qui est la liste des paires
# correspondant aux noms de paquet vle. \n
# Les paquets vle sont recherches sous
# repertoire_racine_lots_pkgs / version_vle / lot_pkg (pour toutes versions
# de liste_versions_vle et tous lots de paquets vle trouves dessous)
#
#*****************************************************************************
def get_liste_choix_noms_pkg( repertoire_racine_lots_pkgs, liste_versions_vle ) :

    liste_choix = []

    for version_vle in liste_versions_vle :

        # liste les repertoires (non caches) situes sous repertoire_racine_lots_pkgs / version_vle
        repertoire_version_vle = os.path.join( repertoire_racine_lots_pkgs, version_vle )
        liste_noms_lot_pkgs = getListeRepertoiresNonCachesRep( repertoire_version_vle )

        for nom_lot_pkgs in liste_noms_lot_pkgs :

            repertoire_lot_pkgs = os.path.join( repertoire_version_vle, nom_lot_pkgs )
            # liste les repertoires (non caches) situes sous repertoire_lot_pkgs
            liste_noms_pkg = getListeRepertoiresNonCachesRep( repertoire_lot_pkgs )

            for nom_pkg in liste_noms_pkg :

                # liste_choix : paires (nom_pkg,nom_texte)
                sep = ",  "
                nom_texte = u"version" + " " + version_vle + sep + u"lot" + " " + nom_lot_pkgs + sep + u"paquet" + " " + nom_pkg
                liste_choix.append( (nom_pkg,nom_texte) )

    #print "---------------------------------------"
    #print " NOMS_PKG : ", liste_choix
    #print "---------------------------------------"
    return liste_choix

#*****************************************************************************
#
# Methodes relatives aux repertoires des fichiers de donnees de simulation
# ie a nom_repertoire_data de ModeleRecord
#
#*****************************************************************************

##..
#*****************************************************************************\n
#
# Methodes relatives a la conf
# qui servent dans des criteres de recherche de repertoires/fichiers qui
# seront proposes dans des listes menus
#
#*****************************************************************************
def get_conf_repertoire_data_base() :
    return CONF_bd_modeles_record.repertoire_data_base

##..
#*****************************************************************************\n
#
# get_liste_choix_noms_relatifs_repertoires_datas
# determine/construit et retourne liste_choix qui est la liste des paires
# correspondant aux noms relatifs de repertoire de donnees de simulation de
# paquets vle (nom relatif depuis repertoire_racine_data). \n
# Les repertoires sont recherches sous repertoire_racine_data, avec un nom
# contenant repertoire_data_base.
#
#*****************************************************************************
def get_liste_choix_noms_relatifs_repertoires_datas( repertoire_racine_data, repertoire_data_base ) :

    # recherche sous repertoire_racine / repertoire_relatif
    def ajouter( liste_noms_relatifs, repertoire_racine, nom_base, repertoire_relatif="" ) :

        if repertoire_relatif == "" :
            repertoire_absolu = repertoire_racine
        else :
            repertoire_absolu = os.path.join( repertoire_racine, repertoire_relatif )
        liste_noms = getListeRepertoiresLargesNonCachesRep( repertoire_absolu )
        for nom in liste_noms : # candidat
            nom_relatif = os.path.join( repertoire_relatif, nom )
            if nom_base in nom : # elu
                liste_noms_relatifs.append( nom_relatif )
            ajouter( liste_noms_relatifs, repertoire_racine, nom_base, nom_relatif ) # recursivite

    # liste_noms_relatifs_data (chemin relatif par rapport a repertoire_racine_data)
    liste_noms_relatifs_data = []
    ajouter( liste_noms_relatifs_data, repertoire_racine_data, repertoire_data_base )

    # liste_choix : paires (nom_relatif,nom_texte)
    liste_choix = []
    for nom_relatif_data in liste_noms_relatifs_data :
        nom_texte = u"répertoire" + " " + nom_relatif_data
        liste_choix.append( (nom_relatif_data,nom_texte) )

    #print "---------------------------------------"
    #print " DATAS : ", liste_choix
    #print "---------------------------------------"
    return liste_choix

##..
#*****************************************************************************\n
#
# Formulaire d'administration des modeles record
#
# Saisie (creation...) des modeles record (edition 'read write')
#
# Note : la saisie normalement obligatoire de certains champs d'un
# ModeleRecord est en fait rendue optionnelle afin de faciliter
# l'enregistrement des informations en bd.
#
#*****************************************************************************
class ModeleRecordAdminForm(forms.ModelForm):

	# nom (translate)

    # description (translate)

    # url

    # pour nom_repertoire_lot_pkgs et nom_pkg
    path_lots_pkgs = get_conf_repertoire_racine_lots_pkgs()
    liste_versions_vle = CONF_bd_modeles_record.liste_versions_vle

    # nom_repertoire_lot_pkgs
    # (nom relatif a partir du repertoire racine path_lots_pkgs).
    # Les repertoires presentes/proposes a l'usr se situent sous un sous
    # repertoire liste_versions_vle du repertoire racine path_lots_pkgs.

    label_admin = ParDefautModeleRecord.label_nom_repertoire_lot_pkgs
    help_text = ParDefautModeleRecord.help_text_nom_repertoire_lot_pkgs + " " + _(u"Seuls sont proposés les répertoires situés sous le répertoire") + " " + path_lots_pkgs + ", " + _(u"plus exactement sous un de ses sous-répertoires de version.")

    liste_choix_lots_pkgs = get_liste_choix_noms_relatifs_repertoires_lots_pkgs( path_lots_pkgs, liste_versions_vle )
    liste_choix = [ choix_vide ]
    for v in liste_choix_lots_pkgs :
        liste_choix.append( v )
    #nom_repertoire_lot_pkgs = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix ) # saisie normalement obligatoire
    nom_repertoire_lot_pkgs = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix, required=False ) # saisie rendue optionnelle pour faciliter l'enregistrement des informations en bd

    # nom_pkg
    label_admin = ParDefautModeleRecord.label_nom_pkg
    help_text = ParDefautModeleRecord.help_text_nom_pkg

    liste_choix_noms_pkg = get_liste_choix_noms_pkg( path_lots_pkgs, liste_versions_vle )
    liste_choix = [ choix_vide ]
    for v in liste_choix_noms_pkg :
        liste_choix.append( v )
    nom_pkg = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix ) # saisie normalement obligatoire
    nom_pkg = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix, required=False ) # saisie rendue optionnelle pour faciliter l'enregistrement des informations en bd

    # pour nom_repertoire_data
    path_data = get_conf_repertoire_racine_data()
    data_base = get_conf_repertoire_data_base()

    #*************************************************************************
    # nom_repertoire_data
    # (nom relatif a partir du repertoire racine path_data).
    # Vaut 'defaultValueNomRepertoireData' si 'repertoire par defaut' choisi.
    # Les repertoires presentes/proposes a l'usr se situent sous le repertoire
    # racine path_data, et repondent a d'autres criteres (cf data_base).
    #*************************************************************************

    label_admin = ParDefautModeleRecord.label_nom_repertoire_data
    help_text = ParDefautModeleRecord.help_text_nom_repertoire_data + " " + _(u"Seuls sont proposés les répertoires situés sous le répertoire") + " " + path_data + " (" + _(u"plus exactement sous toute son arborescence") + ") " + _(u"et dont le nom contient") + " '" + data_base + "', " + _(u"ou bien le répertoire par défaut (sous-répertoire 'data' du paquet principal du modèle)") + "."

    liste_choix_data = get_liste_choix_noms_relatifs_repertoires_datas( path_data, data_base )
    liste_choix = [ ( defaultValueNomRepertoireData(), _(u"répertoire par défaut (sous-répertoire 'data' du paquet principal)") ) ]
    for v in liste_choix_data :
        liste_choix.append( v )
    nom_repertoire_data = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix, required=False )

    # mot_de_passe
    label_admin = ParDefautModeleRecord.label_mot_de_passe
    help_text = ParDefautModeleRecord.help_text_mot_de_passe + _(u" Associer à un modèle un des mots de passe existants ou en créer un nouveau")
    #mot_de_passe = forms.ModelChoiceField(label=label_admin, help_text=help_text, queryset=MotDePasse.objects.all()) # saisie normalement obligatoire
    mot_de_passe = forms.ModelChoiceField(label=label_admin, help_text=help_text, queryset=MotDePasse.objects.all(), required=False ) # saisie rendue optionnelle pour faciliter l'enregistrement des informations en bd

    # modele_active
    label_admin = ParDefautModeleRecord.label_modele_active
    help_text = ParDefautModeleRecord.help_text_modele_active
    liste_choix = liste_choix_non_oui
    modele_active = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix )

    # caracteristique1
    label_admin = ParDefautModeleRecord.label_caracteristique1
    help_text = ParDefautModeleRecord.help_text_caracteristique1
    liste_choix = liste_choix_non_oui
    caracteristique1 = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix )

    # caracteristique2
    label_admin = ParDefautModeleRecord.label_caracteristique2
    help_text = ParDefautModeleRecord.help_text_caracteristique2
    liste_choix = liste_choix_non_oui
    caracteristique2 = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix )

    # caracteristique3
    label_admin = ParDefautModeleRecord.label_caracteristique3
    help_text = ParDefautModeleRecord.help_text_caracteristique3
    liste_choix = liste_choix_non_oui
    caracteristique3 = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix )

    # caracteristique4
    label_admin = ParDefautModeleRecord.label_caracteristique4
    help_text = ParDefautModeleRecord.help_text_caracteristique4
    liste_choix = liste_choix_non_oui
    caracteristique4 = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix )

    # caracteristique5
    label_admin = ParDefautModeleRecord.label_caracteristique5
    help_text = ParDefautModeleRecord.help_text_caracteristique5
    liste_choix = liste_choix_non_oui
    caracteristique5 = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix )

    # caracteristique6
    label_admin = ParDefautModeleRecord.label_caracteristique6
    help_text = ParDefautModeleRecord.help_text_caracteristique6
    liste_choix = liste_choix_non_oui
    caracteristique6 = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix )

    # caracteristique7
    label_admin = ParDefautModeleRecord.label_caracteristique7
    help_text = ParDefautModeleRecord.help_text_caracteristique7
    liste_choix = liste_choix_non_oui
    caracteristique7 = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix )

    # caracteristique8
    label_admin = ParDefautModeleRecord.label_caracteristique8
    help_text = ParDefautModeleRecord.help_text_caracteristique8
    liste_choix = liste_choix_non_oui
    caracteristique8 = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix )

    # caracteristique9
    label_admin = ParDefautModeleRecord.label_caracteristique9
    help_text = ParDefautModeleRecord.help_text_caracteristique9
    liste_choix = liste_choix_non_oui
    caracteristique9 = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix )

    # caracteristique10
    label_admin = ParDefautModeleRecord.label_caracteristique10
    help_text = ParDefautModeleRecord.help_text_caracteristique10
    liste_choix = liste_choix_non_oui
    caracteristique10 = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix )

    # caracteristique11
    label_admin = ParDefautModeleRecord.label_caracteristique11
    help_text = ParDefautModeleRecord.help_text_caracteristique11
    liste_choix = liste_choix_non_oui
    caracteristique11 = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix )

    # caracteristique12
    label_admin = ParDefautModeleRecord.label_caracteristique12
    help_text = ParDefautModeleRecord.help_text_caracteristique12
    liste_choix = liste_choix_non_oui
    caracteristique12 = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix )

    # caracteristique13
    label_admin = ParDefautModeleRecord.label_caracteristique13
    help_text = ParDefautModeleRecord.help_text_caracteristique13
    liste_choix = liste_choix_non_oui
    caracteristique13 = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix )

    # caracteristique14
    label_admin = ParDefautModeleRecord.label_caracteristique14
    help_text = ParDefautModeleRecord.help_text_caracteristique14
    liste_choix = liste_choix_non_oui
    caracteristique14 = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix )

    # caracteristique15
    label_admin = ParDefautModeleRecord.label_caracteristique15
    help_text = ParDefautModeleRecord.help_text_caracteristique15
    liste_choix = liste_choix_non_oui
    caracteristique15 = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix )

    # caracteristique16
    label_admin = ParDefautModeleRecord.label_caracteristique16
    help_text = ParDefautModeleRecord.help_text_caracteristique16
    liste_choix = liste_choix_non_oui
    caracteristique16 = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix )

    #*************************************************************************
    # Fichiers vpz de configuration : 
    # En l'etat actuel des choses, au niveau de l'outil_web_record, la seule
    # syntaxe imposee pour le nom des 3 fichiers vpz de configuration est
    # l'extension extension_config.
    # Les fichiers presentes/proposes a l'usr se situent sous le repertoire 
    # racine path_config et repondent a d'autres criteres (cf
    # get_conf_repertoire_config_web_conf()...)
    #*************************************************************************

    max_length_config = ParDefautModeleRecord.max_length_config

    path_config = get_conf_repertoire_racine_configuration()
    extension_config = "vpz"
    match_config = ".*\." + extension_config + "$"

    # nom_fichier_config_web_conf (optionnel)
    # (nom relatif a partir du repertoire racine path_config)
    label_admin = ParDefautModeleRecord.label_nom_fichier_config_web_conf
    help_text= ParDefautModeleRecord.help_text_nom_fichier_config_web_conf + _(u" Seuls sont proposés les fichiers situés sous un répertoire") + " " + get_conf_repertoire_config_web_conf() + " " + _(u"de") + " " + path_config + "."
    file_path_field = FilePathFieldConfigWebConf( max_length=max_length_config, path=path_config, match=match_config, recursive=True )

    liste_choix = file_path_field.get_liste_choix_noms_relatifs_fichiers() 

    nom_fichier_config_web_conf = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix, required=False ) # saisie optionnelle (reellement optionnelle)

    # nom_fichier_config_web_applis (optionnel)
    # (nom relatif a partir du repertoire racine path_config)
    label_admin = ParDefautModeleRecord.label_nom_fichier_config_web_applis
    help_text= ParDefautModeleRecord.help_text_nom_fichier_config_web_applis + _(u" Seuls sont proposés les fichiers situés sous un répertoire") + " " + get_conf_repertoire_config_web_applis() + " " + _(u"de") + " " + path_config + "."
    file_path_field = FilePathFieldConfigWebApplis( max_length=max_length_config, path=path_config, match=match_config, recursive=True )
    liste_choix = file_path_field.get_liste_choix_noms_relatifs_fichiers() 
    nom_fichier_config_web_applis = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix, required=False ) # saisie optionnelle (reellement optionnelle)

    # nom_fichier_config_dicos (optionnel)
    # (nom relatif a partir du repertoire racine path_config)
    label_admin = ParDefautModeleRecord.label_nom_fichier_config_dicos
    help_text= ParDefautModeleRecord.help_text_nom_fichier_config_dicos + _(u" Seuls sont proposés les fichiers situés sous un répertoire") + " " + get_conf_repertoire_config_dicos() + " "+ _(u"de")+ " " + path_config + "."
    file_path_field = FilePathFieldConfigDicos( max_length=max_length_config, path=path_config, match=match_config, recursive=True )
    liste_choix = file_path_field.get_liste_choix_noms_relatifs_fichiers() 
    nom_fichier_config_dicos = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix, required=False ) # saisie optionnelle (reellement optionnelle)

	# responsable
    label_admin = ParDefautModeleRecord.label_responsable
    help_text = ParDefautModeleRecord.help_text_responsable + _(u" Associer à un modèle un des individus existants ou en créer un nouveau")
    #responsable = forms.ModelChoiceField(label=label_admin, help_text=help_text, queryset=Individu.objects.all() ) # saisie normalement obligatoire
    responsable = forms.ModelChoiceField(label=label_admin, help_text=help_text, queryset=Individu.objects.all(), required=False ) # saisie rendue optionnelle pour faciliter l'enregistrement des informations en bd

    # responsable_scientifique
    label_admin = ParDefautModeleRecord.label_responsable_scientifique
    help_text = ParDefautModeleRecord.help_text_responsable_scientifique + _(u" Associer à un modèle un des individus existants ou en créer un nouveau")
    #responsable_scientifique = forms.ModelChoiceField(label=label_admin, help_text=help_text, queryset=Individu.objects.all()) # saisie normalement obligatoire
    responsable_scientifique = forms.ModelChoiceField(label=label_admin, help_text=help_text, queryset=Individu.objects.all(), required=False ) # saisie rendue optionnelle pour faciliter l'enregistrement des informations en bd

    # responsable_informatique
    label_admin = ParDefautModeleRecord.label_responsable_informatique
    help_text = ParDefautModeleRecord.help_text_responsable_informatique + _(u" Associer à un modèle un des individus existants ou en créer un nouveau")
    #responsable_informatique = forms.ModelChoiceField(label=label_admin, help_text=help_text, queryset=Individu.objects.all()) # saisie normalement obligatoire
    responsable_informatique = forms.ModelChoiceField(label=label_admin, help_text=help_text, queryset=Individu.objects.all(), required=False ) # saisie rendue optionnelle pour faciliter l'enregistrement des informations en bd

    # cr_reception (translate)

    # fonctionne_sous_linux
    label_admin = ParDefautModeleRecord.label_fonctionne_sous_linux
    help_text = ParDefautModeleRecord.help_text_fonctionne_sous_linux
    liste_choix = liste_choix_nesaitpas_non_oui
    fonctionne_sous_linux = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix )

    # sans_trace_ecran
    label_admin = ParDefautModeleRecord.label_sans_trace_ecran
    help_text = ParDefautModeleRecord.help_text_sans_trace_ecran
    liste_choix = liste_choix_nesaitpas_non_oui
    sans_trace_ecran = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix )

    # pour_installation (translate)

    # accord_exploitation
    label_admin = ParDefautModeleRecord.label_accord_exploitation
    help_text = ParDefautModeleRecord.help_text_accord_exploitation
    liste_choix = liste_choix_non_oui
    accord_exploitation = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix )

    # version_vle
    label_admin = ParDefautModeleRecord.label_version_vle
    help_text = ParDefautModeleRecord.help_text_version_vle
    liste_choix = [ choix_vide ]
    for v in CONF_bd_modeles_record.liste_versions_vle :
        liste_choix.append( (v,v) )
    #version_vle = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix ) # saisie normalement obligatoire
    version_vle = forms.ChoiceField( label=label_admin, help_text=help_text, choices=liste_choix, required=False ) # saisie rendue optionnelle pour faciliter l'enregistrement des informations en bd

    class Meta:
        model = ModeleRecord

    ##..
    #*************************************************************************\n
    #
    # Methode de verification du ModeleRecord saisi
    #
    #*************************************************************************
    def clean(self):

        def valeur( cleaned_data, key ) :
            v = None # par defaut
            if key in cleaned_data :
                v = cleaned_data.get( key ) # lecture
            return v

        def isNotNoneAuSensLarge( v ) :
            # la valeur "" est consideree comme None
            return ( ( v is not None ) and ( v != "" ) )

        cleaned_data = super(ModeleRecordAdminForm, self).clean()

        #nom = valeur( cleaned_data, 'nom' )
        #description = valeur( cleaned_data, 'description' )
        #url = valeur( cleaned_data, 'url' )
        nom_repertoire_lot_pkgs = valeur( cleaned_data, 'nom_repertoire_lot_pkgs' )
        nom_pkg = valeur( cleaned_data, 'nom_pkg' )
        nom_repertoire_data = valeur( cleaned_data, 'nom_repertoire_data' )
        #mot_de_passe = valeur( cleaned_data, 'mot_de_passe' )
        modele_active = valeur( cleaned_data, 'modele_active' )
        caracteristique1 = valeur( cleaned_data, 'caracteristique1' )
        caracteristique2 = valeur( cleaned_data, 'caracteristique2' )
        caracteristique3 = valeur( cleaned_data, 'caracteristique3' )
        caracteristique4 = valeur( cleaned_data, 'caracteristique4' )
        caracteristique5 = valeur( cleaned_data, 'caracteristique5' )
        caracteristique6 = valeur( cleaned_data, 'caracteristique6' )
        caracteristique7 = valeur( cleaned_data, 'caracteristique7' )
        caracteristique8 = valeur( cleaned_data, 'caracteristique8' )
        caracteristique9 = valeur( cleaned_data, 'caracteristique9' )
        caracteristique10 = valeur( cleaned_data, 'caracteristique10' )
        caracteristique11 = valeur( cleaned_data, 'caracteristique11' )
        caracteristique12 = valeur( cleaned_data, 'caracteristique12' )
        caracteristique13 = valeur( cleaned_data, 'caracteristique13' )
        caracteristique14 = valeur( cleaned_data, 'caracteristique14' )
        caracteristique15 = valeur( cleaned_data, 'caracteristique15' )
        caracteristique16 = valeur( cleaned_data, 'caracteristique16' )
        nom_fichier_config_web_conf = valeur( cleaned_data, 'nom_fichier_config_web_conf' )
        nom_fichier_config_web_applis = valeur( cleaned_data, 'nom_fichier_config_web_applis' )
        nom_fichier_config_dicos = valeur( cleaned_data, 'nom_fichier_config_dicos' )
        #responsable = valeur( cleaned_data, 'responsable' )
        #responsable_scientifique = valeur( cleaned_data, 'responsable_scientifique' )
        #responsable_informatique = valeur( cleaned_data, 'responsable_informatique' )
        #cr_reception = valeur( cleaned_data, 'cr_reception' )
        fonctionne_sous_linux = valeur( cleaned_data, 'fonctionne_sous_linux' )
        sans_trace_ecran = valeur( cleaned_data, 'sans_trace_ecran' )
        #pour_installation = valeur( cleaned_data, 'pour_installation' )
        accord_exploitation = valeur( cleaned_data, 'accord_exploitation' )
        version_vle = valeur( cleaned_data, 'version_vle' )

        # inutilise
        #instance = getattr(self, 'instance', None)
        #if instance and instance.id : modele_record = instance
        #else : modele_record = None

        # chemins absolus
        nom_absolu_repertoire_lot_pkgs = None
        nom_absolu_repertoire_data = None
        nom_absolu_fichier_config_web_conf = None
        nom_absolu_fichier_config_web_applis = None
        nom_absolu_fichier_config_dicos = None
        if isNotNoneAuSensLarge(nom_repertoire_lot_pkgs) :
            nom_absolu_repertoire_lot_pkgs = getNomAbsoluRepertoireLotPkgs( nom_repertoire_lot_pkgs ) 
        if isNotNoneAuSensLarge(nom_repertoire_data) :
            nom_absolu_repertoire_data = getNomAbsoluRepertoireData( nom_repertoire_data ) 
        if isNotNoneAuSensLarge(nom_fichier_config_web_conf) :
            nom_absolu_fichier_config_web_conf = getNomAbsoluFichierConfigWebConf( nom_fichier_config_web_conf ) 
        if isNotNoneAuSensLarge(nom_fichier_config_web_applis) :
            nom_absolu_fichier_config_web_applis = getNomAbsoluFichierConfigWebApplis( nom_fichier_config_web_applis ) 
        if isNotNoneAuSensLarge(nom_fichier_config_dicos) :
            nom_absolu_fichier_config_dicos = getNomAbsoluFichierConfigDicos( nom_fichier_config_dicos ) 

        # pour l'instant pas de verification sur url

        # verification de version_vle par rapport a nom_repertoire_lot_pkgs
        if isNotNoneAuSensLarge(version_vle) and isNotNoneAuSensLarge(nom_repertoire_lot_pkgs) :
            if version_vle not in nom_repertoire_lot_pkgs : 
                msg = _(u"incompatibilité entre") + " version_vle (" + version_vle + ") " + _(u"et") + " nom_repertoire_lot_pkgs (chemin relatif " + nom_repertoire_lot_pkgs + ")"
                raise forms.ValidationError( msg )

        # pour chacun de nom_fichier_config_web_conf,
        # nom_fichier_config_web_applis, nom_fichier_config_dicos 
        # verifier que le fichier existe (s'il a ete saisi)

        nom_fichier = nom_fichier_config_web_conf
        nom_absolu_fichier = nom_absolu_fichier_config_web_conf
        if isNotNoneAuSensLarge(nom_fichier) :
            if nom_fichier != "" : # nom_fichier (facultatif) a bien ete saisi
                if not verificationFichierOk( nom_absolu_fichier ) :
                    msg = _(u"problème de fichier de configuration") + " : "
                    msg = msg + _(u"le fichier") + " " + nom_absolu_fichier + " " + _(u"n'existe pas")
                    raise forms.ValidationError( msg )
        nom_fichier = nom_fichier_config_web_applis
        nom_absolu_fichier = nom_absolu_fichier_config_web_applis
        if isNotNoneAuSensLarge(nom_fichier) :
            if nom_fichier != "" : # nom_fichier (facultatif) a bien ete saisi
                if not verificationFichierOk( nom_absolu_fichier ) :
                    msg = _(u"problème de fichier de configuration") + " : "
                    msg = msg + _(u"le fichier") + " " + nom_absolu_fichier + " " + _(u"n'existe pas")
                    raise forms.ValidationError( msg )
        nom_fichier = nom_fichier_config_dicos 
        nom_absolu_fichier = nom_absolu_fichier_config_dicos 
        if isNotNoneAuSensLarge(nom_fichier) :
            if nom_fichier != "" : # nom_fichier (facultatif) a bien ete saisi
                if not verificationFichierOk( nom_absolu_fichier ) :
                    msg = _(u"problème de fichier de configuration") + " : "
                    msg = msg + _(u"le fichier") + " " + nom_absolu_fichier + " " + _(u"n'existe pas")
                    raise forms.ValidationError( msg )

        # verifier que le repertoire nom_repertoire_lot_pkgs existe
        if isNotNoneAuSensLarge(nom_repertoire_lot_pkgs) :
            if not verificationLargeRepertoireOk( nom_absolu_repertoire_lot_pkgs ) :
                msg = _(u"le répertoire") + " " + nom_absolu_repertoire_lot_pkgs + " " + _(u"n'existe pas")
                raise forms.ValidationError( msg )

        # verifier que le repertoire nom_pkg existe sous le repertoire
        # nom_repertoire_lot_pkgs 
        if nom_absolu_repertoire_lot_pkgs is not None and isNotNoneAuSensLarge(nom_pkg) : 
            nom_absolu_repertoire_pkg = os.path.join( nom_absolu_repertoire_lot_pkgs, nom_pkg ) 
            if not verificationLargeRepertoireOk( nom_absolu_repertoire_pkg ) :
                msg = _(u"incompatibilité entre") + " nom_pkg (" + nom_pkg + ") " + _(u"et") + " nom_repertoire_lot_pkgs (chemin relatif " + nom_repertoire_lot_pkgs + ")" + " : " 
                msg = msg + _(u"il n'existe pas de répertoire ") + nom_pkg + " " + _(u"sous le répertoire ") + nom_absolu_repertoire_lot_pkgs 
                raise forms.ValidationError( msg )

        # verifier que le repertoire nom_repertoire_data (ou le repertoire
        # par defaut si 'repertoire par defaut' choisi) existe
        if isNotNoneAuSensLarge(nom_repertoire_data) :

            if nom_repertoire_data == defaultValueNomRepertoireData() :
            # correspond au choix du repertoire par defaut

                if isNotNoneAuSensLarge(nom_repertoire_lot_pkgs) and isNotNoneAuSensLarge(nom_pkg) :
                    nom_absolu_repertoire_data_default = getNomAbsoluRepertoireDataParDefaut( nom_repertoire_lot_pkgs, nom_pkg )
                    if not verificationLargeRepertoireOk( nom_absolu_repertoire_data_default ) :
                        msg = _(u"problème de répertoire par défaut des fichiers de données") + " : "
                        msg = msg + _(u"le répertoire par défaut (sous-répertoire 'data' du paquet principal)") + " " + nom_absolu_repertoire_data_default + " " + _(u"n'existe pas")
                        raise forms.ValidationError( msg )
            else :
                if not verificationLargeRepertoireOk( nom_absolu_repertoire_data ) :
                    msg = _(u"problème de répertoire des fichiers de données") + " : "
                    msg = msg + _(u"le répertoire") + " " + nom_absolu_repertoire_data + " " + _(u"n'existe pas")
                    raise forms.ValidationError( msg )

        return cleaned_data

#*****************************************************************************

