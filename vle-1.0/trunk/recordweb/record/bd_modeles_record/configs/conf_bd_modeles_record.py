#-*- coding:utf-8 -*-

## @file record/bd_modeles_record/conf_bd_modeles_record.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File conf_bd_modeles_record.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# CONFIGURATION
#
#*****************************************************************************

from django.conf import settings

#*****************************************************************************
#
# Contexte de fonctionnement/exploitation de l'application bd_modeles_record
#
#*****************************************************************************

#*****************************************************************************
# Traitement de ACTIONS
#
# La liste ACTIONS determine les actions a proposer a l'utilisateur pour un
# modele record : ACTIONS = [ action ] avec 
# action = { 'type':type, 'label':nom du bouton dans le menu, },
#            'url':pour la requete }
#
# La liste ACTIONS est exploitee au travers de menu_actions et les_actions.
#
# Une action sans 'type' et 'label' est ignoree.
# type : 'informations_completes', 'informations_partielles',
#        'redirection', 'redirection_modele_record_id'
# sont les valeurs/types actuellement traitees par bd_modeles_record.views
# (un_modele_record).
# url : optionnel (si absent lors de sa lecture, est force a None).
#
#*****************************************************************************

## ActionSubmit contient les informations pour construire dans template
# <input type="submit" name="action" value=value />
class ActionSubmit( object ) :
    def __init__(self, value ):
        self.name = "action"
        self.value = value

## Construction (a partir de la CONF) de
# menu_actions : pour menu proposition,
# les_actions : pour action suivante
def construire_actions() :

    actions = settings.ACTIONS

    menu_actions = []
    les_actions = {}

    for d in actions :
        if isinstance(d,dict) :
            if 'label' in d.keys() and 'type' in d.keys() : # 'url' optionnel
            # prise en compte de d
                menu_actions.append( ActionSubmit( value=d['label'] ) )
                if 'url' in d.keys() :
                    u = d['url']
                else :
                    u = None
                les_actions[ d['label'] ] = { 'type':d['type'], 'url':u }

    return ( menu_actions, les_actions )

##..
#*****************************************************************************\n
# CONF_bd_modeles_record
#
# Classe de configuration du contexte de fonctionnement/exploitation de
# l'application bd_modeles_record
#
#*****************************************************************************
class CONF_bd_modeles_record(object) :

    #*************************************************************************
    #
    #                       ASPECT ADMINISTRATION
    #
    #*************************************************************************

    #*************************************************************************
    # Les versions vle gerees/prises en compte
    #*************************************************************************

    liste_versions_vle = settings.VERSIONS_VLE

    #*************************************************************************
    # (Lots des) paquets vle des modeles record
    #*************************************************************************

    # Repertoire racine sous lequel est cherche l'ensemble des lots des
    # paquets vle des modeles record
    repertoire_racine_lots_pkgs = settings.LOTS_PKGS_PATH

    #*************************************************************************
    # Personnalisation (configuration web, dictionnaires)
    #*************************************************************************

    # Repertoire racine sous lequel est cherche l'ensemble des fichiers vpz
    # de configuration
    repertoire_racine_configuration = settings.CONFIGURATION_PATH

    # Noms des repertoires susceptibles de contenir des fichiers vpz de
    # configuration web propres a la conf web
    repertoire_config_web_conf="web"

    # Noms des repertoires susceptibles de contenir des fichiers vpz de
    # configuration web propres aux applis web
    repertoire_config_web_applis="web"

    # Noms des repertoires susceptibles de contenir des fichiers vpz de
    # configuration propres aux dictionnaires
    repertoire_config_dicos="meta"

    #*************************************************************************
    # Repertoires des fichiers de donnees de simulation des modeles record
    #*************************************************************************

    # Repertoire racine sous lequel est cherche l'ensemble des repertoires de
    # donnees de simulation
    repertoire_racine_data = settings.DATA_PATH

    # Base des noms de repertoires susceptibles d'etre des repertoires de
    # donnees de simulation
    repertoire_data_base = "data"

    #*************************************************************************
    #
    #                       ASPECT UTILISATION
    #
    #*************************************************************************

    #*************************************************************************
    # Configuration du type de presentation de l'ensemble des modeles record
    # (presentation sous forme de tableau, fiche de renseignement...)
    # Possibilites offertes dans/par bd_modeles_record :
    # 'les_modeles_record/liste_modeles_record_tableau.html'
    # 'les_modeles_record/liste_modeles_record_fiches.html'
    #*************************************************************************
    NOM_page_liste_modeles_record = settings.NOM_PAGE_LISTE_MODELES_RECORD

    #*************************************************************************
    # Configuration du menu propose a l'utilisateur pour chaque modele record
    # Types traites dans/au niveau de bd_modeles_record (un_modele_record) :
    # 'type' : 'informations_completes', 'informations_partielles',
    #          'redirection', 'redirection_modele_record_id'
    #*************************************************************************

    # les menus des actions a proposer et les actions a realiser
    ( menu_actions, les_actions ) = construire_actions()

#*****************************************************************************

