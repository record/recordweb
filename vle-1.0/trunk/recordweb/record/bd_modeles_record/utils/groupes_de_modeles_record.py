#-*- coding:utf-8 -*-

## @file record/bd_modeles_record/utils/groupes_de_modeles_record.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File bd_modeles_record/utils/groupes_de_modeles_record.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# Des methodes de determination de groupes de presentation des modeles record
#
#*****************************************************************************

from record.bd_modeles_record.models import ModeleRecord 

from record.bd_modeles_record.forms.user_forms import ModeleRecordFormResume

from record.bd_modeles_record.models import ParDefautModeleRecord

from configs.conf_internationalisation import CONF_internationalisation

#from configs.conf_trace import CONF_trace
#from record.utils.trace import TraceEcran, TraceErreur

# pour traces
#t_ecr = TraceEcran(__file__,__name__,CONF_trace)
#t_err = TraceErreur(__file__,__name__,CONF_trace)

from django.utils.translation import ugettext as _

##..
#*****************************************************************************\n
# Methode determiner_les_groupesTous :
#
# Definit les groupes sous/selon lesquels les modeles sont presentes.
#
# Les modeles sont presentes par groupes. Il est donne une cle nom_groupe a
# chaque groupe. noms_groupes est la liste ordonnee de ces cles. Chaque
# groupe est caracterise par son titre groupes_titres[nom_groupe] et ses
# modeles groupes_formulaires_modeles_record[nom_groupe].
#
#*****************************************************************************
def determiner_les_groupesTous() :

    liste_modeles_record_bd = ModeleRecord.objects.all()

    noms_groupes = []
    groupes_titres = {}
    groupes_formulaires_modeles_record = {}

    #*************************************************************************
    # 1er groupe de modeles a afficher
    #*************************************************************************
    nom_groupe = "tous"
    noms_groupes.append( nom_groupe )

    groupes_titres[ nom_groupe ] = _(u"Les modèles")

    # les formulaires des modeles record a afficher
    formulaires_modeles_record = []
    for modele_record_bd in liste_modeles_record_bd :
        formulaires_modeles_record.append( ModeleRecordFormResume(instance=modele_record_bd) )

    groupes_formulaires_modeles_record[ nom_groupe ] = formulaires_modeles_record

    #*************************************************************************

    return ( noms_groupes, groupes_titres, groupes_formulaires_modeles_record )

##..
#*****************************************************************************\n
# Methode determiner_les_groupesClassique :
#
# Definit les groupes sous/selon lesquels les modeles sont presentes.
#
# Les modeles sont presentes par groupes. Il est donne une cle nom_groupe a
# chaque groupe. noms_groupes est la liste ordonnee de ces cles. Chaque
# groupe est caracterise par son titre groupes_titres[nom_groupe] et ses
# modeles groupes_formulaires_modeles_record[nom_groupe].
#
#*****************************************************************************
def determiner_les_groupesClassique() :

    liste_modeles_record_bd = ModeleRecord.objects.all()

    noms_groupes = []
    groupes_titres = {}
    groupes_formulaires_modeles_record = {}

    #*************************************************************************
    # 1er groupe de modeles a afficher
    #*************************************************************************
    nom_groupe = "tous"
    noms_groupes.append( nom_groupe )

    groupes_titres[ nom_groupe ] = _(u"Les modèles")

    # les formulaires des modeles record a afficher
    formulaires_modeles_record = []
    for modele_record_bd in liste_modeles_record_bd :
        formulaires_modeles_record.append( ModeleRecordFormResume(instance=modele_record_bd) )

    groupes_formulaires_modeles_record[ nom_groupe ] = formulaires_modeles_record

    #*************************************************************************
    # groupe suivant de modeles a afficher
    #*************************************************************************
    nom_groupe = "publics"
    noms_groupes.append( nom_groupe )

    groupes_titres[ nom_groupe ] = _(u"Les modèles en accès public")

    # les formulaires des modeles record a afficher
    formulaires_modeles_record = []
    for modele_record_bd in liste_modeles_record_bd :
        if modele_record_bd.isPublic() :
            formulaires_modeles_record.append( ModeleRecordFormResume(instance=modele_record_bd) )
    groupes_formulaires_modeles_record[ nom_groupe ] = formulaires_modeles_record

    #*************************************************************************
    # groupe suivant de modeles a afficher
    #*************************************************************************
    nom_groupe = "prives"
    noms_groupes.append( nom_groupe )

    groupes_titres[ nom_groupe ] = _(u"Les modèles en accès sous condition")

    # les formulaires des modeles record a afficher
    formulaires_modeles_record = []
    for modele_record_bd in liste_modeles_record_bd :
        if not modele_record_bd.isPublic() :
            formulaires_modeles_record.append( ModeleRecordFormResume(instance=modele_record_bd) )
    groupes_formulaires_modeles_record[ nom_groupe ] = formulaires_modeles_record

    #*************************************************************************

    return ( noms_groupes, groupes_titres, groupes_formulaires_modeles_record )

##..
#*****************************************************************************\n
# Methode determiner_les_groupesA :
#
# Definit les groupes sous/selon lesquels les modeles sont presentes.
#
# Les modeles sont presentes par groupes. Il est donne une cle nom_groupe a
# chaque groupe. noms_groupes est la liste ordonnee de ces cles. Chaque
# groupe est caracterise par son titre groupes_titres[nom_groupe] et ses
# modeles groupes_formulaires_modeles_record[nom_groupe].
#
#*****************************************************************************
def determiner_les_groupesA() :

    liste_modeles_record_bd = ModeleRecord.objects.all()

    noms_groupes = []
    groupes_titres = {}
    groupes_formulaires_modeles_record = {}

    #*************************************************************************
    # 1er groupe de modeles a afficher
    #*************************************************************************
    nom_groupe = "cv"
    noms_groupes.append( nom_groupe )

    groupes_titres[ nom_groupe ] = _(u"Les modèles 2CV")

    # les formulaires des modeles record a afficher
    formulaires_modeles_record = []
    for modele_record_bd in liste_modeles_record_bd :
        if modele_record_bd.nom_pkg == "2CV" :
            formulaires_modeles_record.append( ModeleRecordFormResume(instance=modele_record_bd) )
    groupes_formulaires_modeles_record[ nom_groupe ] = formulaires_modeles_record

    #*************************************************************************

    return ( noms_groupes, groupes_titres, groupes_formulaires_modeles_record )

#*****************************************************************************

##..
#*****************************************************************************\n
# Methode determiner_les_groupesCaracteristique :
#
# Definit les groupes sous/selon lesquels les modeles sont presentes.
#
# Un groupe par champ caracteristiquei(i=1..16) du modele.
#
# Les modeles sont presentes par groupes. Il est donne une cle nom_groupe a
# chaque groupe. noms_groupes est la liste ordonnee de ces cles. Chaque
# groupe est caracterise par son titre groupes_titres[nom_groupe] et ses
# modeles groupes_formulaires_modeles_record[nom_groupe].
#
#*****************************************************************************
def determiner_les_groupesCaracteristique() :

    liste_modeles_record_bd = ModeleRecord.objects.all()

    noms_groupes = []
    groupes_titres = {}
    groupes_formulaires_modeles_record = {}

    #*************************************************************************
    # groupe de modeles a afficher, correspondant a caracteristiquei(i=1..16)
    #*************************************************************************
    for i in range(1,17) :

        nom_caracteristique = "caracteristique"+str(i)

        nom_groupe = nom_caracteristique
        noms_groupes.append( nom_groupe )

        groupe_titre = _(u"Les modèles avec ") + nom_caracteristique # par defaut
        if i == 16 :
            groupe_titre = _(u"Les modèles des essais (") + nom_caracteristique + ")"

        groupes_titres[ nom_groupe ] = groupe_titre
    
        # les formulaires des modeles record a afficher
        formulaires_modeles_record = []
        for modele_record_bd in liste_modeles_record_bd :
    
            if getattr(modele_record_bd,nom_caracteristique) == "oui" :
            #if modele_record_bd.caracteristique1 == "oui" :
                formulaires_modeles_record.append( ModeleRecordFormResume(instance=modele_record_bd) )
        groupes_formulaires_modeles_record[ nom_groupe ] = formulaires_modeles_record

    #*************************************************************************

    return ( noms_groupes, groupes_titres, groupes_formulaires_modeles_record )


##..
#*****************************************************************************\n
# Methode determiner_les_groupesRecord :
#
# Definit les groupes sous/selon lesquels les modeles sont presentes.
#
# Un groupe par champ caracteristiquei(i=1..16) du modele + ...
#
# Les modeles sont presentes par groupes. Il est donne une cle nom_groupe a
# chaque groupe. noms_groupes est la liste ordonnee de ces cles. Chaque
# groupe est caracterise par son titre groupes_titres[nom_groupe] et ses
# modeles groupes_formulaires_modeles_record[nom_groupe].
#
#*****************************************************************************
def determiner_les_groupesRecord() :

    liste_modeles_record_bd = ModeleRecord.objects.all()

    noms_groupes = []
    groupes_titres = {}
    groupes_formulaires_modeles_record = {}

    #*************************************************************************
    # groupe de modeles a afficher, correspondant a caracteristiquei(i=1..16)
    #*************************************************************************
    #for i in range(1,17) : # 1 a 16 # tous
    for i in range(1,12) : # 1 a 11 (12 a 15 inutilisees, 16 a ne pas montrer)

        nom_caracteristique = "caracteristique"+str(i)

        nom_groupe = nom_caracteristique
        noms_groupes.append( nom_groupe )

        groupe_titre = nom_caracteristique # par defaut
        if i == 1 :
            #groupe_titre = _(u"Modèles de culture") + " (" + groupe_titre + ")"
            groupe_titre = _(u"Modèles de culture")
        if i == 2 :
            #groupe_titre = _(u"Modèles de système de culture") + " (" + groupe_titre + ")"
            groupe_titre = _(u"Modèles de système de culture")
        if i == 3 :
            #groupe_titre = _(u"Modèles de système de production (domaine végétal)") + " (" + groupe_titre + ")"
            groupe_titre = _(u"Modèles de système de production (domaine végétal)")
        if i == 4 :
            #groupe_titre = _(u"Modèles à l'échelle d'un territoire (domaine végétal)") + " (" + groupe_titre + ")"
            groupe_titre = _(u"Modèles à l'échelle d'un territoire (domaine végétal)")
        if i == 5 :
            #groupe_titre = _(u"Modèles architecture de plante ou bioagresseur") + " (" + groupe_titre + ")"
            groupe_titre = _(u"Modèles architecture de plante ou bioagresseur")
        if i == 6 :
            #groupe_titre = _(u"Modèles animal") + " (" + groupe_titre + ")"
            groupe_titre = _(u"Modèles animal")
        if i == 7 :
            #groupe_titre = _(u"Modèles troupeau") + " (" + groupe_titre + ")"
            groupe_titre = _(u"Modèles troupeau")
        if i == 8 :
            #groupe_titre = _(u"Modèles de système de production (domaine animal)") + " (" + groupe_titre + ")"
            groupe_titre = _(u"Modèles de système de production (domaine animal)")
        if i == 9 :
            #groupe_titre = _(u"Modèles à l'échelle d'un territoire (domaine animal)") + " (" + groupe_titre + ")"
            groupe_titre = _(u"Modèles à l'échelle d'un territoire (domaine animal)")
        if i == 10 :
            #groupe_titre = _(u"Modèles utilitaires") + " (" + groupe_titre + ")"
            groupe_titre = _(u"Modèles utilitaires")
        if i == 11 :
            #groupe_titre = _(u"Modèles exemples") + " (" + groupe_titre + ")"
            groupe_titre = _(u"Modèles exemples")
        #if i == 12 :
        #if i == 13 :
        #if i == 14 :
        #if i == 15 :
        if i == 16 :
            #groupe_titre = _(u"Modèles réservés à des essais (") + groupe_titre + ")"
            groupe_titre = _(u"Modèles réservés à des essais")

        groupes_titres[ nom_groupe ] = groupe_titre
    
        # les formulaires des modeles record a afficher
        formulaires_modeles_record = []
        for modele_record_bd in liste_modeles_record_bd :
    
            if getattr(modele_record_bd,nom_caracteristique) == "oui" :
            #if modele_record_bd.caracteristique1 == "oui" :
                formulaires_modeles_record.append( ModeleRecordFormResume(instance=modele_record_bd) )
        groupes_formulaires_modeles_record[ nom_groupe ] = formulaires_modeles_record

    #*************************************************************************
    # groupe suivant de modeles a afficher
    #*************************************************************************
    nom_groupe = "vegetal"
    noms_groupes.append( nom_groupe )

    groupes_titres[ nom_groupe ] = _(u"Modèles du domaine végétal")

    # les formulaires des modeles record a afficher
    formulaires_modeles_record = []
    for modele_record_bd in liste_modeles_record_bd :
        if modele_record_bd.caracteristique1 == "oui" or modele_record_bd.caracteristique2 == "oui" or modele_record_bd.caracteristique3 == "oui" or modele_record_bd.caracteristique4 == "oui" or modele_record_bd.caracteristique5 == "oui" :
            formulaires_modeles_record.append( ModeleRecordFormResume(instance=modele_record_bd) )
    groupes_formulaires_modeles_record[ nom_groupe ] = formulaires_modeles_record

    #*************************************************************************
    # groupe suivant de modeles a afficher
    #*************************************************************************
    nom_groupe = "animal"
    noms_groupes.append( nom_groupe )

    groupes_titres[ nom_groupe ] = _(u"Modèles du domaine animal")

    # les formulaires des modeles record a afficher
    formulaires_modeles_record = []
    for modele_record_bd in liste_modeles_record_bd :
        if modele_record_bd.caracteristique6 == "oui" or modele_record_bd.caracteristique7 == "oui" or modele_record_bd.caracteristique8 == "oui" or modele_record_bd.caracteristique9 == "oui" :
            formulaires_modeles_record.append( ModeleRecordFormResume(instance=modele_record_bd) )
    groupes_formulaires_modeles_record[ nom_groupe ] = formulaires_modeles_record

    #*************************************************************************

    return ( noms_groupes, groupes_titres, groupes_formulaires_modeles_record )


#*****************************************************************************

#*****************************************************************************


