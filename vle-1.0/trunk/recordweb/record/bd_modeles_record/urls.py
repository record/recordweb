
## @file record/bd_modeles_record/urls.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File urls.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
#******************************************************************************

from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('record.bd_modeles_record.views',

    url(r'^$', 'les_modeles_record'),
    url(r'^les_modeles_record/$', 'les_modeles_record'),
    url(r'^bdrec/usr/les_modeles_record/$', 'les_modeles_record'),

    #old url(r'^un_modele_record_menu/(?P<modele_record_id>\d+)/$', 'un_modele_record_menu'),
    #old url(r'^bdrec/usr/un_modele_record_menu/(?P<modele_record_id>\d+)/$', 'un_modele_record_menu'),
    url(r'^un_modele_record_menu/$', 'un_modele_record_menu'),
    url(r'^bdrec/usr/un_modele_record_menu/$', 'un_modele_record_menu'),

    #old url(r'^un_modele_record_mdp/(?P<modele_record_id>\d+)/$', 'un_modele_record_mdp'),
    #old url(r'^bdrec/usr/un_modele_record_mdp/(?P<modele_record_id>\d+)/$', 'un_modele_record_mdp'),
    url(r'^un_modele_record_mdp/$', 'un_modele_record_mdp'),
    url(r'^bdrec/usr/un_modele_record_mdp/$', 'un_modele_record_mdp'),
)

