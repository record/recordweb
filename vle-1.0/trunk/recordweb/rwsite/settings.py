#-*- coding:utf-8 -*-

## @file rwsite/settings.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File settings.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
# Voir commentaires directement dans code
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# Django settings for rwsite project.
#
# Record web site
# for Record web tool and the Record models database administration
# (based on bd_modeles_record and outil_web_record applications)
#
#*****************************************************************************

import os
import sys

from settings_default import *

#*****************************************************************************
# Configuration EN_PRODUCTION, MACHINE_PROD et SERVEUR_DJANGO
#
# EN_PRODUCTION (etat) (impact sur les emplacements/chemins : /opt/...) :
# - EN_PRODUCTION = True pour situation de mise en production
#   soit effective (sur VM de production webrecord) soit dans le cadre des
#   essais (sur VM d'essai webrecord_test) avant mise en production effective
# - EN_PRODUCTION = False pour situation de/en developpement
#
# MACHINE_PROD (la machine d'installation) (impact sur les URLs) :
# - MACHINE_PROD = True  pour machine de production (147.99.96.155)
# - MACHINE_PROD = False pour machine de developpement (mon PC 147.99.96.72)
#
# SERVEUR_DJANGO (le serveur utilise)
#                (impact sur DEBUG, les URLs, specificites apache...) :
# - SERVEUR_DJANGO = True  pour serveur de developpement (django)
# - SERVEUR_DJANGO = False pour serveur de production (apache)
#
#*****************************************************************************

# Sur VM de production (webrecord) :
#EN_PRODUCTION = True # en production
#MACHINE_PROD = True # machine de production
#SERVEUR_DJANGO = False # serveur apache
# #SERVEUR_DJANGO = True # serveur django (temporairement)

# Sur VM d'essai (webrecord_test) avant mise en production :
#EN_PRODUCTION = True # (essais avant mise) en production
#MACHINE_PROD = False # machine de developpement
#SERVEUR_DJANGO = False # serveur apache
# #SERVEUR_DJANGO = True # serveur django (temporairement)

# En developpement (travail local) :
EN_PRODUCTION = False # en developpement
MACHINE_PROD = False # machine de developpement
SERVEUR_DJANGO = True # serveur django

#*****************************************************************************
#                                                                             #
# 'espace web' : emplacement espace_web partage par tous les projets web      #
# 'projet web' : un 'projet web' correspond a une installation                #
# { rmlib, rwtool, rwsite, et leurs bd associees (la bd des modeles et la bd  #
# du site) } sous emplacement espace_projet_web,                              #
# pour l'instant { recordweb, recordweb_depot, bd_modeles_record_directory }  #
#                                                                             #
#*****************************************************************************

if EN_PRODUCTION :
    espace_web = '/opt'
else :
    espace_web = os.path.join( os.environ.get('HOME'), "appli_web/django" )
    #espace_web = os.path.join( os.environ.get('HOME'), "workspace_svn/trunk_recordweb" )           # !!! TMP

if EN_PRODUCTION :
    #espace_projet_web = os.path.join( espace_web, "nom_du_projet_web" )
    espace_projet_web = os.path.join( espace_web, "demonstrateur_prive" )
    #espace_projet_web = os.path.join( espace_web, "demonstrateur_public" )
else :
    espace_projet_web = espace_web

#*****************************************************************************
# Variables, Variables d'environnement
#*****************************************************************************

## RECORDWEB_HOME, repertoire racine (projets, applications, librairies python)
RECORDWEB_HOME = os.path.join( espace_projet_web, "recordweb" )

# sous le repertoire racine RECORDWEB_HOME, repertoire 'record' : code record
# (librairies python et applications au sens django du terme)

# librairies python **********************************************************

# path du project rwsite *****************************************************
# (correspond a RECORDWEB_HOME/rwsite)
SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
if SITE_ROOT not in sys.path :
    sys.path.insert(0, SITE_ROOT )

# RECORDWEB_HOME
# => acces aux librairies record et applications record
if RECORDWEB_HOME not in sys.path :
    sys.path.insert(0, RECORDWEB_HOME )

#*****************************************************************************
# Configuration
#*****************************************************************************

# URLs referencees ***********************************************************

if SERVEUR_DJANGO : # serveur django
    debut_url = "http://127.0.0.1"
else : # serveur apache
    debut_url = "http://webrecord.toulouse.inra.fr"

    # en attendant choix/depot du nom de domaine
    if MACHINE_PROD :
        # machine de production (VM de production webrecord)
        debut_url = "http://147.99.96.155"
    #else :
        # machine de developpement
        # (VM d'essai webrecord_test avant mise en production)
        #debut_url = "http://147.99.96.72"

# remplacer xxxx,yyyy,zzzz par les numeros de port d'installation de rwsite,rwtool,rmlib du projet web a pointer/referencer dans rwsite
#URL_RWTOOL_USR = debut_url + ":" + "yyyy" + "/" + "rwtool/usr" # nom_du_projet_web (utiliser l'outil_web_record)
URL_RWTOOL_USR = debut_url + ":" + "24002" + "/" + "rwtool/usr" 
#URL_RMLIB_ADM  = debut_url + ":" + "zzzz" + "/" + "rmlib/adm"  # nom_du_projet_web (administrer la bd des modeles de l'outil_web_record)
URL_RMLIB_ADM  = debut_url + ":" + "24022" + "/" + "rmlib/adm"
#URL_RWSITE_ADM = debut_url + ":" + "xxxx" + "/" + "admin"      # nom_du_projet_web (administrer le site de l'outil_web_record)
URL_RWSITE_ADM = debut_url + ":" + "24012" + "/" + "admin" 

#*****************************************************************************

# trace **********************************************************************
print ""
print "[rwsite/settings.py] pour info variables d'environnement"
print "[rwsite/settings.py] HOME  = ", os.environ.get('HOME')
print "[rwsite/settings.py] sys.path = ", sys.path
print ""
print "[rwsite/settings.py] pour info variables (settings) :"
print ""
print "[rwsite/settings.py] EN_PRODUCTION  = ", EN_PRODUCTION
print "[rwsite/settings.py] MACHINE_PROD  = ", MACHINE_PROD
print "[rwsite/settings.py] SERVEUR_DJANGO  = ", SERVEUR_DJANGO
print "[rwsite/settings.py] RECORDWEB_HOME  = ", RECORDWEB_HOME
print ""
print "[rwsite/settings.py] URL_RWTOOL_USR = ", URL_RWTOOL_USR
print "[rwsite/settings.py] URL_RMLIB_ADM = ", URL_RMLIB_ADM
print "[rwsite/settings.py] URL_RWSITE_ADM = ", URL_RWSITE_ADM
print ""
print "pour info espace_web  = ", espace_web
print "pour info espace_projet_web  = ", espace_projet_web
print ""

#*****************************************************************************

#*****************************************************************************
# la BD du site web record exploitee par rwsite
#*****************************************************************************

## emplacement et nom de la BD du site web record exploitee par rwsite
BD_SITE_WEB_RECORD = os.path.join( SITE_ROOT, 'bd_site_web_record.db' )

#*****************************************************************************
# recuperation d'elements de configuration propres a record (partie commune)
#*****************************************************************************

from record import record_settings as RECORD_SETTINGS

#*****************************************************************************
# debug
#*****************************************************************************

if SERVEUR_DJANGO :
    DEBUG = True # True # False
else :
    DEBUG = False # par defaut # True # False

TEMPLATE_DEBUG = DEBUG

#*****************************************************************************
# to email errors
#*****************************************************************************

# ADMIN for 500 errors
if not SERVEUR_DJANGO : # apache
    ADMINS = (
        ('rousse', 'nathalie.rousse@toulouse.inra.fr'),
    )

if not SERVEUR_DJANGO : # apache

    # to email 500 errors ****************************************************

    if MACHINE_PROD : # (VM de production webrecord)
        SERVER_EMAIL = 'django-rwsite@webrecord.com'
    else : # (VM d'essai webrecord_test avant mise en production)
        SERVER_EMAIL = 'django-rwsite@webrecordtest.com'

    #pseudo-solution avec 'python -m smtpd -n -c DebuggingServer localhost:1025'
    #EMAIL_HOST= 'localhost'
    #EMAIL_PORT=1025

    # serveur smtp exim :
    EMAIL_HOST= 'smtp.toulouse.inra.fr'
    #EMAIL_PORT par defaut

    # to email 404 errors
    SEND_BROKEN_LINK_EMAILS = True

#*****************************************************************************

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': BD_SITE_WEB_RECORD, # Or path to database file if using sqlite3.
        'USER': '',                 # Not used with sqlite3.
        'PASSWORD': '',             # Not used with sqlite3.
        'HOST': '',                 # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                 # Set to empty string for default. Not used with sqlite3.
    }
}

# Additional locations of static files
STATICFILES_DIRS += RECORD_SETTINGS.STATICFILES_DIRS_RECORD

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'd935zy0n6e$ypn33)+pb9rp&_sojyyu8@!t9piow!$)(^firzp'

# Additional locations of template files
TEMPLATE_DIRS += RECORD_SETTINGS.TEMPLATE_DIRS_RECORD

TEMPLATE_CONTEXT_PROCESSORS += (
    'configs.context_processors.configuration',
)

# Additional apps
# none INSTALLED_APPS += ( , )
#INSTALLED_APPS += ( 'configs', )

#*****************************************************************************

