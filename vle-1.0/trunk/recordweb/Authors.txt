             ===========
              recordweb
             ===========

Copyright
=========

- INRA :
  Copyright (C) 2011-2012 INRA
  French National Institute for Agricultural Research
  Institut National de la Recherche Agronomique
  http://www.inra.fr

Authors
=======

- Nathalie Rousse :
  <nathalie.rousse@toulouse.inra.fr>
  RECORD platform team member http://www4.inra.fr/record
  INRA http://www.inra.fr

IDDN
====

Le logiciel recordweb a fait l'objet d'un referencement a l'APP (Agence pour la Protection des Programmes). Son numero IDDN (Inter Deposit Digital Number) est : IDDN.FR.001.010017.000.R.P.2013.000.31235


