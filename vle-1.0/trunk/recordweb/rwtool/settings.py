#-*- coding:utf-8 -*-

## @file rwtool/settings.py
#..
#******************************************************************************
#
#------------------------------------------------------------------------------
#
# File settings.py
#
# Authors : Nathalie Rousse, RECORD platform team member, INRA.
#
#------------------------------------------------------------------------------
#
# recordweb - RECORD platform Web Development
#
# Copyright (C) 2011-2012 INRA http://www.inra.fr
#
# This file is part of recordweb.
#
# recordweb is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# recordweb is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with recordweb.  If not, see <http://www.gnu.org/licenses/>.
#
#------------------------------------------------------------------------------
#
# Voir commentaires directement dans code
#
#------------------------------------------------------------------------------
#
#******************************************************************************

#*****************************************************************************
#
# Django settings for rwtool project.
#
# Record web tool (based on bd_modeles_record and outil_web_record applications)
#
#
# notes_de_developpement : les duplications ne sont pas gerees pour les
# variables VAR construites a partir de series de VAR_FIRST et VAR_LAST
#
#*****************************************************************************

import os
import sys

#*****************************************************************************
# Configuration EN_PRODUCTION, MACHINE_PROD et SERVEUR_DJANGO
#
# EN_PRODUCTION (etat) (impact sur les emplacements/chemins : /opt/...) :
# - EN_PRODUCTION = True pour situation de mise en production
#   soit effective (sur VM de production webrecord) soit dans le cadre des
#   essais (sur VM d'essai webrecord_test) avant mise en production effective
# - EN_PRODUCTION = False pour situation de/en developpement
#
# MACHINE_PROD (la machine d'installation) (impact sur les URLs) :
# - MACHINE_PROD = True  pour machine de production (147.99.96.155)
# - MACHINE_PROD = False pour machine de developpement (mon PC 147.99.96.72)
#
# SERVEUR_DJANGO (le serveur utilise)
#                (impact sur DEBUG, les URLs, specificites apache...) :
# - SERVEUR_DJANGO = True  pour serveur de developpement (django)
# - SERVEUR_DJANGO = False pour serveur de production (apache)
#
#*****************************************************************************

# Sur VM de production (webrecord) :
#EN_PRODUCTION = True # en production
#MACHINE_PROD = True # machine de production
#SERVEUR_DJANGO = False # serveur apache
# #SERVEUR_DJANGO = True # serveur django (temporairement)

# Sur VM d'essai (webrecord_test) avant mise en production :
#EN_PRODUCTION = True # (essais avant mise) en production
#MACHINE_PROD = False # machine de developpement
#SERVEUR_DJANGO = False # serveur apache
# #SERVEUR_DJANGO = True # serveur django (temporairement)

# En developpement (travail local) :
EN_PRODUCTION = False # en developpement
MACHINE_PROD = False # machine de developpement
SERVEUR_DJANGO = True # serveur django

#*****************************************************************************
#                                                                             #
# 'espace web' : emplacement espace_web partage par tous les projets web      #
# 'projet web' : un 'projet web' correspond a une installation                #
# { rmlib, rwtool, rwsite, et leurs bd associees (la bd des modeles et la bd  #
# du site) } sous emplacement espace_projet_web,                              #
# pour l'instant { recordweb, recordweb_depot, bd_modeles_record_directory }  #
#                                                                             #
#*****************************************************************************

if EN_PRODUCTION :
    espace_web = '/opt'
else :
    espace_web = os.path.join( os.environ.get('HOME'), "appli_web/django" )
    #espace_web = os.path.join( os.environ.get('HOME'), "workspace_svn/trunk_recordweb" )           # !!! TMP

if EN_PRODUCTION :
    #espace_projet_web = os.path.join( espace_web, "nom_du_projet_web" )
    #espace_projet_web = os.path.join( espace_web, "demonstrateur_prive" )
    espace_projet_web = os.path.join( espace_web, "demonstrateur_public" )
else :
    espace_projet_web = espace_web

#*****************************************************************************
# Variables, Variables d'environnement
#*****************************************************************************

## RECORDWEB_HOME, repertoire racine (projets, applications, librairies python)
RECORDWEB_HOME = os.path.join( espace_projet_web, "recordweb" )

# sous le repertoire racine RECORDWEB_HOME, repertoire 'record' : code record
# (librairies python et applications au sens django du terme)

# vle ************************************************************************
# VLE_HOME : la variable d'environnement VLE_HOME va etre initialisee a la valeur VLE_HOME. Le repertoire VLE_HOME est le repertoire sous lequel sont construits les repertoires VLE_HOME attribues a chaque session/connexion

## pour outil_web_record (usr)
# VLE_HOME (${HOME}/.vle)
if EN_PRODUCTION :
    VLE_HOME = os.path.join( espace_projet_web, 'VLE' )
else :
    VLE_HOME = os.path.join( os.environ.get('HOME'), ".vle" )

## pour outil_web_record (usr)
# variable d'environnement VLE_HOME
os.environ['VLE_HOME'] = VLE_HOME

## pour bd_modeles_record (adm)
# VERSIONS_VLE, liste des versions vle gerees/prises en compte
VERSIONS_VLE = [ "vle-0.8.9", "vle-1.0", "vle-1.1" ]

# depot des modeles record ***************************************************
# - LOTS_PKGS_PATH : lots des paquets vle (pouvant contenir des fichiers de configuration, contenant des repertoires data)
# - CONFIGURATION_PATH : repertoire englobant l'ensemble des fichiers de configuration/personnalisation (ceux presents dans les paquets vle, et d'autres en plus)
# - DATA_PATH : repertoire englobant l'ensemble des repertoires de donnees de simulation (ceux presents dans les paquets vle, et d'autres en plus)

## pour bd_modeles_record (adm)
# DEPOT_PATH, racine du depot des modeles record (emplacement)
DEPOT_PATH = os.path.join( espace_projet_web, "recordweb_depot" )

## pour bd_modeles_record (adm)
# LOTS_PKGS_PATH, repertoire racine sous lequel est cherche l'ensemble des lots des paquets vle des modeles record
LOTS_PKGS_PATH = os.path.join( DEPOT_PATH, "pkgs_recordweb" )

## pour bd_modeles_record (adm)
# CONFIGURATION_PATH, repertoire racine sous lequel est cherche l'ensemble des fichiers de configuration (pas partout, seulement dans certains sous repertoires, cf code)
CONFIGURATION_PATH = DEPOT_PATH

## pour bd_modeles_record (adm)
# DATA_PATH, repertoire racine sous lequel est cherche l'ensemble des repertoires de donnees de simulation (pas partout, seulement dans certains sous repertoires, cf code)
DATA_PATH = DEPOT_PATH

# vendors ********************************************************************

# RECORDWEB_VENDOR, repertoire des librairies, applications django externes (django-transmeta...)
RECORDWEB_VENDOR = os.path.join( espace_web, "recordweb_vendor" )

# librairies python **********************************************************

# path du project rwtool *****************************************************
# (correspond a RECORDWEB_HOME/rwtool)
SITE_ROOT = os.path.realpath(os.path.dirname(__file__))
if SITE_ROOT not in sys.path :
    sys.path.insert(0, SITE_ROOT )

# RECORDWEB_HOME
# => acces aux librairies record et applications record
if RECORDWEB_HOME not in sys.path :
    sys.path.insert(0, RECORDWEB_HOME )

# path admin static files ****************************************************
if not SERVEUR_DJANGO : # apache
    PATH_DJANGO_ADMIN_MEDIA='/usr/local/lib/python2.6/dist-packages/django/contrib/admin/media'
    if PATH_DJANGO_ADMIN_MEDIA not in sys.path :
        sys.path.insert(0, PATH_DJANGO_ADMIN_MEDIA )

# django-transmeta (RECORDWEB_VENDOR/django-transmeta-read-only)
#enleve depuis installation de django-transmeta par 'python setup.py install'
#sys.path.insert(0, os.path.join( RECORDWEB_VENDOR, "django-transmeta-read-only" ) )

#*****************************************************************************
# Configuration du fonctionnement de l'application - aspect utilisation
# (voir bd_modeles_record.configs.CONF_bd_modeles_record)
#*****************************************************************************

# pour bd_modeles_record (usr)
NOM_PAGE_LISTE_MODELES_RECORD = 'les_modeles_record/liste_modeles_record_tableau.html'
#NOM_PAGE_LISTE_MODELES_RECORD = 'les_modeles_record/liste_modeles_record_fiches.html'
#NOM_PAGE_LISTE_MODELES_RECORD = '' # pour garder valeur par defaut

# pour bd_modeles_record (usr)
# pour connaitre les valeurs traitees au niveau de record/bd_modeles_record,
# voir bd_modeles_record.configs.CONF_bd_modeles_record
if SERVEUR_DJANGO : # serveur django
    debut_url = "http://127.0.0.1"
else : # serveur apache
    debut_url = "http://webrecord.toulouse.inra.fr"

#debut_url_rwtool = debut_url+":xxx/" # nom_du_projet_web : remplacer xxx par le numero du port d'installation de rwtool du projet web nom_du_projet_web
#debut_url_rwtool = debut_url+":24002/" # demonstrateur_prive
debut_url_rwtool = debut_url+":24000/" # demonstrateur_public

ACTIONS = [
            # fiche de renseignements partielle (sans demande de mot de passe)
            #{ 'type':'informations_partielles' ,
              #'label':u"Fiche de renseignements partielle", }, # 'url' par defaut

            # fiche de renseignements complete
            { 'type':'informations_completes' ,
              'label':u"Informations", }, # 'url' par defaut
            #  'label':u"Fiche de renseignements complète", }, # 'url' par defaut

            { 'type':'redirection_modele_record_id',
              'label':u"Simulation",
              'url': debut_url_rwtool+"owrec/usr/liste_scenarios/", },

           # { 'type':'redirection',
           #  'label':"Ancienne version",
           #  'url':"http://147.99.107.100", },
           ]

#*****************************************************************************
# Divers
#*****************************************************************************

## pour outil_web_record (usr), pour bd_modeles_record (usr)
# pour CONF_trace (traces ecran)
if SERVEUR_DJANGO :
    TRACE_ECRAN = True
    TRACE_ERREUR = True
else :
    TRACE_ECRAN = False
    TRACE_ERREUR = True
TRACE_ECRAN = True
TRACE_ERREUR = True

## pour CONF_internationalisation
MENU_LANGAGE_BD_MODELES_RECORD = True # False # (avec/sans menu de choix de la langue)

#*****************************************************************************

# trace **********************************************************************
print ""
#print "pour info variables d'environnement"
#print "HOME  = ", os.environ.get('HOME')
#print "VLE_HOME  = ", os.environ.get('VLE_HOME')
#print "sys.path = ", sys.path
#print ""
#print "pour info variables (settings)"
#print "pour info EN_PRODUCTION  = ", EN_PRODUCTION
#print "pour info MACHINE_PROD  = ", MACHINE_PROD
#print "pour info SERVEUR_DJANGO  = ", SERVEUR_DJANGO
#print "pour info RECORDWEB_HOME  = ", RECORDWEB_HOME
#print "pour info VLE_HOME  = ", VLE_HOME
#print "pour info LOTS_PKGS_PATH = ", LOTS_PKGS_PATH
#print "pour info CONFIGURATION_PATH = ", CONFIGURATION_PATH
#print "pour info DATA_PATH = ", DATA_PATH
#print "pour info RECORDWEB_VENDOR  = ", RECORDWEB_VENDOR
#print "pour info NOM_PAGE_LISTE_MODELES_RECORD  = ", NOM_PAGE_LISTE_MODELES_RECORD
#print "pour info ACTIONS  = ", ACTIONS
print "pour info espace_web  = ", espace_web
print "pour info espace_projet_web  = ", espace_projet_web
#print ""
#*****************************************************************************

#*****************************************************************************
# la BD des modeles record exploitee par l'outil_web_record
#*****************************************************************************

## emplacement et nom de la BD des modeles record exploitee par l'outil_web_record
BD_MODELES_RECORD = os.path.join( espace_projet_web, 'bd_modeles_record_directory', 'bd_modeles_record.db' )

#*****************************************************************************
# Configuration recueil de statistiques sur le site
#*****************************************************************************

#************* avec django-request (statistics module) ***********************
# See http://django-request.readthedocs.org/en/latest/settings.html#settings

#REQUEST_IGNORE_AJAX
#REQUEST_IGNORE_IP = ( '147.99.96.95',)
#REQUEST_IGNORE_USERNAME
#REQUEST_IGNORE_PATHS
REQUEST_TRAFFIC_MODULES = (
    #'request.traffic.Ajax', # to show the amount of requests made from javascript
    #'request.traffic.NotAjax', # to show the amount of requests that are NOT made from javascript
    'request.traffic.UniqueVisitor', # to show the amount of requests made from unique visitors based upon IP address
    'request.traffic.UniqueVisit', # to show visits based from outsider referrals
    'request.traffic.Hit', # to show the total amount of requests
    'request.traffic.Search', # to display requests from search engines
    #'request.traffic.Secure', # to show the amount of requests over SSL
    #'request.traffic.Unsecure', # to show the amount of requests NOT over SSL
    #'request.traffic.User', # to show the amount of requests made from a valid user account
    #'request.traffic.UniqueUser', # to show the amount of users
    'request.traffic.Error', # to show the amount of error’s, this includes error 500 and page not found
    'request.traffic.Error404', # to show the amount of page not found
    )
#REQUEST_PLUGINS
#REQUEST_BASE_URL
#REQUEST_ONLY_ERRORS

#*****************************************************************************
# recuperation d'elements de configuration propres a bd_modeles_record
#*****************************************************************************

from record.bd_modeles_record.bd_modeles_record_settings import bd_modeles_record_settings

BD_MODELES_RECORD_SETTINGS = bd_modeles_record_settings( 
                                 var_RECORDWEB_HOME=RECORDWEB_HOME, 
                                 var_VERSIONS_VLE=VERSIONS_VLE, 
                                 var_LOTS_PKGS_PATH=LOTS_PKGS_PATH, 
                                 var_CONFIGURATION_PATH=CONFIGURATION_PATH, 
                                 var_DATA_PATH=DATA_PATH, 
                                 var_RECORDWEB_VENDOR=RECORDWEB_VENDOR,
                                 var_NOM_PAGE_LISTE_MODELES_RECORD=NOM_PAGE_LISTE_MODELES_RECORD,
                                 var_ACTIONS=ACTIONS )

#*****************************************************************************
# recuperation d'elements de configuration propres a outil_web_record
#*****************************************************************************

from record.outil_web_record.outil_web_record_settings import outil_web_record_settings

OUTIL_WEB_RECORD_SETTINGS = outil_web_record_settings( 
                                 var_RECORDWEB_HOME=RECORDWEB_HOME, 
                                 var_VLE_HOME=VLE_HOME, 
                                 var_RECORDWEB_VENDOR=RECORDWEB_VENDOR )

#*****************************************************************************
# debug
#*****************************************************************************

if SERVEUR_DJANGO :
    DEBUG = True # True # False
else :
    DEBUG = False # par defaut # True # False

TEMPLATE_DEBUG = DEBUG

#*****************************************************************************
# to email errors
#*****************************************************************************

# ADMIN for 500 errors
ADMINS = (
    # ('Your Name', 'your_email@example.com'),
)
if not SERVEUR_DJANGO : # apache
    ADMINS = (
        ('rousse', 'nathalie.rousse@toulouse.inra.fr'),
    )

# MANAGERS for 404 errors
MANAGERS = ADMINS

if not SERVEUR_DJANGO : # apache

    # to email 500 errors ****************************************************

    if MACHINE_PROD : # (VM de production webrecord)
        SERVER_EMAIL = 'django-rwtool@webrecord.com'
    else : # (VM d'essai webrecord_test avant mise en production)
        SERVER_EMAIL = 'django-rwtool@webrecordtest.com'

    #pseudo-solution avec 'python -m smtpd -n -c DebuggingServer localhost:1025'
    #EMAIL_HOST= 'localhost'
    #EMAIL_PORT=1025

    # serveur smtp exim :
    EMAIL_HOST= 'smtp.toulouse.inra.fr'
    #EMAIL_PORT par defaut

    # to email 404 errors
    SEND_BROKEN_LINK_EMAILS = True

#*****************************************************************************

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': BD_MODELES_RECORD, # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

## directories where Django looks for translation files
# (en plus des repertoires 'locale' de projet etc)
# (du plus au moins prioritaire)
#
# Actuellement  le seul dictionnaire utilise/renseigne est celui de record (commun) ; des dictionnaires d'application (au sens django du terme) existent mais inusites
#
LOCALE_PATHS = ()
LOCALE_PATHS += BD_MODELES_RECORD_SETTINGS.LOCALE_PATHS
LOCALE_PATHS += OUTIL_WEB_RECORD_SETTINGS.LOCALE_PATHS # !!! duplications non gerees

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
#TIME_ZONE = 'America/Chicago'
TIME_ZONE = 'Europe/Paris'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
# enlever 'django.middleware.locale.LocaleMiddleware' pour que LANGUAGE_CODE soit actif
# LANGUAGE_CODE : default language in/for the site
#LANGUAGE_CODE = 'en-us'
LANGUAGE_CODE = 'fr'
#LANGUAGE_CODE = 'en'

# TRANSMETA_DEFAULT_LANGUAGE : default language to transmeta
TRANSMETA_DEFAULT_LANGUAGE = 'fr'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
# attention ! si USE_L10N = True, alors en fr float avec ',' au lieu de '.'
USE_L10N = False

# pour internationalisation traductions : redefinition LANGUAGES a laisser ?
_ = lambda s: s
LANGUAGES = (
    ('fr', _(u'Français')),
    ('en', _(u'Anglais')),
)

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
# STATIC_ROOT = '' # a ete remplace par
# STATIC_ROOT avec collectstatic
# (pour rafraichir/generer STATIC_ROOT : 'python manage.py collectstatic')
STATIC_ROOT = os.path.join( RECORDWEB_HOME, 'static_root_rwtool' )
#print "STATIC_ROOT = ", STATIC_ROOT

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
ADMIN_MEDIA_PREFIX = '/static/admin/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)
STATICFILES_DIRS += BD_MODELES_RECORD_SETTINGS.STATICFILES_DIRS_FIRST
STATICFILES_DIRS += OUTIL_WEB_RECORD_SETTINGS.STATICFILES_DIRS_FIRST
STATICFILES_DIRS += BD_MODELES_RECORD_SETTINGS.STATICFILES_DIRS_LAST
STATICFILES_DIRS += OUTIL_WEB_RECORD_SETTINGS.STATICFILES_DIRS_LAST # !!! duplications non gerees

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)


# Make this unique, and don't share it with anybody.
SECRET_KEY = 'n%@$ei5#-7cnx-zh1lk#g(9vqqqck9ct@1!x=f@5u57n9k6%fj'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware', # internationalisation traductions (doit etre apres SessionMiddleware) (Autodetect user language (from browser) or use session defined language (or fallback to project default)) : en commentaire pour que LANGUAGE_CODE soit actif
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'request.middleware.RequestMiddleware', # for django-request # a placer apres django.contrib.auth, avant django.contrib.flatpages
    'django.contrib.messages.middleware.MessageMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.core.context_processors.auth", # (django1.3)
    #"django.contrib.auth.context_processors.auth", # (django1.4)
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n", # internationalisation traductions
    "django.core.context_processors.media",
    "django.core.context_processors.debug",
)

ROOT_URLCONF = 'rwtool.urls'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)
TEMPLATE_DIRS += OUTIL_WEB_RECORD_SETTINGS.TEMPLATE_DIRS_FIRST
TEMPLATE_DIRS += BD_MODELES_RECORD_SETTINGS.TEMPLATE_DIRS_FIRST
TEMPLATE_DIRS += OUTIL_WEB_RECORD_SETTINGS.TEMPLATE_DIRS_LAST # !!! duplications non gerees
TEMPLATE_DIRS += BD_MODELES_RECORD_SETTINGS.TEMPLATE_DIRS_LAST

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'django.contrib.admin', # to enable the admin

    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',

    'floppyforms', # formulaires
    'form_utils',

    'transmeta',

    # Uncomment the next line for django-request statistics module :
    'request',

    # outil web record
    'record.outil_web_record', # utilisation outil web record
    
    # bd des modeles record
    'record.bd_modeles_record', # administration, utilisation (visualisation) bd des modeles record

)



# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}


#*****************************************************************************
# SESSION
#
# Using cached sessions (with Memcached) : not ok with Memcached
# or Using file-based sessions
# or Using cookie-based sessions
#
#*****************************************************************************

# Using cached sessions (with Memcached)
#SESSION_ENGINE = 'django.contrib.sessions.backends.cache' # for a simple caching session store
#CACHES = {
#     'default': {
#        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache', # using the python-memcached binding
#        'BACKEND': 'django.core.cache.backends.memcached.PyLibMCCache', # using the python-pylibmc binding
#        'LOCATION': '127.0.0.1:11211', # Memcached is running on localhost (127.0.0.1) port 11211
#        'LOCATION': 'unix:/tmp/memcached.sock', # Memcached is available through a local Unix socket file /tmp/memcached.sock
#    }
#}

# Using file-based sessions

SESSION_ENGINE = 'django.contrib.sessions.backends.file'

#SESSION_FILE_PATH setting (which defaults to output from tempfile.gettempdir(), most likely /tmp)

#SESSION_EXPIRE_AT_BROWSER_CLOSE
#SESSION_EXPIRE_AT_BROWSER_CLOSE = False # valeur par defaut # session cookies will be stored in users browsers for as long as SESSION_COOKIE_AGE
#SESSION_EXPIRE_AT_BROWSER_CLOSE = True # Django will use browser-length cookies (cookies that expire as soon as the user closes his or her browser)

#*****************************************************************************

