/**
 * @file test/make_conf_web.cpp
 * @author Nathalie Rousse, INRA RECORD team member.
 */

/** @file
 *
 * Copyright (C) 2012-2013 INRA.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* Description ***************************************************************/
/** @file
 *
 * @brief
 * Fichier de construction de trames de fichiers de configuration web (dicos, web_conf et web_applis)
 *
 *****************************************************************************/

/** @file
 *
 * Precisions *****************************************************************
 *
 * Construction de trames de fichiers de configuration web : generation des 3 fichiers vpz de configuration (dicos, web_conf et web_applis) de chacun des fichiers vpz scenario de simulation qui existent sous le repertoire exp.
 *
 * Fichiers sources : les fichiers vpz scenario de simulation qui existent sous le repertoire exp
 *
 * Fichiers produits : les fichiers de configuration (dicos, web_conf et web_applis) generes sous le repertoire output ; il s'agit de trames qu'il reste a remplir/adapter aux souhaits et a deplacer/ranger dans les repertoires meta (dicos) et web (web_conf et web_applis) pour les conserver.
 *
 *****************************************************************************/

#define BOOST_TEST_MAIN
#define BOOST_AUTO_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE package_test
#include <boost/test/unit_test.hpp>
#include <boost/test/auto_unit_test.hpp>

#include <vle/vpz.hpp>
#include <vle/vpz/Condition.hpp>
#include <vle/graph/Model.hpp>
#include <vle/graph/CoupledModel.hpp>
#include <vle/value.hpp> 

#include <conf_web.hpp>


BOOST_AUTO_TEST_CASE (make_conf_web)
{

    std::string nomPaquet = "weed"; // a renseigner

    std::string nomFichierVpz;
	conf_web::FichierVpzScenario *scn;

    /* (tous)
     * les fichiers dicos, web_conf et web_applis
     * de chacun des fichiers vpz scenario du paquet nomPaquet
     */
	BOOST_TEST_MESSAGE( conf_web::TousDicos(nomPaquet) );
	BOOST_TEST_MESSAGE( conf_web::TousWebConf(nomPaquet) );
	BOOST_TEST_MESSAGE( conf_web::TousWebApplis(nomPaquet) );

    /* (1 par 1)
     * les fichiers dicos, web_conf et web_applis
     * d'un fichier vpz scenario donne nomFichierVpz
     */
    /*
    nomFichierVpz = "weed.vpz"; // a renseigner
	scn = new conf_web::FichierVpzScenario( nomPaquet, nomFichierVpz );
	BOOST_TEST_MESSAGE( scn->Dicos( "mon_dicos.vpz" ) );          // ou BOOST_TEST_MESSAGE( scn->Dicos() );
	BOOST_TEST_MESSAGE( scn->WebConf( "mon_web_conf.vpz" ) );     // ou BOOST_TEST_MESSAGE( scn->WebConf() );
	BOOST_TEST_MESSAGE( scn->WebApplis( "mon_web_applis.vpz" ) ); // ou BOOST_TEST_MESSAGE( scn->WebApplis() );
     */

}

