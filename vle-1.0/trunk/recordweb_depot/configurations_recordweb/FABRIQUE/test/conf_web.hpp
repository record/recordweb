/**
 * @file test/conf_web.hpp
 * @author Nathalie Rousse, INRA RECORD team member.
 */

/** @file
 *
 * Copyright (C) 2012-2013 INRA.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* Description ***************************************************************/
/** @file
 *
 * @brief
 * Fichier de traitements de construction de trames de fichiers de configuration web (dicos de meta, web_conf et web_applis de web)
 *
 *****************************************************************************/

/** @file
 *
 * Precisions *****************************************************************
 *
 * Traitements de construction de trames de fichiers de configuration web (dicos de meta, web_conf et web_applis de web)
 *
 *****************************************************************************/

#ifndef CONF_WEB_HPP
#define CONF_WEB_HPP

#include <vle/vpz.hpp>
#include <vle/vpz/Condition.hpp>
#include <vle/graph/Model.hpp>
#include <vle/graph/CoupledModel.hpp>
#include <vle/manager.hpp>
#include <vle/utils/Package.hpp>
#include <vle/utils/Path.hpp>


namespace conf_web {


/* Construction dicos de chacun des fichiers vpz scenario d'un paquet */
std::string TousDicos( std::string nomDuPaquet );

/* Construction web_conf de chacun des fichiers vpz scenario d'un paquet */
std::string TousWebConf( std::string nomDuPaquet );

/* Construction web_applis de chacun des fichiers vpz scenario d'un paquet */
std::string TousWebApplis( std::string nomDuPaquet );


/***************************************************************//** \brief ...
 *
 * Classe pour la construction de trames de fichiers de configuration web
 * (dicos sous meta, web_conf et web_applis sous web)
 *
 ******************************************************************************/
class FichierVpzScenario {

public :

    /* constructeurs */
    FichierVpzScenario( std::string nomDuPaquet, 
                        std::string nomDuFichierVpz );
    ~FichierVpzScenario( void );

    /* construction dicos */
    std::string Dicos( std::string nomDuFichierVpzDicos="" );

    /* construction web_conf */
    std::string WebConf( std::string nomDuFichierVpzWebConf="" );

    /* construction web_applis */
    std::string WebApplis( std::string nomDuFichierVpzWebApplis="" );

private :

    void ajouterDonneesProjet( vle::value::Set& s );
    void ajouterDonneesCondition( std::string nom_condition, vle::vpz::Condition& c, vle::value::Set& s );

    std::string determinerNomFichierProduit( std::string nomChoisi, std::string prefixeParDefaut, std::string racineParDefaut );

    std::string getTexteArenseigner( void );
    std::string getNomDicoDeCondition( std::string & nom_condition );
    std::string getNomDicoProject( void );
    std::string getNomDicoEntreesSorties( void );
    std::string getNomPageDeCondition( std::string & nom_condition );
    std::string getNomPageProject( void );
    std::string getNomPageToutesInformations( void );

    std::string getIntitule_Parametre( std::string & nom_port );
    std::string getIntitule_Variable( std::string & nom_port );
    std::string getIntitule_Exp_Begin( void );
    std::string getIntitule_Exp_Duration( void );
    std::string getIntitule_Plan_Number( void );
    std::string getIntitule_Plan_Seed( void );
    std::string getIntitule_Simu_Seed( void );
    std::string getIntitule_experiment_name( void );

    void setMapInformationsGenerales( vle::value::Map & m, std::string r="" );
    void setMapInformationsGenerales( vle::value::Map & m, std::string description, std::string help, std::string titre );
    void setMapDocumentationDonnee( vle::value::Map & m );
    void setMapDocumentationParametre( vle::value::Map & m, std::string & nom_port );
    void setMapDocumentationVariable( vle::value::Map & m, std::string & nom_port );
    void setMapDocumentation_Exp_Begin( vle::value::Map & m );
    void setMapDocumentation_Exp_Duration( vle::value::Map & m );
    void setMapDocumentation_Plan_Number( vle::value::Map & m );
    void setMapDocumentation_Plan_Seed( vle::value::Map & m );
    void setMapDocumentation_Simu_Seed( vle::value::Map & m );
    void setMapDocumentation_experiment_name( vle::value::Map & m );

private :

    std::string nomPaquet;
    std::string nomFichierVpz;

};

} // namespace conf_web 

#endif // CONF_WEB_HPP
