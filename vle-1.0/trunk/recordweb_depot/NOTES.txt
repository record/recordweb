
Voir aussi documentation.py

###############################################################################
Dans documentation.py du depot svn recordweb :

- Depot des modeles record ; Arborescence de recordweb_depot (a respecter)

###############################################################################
Dans documentation.py du depot svn recordweb :

- Exigences concernant les modeles record livres/deposes dans la bd des modeles record pour exploitation par l'outil web record.

###############################################################################
#
# Conventions de nommage
#
###############################################################################

    # Noms des repertoires susceptibles de contenir des fichiers vpz de
    # configuration web propres a la conf web
    repertoire_config_web_conf="web"

    # Noms des repertoires susceptibles de contenir des fichiers vpz de
    # configuration web propres aux applis web
    repertoire_config_web_applis="web"

    # Noms des repertoires susceptibles de contenir des fichiers vpz de
    # configuration propres aux dictionnaires
    repertoire_config_dicos="meta"

    ###########################################################################
    # Repertoires des fichiers de donnees de simulation des modeles record
    ###########################################################################

    # Repertoire racine sous lequel est cherche l'ensemble des repertoires de
    # donnees de simulation
    repertoire_racine_data = settings.DATA_PATH

    # Base des noms de repertoires susceptibles d'etre des repertoires de
    # donnees de simulation
    repertoire_data_base = "data"

###############################################################################

