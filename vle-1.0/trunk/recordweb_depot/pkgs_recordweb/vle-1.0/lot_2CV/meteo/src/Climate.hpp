/**
 * @file Climate.hpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2009-2011 INRA
 * Copyright (C) 2009-2011 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLIMATE_HPP
#define CLIMATE_HPP 1

#include <vle/extension/DifferenceEquation.hpp>

namespace meteo {

/*! \class Climate
 *  \brief
 */

class Climate : public vle::extension::DifferenceEquation::Multiple
{
public:
    Climate(const vle::devs::DynamicsInit& init,
	    const vle::devs::InitEventList& events);

    virtual ~Climate();

    void initValue (const vle::devs::Time& time);

    void createVariable(const std::string& name)
    { mVariables[name] = createVar(name); }

    void setVariable(const std::string& name, double value)
    { mVariables[name] = value; }

private:
    std::map < std::string,
	       vle::extension::DifferenceEquation::Multiple::Var > mVariables;

protected:
    // support for assynchronism
    double deltaTime;
    double beginTime;
};

} // namespace meteo

#endif
