/**
 * @file NoHeader.hpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2009-2011 INRA
 * Copyright (C) 2009-2011 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NO_HEADER_HPP
#define NO_HEADER_HPP

#include <ClimateReader.hpp>

namespace meteo {

/*! \class NoHeader
 *  \brief
 */

class NoHeader : public ClimateReader
{
public:
    NoHeader(const vle::devs::DynamicsInit& init,
	     const vle::devs::InitEventList& events,
	     const std::string& filePath);
    virtual ~NoHeader();
};

} // namespace meteo

#endif
