INCLUDE_DIRECTORIES(
  ${CMAKE_SOURCE_DIR}/src
  ${VLE_INCLUDE_DIRS}
  ${Boost_INCLUDE_DIRS})

LINK_DIRECTORIES(
  ${VLE_LIBRARY_DIRS}
  ${Boost_LIBRARY_DIRS})

ADD_LIBRARY(meteo SHARED Climate.cpp Climate.hpp ClimateReader.cpp
  ClimateReader.hpp GenericWithHeader.cpp
  GenericWithoutHeader.cpp Header.cpp Header.hpp
  NoHeader.cpp NoHeader.hpp
  Stics.cpp SticsSeven.cpp MeteoFrance.cpp Agroclim.cpp
  ClimateGenerator.cpp ClimateGenerator.hpp)

TARGET_LINK_LIBRARIES(meteo
  ${VLE_LIBRARIES}
  ${Boost_LIBRARIES})

INSTALL(TARGETS meteo
  RUNTIME DESTINATION lib
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib)
