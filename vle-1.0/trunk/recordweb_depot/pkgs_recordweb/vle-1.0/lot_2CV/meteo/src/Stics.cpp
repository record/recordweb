/**
 * @file Stics.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2009-2011 INRA
 * Copyright (C) 2009-2011 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <NoHeader.hpp>

namespace meteo {

class Stics : public NoHeader
{
public:
    Stics(const vle::devs::DynamicsInit& init,
	  const vle::devs::InitEventList& events) :
	NoHeader(init, events, vle::value::toString(events.get("meteo_file")))
    {
        jumpLines(1);
        // col 6 : temperature minimale
        addColumn("Tmin", 5);
        // col 7 : temperature maximale
        addColumn("Tmax", 6);
        //col 8 : rayonnement (MJm-2 j-1)
        addColumn("RG", 7);
        // col 9 : ETP Penman (mmj-1) utile si code PE en ligne 1
        addColumn("ETP", 8);
        // col 10 : pluies (mmj-1)
        addColumn("Rain", 9);
        //col 11 : vent (ms-1) utile si code SW en ligne 1
        addColumn("Wind", 10);
        // col 12 : pression de vapeur (mbars) utile si code SW en ligne 1
        addColumn("Pressure", 11);
        createVariables();
    }

    virtual ~Stics()
    { }

private:
    int yearColumn() const
    { return 1; }

    int monthColumn() const
    { return 2; }

    int dayColumn() const
    { return 3; }
};

DECLARE_NAMED_DYNAMICS(Stics, Stics);

} // namespace meteo
