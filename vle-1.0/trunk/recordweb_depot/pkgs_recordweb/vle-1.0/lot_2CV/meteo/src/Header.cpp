/**
 * @file Header.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2009-2011 INRA
 * Copyright (C) 2009-2011 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Header.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/version.hpp>

#if BOOST_VERSION < 103600
#include <boost/spirit/utility/chset.hpp>
#include <boost/spirit/core.hpp>
#include <boost/spirit/utility/confix.hpp>
#include <boost/spirit/utility/lists.hpp>
#include <boost/spirit/utility/escape_char.hpp>

using namespace boost::spirit;

#else
#include <boost/spirit/include/classic_chset.hpp>
#include <boost/spirit/include/classic_core.hpp>
#include <boost/spirit/include/classic_confix.hpp>
#include <boost/spirit/include/classic_lists.hpp>
#include <boost/spirit/include/classic_escape_char.hpp>

using namespace BOOST_SPIRIT_CLASSIC_NS;

#endif

namespace meteo {

Header::Header(const vle::devs::DynamicsInit& init,
	       const vle::devs::InitEventList& events,
	       const std::string& filePath) :
    ClimateReader(init, events, filePath)
{ }

Header::~Header()
{ }

void Header::parseColumnHeader(const std::vector < std::string >& fields,
			       const std::string& colName,
			       const std::string& name)
{
    std::vector < std::string >::const_iterator it =
	find(fields.begin(), fields.end(), colName);

    if (it != fields.end()) {
	addColumn(colName, name);
	ClimateReader::addColumn(name,
				 (int)distance(fields.begin(), it));
    } else {
	throw vle::utils::FileError(
	    boost::format(_("[%1%] Meteo: column %2% missing"))
	    % getModelName() % colName);
    }
}

void Header::parseHeader(std::vector < std::string >& fields)
{
    std::string header;

    getline(getFile(), header);
    parseLine(fields, header);
}

void Header::parseLine(std::vector < std::string >& fields,
		       std::string& line)
{
    boost::trim(line);

    rule <> list_csv, list_csv_item;

    list_csv_item =
        !(
	    confix_p('\"', *c_escape_ch_p, '\"')
 	    | +(alnum_p | ch_p('_') | ch_p('-'))
	    | longest_d[real_p | int_p]
	    );

    list_csv =
	list_p(
	    list_csv_item[push_back_a(fields)],
	    +chset_p(columnSeparator().c_str()));

    parse_info <> result = parse (line.c_str(), list_csv);

    for (std::vector < std::string >::iterator it = fields.begin();
	 it != fields.end(); ++it) {
	if ((*it)[0] == '\"' and (*it)[it->size() - 1] == '\"') {
	    *it = it->substr(1, it->size() - 2);
	}
    }
}

} // namespace meteo
