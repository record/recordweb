/**
 * @file Climate.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2009-2011 INRA
 * Copyright (C) 2009-2011 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Climate.hpp>
#include <vle/utils.hpp>

namespace vu = vle::utils;

namespace meteo {

Climate::Climate(const vle::devs::DynamicsInit& init,
                 const vle::devs::InitEventList& events) :
    vle::extension::DifferenceEquation::Multiple(init, events),
    deltaTime(0), beginTime(0)
{
    if (events.exist("begin_date")) {
        beginTime = vu::DateTime::toJulianDayNumber(events.getString("begin_date"));
    }
}

Climate::~Climate()
{ }

void Climate::initValue (const vle::devs::Time& time)
{
    if (beginTime != 0) {
        deltaTime = beginTime - time;
    }
}

} // namespace meteo
