/**
 * @file GenericGenerator.hpp
 * @author The RECORD Development Team (INRA)
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2009-2011 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLIMATE_GENERATOR_HPP
#define CLIMATE_GENERATOR_HPP 1

#include "Climate.hpp"
#include <map>
#include <string>
#include <vector>
#include <fstream>
#include <vle/extension/DifferenceEquation.hpp>
#include <vle/utils/Path.hpp>

#include <boost/algorithm/string.hpp>
#include <boost/version.hpp>

#if BOOST_VERSION < 103600
#include <boost/spirit/utility/chset.hpp>
#include <boost/spirit/core.hpp>
#include <boost/spirit/utility/confix.hpp>
#include <boost/spirit/utility/lists.hpp>
#include <boost/spirit/utility/escape_char.hpp>
#include <boost/spirit.hpp>
#include <boost/spirit/actor.hpp>


using namespace boost::spirit;

#else

#include <boost/spirit/include/classic_chset.hpp>
#include <boost/spirit/include/classic_core.hpp>
#include <boost/spirit/include/classic_confix.hpp>
#include <boost/spirit/include/classic_lists.hpp>
#include <boost/spirit/include/classic_escape_char.hpp>
#include <boost/spirit.hpp>
#include <boost/spirit/actor.hpp>


using namespace BOOST_SPIRIT_CLASSIC_NS;

#endif

namespace meteo {

/**
 * @brief Class providing a a static function for parsing
 * a "sta" file composed of Item, each Item is composed
 * of 1 line of comment and a matrix of nbrow * nbcol coefficients
 */
class ParserItemSta
{
public:
    /**
     * @brief Method to parse a Item o a sta file
     * @param stream the stream of the file to parse
     * (current line is the beginning of the Item)
     * @param nbrow the number of row of the coefficient matrix
     * @param nbcol the number of column of the coefficient matrix
     * @return a vector containing the concatenated lines of the matrix
     */
    static std::vector<double> parseItem(std::ifstream& stream,
        unsigned int nbrow, unsigned int nbcol);
};

/**
 * @brief A class that represents a set of Fourier series
 */
class Fourier
{
public:
    /**
     * @brief Fourier constructor
     * @param stream the file stream in order to parse Fourier coefficients
     * @param nbrow number of rows in the coefficients matrix
     * @param nbcol number of columns (number of Fourier series)
     * in the coefficient matrix
     * @param integrationPeriod period for Fourier integration
     */
    Fourier(std::ifstream& stream, unsigned int nbrow, unsigned int nbcol,
            int integrationPeriod);
    /**
     *@brief Fourier Destructor
     */
    Fourier();
    /**
     * @brief Get the value of the Fourier
     * @param the day in the year
     * @return the value of the Fourier
     */
    double getValue(unsigned int dayOfYear);
    /**
     * @brief PI value
     */
    static const double PI = 3.141592;

private:
    /**
     * @brief Fourier coefficients (for cosinus)
     */
    double a[10];
    /**
     * @brief Fourier coefficients (for sinus)
     */
    double b[10];
    /**
     * @brief number of Fourier series
     */
    int n;
    /**
     * @brief period for Fourier integration
     */
    int T;
};
/**
 * @brief Climate generator based on Stochastic Weather Simulation Model.
 * Author : M.Semenov and updates from F. Garcia, MJC, R. Trépos.
 * Date :  18 August 1994, 20 aout 96, 31/5/98, 21/1/99, 29/2/2000,
 * 29/10/2009.
 * Provide daily outputs :
 * - ETP : evapotranspiration (mm)
 * - RG : global radiation (MJ/m^2)
 * - Rain : precipitation (mm)
 * - Tmin : minimal temperature (C)
 * - Tmax : maximal temperature (C)
 * - Tmoy : mean temperature (C)
 */
class ClimateGenerator: public meteo::Climate
{
public:
    ClimateGenerator(const vle::devs::DynamicsInit& init,
                     const vle::devs::InitEventList& events);

    void compute(const vle::devs::Time& julianDay);

    void initValue(const vle::devs::Time& julianDay);

    virtual ~ClimateGenerator();

private:
    /**
     * @brief the set of Fourier series  for the climate parameters
     */
    std::map<std::string,Fourier> mfouriers;
    /**
     * @brief regression parameter alpha for computing evapo-transpiration (ETP)
     */
    double malpha;
    /**
     * @brief regression parameter beta for computing evapo-transpiration (ETP)
     */
    double mbeta;
    /**
     * @brief regression parameter gamma for computing evapo-transpiration (ETP)
     */
    double mgamma;
    /**
     * @brief the current remaining days of the climate serie (wet or dry)
     */
    unsigned int mserialength;
    /**
     * @brief the current value of ETP
     */
    double ETP_wo;
    /**
     * @brief the current residual computed for minimal temperature
     */
    double Resid_min;
    /**
     * @brief the current residual computed for maximal temperature
     */
    double Resid_max;
    /**
     * @brief the current residual computed for radiation
     */
    double Resid_rad;
    /**
     * @brief the latitude of the site location
     */
    double mlatitude;
    /**
     * @brief mean size of a wet or dry climatic serie (?)
     */
    int mendserial;
    /**
     * @brief the current climatic serie (wet or dry)
     */
    bool mwet;
    /**
     * @brief value of the mean for normal distribution
     */
    double mnormalmean;
    /**
     * @brief value of the standard deviation for normal distribution
     */
    double mnormalsigma;
    /**
     * @brief period for Fourier integration
     */
    int mFourierIntegrationPeriod;
    /**
     * @brief Parse the file containing Fourier coefficient values
     * for all the Fourier series and the parameters for ETP regression
     * @param the path to the file
     */
    void parseFile(const std::string& filepath);
    /**
     * @brief Compute climatic serie (if new one is required)
     * and one day of climatic data
     * @param dayOfYear current day in the year
     */
    void weatherGen(unsigned int dayOfYear);

    /**
     * @brief Compute the length of a wet climatic serie
     * @param dayOfYear current day in the year
     */
    int wetSeries(unsigned int dayOfYear);
    /**
     * @brief Compute the length of a dry climatic serie
     * @param dayOfYear current day in the year
     */
    int drySeries(unsigned int dayOfYear);
    /**
     * @brief Compute and update climatic variables for one day
     * of simulation
     * @param dayOfYear current day in the year
     */
    void weatherOutput(unsigned int dayOfYear);

    /**
     * @brief Compute the minimal temperature for one day
     * of simulation
     * @param dayOfYear current day in the year
     * @return minimal temperature
     */
    double  minTem(unsigned int dayOfYear);

    /**
     * @brief Compute the maximal temperature for one day
     * of simulation
     * @param dayOfYear current day in the year
     * @return maximal temperature
     */
    double  maxTem(unsigned int dayOfYear);
    /**
     * @brief Compute the precipitation for one day
     * of simulation
     * @param dayOfYear current day in the year
     * @return precipitation
     */
    double precipitation(unsigned int dayOfYear);
    /**
     * @brief Compute the radiation for one day
     * of simulation
     * @param dayOfYear current day in the year
     * @return radiation
     */
    double  rad(unsigned int dayOfYear);
    /**
     * @brief Compute the min and max radiation for one day
     * of simulation on the site
     * @param dayOfYear current day in the year
     * @return min and max values for the radiation
     */

    std::pair<double,double> maxMinRadiation(unsigned int dayOfYear);

    /**
     * @brief Random pick of a choice into integers from 1 to n according
     * the probability distribution given in table p
     * @param n the number of choices
     * @param p the probability distributions of i (1<i<n)
     * @return an integer between 1 and n
     */
    int choice(int n, double p[]);

};

} //namespace meteo

#endif /* CLIMATE_GENERATOR_H */
