/**
 * @file GenericWithHeader.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2009-2011 INRA
 * Copyright (C) 2009-2011 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Header.hpp>

namespace meteo {

class GenericWithHeader : public Header
{
public :
    GenericWithHeader(const vle::devs::DynamicsInit& init,
		      const vle::devs::InitEventList& events) :
	Header(init, events, vle::value::toString(events.get("meteo_file"))),
	mBegin(0),
	mColumnSeparator(" \t\n"),
	mDateFormat(""),
	mYearColumn(-1),
	mMonthColumn(-1),
	mWeekColumn(-1),
	mDayColumn(-1)
    {
        std::vector < std::string > fields;

        parseParameters(events);
        jumpLines(mBegin - 2);
        parseHeader(fields);
        for (std::map < std::string,
                 std::string >::const_iterator it = mMapping.begin();
             it != mMapping.end(); ++it) {
            parseColumnHeader(fields, it->first, it->second);
        }
        createVariables();
    }

    ~GenericWithHeader()
    { }

private:
    void parseParameters(const vle::devs::InitEventList& events)
    {
        if (events.exist("begin")) {
            mBegin = vle::value::toInteger(events.get("begin"));
        }

        if (events.exist("columns")) {
            const vle::value::Map& mapping = events.getMap("columns");
            const vle::value::MapValue& lst = mapping.value();
            for (vle::value::MapValue::const_iterator it = lst.begin();
                 it != lst.end(); ++it) {
                std::string column = it->first;
                std::string varName = vle::value::toString(it->second);

                mMapping[column] = varName;
            }
        }

        if (events.exist("column_separator")) {
            mColumnSeparator = vle::value::toString(
                events.get("column_separator"));
        }
        if (events.exist("date_format")) {
            mDateFormat = vle::value::toString(
                events.get("date_format"));
        }
        if (events.exist("year_column")) {
            mYearColumn = vle::value::toInteger(events.get("year_column"));
        }
        if (events.exist("month_column")) {
            mMonthColumn = vle::value::toInteger(
                events.get("month_column"));
        }
        if (events.exist("week_column")) {
            mWeekColumn = vle::value::toInteger(events.get("week_column"));
        }
        if (events.exist("day_column")) {
            mDayColumn = vle::value::toInteger(events.get("day_column"));
        }
    }

/*  - - - - - - - - - - - - - --ooOoo-- - - - - - - - - - - -  */

    std::string columnSeparator() const
    { return mColumnSeparator; }

    std::string dateFormat() const
    { return mDateFormat; }

    int yearColumn() const
    { return mYearColumn; }

    int monthColumn() const
    {  return mMonthColumn; }

    int weekColumn() const
    { return mWeekColumn; }

    int dayColumn() const
    { return mDayColumn; }

/*  - - - - - - - - - - - - - --ooOoo-- - - - - - - - - - - -  */

    int mBegin;
    std::map < std::string, std::string > mMapping;
    std::string mColumnSeparator;
    std::string mDateFormat;
    int mYearColumn;
    int mMonthColumn;
    int mWeekColumn;
    int mDayColumn;
};

DECLARE_NAMED_DYNAMICS(GenericWithHeader, GenericWithHeader);

} // namespace meteo
