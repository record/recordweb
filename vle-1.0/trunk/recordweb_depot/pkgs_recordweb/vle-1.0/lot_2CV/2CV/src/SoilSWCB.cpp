/**
 * @file src/SoilSWCB.cpp
 * @author The RECORD Development Team (INRA)
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/extension/DifferenceEquation.hpp>// correspond a l'extension DEVS utilise ici
#include <vle/utils/Trace.hpp>
#include <vle/utils/i18n.hpp>

// raccourcis de nommage des namespaces frequement utilises
namespace ve = vle::extension;
namespace vd = vle::devs;
namespace vv = vle::value;

//definition du namespace de la classe du modele
namespace record {
namespace cv {

class SoilSWCB: public ve::DifferenceEquation::Multiple {
public:
    SoilSWCB(const vd::DynamicsInit& atom, const vd::InitEventList& events) :
        ve::DifferenceEquation::Multiple(atom, events)
    {
        // Variables d'etat gerees par ce composant
        FAW1 = createVar("FAW1");
        FAW2 = createVar("FAW2");
        FAW3 = createVar("FAW3");
        FAW4 = createVar("FAW4");

        TH1 = createVar("TH1");
        TH2 = createVar("TH2");
        TH3 = createVar("TH3");
        TH4 = createVar("TH4");

        Q1 = createVar("Q1");
        Q2 = createVar("Q2");
        Q3 = createVar("Q3");
        Q4 = createVar("Q4");

        Qx1 = createVar("Qx1");
        Qx2 = createVar("Qx2");
        Qx3 = createVar("Qx3");
        Qx4 = createVar("Qx4");

        S1 = createVar("S1");
        S2 = createVar("S2");
        S3 = createVar("S3");
        S4 = createVar("S4");

        Drainage1 = createVar("Drainage1");
        Drainage2 = createVar("Drainage2");
        Drainage3 = createVar("Drainage3");
        Drainage4 = createVar("Drainage4");

        FAW12 = createVar("FAW12");
        FAW123 = createVar("FAW123");

        // Variables  gerees par un autre  composant
        RootDepth = createSync("RootDepth");
        Infiltration = createSync("Infiltration");
        AE1 = createSync("AE1");
        AE2 = createSync("AE2");
        AT2 = createSync("AT2");
        AT3 = createSync("AT3");

        // Lecture des parametres
        soilDepth = vv::toDouble(events.get("soilDepth"));
        FC12 = vv::toDouble(events.get("FC12"));
        FC34 = vv::toDouble(events.get("FC34"));
        WP12 = vv::toDouble(events.get("WP12"));
        WP34 = vv::toDouble(events.get("WP34"));
    }

    virtual ~SoilSWCB() {}

    virtual void compute(const vd::Time& /*time*/)
    {
        TH1 = 20;
        TH2 = 280;
        TH3 = std::max(0., RootDepth() - (TH1() + TH2()));
        TH4 = std::max(0., soilDepth - (TH1() + TH2() + TH3()));

        double S3temp = S3(-1);
        double S4temp = S4(-1);
        if (TH4() < TH4(-1)) {
            S4temp = S4(-1) * TH4() / TH4(-1);
            S3temp = S3(-1) + S4(-1) - S4temp;
        }
        if (TH4() > TH4(-1)) {
            S3temp = S3(-1) * TH3() / TH3(-1);
            S4temp = S4(-1) + S3(-1) - S3temp;
        }

        Q1 = std::max(0., S1(-1) - WP12 * TH1());
        Q2 = std::max(0., S2(-1) - WP12 * TH2());
        Q3 = std::max(0., S3temp - WP34 * TH3());
        Q4 = std::max(0., S4temp - WP34 * TH4());

        Qx1 = (FC12 - WP12) * TH1();
        Qx2 = (FC12 - WP12) * TH2();
        Qx3 = (FC34 - WP34) * TH3();
        Qx4 = (FC34 - WP34) * TH4();

        FAW1 = Q1() / Qx1();
        FAW2 = Q2() / Qx2();
        if (Qx3() > 0) {
            FAW3 = Q3() / Qx3();
        } else {
            FAW3 = 0;
        }
        if (Qx4() > 0) {
            FAW4 = Q4() / Qx4();
        } else {
            FAW4 = 0;
        }

        double Stemp;
        Stemp = S1(-1) + Infiltration() - AE1();
        Drainage1 = std::max(0., Stemp - (FC12 * TH1()));
        S1 = std::max(0., Stemp - Drainage1());

        Stemp = S2(-1) + Drainage1() - AE2() - AT2();
        Drainage2 = std::max(0., Stemp - (FC12 * TH2()));
        S2 = std::max(0., Stemp - Drainage2());

        Stemp = S3temp + Drainage2() - AT3();
        Drainage3 = std::max(0., Stemp - (FC34 * TH3()));
        S3 = std::max(0., Stemp - Drainage3());

        Stemp = S4temp + Drainage3();
        Drainage4 = std::max(0., Stemp - (FC34 * TH4()));
        S4 = std::max(0., Stemp - Drainage4());

        FAW12 = (Q1() + Q2()) / (Qx1() + Qx2());
        FAW123 = (Q1() + Q2() + Q3()) / (Qx1() + Qx2() + Qx3());
    }

    virtual void initValue(const vd::Time& /* time */)
    {
        FAW1 = 0.0;
        FAW2 = 0.0;
        FAW3 = 0.0;
        FAW4 = 0.0;

        TH1 = 20.0;
        TH2 = 280.0;
        TH3 = std::max(0., RootDepth() - (TH1() + TH2()));
        TH4 = std::max(0., soilDepth - (TH1() + TH2() + TH3()));

        Q1 = 0.0;
        Q2 = 0.0;
        Q3 = 0.0;
        Q4 = 0.0;

        Qx1 = (FC12 - WP12) * TH1();
        Qx2 = (FC12 - WP12) * TH2();
        Qx3 = (FC34 - WP34) * TH3();
        Qx4 = (FC34 - WP34) * TH4();

        S1 = WP12 * TH1();
        S2 = WP12 * TH2();
        S3 = WP34 * TH3();
        S4 = WP34 * TH4();

        Drainage1 = 0.0;
        Drainage2 = 0.0;
        Drainage3 = 0.0;
        Drainage4 = 0.0;

        FAW12 = 0.0;
        FAW123 = 0.0;
    }

private:
    //Variables d'etat
    Var FAW1;
    Var FAW2;
    Var FAW3;
    Var FAW4;

    Var TH1;
    Var TH2;
    Var TH3;
    Var TH4;

    Var Q1;
    Var Q2;
    Var Q3;
    Var Q4;

    Var Qx1;
    Var Qx2;
    Var Qx3;
    Var Qx4;

    Var S1;
    Var S2;
    Var S3;
    Var S4;

    Var Drainage1;
    Var Drainage2;
    Var Drainage3;
    Var Drainage4;

    Var FAW12;
    Var FAW123;

    //Entrées
    Sync RootDepth;
    Sync Infiltration;
    Sync AE1;
    Sync AE2;
    Sync AT2;
    Sync AT3;

    //Parametres du modele
    double soilDepth;
    double FC12;
    double FC34;
    double WP12;
    double WP34;

};
}
}//namespaces
DECLARE_DYNAMICS( record::cv::SoilSWCB); // balise specifique VLE

