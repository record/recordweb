/**
 * @file src/CropDvtStade.cpp
 * @author The RECORD Development Team (INRA)
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CROPDVTSTADE_HPP_
#define CROPDVTSTADE_HPP_

namespace record {
namespace cv {

/**
 * @brief Definition commune des differents etats phenologique de la plante
 * @enum State
 */
enum State {
    SOWING, /*!< Semis <==> 0. */
    EMERGENCE, /*!< Emergence <==> 1. */
    MAX_LAI, /*!< LAI Maximum <==> 2. */
    FLOWERING, /*!< Floraison <==> 3. */
    CRITICAL_GRAIN_ABORTION, /*!< Etape critique pour l'avortement des grains <==> 4. */
    LEAF_SENESCENCE, /*!< Senescence des feuilles <==> 5. */
    MATURITY, /*!< Maturité <==> 6. */
    BARE_SOIL /*!< Sol nu <==> 7. */
};

}
}//namespaces

#endif /* CROPDVTSTADE_HPP_ */
