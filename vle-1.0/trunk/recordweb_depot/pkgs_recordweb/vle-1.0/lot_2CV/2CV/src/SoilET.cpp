/**
 * @file src/SoilET.cpp
 * @author The RECORD Development Team (INRA)
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/extension/DifferenceEquation.hpp>// correspond a l'extension DEVS utilise ici

// raccourcis de nommage des namespaces frequement utilises
namespace ve = vle::extension;
namespace vd = vle::devs;
namespace vv = vle::value;

//definition du namespace de la classe du modele
namespace record {
namespace cv {

class SoilET: public ve::DifferenceEquation::Multiple {
public:
    SoilET(const vd::DynamicsInit& atom, const vd::InitEventList& events) :
        ve::DifferenceEquation::Multiple(atom, events)
    {
        // Variables d'etat gerees par ce composant
        ATPT = createVar("ATPT");
        AT = createVar("AT");
        AT2 = createVar("AT2");
        AT3 = createVar("AT3");
        PT = createVar("PT");
        AE = createVar("AE");
        AE1 = createVar("AE1");
        AE2 = createVar("AE2");
        PE = createVar("PE");

        // Variables  gerees par un autre  composant
        ETP = createSync("ETP");
        ALAI = createSync("ALAI");
        FAW1 = createNosync("FAW1");
        FAW2 = createNosync("FAW2");
        FAW3 = createNosync("FAW3");
        TH1 = createNosync("TH1");
        TH2 = createNosync("TH2");
        TH3 = createNosync("TH3");
        Q1 = createNosync("Q1");
        Q2 = createNosync("Q2");
        Q3 = createNosync("Q3");
        Qx2 = createNosync("Qx2");
        Qx3 = createNosync("Qx3");

        // Lecture des valeurs de parametres dans les conditions du vpz
        //ces parametres ont une valeur par defaut utilise si la condition n'est pas definie
        xtinc = (events.exist("xtinc")) ? vv::toDouble(events.get("xtinc")) : 0.7;
        p1Evap = (events.exist("p1Evap")) ? vv::toDouble(events.get("p1Evap")) : 0.075;
        p2Evap = (events.exist("p2Evap")) ? vv::toDouble(events.get("p2Evap")) : 1.2;
        p3Evap = (events.exist("p3Evap")) ? vv::toDouble(events.get("p3Evap")) : 0.3;
        pke = (events.exist("pke")) ? vv::toDouble(events.get("pke")) : 1.25;
        r1Tran = (events.exist("r1Tran")) ? vv::toDouble(events.get("r1Tran")) : 0.4;
        r2Tran = (events.exist("r2Tran")) ? vv::toDouble(events.get("r2Tran")) : 0.6;
    }

    virtual ~SoilET() {}

    virtual void compute(const vd::Time& /*time*/)
    {
        PE = ETP() * std::exp(-xtinc * ALAI(-1));
        PT = ETP() - PE();

        if (FAW1(-1) > 0) {
            AE = PE() * (p1Evap + p2Evap * exp(-p3Evap / FAW1(-1)));
        } else {
            AE = 0;
        }

        double f1 = FAW1(-1) * (1 - std::exp(-pke * TH1(-1)));
        double f2 = FAW2(-1) * (std::exp(-pke * TH1(-1)) - std::exp(
                -pke * TH2(-1)));

        if (f1 + f2 > 0) {
            AE1 = std::min(Q1(-1), AE() * f1 / (f1 + f2));
            AE2 = std::min(Q2(-1), AE() * f2 / (f1 + f2));
        } else {
            AE1 = 0;
            AE2 = 0;
        }

        double FAW23 = 0;

        if (Qx2(-1) + Qx3(-1) > 0) {
            FAW23 = (Q2(-1) + Q3(-1)) / (Qx2(-1) + Qx3(-1));
        }

        AT = std::min(Q2(-1) + Q3(-1), PT() * reduc(FAW23, r1Tran, r2Tran));

        if (PT() > 0) {
            ATPT = AT() / PT();
        } else {
            ATPT = 1;
        }

        double tempAT2 = std::min(Q2(-1), AT() * TH2(-1) / (TH2(-1) + TH3(-1)));
        double tempAT3 = std::min(Q3(-1), AT() * TH3(-1) / (TH2(-1) + TH3(-1)));

        if (Q2(-1) < AT() * TH2(-1) / (TH2(-1) + TH3(-1))) {
            AT3 = tempAT3 + AT() * TH2(-1) / (TH2(-1) + TH3(-1)) - Q2(-1);
        } else {
            AT3 = tempAT3;
        }

        if (Q3(-1) < AT() * TH3(-1) / (TH2(-1) + TH3(-1))) {
            AT2 = tempAT2 + AT() * TH3(-1) / (TH2(-1) + TH3(-1)) - Q3(-1);
        } else {
            AT2 = tempAT2;
        }
    }

    virtual void initValue(const vd::Time& /* time */)
    {
        ATPT = 0.0;
        AT = 0.0;
        AT2 = 0.0;
        AT3 = 0.0;
        AE = 0.0;
        AE1 = 0.0;
        AE2 = 0.0;
        PE = ETP();
        PT = 0.0;
    }

private:
    //Variables d'etat
    Var ATPT;
    Var AT;
    Var AT2;
    Var AT3;
    Var PT;
    Var AE;
    Var AE1;
    Var AE2;
    Var PE;

    //Entrées
    Sync ETP;
    Sync ALAI;
    Nosync FAW1;
    Nosync FAW2;
    Nosync FAW3;
    Nosync TH1;
    Nosync TH2;
    Nosync TH3;
    Nosync Q1;
    Nosync Q2;
    Nosync Q3;
    Nosync Qx2;
    Nosync Qx3;

    //Parametres du modele
    double xtinc;
    double p1Evap;
    double p2Evap;
    double p3Evap;
    double pke;
    double r1Tran;
    double r2Tran;

    //fonction locale
    double reduc(double x, double p1, double p2)
    {
        if (x < p2 - p1) {
            return 0;
        } else {
            if (x > p2) {
                return 1;
            } else {
                return (p1 - p2 + x) / p1;
            }
        }
    }

};
}
}//namespaces
DECLARE_DYNAMICS( record::cv::SoilET); // balise specifique VLE

