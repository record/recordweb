/**
 * @file src/CropTT_TT.cpp
 * @author The RECORD Development Team (INRA)
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/extension/DifferenceEquation.hpp>// Correspond a l'extension DEVS utilise ici
#include <CropDvtStade.hpp>// Definition commune des differents etats phenologique de la plante

// Raccourcis de nommage des namespaces frequement utilises
namespace ve = vle::extension;
namespace vd = vle::devs;
namespace vv = vle::value;

// Definition du namespace de la classe du modele
namespace record {
namespace cv {

/**
 * @brief Calcul de l'avancement du temps thermique.
 * Utilise l'extension DEVS DifferenceEquation::Multiple.
 *
 * Le modèle calcul à chaque pas de temps les variables d'état :
 * - TT : temps thermique (°C.j)
 * - DvtStade : le stade phenologique de la plante (cf CropDvtStade.hpp) (-)
 * - tt_em_mat : le temps thermique entre emergence et maturité (°C.j)
 *
 * A partir des données d'entrée :
 * - Tmin : la température minimale journalière (°C)
 * - Tmax : la température maximale journalière (°C)
 *
 * Et des paramètres :
 * - tt_Sowing_Emergence : temps thermique entre les stades phenologiques SOWING et EMERGENCE (resp 0 & 1) - [TT] <==> °C.jour
 * - tt_Emergence_MaxLAI : temps thermique entre les stades phenologiques EMERGENCE et MAX_LAI (resp 1 & 2) - [TT] <==> °C.jour
 * - tt_MaxLAI_Flowering : temps thermique entre les stades phenologiques MAX_LAI et FLOWERING (resp 2 & 3) - [TT] <==> °C.jour
 * - tt_Flowering_GrainAbort : temps thermique entre les stades phenologiques FLOWERING et CRITICAL_GRAIN_ABORTION (resp 3 & 4) - [TT] <==> °C.jour
 * - tt_GrainAbort_LeafSen : temps thermique entre les stades phenologiques CRITICAL_GRAIN_ABORTION et LEAF_SENESCENCE (resp 4 & 5) - [TT] <==> °C.jour
 * - tt_LeafSen_Maturity : temps thermique entre les stades phenologiques LEAF_SENESCENCE et MATURITY (resp 5 & 6) - [TT] <==> °C.jour
 */
class CropTT_TT: public ve::DifferenceEquation::Multiple {
public:
    /**
     * @brief Constructeur de la classe du modèle.
     * C'est ici que se font les enregistrements des variables d'état (Var)
     * et des variables d'entrées (Sync & Nosync) dans le moteur de simulation VLE.
     * C'est aussi ici que l'ont attribut leur valeurs aux paramètres du modèle à
     * partir des conditions expérimentales définies dans le VPZ
     *
     * @param events liste des evenements provenant des conditions expérimentales du VPZ
     * @param init ?
     */
    CropTT_TT(const vd::DynamicsInit& init, const vd::InitEventList& events) :
        ve::DifferenceEquation::Multiple(init, events)
    {
        // Variables gerees par un autre composant
        Tmin = createSync("Tmin");
        Tmax = createSync("Tmax");
        // Variables d'etat gerees par ce composant
        DvtStade = createVar("DvtStade");
        TT = createVar("TT");
        tt_em_mat = createVar("tt_em_mat");

        // Lecture des valeurs de parametres dans les conditions du vpz
        tt_Emergence_MaxLAI = vv::toDouble(events.get("tt_Emergence_MaxLAI"));
        tt_LeafSen_Maturity = vv::toDouble(events.get("tt_LeafSen_Maturity"));
        //ces parametres ont une valeur par defaut utilise si la condition n'est pas definie
        tt_Sowing_Emergence = (events.exist("tt_Sowing_Emergence")) ?
            vv::toDouble(events.get("tt_Sowing_Emergence")) : 80.0;
        tt_MaxLAI_Flowering = (events.exist("tt_MaxLAI_Flowering")) ?
            vv::toDouble(events.get("tt_MaxLAI_Flowering")) : 90.0;
        tt_Flowering_GrainAbort = (events.exist("tt_Flowering_GrainAbort")) ?
            vv::toDouble(events.get("tt_Flowering_GrainAbort")) : 250.0;
        tt_GrainAbort_LeafSen = (events.exist("tt_GrainAbort_LeafSen")) ?
            vv::toDouble(events.get("tt_GrainAbort_LeafSen")) : 245.0;
    }

    /**
     * @brief Destructeur de la classe du modèle.
    **/
    virtual ~CropTT_TT() {}

    /**
     * @brief Methode de calcul effectuée à chaque pas de temps
     * @param time la date du pas de temps courant
     */
    virtual void compute(const vd::Time& /*time*/)
    {
        DvtStade = DvtStade(-1);
        tt_em_mat = tt_em_mat(-1);

        switch ((int) DvtStade()) {
            case BARE_SOIL:
                TT = 0.0;
                break;
            case MATURITY:
                TT = TT(-1);
                break;
            case SOWING:
            case EMERGENCE:
            case MAX_LAI:
            case FLOWERING:
            case CRITICAL_GRAIN_ABORTION:
            case LEAF_SENESCENCE:
                TT = TT(-1) + std::max(0.,
                        (Tmin() + std::min(30., Tmax())) / 2 - 6);
                break;
        }
    }

    /**
     * @brief Methode d'initialisation pour les variables d'état du modèle
     * @param time la date du pas de temps courant
     */
    virtual void initValue(const vd::Time& /*time*/)
    {
        DvtStade = BARE_SOIL;
        TT = 0;
        tt_em_mat = tt_Emergence_MaxLAI + tt_MaxLAI_Flowering
                + tt_Flowering_GrainAbort + tt_GrainAbort_LeafSen
                + tt_LeafSen_Maturity;
    }

private:
    //Variables d'etat
    /**
     * @brief temps thermique (°C.j)
     */
    Var TT;
    /**
     * @brief stades phénologique de la plante (cf CropDvtStade.hpp) {0:SOWING - 1:EMERGENCE - 2:MAX_LAI - 3:FLOWERING - 4:CRITICAL_GRAIN_ABORTION - 5:LEAF_SENESCENCE - 6:MATURITY
     */
    Var DvtStade;
    /**
     * @brief temps thermique entre emergence et maturité (°C.j)
     */
    Var tt_em_mat;

    //Entrees
    /**
     * @brief température journalière minimale (°C)
     */
    Sync Tmin;
    /**
     * @brief température journalière maximale (°C)
     */
    Sync Tmax;

    //Parametres du modele
    /**
     * @brief temps thermique entre les stades : SOWING et EMERGENCE (respectivement 0 & 1) (°C.j)
     */
    double tt_Sowing_Emergence;
    /**
     * @brief temps thermique entre les stades : EMERGENCE et MAX_LAI (respectivement 1 & 2) (°C.j)
     */
    double tt_Emergence_MaxLAI;
    /**
     * @brief temps thermique entre les stades : MAX_LAI et FLOWERING (respectivement 2 & 3) (°C.j)
     */
    double tt_MaxLAI_Flowering;
    /**
     * @brief temps thermique entre les stades : FLOWERING et CRITICAL_GRAIN_ABORTION (respectivement 3 & 4) (°C.j)
     */
    double tt_Flowering_GrainAbort;
    /**
     * @brief temps thermique entre les stades : CRITICAL_GRAIN_ABORTION et LEAF_SENESCENCE (respectivement 4 & 5) (°C.j)
     */
    double tt_GrainAbort_LeafSen;
    /**
     * @brief temps thermique entre les stades : LEAF_SENESCENCE et MATURITY (respectivement 5 & 6) (°C.j)
     */
    double tt_LeafSen_Maturity;

};

}
}//namespaces

DECLARE_DYNAMICS( record::cv::CropTT_TT);
