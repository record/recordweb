/**
 * @file src/CropAGB.cpp
 * @author The RECORD Development Team (INRA)
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/extension/DifferenceEquation.hpp>// Correspond a l'extension DEVS utilise ici
#include <CropDvtStade.hpp>// Definition commune des differents etats phenologique de la plante

// Raccourcis de nommage des namespaces frequement utilises
namespace ve = vle::extension;
namespace vd = vle::devs;
namespace vv = vle::value;

// Definition du namespace de la classe du modele
namespace record {
namespace cv {

/**
 * @brief Croissance de la biomasse aerienne totale et de grain de la plante
 * par conversion de la lumière intercepté par les feuilles et utilisation d'un indice de recolte
 * Utilise l'extension DEVS DifferenceEquation::Multiple.
 * #include <CropDvtStade.hpp>
 *
 * Le modèle calcul à chaque pas de temps les variables d'état :
 * - AGBiomass : la biomasse aerienne totale de la plante. (g/m²)
 * - Yield : la biomasse de grain de la plante. (g/m²)
 * - HIpot : indice de recolte potentiel (-)
 * - HI : indice de recolte actuel (-)
 * - dayCount : nombre de jour depuis l'emergence (j)
 *
 * A partir des données d'entrée :
 * - Rad : la radiation solaire journalière totale (MJ/m²)
 * - ALAI : LAI actif (m²/m²)
 * - ATPT : le rapport transpiration réelle / potentielle utilisé comme indice de stress (-)
 * - DvtStade : le stade phenologique de la plante (cf CropDvtStade.hpp) (-)
 *
 * Et des paramètres :
 * - xtinc : le coefficient d'extinction de la lumière à travers les feuilles (-)
 * - Rue1 : RUE avant debut de senescence rapide des feuilles. (g/MJ)
 * - Rue2 : RUE après debut de senescence rapide des feuilles. (g/MJ)
 * - r1Rue : effet du rapport ATPT sur la croissance en biomasse (-)
 * - r2Rue : effet du rapport ATPT sur la croissance en biomasse (-)
 * - hiMax : indice de recolte maximal (-)
 * - r1hi : effet du rapport ATPT sur l'indice de recolte (-)
 * - r2hi : effet du rapport ATPT sur l'indice de recolte (-)
 * - rateHI : croissance journaliere maximale du HI(/j)
 */
class CropAGB: public ve::DifferenceEquation::Multiple {
public:
    /**
     * @brief Constructeur de la classe du modèle.
     * C'est ici que se font les enregistrements des variables d'état (Var)
     * et des variables d'entrées (Sync & Nosync) dans le moteur de simulation VLE.
     * C'est aussi ici que l'ont attribut leur valeurs aux paramètres du modèle à
     * partir des conditions expérimentales définies dans le VPZ
     *
     * @param events liste des evenements provenant des conditions expérimentales du VPZ
     * @param atom ?
     */
    CropAGB(const vd::DynamicsInit& init, const vd::InitEventList& events) :
        ve::DifferenceEquation::Multiple(init, events)
    {
        // Variables d'etat gerees par ce composant
        AGBiomass = createVar("AGBiomass");
        Yield = createVar("Yield");
        HIpot = createVar("HIpot");
        HI = createVar("HI");
        dayCount = createVar("dayCount");

        // Variables d'entree gerees par un autre composant
        Rad = createSync("Rad");
        ALAI = createSync("ALAI");
        ATPT = createNosync("ATPT");
        DvtStade = createSync("DvtStade");

        // Lecture des valeurs de parametres dans les conditions du vpz
        //ces parametres ont une valeur par defaut utilise si la condition n'est pas definie
        xtinc = (events.exist("xtinc")) ? vv::toDouble(events.get("xtinc")) : 0.7;
        Rue1 = (events.exist("Rue1")) ? vv::toDouble(events.get("Rue1")) : 2.8;
        Rue2 = (events.exist("Rue2")) ? vv::toDouble(events.get("Rue2")) : 1.5;
        r1Rue = (events.exist("r1Rue")) ? vv::toDouble(events.get("r1Rue")) : 1.0;
        r2Rue = (events.exist("r2Rue")) ? vv::toDouble(events.get("r2Rue")) : 1.0;
        hiMax = (events.exist("hiMax")) ? vv::toDouble(events.get("hiMax")) : 0.55;
        r1hi = (events.exist("r1hi")) ? vv::toDouble(events.get("r1hi")) : 0.6;
        r2hi = (events.exist("r2hi")) ? vv::toDouble(events.get("r2hi")) : 0.8;
        rateHI = (events.exist("rateHI")) ? vv::toDouble(events.get("rateHI")) : 0.015;
    }

    /**
     * @brief Destructeur de la classe du modèle.
    **/
    virtual ~CropAGB() { };

    /**
     * @brief Methode de calcul effectuée à chaque pas de temps
     * @param time la date du pas de temps courant
     */
    virtual void compute(const vd::Time& /*time*/)
    {
        if (DvtStade() == BARE_SOIL) {
            AGBiomass = 0;
            Yield = 0.0;
            HI = 0;
            HIpot = 0;
            dayCount = 0;
        } else {
            double Rue = 0;
            switch ((int) DvtStade()) {
                case EMERGENCE:
                case MAX_LAI:
                case FLOWERING:
                case CRITICAL_GRAIN_ABORTION:
                    Rue = Rue1;
                    break;
                case LEAF_SENESCENCE:
                case MATURITY:
                case SOWING:
                    Rue = Rue2;
                    break;
            }
            double dAGB = 0.48 * Rad() * Rue * (1 - exp(-xtinc * ALAI()))
                    * reduc(ATPT(-1), r1Rue, r2Rue);
            AGBiomass = AGBiomass(-1) + dAGB;
            switch ((int) DvtStade()) {
                case SOWING:
                case EMERGENCE:
                    HIpot = 0;
                    dayCount = 0;
                    break;
                case MAX_LAI:
                case FLOWERING:
                    dayCount = dayCount(-1) + 1;
                    HIpot = (HIpot(-1) * dayCount(-1) + hiMax * reduc(ATPT(-1),
                            r1hi, r2hi)) / dayCount();
                    break;
                case CRITICAL_GRAIN_ABORTION:
                case LEAF_SENESCENCE:
                case MATURITY:
                    HIpot = HIpot(-1);
                    dayCount = dayCount(-1);
                    break;
            }
            double dHI = 0;
            double buff = HIpot() - HI(-1);
            switch ((int) DvtStade()) {
                case SOWING:
                case EMERGENCE:
                case MAX_LAI:
                case FLOWERING:
                    dHI = 0;
                    break;
                case CRITICAL_GRAIN_ABORTION:
                case LEAF_SENESCENCE:
                case MATURITY:
                    dHI = std::min(rateHI, buff);
                    break;
            }
            HI = HI(-1) + dHI;
            switch ((int) DvtStade()) {
                case SOWING:
                case EMERGENCE:
                case MAX_LAI:
                case FLOWERING:
                case CRITICAL_GRAIN_ABORTION:
                case LEAF_SENESCENCE:
                    Yield = Yield(-1);
                    break;
                case MATURITY:
                    Yield = HI() * AGBiomass();
                    break;
            }
        }
    }

    /**
     * @brief Methode d'initialisation pour les variables d'état du modèle
     * @param time la date du pas de temps courant
     */
    virtual void initValue(const vd::Time& /*time*/)
    {
        AGBiomass = 0.0;
        Yield = 0.0;
        HIpot = 0.0;
        HI = 0.0;
        dayCount = 0.0;
    }

private:
    //Variables d'etat
    /**
     * @brief la biomasse aerienne totale de la plante. (g/m²)
     */
    Var AGBiomass;
    /**
     * @brief la biomasse de grain de la plante. (g/m²)
     */
    Var Yield;
    /**
     * @brief indice de recolte potentiel (-)
     */
    Var HIpot;
    /**
     * @brief indice de recolte actuel (-)
     */
    Var HI;
    /**
     * @brief nombre de jour depuis l'emergence (j)
     */
    Var dayCount;

    //Entrées
    /**
     * @brief a radiation solaire journalière totale (MJ/m²)
     */
    Sync Rad;
    /**
     * @brief LAI actif (m²/m²)
     */
    Sync ALAI;
    /**
     * @brief le rapport transpiration réelle / potentielle utilisé comme indice de stress (-)
     */
    Nosync ATPT;
    /**
     * @brief le stade phenologique de la plante (cf CropDvtStade.hpp) (-)
     */
    Sync DvtStade;

    //Parametres du modele
    /**
     * @brief le coefficient d'extinction de la lumière à travers les feuilles (-)
     */
    double xtinc;
    /**
     * @brief RUE avant debut de senescence rapide des feuilles. (g/MJ)
     */
    double Rue1;
    /**
     * @brief RUE après debut de senescence rapide des feuilles. (g/MJ)
     */
    double Rue2;
    /**
     * @brief effet du rapport ATPT sur la croissance en biomasse (-)
     */
    double r1Rue;
    /**
     * @brief effet du rapport ATPT sur la croissance en biomasse (-)
     */
    double r2Rue;
    /**
     * @brief indice de recolte maximal (-)
     */
    double hiMax;
    /**
     * @brief effet du rapport ATPT sur l'indice de recolte (-)
     */
    double r1hi;
    /**
     * @brief effet du rapport ATPT sur l'indice de recolte (-)
     */
    double r2hi;
    /**
     * @brief croissance journaliere maximale du HI(/j)
     */
    double rateHI;

    //fonction locale
    /**
     * @brief fonction de reduction utilisée avec l'indice de stress ATPT
     * @param x la valeur du stress
     * @param p1 parametre de controle
     * @param p2 parametre de controle
     * @return valeur de la reduction a appliquer
     */
    double reduc(double x, double p1, double p2)
    {
        if (x < p2 - p1) {
            return 0;
        } else {
            if (x > p2) {
                return 1;
            } else {
                return (p1 - p2 + x) / p1;
            }
        }
    }

};
}
}//namespaces
DECLARE_DYNAMICS(record::cv::CropAGB); // balise specifique VLE

