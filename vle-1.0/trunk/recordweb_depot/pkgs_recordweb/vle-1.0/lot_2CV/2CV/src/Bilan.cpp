/**
 * @file src/Bilan.cpp
 * @author The RECORD Development Team (INRA)
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/extension/fsa/Statechart.hpp> // correspond a l'extension DEVS utilise ici
#include <vle/extension/DifferenceEquation.hpp> // inclus ici car nous utilisons les Var comme evenement d'entree
#include <vle/utils/DateTime.hpp> //pour recuperer la date a partir du moteur de simulation de VLE

// raccourcis de nommage des namespaces frequement utilises
namespace vd = vle::devs;
namespace vv = vle::value;
namespace ve = vle::extension;
namespace ved = vle::extension::DifferenceEquation;

//definition du namespace de la classe du modele
namespace record {
namespace cv {

/**
 * @brief liste des differents etat possibles de l'automate
 * @enum BilanEtat
 */
enum BilanEtat {AVANT_RECOLTE, APRES_RECOLTE};

/**
 * @brief Calcul des bilans par rotation.
 * Utilise l'extension DEVS fsa::Statechart.
 *
 * Le modèle calcul de bilans pour chaque rotation de la culture :
 * - NombreIrrigations : le nombre d'irrigation effectuées durant le cycle (-)
 * - Profit : le profit réalisé sur le cycle de la culture (euros)
 * - EauCoutTotal : le cout total de l'eau d'irrigation (euros)
 * - EauConsommeeTotale : la quantté d'eau consommée pour l'irrigation durant le cycle de la culture (mm)
 * - Rendement : rendement de production de la culture (g/m^2)
 * - DateRecolte : la date de recolte (julian day)
 *
 * A partir des evenements d'entrée :
 * - Harvesting : l'evenement de declenchement de la recolte
 * - Irrigation : l'evenement de declenchement de l'irrigation
 * - Yield : le rendement de la culture à la recolte (g/m^2)
 *
 * Et des paramètres :
 * - coutEau : le cout de l'eau d'irrigation (euros/mm)
 * - prixVente : le prix de vente de la culture (euros * ha / tonne)
 */
class Bilan: public ve::fsa::Statechart {
public:
    /**
     * @brief Constructeur de la classe du modèle.
     * C'est ici que sont définis les états possibles de l'automate, ainsi que
     * les transitions d'un état à un autre, et les réactions aux evenements d'entrée
     * C'est aussi ici que l'ont attribut leur valeurs aux paramètres du modèle à
     * partir des conditions expérimentales définies dans le VPZ
     *
     * @param events liste des evenements provenant des conditions expérimentales du VPZ
     * @param init ?
     */
    Bilan(const vd::DynamicsInit& init, const vd::InitEventList& events) :
        ve::fsa::Statechart(init, events), tmp_irrigation(0), tmp_rec(0),
                mRendement(0), mDateRecolte(""), mEauConsommeeTotale(0),
                mEauCoutTotal(0), mProfit(0), mNombreIrrigations(0)
    {
        //defini la liste des etats pris en compte par l'automate
        states(this) << AVANT_RECOLTE << APRES_RECOLTE;

        //defini les transitions conditionnelles possibles entre etats
        transition(this, AVANT_RECOLTE, APRES_RECOLTE)
                << guard(&Bilan::recolte); // defini la fonction logique utilisee comme condition de declenchement

        //defini la reaction aux evenements d'entree 
        eventInState(this, "Irrigation", &Bilan::inIrrigation) // defini le nom de l'evenement sur le port d'entree et la fonction void a utiliser
                >> AVANT_RECOLTE; //defini l'etat pour lequel la reaction est active

        eventInState(this, "Harvesting", &Bilan::inHarvesting)// defini le nom de l'evenement sur le port d'entree et la fonction void a utiliser
                >> AVANT_RECOLTE; //defini l'etat pour lequel la reaction est active

        eventInState(this, "Yield", &Bilan::inYield)// defini le nom de l'evenement sur le port d'entree et la fonction void a utiliser
                >> AVANT_RECOLTE; //defini l'etat pour lequel la reaction est active

        //defini l'etat initial
        initialState(AVANT_RECOLTE);

        //attribut les valeurs des parametres a partir des conditions du vpz
        mCoutEau = vv::toDouble(events.get("coutEau"));
        mPrixVente = vv::toDouble(events.get("prixVente"));
    }

    //fonction de condition controlant le changement d'etat
    /**
     * @brief Condition controlant le passage de l'état AVANT_RECOLTE à APRES_RECOLTE
     * @return decision de changer d'état
     */
    bool recolte(const vd::Time& /* time */)
    { return !mDateRecolte.empty(); }

    //fonctions "in" de reaction aux evenement d'entree
    /**
     * @brief Réaction à l'evenement d'entrée "Irrigation"
     * @param event evenement d'entrée
     */
    void inIrrigation(const vd::Time& /*time*/, const vd::ExternalEvent* event)
    {
        tmp_irrigation << ved::Var("Irrigation", event); // stockage temporaire de la valeur de l'evenement d'entree
        mEauConsommeeTotale += tmp_irrigation;
        mNombreIrrigations++;
        tmp_irrigation = 0; // remise a zero apres utilisation de la valeur
    }

    /**
     * @brief Réaction à l'evenement d'entrée "Harvesting"
     * @param event evenement d'entrée
     * @param time la date du pas de temps courant
     */
    void inHarvesting(const vd::Time& time, const vd::ExternalEvent* event)
    {
        tmp_rec << ved::Var("Harvesting", event); // stockage temporaire de la valeur de l'evenement d'entree
        if (tmp_rec == 1.0) { // l'evenement d'entree prends 0 ou 1 pour valeurs utilisees comme un boolean
            mDateRecolte = vle::utils::DateTime::toJulianDayNumber(time); //mise a jour de la variable de sortie mDateRecolte
        }
    }

    /**
     * @brief Réaction à l'evenement d'entrée "Yield"
     * @param event evenement d'entrée
     */
    void inYield(const vd::Time& /* time */, const vd::ExternalEvent* event)
    {
        tmp_yield << ved::Var("Yield", event); // stockage temporaire de la valeur de l'evenement d'entree
        mRendement = tmp_yield;
        mEauCoutTotal = mEauConsommeeTotale * mCoutEau;//mise a jour de la variable de sortie mEauCoutTotal
        mProfit = ((mRendement * mPrixVente) / 100.0) - mEauCoutTotal;//mise a jour de la variable de sortie mProfit
    }

    /**
     * @brief Genere un evenement d'observation : calcul l'etat courant du modele a un temps donne et pour un port de sortie specifique
     * @param event evenement d'entrée sur un port nommé
     * @return renvoi la valeur courante de la variable correspondant au port sur lequel l'evenement est arrivé.
     */
    virtual vv::Value* observation(const vd::ObservationEvent& event) const
    {
        if (event.onPort("NombreIrrigations")) {
            return vv::Integer::create(mNombreIrrigations);
        } else if (event.onPort("Profit")) {
            return vv::Double::create(mProfit);
        } else if (event.onPort("EauCoutTotal")) {
            return vv::Double::create(mEauCoutTotal);
        } else if (event.onPort("EauConsommeeTotale")) {
            return vv::Double::create(mEauConsommeeTotale);
        } else if (event.onPort("Rendement")) {
            return vv::Double::create(mRendement);
        } else if (event.onPort("DateRecolte")) {
            if (mDateRecolte == "") {
                return vv::String::create("PasDeRecolte");
            } else {
                return vv::String::create(mDateRecolte);
            }
        } else {
            return 0;
        }
    }

private:
    //variables locales temporaires (utilisee pour les valeurs des evenement d'entree)
    /**
     * @brief quantité d'irrigation reçue. [Irrigation] <==> mm
     */
    double tmp_irrigation;
    /**
     * @brief déclenchement de la recolte. [Harvesting] <==> 0/1 - bool
     */
    double tmp_rec;
    /**
     * @brief production de la culture. [Yield] <==> g/m^2 <==> 10^(-2) tonne / ha
     */
    double tmp_yield;

    //variables des evenements de sortie
    /**
     * @brief rendement. [Yield] <==> g/m^2 <==> 10^(-2) tonne / ha
     */
    double mRendement;
    /**
     * @brief Date de recolte
     */
    std::string mDateRecolte;
    /**
     * @brief Eau consomée. [Irrigation] <==> mm  <==> (1 mm = 10^(-3) m^3 / m^2) <==> 1 l/m^2 <==> 10^4 l/ha
     */
    double mEauConsommeeTotale;
    /**
     * @brief cout de l'eau d'irrigation. [mEauConsommeeTotale]*[mCoutEau] <==> euros
     */
    double mEauCoutTotal;
    /**
     * @brief Profit. [mEauCoutTotal] <==> [mRendement]*[mPrixVente]/100 <==> euros
     */
    double mProfit;
    /**
     * @brief nombre d'irrigation effectuées.
     */
    int mNombreIrrigations;

    //parametres lus dans le vpz
    /**
     * @brief cout de l'eau d'irrigation. euros/[mEauConsommeeTotale] <==> euros/mm
     */
    double mCoutEau;
    /**
     * @brief prix de vente de la culture. euros/[mRendement]*100 <==> euros/(tonne/ha) <==> euros * ha / tonne
     */
    double mPrixVente;

};
}
}//namespace record cv
DECLARE_DYNAMICS(record::cv::Bilan); // balise specifique VLE


