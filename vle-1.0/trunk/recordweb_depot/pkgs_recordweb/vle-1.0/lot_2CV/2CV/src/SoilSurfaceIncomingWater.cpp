/**
 * @file src/SoilSurfaceIncomingWater.cpp
 * @author The RECORD Development Team (INRA)
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/devs/DynamicsDbg.hpp>
#include <vle/extension/DifferenceEquation.hpp>// correspond a l'extension DEVS utilise ici

// raccourcis de nommage des namespaces frequement utilises
namespace ve = vle::extension;
namespace vd = vle::devs;
namespace vv = vle::value;

//definition du namespace de la classe du modele
namespace record {
namespace cv {

class SoilSurfaceIncomingWater: public ve::DifferenceEquation::Multiple {
public:
    SoilSurfaceIncomingWater(const vd::DynamicsInit& atom,
            const vd::InitEventList& events) :
        ve::DifferenceEquation::Multiple(atom, events)
    {
        // Variables d'etat gerees par ce composant
        Irrigation = createVar("Irrigation");
        Infiltration = createVar("Infiltration");
        IncomingRunOff = createVar("IncomingRunOff");
        RunOff = createVar("RunOff");

        // Variables  gerees par un autre  composant
        Rain = createSync("Rain");

        // Lecture des parametres
        //ces parametres ont une valeur par defaut utilise si la condition n'est pas definie
        fracRunOff = (events.exist("fracRunOff")) ? vv::toDouble(events.get("fracRunOff")) : 0;
    }

    virtual ~SoilSurfaceIncomingWater() {}

    virtual void compute(const vd::Time& /*julianDay*/)
    {
        //variables qui peuvent être perturbées
        Irrigation = 0.0;
        IncomingRunOff = 0.0;
        //calcul des autres variables d'etat
        Infiltration = Rain() + Irrigation();
        RunOff = (Rain() + Irrigation() + IncomingRunOff()) * fracRunOff;
        Infiltration = (Rain() + Irrigation() + IncomingRunOff()) * (1 - fracRunOff);
    }

    virtual void initValue(const vd::Time& /* julianDay */)
    {
        Infiltration = 0.0;
        Irrigation = 0.0;
    }

private:
    //Variables d'etat
    Var Irrigation;
    Var Infiltration;
    Var IncomingRunOff;
    Var RunOff;

    //Entrées
    Sync Rain;

    //Parametres du modele
    double fracRunOff;
};
}
}//namespaces
DECLARE_DYNAMICS( record::cv::SoilSurfaceIncomingWater); // balise specifique VLE
DECLARE_NAMED_DYNAMICS_DBG(Dbg,record::cv::SoilSurfaceIncomingWater); // balise specifique VLE


