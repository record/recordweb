/**
 * @file src/CropLAI.cpp
 * @author The RECORD Development Team (INRA)
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/extension/DifferenceEquation.hpp>// Correspond a l'extension DEVS utilise ici
#include <CropDvtStade.hpp>// Definition commune des differents etats phenologique de la plante

// Raccourcis de nommage des namespaces frequement utilises
namespace ve = vle::extension;
namespace vd = vle::devs;
namespace vv = vle::value;

// Definition du namespace de la classe du modele
namespace record {
namespace cv {

/**
 * @brief Croissance et senescence du LAI de la culture basé sur le temps thermique et le stress hydrique
 * Utilise l'extension DEVS DifferenceEquation::Multiple.
 * #include <CropDvtStade.hpp>
 *
 * Le modèle calcul à chaque pas de temps les variables d'état :
 * - lplant : LAI total theorique (m²/m²)
 * - LAI : LAI total de la plante (m²/m²)
 * - ALAI : LAI actif (m²/m²)
 * - FracSen : fraction du LAI senescent (-)
 *
 * A partir des données d'entrée :
 * - TT : temps thermique (°C.j)
 * - DvtStade : le stade phenologique de la plante (cf CropDvtStade.hpp) (-)
 * - tt_em_mat : le temps thermique entre emergence et maturité (°C.j)
 * - ATPT : le rapport transpiration réelle / potentielle utilisé comme indice de stress (-)
 *
 * Et des paramètres :
 * - dens : la densité de semis (plante /m²)
 * - p1logi : paramètre de l'équation logistique du LAI (-)
 * - p2logi : paramètre de l'équation logistique du LAI ((°C.j)^-1)
 * - lai0 : la valeur du LAI à l'emergtence (m²/m²)
 * - r1sf, r2sf : effet du rapport ATPT sur la croissance du LAI (-)
 * - p1sen, p2sen : parametre de la fraction de LAI senescent (-)
 */
class CropLAI: public ve::DifferenceEquation::Multiple {
public:
    /**
     * @brief Constructeur de la classe du modèle.
     * C'est ici que se font les enregistrements des variables d'état (Var)
     * et des variables d'entrées (Sync & Nosync) dans le moteur de simulation VLE.
     * C'est aussi ici que l'ont attribut leur valeurs aux paramètres du modèle à
     * partir des conditions expérimentales définies dans le VPZ
     *
     * @param events liste des evenements provenant des conditions expérimentales du VPZ
     * @param atom ?
     */
    CropLAI(const vd::DynamicsInit& atom, const vd::InitEventList& events) :
        ve::DifferenceEquation::Multiple(atom, events)
    {
        // Variables d'etat gerees par ce composant
        lplant = createVar("lplant");
        LAI = createVar("LAI");
        ALAI = createVar("ALAI");
        FracSen = createVar("FracSen");

        // Variables gerees par un autre composant
        TT = createSync("TT");
        DvtStade = createSync("DvtStade");
        tt_em_mat = createSync("tt_em_mat");
        ATPT = createNosync("ATPT");

        // Lecture des valeurs de parametres dans les conditions du vpz
        dens = vv::toDouble(events.get("dens"));
        // Ces parametres ont une valeur par defaut utilise si la condition n'est pas definie
        p1logi = (events.exist("p1logi")) ? vv::toDouble(events.get("p1logi")) : 0.6847;
        p2logi = (events.exist("p2logi")) ? vv::toDouble(events.get("p2logi")) : 0.01;
        lai0 = (events.exist("lai0")) ? vv::toDouble(events.get("lai0")) : 0.0016;
        r1sf = (events.exist("r1sf")) ? vv::toDouble(events.get("r1sf")) : 0.6;
        r2sf = (events.exist("r2sf")) ? vv::toDouble(events.get("r2sf")) : 0.8;
        p1sen = (events.exist("p1sen")) ? vv::toDouble(events.get("p1sen")) : 0.00161;
        p2sen = (events.exist("p2sen")) ? vv::toDouble(events.get("p2sen")) : 6.0;
    }

    /**
     * @brief Destructeur de la classe du modèle.
    **/
    virtual ~CropLAI() {};

    /**
     * @brief Methode de calcul effectuée à chaque pas de temps
     * @param time la date du pas de temps courant
     */
    virtual void compute(const vd::Time& /*time*/)
    {
        if (DvtStade() == BARE_SOIL) {
            FracSen = 0.0;
            lplant = 0.0;
            LAI = 0.0;
            ALAI = 0.0;
        } else {
            switch ((int) DvtStade()) {
                case SOWING:
                    FracSen = FracSen(-1);
                    break;
                case EMERGENCE:
                case MAX_LAI:
                case FLOWERING:
                case CRITICAL_GRAIN_ABORTION:
                case LEAF_SENESCENCE:
                case MATURITY:
                    FracSen = std::max(
                            0.,
                            std::min(1.,
                                    p1sen * exp(p2sen * TT() / tt_em_mat())));
                    break;
            }
            switch ((int) DvtStade()) {
                case FLOWERING:
                case CRITICAL_GRAIN_ABORTION:
                case LEAF_SENESCENCE:
                case MATURITY:
                case SOWING:
                    lplant = lplant(-1);
                    LAI = LAI(-1);
                    ALAI = (1 - FracSen()) * LAI();
                    break;
                case EMERGENCE:
                case MAX_LAI:
                    double dlplant = 0;
                    lplant = p1logi / (1 + (p1logi / lai0 - 1) * std::exp(
                            -p2logi * TT()));
                    dlplant = lplant() - lplant(-1);
                    if (DvtStade(-1) == SOWING) {
                        LAI = lai0;
                    } else {
                        LAI = LAI(-1) + dens * dlplant * reduc(ATPT(-1), r1sf,
                                r2sf);
                    }
                    ALAI = (1 - FracSen()) * LAI();
                    break;
            }
        }
    }

    /**
     * @brief Methode d'initialisation pour les variables d'état du modèle
     * @param time la date du pas de temps courant
     */
    virtual void initValue(const vd::Time& /* time */)
    {
        lplant = 0.0;
        LAI = 0.0;
        ALAI = 0.0;
        FracSen = 0.0;
    }

private:
    //Variables d'etat
    /**
     * @brief LAI total theorique (m²/m²)
     */
    Var lplant;
    /**
     * @brief LAI total de la plante (m²/m²)
     */
    Var LAI;
    /**
     * @brief LAI actif (m²/m²)
     */
    Var ALAI;
    /**
     * @brief fraction du LAI senescent (-)
     */
    Var FracSen;

    //Entrées
    /**
     * @brief temps thermique (°C.j)
     */
    Sync TT;
    /**
     * @brief le stade phenologique de la plante (cf CropDvtStade.hpp) (-)
     */
    Sync DvtStade;
    /**
     * @brief le temps thermique entre emergence et maturité (°C.j)
     */
    Sync tt_em_mat;
    /**
     * @brief le rapport transpiration réelle / potentielle utilisé comme indice de stress (-)
     */
    Nosync ATPT;

    //Parametres du modele
    /**
     * @brief paramètre de l'équation logistique du LAI (-)
     */
    double p1logi;
    /**
     * @brief paramètre de l'équation logistique du LAI ((°C.j)^-1)
     */
    double p2logi;
    /**
     * @brief la valeur du LAI à l'emergtence (m²/m²)
     */
    double lai0;
    /**
     * @brief la densité de semis (plante /m²)
     */
    double dens;
    /**
     * @brief effet du rapport ATPT sur la croissance du LAI (-)
     */
    double r1sf;
    /**
     * @brief effet du rapport ATPT sur la croissance du LAI (-)
     */
    double r2sf;
    /**
     * @brief parametre de la fraction de LAI senescent (-)
     */
    double p1sen;
    /**
     * @brief parametre de la fraction de LAI senescent (-)
     */
    double p2sen;

    //fonction locale
    /**
     * @brief fonction de reduction utilisée avec l'indice de stress ATPT
     * @param x la valeur du stress
     * @param p1 parametre de controle
     * @param p2 parametre de controle
     * @return valeur de la reduction a appliquer
     */
    double reduc(double x, double p1, double p2)
    {
        if (x < p2 - p1) {
            return 0;
        } else {
            if (x > p2) {
                return 1;
            } else {
                return (p1 - p2 + x) / p1;
            }
        }
    }

};
}
}//namespaces
DECLARE_DYNAMICS( record::cv::CropLAI); // balise specifique VLE

