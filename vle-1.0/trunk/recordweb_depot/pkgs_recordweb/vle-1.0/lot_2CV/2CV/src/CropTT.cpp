/**
 * @file src/CropTT.cpp
 * @author The RECORD Development Team (INRA)
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/extension/DifferenceEquation.hpp> // Correspond a l'extension DEVS utilise ici

// Raccourcis de nommage des namespaces frequement utilises
namespace vd = vle::devs;
namespace ve = vle::extension;
namespace vv = vle::value;

// Definition du namespace de la classe du modele
namespace record {
namespace cv {

/**
 * @brief Calcul de l'avancement du temps thermique et des stades phenologiques de la plantes.
 * Utilise l'extension DEVS DifferenceEquation::Multiple.
 *
 * Le modèle calcul à chaque pas de temps les variables d'état :
 * - TT : temps thermique (°C.j)
 * - DvtStade : le stade phenologique de la plante (cf CropDvtStade.hpp) (-)
 *
 * A partir des données d'entrée :
 * - Tmin : la température minimale journalière (°C)
 * - Tmax : la température maximale journalière (°C)
 * - tt_em_mat : le temps thermique entre emergence et maturité (°C.j)
 *
 * Et des paramètres :
 * - tt_Sowing_Emergence : le temps thermique entre les stades 0 & 1 (°C.j)
 * - tt_Emergence_MaxLAI : le temps thermique entre les stades 1 & 2 (°C.j)
 * - tt_MaxLAI_Flowering : le temps thermique entre les stades 2 & 3 (°C.j)
 * - tt_Flowering_GrainAbort : le temps thermique entre les stades 3 & 4 (°C.j)
 * - tt_GrainAbort_LeafSen : le temps thermique entre les stades 4 & 5 (°C.j)
 * - tt_LeafSen_Maturity : le temps thermique entre les stades 5 & 6 (°C.j)
 */
class CropTT : public ve::DifferenceEquation::Multiple
{
public:
    /**
     * @brief Constructeur de la classe du modèle.
     * C'est ici que se font les enregistrements des variables d'état (Var)
     * et des variables d'entrées (Sync & Nosync) dans le moteur de simulation VLE.
     * C'est aussi ici que l'ont attribut leur valeurs aux paramètres du modèle à
     * partir des conditions expérimentales définies dans le VPZ
     *
     * @param events liste des evenements provenant des conditions expérimentales du VPZ
     * @param atom ?
     */
    CropTT(const vd::DynamicsInit& atom, const vd::InitEventList& evts)
        : ve::DifferenceEquation::Multiple(atom, evts)
    {
        //Attribution des valeurs des paramètres selon les valeurs contenues dans les conditions du fichier vpz
        tt_Emergence_MaxLAI = vv::toDouble(evts.get("tt_Emergence_MaxLAI"));
        tt_LeafSen_Maturity = vv::toDouble(evts.get("tt_LeafSen_Maturity"));
        tt_Sowing_Emergence = vv::toDouble(evts.get("tt_Sowing_Emergence"));
        tt_MaxLAI_Flowering = vv::toDouble(evts.get("tt_MaxLAI_Flowering"));
        tt_Flowering_GrainAbort = vv::toDouble(evts.get("tt_Flowering_GrainAbort"));
        tt_GrainAbort_LeafSen = vv::toDouble(evts.get("tt_GrainAbort_LeafSen"));
        //enregistrement des variables d'état et d'entrée du modèle
        TT = createVar("TT");
        DvtStade = createVar("DvtStade");
        Tmin = createSync("Tmin");
        Tmax = createSync("Tmax");
    }

    /**
     * @brief Destructeur de la classe du modèle.
    **/
    virtual ~CropTT() {}

    /**
     * @brief Methode de calcul effectuée à chaque pas de temps
     * @param time la date du pas de temps courant
     */
    virtual void compute(const vd::Time& /*time*/) {
        //variable locale temporaire pour le calcul de TT
        double TTtemp = TT(-1) + std::max(0., (Tmin() + std::min(30., Tmax() )) / 2 - 6);

        if ((DvtStade(-1) == 0) and (TTtemp >= tt_Sowing_Emergence)) {
            TT = TTtemp - tt_Sowing_Emergence;//reinitialisation de TT à l'emergence
            DvtStade = 1;
        } else {
            TT = TTtemp;
        }
        if ((DvtStade(-1) == 1) and (TT() >= tt_Emergence_MaxLAI)) {
            DvtStade = 2;
        }
        if ((DvtStade(-1) == 2) and
          (TT() >= tt_Emergence_MaxLAI + tt_MaxLAI_Flowering)) {
            DvtStade = 3;
        }
        if ((DvtStade(-1) == 3) and
          (TT() >= tt_Emergence_MaxLAI+tt_MaxLAI_Flowering+tt_Flowering_GrainAbort)) {
            DvtStade = 4;
        }
        if ((DvtStade(-1) == 4) and
          (TT() >= tt_Emergence_MaxLAI + tt_MaxLAI_Flowering +
          tt_Flowering_GrainAbort + tt_GrainAbort_LeafSen)) {
            DvtStade = 5;
        }
        if ((DvtStade(-1) == 5) and
          (TT() >= tt_Emergence_MaxLAI + tt_MaxLAI_Flowering +
          tt_Flowering_GrainAbort + tt_GrainAbort_LeafSen +
          tt_LeafSen_Maturity)) {
            DvtStade = 6;
        }

    }

    /**
     * @brief Methode d'initialisation pour les variables d'état du modèle
     * @param time la date du pas de temps courant
     */
    virtual void initValue(const vd::Time& /*time*/) { 
        TT=0.0;
        DvtStade =0; 
    }

private:
    //paramètres
    /**
     * @brief temps thermique entre les stades : SOWING et EMERGENCE (respectivement 0 & 1) (°C.j)
     */
    double tt_Sowing_Emergence;
    /**
     * @brief temps thermique entre les stades : EMERGENCE et MAX_LAI (respectivement 1 & 2) (°C.j)
     */
    double tt_Emergence_MaxLAI;
    /**
     * @brief temps thermique entre les stades : MAX_LAI et FLOWERING (respectivement 2 & 3) (°C.j)
     */
    double tt_MaxLAI_Flowering;
    /**
     * @brief temps thermique entre les stades : FLOWERING et CRITICAL_GRAIN_ABORTION (respectivement 3 & 4) (°C.j)
     */
    double tt_Flowering_GrainAbort;
    /**
     * @brief temps thermique entre les stades : CRITICAL_GRAIN_ABORTION et LEAF_SENESCENCE (respectivement 4 & 5) (°C.j)
     */
    double tt_GrainAbort_LeafSen;
    /**
     * @brief temps thermique entre les stades : LEAF_SENESCENCE et MATURITY (respectivement 5 & 6) (°C.j)
     */
    double tt_LeafSen_Maturity;
    //variables d'état
    /**
     * @brief temps thermique (°C.j)
     */
    Var TT;
    /**
     * @brief stades phénologique de la plante (cf CropDvtStade.hpp) {0:SOWING - 1:EMERGENCE - 2:MAX_LAI - 3:FLOWERING - 4:CRITICAL_GRAIN_ABORTION - 5:LEAF_SENESCENCE - 6:MATURITY
     */
    Var DvtStade; //
    //variable d'entrée
    /**
     * @brief température journalière minimale (°C)
     */
    Sync Tmin;
    /**
     * @brief température journalière maximale (°C)
     */
    Sync Tmax;
};

}
} // namespace record::cv

DECLARE_DYNAMICS(record::cv::CropTT)

