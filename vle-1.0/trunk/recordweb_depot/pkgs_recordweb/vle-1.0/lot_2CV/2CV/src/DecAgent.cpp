/**
 * @file src/DecAgent.cpp
 * @author The RECORD Development Team (INRA)
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/extension/Decision.hpp>// correspond a l'extension DEVS utilise ici
#include <vle/utils.hpp>
#include <sstream>
#include <numeric>
#include <iterator>
#include <CropDvtStade.hpp>//Definition commune des differents etats phenologique de la plante

// raccourcis de nommage des namespaces frequement utilises
namespace vd = vle::devs;
namespace vv = vle::value;
namespace ved = vle::extension::decision;
namespace vu = vle::utils;

//definition du namespace de la classe du modele
namespace record {
namespace cv {

class DecAgent: public ved::Agent {
public:
    DecAgent(const vd::DynamicsInit& mdl, const vd::InitEventList& evts) :
        ved::Agent(mdl, evts), mPluies(5, 0.), mStade(BARE_SOIL),
                mDateDebutMaturite(0.0), mDateDerniereIrrigation(0.0),
                mNombreIrrigation(1)
    {
        // Lecture des valeurs de parametres dans les conditions du vpz
        stockEau = evts.getDouble("stockEau");
        apportEau = evts.getDouble("apportEau");
        quantiteEauSolPortant = evts.getDouble("quantiteEauSolPortant");
        quantiteEauPeriodeSeche = evts.getDouble("quantiteEauPeriodeSeche");
        nbJoursAvantRetour = evts.getInt("nbJoursAvantRetour");

        //inititialisation du KnowledgeBase
        addFacts(this) += F("Rain", &DecAgent::Rain),
                F("DvtStade", &DecAgent::DvtStade);

        addPredicates(this) += P("periodeSeche", &DecAgent::periodeSeche),
                P("maturiteAtteinte", &DecAgent::maturiteAtteinte),
                P("eauDisponible", &DecAgent::eauDisponible),
                P("solPortant",&DecAgent::solPortant),
                P("finSemis", &DecAgent::finSemis),
                P("finRecolte", &DecAgent::finRecolte),
                P("recolteNonEffectuee",&DecAgent::recolteNonEffectuee);

        addOutputFunctions(this) += O("semis", &DecAgent::semis),
                O("recolte",&DecAgent::recolte),
                O("irrigation", &DecAgent::irrigation);

        addAcknowledgeFunctions(this) += A("ackIrrigation", &DecAgent::ackIrrigation);

        //Initalisation du plan
        std::string filePath = vu::Path::path().getExternalPackageDataFile("2CV", "DecAgentPlan.txt");
        std::ifstream fileStream(filePath.c_str());
        KnowledgeBase::plan().fill(fileStream);
    }

    virtual ~DecAgent() {}

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Predicats
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * @brief predicat periodeSeche
     * (somme de pluie sur les 5 derniers jours < quantiteEauPeriodeSeche)
     */
    bool periodeSeche() const
    {
        return std::accumulate(mPluies.begin(), mPluies.begin() + 5, 0.)
                <= quantiteEauPeriodeSeche;
    }

    /**
     * @brief predicat maturiteAtteinte
     * (la variable mDateDebutMaturite a ete initialisee)
     */
    bool maturiteAtteinte() const
    { return (mDateDebutMaturite > 0); }

    /**
     * @brief predicat finRecolte
     * (la date de maturite est depassee exactement de 30 jours)
     */
    bool finRecolte() const
    { return currentTime() == mDateDebutMaturite + 30; }

    /**
     * @brief predicat eauDisponible
     * (le stoch d'eau contient suffisamment d'eau pour un apport)
     */
    bool eauDisponible() const
    { return stockEau >= apportEau; }

    /**
     * @brief predicat solPortant
     * (somme de pluie sur les 3 derniers jours < quantiteEauSolPortant)
     */
    bool solPortant() const
    {
        return std::accumulate(mPluies.begin(), mPluies.begin() + 3, 0.)
                <= quantiteEauSolPortant;
    }

    /**
     * @brief predicat finSemis
     * (on est a la date dateFinSemis).
     */
    bool finSemis() const
    { return activite("Semis").finish() == currentTime(); }

    /**
     * @brief predicat recolteNonEffectuee
     * (à vrai si a recolte n'a pas été effectuée).
     */
    bool recolteNonEffectuee() const
    { return !activite("Recolte").isInDoneState(); }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Facts
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * @brief mise a jour du fait DvtStade
     */
    void DvtStade(const vv::Value& value)
    {
        int newstade = std::floor(value.toDouble().value());
        if (newstade == MATURITY && mStade != MATURITY) {
            mDateDebutMaturite = mCurrentTime;
        }
        mStade = newstade;
    }

    /**
     * @brief mise a jour du fait Rain
     */
    void Rain(const vv::Value& value)
    {
        std::rotate(mPluies.rbegin(), mPluies.rbegin() + 1, mPluies.rend());
        mPluies[0] = value.toDouble().value();
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Output functions
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * @brief activation du semis
     */
    void semis(const std::string& name, const ved::Activity& activity,
            vd::ExternalEventList& output)
    {
        if (activity.isInStartedState()) {
            vd::ExternalEvent* evt = new vd::ExternalEvent("Sowing");
            evt->putAttribute("name", new vv::String(name));
            evt->putAttribute("activity", new vv::String(name));
            evt->putAttribute("value", new vv::Double(1.));
            output.addEvent(evt);
        }
    }

    /**
     * @brief activation de la recolte
     */
    void recolte(const std::string& name, const ved::Activity& activity,
            vd::ExternalEventList& output)
    {
        if (activity.isInStartedState()) {
            vd::ExternalEvent* evt = new vd::ExternalEvent("Harvesting");
            evt->putAttribute("name", new vv::String(name));
            evt->putAttribute("activity", new vv::String(name));
            evt->putAttribute("value", new vv::Double(1.));
            output.addEvent(evt);
        }
    }

    /**
     * @brief activation d'une irrigation
     */
    void irrigation(const std::string& name, const ved::Activity& activity,
            vd::ExternalEventList& output)
    {
        if (activity.isInStartedState()) {
            vd::ExternalEvent* evt = new vd::ExternalEvent("Irrigation");
            evt->putAttribute("name", new vv::String("Irrigation"));
            evt->putAttribute("activity", new vv::String(name));
            evt->putAttribute("value",
                    new vv::Double(std::min(apportEau, stockEau)));
            output.addEvent(evt);
        }
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * Acknowledge functions
     * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    void ackIrrigation(const std::string&
    /*activityname*/, const ved::Activity& /*activity*/)
    {
        mNombreIrrigation++;
        std::string name(activiteIrrigation(mNombreIrrigation));
        std::string prec(activiteIrrigation(mNombreIrrigation - 1));
        mDateDerniereIrrigation = currentTime();
        stockEau -= apportEau;
        if (eauDisponible() and mStade != BARE_SOIL and mNombreIrrigation <= 12) {
            ved::Activity& a = addActivity(name,
                    mDateDerniereIrrigation + nbJoursAvantRetour, 2437641.0); //correspond au debut de simulation + 340 jours
            a.addRule("regleIrrigation", regle("regleIrrigation"));
            a.addAcknowledgeFunction(ack("ackIrrigation"));
            a.addOutputFunction(out("irrigation"));
        }
    }

private:

    //Parametres de l'agent
    double stockEau;
    double apportEau;
    double quantiteEauSolPortant;
    double quantiteEauPeriodeSeche;
    int nbJoursAvantRetour;

    //variables de stockage de données
    std::vector<double> mPluies;
    int mStade;
    vd::Time mDateDebutMaturite;
    vd::Time mDateDerniereIrrigation;
    int mNombreIrrigation;

    /**
     * @brief recupere une activite
     * @param nom, nom de l'activite
     * @return l'activite de nom 'nom'
     */
    const ved::Activity& activite(const std::string& nom) const
    { return activities().get(nom)->second; }
    /**
     * @brief recupere une regle du KnowledgeBase
     * @param regle, nom de la fonction
     * @return la regle de nom 'regle'
     */
    const ved::Rule& regle(const std::string& regle)
    { return KnowledgeBase::rules().get(regle); }

    /**
     * @brief recupere une fonction ack du KnowledgeBase
     * @param ack, nom de la fonction
     * @return la fonction acknowledge de nom 'ack'
     */
    const ved::Activity::AckFct& ack(const std::string& ack)
    { return KnowledgeBase::acknowledgeFunctions().get(ack)->second; }

    /**
     * @brief recupere une fonction output du KnowledgeBase
     * @param out, nom de la fonction
     * @return la fonction output de nom 'out'
     */
    const ved::Activity::OutFct& out(const std::string& out)
    { return KnowledgeBase::outputFunctions().get(out)->second; }

    /**
     * @brief dit si une activite est une irrigation supplémentaire
     * (autre que la 1ere)
     * @param act, le nom de l'activite
     * @return true si l'activite est une irrigation supplémantaire
     */
    bool estUneIrrigationSupplementaire(const std::string& act) const
    { return (act.size() >= 11 and act.compare(0, 10, "Irrigation") == 0); }

    /**
     * @brief retourne le nom de l'activité correspondant à une irrigation
     * @param nbr, le numéro de l'irrigation
     * @return le nom de l'activité correspondant à la 'nbr' ème irrigation
     */
    std::string activiteIrrigation(int nbr) const
    {
        std::stringstream ret;
        ret << "Irrigation_";
        ret << boost::format("%1$02d") % nbr;
        return ret.str();

    }

    /**
     * @brief Fonction d'observation des activités et des prédicats
     */
    vv::Value* observation(const vle::devs::ObservationEvent& event) const
    {
        std::string port_name = event.getPortName();
        if (port_name == "Recolte" or port_name == ".Semis" or port_name
                == "Irrigation_01" or estUneIrrigationSupplementaire(port_name)) {

            if (port_name == ".Semis")
                port_name = "Semis";
            std::string obs = port_name + ":";

            if (activities().exist(port_name)) {
                const ved::Activity& act = activities().get(port_name)->second;
                std::string interval = "ext";
                if (event.getTime() >= act.start() and event.getTime()
                        <= act.finish()) {
                    interval = "int";
                }
                obs = port_name + ":";
                obs = obs + interval;

                switch (act.state()) {
                    case ved::Activity::WAIT:
                        obs = obs + "-wait";
                        break;
                    case ved::Activity::STARTED:
                        obs = obs + "-started";
                        break;
                    case ved::Activity::FF:
                        obs = obs + "-ff";
                        break;
                    case ved::Activity::DONE:
                        obs = obs + "-done";
                        break;
                    case ved::Activity::FAILED:
                        obs = obs + "-failed";
                        break;
                }
            }

            return buildString(obs);
        } else if (port_name == ".Date") {
            return buildString(
                    vu::DateTime::toJulianDayNumber((long) event.getTime()));
        } else if (event.onPort("eauDisponible")) {
            if (eauDisponible()) {
                return buildString("eauDisponible");
            } else {
                return buildString("false");
            }
        } else if (event.onPort("finRecolte")) {
            if (finRecolte()) {
                return buildString("finRecolte");
            } else {
                return buildString("false");
            }
        } else if (event.onPort("finSemis")) {
            if (finSemis()) {
                return buildString("finSemis");
            } else {
                return buildString("false");
            }
        } else if (event.onPort("maturiteAtteinte")) {
            if (maturiteAtteinte()) {
                return buildString("maturiteAtteinte");
            } else {
                return buildString("false");
            }
        } else if (event.onPort("periodeSeche")) {
            if (periodeSeche()) {
                return buildString("periodeSeche");
            } else {
                return buildString("false");
            }
        } else if (event.onPort("solPortant")) {
            if (solPortant()) {
                return buildString("solPortant");
            } else {
                return buildString("false");
            }
        }
        return ved::Agent::observation(event);
    }

};

}
} // namespaces

DECLARE_DYNAMICS(record::cv::DecAgent)
