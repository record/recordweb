/**
 * @file src/CropTT_fsa.cpp
 * @author The RECORD Development Team (INRA)
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/extension/fsa/Statechart.hpp>// Correspond a l'extension DEVS utilise ici
#include <vle/extension/DifferenceEquation.hpp>// Inclus ici car nous utilisons les Var comme evenement d'entree
#include <vle/utils/DateTime.hpp>// Pour recuperer la date a partir du moteur de simulation de VLE
#include <CropDvtStade.hpp>// Definition commune des differents etats phenologique de la plante

// Raccourcis de nommage des namespaces frequement utilises
namespace ve = vle::extension;
namespace vd = vle::devs;
namespace vv = vle::value;
namespace ved = vle::extension::DifferenceEquation;

// Definition du namespace de la classe du modele
namespace record {
namespace cv {

/**
 * @brief Changements de stades phenologiques de la plante durant le cycle de croissance.
 * Utilise l'extension DEVS fsa::Statechart.
 *
 * Le modèle calcul à chaque pas de temps les variables d'état :
 * - DvtStade : le stade phenologique de la plante (cf CropDvtStade.hpp) (-)
 * - TT : temps thermique -> remis à 0 à l'emergence(°C.j)
 *
 * A partir des evenements d'entrée :
 * - TT : temps thermique (°C.j)
 * - Sowing : l'evenement de declenchement du semis
 * - Harvesting : ll'evenement de declenchement de la recolte
 *
 * Et des paramètres :
 * - tt_Sowing_Emergence : temps thermique entre les stades phenologiques SOWING et EMERGENCE (resp 0 & 1) - [TT] <==> °C.jour
 * - tt_Emergence_MaxLAI : temps thermique entre les stades phenologiques EMERGENCE et MAX_LAI (resp 1 & 2) - [TT] <==> °C.jour
 * - tt_MaxLAI_Flowering : temps thermique entre les stades phenologiques MAX_LAI et FLOWERING (resp 2 & 3) - [TT] <==> °C.jour
 * - tt_Flowering_GrainAbort : temps thermique entre les stades phenologiques FLOWERING et CRITICAL_GRAIN_ABORTION (resp 3 & 4) - [TT] <==> °C.jour
 * - tt_GrainAbort_LeafSen : temps thermique entre les stades phenologiques CRITICAL_GRAIN_ABORTION et LEAF_SENESCENCE (resp 4 & 5) - [TT] <==> °C.jour
 * - tt_LeafSen_Maturity : temps thermique entre les stades phenologiques LEAF_SENESCENCE et MATURITY (resp 5 & 6) - [TT] <==> °C.jour
 */
class CropTT_fsa: public ve::fsa::Statechart {
public:
    /**
     * @brief Constructeur de la classe du modèle.
     * C'est ici que sont définis les états possibles de l'automate, ainsi que
     * les transitions d'un état à un autre, et les réactions aux evenements d'entrée
     * C'est aussi ici que l'ont attribut leur valeurs aux paramètres du modèle à
     * partir des conditions expérimentales définies dans le VPZ
     *
     * @param events liste des evenements provenant des conditions expérimentales du VPZ
     * @param init ?
     */
    CropTT_fsa(const vd::DynamicsInit& init, const vd::InitEventList& events) :
        ve::fsa::Statechart(init, events), TT(0.0), Sowing(0.0), Harvesting(0.0)
    {
        //definition des etats possibles de l'automate
        states(this) << SOWING << EMERGENCE << MAX_LAI << FLOWERING
                << CRITICAL_GRAIN_ABORTION << LEAF_SENESCENCE << MATURITY
                << BARE_SOIL;

        //Definition des transition entre les etats
        // "guard" defini la fonction de condition de declenchement de changement d'etat
        // "action" defini une action a mener dans le cas d'une transition
        // "send" defini un evenement sur un port de sortie dans le cas d'une transition
        transition(this, SOWING, EMERGENCE)
            << guard(&CropTT_fsa::c1)
            << action(&CropTT_fsa::actionInit)
            << send(&CropTT_fsa::out1);
        transition(this, EMERGENCE, MAX_LAI)
            << guard(&CropTT_fsa::c2)
            << send(&CropTT_fsa::out2);
        transition(this, MAX_LAI, FLOWERING)
            << guard(&CropTT_fsa::c3)
            << send(&CropTT_fsa::out2);
        transition(this, FLOWERING, CRITICAL_GRAIN_ABORTION)
            << guard(&CropTT_fsa::c4)
            << send(&CropTT_fsa::out2);
        transition(this, CRITICAL_GRAIN_ABORTION, LEAF_SENESCENCE)
            << guard(&CropTT_fsa::c5)
            << send(&CropTT_fsa::out2);
        transition(this, LEAF_SENESCENCE, MATURITY)
            << guard(&CropTT_fsa::c6)
            << send(&CropTT_fsa::out2);
        //sowing (semis)
        transition(this, BARE_SOIL, SOWING)
            << guard(&CropTT_fsa::cSowing)
            << action(&CropTT_fsa::actionSowing)
            << send(&CropTT_fsa::outSowing);
        //harvest (recolte)
        transition(this, SOWING, BARE_SOIL)
            << guard(&CropTT_fsa::cHarvest)
            << action(&CropTT_fsa::actionHarvest)
            << send(&CropTT_fsa::outHarvest);
        transition(this, EMERGENCE, BARE_SOIL)
            << guard(&CropTT_fsa::cHarvest)
            << action(&CropTT_fsa::actionHarvest)
            << send(&CropTT_fsa::outHarvest);
        transition(this, MAX_LAI, BARE_SOIL)
            << guard(&CropTT_fsa::cHarvest)
            << action(&CropTT_fsa::actionHarvest)
            << send(&CropTT_fsa::outHarvest);
        transition(this, FLOWERING, BARE_SOIL)
            << guard(&CropTT_fsa::cHarvest)
            << action(&CropTT_fsa::actionHarvest)
            << send(&CropTT_fsa::outHarvest);
        transition(this, CRITICAL_GRAIN_ABORTION, BARE_SOIL)
            << guard(&CropTT_fsa::cHarvest)
            << action(&CropTT_fsa::actionHarvest)
            << send(&CropTT_fsa::outHarvest);
        transition(this, LEAF_SENESCENCE, BARE_SOIL)
            << guard(&CropTT_fsa::cHarvest)
            << action(&CropTT_fsa::actionHarvest)
            << send(&CropTT_fsa::outHarvest);
        transition(this, MATURITY, BARE_SOIL)
            << guard(&CropTT_fsa::cHarvest)
            << action(&CropTT_fsa::actionHarvest)
            << send(&CropTT_fsa::outHarvest);

        //defini la reaction aux evenements d'entree 
        eventInState(this, "TT", &CropTT_fsa::inTT)
            >> SOWING >> EMERGENCE >> MAX_LAI >> FLOWERING
            >> CRITICAL_GRAIN_ABORTION >> LEAF_SENESCENCE >> MATURITY;

        eventInState(this, "Sowing", &CropTT_fsa::inSowing)
            >> BARE_SOIL>> SOWING;

        eventInState(this, "Harvesting", &CropTT_fsa::inHarvesting)
            >> SOWING >> EMERGENCE >> MAX_LAI >> FLOWERING
            >> CRITICAL_GRAIN_ABORTION>> LEAF_SENESCENCE >> MATURITY
            >> BARE_SOIL;

        //etat initial de l'automate
        initialState(BARE_SOIL);

        //attribut les valeurs des parametres a partir des conditions du vpz
        tt_Emergence_MaxLAI = vv::toDouble(events.get("tt_Emergence_MaxLAI"));
        tt_LeafSen_Maturity = vv::toDouble(events.get("tt_LeafSen_Maturity"));
        //ces parametres ont une valeur par defaut utilise si la condition n'est pas definie
        tt_Sowing_Emergence = (events.exist("tt_Sowing_Emergence")) ?
            vv::toDouble(events.get("tt_Sowing_Emergence")) : 80.0;
        tt_MaxLAI_Flowering = (events.exist("tt_MaxLAI_Flowering")) ?
            vv::toDouble(events.get("tt_MaxLAI_Flowering")) : 90.0;
        tt_Flowering_GrainAbort = (events.exist("tt_Flowering_GrainAbort")) ?
            vv::toDouble( events.get("tt_Flowering_GrainAbort")) : 250.0;
        tt_GrainAbort_LeafSen = (events.exist("tt_GrainAbort_LeafSen")) ?
            vv::toDouble(events.get("tt_GrainAbort_LeafSen")) : 245.0;
    }

    /**
     * @brief Destructeur de la classe du modèle.
    **/
    virtual ~CropTT_fsa() {}

private:
    //ecoute des ports d'entree
    /**
     * @brief Réaction à l'evenement d'entrée "TT"
     * @param event evenement d'entrée
     */
    void inTT(const vd::Time& /*time*/, const vd::ExternalEvent* event)
    { TT << ved::Var("TT", event); }

    /**
     * @brief Réaction à l'evenement d'entrée "Sowing"
     * @param event evenement d'entrée
     */
    void inSowing(const vd::Time& /*time*/, const vd::ExternalEvent* event)
    { Sowing << ved::Var("Sowing", event); }

    /**
     * @brief Réaction à l'evenement d'entrée "Harvesting"
     * @param event evenement d'entrée
     */
    void inHarvesting(const vd::Time& /*time*/, const vd::ExternalEvent* event)
    {
        Harvesting << ved::Var("Harvesting", event);
        Sowing = 0;
    }

    //actions
    /**
     * @brief action realisée à l'emergence
     */
    void actionInit(const vd::Time& /*time*/)
    { TT -= tt_Sowing_Emergence; }

    /**
     * @brief action realisée à la recolte
     */
    void actionHarvest(const vd::Time& /*time*/)
    { TT = 0.0; }

    /**
     * @brief action realisée au semis
     */
    void actionSowing(const vd::Time& /*time*/)
    { Sowing = 0.0; }

    //sorties
    /**
     * @brief evenement de sortie envoyé lors de l'emergence
     */
    void out1(const vd::Time& /*time*/, vd::ExternalEventList& output) const
    {
        output << (ved::Var("DvtStade") = (double)currentState() + 1);
        output << (ved::Var("TT") = TT);
    }

    /**
     * @brief evenement de sortie envoyé lors d'un changement de stade phenologique
     */
    void out2(const vd::Time& /*time*/, vd::ExternalEventList& output) const
    { output << (ved::Var("DvtStade") = (double) currentState() + 1); }

    /**
     * @brief evenement de sortie envoyé lors du semis
     */
    void outSowing(const vd::Time& /*time*/, vd::ExternalEventList& output) const
    { output << (ved::Var("DvtStade") = SOWING); }

    /**
     * @brief evenement de sortie envoyé lors de la recolte
     */
    void outHarvest(const vd::Time& /*time*/, vd::ExternalEventList& output) const
    { output << (ved::Var("DvtStade") = BARE_SOIL); }

    //conditions
    /**
     * @brief condition de changement d'état pour l'emergence
     */
    bool c1(const vd::Time& /*time*/)
    { return TT >= tt_Sowing_Emergence; }

    /**
     * @brief condition de changement d'état pour MaxLAI
     */
    bool c2(const vd::Time& /*time*/)
    { return TT >= tt_Emergence_MaxLAI; }

    /**
     * @brief condition de changement d'état pour Flowering
     */
    bool c3(const vd::Time& /*time*/)
    { return TT >= tt_Emergence_MaxLAI + tt_MaxLAI_Flowering; }

    /**
     * @brief condition de changement d'état pour GrainAbort
     */
    bool c4(const vd::Time& /*time*/)
    {
        return TT >= tt_Emergence_MaxLAI + tt_MaxLAI_Flowering
                + tt_Flowering_GrainAbort;
    }

    /**
     * @brief condition de changement d'état pour LeafSe
     */
    bool c5(const vd::Time& /*time*/)
    {
        return TT >= tt_Emergence_MaxLAI + tt_MaxLAI_Flowering
                + tt_Flowering_GrainAbort + tt_GrainAbort_LeafSen;
    }

    /**
     * @brief condition de changement d'état pour Maturity
     */
    bool c6(const vd::Time& /*time*/)
    {
        return TT >= tt_Emergence_MaxLAI + tt_MaxLAI_Flowering
                + tt_Flowering_GrainAbort + tt_GrainAbort_LeafSen
                + tt_LeafSen_Maturity;
    }

    /**
     * @brief condition de changement d'état pour le semis
     */
    bool cSowing(const vd::Time& /*time*/)
    { return Sowing == 1.0; }

    /**
     * @brief condition de changement d'état pour la recolte
     */
    bool cHarvest(const vd::Time& /*time*/)
    {
        return Harvesting == 1.0;
    }

    // variables d'entrée
    /**
     * @brief temps thermique (°C.j)
     */
    double TT;
    /**
     * @brief l'evenement de declenchement du semis
     */
    double Sowing;
    /**
     * @brief l'evenement de declenchement de la recolte
     */
    double Harvesting;

    // parametres
    /**
     * @brief temps thermique entre les stades phenologiques SOWING et EMERGENCE (resp 0 & 1) - [TT] <==> °C.jour
     */
    double tt_Sowing_Emergence;
    /**
     * @brief temps thermique entre les stades phenologiques EMERGENCE et MAX_LAI (resp 1 & 2) - [TT] <==> °C.jour
     */
    double tt_Emergence_MaxLAI;
    /**
     * @brief temps thermique entre les stades phenologiques MAX_LAI et FLOWERING (resp 2 & 3) - [TT] <==> °C.jour
     */
    double tt_MaxLAI_Flowering;
    /**
     * @brief temps thermique entre les stades phenologiques FLOWERING et CRITICAL_GRAIN_ABORTION (resp 3 & 4) - [TT] <==> °C.jour
     */
    double tt_Flowering_GrainAbort;
    /**
     * @brief temps thermique entre les stades phenologiques CRITICAL_GRAIN_ABORTION et LEAF_SENESCENCE (resp 4 & 5) - [TT] <==> °C.jour
     */
    double tt_GrainAbort_LeafSen;
    /**
     * @brief temps thermique entre les stades phenologiques LEAF_SENESCENCE et MATURITY (resp 5 & 6) - [TT] <==> °C.jour
     */
    double tt_LeafSen_Maturity;

};

}
}//namespaces

DECLARE_DYNAMICS( record::cv::CropTT_fsa);
