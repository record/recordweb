/**
 * @file src/CropRoot.cpp
 * @author The RECORD Development Team (INRA)
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/extension/DifferenceEquation.hpp>// Correspond a l'extension DEVS utilise ici
#include <CropDvtStade.hpp>// Definition commune des differents etats phenologique de la plante

// Raccourcis de nommage des namespaces frequement utilises
namespace ve = vle::extension;
namespace vd = vle::devs;
namespace vv = vle::value;

// Definition du namespace de la classe du modele
namespace record {
namespace cv {

/**
 * @brief Croissance du front racinaire basé sur le temps thermique.
 * Utilise l'extension DEVS DifferenceEquation::Multiple.
 * #include <CropDvtStade.hpp>
 *
 * Le modèle calcul à chaque pas de temps la variable d'état :
 * - RootDepth : profondeur du front racinaire. (mm)
 *
 * A partir des données d'entrée :
 * - TT : temps thermique (°C.j)
 * - DvtStade : le stade phenologique de la plante (cf CropDvtStade.hpp) (-)
 *
 * Et des paramètres :
 * - soilDepth : la profondeur du sol. (mm)
 * - maxDepth : la profondeur maximal atteignable par les racines. (mm)
 * - rateDepth : la croissance journaliere maximale du front racinaire (mm/j)
 * - iniDepth : la profondeur initiale à l'emergence (mm)
 */
class CropRoot: public ve::DifferenceEquation::Multiple {
public:
    /**
     * @brief Constructeur de la classe du modèle.
     * C'est ici que se font les enregistrements des variables d'état (Var)
     * et des variables d'entrées (Sync & Nosync) dans le moteur de simulation VLE.
     * C'est aussi ici que l'ont attribut leur valeurs aux paramètres du modèle à
     * partir des conditions expérimentales définies dans le VPZ
     *
     * @param events liste des evenements provenant des conditions expérimentales du VPZ
     * @param atom ?
     */
    CropRoot(const vd::DynamicsInit& atom, const vd::InitEventList& events) :
        ve::DifferenceEquation::Multiple(atom, events)
    {
        // Variables d'etat gerees par ce composant
        RootDepth = createVar("RootDepth");

        // Variables gerees par un autre composant
        TT = createSync("TT");
        DvtStade = createSync("DvtStade");

        // Lecture des valeurs de parametres dans les conditions du vpz
        soilDepth = vv::toDouble(events.get("soilDepth"));
        //ces parametres ont une valeur par defaut utilise si la condition n'est pas definie
        maxDepth = (events.exist("maxDepth")) ? vv::toDouble(events.get("maxDepth")) : 1300;
        rateDepth = (events.exist("rateDepth")) ? vv::toDouble(events.get("rateDepth")) : 1.63;
        iniDepth = (events.exist("iniDepth")) ? vv::toDouble(events.get("iniDepth")) : 100;
    }

    /**
     * @brief Destructeur de la classe du modèle.
    **/
    virtual ~CropRoot() {};

    /**
     * @brief Methode de calcul effectuée à chaque pas de temps
     * @param time la date du pas de temps courant
     */
    virtual void compute(const vd::Time& /*time*/)
    {
        if (DvtStade() == BARE_SOIL) {
            RootDepth = 0.0;
        } else {
            double dRootDepth = 0;
            switch ((int) DvtStade()) {
                case SOWING:
                    dRootDepth = 0;
                    RootDepth = 0;
                    break;
                case EMERGENCE:
                case MAX_LAI:
                case FLOWERING:
                case CRITICAL_GRAIN_ABORTION:
                case LEAF_SENESCENCE:
                case MATURITY:
                    if (DvtStade(-1) == SOWING) {
                        dRootDepth = iniDepth;
                    } else {
                        dRootDepth = std::min(
                                std::min(soilDepth - RootDepth(-1),
                                        maxDepth - RootDepth(-1)),
                                rateDepth * (TT() - TT(-1)));
                    }
                    RootDepth = RootDepth(-1) + dRootDepth;
                    break;
            }
        }
    }

    /**
     * @brief Methode d'initialisation pour les variables d'état du modèle
     * @param time la date du pas de temps courant
     */
    virtual void initValue(const vd::Time& /* time */)
    { RootDepth = 0.0; }

private:
    //Variables d'etat
    /**
     * @brief profondeur du front racinaire. (mm)
     */
    Var RootDepth;

    //Entrées
    /**
     * @brief temps thermique (°C.j)
     */
    Sync TT;
    /**
     * @brief le stade phenologique de la plante (cf CropDvtStade.hpp) (-)
     */
    Sync DvtStade;

    //Parametres du modele
    /**
     * @brief la profondeur du sol. (mm)
     */
    double soilDepth;
    /**
     * @brief la profondeur maximal atteignable par les racines. (mm)
     */
    double maxDepth;
    /**
     * @brief la croissance journaliere maximale du front racinaire (mm/j)
     */
    double rateDepth;
    /**
     * @brief la profondeur initiale à l'emergence (mm)
     */
    double iniDepth;

};
}
}//namespaces
DECLARE_DYNAMICS( record::cv::CropRoot); // balise specifique VLE
