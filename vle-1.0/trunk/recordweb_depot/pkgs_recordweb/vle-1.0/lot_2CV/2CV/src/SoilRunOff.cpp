/**
 * @file src/SoilRunOff.cpp
 * @author The RECORD Development Team (INRA)
 */

/*
 * Copyright (C) 2009 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <vle/extension/DifferenceEquation.hpp>// correspond a l'extension DEVS utilise ici

// raccourcis de nommage des namespaces frequement utilises
namespace ve = vle::extension;
namespace vd = vle::devs;
namespace vv = vle::value;

//definition du namespace de la classe du modele
namespace record {
namespace cv {

class SoilRunOff: public ve::DifferenceEquation::Multiple {
public:
    SoilRunOff(const vd::DynamicsInit& atom, const vd::InitEventList& events) :
        ve::DifferenceEquation::Multiple(atom, events)
    {
        // Variables d'etat gerees par ce composant
        RunOff = createVar("RunOff");
        Infiltration = createVar("Infiltration");

        // Variables  gerees par un autre  composant
        Rain = createSync("Rain");
        Irrigation = createSync("Irrigation");
        IncomingRunOff = createSync("IncomingRunOff");

        // Lecture des parametres
        //ces parametres ont une valeur par defaut utilise si la condition n'est pas definie
        fracRunOff = (events.exist("fracRunOff")) ? vv::toDouble(events.get("fracRunOff")) : 0.1;
    }

    virtual ~SoilRunOff() {}

    virtual void compute(const vd::Time& /*time*/)
    {
        RunOff = (Rain() + Irrigation() + IncomingRunOff()) * fracRunOff;
        Infiltration = (Rain() + Irrigation() + IncomingRunOff()) * (1 - fracRunOff);
    }

    virtual void initValue(const vd::Time& /* time */)
    {
        RunOff = 0.0;
        Infiltration = 0.0;
    }

private:
    //Variables d'etat
    Var RunOff;
    Var Infiltration;

    //Entrées
    Sync Rain;
    Sync Irrigation;
    Sync IncomingRunOff;

    //Parametres du modele
    double fracRunOff;

};
}
}//namespaces
DECLARE_DYNAMICS( record::cv::SoilRunOff); // balise specifique VLE

