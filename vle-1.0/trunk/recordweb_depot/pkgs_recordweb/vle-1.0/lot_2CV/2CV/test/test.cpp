/**
 * @file vle/share/template/package/src/simple.cpp
 * @author The VLE Development Team
 */

/*
 * VLE Environment - the multimodeling and simulation environment
 * This file is a part of the VLE environment (http://vle.univ-littoral.fr)
 * Copyright (C) 2003 - 2009 The VLE Development Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define BOOST_TEST_MAIN
#define BOOST_AUTO_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE package_test
#include <boost/test/unit_test.hpp>
#include <boost/test/auto_unit_test.hpp>
#include <boost/test/floating_point_comparison.hpp>

#include <ostream>

#include <vle/value.hpp>
#include <vle/vpz.hpp>
#include <vle/utils.hpp>
#include <vle/manager/Run.hpp>
#include <vle/manager/VLE.hpp>
#include <vle/oov/OutputMatrix.hpp>

namespace vz = vle::vpz;
namespace vu = vle::utils;
namespace vm = vle::manager;
namespace vo = vle::oov;
namespace va = vle::value;

struct F
{
    F()
    {
        vle::manager::init();
    }

    ~F()
    {
        vle::manager::finalize();
    }

};
BOOST_GLOBAL_FIXTURE(F)


BOOST_AUTO_TEST_CASE(test_2CV_parcelle)
{
    std::cout << "  test_2CV_parcelle " << std::endl;
    vu::Package::package().select("2CV");//TODO should be managed by meteo
    vz::Vpz vpz(vu::Path::path().getExternalPackageExpFile("2CV",
        "2CV-parcelle.vpz"));
    vz::Outputs::iterator itb =
            vpz.project().experiment().views().outputs().begin();
    vz::Outputs::iterator ite =
            vpz.project().experiment().views().outputs().end();
    for(;itb!=ite;itb++){
        itb->second.setLocalStream("", "storage", std::string());
    }
    vm::RunQuiet r;
    r.start(vpz);
    //checks that simulation has succeeded
    BOOST_REQUIRE_EQUAL(r.haveError(), false);

    //check the number of views
    vo::OutputMatrixViewList& out(r.outputs());
    BOOST_REQUIRE_EQUAL(out.size(),(vo::OutputMatrixViewList::size_type)9);
    {
        vo::OutputMatrix& view(out["vueLAI"]);
        va::MatrixView result(view.values());
        BOOST_REQUIRE_EQUAL(result.shape()[0],
            (va::MatrixView::size_type)5);
        BOOST_REQUIRE_EQUAL(result.shape()[1],
            (va::MatrixView::size_type)351);

        va::Matrix::index colALAI =
                view.column("Top model,2CV,CropFull:CropLAI","ALAI");
        va::Matrix::index colFracSen =
                view.column("Top model,2CV,CropFull:CropLAI","FracSen");
        va::Matrix::index colLAI =
                view.column("Top model,2CV,CropFull:CropLAI","LAI");
        va::Matrix::index collplant =
                view.column("Top model,2CV,CropFull:CropLAI","lplant");

        //check ALAI line 197
        BOOST_REQUIRE_CLOSE(va::toDouble(result[colALAI][197]),
            4.160459, 10e-5);
        //check FracSen line 239
        BOOST_REQUIRE_CLOSE(va::toDouble(result[colFracSen][239]),
            0.1378907, 10e-5);
        //check LAI line 260
        BOOST_REQUIRE_CLOSE(va::toDouble(result[colLAI][260]),
            5.790568, 10e-5);
        //check lplant line 299
        BOOST_REQUIRE_CLOSE(va::toDouble(result[collplant][299]),
            0.5805587, 10e-5);
    }
    {
        vo::OutputMatrix& view(out["vueAGB"]);
        va::MatrixView result(view.values());
        BOOST_REQUIRE_EQUAL(result.shape()[0],
            (va::MatrixView::size_type)6);
        BOOST_REQUIRE_EQUAL(result.shape()[1],
            (va::MatrixView::size_type)351);

        va::Matrix::index colAGBiomass =
                view.column("Top model,2CV,CropFull:CropAGB","AGBiomass");
        va::Matrix::index colHI =
                view.column("Top model,2CV,CropFull:CropAGB","HI");
        va::Matrix::index colHIpot =
                view.column("Top model,2CV,CropFull:CropAGB","HIpot");
        va::Matrix::index colYield =
                view.column("Top model,2CV,CropFull:CropAGB","Yield");
        va::Matrix::index coldayCount =
                view.column("Top model,2CV,CropFull:CropAGB","dayCount");

        //check AGBiomass line 205
        BOOST_REQUIRE_CLOSE(va::toDouble(result[colAGBiomass][205]),
            927.222617, 10e-5);
        //check HI line 253
        BOOST_REQUIRE_CLOSE(va::toDouble(result[colHI][233]),
            0.09, 10e-5);
        //check HIpot line 287
        BOOST_REQUIRE_CLOSE(va::toDouble(result[colHIpot][287]),
            0.506732, 10e-5);
        //check Yield line 294
        BOOST_REQUIRE_CLOSE(va::toDouble(result[colYield][294]),
            1209.301830, 10e-5);
        //check dayCount line 299
        BOOST_REQUIRE_CLOSE(va::toDouble(result[coldayCount][299]),
            26, 10e-5);
    }
}


BOOST_AUTO_TEST_CASE(test_2CV_decision)
{
    std::cout << "  test_2CV_decision " << std::endl;
    vz::Vpz vpz(vu::Path::path().getExternalPackageExpFile("2CV",
        "2CV-decision.vpz"));
    vz::Outputs::iterator itb =
            vpz.project().experiment().views().outputs().begin();
    vz::Outputs::iterator ite =
            vpz.project().experiment().views().outputs().end();
    for(;itb!=ite;itb++){
        itb->second.setLocalStream("", "storage", std::string());
    }
    vm::RunQuiet r;
    r.start(vpz);
    //checks that simulation has succeeded
    BOOST_REQUIRE_EQUAL(r.haveError(), false);

    //check the number of views
    vo::OutputMatrixViewList& out(r.outputs());
    BOOST_REQUIRE_EQUAL(out.size(),(vo::OutputMatrixViewList::size_type)2);

    vo::OutputMatrix& view(out["vueDecision"]);

    va::Matrix::index colDate = view.column("2CV_decision:Decision",".Date");
    va::Matrix::index colPeriodeSeche =
            view.column("2CV_decision:Decision","periodeSeche");
    va::Matrix::index colRecolte =
            view.column("2CV_decision:Decision","Recolte");
    va::Matrix::index colFinRecolte =
            view.column("2CV_decision:Decision","finRecolte");
    va::Matrix::index colsolPortant =
            view.column("2CV_decision:Decision","solPortant");
    va::Matrix::index colIrr =
            view.column("2CV_decision:Decision","Irrigation_09");


    va::MatrixView result(view.values());
    BOOST_REQUIRE_EQUAL(result.shape()[0],
        (va::MatrixView::size_type)22);
    BOOST_REQUIRE_EQUAL(result.shape()[1],
        (va::MatrixView::size_type)351);

    //check .Date line 110
    BOOST_REQUIRE_EQUAL(va::toString(result[colDate][110]), "1961-04-21");
    //check periodeSeche line 110
    BOOST_REQUIRE_EQUAL(va::toString(result[colPeriodeSeche][110]),
        "periodeSeche");

     //check Recolte line 247
     BOOST_REQUIRE_EQUAL(va::toString(result[colRecolte][247]),
         "Recolte:int-wait");
     //check Recolte line 248
     BOOST_REQUIRE_EQUAL(va::toString(result[colRecolte][248]),
         "Recolte:int-done");
     //check finRecolte line 278
     BOOST_REQUIRE_EQUAL(va::toString(result[colFinRecolte][278]),
         "finRecolte");
     //check solPortant line 169
     BOOST_REQUIRE_EQUAL(va::toString(result[colsolPortant][169]),
         "solPortant");
     //check solPortant line 235
     BOOST_REQUIRE_EQUAL(va::toString(result[colsolPortant][235]),
         "false");
     //check Irrigation_09 line 341
     BOOST_REQUIRE_EQUAL(va::toString(result[colIrr][341]),
         "Irrigation_09:ext-failed");
     //check periodeSeche line 110
     BOOST_REQUIRE_EQUAL(va::toString(result[colPeriodeSeche][110]),
         "periodeSeche");
}

BOOST_AUTO_TEST_CASE(test_2CV_multi_parcellaire)
{
    std::cout << "  test_2CV_multi_parcellaire " << std::endl;
    vu::Package::package().select("2CV");//TODO should be managed by meteo
    vz::Vpz vpz(vu::Path::path().getExternalPackageExpFile("2CV",
        "2CV-multi-parcellaire.vpz"));
    vz::Outputs::iterator itb =
            vpz.project().experiment().views().outputs().begin();
    vz::Outputs::iterator ite =
            vpz.project().experiment().views().outputs().end();
    for(;itb!=ite;itb++){
        itb->second.setLocalStream("", "storage", std::string());
    }
    vm::RunQuiet r;
    r.start(vpz);
    //checks that simulation has succeeded
    BOOST_REQUIRE_EQUAL(r.haveError(), false);

    //check the number of views
    vo::OutputMatrixViewList& out(r.outputs());
    BOOST_REQUIRE_EQUAL(out.size(),(vo::OutputMatrixViewList::size_type)9);

    vo::OutputMatrix& view(out["vueSWCB"]);
    va::MatrixView result(view.values());
    BOOST_REQUIRE_EQUAL(result.shape()[0],
        (va::MatrixView::size_type)73);
    BOOST_REQUIRE_EQUAL(result.shape()[1],
        (va::MatrixView::size_type)351);

    va::Matrix::index colS4 =
            view.column("Top model,2CV_2,SoilFull:SoilSWCB","S4");

    //check S4 line 350
    BOOST_REQUIRE_CLOSE(va::toDouble(result[colS4][350]),
       322.4714338, 10e-5);
}
