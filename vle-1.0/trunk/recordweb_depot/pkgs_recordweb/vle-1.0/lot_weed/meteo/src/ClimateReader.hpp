/**
 * @file ClimateReader.hpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2009-2011 INRA
 * Copyright (C) 2009-2011 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CLIMATE_READER_HPP
#define CLIMATE_READER_HPP 1

#include <Climate.hpp>
#include <fstream>

#include <boost/algorithm/string.hpp>
#include <vle/utils/Path.hpp>
#include <boost/version.hpp>
#include <boost/cstdint.hpp>
#include <vle/utils.hpp>

#if BOOST_VERSION < 103600
#include <boost/spirit/utility/chset.hpp>
#include <boost/spirit/core.hpp>
#include <boost/spirit/utility/confix.hpp>
#include <boost/spirit/utility/lists.hpp>
#include <boost/spirit/utility/escape_char.hpp>

using namespace boost::spirit;

#else
#include <boost/spirit/include/classic_chset.hpp>
#include <boost/spirit/include/classic_core.hpp>
#include <boost/spirit/include/classic_confix.hpp>
#include <boost/spirit/include/classic_lists.hpp>
#include <boost/spirit/include/classic_escape_char.hpp>

using namespace BOOST_SPIRIT_CLASSIC_NS;

#endif
namespace meteo {

/**
 * @brief a class in charge of reading the datas of a file line by line.
 *
 * The datas expected are structured by columns.
 */
class ClimateReader : public Climate
{
public:
    ClimateReader(const vle::devs::DynamicsInit& init,
		  const vle::devs::InitEventList& events,
		  const std::string& filePath);
    virtual ~ClimateReader();

    /**
     * @brief initialize the inherited classes
     *
     * initialize also the line parser.
     * and according to the time read the first line.
     */
    void initValue (const vle::devs::Time& time)
    {
        Climate::initValue(time);
        Multiple::initValue(time);

        list_csv_item =
            !(
                confix_p('\"', *c_escape_ch_p, '\"')
                | longest_d[real_p | int_p]
                | +alnum_p
                );

        list_csv = list_p(
            list_csv_item[push_back_a(mColumnValues)],
            +chset_p(columnSeparator().c_str()));

        readLine(time + deltaTime);
    }

    /**
     * @brief compute the model
     *
     * Computing the model, in this case means reading and parsing a
     * line, but also feed the state Variables of the difference equation
     */
    void compute(const vle::devs::Time& time)
    { readLine(time + deltaTime); }

protected:
    typedef std::map < std::string, int > columns_t; //!< to store the
                                                     //!id of the column
    typedef std::vector < std::string > columnValues_t; //!<to store
                                                        //!the values
                                                        //!of a parsed
                                                        //!lineline
    /**
     * @brief store the declaration of the column
     *
     * @param name of the column
     * @param index of the column
     */
    void addColumn(const std::string& name, int index)
    { mColumns[name] = index; }

    /**
     * @brief to create all the variables corresponding to the columns
     *
     */
    void createVariables()
    {
        for (columns_t::const_iterator it = mColumns.begin();
             it != mColumns.end(); ++it) {
            createVariable(it->first);
        }
    }

    /**
     * @brief getter of the file stream
     *
     */
    std::ifstream& getFile()
    { return mFile; }

    /**
     * @brief getter of the file path
     *
     */
    const std::string& getFilePath() const
    { return mFilePath; }

    /**
     * @brief jump to the fist line of interrest
     *
     */
    void jumpLines(unsigned int n);

/*  - - - - - - - - - - - - - --ooOoo-- - - - - - - - - - - -  */

    /**
     * @brief getter of the columnSeparator
     *
     */
    virtual std::string columnSeparator() const
    { return " \n\t"; }
    /**
     * @brief getter of the dateFormat
     *
     */
    virtual std::string dateFormat() const
    { return ""; }
    /**
     * @brief getter of the yearColumn index
     *
     */
    virtual int yearColumn() const
    { return -1; }
    /**
     * @brief getter of the monthColumn index
     *
     */
    virtual int monthColumn() const
    { return -1; }
    /**
     * @brief getter of the weekColumn index
     *
     */
    virtual int weekColumn() const
    { return -1; }
    /**
     * @brief getter of the dayColumn index.
     *
     */
    virtual int dayColumn() const
    { return -1; }

private:
    /**
     * @brief check if the model is on the good line
     *
     * compare the date of the model, and the date of the line of
     * the file.
     */
    bool isGoodLine(const vle::devs::Time& time);

    /**
     * @brief read the line and control the line.
     */
    void nextLine(const vle::devs::Time& time);

    /**
     * @brief parse the string of a line, and store the items inside a vector.
     */
    bool parseLine(std::string& line);

    /**
     * @brief set the variables, by converting each string item of the vector.
     */
    void readLine(const vle::devs::Time& time);

    std::string mFilePath; //!< file path
    std::ifstream mFile; //!< file stream

    columns_t mColumns; //!< file map of column index
    columnValues_t mColumnValues; //!< file vector or the values of
                                  //!the parsed line

    rule <> list_csv; //!< rule to parse an item
    rule <> list_csv_item; //! rue to parse a line
};

} // namespace meteo

#endif
