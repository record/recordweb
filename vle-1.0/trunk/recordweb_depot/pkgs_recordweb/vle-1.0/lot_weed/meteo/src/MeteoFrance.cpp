/**
 * @file GenericWithHeader.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2009-2011 INRA
 * Copyright (C) 2009-2011 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Header.hpp>
#include <boost/algorithm/string.hpp>

namespace meteo {

class MeteoFrance : public Header
{
public:
    MeteoFrance(const vle::devs::DynamicsInit& init,
		const vle::devs::InitEventList& events) :
	Header(init, events, vle::value::toString(events.get("meteo_file")))
    {
        std::vector < std::string > fields;

        jumpLines(1);
        parseHeader(fields);
        parseColumnHeader(fields, "Tmin", "Tmin");
        parseColumnHeader(fields, "Tmax", "Tmax");
        parseColumnHeader(fields, "Pluie", "Rain");
        parseColumnHeader(fields, "ETP", "ETP");
        parseColumnHeader(fields, "Rayonnement Visible", "RG");
        createVariables();
    }

    virtual ~MeteoFrance()
    { }

private:
    int yearColumn() const
    { return 0; }

    int monthColumn() const
    { return 1; }

    int dayColumn() const
    { return 2; }
};

DECLARE_NAMED_DYNAMICS(MeteoFrance, MeteoFrance);

} // namespace meteo
