/**
 * @file Header.hpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2009-2011 INRA
 * Copyright (C) 2009-2011 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HEADER_HPP
#define HEADER_HPP 1

#include "ClimateReader.hpp"

namespace meteo {

/*! \class Header
 *  \brief
 */

class Header : public ClimateReader
{
public:
    Header(const vle::devs::DynamicsInit& init,
	   const vle::devs::InitEventList& events,
	   const std::string& filePath);
    virtual ~Header();

protected:
    typedef std::map < std::string, std::string > columns_t;

    void addColumn(const std::string& colName, const std::string& varName)
    { mColumns[colName] = varName; }

    const columns_t& columns() const
    { return mColumns; }

    void parseColumnHeader(const std::vector < std::string >& fields,
			   const std::string& colName,
			   const std::string& name);

    void parseHeader(std::vector < std::string >& fields);

    void parseLine(std::vector < std::string >& fields,
		   std::string& line);

private:
    // name of column -> name of variable
    std::map < std::string, std::string > mColumns;
};

} // namespace meteo

#endif
