/**
 * @file GenericGenerator.hpp
 * @author The RECORD Development Team (INRA)
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2009-2011 INRA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ClimateGenerator.hpp"

#if BOOST_VERSION < 103600

using namespace boost::spirit;

#else

using namespace BOOST_SPIRIT_CLASSIC_NS;

#endif

namespace meteo {

std::vector<double> ParserItemSta::parseItem(std::ifstream& stream,
                                             unsigned int nbrow,
                                             unsigned int nbcol)
{
    std::string line;
    std::vector<double> params;
    //a comment line
    if (stream.eof()) {
        throw vle::utils::FileError(boost::format(
            _(" ClimateGenerator : initialization file error 1")));
    }
    getline(stream, line);
    int count = 0;
    parse(line.c_str(), ch_p('/')[increment_a(count)]);

    for (unsigned int i = 0; i < nbrow; i++) {
        //a line of nbcol doubles
        if (stream.eof()) {
            throw vle::utils::FileError(boost::format(
                _(" ClimateGenerator : initialization file error 2")));
        }
        getline(stream, line);
        parse(line.c_str(), *(real_p[push_back_a(params)] | anychar_p));
        if (params.size() != (i + 1) * nbcol) {
            throw vle::utils::FileError(boost::format(
                _(" ClimateGenerator : initialization file error 3")));
        }
    }
    return params;
}

/*  - - - - - - - - - - - - - --ooOoo-- - - - - - - - - - - -  */

Fourier::Fourier(std::ifstream& stream, unsigned int nbrow,
                 unsigned int nbcol, int integrationPeriod)
{
    n = nbcol;
    T = integrationPeriod;
    std::vector<double> itemsta =
            ParserItemSta::parseItem(stream, nbrow, nbcol);
    if ((nbrow != 2) and (nbcol != 4)) {
        throw vle::utils::FileError(boost::format(
            _(" ClimateGenerator : initialization file error 4")));
    }
    for (unsigned int i = 0; i < nbrow; i++) {
        for (unsigned int j = 0; j < nbcol; j++) {
            if (i == 0) {
                a[j] = itemsta[(i) * nbcol + j];
            } else {
                b[j] = itemsta[(i) * nbcol + j];
            }
        }
    }
}

Fourier::Fourier()
{ }

double Fourier::getValue(unsigned int dayOfYear)
{
    double value, omega;

    omega = 2 * Fourier::PI / this->T;
    value = this->a[0] / 2;

    for (int i = 1; i < this->n; ++i) {

        value += this->b[i] * sin(omega * i * dayOfYear) + this->a[i] * cos(
            omega * i * dayOfYear);
    }
    return (value);
}

/*  - - - - - - - - - - - - - --ooOoo-- - - - - - - - - - - -  */

ClimateGenerator::ClimateGenerator(const vle::devs::DynamicsInit& init,
                                   const vle::devs::InitEventList& events) :
    Climate(init, events)
{
    //WARNING parameters that are constants
//    mlatitude = 48.1; //Rennes
    mlatitude = 43.617; //Toulouse
    mendserial = 8;
    mwet = true;
    mnormalmean = 0.0;
    mnormalsigma = 1.0;
    mFourierIntegrationPeriod = 365;
    //Fouriers
    std::string filepath = getPackageDataFile(events.getString("stats-file"));
    parseFile(filepath);
    createVariable("ETP");
    createVariable("RG");
    createVariable("Rain");
    createVariable("Tmax");
    createVariable("Tmin");
    createVariable("Tmoy");

    mserialength = 0;
    ETP_wo = 0;
    Resid_min = 0;
    Resid_max = 0;
    Resid_rad = 0;

}

void ClimateGenerator::compute(const vle::devs::Time& julianDay)
{
    unsigned int dayOfYear = vle::utils::DateTime::dayOfYear(julianDay + deltaTime);
    weatherGen(dayOfYear);
}

void ClimateGenerator::initValue(const vle::devs::Time& julianDay)
{
    Climate::initValue(julianDay);
    Multiple::initValue(julianDay);
    unsigned int dayOfYear = vle::utils::DateTime::dayOfYear(julianDay + deltaTime);
    weatherGen(dayOfYear);
}

ClimateGenerator::~ClimateGenerator()
{ }

void ClimateGenerator::parseFile(const std::string& filepath)
{
    std::ifstream filestream(filepath.c_str());

    /* Wet & Dry series: */
    /*  probability of occurence (PR) and average lenght (MN) */
    mfouriers.insert(std::pair<std::string, Fourier>(
                         "WetMN", Fourier(filestream, 2, 4,
                                          mFourierIntegrationPeriod)));
    mfouriers.insert(std::pair<std::string, Fourier>(
                         "WetLPR",  Fourier(filestream, 2, 4,
                                            mFourierIntegrationPeriod)));
    mfouriers.insert(std::pair<std::string, Fourier>(
                         "WetLMN", Fourier(filestream, 2, 4,
                                           mFourierIntegrationPeriod)));
    mfouriers.insert(std::pair<std::string, Fourier>(
                         "DryMN", Fourier(filestream, 2, 4,
                                          mFourierIntegrationPeriod)));
    mfouriers.insert(std::pair<std::string, Fourier>(
                         "DryLPR", Fourier(filestream, 2, 4,
                                           mFourierIntegrationPeriod)));
    mfouriers.insert(std::pair<std::string, Fourier>(
                         "DryLMN", Fourier(filestream, 2, 4,
                                           mFourierIntegrationPeriod)));

    /* Precipitation probability of occuarence (PR)of M and L groups */
    /* and mean (MN) amount, mm */
    mfouriers.insert(std::pair<std::string, Fourier>(
                         "RainMPR", Fourier(filestream, 2,
                                            4, mFourierIntegrationPeriod)));
    mfouriers.insert(std::pair<std::string, Fourier>(
                         "RainLPR", Fourier(filestream, 2,
                                            4, mFourierIntegrationPeriod)));
    mfouriers.insert(std::pair<std::string, Fourier>(
                         "RainMMN", Fourier(filestream, 2,
                                            4, mFourierIntegrationPeriod)));
    mfouriers.insert(std::pair<std::string, Fourier>(
                         "RainLMN", Fourier(filestream, 2,
                                            4, mFourierIntegrationPeriod)));

    /* Min temperature */
    /* mean (MN) and sd (SD) for wet/dry  (W/D) series, C */
    mfouriers.insert(std::pair<std::string, Fourier> (
                         "MinWMN", Fourier(filestream, 2, 4,
                                           mFourierIntegrationPeriod)));
    mfouriers.insert(std::pair<std::string, Fourier> (
                         "MinWSD", Fourier(filestream, 2, 4,
                                           mFourierIntegrationPeriod)));
    mfouriers.insert(std::pair<std::string, Fourier> (
                         "MinDMN", Fourier(filestream, 2, 4,
                                           mFourierIntegrationPeriod)));
    mfouriers.insert(std::pair<std::string, Fourier> (
                         "MinDSD", Fourier(filestream, 2, 4,
                                           mFourierIntegrationPeriod)));
    mfouriers.insert(std::pair<std::string, Fourier> (
                         "MinCor", Fourier(filestream, 2, 4,
                                           mFourierIntegrationPeriod)));

    /* Max temperature */
    /* mean (MN) and sd (SD) for wet/dry  (W/D) series, C */
    mfouriers.insert(std::pair<std::string, Fourier> (
                         "MaxWMN", Fourier(filestream, 2, 4,
                                           mFourierIntegrationPeriod)));
    mfouriers.insert(std::pair<std::string, Fourier> (
                         "MaxWSD", Fourier(filestream, 2, 4,
                                           mFourierIntegrationPeriod)));
    mfouriers.insert(std::pair<std::string, Fourier> (
                         "MaxDMN", Fourier(filestream, 2, 4,
                                           mFourierIntegrationPeriod)));
    mfouriers.insert(std::pair<std::string, Fourier> (
                         "MaxDSD", Fourier(filestream, 2, 4,
                                           mFourierIntegrationPeriod)));
    /* Coefficients de correlation */
    mfouriers.insert(std::pair<std::string, Fourier> (
                         "MaxCor", Fourier(filestream, 2, 4,
                                           mFourierIntegrationPeriod)));

    /* Radiation mean (MN) and sd (SD) for wet/dry (W/D) series, hours */
    mfouriers.insert(std::pair<std::string, Fourier> (
                         "RadWMN", Fourier(filestream, 2, 4,
                                           mFourierIntegrationPeriod)));
    mfouriers.insert(std::pair<std::string, Fourier> (
                         "RadWSD", Fourier(filestream, 2, 4,
                                           mFourierIntegrationPeriod)));
    mfouriers.insert(std::pair<std::string, Fourier> (
                         "RadDMN", Fourier(filestream, 2, 4,
                                           mFourierIntegrationPeriod)));
    mfouriers.insert(std::pair<std::string, Fourier> (
                         "RadDSD", Fourier(filestream, 2, 4,
                                           mFourierIntegrationPeriod)));
    /* Coefficients de correlation */
    mfouriers.insert(std::pair<std::string, Fourier> (
                         "RadCor", Fourier(filestream, 2, 4,
                                           mFourierIntegrationPeriod)));

    /* Linear Regression of ETP */
    std::vector<double> ETPregression = ParserItemSta::parseItem(filestream, 1,
        3);
    malpha = ETPregression[0];
    mbeta = ETPregression[1];
    mgamma = ETPregression[2];
}

void ClimateGenerator::weatherGen(unsigned int dayOfYear)
{
    if (mserialength <= 0) {
        mwet = not mwet;
        if (mwet) {
            mserialength = wetSeries(dayOfYear);
        } else {
            mserialength = drySeries(dayOfYear);
        }
    }
    mserialength = mserialength - 1;
    //WARNING dayOfYear rather than dayOfYear+1 (bug??)
    weatherOutput(dayOfYear + 1);
}

int ClimateGenerator::wetSeries(unsigned int dayOfYear)
{
    double p[15];
    int s = 0;
    double lambda;
    /* Probablity interval for a mixed distribution */
    p[0] = mfouriers["WetLPR"].getValue(dayOfYear);
    p[1] = 1;

    /* Lenght of series */
    switch (choice(2, p)) {
    case 1: /* Series > 8 */
        lambda = 1 / mfouriers["WetLMN"].getValue(dayOfYear);
        if(lambda <= 0) lambda = 0.001;
        s = (int) (rand().exponential(lambda) + 0.5 + mendserial);
        break;

    case 2: /* Series < 8 */
        lambda = mfouriers["WetMN"].getValue(dayOfYear);
        if(lambda <= 0) lambda = 0.001;
        s = (int) (rand().exponential(lambda) + 0.6);
        break;
    }
    if (s == 0) s = 1;
    return (s);
}

int ClimateGenerator::drySeries(unsigned int dayOfYear)
{
    double p[15];
    int s = 0;
    double lambda;
    /* Probablity interval for a mixed distribution */
    p[0] = mfouriers["DryLPR"].getValue(dayOfYear);
    p[1] = 1;
    /* Lenght of series */
    switch (choice(2, p)) {
    case 1: /* Series > 8 */
        lambda = 1 / mfouriers["DryLMN"].getValue(dayOfYear);
        if(lambda <= 0) lambda = 0.001;
        s = (int) (rand().exponential(lambda) + 0.5 + mendserial);
        break;
    case 2: /* Series < 8 */
        lambda = mfouriers["DryMN"].getValue(dayOfYear);
        if(lambda <= 0) lambda = 0.001;
        s = (int) (rand().exponential(lambda) + 0.6);
        break;
    }
    if (s == 0) s = 1;
    return (s);
}

void ClimateGenerator::weatherOutput(unsigned int dayOfYear)
{

    double radiation;
    double MaxRad, MinRad;

    /* Temperature, C */
    double tmin, tmax, rg, tmoy;
    tmin = minTem(dayOfYear);
    tmax = maxTem(dayOfYear);
    tmoy = ((tmin + tmax) / 2.0);
    setVariable("Tmin", tmin);
    setVariable("Tmax", tmax);
    setVariable("Tmoy", tmoy);

    /* Precipitation, mm */
    if (mwet) {
        setVariable("Rain", precipitation(dayOfYear));
    } else {
        setVariable("Rain", 0);
    }

    /* Radiation, MJ/m2/j */
    radiation = rad(dayOfYear);
    std::pair<double, double> rads = maxMinRadiation(dayOfYear);
    MinRad = rads.first;
    MaxRad = rads.second;

    if (radiation >= MaxRad) {
        rg = MaxRad;
    } else {
        if (radiation <= MinRad) {
            rg = MinRad;
        } else {
            rg = radiation;
        }
    }
    setVariable("RG", rg);

    /* Etp based on linear regression */
    ETP_wo = malpha * tmoy * tmoy + mbeta * rg * rg + mgamma * ETP_wo;
    setVariable("ETP", ETP_wo);
}

double ClimateGenerator::minTem(unsigned int dayOfYear)
{
    double mean, sigma, ro;

    /* Calculate mean and sd for the current day considering the series type */
    if (mwet) {
        mean = mfouriers["MinWMN"].getValue(dayOfYear);
        sigma = mfouriers["MinWSD"].getValue(dayOfYear);
    } else {
        mean = mfouriers["MinDMN"].getValue(dayOfYear);
        sigma = mfouriers["MinDSD"].getValue(dayOfYear);
    }

    ro = mfouriers["MinCor"].getValue(dayOfYear);

    /* Calculate current min temperature */
    double newResid;
    double resid;

    if (dayOfYear == 1) {/* If day ==1, */
        resid = rand().normal(mnormalmean, mnormalsigma);
        newResid = Resid_min;
    } else {/* for other days temperature is autocorrelated */
        resid = rand().normal(mnormalmean, mnormalsigma);
        newResid = (resid + ro * Resid_min) / sqrt(1 + ro * ro);
    }

    Resid_min = resid;
    return (newResid * sigma + mean);
}

double ClimateGenerator::maxTem(unsigned int dayOfYear)
{
    double mean, sigma, ro;

    /* Calculate mean and sd for the current day considering the series type */
    if (mwet) {
        mean = mfouriers["MaxWMN"].getValue(dayOfYear);
        sigma = mfouriers["MaxWSD"].getValue(dayOfYear);
    } else {
        mean = mfouriers["MaxDMN"].getValue(dayOfYear);
        sigma = mfouriers["MaxDSD"].getValue(dayOfYear);
    }
    ro = mfouriers["MaxCor"].getValue(dayOfYear);

    /* Calculate current max temperature */
    double newResid;
    double resid;
    if (dayOfYear == 1) {/* If day ==1, */
        resid = rand().normal(mnormalmean, mnormalsigma);
        newResid = Resid_max;
    } else {/* for other days temperature is autocorrelated */
        resid = rand().normal(mnormalmean, mnormalsigma);
        newResid = (resid + ro * Resid_max) / sqrt(1 + ro * ro);
    }

    Resid_max = resid;

    return (newResid * sigma + mean);
}

double ClimateGenerator::precipitation(unsigned int dayOfYear)
{
    double w=0;
    double p[10], x, lambda;

    /*for  CLAIRE ONLY! Precipitaion should be adjested by this vector */
    /*to compensate decrease in in mean monthly precipittaion */
    /* The following vecor for RES */
    /* double R[]= {1.26,1.99,1.90,1.44,1.40,1.26,1.99,1.58,1.54,1.27,1.33,1.77}; */

    /* Probability interval for L, M and S groups */
    p[0] = mfouriers["RainLPR"].getValue(dayOfYear); /* (0,p[0])    -L */
    p[1] = p[0] + mfouriers["RainMPR"].getValue(dayOfYear); /* (p[0],p[1]) -M */
    p[2] = 1; /* (p[1],p[2]) -S */

    /* Amount of precipitation, mm */
    switch (choice(3, p)) {
    case 1: /* L - precipitation, average value */
        w = mfouriers["RainLMN"].getValue(dayOfYear);
        break;
    case 2: /* M - precipitation, exponentially distributed */
        lambda = 1 / mfouriers["RainMMN"].getValue(dayOfYear);
        if(lambda <= 0) lambda = 0.001;
        w = rand().exponential(lambda);
        break;
    case 3: /* S - precipitation, uniformly distributed */
        w = 0.2;
        x = 3 * rand().getDouble();
        if (x < 1) w = 0.1;
        if (2 < x) w = 0.3;
        break;
    };

    return (w);
}

double ClimateGenerator::rad(unsigned int dayOfYear)
{
    double mean, sigma, ro;

    /* Calculate mean and sd for the current day considering the series type */
    if (mwet) {
        mean = mfouriers["RadWMN"].getValue(dayOfYear);
        sigma = mfouriers["RadWSD"].getValue(dayOfYear);
    } else {
        mean = mfouriers["RadDMN"].getValue(dayOfYear);
        sigma = mfouriers["RadDSD"].getValue(dayOfYear);
    }

    ro = mfouriers["RadCor"].getValue(dayOfYear);

    /* Calculate current radiation  */
    double newResid;
    double resid;
    if (dayOfYear == 1) {/* If day == 1 than */
        resid = rand().normal(mnormalmean, mnormalsigma);
        newResid = Resid_rad;
    } else {
        resid = rand().normal(mnormalmean, mnormalsigma);
        newResid = (resid + ro * Resid_rad) / sqrt(1 + ro * ro);
    }
    Resid_rad = resid;
    return (newResid * sigma + mean);
}

std::pair<double, double> ClimateGenerator::maxMinRadiation(
    unsigned int dayOfYear)
{
    double sd, h, ra, xlat, scon = 1370; /* W/m2 */
    double MinRad, MaxRad;

    /* Change from degree to radian */
    xlat = mlatitude * 2 * Fourier::PI / 360;

    /*solar declination */
    sd = 0.41 * cos(2.0 * Fourier::PI * (dayOfYear - 172.) / 365.);
    h = acos(-tan(xlat) * tan(sd));

    /*radiation at the top of the atmos */
    ra = (86400. / Fourier::PI) * scon * 1.035 * (h * sin(xlat) * sin(sd)
            + cos(xlat) * cos(sd) * sin(h));

    /*If sunshine is zero, then a = 0.10 */
    MinRad = (ra * 0.1) / 1000000.;

    /* radiation (MJ/m2) = (ra*a)/1000000 */

    /* If sunshine == daylength the a= 0.34, b=0.46 */
    MaxRad = ra * 0.8 / 1000000.;

    return std::pair<double, double> (MinRad, MaxRad);
}

int ClimateGenerator::choice(int n, double p[])
{
    int i;
    double x;
    x = rand().getDouble();
    for (i = 0; i < n; ++i) {
        if (x <= p[i]) {
            return i + 1;
        }
    }
    return n;
}

DECLARE_NAMED_DYNAMICS(ClimateGenerator, ClimateGenerator);

}//namespace meteo
