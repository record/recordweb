/**
 * @file Agroclim.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2009-2011 INRA
 * Copyright (C) 2009-2011 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Header.hpp>

namespace meteo {

class Agroclim : public Header
{
public:
    Agroclim(const vle::devs::DynamicsInit& init,
	     const vle::devs::InitEventList& events) :
	Header(init, events, vle::value::toString(events.get("meteo_file")))
    {
        std::vector < std::string > fields;

        jumpLines(51);
        parseHeader(fields);
        parseColumnHeader(fields, "TN", "Tmin");
        parseColumnHeader(fields, "TX", "Tmax");
        parseColumnHeader(fields, "RR", "Rain");
        parseColumnHeader(fields, "ETP", "ETP");
        parseColumnHeader(fields, "RG", "RG");
        parseColumnHeader(fields, "TM", "Tmoy");
        createVariables();
    }

    virtual ~Agroclim()
    { }

private:
    int yearColumn() const
    { return 1; }

    int monthColumn() const
    { return 2; }

    int dayColumn() const
    { return 3; }

    std::string columnSeparator() const
    { return ";"; }
};

DECLARE_NAMED_DYNAMICS(Agroclim, Agroclim);

} // namespace meteo
