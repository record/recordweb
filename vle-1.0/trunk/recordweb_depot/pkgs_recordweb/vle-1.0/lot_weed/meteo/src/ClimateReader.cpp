/**
 * @file ClimateReader.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2009-2011 INRA
 * Copyright (C) 2009-2011 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <ClimateReader.hpp>


namespace vu = vle::utils;

namespace meteo {

ClimateReader::ClimateReader(const vle::devs::DynamicsInit& init,
			     const vle::devs::InitEventList& events,
			     const std::string& filePath) :
    Climate(init, events),
    mFilePath(vle::utils::Path::path().getPackageDataFile(filePath)),
    mFile(getFilePath().c_str())
{
    if (not mFile) {
	throw vle::utils::ModellingError(
	    vle::fmt(_("[%1%] ClimateReader - cannot"	\
		       " open file : %2%"))
	    % getModelName() % mFilePath);
    }
}

ClimateReader::~ClimateReader()
{ }

bool ClimateReader::isGoodLine(const vle::devs::Time& time)
{
    unsigned int day = 0;
    unsigned int week = 0;
    unsigned int month = 0;
    unsigned int year = 0;

    if (dayColumn() >= 0) {
	try {
	    day = boost::lexical_cast < unsigned int >(
		mColumnValues[dayColumn()]);
	} catch (...) {
	    throw vle::utils::ModellingError(
		vle::fmt(_("[%1%] ClimateReader - bad format "	\
			   "or bad index for day column"))
		% getModelName());
	}
    }
    if (monthColumn() >= 0) {
	try {
	    month = boost::lexical_cast < unsigned int >(
		mColumnValues[monthColumn()]);
	} catch (...) {
	    throw vle::utils::ModellingError(
		vle::fmt(_("[%1%] ClimateReader - bad format "	\
			   "or bad index for month column"))
		% getModelName());
	}
    }
    if (yearColumn() >= 0) {
	try {
	    year = boost::lexical_cast < unsigned int >(
		mColumnValues[yearColumn()]);
	} catch (...) {
	    throw vle::utils::ModellingError(
		vle::fmt(_("[%1%] ClimateReader - bad format "	\
			   "or bad index for year column"))
		% getModelName());
	}
    }
    return (year == 0 or vle::utils::DateTime::year(time) == year) and
	(month == 0 or vle::utils::DateTime::month(time) == month) and
	(week == 0 or vle::utils::DateTime::weekOfYear(time) == week) and
	(day == 0 or vle::utils::DateTime::dayOfMonth(time) == day);
}

void ClimateReader::jumpLines(unsigned int n)
{
    std::string line;

    for (boost::uint32_t i = 0; i < n; ++i) {
	getline(getFile(), line);
    }
}

void ClimateReader::nextLine(const vle::devs::Time& time)
{
    std::string line;
    bool found = false;
    bool stop = false;

    do {
	getline(mFile, line);
	if (not line.empty()) {
	    if (parseLine(line)) {
		found = isGoodLine(time);
	    } else {
		stop = true;
	    }
	}
    } while (not stop and not found and not mFile.eof() and mFile.good());
    if (not found) {
	throw vle::utils::FileError(
	    boost::format(_("[%1%] Meteo: invalid date %2% or invalid data"))
	    % getModelName() % vle::utils::DateTime::toJulianDay(time));
    }
}

bool ClimateReader::parseLine(std::string& line)
{
    mColumnValues.clear();
    boost::trim(line);

    parse_info <> result = parse (line.c_str(), list_csv);

    if (not result.hit) {
	mColumnValues.clear();
    }
    return result.hit;
}

void ClimateReader::readLine(const vle::devs::Time& time)
{
    nextLine(time);
    if (not mColumnValues.empty()) {
	for (std::map < std::string, int >::const_iterator
		 it = mColumns.begin(); it != mColumns.end(); ++it) {
	    setVariable(it->first,
			boost::lexical_cast <double>(
			    mColumnValues.at(it->second)));
	}
    }
}

} // namespace meteo
