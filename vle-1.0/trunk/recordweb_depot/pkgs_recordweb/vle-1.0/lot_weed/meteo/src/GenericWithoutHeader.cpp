/**
 * @file GenericWithHeader.cpp
 * @author The RECORD Development Team (INRA) and the VLE Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2009-2011 INRA
 * Copyright (C) 2009-2011 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <NoHeader.hpp>

namespace meteo {

class GenericWithoutHeader : public NoHeader
{
public:
    GenericWithoutHeader(const vle::devs::DynamicsInit& init,
			 const vle::devs::InitEventList& events) :
	NoHeader(init, events, vle::value::toString(events.get("meteo_file"))),
	mBegin(0),
	mColumnSeparator(" \t\n"),
	mDateFormat(""),
	mYearColumn(-1),
	mMonthColumn(-1),
	mWeekColumn(-1),
	mDayColumn(-1)
    {
        parseParameters(events);
        jumpLines(mBegin - 2);
        for (mapping::const_iterator it = mMapping.begin();
             it != mMapping.end(); ++it) {
            addColumn(it->first, it->second);
        }
        createVariables();
    }

    virtual ~GenericWithoutHeader()
    { }

private:
    void parseParameters(const vle::devs::InitEventList& events)
    {
        if (events.exist("begin")) {
            mBegin = vle::value::toInteger(events.get("begin"));
        }

        if (events.exist("columns")) {
            const vle::value::Map& mapping = events.getMap("columns");
            const vle::value::MapValue& lst = mapping.value();
            for (vle::value::MapValue::const_iterator it = lst.begin();
                 it != lst.end(); ++it) {
                std::string varName = it->first;
                int index = vle::value::toInteger(it->second);

                mMapping.push_back(std::make_pair(varName, index));
            }
        }

        if (events.exist("column_separator")) {
            mColumnSeparator = vle::value::toString(
                events.get("column_separator"));
        }
        if (events.exist("date_format")) {
            mDateFormat = vle::value::toString(
                events.get("date_format"));
        }
        if (events.exist("year_column")) {
            mYearColumn = vle::value::toInteger(events.get("year_column"));
        }
        if (events.exist("month_column")) {
            mMonthColumn = vle::value::toInteger(
                events.get("month_column"));
        }
        if (events.exist("week_column")) {
            mWeekColumn = vle::value::toInteger(events.get("week_column"));
        }
        if (events.exist("day_column")) {
            mDayColumn = vle::value::toInteger(events.get("day_column"));
        }
    }

/*  - - - - - - - - - - - - - --ooOoo-- - - - - - - - - - - -  */

    std::string columnSeparator() const
    { return mColumnSeparator; }

    std::string dateFormat() const
    { return mDateFormat; }

    int yearColumn() const
    { return mYearColumn; }

    int monthColumn() const
    { return mMonthColumn; }

    int weekColumn() const
    { return mWeekColumn; }

    int dayColumn() const
    { return mDayColumn; }

/*  - - - - - - - - - - - - - --ooOoo-- - - - - - - - - - - -  */

    typedef std::vector < std::pair < std::string, int > > mapping;

    int mBegin;
    mapping mMapping;
    std::string mColumnSeparator;
    std::string mDateFormat;
    int mYearColumn;
    int mMonthColumn;
    int mWeekColumn;
    int mDayColumn;
};

DECLARE_NAMED_DYNAMICS(GenericWithoutHeader, GenericWithoutHeader);

} // namespace meteo
