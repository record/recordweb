/**
 * @file test/conf_web.cpp
 * @author Nathalie Rousse, INRA RECORD team member.
 */

/** @file
 *
 * Copyright (C) 2012-2013 INRA.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* Description ***************************************************************/
/** @file
 *
 * @brief
 * Fichier de traitements de construction de trames de fichiers de configuration web (dicos de meta, web_conf et web_applis de web)
 *
 *****************************************************************************/

/** @file
 *
 * Precisions *****************************************************************
 *
 * Traitements de construction de trames de fichiers de configuration web (dicos de meta, web_conf et web_applis de web)
 *
 *****************************************************************************/

#include <conf_web.hpp>

#include <vle/vpz.hpp>
#include <vle/vpz/Condition.hpp>
#include <vle/graph/Model.hpp>
#include <vle/graph/CoupledModel.hpp>
#include <vle/manager.hpp>
#include <vle/value.hpp>

namespace vu = vle::utils;
namespace vz = vle::vpz;
namespace vg = vle::graph;
namespace vv = vle::value;

namespace conf_web {

/********************************************************************* 
 *
 *      Methodes d'application de methodes de FichierVpzScenario
 *       sur chacun des fichiers vpz scenario d'un paquet donne
 *
 *********************************************************************/

/********************************************************************* 
 * Construction dicos de chacun des fichiers vpz scenario d'un paquet
 *********************************************************************/
std::string TousDicos( std::string nomDuPaquet ){ 

    vle::manager::init(); 

    std::string nomPaquet = nomDuPaquet;
    std::string nomFichierVpz;
	conf_web::FichierVpzScenario *scn;

    /* liste des fichiers vpz scenario de simulation sous exp */
    vu::Package::package().select( nomPaquet ); 
    vu::PathList vpzlist = vu::Path::path().getInstalledExperiments();

    std::string msg = (std::string)" \n";
    msg = msg + (std::string)"-------------------------------------------------------------------------\n";
    msg = msg + (std::string)"[Configuration web] Construction dicos de tous les fichiers vpz scenario du paquet " + nomPaquet + "\n";

    for ( vu::PathList::const_iterator it = vpzlist.begin(); it != vpzlist.end(); it++ ){
        nomFichierVpz = vu::Path::filename(*it);
	    scn = new conf_web::FichierVpzScenario( nomPaquet, nomFichierVpz );
	    msg = msg + scn->Dicos();
    }

    return msg;
}

/********************************************************************* 
 * Construction web_conf de chacun des fichiers vpz scenario d'un paquet
 *********************************************************************/
std::string TousWebConf( std::string nomDuPaquet ){ 

    vle::manager::init(); 

    std::string nomPaquet = nomDuPaquet;
    std::string nomFichierVpz;
	conf_web::FichierVpzScenario *scn;

    /* liste des fichiers vpz scenario de simulation sous exp */
    vu::Package::package().select( nomPaquet ); 
    vu::PathList vpzlist = vu::Path::path().getInstalledExperiments();

    std::string msg = (std::string)" \n";
    msg = msg + (std::string)"-------------------------------------------------------------------------\n";
    msg = msg + (std::string)"[Configuration web] Construction web_conf de tous les fichiers vpz scenario du paquet " + nomPaquet + "\n";

    for ( vu::PathList::const_iterator it = vpzlist.begin(); it != vpzlist.end(); it++ ){
        nomFichierVpz = vu::Path::filename(*it);
	    scn = new conf_web::FichierVpzScenario( nomPaquet, nomFichierVpz );
	    msg = msg + scn->WebConf();
    }

    return msg;
}

/********************************************************************* 
 * Construction web_applis de chacun des fichiers vpz scenario d'un paquet
 *********************************************************************/
std::string TousWebApplis( std::string nomDuPaquet ){ 

    vle::manager::init(); 

    std::string nomPaquet = nomDuPaquet;
    std::string nomFichierVpz;
	conf_web::FichierVpzScenario *scn;

    /* liste des fichiers vpz scenario de simulation sous exp */
    vu::Package::package().select( nomPaquet ); 
    vu::PathList vpzlist = vu::Path::path().getInstalledExperiments();

    std::string msg = (std::string)" \n";
    msg = msg + (std::string)"-------------------------------------------------------------------------\n";
    msg = msg + (std::string)"[Configuration web] Construction web_applis de tous les fichiers vpz scenario du paquet " + nomPaquet + "\n";

    for ( vu::PathList::const_iterator it = vpzlist.begin(); it != vpzlist.end(); it++ ){
        nomFichierVpz = vu::Path::filename(*it);
	    scn = new conf_web::FichierVpzScenario( nomPaquet, nomFichierVpz );
	    msg = msg + scn->WebApplis();
    }

    return msg;
}

/********************************************************************* 
 *
 *                 Classe FichierVpzScenario
 *
 *********************************************************************/

/********************************************************************* 
 *
 * Constructeurs
 *
 *********************************************************************/

FichierVpzScenario::FichierVpzScenario( std::string nomDuPaquet, 
                                        std::string nomDuFichierVpz ){
    vle::manager::init(); 

    nomPaquet = nomDuPaquet;
    nomFichierVpz = nomDuFichierVpz;
}

FichierVpzScenario::~FichierVpzScenario( void ){
    vle::manager::finalize();
}

/********************************************************************* 
 *
 * Construction dicos
 *
 * Une condition par condition du modele (dico_nomcondition)
 * Une condition associee au projet (dico_project)
 * Une condition associee aux entrees et sorties du modele (dico_es)
 *
 *********************************************************************/
std::string FichierVpzScenario::Dicos( std::string nomDuFichierVpzDicos ){

    /* Initialisations */

    vu::Package::package().select( nomPaquet ); // localisation

    std::string nomFichierVpzDicos; // nom du fichier dicos qui va etre cree 
    nomFichierVpzDicos = determinerNomFichierProduit( nomDuFichierVpzDicos, "dicos__", nomFichierVpz );

    std::string msg = (std::string)" \n";

    vz::Vpz *vpz_dicos; // la production
    // vpz_dicos = new vz::Vpz( vu::Path::path().getPackageExpFile( nomFichierVpz ) );
    if ( vu::Path::existFile( vu::Path::path().getPackageExpFile(nomFichierVpz) ) ){

        vpz_dicos = new vz::Vpz( vu::Path::path().getPackageExpFile( nomFichierVpz ) );

        msg = msg + (std::string)"-------------------------------------------------------------------------\n";
        msg = msg + (std::string)"[Configuration web] Construction dicos du fichier vpz scenario " + nomFichierVpz + "\n";
    
        /* creation d'une condition explicative, en plus des conditions de dicos : condition_hors_sujet_help, sera alimentee au fur et a mesure de la construction des conditions de dicos */
        vz::Condition* condition_hors_sujet_help;
        condition_hors_sujet_help = new vz::Condition( "__hs_help" );
        vv::Value* m_hs;
        std::string nom_port_hs;
    
        /*****************************************************************
         *
         * condition dico de chacune des conditions du modele
         *
         *****************************************************************/
    
        /* creation des conditions dico par renommage des conditions du modele */
    
        vz::Conditions& conditions_dicos( vpz_dicos->project().experiment().conditions() );
    
        std::list< std::string > noms_conditions;
        conditions_dicos.conditionnames( noms_conditions );
    
        for ( std::list<std::string>::iterator nom_condition = noms_conditions.begin(); nom_condition != noms_conditions.end(); nom_condition++ ){
            conditions_dicos.rename( *nom_condition, getNomDicoDeCondition(*nom_condition) );
        }
    
        /* valeurs des ports des conditions */
        conditions_dicos.conditionnames( noms_conditions );
        for ( std::list<std::string>::iterator nom_condition = noms_conditions.begin(); nom_condition != noms_conditions.end(); nom_condition++ ){
            vz::Condition& c( conditions_dicos.get( *nom_condition ) );
            std::list< std::string > noms_ports;
            c.portnames( noms_ports );
            for ( std::list<std::string>::iterator nom_port = noms_ports.begin(); nom_port != noms_ports.end(); nom_port++ ){
    
                // la map valeur par defaut d'un port/parametre
                vv::Value* map_defaut = vv::Map::create();
                setMapDocumentationParametre( map_defaut->toMap(), *nom_port );
    
                c.setValueToPort( *nom_port, *map_defaut );
            }
        }
    
        /*****************************************************************
         *
         * condition dico_project associee au projet 
         *
         *****************************************************************/
    
        vz::Condition* c;
        c = new vz::Condition( getNomDicoProject() );
    
        // explications
        {
        nom_port_hs = getNomDicoProject();
        std::string texte = "les noms des Parameters de la condition/dico " + getNomDicoProject() + " (Exp_Begin,...,experiment_name) sont predefinis";
        m_hs = vv::String::create( texte );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
        }
    
        std::string nom_port;
    
        /* creation un a un des ports specifiques */
    
        vv::Value* m;
    
        nom_port = "Exp_Begin";
        m = vv::Map::create();
        setMapDocumentation_Exp_Begin( m->toMap() );
        c->addValueToPort( nom_port, *m );
    
        nom_port = "Exp_Duration";
        m = vv::Map::create();
        setMapDocumentation_Exp_Duration( m->toMap() );
        c->addValueToPort( nom_port, *m );
    
        nom_port = "Plan_Number";
        m = vv::Map::create();
        setMapDocumentation_Plan_Number( m->toMap() );
        c->addValueToPort( nom_port, *m );
    
        nom_port = "Plan_Seed";
        m = vv::Map::create();
        setMapDocumentation_Plan_Seed( m->toMap() );
        c->addValueToPort( nom_port, *m );
    
        nom_port = "Simu_Seed";
        m = vv::Map::create();
        setMapDocumentation_Simu_Seed( m->toMap() );
        c->addValueToPort( nom_port, *m );
    
        nom_port = "experiment_name";
        m = vv::Map::create();
        setMapDocumentation_experiment_name( m->toMap() );
        c->addValueToPort( nom_port, *m );
    
        conditions_dicos.add( *c );
    
        /*****************************************************************
         *
         * condition dico_es associee aux entrees et sorties du modele
         *
         * Creation d'un port/parametre pour chaque port d'entree et/ou
         * de sortie du modele.
         * Les ports d'entree/sortie sont lus dans les observables => un
         * port d'entree/sortie n'est represente que s'il appartient a un
         * modele atomique, dans l'observable duquel il a ete declare.
         * Si le modele comporte plusieurs ports d'entree/sortie de meme
         * nom (appartenant a differents modele atomiques), un seul
         * port/parametre est cree avec ce nom de port d'entree/sortie.
         *
         *****************************************************************/
    
        c = new vz::Condition( getNomDicoEntreesSorties() );
    
        /* liste des observables */
        vz::ObservableList& obslst( vpz_dicos->project().experiment().views().observables().observablelist() );
        std::string nom_observable;
        for ( vz::ObservableList::iterator ito = obslst.begin(); ito != obslst.end(); ito++ ){
    
            nom_observable = ito->first;
    
            // les ports d'entree/sortie de l'observable */
            vz::ObservablePortList& obsportlst(vpz_dicos->project().experiment().views().observables().get(nom_observable).observableportlist());
            vz::ObservablePortList::iterator itp;
	        for ( itp = obsportlst.begin(); itp != obsportlst.end(); itp++ ){
    
	            nom_port = itp->first;
    
                // la map valeur par defaut d'un port
                vv::Value* map_defaut = vv::Map::create();
                setMapDocumentationVariable( map_defaut->toMap(), nom_port );
    
                c->addValueToPort( nom_port, *map_defaut ); // cree port si inexistant
                c->setValueToPort( nom_port, *map_defaut ); // valeur unique valeur (ecrasement des pre-existantes)
	        }
        }
    
        conditions_dicos.add( *c );
    
        // et condition en plus
        conditions_dicos.add( *condition_hors_sujet_help );
    
        /* nettoyage de vpz_dicos */
    
        vg::CoupledModel *cm;
        cm = (vg::CoupledModel *)( vpz_dicos->project().model().model() );
        cm->delAllModel(); // (... il reste les ports e/s du top model)
        vg::Model::rename( vpz_dicos->project().model().model(), "CONFIGURATION_DICOS" );
    
        vpz_dicos->project().dynamics().clear();
        vpz_dicos->project().experiment().views().clear();
    
    
        /* sauvegarde/creation nomFichierVpzDicos */
        vpz_dicos->write( nomFichierVpzDicos );
    
        msg = msg + (std::string)"[Configuration web] Le fichier " + nomFichierVpzDicos + " produit est range dans output.\n";
        msg = msg + (std::string)"\n";
        msg = msg + (std::string)"-------------------------------------------------------------------------\n";
    
    } else {

        msg = msg + (std::string)"-------------------------------------------------------------------------\n";
        msg = msg + (std::string)"[Configuration web] Pas de construction dicos du fichier vpz scenario " + nomFichierVpz + " qui n'existe pas DIRECTEMENT sous exp\n";
        msg = msg + (std::string)"-------------------------------------------------------------------------\n";

    }
    return msg;
}


/********************************************************************* 
 *
 * Construction web_conf
 *
 * Une condition etat
 * Une condition scenarios
 *
 *********************************************************************/
std::string FichierVpzScenario::WebConf( std::string nomDuFichierVpzWebConf ){

    /* Initialisations */

    vu::Package::package().select( nomPaquet ); // localisation

    std::string nomFichierVpzWebConf; // nom du fichier web_conf qui va etre cree 
    nomFichierVpzWebConf = determinerNomFichierProduit( nomDuFichierVpzWebConf, "web_conf__", nomFichierVpz );

    std::string msg = (std::string)" \n";

    vz::Vpz *vpz_web_conf; // la production
    //vpz_web_conf = new vz::Vpz( vu::Path::path().getPackageExpFile( nomFichierVpz ) );
    if ( vu::Path::existFile( vu::Path::path().getPackageExpFile(nomFichierVpz) ) ){

        vpz_web_conf = new vz::Vpz( vu::Path::path().getPackageExpFile( nomFichierVpz ) );

        vz::Conditions& conditions_web_conf( vpz_web_conf->project().experiment().conditions() );

        msg = msg + (std::string)"-------------------------------------------------------------------------\n";
        msg = msg + (std::string)"[Configuration web] Construction web_conf du fichier vpz scenario " + nomFichierVpz + "\n";
    
        /* nettoyage de vpz_web_conf */
    
        conditions_web_conf.clear();
    
        vg::CoupledModel *cm;
        cm = (vg::CoupledModel *)( vpz_web_conf->project().model().model() );
        cm->delAllModel(); // (... il reste les ports e/s du top model)
        vg::Model::rename( vpz_web_conf->project().model().model(), "CONFIGURATION_WEB_CONF" );
    
        vpz_web_conf->project().dynamics().clear();
        vpz_web_conf->project().experiment().views().clear();
    
        vv::Value* m;
        vz::Condition* c;

        /* creation d'une condition explicative, en plus des conditions de web_conf : condition_hors_sujet_help, sera alimentee au fur et a mesure de la construction des conditions de web_conf */
        vz::Condition* condition_hors_sujet_help;
        condition_hors_sujet_help = new vz::Condition( "__hs_help" );
        vv::Value* m_hs;
        std::string nom_port_hs;

        /*****************************************************************
         * condition etat
         *****************************************************************/
    
        c = new vz::Condition( "etat" );
    
        std::string nom_port;
        nom_port = "a_appliquer";
        m = vv::Boolean::create( true ); // valeurs true false
        c->addValueToPort( nom_port, *m );
    
        nom_port = "mode_application";
        m = vv::String::create( "maximum" ); // valeurs "maximum" "minimum"
        c->addValueToPort( nom_port, *m );
    
        conditions_web_conf.add( *c );
    
        // explications
        nom_port_hs = "etat";
        m_hs = vv::String::create( "valeurs du port mode_application de la condition etat : maximum, minimum" );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
        m_hs = vv::String::create( "valeurs du port a_appliquer de la condition etat : true (1), false (0)" );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
    
        /*****************************************************************
         * condition scenarios
         *****************************************************************/
    
        c = new vz::Condition( "scenarios" );
    
        std::string nomScenario; // nom fichier vpz scenario de simulation sans extension .vpz
    
        /* liste des fichiers vpz scenarios de simulation sous exp */
        vu::Package::package().select( nomPaquet ); 
        vu::PathList vpzlist = vu::Path::path().getInstalledExperiments();
    
        /* ports scenarios_montres et scenarios_caches */
    
        std::string nom_port_scenarios_montres = "scenarios_montres";
        std::string nom_port_scenarios_caches = "scenarios_caches";
    
        for ( vu::PathList::const_iterator it = vpzlist.begin(); it != vpzlist.end(); it++ ){
    
            nomScenario = vu::Path::basename(*it);
    
            /* la map du scenario nomScenario */
    
            m = vv::Map::create();
    
            m->toMap().addString( "nom_scenario", nomScenario );
    
            setMapInformationsGenerales( m->toMap().addMap("infos_generales"),
                    "description du scenario " + nomScenario,
                    "help du scenario " + nomScenario,
                    "titre du scenario " + nomScenario );
    
            vv::Set& s_aa = m->toMap().addSet( "applications_associees" );
            s_aa.addString( "application_par_conditions" );
            s_aa.addString( "application_tout_dans_une_page" );
    
            c->addValueToPort( nom_port_scenarios_montres, *m );
            c->addValueToPort( nom_port_scenarios_caches, *m );
        }
    
        conditions_web_conf.add( *c );
    
        // explications
        nom_port_hs = "scenarios";
        m_hs = vv::String::create( "les scenarios qui seront pris en consideration vont dependre des ports scenarios_caches et scenarios_montres de la condition scenarios et aussi du port mode_application de la condition etat" );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
        m_hs = vv::String::create( "dans la condition scenarios, renseigner scenarios_caches ou bien scenarios_montres" );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
        m_hs = vv::String::create( "dans le port scenarios_caches de la condition scenarios : chaque valeur (map) correspond a un scenario a cacher" );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
        m_hs = vv::String::create( "dans le port scenarios_montres de la condition scenarios : chaque valeur (map) correspond a un scenario a montrer" );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
        m_hs = vv::String::create( "Key applications_associees de chaque scenario (ie de chaque valeur (map) du port scenarios_caches ou du port scenarios_montres) : le Set contient autant de String que d'applications a appliquer au scenario. Saisir dans chaque String le nom d'une application applicable/a appliquer au scenario, ie le nom d'un port de la condition applications du fichier de configuration web_applis" );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
    
    
        /* et condition en plus */
        conditions_web_conf.add( *condition_hors_sujet_help );
    
        /* sauvegarde/creation nomFichierVpzWebConf */
        vpz_web_conf->write( nomFichierVpzWebConf );
    
        msg = msg + (std::string)"[Configuration web] Le fichier " + nomFichierVpzWebConf + " produit est range dans output.\n";
        msg = msg + (std::string)"\n";
        msg = msg + (std::string)"-------------------------------------------------------------------------\n";
    
    } else {

        msg = msg + (std::string)"-------------------------------------------------------------------------\n";
        msg = msg + (std::string)"[Configuration web] Pas de construction web_conf du fichier vpz scenario " + nomFichierVpz + " qui n'existe pas DIRECTEMENT sous exp\n";
        msg = msg + (std::string)"-------------------------------------------------------------------------\n";

    }
    return msg;
}

/********************************************************************* 
 *
 * Construction web_applis
 *
 * Une condition applications
 * Une condition pages_def
 * Une condition pages_res
 *
 *********************************************************************/
std::string FichierVpzScenario::WebApplis( std::string nomDuFichierVpzWebApplis ){

    /* Initialisations */

    vu::Package::package().select( nomPaquet ); // localisation

    std::string nomFichierVpzWebApplis; // nom du fichier web_applis qui va etre cree 
    nomFichierVpzWebApplis = determinerNomFichierProduit( nomDuFichierVpzWebApplis, "web_applis__", nomFichierVpz );

    std::string msg = (std::string)" \n";

    vz::Vpz *vpz_web_applis; // la production
    // vpz_web_applis = new vz::Vpz( vu::Path::path().getPackageExpFile( nomFichierVpz ) );
    if ( vu::Path::existFile( vu::Path::path().getPackageExpFile(nomFichierVpz) ) ){

        vpz_web_applis = new vz::Vpz( vu::Path::path().getPackageExpFile( nomFichierVpz ) );

        vz::Conditions& conditions_web_applis( vpz_web_applis->project().experiment().conditions() );
    
        vv::Value* m;
        std::string nom_port;
    
        msg = msg + (std::string)"-------------------------------------------------------------------------\n";
        msg = msg + (std::string)"[Configuration web] Construction web_applis du fichier vpz scenario " + nomFichierVpz + "\n";
    
        /* creation d'une condition explicative, en plus des conditions de web_applis : condition_hors_sujet_help, sera alimentee au fur et a mesure de la construction des conditions de web_applis */
        vz::Condition* condition_hors_sujet_help;
        condition_hors_sujet_help = new vz::Condition( "__hs_help" );
        vv::Value* m_hs;
        std::string nom_port_hs;
    
    
        /* Creation/construction des informations/conditions de web_applis */
    
    
        /*****************************************************************
         *
         * condition applications
         *
         *****************************************************************/
    
        vz::Condition* condition_applications;
        condition_applications = new vz::Condition( "applications" );
    
        // explications
        nom_port_hs = "applications";
        m_hs = vv::String::create( "dans la condition applications : chaque port correspond a une application applicable a un(des) scenario(s) qui est(sont) defini(s) dans le port scenarios_montres de la condition scenarios du fichier de configuration web_conf" );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
        m_hs = vv::String::create( "dans la condition applications, ajouter un port par nouvelle application" );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
    
        /*****************************************************************
         * condition applications, construction d'un port/application
         * contenant une page par condition du modele et une page
         * contenant les informations project du modele (cf pages_def)
         *****************************************************************/
    
        nom_port = "application_par_conditions";
    
        /* les informations generales */
        m = vv::Map::create();
        setMapInformationsGenerales( m->toMap().addMap("infos_generales"),
                "application donnant accès à toutes les informations du scénario, organisées en rubriques : une rubrique par condition, une rubrique d'informations générales (début de simulation...)",
                "help de " + nom_port,
                "les informations classées par conditions" );
        condition_applications->addValueToPort( nom_port, *m );
    
        /* dicos */
        {
        m = vv::Map::create();
        vv::Set& s = m->toMap().addSet( "dicos" );
    
        s.addString( getNomDicoProject() );
    
        /* dicos des conditions du modele */
        std::list< std::string > noms_conditions;
        conditions_web_applis.conditionnames( noms_conditions );
        for ( std::list<std::string>::iterator nom_condition = noms_conditions.begin(); nom_condition != noms_conditions.end(); nom_condition++ ){
             
            s.addString( getNomDicoDeCondition(*nom_condition) );
        }
    
        s.addString( getNomDicoEntreesSorties() );
    
        condition_applications->addValueToPort( nom_port, *m );
        }
    
        /* pages_def */
        {
        m = vv::Map::create();
        vv::Set& s = m->toMap().addSet("pages_def");
    
        s.addString( getNomPageProject() );
    
        /* pages des conditions du modele */
        std::list< std::string > noms_conditions;
        conditions_web_applis.conditionnames( noms_conditions );
        for ( std::list<std::string>::iterator nom_condition = noms_conditions.begin(); nom_condition != noms_conditions.end(); nom_condition++ ){
            std::string nom_page = getNomPageDeCondition(*nom_condition);
            s.addString( nom_page );
        }
    
        condition_applications->addValueToPort( nom_port, *m );
        }
    
        /* pages_res */
        {
        m = vv::Map::create();
        /* vv::Set& s = */ m->toMap().addSet("pages_res");
        // set vide
        condition_applications->addValueToPort( nom_port, *m );
        }
    
        /*****************************************************************
         * condition applications, construction d'un port/application
         * contenant une seule page dans laquelle toutes les informations
         * du modele (cf pages_def)
         *****************************************************************/
    
        nom_port = "application_tout_dans_une_page";
    
        /* les informations generales */
        m = vv::Map::create();
        setMapInformationsGenerales( m->toMap().addMap("infos_generales"), 
                "application donnant accès à toutes les informations du scénario, présentées toutes ensemble (dans une seule rubrique)",
                "help de " + nom_port,
                "les informations toutes ensemble" );
        condition_applications->addValueToPort( nom_port, *m );
    
        /* dicos */
        {
        m = vv::Map::create();
        vv::Set& s = m->toMap().addSet( "dicos" );
    
        s.addString( getNomDicoProject() );
    
        /* dicos des conditions du modele */
        std::list< std::string > noms_conditions;
        conditions_web_applis.conditionnames( noms_conditions );
        for ( std::list<std::string>::iterator nom_condition = noms_conditions.begin(); nom_condition != noms_conditions.end(); nom_condition++ ){
            s.addString( getNomDicoDeCondition(*nom_condition) );
        }
    
        s.addString( getNomDicoEntreesSorties() );
    
        condition_applications->addValueToPort( nom_port, *m );
        }
    
        /* pages_def */
        {
        m = vv::Map::create();
        vv::Set& s = m->toMap().addSet("pages_def");
    
        s.addString( getNomPageToutesInformations() );
    
        condition_applications->addValueToPort( nom_port, *m );
        }
    
        /* pages_res */
        {
        m = vv::Map::create();
        /* vv::Set& s = */ m->toMap().addSet("pages_res");
        // set vide
        condition_applications->addValueToPort( nom_port, *m );
        }
    
        /*****************************************************************
         *
         * condition pages_def
         *
         *****************************************************************/
    
        vz::Condition* condition_pages_def;
        condition_pages_def = new vz::Condition( "pages_def" );
    
        // explications
        nom_port_hs = "pages_def";
        m_hs = vv::String::create( "dans la condition pages_def : chaque port correspond a une page de definition selon laquelle peuvent etre presentees des informations d'un scenario dans une(des) applications qui est(sont) definie(s) dans le(s) port(s) de la condition applications (cf Set de Key pages_def de chaque application)" );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
        m_hs = vv::String::create( "dans la condition pages_def, ajouter un port par nouvelle page de definition" );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
        m_hs = vv::String::create( "dans la condition pages_def : les donnees d'une page de definition (ie d'un port de la condition pages_def) sont definies dans la Key donnees de sa valeur (map)" );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
        m_hs = vv::String::create( "Keys d'une donnee d'une page de definition dans la condition pages_def : chaque element/donnee du Set de la Key donnees est une map ayant ses Keys parmi : doc_dicos, doc_locale, vpz_type, vpz_cond_name, vpz_port_name, web_dimension, web_mode, web_name. Toutes les Keys ne sont pas forcement presentes" );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
        m_hs = vv::String::create( "valeurs de la Key vpz_type d'une donnee d'une page de definition dans la condition pages_def : type_exp_name, type_exp_duration, type_exp_begin, type_simu_seed, type_plan_seed, type_plan_number, type_condition_port" );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
        m_hs = vv::String::create( "La Key vpz_cond_name et la Key vpz_port_name d'une donnee d'une page de definition dans la condition pages_def sont a renseigner si sa Key vpz_type vaut type_condition_port" );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
        m_hs = vv::String::create( "valeurs de la Key web_mode d'une donnee d'une page de definition dans la condition pages_def : hidden, read_write, read_only" );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
        m_hs = vv::String::create( "valeurs de la Key web_dimension d'une donnee d'une page de definition dans la condition pages_def : variable, fixe" );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
        m_hs = vv::String::create( "nom/intitule d'une donnee d'une page de definition dans la condition pages_def : le nom/intitule d'une donnee est determine en fonction prioritairement de sa Key web_name, puis de sa Key doc_locale, puis de sa Key doc_dicos, avec un nom par defaut si jamais aucune de ces Keys n'existe" );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
        m_hs = vv::String::create( "valeurs particulieres de la Key web_name d'une donnee d'une page de definition dans la condition pages_def : frenchname et englishname" );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
        m_hs = vv::String::create( "valeur particuliere frenchname de la Key web_name d'une donnee d'une page de definition dans la condition pages_def : la valeur frenchname sert a demander la valeur de nom_frenchname issue prioritairement de la Key doc_locale, ou de la Key doc_dicos, ou des dicos (voir Key dicos de l'application/port dans la condition applications)" );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
        m_hs = vv::String::create( "valeur particuliere englishname de la Key web_name d'une donnee d'une page de definition dans la condition pages_def : la valeur englishname sert a demander la valeur de nom_englishname issue prioritairement de la Key doc_locale, ou de la Key doc_dicos, ou des dicos (voir Key dicos de l'application/port dans la condition applications)" );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
        m_hs = vv::String::create( "Key doc_dicos d'une donnee d'une page de definition dans la condition pages_def : sa Key dico_prefere donne le nom du dico dans lequel chercher prioritairement la donnee, la Key name donne le nom sous lequel chercher la donnee dans un dico (il s'agit du nom d'un port d'une condition/dico du fichier de configuration dicos)" );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
    
        /*****************************************************************
         * condition pages_def, construction d'un port/page contenant
         * toutes les informations du modele (information project et
         * ports/paras des conditions)
         *****************************************************************/
        nom_port = getNomPageToutesInformations();
    
        m = vv::Map::create();
    
        /* les informations generales */
        setMapInformationsGenerales( m->toMap().addMap("infos_generales"),
                    "description de " + nom_port,
                    "help de " + nom_port,
                    "Toutes les informations" );
    
        /* les donnees */
        {
        vv::Set& s_d = m->toMap().addSet( "donnees" );

        /* informations generales et gestion alea */
        ajouterDonneesProjet( s_d );
    
        /* ports/parametres des conditions du modele */
        std::list< std::string > noms_conditions;
        conditions_web_applis.conditionnames( noms_conditions );
        for ( std::list<std::string>::iterator nom_condition = noms_conditions.begin(); nom_condition != noms_conditions.end(); nom_condition++ ){
            vz::Condition& c( conditions_web_applis.get( *nom_condition ) );
            ajouterDonneesCondition( *nom_condition, c, s_d );
        }
        }
    
        condition_pages_def->addValueToPort( nom_port, *m );
    
        /*****************************************************************
         * condition pages_def, construction d'un port/page contenant
         * les informations project du modele 
         * (informations generales et gestion alea)
         *****************************************************************/
        nom_port = getNomPageProject();
    
        m = vv::Map::create();
    
        /* les informations generales */
        setMapInformationsGenerales( m->toMap().addMap("infos_generales"),
                    "description de " + nom_port,
                    "help de " + nom_port,
                    "Informations générales" );
    
        /* les donnees */
        {
        vv::Set& s_d = m->toMap().addSet( "donnees" );
        ajouterDonneesProjet( s_d ); /* informations generales et gestion alea */
        }
    
        condition_pages_def->addValueToPort( nom_port, *m );
    
        /*****************************************************************
         * condition pages_def, construction d'un port/page par
         * condition du modele
         *****************************************************************/
        std::list< std::string > noms_conditions;
        conditions_web_applis.conditionnames( noms_conditions );
        for ( std::list<std::string>::iterator nom_condition = noms_conditions.begin(); nom_condition != noms_conditions.end(); nom_condition++ ){
    
            nom_port = getNomPageDeCondition( *nom_condition );
    
            m = vv::Map::create();
    
            /* les informations generales */
            setMapInformationsGenerales( m->toMap().addMap("infos_generales"), 
                    "description de " + *nom_condition,
                    "help de " + *nom_condition,
                    "Condition " + *nom_condition );
    
            /* les donnees */
            vv::Set& s_d = m->toMap().addSet( "donnees" );
            vz::Condition& c( conditions_web_applis.get( *nom_condition ) );
            ajouterDonneesCondition( *nom_condition, c, s_d );
    
            condition_pages_def->addValueToPort( nom_port, *m );
        }
    
        /*****************************************************************
         *
         * condition pages_res
         *
         *****************************************************************/
    
        vz::Condition* condition_pages_res;
        condition_pages_res = new vz::Condition( "pages_res" );
    
        // explications
        nom_port_hs = "pages_res";
        m_hs = vv::String::create( "la condition pages_res est lue mais pour l'instant inexploitee par outil web" );
        condition_hors_sujet_help->addValueToPort( nom_port_hs, *m_hs );
    
        /* map valeur d'un port page res */
    
        m = vv::Map::create();
    
        setMapInformationsGenerales( m->toMap().addMap("infos_generales") );
    
        vv::Set& s_d = m->toMap().addSet( "donnees" );
        {
        vv::Map& m_d = s_d.addMap();
        m_d.addString( "doc_dico_prefere", getTexteArenseigner() );
        m_d.addString( "vpz_view_name",    getTexteArenseigner() );
        }
        {
        vv::Map& m_d = s_d.addMap();
        m_d.addString( "doc_dico_prefere", getTexteArenseigner() );
        m_d.addString( "vpz_view_name",    getTexteArenseigner() );
        }
    
        /* port page_xxx */
    
        nom_port = "page_xxx";
        condition_pages_res->addValueToPort( nom_port, *m );
    
        nom_port = "etc";
        condition_pages_res->addValueToPort( nom_port, *m );
    
        /*****************************************************************/
    
        /* nettoyage de vpz_web_applis */
    
        conditions_web_applis.clear();
    
        vg::CoupledModel *cm;
        cm = (vg::CoupledModel *)( vpz_web_applis->project().model().model() );
        cm->delAllModel(); // (... il reste les ports e/s du top model)
        vg::Model::rename( vpz_web_applis->project().model().model(), "CONFIGURATION_WEB_APPLIS" );
    
        vpz_web_applis->project().dynamics().clear();
        vpz_web_applis->project().experiment().views().clear();
    
        /* entree des informations dans vpz_web_applis */
        conditions_web_applis.add( *condition_hors_sujet_help ); // en plus
        conditions_web_applis.add( *condition_applications );
        conditions_web_applis.add( *condition_pages_def );
        conditions_web_applis.add( *condition_pages_res );
    
        /* sauvegarde/creation nomFichierVpzWebApplis */
        vpz_web_applis->write( nomFichierVpzWebApplis );
    
        msg = msg + (std::string)"[Configuration web] Le fichier " + nomFichierVpzWebApplis + " produit est range dans output.\n";
        msg = msg + (std::string)"\n";
        msg = msg + (std::string)"-------------------------------------------------------------------------\n";
    
    } else {

        msg = msg + (std::string)"-------------------------------------------------------------------------\n";
        msg = msg + (std::string)"[Configuration web] Pas de construction web_applis du fichier vpz scenario " + nomFichierVpz + " qui n'existe pas DIRECTEMENT sous exp\n";
        msg = msg + (std::string)"-------------------------------------------------------------------------\n";

    }
    return msg;
}

/********************************************************************* 
 *
 * Traitements pour WebApplis : ajout de donnees dans Set
 *
    vv::Map& m_d = s_d.addMap();

    m_d.addString( "vpz_type", "type_exp_name" ); // valeur parmi "type_exp_name" "type_exp_duration" "type_exp_begin" "type_simu_seed" "type_plan_seed" "type_plan_number" "type_condition_port" 

    // "vpz_cond_name" et "vpz_port_name" sont a renseigner si "type_condition_port" 
    m_d.addString( "vpz_cond_name", "..." );
    m_d.addString( "vpz_port_name", "..." );

    m_d.addString( "web_mode", "read_write" ); // valeur parmi "hidden" "read_write" "read_only"
    m_d.addString( "web_dimension", "fixe" ); // valeur parmi "variable" "fixe"

    m_d.addString( "web_name", "..." ); // valeurs particulieres : "frenchname" pour valeur nom_frenchname issue de dicos "englishname" pour demander nom_englishname issue de dicos

    setMapDocumentationDonnee( m_d.addMap("doc_locale") );

    vv::Map& m_dd = m_d.addMap( "doc_dicos" );
    m_dd.addString( "dico_prefere", "..." ); // a pour valeur le nom d'un des dicos
    m_dd.addString( "name", "..." ); // a pour valeur le nom d'une donnee d'un des dicos ("Exp_Begin" "Exp_Duration" "Plan_Number" "Plan_Seed" "Simu_Seed" "experiment_name" etc)

 *
 *********************************************************************/
void FichierVpzScenario::ajouterDonneesProjet( vv::Set& s ){

    /* debut simulation */
    {
    vv::Map& m_d = s.addMap();
    m_d.addString( "vpz_type", "type_exp_begin" );
    m_d.addString( "web_mode", "read_write" );
    m_d.addString( "web_dimension", "fixe" );
    m_d.addString( "web_name", getIntitule_Exp_Begin() );
    setMapDocumentation_Exp_Begin( m_d.addMap("doc_locale") );

    vv::Map& m_dd = m_d.addMap( "doc_dicos" );
    m_dd.addString( "dico_prefere", getNomDicoProject() );
    m_dd.addString( "name", "Exp_Begin" );
    }

    /* duree simulation  */
    {
    vv::Map& m_d = s.addMap();

    m_d.addString( "vpz_type", "type_exp_duration" );
    m_d.addString( "web_mode", "read_write" );
    m_d.addString( "web_dimension", "fixe" );
    m_d.addString( "web_name", getIntitule_Exp_Duration() );
    setMapDocumentation_Exp_Duration( m_d.addMap("doc_locale") );

    vv::Map& m_dd = m_d.addMap( "doc_dicos" );
    m_dd.addString( "dico_prefere", getNomDicoProject() );
    m_dd.addString( "name", "Exp_Duration" );
    }

    /* nom experience */
    {
    vv::Map& m_d = s.addMap();

    m_d.addString( "vpz_type", "type_exp_name" );
    m_d.addString( "web_mode", "read_write" );
    m_d.addString( "web_dimension", "fixe" );
    m_d.addString( "web_name", getIntitule_experiment_name() );
    setMapDocumentation_experiment_name( m_d.addMap("doc_locale") );

    vv::Map& m_dd = m_d.addMap( "doc_dicos" );
    m_dd.addString( "dico_prefere", getNomDicoProject() );
    m_dd.addString( "name", "experiment_name" );
    }

    /* graine de simulation */
    {
    vv::Map& m_d = s.addMap();

    m_d.addString( "vpz_type", "type_simu_seed" );
    m_d.addString( "web_mode", "read_write" );
    m_d.addString( "web_dimension", "fixe" );
    m_d.addString( "web_name", getIntitule_Simu_Seed() );
    setMapDocumentation_Simu_Seed( m_d.addMap("doc_locale") );

    vv::Map& m_dd = m_d.addMap( "doc_dicos" );
    m_dd.addString( "dico_prefere", getNomDicoProject() );
    m_dd.addString( "name", "Simu_Seed" );
    }

    /* graine de generation des graines de simulation */
    {
    vv::Map& m_d = s.addMap();

    m_d.addString( "vpz_type", "type_plan_seed" );
    m_d.addString( "web_mode", "read_write" );
    m_d.addString( "web_dimension", "fixe" );
    m_d.addString( "web_name", getIntitule_Plan_Seed() );
    setMapDocumentation_Plan_Seed( m_d.addMap("doc_locale") );

    vv::Map& m_dd = m_d.addMap( "doc_dicos" );
    m_dd.addString( "dico_prefere", getNomDicoProject() );
    m_dd.addString( "name", "Plan_Seed" );
    }

    /* nombre de repliquas de chaque combinaison */
    {
    vv::Map& m_d = s.addMap();

    m_d.addString( "vpz_type", "type_plan_number" );
    m_d.addString( "web_mode", "read_write" );
    m_d.addString( "web_dimension", "fixe" );
    m_d.addString( "web_name", getIntitule_Plan_Number() );
    setMapDocumentation_Plan_Number( m_d.addMap("doc_locale") );

    vv::Map& m_dd = m_d.addMap( "doc_dicos" );
    m_dd.addString( "dico_prefere", getNomDicoProject() );
    m_dd.addString( "name", "Plan_Number" );
    }

}

void FichierVpzScenario::ajouterDonneesCondition( std::string nom_condition, vz::Condition& c, vv::Set& s ){

        std::list< std::string > noms_ports;
        c.portnames( noms_ports );
        for ( std::list<std::string>::iterator nom_port = noms_ports.begin(); nom_port != noms_ports.end(); nom_port++ ){

            vv::Map& m_d = s.addMap();

            m_d.addString( "vpz_type", "type_condition_port" );
            m_d.addString( "vpz_cond_name", nom_condition );
            m_d.addString( "vpz_port_name", *nom_port );

            m_d.addString( "web_mode", "read_write" );
            m_d.addString( "web_dimension", "variable" );
        
            m_d.addString( "web_name", getIntitule_Parametre( *nom_port ) );
        
            setMapDocumentationParametre( m_d.addMap("doc_locale"), *nom_port );
        
            vv::Map& m_dd = m_d.addMap( "doc_dicos" );
            m_dd.addString( "dico_prefere", getNomDicoDeCondition(nom_condition) );
            m_dd.addString( "name", *nom_port );
        }
}

/********************************************************************* 
 *
 * Determination du nom pour un fichier produit
 *
 *********************************************************************/
std::string FichierVpzScenario::determinerNomFichierProduit( std::string nomChoisi, std::string prefixeParDefaut, std::string racineParDefaut ){
    std::string nomFichierProduit;
    if ( nomChoisi == "" ){
        nomFichierProduit = prefixeParDefaut + racineParDefaut; // nom par defaut
    } else {
        nomFichierProduit = nomChoisi; // nom demande
    }
    // si nomFichierProduit deja existant sous output, alors petite transformation du nom
    while ( vu::Path::existFile( vu::Path::buildFilename(vu::Path::path().getPackageOutputDir(),nomFichierProduit) ) ){
        nomFichierProduit = "_" + nomFichierProduit;
    }
    return nomFichierProduit;
}

/********************************************************************* 
 *
 * Quelques constructions/affectations
 *
 *********************************************************************/

std::string FichierVpzScenario::getTexteArenseigner( void ){
    return "a renseigner (ou Remove Key)";
};

std::string FichierVpzScenario::getNomDicoDeCondition( std::string & nom_condition ){
    std::string nom_dico = "dico_" + nom_condition;
    return nom_dico;
};
std::string FichierVpzScenario::getNomDicoProject( void ){
    return "dico_project";
};
std::string FichierVpzScenario::getNomDicoEntreesSorties( void ){
    return "dico_es";
};

std::string FichierVpzScenario::getNomPageDeCondition( std::string & nom_condition ){
    std::string nom_page = "page_" + nom_condition;
    return nom_page;
};
std::string FichierVpzScenario::getNomPageProject( void ){
    return "page_project";
};
std::string FichierVpzScenario::getNomPageToutesInformations( void ){
    return "page_tout";
};

std::string FichierVpzScenario::getIntitule_Parametre( std::string & nom_port ){
    return ( "Paramètre " + nom_port );
}
std::string FichierVpzScenario::getIntitule_Variable( std::string & nom_port ){
    return ( "Variable " + nom_port );
}
std::string FichierVpzScenario::getIntitule_Exp_Begin( void ){
    return ( "Début de simulation" );
}
std::string FichierVpzScenario::getIntitule_Exp_Duration( void ){
    return ( "Durée simulation" );
}
std::string FichierVpzScenario::getIntitule_Plan_Number( void ){
    return ( "Plan Number" );
}
std::string FichierVpzScenario::getIntitule_Plan_Seed( void ){
    return ( "Plan Seed" );
}
std::string FichierVpzScenario::getIntitule_Simu_Seed( void ){
    return ( "Simu Seed" );
}
std::string FichierVpzScenario::getIntitule_experiment_name( void ){
    return ( "Nom du plan d'expérience" );
}

void FichierVpzScenario::setMapInformationsGenerales( vv::Map & m, std::string r ){
    if ( r == "" ){
        m.addString( "description", getTexteArenseigner() );
        m.addString( "help",        getTexteArenseigner() );
        m.addString( "titre",       getTexteArenseigner() );
    } else {
        m.addString( "description", "description de " + r );
        m.addString( "help",        "help de " + r );
        m.addString( "titre",       "titre de " + r );
    }
}
void FichierVpzScenario::setMapInformationsGenerales( vv::Map & m, std::string description, std::string help, std::string titre ){
    m.addString( "description", description );
    m.addString( "help",        help );
    m.addString( "titre",       titre );
}

void FichierVpzScenario::setMapDocumentationDonnee( vv::Map & m ){
    m.addString( "description",     getTexteArenseigner() );
    m.addString( "help",            getTexteArenseigner() );
    m.addString( "nom_englishname", getTexteArenseigner() );
    m.addString( "nom_frenchname",  getTexteArenseigner() );
    m.addString( "unite",           getTexteArenseigner() );
    m.addString( "val_max",         getTexteArenseigner() );
    m.addString( "val_min",         getTexteArenseigner() );
    m.addString( "val_recommandee", getTexteArenseigner() );
}
void FichierVpzScenario::setMapDocumentationParametre( vv::Map & m, std::string & nom_port ){
    m.addString( "description",     getTexteArenseigner() );
    m.addString( "help",            getTexteArenseigner() );
    m.addString( "nom_englishname", getTexteArenseigner() );
    m.addString( "nom_frenchname",  getIntitule_Parametre( nom_port ) );
    m.addString( "unite",           getTexteArenseigner() );
    m.addString( "val_max",         getTexteArenseigner() );
    m.addString( "val_min",         getTexteArenseigner() );
    m.addString( "val_recommandee", getTexteArenseigner() );
}
void FichierVpzScenario::setMapDocumentationVariable( vv::Map & m, std::string & nom_port ){
    m.addString( "description",     getTexteArenseigner() );
    m.addString( "help",            getTexteArenseigner() );
    m.addString( "nom_englishname", getTexteArenseigner() );
    m.addString( "nom_frenchname",  getIntitule_Variable( nom_port ) );
    m.addString( "unite",           getTexteArenseigner() );
    m.addString( "val_max",         getTexteArenseigner() );
    m.addString( "val_min",         getTexteArenseigner() );
    m.addString( "val_recommandee", getTexteArenseigner() );
}
void FichierVpzScenario::setMapDocumentation_Exp_Begin( vv::Map & m ){
    m.addString( "description",     "Instant date début de la simulation" );
    m.addString( "help",            "" );
    m.addString( "nom_englishname", "Simulation Begin" );
    m.addString( "nom_frenchname",  getIntitule_Exp_Begin() );
    m.addString( "unite",           "" );
    m.addString( "val_max",         getTexteArenseigner() );
    m.addString( "val_min",         getTexteArenseigner() );
    m.addString( "val_recommandee", getTexteArenseigner() );
}

void FichierVpzScenario::setMapDocumentation_Exp_Duration( vv::Map & m ){
    m.addString( "description",     "Durée de la simulation" );
    m.addString( "help",            "" );
    m.addString( "nom_englishname", "Simulation duration" );
    m.addString( "nom_frenchname",  getIntitule_Exp_Duration() );
    m.addString( "unite",           "" );
    m.addString( "val_max",         getTexteArenseigner() );
    m.addString( "val_min",         getTexteArenseigner() );
    m.addString( "val_recommandee", getTexteArenseigner() );
}

void FichierVpzScenario::setMapDocumentation_Plan_Number( vv::Map & m ){
    m.addString( "description",     "" );
    m.addString( "help",            "" );
    m.addString( "nom_englishname", "Plan Number" );
    m.addString( "nom_frenchname",  getIntitule_Plan_Number() );
    m.addString( "unite",           "" );
    m.addString( "val_max",         getTexteArenseigner() );
    m.addString( "val_min",         getTexteArenseigner() );
    m.addString( "val_recommandee", getTexteArenseigner() );
}

void FichierVpzScenario::setMapDocumentation_Plan_Seed( vv::Map & m ){
    m.addString( "description",     "" );
    m.addString( "help",            "" );
    m.addString( "nom_englishname", "Plan Seed" );
    m.addString( "nom_frenchname",  getIntitule_Plan_Seed() );
    m.addString( "unite",           "" );
    m.addString( "val_max",         getTexteArenseigner() );
    m.addString( "val_min",         getTexteArenseigner() );
    m.addString( "val_recommandee", getTexteArenseigner() );
}

void FichierVpzScenario::setMapDocumentation_Simu_Seed( vv::Map & m ){
    m.addString( "description",     "" );
    m.addString( "help",            "" );
    m.addString( "nom_englishname", "Simu Seed" );
    m.addString( "nom_frenchname",  getIntitule_Simu_Seed() );
    m.addString( "unite",           "" );
    m.addString( "val_max",         getTexteArenseigner() );
    m.addString( "val_min",         getTexteArenseigner() );
    m.addString( "val_recommandee", getTexteArenseigner() );
}

void FichierVpzScenario::setMapDocumentation_experiment_name( vv::Map & m ){
    m.addString( "description",     "Nom de l'expérience, qui sert de préfixe aux noms des fichiers de sortie/résultat de simulation" );
    m.addString( "help",            "Peut être utilisé pour organiser ses archivages de résultats" );
    m.addString( "nom_englishname", "Experiment name" );
    m.addString( "nom_frenchname",  getIntitule_experiment_name() );
    m.addString( "unite",           "sans" );
    m.addString( "val_max",         getTexteArenseigner() );
    m.addString( "val_min",         getTexteArenseigner() );
    m.addString( "val_recommandee", getTexteArenseigner() );
}


} // namespace conf_web 
