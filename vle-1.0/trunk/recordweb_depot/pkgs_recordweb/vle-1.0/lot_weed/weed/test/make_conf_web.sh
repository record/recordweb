#!/bin/bash

###
# @author Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2012-2013 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###

###############################################################################
#
# Script de lancement de la generation de fichiers vpz de configuration web
# (dicos, web_conf et web_applis)
#
###############################################################################

# Pour affichage des traces BOOST_TEST_MESSAGE lors de commande ctest -VV
export BOOST_TEST_LOG_LEVEL="message"
echo "> Verification de BOOST_TEST_LOG_LEVEL (qui doit valoir message) :"
echo "> BOOST_TEST_LOG_LEVEL vaut $BOOST_TEST_LOG_LEVEL"

cd ../build

ctest -R make_conf_web -VV

cd ../test

echo "##############################################################"
echo "#"
echo "# Les trames des fichiers de configuration web sont disponibles sous output."
echo "#"
echo "##############################################################"
echo ""

