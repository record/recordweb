/**
 * @file test/test.cpp
 */

/** @file
 *
 * ****************************************************************************
 *
 * @author Nathalie Rousse, INRA RECORD team member.
 *
 * Copyright (C) 2012-2013 INRA.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#define BOOST_TEST_MAIN
#define BOOST_AUTO_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE package_test
#include <boost/test/unit_test.hpp>
#include <boost/test/auto_unit_test.hpp>

#include <vle/vpz.hpp>
#include <vle/utils.hpp>
#include <vle/manager/Run.hpp>
#include <vle/manager/VLE.hpp>
#include <vle/oov/OutputMatrix.hpp>

namespace vz = vle::vpz;
namespace vu = vle::utils;
namespace vm = vle::manager;
namespace vo = vle::oov;
namespace va = vle::value;

struct F {

    F(){ vle::manager::init(); }

    ~F() { vle::manager::finalize(); }
};

BOOST_GLOBAL_FIXTURE(F)

/* ****************************************************************************
 *
 * Test du modele weed
 *
 * Jeu de test base sur le document suivant :
 *
 *    Document WeedModel_EN.doc 
 *    Titre : 'Weed Model - weed.model'
 *    Authors : David Makowski, Sylvain Toulet, Francois Brun
 *    Version : 1 may 2012
 *
 * Le modele weed code sous vle correspond a la description qui en est faite
 * dans le document :
 * - 'Table 1. Description and equations of the weed model'.
 * - 'Table 4. Listing of the Weed model functions: the update function and
 *   the integration loop function'.
 * 
 * Le jeu de test est base sur le fichier scenario weed_test.vpz dont :
 * - le parametrage correspond aux valeurs nominales des parametres du modele
 *   donnees dans le document en 'Table 2. Parameters of the weed model'.
 * - le fichier d'entree est decision_test.txt qui correspond au cas 'with
 *   herbicide every year' donne dans le document en 'Table 3. Examples of
 *   decision table'.
 * 
 * Les valeurs de sortie verifiees sont :
 * - celles donnees dans le document en 'Table 5. Output of weed.model
 *   function with the different states variable, including prediction of
 *   annual Yield, for decisions with herbicide every year'. SAUF correction
 *   (a priori faute de frappe ?) : valeur 17821.76 (de DSBa en Year 1)
 *   remplacee par 17921.76.
 * + verification de YieldSum (sortie supplementaire).
 *
 * ****************************************************************************
 */
BOOST_AUTO_TEST_CASE(test_modele)
{
    BOOST_TEST_MESSAGE( " ************** test du modele weed ************** " );

    vu::Package::package().select( "weed" );
    vz::Vpz vpz( vu::Path::path().getExternalPackageExpFile( "weed", "weed_test.vpz" ) );
    vz::Outputs::iterator itb = vpz.project().experiment().views().outputs().begin();
    vz::Outputs::iterator ite = vpz.project().experiment().views().outputs().end();
    for(;itb!=ite;itb++){
        itb->second.setLocalStream( "", "storage", std::string() );
    }
    vm::RunQuiet r;
    r.start( vpz );

    // verification deroulement simulation OK
    BOOST_REQUIRE_EQUAL( r.haveError(), false );

    // verification du nombre de views (vueDecision, vueWeed, vueWeedFinal)
    vo::OutputMatrixViewList& out( r.outputs() );
    BOOST_REQUIRE_EQUAL( out.size(), (vo::OutputMatrixViewList::size_type)3 );

    { // verification des decisions en entree du test

        vo::OutputMatrix& view( out["vueDecision"] );
        va::MatrixView result( view.values() );
        BOOST_REQUIRE_EQUAL( result.shape()[0], (va::MatrixView::size_type)4 );
        BOOST_REQUIRE_EQUAL( result.shape()[1], (va::MatrixView::size_type)7 );

        va::Matrix::index colCrop = view.column( "top:decision", "Crop" );
        va::Matrix::index colHerb = view.column( "top:decision", "Herb" );
        va::Matrix::index colSoil = view.column( "top:decision", "Soil" );

        // (cf initialisation dans variables de zCondDecision)
        BOOST_REQUIRE_EQUAL( va::toDouble(result[colCrop][0]), 1 );
        BOOST_REQUIRE_EQUAL( va::toDouble(result[colHerb][0]), 1 );
        BOOST_REQUIRE_EQUAL( va::toDouble(result[colSoil][0]), 1 );

        // Crop
        BOOST_REQUIRE_EQUAL( va::toDouble(result[colCrop][1]), 1 );
        BOOST_REQUIRE_EQUAL( va::toDouble(result[colCrop][2]), 1 );
        BOOST_REQUIRE_EQUAL( va::toDouble(result[colCrop][3]), 1 );
        BOOST_REQUIRE_EQUAL( va::toDouble(result[colCrop][4]), 1 );
        BOOST_REQUIRE_EQUAL( va::toDouble(result[colCrop][5]), 1 );
        BOOST_REQUIRE_EQUAL( va::toDouble(result[colCrop][6]), 1 );

        // Herb
        BOOST_REQUIRE_EQUAL( va::toDouble(result[colHerb][1]), 1 );
        BOOST_REQUIRE_EQUAL( va::toDouble(result[colHerb][2]), 1 );
        BOOST_REQUIRE_EQUAL( va::toDouble(result[colHerb][3]), 1 );
        BOOST_REQUIRE_EQUAL( va::toDouble(result[colHerb][4]), 1 );
        BOOST_REQUIRE_EQUAL( va::toDouble(result[colHerb][5]), 1 );
        BOOST_REQUIRE_EQUAL( va::toDouble(result[colHerb][6]), 1 );

        // Soil
        BOOST_REQUIRE_EQUAL( va::toDouble(result[colSoil][1]), 1 );
        BOOST_REQUIRE_EQUAL( va::toDouble(result[colSoil][2]), 0 );
        BOOST_REQUIRE_EQUAL( va::toDouble(result[colSoil][3]), 1 );
        BOOST_REQUIRE_EQUAL( va::toDouble(result[colSoil][4]), 0 );
        BOOST_REQUIRE_EQUAL( va::toDouble(result[colSoil][5]), 1 );
        BOOST_REQUIRE_EQUAL( va::toDouble(result[colSoil][6]), 0 );
    }

    { // verification des variables de sortie

        vo::OutputMatrix& view( out["vueWeed"] );
        va::MatrixView result( view.values() );
        BOOST_REQUIRE_EQUAL( result.shape()[0], (va::MatrixView::size_type)7 );
        BOOST_REQUIRE_EQUAL( result.shape()[1], (va::MatrixView::size_type)7 );

        va::Matrix::index cold = view.column( "top,modele_weed:weed", "d" );
        va::Matrix::index colS = view.column( "top,modele_weed:weed", "S" );
        va::Matrix::index colSSBa = view.column( "top,modele_weed:weed", "SSBa" );
        va::Matrix::index colDSBa = view.column( "top,modele_weed:weed", "DSBa" );
        va::Matrix::index colYield = view.column( "top,modele_weed:weed", "Yield" );
        va::Matrix::index colYieldSum = view.column( "top,modele_weed:weed", "YieldSum" );

        BOOST_REQUIRE_CLOSE( va::toDouble(result[cold][0]),         400,  10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colS][0]),       68000,  10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colSSBa][0]),     3350,  10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colDSBa][0]),      280,  10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colYield][0]),     7.88, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colYieldSum][0]),  7.88, 10e-2 );
        
        BOOST_REQUIRE_CLOSE( va::toDouble(result[cold][1]),       148.81, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colS][1]),      1313.55, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colSSBa][1]),    955.04, 10e-2 );
        /* 17821.76 remplace par 17921.76 (a priori faute de frappe) */
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colDSBa][1]),  17921.76, 10e-2 ); //!!!
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colYield][1]),     7.95, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colYieldSum][1]), 15.83, 10e-2 );
        
        BOOST_REQUIRE_CLOSE( va::toDouble(result[cold][2]),       116.53, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colS][2]),      1030.44, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colSSBa][2]),    530.03, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colDSBa][2]),   2820.84, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colYield][2]),     7.96, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colYieldSum][2]), 23.79, 10e-2 );
        
        BOOST_REQUIRE_CLOSE( va::toDouble(result[cold][3]),        43.69, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colS][3]),       387.98, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colSSBa][3]),    152.62, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colDSBa][3]),    643.14, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colYield][3]),     7.98, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colYieldSum][3]), 31.77, 10e-2 );
        
        BOOST_REQUIRE_CLOSE( va::toDouble(result[cold][4]),        18.29, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colS][4]),       162.67, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colSSBa][4]),    102.89, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colDSBa][4]),    122.19, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colYield][4]),     7.99, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colYieldSum][4]), 39.76, 10e-2 );
        
        BOOST_REQUIRE_CLOSE( va::toDouble(result[cold][5]),         2.29, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colS][5]),        20.39, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colSSBa][5]),      8.74, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colDSBa][5]),     68.27, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colYield][5]),     7.99, 10e-1 ); //!!!
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colYieldSum][5]), 47.75, 10e-2 );
        
        BOOST_REQUIRE_CLOSE( va::toDouble(result[cold][6]),         1.07, 10e-1 ); //!!!
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colS][6]),         9.54, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colSSBa][6]),      5.78, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colDSBa][6]),     11.68, 10e-2 );
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colYield][6]),     7.99, 10e-1 ); //!!!
        BOOST_REQUIRE_CLOSE( va::toDouble(result[colYieldSum][6]), 55.74, 10e-2 );
    }

}

