#-*- coding:utf-8 -*-

###
# @author Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2012-2013 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###

###############################################################################
#
#               Des appels sous Python de traitements R
#
#              avec RPy2 (an interface to R from Python)
#
###############################################################################

import rpy2.robjects as robjects
r=robjects.r

# les traitements R utilises
r.source('pour_python.R') # Simulation___R, Representation___R,
                          # Names___R, MuStar___R, Sigma___R
r.source( 'as.R') # MethodeMorris

# Simulation sous R ###########################################################

print "\nappel de la fonction R d'execution d'une simulation."
res= r.Simulation___R()

print "\naffichage a l'ecran du resultat :"
print(res)

print "\nresultat numerique sous python :"
# dict sorties :
#  - cle : nom sortie
#  - value : list des valeurs de la sortie
sorties = dict()
for s in res :
    nomsvars = list(s.names)
    for i,j in enumerate(s) : 
        k = nomsvars[i]
        sorties[k] = list(j)
print sorties

print "\nen python, affichage des traces des sorties de simulation en utilisant"
print "les classes Exp et Graphique de recorweb (recordweb/record/utils)."

# path du repertoire recordweb
import os, sys
RECORDWEB_HOME = "/home/nrousse/workspace_svn/recordweb"
if RECORDWEB_HOME not in sys.path :
    sys.path.insert(0, RECORDWEB_HOME )

from record.utils.graphique import Graphique # pour traitements graphiques

X = ( 'time', sorties['time'] )
for nomsortie in sorties.keys() :
    les_XY=[]
    les_legendes=[]
    Y = ( nomsortie, sorties[nomsortie] )
    les_XY.append( (X,Y) )
    n = nomsortie.split(":")[-1] # diminutif
    les_legendes.append( n )
    g=Graphique()
    g.setTitre( "Sortie " + nomsortie )
    g.setLegendes( "time", n, les_legendes )
    g.setLesXY( les_XY )
    image=g.produireGraphique()
    image.show()

# Analyse de sensibilite sous R ###############################################

print "\nappel de la fonction R d'analyse de sensibilite par methode de Morris."
res = r.MethodeMorris(nomSortie='top,modele_weed:weed.DSBa')

print "\naffichage a l'ecran du resultat :"
print(res)

print "\nappel de la fonction R d'enregistrement de la representation graphique"
print " du resultat dans un fichier pdf (res1_Morris.pdf)."
r.Representation___R( res, 'res1_Morris.pdf')

print "\nsous python, instructions R d'enregistrement de la representation"
print " graphique du resultat dans un fichier pdf (res2_Morris.pdf)."
from rpy2.robjects.packages import importr
graphics = importr('graphics')
grdevices = importr('grDevices')
nomFichier='res2_Morris.pdf'
grdevices.pdf(file=nomFichier, height=7, width=10)
graphics.plot(res)
grdevices.dev_off()

print "\nresultats numeriques sous python mu star et sigma :"

mu_star = r.MuStar___R(res)
sigma = r.Sigma___R(res)

names = list(mu_star.names)
mu_star = list(mu_star)
sigma = list(sigma)

mu_star_sigma = dict()
for i,name in enumerate(names) :
    mu_star_sigma[ name ] = { "mustar": mu_star[i], "sigma":sigma[i] }

# affichage a l'ecran du resultat
print mu_star_sigma 
#for k,v in mu_star_sigma.iteritems() :
    #k
    #v

###############################################################################

