#!/usr/bin/env Rscript

###
# @author Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2012-2013 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###

# ****************************************************************************
#
# Fonction dediees a des appels de traitements R sous python
#
# ****************************************************************************

library("rvle")

# simulation
Simulation___R = function(){
    s = new( 'Rvle', file = 'weed_storage.vpz', pkg = 'weed' )
    res = results( run( s, COND_Weed.phi = 0.5) )
    return( res )
}

# trace dans un fichier pdf
Representation___R = function( x, nomFichier ){
    pdf(file=nomFichier, height=7, width=10)
    par(mfrow=c(1,1))
    plot(x)
    dev.off()
}

# x : resultat de methode de Morris
MuStar___R = function( x ){
    mu.star <- apply(x$ee, 2, function(x) mean(abs(x)))
    return( mu.star )
}
Sigma___R = function( x ){
    sigma <- apply(x$ee, 2, sd)
    return( sigma )
}

