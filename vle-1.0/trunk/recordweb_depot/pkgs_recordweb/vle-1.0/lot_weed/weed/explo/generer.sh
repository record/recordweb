#!/bin/bash

###
# @author Nathalie Rousse, inra record team member.
#
# copyright (c) 2012-2013 INRA
#
# this program is free software: you can redistribute it and/or modify
# it under the terms of the gnu general public license as published by
# the free software foundation, either version 3 of the license, or
# (at your option) any later version.
#
# this program is distributed in the hope that it will be useful,
# but without any warranty; without even the implied warranty of
# merchantability or fitness for a particular purpose.  see the
# gnu general public license for more details.
#
# you should have received a copy of the gnu general public license
# along with this program.  if not, see <http://www.gnu.org/licenses/>.
#
###


# script de generation des resultats d'analyse du modele weed

echo "Generation (sous output/genere_lesAnalyses) des resultats d'analyse du modele weed"

cd ../output
if [ ! -d genere_lesAnalyses ]
then
    mkdir genere_lesAnalyses
fi

fichier_rapport=../output/genere_lesAnalyses/fichier_rapport.txt

### ### ###

cd ../output/genere_lesAnalyses
if [ ! -d impact_herb_labour ]
then
    mkdir impact_herb_labour
fi
cd ..

cd ../explo
Rscript impact_herb_labour.R > $fichier_rapport

### ### ###

cd ../explo
Rscript anova_weed.R >> $fichier_rapport

### ### ###

cd ../output/genere_lesAnalyses
if [ ! -d as ]
then
    mkdir as
fi
cd ..

cd ../explo
Rscript analyse_weed.R >> $fichier_rapport

### ### ###

