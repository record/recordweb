#-*- coding:utf-8 -*-

###
# @author Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2012-2013 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###

###############################################################################
#
#  Des appels sous Python de traitements R (differentes formes d'interaction)
#
#        avec PypeR (un paquet Python pour utiliser R dans Python)
#
###############################################################################

from pyper import *
r = R()

# les traitements R utilises
r("source('as.R')")
r("source('pour_python.R')")

# Execution sous Python d'instructions R ######################################

print "\ninstructions de lancement sous R d'une simulation."
r(" library('rvle') ")
r(" s = new( 'Rvle', file = 'weed_storage.vpz', pkg = 'weed' ) ")
r(" res = results(run( s, COND_Weed.phi = 0.5)) ")

print "\naffichage a l'ecran du resultat :"
#print(r('res'))
res=r("res")
print(res)

# Appel sous Python de fonctions R ############################################

print "\nappel de la fonction R d'analyse de sensibilite par methode de Morris."
r(" x=MethodeMorris(nomSortie='top,modele_weed:weed.DSBa') ")

print "\naffichage a l'ecran du resultat :"
#print( r('x') )
res_as= r('x')
print(res_as)

print "\nappel de la fonction R d'enregistrement de la representation graphique du"
print "resultat dans un fichier pdf (TMP_weed_morris.pdf)."
r(" Representation___R(x, 'TMP_weed_morris.pdf') ")

print "\ninstructions d'extraction/calcul sous R des valeurs mu.star et sigma."
r(" mu.star <- apply(x$ee, 2, function(x) mean(abs(x))) ")
r(" sigma <- apply(x$ee, 2, sd) ")

print "\naffichage a l'ecran des valeurs mu.star et sigma :"
mu_star = r(" mu.star ")
sigma = r(" sigma ")
print(mu_star)
print(sigma)
#mu_star
#sigma

# Appel sous Python d'un script R #############################################

print "\nappel du script R d'ANOVA."
res=runR("source('anova_weed.R')")

print "\naffichage a l'ecran du resultat :"
print(res)

###############################################################################

