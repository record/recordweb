/**
 * @file src/Weed.hpp
 */

/** @file
 *
 * ****************************************************************************
 *
 * @author Nathalie Rousse, INRA RECORD team member.
 *
 * Copyright (C) 2012-2013 INRA.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* Description ***************************************************************/
/** @file
 *
 * @brief
 * Fichier d'interface du modele weed : definition de valeurs prises par des
 * variables du modele (entrees, sorties, parametres), interface des
 * parametres du modele weed.
 *
 * Description ****************************************************************
 *
 * The model is a dynamic model of a weed infestation of blackgrass
 * (A. myosuroides) and it damage effect on wheat yield. It is based on weed
 * model of Munier-Jolain et al. (2002) that included classical ecological
 * concepts (survival, reproduction, fluxes between classes) often used to
 * described population dynamic.
 *
 * Publication : Munier-Jolain N.M., Chauvel B. and Gasquez J., 2002. Longterm modelling of weed control strategies: analysis of threshold-based options for weed species with contrasted competitive abilities. Weed Research 42, 107–122.
 *
 *****************************************************************************/

#ifndef WEED_HPP
#define WEED_HPP

#include <vle/extension/DifferenceEquation.hpp>
#include <trace.hpp>

namespace weed {

/******************************************************************************
 * definition de valeurs prises par des variables du modele
 ******************************************************************************/

/**
 * Valeurs pour une action de travail du sol (valeurs prises par Soil) \n
 * Values for soil work
 */
typedef enum ValeurActionSoil {

    /// travail du sol superficiel \n
    /// superficial work
    ACTIONSOIL_TRAVAILSOLSUPERFICIEL = 0,

    /// labour \n
    /// tillage
    ACTIONSOIL_LABOUR = 1

} ValeurActionSoil;

#define LEGENDE_ValeurActionSoil "ACTIONSOIL_TRAVAILSOLSUPERFICIEL correspond a 0, ACTIONSOIL_LABOUR correspond a 1"

/**
 * Valeurs pour une action d'application de traitement herbicide (valeurs prises par Herb) \n
 * Values for herbicide treatment
 */
typedef enum ValeurActionHerb {

    /// pas d'application de traitement herbicide \n
    /// no application of a treatment
    ACTIONHERB_SANSTRAITEMENT = 0,

    /// un traitement herbicide est applique \n
    /// application of a treatment
    ACTIONHERB_AVECTRAITEMENT = 1

} ValeurActionHerb;

#define LEGENDE_ValeurActionHerb "ACTIONHERB_SANSTRAITEMENT correspond a 0, ACTIONHERB_AVECTRAITEMENT correspond a 1"

/**
 * Valeurs pour un type de culture de ble (valeurs prises par Crop) \n
 * Values for a type of crop
 */
typedef enum ValeurTypeCrop {

    /// culture autre que ble d'hiver \n
    /// other than winter wheat
    TYPECROP_AUTRE = 0,

    /// la culture est du ble d'hiver \n
    /// winter wheat
    TYPECROP_BLEDHIVER = 1

} ValeurTypeCrop;

#define LEGENDE_ValeurTypeCrop "TYPECROP_AUTRE correspond a 0, TYPECROP_BLEDHIVER correspond a 1"

/******************************************************************************
 * Valeurs particulieres des sorties du modele weed
 ******************************************************************************/

#define NOMINAL_d         4.00e+02
#define NOMINAL_S         68000.0 // rq : valait 1.00e+04 dans code R de ECmexico2012
#define NOMINAL_SSBa      3.35e+03
#define NOMINAL_DSBa      2.80e+02

/* valeurs BINF_d,...,BINF_DSBa issues de code R de ECmexico2012, inutilisees */
#define BINF_d            300.0000
#define BINF_S            7500.0000
#define BINF_SSBa         2512.5000
#define BINF_DSBa         210.0000

/* valeurs BSUP_d,...,BSUP_DSBa issues de code R de ECmexico2012, inutilisees */
#define BSUP_d            500.0000
#define BSUP_S            12500.0000 // incorrect par rapport a NOMINAL_S !!!
#define BSUP_SSBa         4187.5000
#define BSUP_DSBa         350.0000

/******************************************************************************
 * Valeurs particulieres des entrees/parametres du modele weed
 ******************************************************************************/

#define NOMINAL_mu        8.40e-01
#define NOMINAL_v         6.00e-01
#define NOMINAL_phi       5.50e-01
#define NOMINAL_beta_1    9.50e-01
#define NOMINAL_beta_0    2.00e-01
#define NOMINAL_chsi_1    3.00e-01
#define NOMINAL_chsi_0    5.00e-02
#define NOMINAL_delta_new 1.50e-01
#define NOMINAL_delta_old 3.00e-01
#define NOMINAL_mh        9.80e-01
#define NOMINAL_mc        0.00e+00
#define NOMINAL_Smax_1    4.45e+02
#define NOMINAL_Smax_0    2.96e+02
#define NOMINAL_Ymax      8.00e+00
#define NOMINAL_rmax      2.00e-03
#define NOMINAL_gamma     5.00e-03

#define BINF_mu           0.7560
#define BINF_v            0.5400
#define BINF_phi          0.4950
#define BINF_beta_1       0.8550
#define BINF_beta_0       0.1800
#define BINF_chsi_1       0.2700
#define BINF_chsi_0       0.0450
#define BINF_delta_new    0.1350
#define BINF_delta_old    0.2700
#define BINF_mh           0.8820
#define BINF_mc           0.0000
#define BINF_Smax_1       400.5000
#define BINF_Smax_0       266.4000
#define BINF_Ymax         7.2000
#define BINF_rmax         0.0018
#define BINF_gamma        0.0045

#define BSUP_mu           0.9240
#define BSUP_v            0.6600
#define BSUP_phi          0.6050
#define BSUP_beta_1       1.0000
#define BSUP_beta_0       0.2200
#define BSUP_chsi_1       0.3300
#define BSUP_chsi_0       0.0550
#define BSUP_delta_new    0.1650
#define BSUP_delta_old    0.3300
#define BSUP_mh           1.0000
#define BSUP_mc           0.1000
#define BSUP_Smax_1       489.5000
#define BSUP_Smax_0       325.6000
#define BSUP_Ymax         8.8000
#define BSUP_rmax         0.0022
#define BSUP_gamma        0.0055

/***************************************************************//** \brief ...
 * Classe des parametres du modele Weed
 *
 * Les parametres :
 * - parametres determinant l'effet du travail du sol sur l'enfouissement et
 *   la mise a jour des graines de vulpin : beta_1, beta_0, chsi_1, chsi_0
 * - parametres definissant l'augmentation du nombre de graines : delta_new,
 *   delta_old, Smax_1, Smax_0, v 
 * - parametres determinant les pertes en graines : mu, phi, mh, mc
 * - parametres definissant la production de ble : Ymax, rmax, gamma
 *
 ****************************************************************************** 
 *
 * The model has 16 parameters :
 * - 4 parameters determine the effect of tillage on transfers between surface
 *   and depth seed bank : beta_0, beta_1, chsi_0 and chsi_1.
 * - 5 parameters determine the production of seeds : delta_new, delta_old,
 *   Smax_0, Smax_1 and v.
 * - 4 parameters determine the seed losses : mh, mc, phi and mu.
 * - 3 parameters define the wheat production : Ymax, rmax and gamma.
 *
 ******************************************************************************/
class Parametres {

public :

    /// Rate of annual seedbank decline
    double mu;

    /// Proportion of viable seeds
    double v;

    /// Fresh seed loss
    double phi;

    /// proportion of SSB buried (if Soil = 1)
    double beta_1;

    /// proportion of SSB buried (if Soil = 0)
    double beta_0;

    /// proportion of DSB carried up (if Soil = 1)
    double chsi_1;

    /// proportion of DSB carried up (if Soil = 0)
    double chsi_0;

    /// Seedling recruitment from SSB (Fresh seeds)
    double delta_new;

    /// Seedling recruitment from SSB (Old seeds)
    double delta_old;

    /// mortality of emerged weed due to herbicide (herbicide efficacy)
    double mh;

    /// seedling mortality due to cold
    double mc;

    /// Seeds per plant without density stress (autumn sown crops)
    double Smax_1;

    /// Seeds per plant without density stress (spring sown crops)
    double Smax_0;

    /// potential production of wheat without weed (ton.ha-1)
    double Ymax;

    /// Crop yield losses due to weed
    double rmax;

    /// reduction of production of wheat due to weed
    double gamma;

public :

    /*********************************************************************/ /**
     * Initialisation des parametres :
     * lecture des valeurs initiales dans le vpz, ou valeur par defaut
     **************************************************************************/
    void initialiser( const vle::devs::InitEventList& events ){

        // initialisation des affichages ecran
        t = new commun::Trace( commun::Trace::VALEURPARDEFAUT_MODE_TRACE );
        t_erreur = new commun::Trace( commun::Trace::VALEURPARDEFAUT_MODE_TRACE_ERREUR );

        /* initialisation parametres ******************************************/

        if ( events.exist("mu") ){
            mu = events.getDouble("mu");
        } else {
            mu = NOMINAL_mu;
            t->trace( MOTCLE_MSG_AVERTISSEMENT + " - Parametres::initialiser : absence d'evenement 'mu', parametre mu initialise a valeur nominale : ", mu );
        }
        if ( events.exist("v") ){
            v = events.getDouble("v");
        } else {
            v = NOMINAL_v;
            t->trace( MOTCLE_MSG_AVERTISSEMENT + " - Parametres::initialiser : absence d'evenement 'v', parametre v initialise a valeur nominale : ", v );
        }
        if ( events.exist("phi") ){
            phi = events.getDouble("phi");
        } else {
            phi = NOMINAL_phi;
            t->trace( MOTCLE_MSG_AVERTISSEMENT + " - Parametres::initialiser : absence d'evenement 'phi', parametre phi initialise a valeur nominale : ", phi );
        }
        if ( events.exist("beta_1") ){
            beta_1 = events.getDouble("beta_1");
        } else {
            beta_1 = NOMINAL_beta_1;
            t->trace( MOTCLE_MSG_AVERTISSEMENT + " - Parametres::initialiser : absence d'evenement 'beta_1', parametre beta_1 initialise a valeur nominale : ", beta_1 );
        }
        if ( events.exist("beta_0") ){
            beta_0 = events.getDouble("beta_0");
        } else {
            beta_0 = NOMINAL_beta_0;
            t->trace( MOTCLE_MSG_AVERTISSEMENT + " - Parametres::initialiser : absence d'evenement 'beta_0', parametre beta_0 initialise a valeur nominale : ", beta_0 );
        }
        if ( events.exist("chsi_1") ){
            chsi_1 = events.getDouble("chsi_1");
        } else {
            chsi_1 = NOMINAL_chsi_1;
            t->trace( MOTCLE_MSG_AVERTISSEMENT + " - Parametres::initialiser : absence d'evenement 'chsi_1', parametre chsi_1 initialise a valeur nominale : ", chsi_1 );
        }
        if ( events.exist("chsi_0") ){
            chsi_0 = events.getDouble("chsi_0");
        } else {
            chsi_0 = NOMINAL_chsi_0;
            t->trace( MOTCLE_MSG_AVERTISSEMENT + " - Parametres::initialiser : absence d'evenement 'chsi_0', parametre chsi_0 initialise a valeur nominale : ", chsi_0 );
        }
        if ( events.exist("delta_new") ){
            delta_new = events.getDouble("delta_new");
        } else {
            delta_new = NOMINAL_delta_new;
            t->trace( MOTCLE_MSG_AVERTISSEMENT + " - Parametres::initialiser : absence d'evenement 'delta_new', parametre delta_new initialise a valeur nominale : ", delta_new );
        }
        if ( events.exist("delta_old") ){
            delta_old = events.getDouble("delta_old");
        } else {
            delta_old = NOMINAL_delta_old;
            t->trace( MOTCLE_MSG_AVERTISSEMENT + " - Parametres::initialiser : absence d'evenement 'delta_old', parametre delta_old initialise a valeur nominale : ", delta_old );
        }
        if ( events.exist("mh") ){
            mh = events.getDouble("mh");
        } else {
            mh = NOMINAL_mh;
            t->trace( MOTCLE_MSG_AVERTISSEMENT + " - Parametres::initialiser : absence d'evenement 'mh', parametre mh initialise a valeur nominale : ", mh );
        }
        if ( events.exist("mc") ){
            mc = events.getDouble("mc");
        } else {
            mc = NOMINAL_mc;
            t->trace( MOTCLE_MSG_AVERTISSEMENT + " - Parametres::initialiser : absence d'evenement 'mc', parametre mc initialise a valeur nominale : ", mc );
        }
        if ( events.exist("Smax_1") ){
            Smax_1 = events.getDouble("Smax_1");
        } else {
            Smax_1 = NOMINAL_Smax_1;
            t->trace( MOTCLE_MSG_AVERTISSEMENT + " - Parametres::initialiser : absence d'evenement 'Smax_1', parametre Smax_1 initialise a valeur nominale : ", Smax_1 );
        }
        if ( events.exist("Smax_0") ){
            Smax_0 = events.getDouble("Smax_0");
        } else {
            Smax_0 = NOMINAL_Smax_0;
            t->trace( MOTCLE_MSG_AVERTISSEMENT + " - Parametres::initialiser : absence d'evenement 'Smax_0', parametre Smax_0 initialise a valeur nominale : ", Smax_0 );
        }
        if ( events.exist("Ymax") ){
            Ymax = events.getDouble("Ymax");
        } else {
            Ymax = NOMINAL_Ymax;
            t->trace( MOTCLE_MSG_AVERTISSEMENT + " - Parametres::initialiser : absence d'evenement 'Ymax', parametre Ymax initialise a valeur nominale : ", Ymax );
        }
        if ( events.exist("rmax") ){
            rmax = events.getDouble("rmax");
        } else {
            rmax = NOMINAL_rmax;
            t->trace( MOTCLE_MSG_AVERTISSEMENT + " - Parametres::initialiser : absence d'evenement 'rmax', parametre rmax initialise a valeur nominale : ", rmax );
        }
        if ( events.exist("gamma") ){
            gamma = events.getDouble("gamma");
        } else {
            gamma = NOMINAL_gamma;
            t->trace( MOTCLE_MSG_AVERTISSEMENT + " - Parametres::initialiser : absence d'evenement 'gamma', parametre gamma initialise a valeur nominale : ", gamma );
        }
}

private :

    /// pour les affichages ecran
    commun::Trace *t;
    commun::Trace *t_erreur;
};


} // namespace weed 

#endif // WEED_HPP

