/**
 * @file src/trace.hpp
 */

/** @file
 *
 * ****************************************************************************
 *
 * @author Nathalie Rousse, INRA RECORD team member.
 *
 * Copyright (C) 2012-2013 INRA.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* Description ***************************************************************/
/** @file
 *
 * @brief
 * Fichier contenant des utilitaires communs.
 *
 * Description ****************************************************************
 *
 *****************************************************************************/

#ifndef TRACE_HPP
#define TRACE_HPP


#include <iostream>

/// Mots cles employes dans les messages de trace (donnant leur nature)
#define MOTCLE_MSG_ERREUR (std::string)"ERREUR " ///< message d'erreur
#define MOTCLE_MSG_AVERTISSEMENT (std::string)"Avertissement " ///< message d'avertissement

namespace commun {

/***************************************************************//** \brief ...
 *
 * Classe de traitements utilitaires a base de std::cout (TRACE) et std::cin (PAUSE) permettant notamment de surveiller le deroulement en cours d'execution.
 *
 *****************************************************************************/
class Trace { /* trace... */

    private :
	bool avecTraceEcran;

    public :
        /// Valeur par defaut des affichages de trace ecran
        static const bool VALEURPARDEFAUT_MODE_TRACE = false; // true;
        static const bool VALEURPARDEFAUT_MODE_TRACE_ERREUR = true;

    public :
        /* les constructeurs */
        Trace( void ){
            avecTraceEcran = false; // pas de trace par defaut
	}
        Trace(bool avecTraceEcranInit){
            avecTraceEcran = avecTraceEcranInit;
	}

	/* des/activation de la trace */
	void activerTrace( void ){
            avecTraceEcran = true;
        }
	void desactiverTrace( void ){
            avecTraceEcran = false;
        }

	/* les actions de trace */
        template<class typeTraite>
        void trace( typeTraite texte ){
            if ( avecTraceEcran ){ std::cout << std::endl << texte; }
	}

        template<class typeTraite>
        void trace( std::string texte, typeTraite donnee ){
            if ( avecTraceEcran ){ std::cout << std::endl << texte << donnee; }
	}

        template<class typeTraite>
        void traceSansSaut( typeTraite texte ){
            if ( avecTraceEcran ){ std::cout << texte; }
	}
        template<class typeTraite>
        void traceSansSaut( std::string texte, typeTraite donnee ){
            if ( avecTraceEcran ){ std::cout << texte << donnee; }
	}

        template<class typeTraite>
        void traceAvecSautAvant( typeTraite texte ){
            if ( avecTraceEcran ){ std::cout << std::endl << texte; }
	}
        template<class typeTraite>
        void traceAvecSautApres( typeTraite texte ){
            if ( avecTraceEcran ){ std::cout << texte << std::endl; }
	}

        template<class typeTraite>
        void traceDonneesSurUneLigne( typeTraite e1, typeTraite e2, typeTraite e3 ){
            if ( avecTraceEcran ){
                traceAvecSautAvant( e1 ); traceSansSaut( " , " ); traceSansSaut( e2 ); traceSansSaut( " , " ); traceSansSaut( e3 );
	    }
        }
        template<class typeTraite>
        void traceDonneesSurUneLigne( typeTraite e1, typeTraite e2 ){
            if ( avecTraceEcran ){
                traceAvecSautAvant( e1 ); traceSansSaut( " , " ); traceSansSaut( e2 );
	    }
        }
};

} // namespace commun 

#endif /* TRACE_HPP */

