/**
 * @file src/Weed.cpp
 */

/** @file
 *
 * ****************************************************************************
 *
 * @author Nathalie Rousse, INRA RECORD team member.
 *
 * Copyright (C) 2012-2013 INRA.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* Description ***************************************************************/
/** @file
 *
 * @brief
 * Fichier correspondant au modele Weed (modele de mauvaises herbes)
 *
 * Description ****************************************************************
 *
 * The model is a dynamic model of a weed infestation of blackgrass
 * (A. myosuroides) and it damage effect on wheat yield. It is based on weed
 * model of Munier-Jolain et al. (2002) that included classical ecological
 * concepts (survival, reproduction, fluxes between classes) often used to
 * described population dynamic.
 *
 * Publication : Munier-Jolain N.M., Chauvel B. and Gasquez J., 2002. Longterm modelling of weed control strategies: analysis of threshold-based options for weed species with contrasted competitive abilities. Weed Research 42, 107–122.
 *
 *****************************************************************************/

#include <vle/extension/DifferenceEquation.hpp>

#include <trace.hpp> // pour affichages ecran

#include <Weed.hpp> // l'interface du modele Weed

namespace weed {

/***************************************************************//** \brief ...
 * Classe du modele Weed
 *
 * Le modele weed est un modele de simulation de la croissance d'une culture
 * de ble et d'une population de vulpin (adventice) sur une meme parcelle,
 * representes par les sorties Yield, S, d, SSBa, DSBa.
 *
 * The system is represented by 5 state variables : d, S, SSBa, DSBa, Yield.
 * The model is defined by 8 equations. The model has a time step of one
 * year, with an intermediate step that is tillage (before or after tillage).
 *
 *****************************************************************************/
class Weed : public vle::extension::DifferenceEquation::Multiple
{
public :

    /***********************************************************//** \brief ...
     * Structure des donnees d'entree du modele Weed
     *
     * Les donnees d'entree sont des decisions de techniques culturales
     *
     *************************************************************************/
    typedef struct VariablesDentree {

        /// action de travail du sol
        vle::extension::DifferenceEquation::Multiple::Sync Soil;

        /// action d'application de traitement herbicide
        vle::extension::DifferenceEquation::Multiple::Sync Herb;

        /// type de culture de ble
        vle::extension::DifferenceEquation::Multiple::Sync Crop;

    } VariablesDentree;

public :

    /***********************************************************//** \brief ...
     * Structure des donnees de sortie (effectives ou potentielles,
     * variables intermediaires) du modele Weed
     *************************************************************************/
    typedef struct VariablesDeSortie {

        /// nombre de graines de vulpin par m2.  \n
        /// seed production (seeds.m-2)
        Var S;

        /// densite de plantes de vulpin a emergence (en debut de saison) (plantes par m2.  \n
        /// weed density at emergence (plants.m-2)
        Var d;

        /// nombre de graines de vulpin par m2 dans le sol en surface apres travail du sol, banque de graines.  \n
        /// surface seed bank after tillage (seeds.m-2)
        Var SSBa;

        /// nombre de graines de vulpin par m2 dans le sol en profondeur apres travail du sol, banque de graines.  \n
        /// depth seed bank after tillage (seeds.m-2)
        Var DSBa;

        /// rendement en ble (t/ha).  \n
        /// wheat yield (t.ha-1)
        Var Yield;

        /// somme des rendements du ble sur les annees ecoulees (t par ha)
        Var YieldSum;

    } VariablesDeSortie;

public :
    /**********************************************************************//**
     * Creation des donnees
     *************************************************************************/
    void creerVariables( void ){

        /* Donnees d'entree */
        ve.Soil = createSync("Soil");
        ve.Herb = createSync("Herb");
        ve.Crop = createSync("Crop");

        /* Donnees de sortie */
        vs.S = createVar("S");
        vs.d = createVar("d");
        vs.SSBa = createVar("SSBa");
        vs.DSBa = createVar("DSBa");
        vs.Yield = createVar("Yield");
        vs.YieldSum = createVar("YieldSum");
    }

public :
    /**********************************************************************//**
     * Initialisation des donnees
     *************************************************************************/
    void initialiserVariables( void ){

        vs.S =    NOMINAL_S;
        vs.d =    NOMINAL_d;
        vs.SSBa = NOMINAL_SSBa;
        vs.DSBa = NOMINAL_DSBa;

        // Yield
        double d = NOMINAL_d;
        double Herb = ve.Herb();
        double D = (1 - p.mh * Herb) * (1 - p.mc) * d;
        double Yield = p.Ymax * (1 - (p.rmax * D/(1 + p.gamma * D)));
        if ( Yield < 0.0 ){ Yield = 0.0; }
        vs.Yield = Yield;

        // YieldSum
        vs.YieldSum = Yield;
    }

public :

    /**********************************************************************//**
     * Construction, configuration, initialisation du modele Weed
     *************************************************************************/
    Weed(const vle::devs::DynamicsInit& model,
         const vle::devs::InitEventList& events) :
    vle::extension::DifferenceEquation::Multiple(model, events)
    {

        // initialisation des affichages ecran
        t = new commun::Trace( commun::Trace::VALEURPARDEFAUT_MODE_TRACE );
        t_erreur = new commun::Trace( commun::Trace::VALEURPARDEFAUT_MODE_TRACE_ERREUR );

        // initialisation des parametres
        p.initialiser( events );

        // creations des variables
        creerVariables();

        // traces/affichages ecran
        t->trace( " " );
        t->trace( " *** Weed::Weed valeurs des parametres : " );
        t->trace( " " );
        t->trace( "mu : ", p.mu );
        t->trace( "v : ", p.v );
        t->trace( "phi : ", p.phi );
        t->trace( "beta_1 : ", p.beta_1 );
        t->trace( "beta_0 : ", p.beta_0 );
        t->trace( "chsi_1 : ", p.chsi_1 );
        t->trace( "chsi_0 : ", p.chsi_0 );
        t->trace( "delta_new : ", p.delta_new );
        t->trace( "delta_old : ", p.delta_old );
        t->trace( "mh : ", p.mh );
        t->trace( "mc : ", p.mc );
        t->trace( "Smax_1 : ", p.Smax_1 );
        t->trace( "Smax_0 : ", p.Smax_0 );
        t->trace( "Ymax : ", p.Ymax );
        t->trace( "rmax : ", p.rmax );
        t->trace( "gamma : ", p.gamma );
        t->trace( " " );
        t->trace( " --------------------------------------- Weed::Weed LEGENDE debut : ----- " );
        t->trace( " * Signification des valeurs que Soil peut prendre : ", LEGENDE_ValeurActionSoil );
        t->trace( " * Signification des valeurs que Herb peut prendre : ", LEGENDE_ValeurActionHerb );
        t->trace( " * Signification des valeurs que Crop peut prendre : ", LEGENDE_ValeurTypeCrop );
        t->trace( " ----------------------------------------------------- LEGENDE fin. ----- " );
        t->trace( " " );

        /* pas de controles/verifications sur les parametres */
    }

    virtual ~Weed() { }

    /**********************************************************************//**
     * Initialisation des variables 
     * effectuee ici et non pas dans .vpz pour le cas ou 
     * les valeurs(time=-1) des variables Sync interviendraient dans
     * le calcul(time=0)
     *************************************************************************/
    virtual void initValue(const vle::devs::Time& /* time */)
    {
        t->trace( " ***** Weed::initValue" );
        initialiserVariables();
    }

    /**********************************************************************//**
     *
     * Traitement effectue a chaque pas de temps.
     *
     * Deux equations de definition des quantites de graines de vulpin
     * presentes en surface et en profondeur :
     * - SSBb : le stock de graines de vulpin en surface au debut de l'annee
     *   avant travail du sol = stock precedent - proportion ayant deperi ou
     *   etant devenue plantule + quantite nette de graines produites.
     * - DSBb : le stock de graines de vulpin en profondeur au debut de
     *   l'annee avant travail du sol = stock precedent - proportion ayant
     *   deperi.
     *
     * Deux equations rendant compte de l'effet du travail du sol sur le
     * transfert de graines entre les 2 zones (flux surface-profondeur) :
     * - beta : flux de graines de la surface vers la profondeur.
     * - chsi : flux inverse de graines de la profondeur vers la surface .
     *
     * Quantite de graines presentes en surface et en profondeur :
     * - SSBa = (1-beta) SSBb + chsi DSBb.
     * - DSBa = (1-chsi) DSBb + beta SSBb.
     *
     * d : nombre de plantules de vulpin emergeant calcule a partir des
     * differents stocks de graines de surface.
     *
     * D : nombre de plantules de vulpin parvenant a maturite (causes de
     * mortalite de plantules : froid, herbicide).
     *
     * Yield : rendement en ble calcule a partir de la quantite de plants de
     * vulpins matures et d'un rendement maximum.
     *
     * S : nombre de graines de vulpin par m2.
     *
     * YieldSum : cumul des rendements en ble Yield depuis la premiere annee.
     *
     *************************************************************************/
    virtual void compute(const vle::devs::Time& time )
    {
        t->trace( " ***** Weed::compute, time vaut ", time );

        double beta = p.beta_1 * ve.Soil() + p.beta_0 * (1 - ve.Soil());
        double chsi = p.chsi_1 * ve.Soil() + p.chsi_0 * (1 - ve.Soil());

        double SSBb = (1 - p.mu) * (vs.SSBa(-1) - vs.d(-1)) + p.v * (1 - p.phi) * vs.S(-1);
        double DSBb = (1 - p.mu) * vs.DSBa(-1);

        double SSBa = (1 - beta) * SSBb + chsi * DSBb;
        double DSBa = (1 - chsi) * DSBb + beta * SSBb;

        double d = p.delta_new * p.v * (1 - p.phi) * (1 - beta) * vs.S(-1) + p.delta_old * (SSBa - vs.S(-1) * p.v * (1 - p.phi) * (1 - beta));

        double D = (1 - p.mh * ve.Herb()) * (1 - p.mc) * d;

        double Smax = p.Smax_1 * ve.Crop() + p.Smax_0 * (1 - ve.Crop());
        double alpha = Smax/160000;
        double S = Smax * D/(1 + alpha * D);

        double Yield = p.Ymax * (1 - (p.rmax * D/(1 + p.gamma * D)));
        if ( Yield < 0.0 ){ Yield = 0.0; }

        double YieldSum = Yield + vs.YieldSum(-1);

        vs.d = d;
        vs.S = S;
        vs.SSBa = SSBa;
        vs.DSBa = DSBa;
        vs.Yield = Yield;
        vs.YieldSum = YieldSum;
    }

private:
    /**************************************************************************
     * Donnees du modele Weed
     *************************************************************************/

    /// les parametres
    weed::Parametres p;

    /// les donnees d'entree
    VariablesDentree ve;

    /// les donnees de sortie (effectives ou potentielles, variables intermediaires)
    VariablesDeSortie vs;

private :

    /// pour les affichages ecran
    commun::Trace *t;
    commun::Trace *t_erreur;

};

} // namespace weed 

DECLARE_NAMED_DYNAMICS(Weed, weed::Weed);

