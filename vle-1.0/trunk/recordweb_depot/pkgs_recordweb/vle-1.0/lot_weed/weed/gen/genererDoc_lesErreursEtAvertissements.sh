#!/bin/bash

###
# @author Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2012-2013 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###


# Script de generation du fichier de documentation (sous genere_listeErreursEtAvertissements) qui donne la liste des erreurs et avertissements geres par le modele logiciel weed

nomFichier=lesErreursEtAvertissements
nomFichierTxt=$nomFichier.txt

echo "Generation du fichier de documentation $nomFichierTxt qui donne la liste des erreurs et avertissements geres par le modele logiciel weed"

cheminSrc=../src  # cheminSrc=$1

cheminGenRes=../build/generer_resultats
cheminResultat=$cheminGenRes/genere_doc_lesErreursEtAvertissements # cheminResultat=$2
if [ ! -d $cheminGenRes ]
then
    mkdir $cheminGenRes
fi
if [ ! -d $cheminResultat ]
then
    mkdir $cheminResultat
fi

dest=$cheminResultat/$nomFichierTxt

echo "***************************************************************" > $dest
echo "*                                                              " >> $dest
echo "*                  Modele weed                                 " >> $dest
echo "*                                                              " >> $dest
echo "***************************************************************" >> $dest
echo "" >> $dest
echo "" >> $dest
echo "***************************************************************" >> $dest
echo "* Liste des erreurs gerees par le modele logiciel weed :" >> $dest
echo "***************************************************************" >> $dest
echo "" >> $dest
echo "Les messages d'erreur de type 't_erreur->trace' sont affiches a l'ecran en cours de simulation." >> $dest
echo "A l'affichage, ces messages d'erreur sont accompagnes du mot cle defini a la ligne commencant par '#define MOTCLE_MSG_ERREUR'." >> $dest
echo "" >> $dest

   nomPaquet="weed"
   grep -h MOTCLE_MSG_ERREUR $cheminSrc/*.cpp >> $dest
   grep -h MOTCLE_MSG_ERREUR $cheminSrc/*.hpp >> $dest

echo "" >> $dest
echo "***************************************************************" >> $dest
echo "* Liste des avertissements geres par le modele logiciel weed :" >> $dest
echo "***************************************************************" >> $dest
echo "" >> $dest
echo "Les messages d'avertissement de type 't->trace' sont affiches a l'ecran en cours de simulation." >> $dest
echo "A l'affichage, ces messages d'avertissement sont accompagnes du mot cle defini a la ligne commencant par '#define MOTCLE_MSG_AVERTISSEMENT'." >> $dest
echo "" >> $dest

   nomPaquet="weed"
   grep -h MOTCLE_MSG_AVERTISSEMENT $cheminSrc/*.cpp >> $dest
   grep -h AVERTISSEMENT $cheminSrc/*.hpp >> $dest

echo "" >> $dest
echo "***************************************************************" >> $dest

# Mise en forme (suppression des ' ' superflus)
cat $dest | tr --squeeze-repeats ' ' > $cheminResultat/tmp.txt
mv -f $cheminResultat/tmp.txt $dest

# Generation du pdf
# enscript $dest -r -p $cheminResultat/tmp.ps
# ps2pdf $cheminResultat/tmp.ps $cheminResultat/$nomFichier.pdf
# rm -f $cheminResultat/tmp.ps

