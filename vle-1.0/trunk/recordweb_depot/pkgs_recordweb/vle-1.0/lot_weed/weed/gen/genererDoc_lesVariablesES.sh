#!/bin/bash

###
# @author Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2012-2013 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###


# Script de generation du fichier de documentation bilan des variables E/S du modele weed

nomFichier=lesVariablesES
nomFichierTxt=$nomFichier.txt

echo "Generation du fichier de documentation $nomFichierTxt bilan des variables E/S du modele weed"

cheminSrc=../src  # cheminSrc=$1

cheminGenRes=../build/generer_resultats
cheminResultat=$cheminGenRes/genere_doc_lesVariablesES # cheminResultat=$2
if [ ! -d $cheminGenRes ]
then
    mkdir $cheminGenRes
fi
if [ ! -d $cheminResultat ]
then
    mkdir $cheminResultat
fi

dest=$cheminResultat/$nomFichierTxt

echo "***************************************************************" > $dest
echo "*                                                             *" >> $dest
echo "*                  Modele weed                                *" >> $dest
echo "*                                                             *" >> $dest
echo "***************************************************************" >> $dest
echo "" >> $dest
echo "***************************************************************" >> $dest
echo "* Liste des variables de sortie du modele weed " >> $dest
echo "* (sorties effectives ou potentielles, autrement dit observables, Var) :" >> $dest
echo "***************************************************************" >> $dest
echo "" >> $dest

   nomPaquet="weed"
   echo "" >> $dest
   grep createVar $cheminSrc/Weed.cpp >> $dest

echo "" >> $dest
echo "***************************************************************" >> $dest
echo "* Liste des variables d'entree du modele weed :" >> $dest
echo "***************************************************************" >> $dest
echo "" >> $dest

   nomPaquet="weed"
   echo "*** les variables d'entree du type Sync :" >> $dest
   echo "" >> $dest
   grep createSync $cheminSrc/Weed.cpp >> $dest
   echo "" >> $dest
   echo "*** les variables d'entree du type Nosync :" >> $dest
   echo "" >> $dest
   grep createNosync $cheminSrc/Weed.cpp >> $dest

echo "" >> $dest
echo "***************************************************************" >> $dest

# Generation du pdf
# enscript $dest -r -p $cheminResultat/tmp.ps
# ps2pdf $cheminResultat/tmp.ps $cheminResultat/$nomFichier.pdf
# rm -f $cheminResultat/tmp.ps

