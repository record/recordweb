#!/bin/bash

###
# @author Nathalie Rousse, INRA RECORD team member.
#
# Copyright (C) 2012-2013 INRA
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###


# Script de construction (configure + build) des paquets composant le logiciel WEED

echo " Construction (configure + build) des paquets composant le logiciel WEED"

echo "***  Paquet lecture_fichier *********************************************"
vle -P lecture_fichier configure build
echo "***  Paquet weed ********************************************************"
vle -P weed configure build

