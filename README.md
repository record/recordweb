
### **[online documentation of erecord site](https://record.pages.mia.inra.fr/recordweb)**

## erecord web services, web solutions for vle models

The **erecord web services** allow to **edit, modify and simulate
some vle models**, such as the **Record platform** ones.

The **erecord** web services have been online
*(at http://erecord.toulouse.inra.fr)* until 2024.
From 2024, only **documentation part of erecord site remains published**, now
online at :
**[documentation of erecord site](https://record.pages.mia.inra.fr/recordweb)**

## Latest version in production

- **vle-x** is the latest version that had been under production (development
  stopped), the latest version that had been installed under erecord VM
  (erecord.toulouse.inra.fr).

- Presentation 

  - erecord project (**2018**-**2024**) :
    the web services provided by erecord allow to edit, modify and simulate
    some vle models, such as the Record platform ones.

  - Features :
    - Version using erecord package instead of pyvle.
    - Version supporting several vle versions (vle-1.1.3, vle-2.0.0...).

  - Note :
    The erecord online documentation uses/refers to the archive of the old
    webrecord site (no longer online) (see vle-1.0 version), that has been 
    copied/installed under vle-x :
    recordweb/vle-x/trunk/archive_doc_online/varwww/misc/webrecord is a copy
    of recordweb/vle-1.0/archive/webrecord. 

# Older, previous versions in production
                                  
- **vle-1.1**

  The erecord project (**2014**-**2018**) is dedicated to the Record platform
  web development, concerning more specifically its models developed with
  vle-1.1 version.
  The web services provided by erecord allow to edit, modify and simulate
  some vle models, such as the Record platform ones.

  Version using pyvle.

- **vle-1.0**

  The webrecord project (**2011**-**2014**), web solutions for models of the
  Record platform :
  a web application prototype that enables remote simulations of
  some existing agronomic models developped with vle-1.0.3 version, that have
  been previously loaded into the web tool.

  An archive :

  - There exists an archive of the webrecord site (no longer online) dedicated
    to the webrecord project presentation and productions :
    recordweb/vle-1.0/archive/webrecord/webrecord.pdf and
    recordweb/vle-1.0/archive/webrecord

  - This archive is required by the erecord online documentation : see
    vle-x version.

# Tests not finalized
                                  
- **dev/erecord** is a version of **tests**, **not operational**.

- Presentation

  - erecord project (from 2019) :
    the web services provided by erecord allow to edit, modify and simulate
    some vle models, such as the Record platform ones.

  - Features :
    - Version using containers for models repositories.
    - Version under python 3.

