# archive_doc_online

Contains the last generated documentation that was online on erecord site
(erecord.toulouse.inra.fr), captured at closing.

It has been archived here.

It remains published, now on Gitlab pages of this recordweb project (CI/CD). 

