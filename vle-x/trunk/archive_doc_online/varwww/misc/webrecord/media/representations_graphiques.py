# Affichage des traces des sorties de simulation du fichier_scenario_vpz
# (une representation graphique par vue).
# Utilise les classes Exp et Graphique de recorweb (recordweb/record/utils)

# path du repertoire recordweb
import os, sys
RECORDWEB_HOME = "/home/nrousse/workspace_svn/recordweb"
if RECORDWEB_HOME not in sys.path :
    sys.path.insert(0, RECORDWEB_HOME )

# pour traitements vle (simulation...)
from record.utils.vle.exp import Exp

# pour traitements graphiques
from record.utils.graphique import Graphique

# Fonction
# d'affichage des traces des sorties de simulation du fichier_scenario_vpz
# (une representation graphique par vue)
def representationsGraphiques( fichier_scenario_vpz, paquet_vle ) :

    exp = Exp( fichier_scenario_vpz, paquet_vle ) # fichier scenario vpz
    
    res = exp.mono_runModeStorage() # simulation

    for nomvue in res.keys() :
        vue = res[nomvue]
        les_XY=[]
        les_legendes=[]
        X = ( 'time', vue['time'] )
        for name in vue.keys() :
            if name is not 'time' :
                Y = ( name, vue[name] )
                les_XY.append( (X,Y) )
                n = name.split(":")[-1] # diminutif
                les_legendes.append(n)
        g=Graphique()
        g.setTitre( "Variables de la vue " + nomvue )
        g.setLegendes( "time", "Y", les_legendes )
        g.setLesXY( les_XY )
        image=g.produireGraphique()
        image.show()

# application du traitement au scenario de simulation weed.vpz (du paquet weed)
fichier_scenario_vpz = "weed.vpz"
paquet_vle = "weed"
representationsGraphiques( fichier_scenario_vpz, paquet_vle )

