# -*- coding: utf-8 -*-
"""factory.static

Static files installation

Contains the static files of erecord package.

Contains a directory for each project where (i) a subdirectory for static files that are common to the erecord package, (ii) a subdirectory for each app static files

See :ref:`index`

"""
