-------------------------------------------------------------------------------
* Case :
- wwdm_data.zip as datafolder
- overwrite datafoldercopy
- cond_meteo.meteo_file="31035002_ter.csv"
- linear plan (mode)
- dataframe restype (mode)
- format yaml

* cURL command line :

curl -F 'vpz=266' -F 'datafolder=@wwdm_data.zip' -F 'datafoldercopy=overwrite' -F 'cond_meteo.meteo_file="31035002_ter.csv"' -F 'duration=4' -F 'cond_wwdm.TI=[899.0,900.0,901.0]' -F 'parselect=cond_meteo.meteo_file' -F 'parselect=cond_wwdm.A' -F 'parselect=cond_wwdm.B' -F 'parselect=cond_wwdm.TI' -F 'outselect=view.top:wwdm.LAI' -F 'outselect=view.top:wwdm.U' -F 'mode=todownload' -F 'plan=linear' -F 'restype=dataframe' -F 'format=yaml' http://erecord.toulouse.inra.fr:8000/vpz/report/

* Comments :

The modified input datas folder : data/31035002.csv
                                      /serieclim.dat
                                      /31035002_bis.csv
                                      /31035002_ter.csv

-------------------------------------------------------------------------------

