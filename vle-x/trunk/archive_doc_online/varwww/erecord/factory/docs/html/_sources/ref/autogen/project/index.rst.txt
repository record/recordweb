.. erecord documentation master file, created by
   sphinx-quickstart on Mon Nov 19 11:03:11 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to erecord's documentation!
===================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   databases
   docs
   erecord
   factory
   repositories


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
