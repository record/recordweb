factory.install package
=======================

Module contents
---------------

.. automodule:: factory.install
    :members:
    :undoc-members:
    :show-inheritance:
