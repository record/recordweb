
-> *Example* : 
:ref:`In a webbrowser <erecord_use_capture_031>` |
:ref:`In command line with cURL <erecord_use_capture_131>` |
:ref:`In Python language <erecord_use_capture_231>` |
:ref:`In R language <erecord_use_capture_331>` |
:ref:`In PHP language <erecord_use_capture_431>` |
:ref:`In C++ language <erecord_use_capture_531>`

