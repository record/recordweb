
======================
More about db/rep/{Id}
======================

The 'db/rep/{Id}' resource is one of the resources of :ref:`webapi_db`.

Code
====

The corresponding class is VleRepDetail.

For more about VleRepList, see :

    .. autoclass:: erecord_db.views.objects.VleRepDetail
       :members:
