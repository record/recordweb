.. _wwdm_ident_fields:

================================
Information to set and/or select
================================

Introduction
============

These operations are useful as prerequisites for :ref:`wwdm_editmodif`,
:ref:`wwdm_runmodif`.

They are based on :ref:`wwdm_editasis`, :ref:`wwdm_editmodif`.

To configure (set, select) some information of the simulator (when
:ref:`wwdm_editmodif`, :ref:`wwdm_runmodif`), it will be necessary to know
how this information is identified/selected in the request.

.. _wwdm_ident_parameters:

Parameters and conditions
=========================

  A simulator :ref:`parameter <dm_vlepar>` is identified by its 'cname' and
  'pname' values, its 'selection_name' value *(concatenation of 'cname' and
  'pname')*. In the :ref:`input information <dm_vpzinput>`, the simulator
  parameters are gathered in some conditions (sets of parameters). A simulator
  :ref:`condition <dm_vlecond>` is identified by its 'selection_name' value.

  When :ref:`Seeing the modified simulator <wwdm_editmodif>` with 'all' as
  '**parselect**' value and '**compact**' as style of presentation :

    - you will access to the 'pname', 'cname', 'selection_name' values of each
      existing parameter.
    - you will access to the 'selection_name' values of each existing condition.

  .. include:: include/wwdm_editmodif_quickoverview_compact.rst

  .. note:: '**compactlist**' as style of presentation can also be used
     instead of 'compact' value. You can try it in the following example but
     giving 'all' value for '**parselect**'.

     .. include:: include/wwdm_editmodif_quickoverview.rst

  .. note:: An easier/faster solution is to use
     :ref:`Seeing the simulator as is <wwdm_editasis>` with 'compact' as style
     of presentation. In that case, the URL to be entered in a webbrowser is :
     http://erecord.toulouse.inra.fr:8000/vpz/input/?vpz=266&mode=compact

.. _wwdm_ident_outputs:

Output datas and views
======================

  An :ref:`output data <dm_vleout>` of the simulator is identified by its
  'selection_name' value *(concatenation of its 'vname' and 'oname')*. In the
  :ref:`output information <dm_vpzoutput>`, the simulator output datas are
  gathered in some views (sets of output datas). A simulator
  :ref:`view <dm_vleview>` is identified by its 'selection_name' value.

  When :ref:`Seeing the modified simulator <wwdm_editmodif>` with 'all' as
  '**outselect**' value and '**compact**' as style of presentation :

    - you will access to the 'selection_name' values of each existing
      output data.
    - you will access to the 'selection_name' value of each existing view.

  -> *Example* : :ref:`in a web browser <erecord_use_capture_012>`

  .. include:: include/wwdm_editmodif_quickoverview_compact.rst

  .. note:: '**compactlist**' as style of presentation can also be used
     instead of 'compact' value. You can try it in the following example but
     giving 'all' value for '**outselect**'.

     .. include:: include/wwdm_editmodif_quickoverview.rst

