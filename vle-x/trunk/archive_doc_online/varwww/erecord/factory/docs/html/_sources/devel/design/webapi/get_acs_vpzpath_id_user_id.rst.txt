.. _get_acs_vpzpath_id_user_id:

============================
GET acs/vpzpath/{Id}/user/id
============================

Resource URL
============
:ref:`online_url`\ **/acs/vpzpath/{Id}/user/id**

Description
===========

Returns the list of id of users authorized for a :term:`simulator` (as :ref:`dm_vpzpath`) having the Id value (as :ref:`dm_vpzpath`).

Request parameters
==================

* *Id* : id value of the simulator as :ref:`dm_vpzpath`

* :ref:`webapi_opt_format` 

Response result
===============

"list" : list of users ids.


More
====

This resource is part of :ref:`webapi_acs`

