.. _webapi_opt_jwt:

===
jwt
===

The 'jwt' option is used to give an available JWT value, required in limited access case (:ref:`for more <erecord_authentication>`).

- jwt

    - value : an available JWT (JSON Web Token) value.

.. note::

   To get a JWT value : :ref:`post_acs_jwt_obtain` request.

   See also : :ref:`howto_get_jwt`

   For more : :ref:`erecord_authentication`

