# -*- coding: UTF-8 -*-
"""erecord_db.views.menus """

from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.reverse import reverse

from erecord_cmn.utils.urls import url_add_options

from erecord_db.serializers import RepOptionSerializer
from erecord_db.serializers import PkgOptionSerializer
from erecord_db.serializers import VpzOptionSerializer
from erecord_db.models import VleVersion
from erecord_db.models import VleRep
from erecord_db.models import VlePkg
from erecord_db.models import VleVpz

from erecord_db.forms import VpzInputFormatForm
from erecord_db.forms import VpzInputStyleForm
from erecord_db.forms import VpzInputOutselectForm
from erecord_db.forms import VpzOutputFormatForm
from erecord_db.forms import VpzOutputStyleForm
from erecord_db.forms import VpzOutputPlanForm
from erecord_db.forms import VpzOutputRestypeForm
from erecord_db.forms import VpzOutputStorageForm
from erecord_db.forms import VpzOutputOutselectForm
from erecord_db.forms import VpzOutputDatafoldercopyForm
from erecord_db.forms import VpzInoutFormatForm
from erecord_db.forms import VpzInoutStyleForm
from erecord_db.forms import VpzInoutPlanForm
from erecord_db.forms import VpzInoutRestypeForm
from erecord_db.forms import VpzInoutOutselectForm
from erecord_db.forms import VpzInoutDatafoldercopyForm
from erecord_db.forms import VpzInoutModifyTodownloadForm
from erecord_db.forms import VpzInoutModifyDatafoldercopyForm
from erecord_db.forms import IdentityFormatForm
from erecord_db.forms import IdentityStyleForm

from django.template.context_processors import csrf
from django import forms

#headsubtitle = "erecord_db application (models repositories and their tree)"
headsubtitle = "(erecord_db)"

#------------------------------------------------------------------------------

# common definitions

cases_format_style = ('api', 'html', 'json', 'api_tree', 'json_tree')

options_format_style = {
    'html' : { 'format':'html', },
    'api' : { 'format':'api', 'mode':'link' },
    'json' : { 'format':'json', 'mode':'link' },
    'api_tree' : { 'format':'api', 'mode':'tree' },
    'json_tree' : { 'format':'json', 'mode':'tree' },
}

titles = {
    'api' :      "(.api, link style) :  ",
    'html' :     "(.html) :             ",
    'json' :     "(.json, link style) :  ",
    'api_tree' : "(.api, tree style) :  ",
    'json_tree': "(.json, tree style) : ",
}

objectcases = ('vlerep', 'vlepkg', 'vlevpz')

#------------------------------------------------------------------------------

@api_view(('GET',))
def home( request, format=None ):
    """erecord_db application

    The erecord_db application manages the database.
    The database contains the models repositories and their tree (vle \
    packages and vpz files).

    Services for all users : reading database information.
    Services only for admin user : managing database information (create, \
    update, delete).

    Main menu
    """

    from erecord_cmn.configs.config import ONLINEDOC_URL

    generated_admindocs = 'generated admindocs :   ' + \
        reverse('django-admindocs-docroot', request=request, format=format)
    online_documentation = 'online documentation (erecord web site) : ' + \
        ONLINEDOC_URL

    docs = [
        generated_admindocs,
        online_documentation,
        ]

    main_menus = [
        "Home menu (.api) :  " + \
            url_add_options(reverse('erecord_db-home-menu', request=request, 
            format=format), {'format':'api'} ),
        "Home menu (.json) : " + \
            url_add_options(reverse('erecord_db-home-menu', request=request, 
            format=format), {'format':'json'} ),
        "Page menu (html) :  " + \
            reverse('erecord_db-menu-main-page-list', request=request,
            format=format),
        ]

    admin_pages = [
        'Administration : (db/admin)',
        ]

    home_page = [
        "MENUS :" , main_menus,

        "LISTS, a submenu to see all the existing objects into the " + \
        "database (vlerep, vlepkg, vlevpz...)", 
        [ "LISTS : " + \
        reverse('erecord_db-home-menu-lists', request=request, format=format),
        ],

        "DETAIL, a submenu to see one existing object of the database " + \
        "(vlerep, vlepkg, vlevpz...).", 
        "The object is selected by its id. To know an object id, " + \
        "go to the LISTS submenu.", 
        [ "DETAIL (the '1' value must be changed by the id value) : " + \
        reverse('erecord_db-home-menu-detail', args=(1,), request=request),
        ],

        "Some docs :" , docs,
        "Only for admin :" , admin_pages,
        ]

    return Response( home_page )

#------------------------------------------------------------------------------

class MenuPage(APIView):
    """menu page

    Some actions menus on vlevpz after having selected it by lists of vlerep, \
    vlepkg, vlevpz.

    The actions depend on 'show_postvpzinput', 'show_...' ...

    The lists of vlerep, vlepkg, vlevpz are filtered according to rep, pkg, \
    vpz options. Singleton if pk (Id of VleVpz) is given.

    vlerep_list is built from VleRep, VlePkg, VleVpz : containing only 'id' \
    and 'name' fields (and rebuilt 'vlepkg_list', 'vlevpz_list'), not the \
    full information.

    """

    def init_page(self, pk=None):
        """ Create the page context (all desactivated by default) """

        vlerep_list = self.get_vlerep_list(pk=pk)
        identity_form_list = [IdentityFormatForm(), IdentityStyleForm(),]

        # token not managed by form in order to show it only for locked vpz
        # note : token also required for 'download' button
        postvpzinput_form_list = [
                 VpzInputFormatForm(), VpzInputStyleForm(),
                 VpzInputOutselectForm(),] # +token
        postvpzinout_modify_form_list = [
                 VpzInoutModifyTodownloadForm(),
                 VpzInoutModifyDatafoldercopyForm(),] # +token
        postvpzoutput_form_list = [
                 VpzOutputFormatForm(), VpzOutputStyleForm(),
                 VpzOutputPlanForm(), VpzOutputRestypeForm(),
                 VpzOutputStorageForm(), VpzOutputOutselectForm(),
                 VpzOutputDatafoldercopyForm(),] # +token
        postvpzinout_form_list = [
                 VpzInoutFormatForm(), VpzInoutStyleForm(), VpzInoutPlanForm(),
                 VpzInoutRestypeForm(), VpzInoutOutselectForm(),
                 VpzInoutDatafoldercopyForm(),] # +token

        self.context = { 'vlerep_list': vlerep_list,
            'identity_form_list':identity_form_list,
            'postvpzinput_form_list':postvpzinput_form_list,
            'postvpzinout_modify_form_list':postvpzinout_modify_form_list,
            'postvpzoutput_form_list':postvpzoutput_form_list,
            'postvpzinout_form_list':postvpzinout_form_list,
            'headsubtitle':headsubtitle,
            #'show_postvpzinput':1,
            #'show_postvpzinout_modify':1,
            #'show_postvpzoutput':1,
            #'show_postvpzinout':1,
                  }
        self.context.update(csrf(self.request))
        self.template_name = 'erecord_db/menu.html'
            
    def activate_postvpzinput(self):
        self.context['show_postvpzinput'] = 1
            
    def activate_postvpzinout_modify(self):
        self.context['show_postvpzinout_modify'] = 1
            
    def activate_postvpzoutput(self):
        self.context['show_postvpzoutput'] = 1
            
    def activate_postvpzinout(self):
        self.context['show_postvpzinout'] = 1

    def get_vlerep_list(self, pk=None):
        """" Build and return the vlerep list to present """

        def limited_value(model) :
            if model.has_lock() :
                limited = 'yes'
            else :
                limited = 'no'
            return limited

        if not pk :
            p = RepOptionSerializer(data=self.request.query_params)
            p.is_valid()
            rep = p.data['rep']
            p = PkgOptionSerializer(data=self.request.query_params)
            p.is_valid()
            pkg = p.data['pkg']
            p = VpzOptionSerializer(data=self.request.query_params)
            p.is_valid()
            vpz = p.data['vpz']
            vlerep_list = list()
            if vpz is None and pkg is None and rep is None : # no filter

                #vlerepfull_list = VleRep.objects.all()
                # ordered by vleversion (descendant) then name :
                vlerepfull_list = list()
                vle_version_list = VleVersion.objects.all().order_by('-name')
                for vle_version in vle_version_list :
                    #vlerepfull_list.extend(vle_version.vlerep_list.all())
                    vlerepfull_list.extend(vle_version.vlerep_list.all().order_by('name'))

                for vlerepfull in vlerepfull_list :
                    vlerep = { 'id':vlerepfull.id, 'name':vlerepfull.name,
                               'vlepkg_list':list() }
                    #vlepkgfull_list = vlerepfull.vlepkg_list.all()
                    vlepkgfull_list = vlerepfull.vlepkg_list.all().order_by('name')
                    for vlepkgfull in vlepkgfull_list :
                        vlepkg = { 'id':vlepkgfull.id, 'name':vlepkgfull.name,
                                   'vlevpz_list':list() }
                        vlevpzfull_list = vlepkgfull.vlevpz_list.all().order_by('name')
                        for vlevpzfull in vlevpzfull_list :
                            vlevpz = { 'id':vlevpzfull.id,
                                       'name':vlevpzfull.name,
                                       'limited':limited_value(vlevpzfull) }
                            vlepkg['vlevpz_list'].append(vlevpz)
                        vlerep['vlepkg_list'].append(vlepkg)
                    vlerep_list.append(vlerep)
            elif vpz is not None : # only vlevpz with id=vpz
                if VleVpz.objects.filter(pk=vpz).exists():
                    vlevpzfull = VleVpz.objects.get(pk=vpz)
                    vlevpz = {'id':vlevpzfull.id,
                               'name':vlevpzfull.name,
                               'limited':limited_value(vlevpzfull) }
                    vlepkg = {'id':vlevpzfull.vlepkg.id,
                              'name':vlevpzfull.vlepkg.name,
                              'vlevpz_list':[vlevpz]}
                    vlerep = {'id':vlevpzfull.vlepkg.vlerep.id,
                              'name':vlevpzfull.vlepkg.vlerep.name,
                              'vlepkg_list':[vlepkg]}
                    vlerep_list = [vlerep]
            elif pkg is not None : # only vlepkg with id=pkg
                if VlePkg.objects.filter(pk=pkg).exists():
                    vlepkgfull = VlePkg.objects.get(pk=pkg)
                    vlepkg = {'id':vlepkgfull.id, 'name':vlepkgfull.name,
                              'vlevpz_list':list() }
                    vlevpzfull_list = vlepkgfull.vlevpz_list.all().order_by('name')
                    for vlevpzfull in vlevpzfull_list :
                        vlevpz = { 'id':vlevpzfull.id,
                                   'name':vlevpzfull.name,
                                   'limited':limited_value(vlevpzfull) }
                        vlepkg['vlevpz_list'].append(vlevpz)
                    vlerep = {'id':vlepkgfull.vlerep.id,
                              'name':vlepkgfull.vlerep.name,
                              'vlepkg_list':[vlepkg]}
                    vlerep_list = [vlerep]
            elif rep is not None : # only vlerep with id=rep
                if VleRep.objects.filter(pk=rep).exists():
                    vlerepfull = VleRep.objects.get(pk=rep)
                    vlerep = { 'id':vlerepfull.id, 'name':vlerepfull.name,
                               'vlepkg_list':list() }
                    vlepkgfull_list = vlerepfull.vlepkg_list.all().order_by('name')
                    for vlepkgfull in vlepkgfull_list :
                        vlepkg = { 'id':vlepkgfull.id, 'name':vlepkgfull.name,
                                   'vlevpz_list':list() }
                        vlevpzfull_list = vlepkgfull.vlevpz_list.all().order_by('name')
                        for vlevpzfull in vlevpzfull_list :
                            vlevpz = { 'id':vlevpzfull.id,
                                       'name':vlevpzfull.name,
                                       'limited':limited_value(vlevpzfull) }
                            vlepkg['vlevpz_list'].append(vlevpz)
                        vlerep['vlepkg_list'].append(vlepkg)
                    vlerep_list = [vlerep]
        else : # only one
            vpz = pk
            vlerep_list = list()
            if VleVpz.objects.filter(pk=vpz).exists():
                vlevpzfull = VleVpz.objects.get(pk=vpz)
                vlevpz = {'id':vlevpzfull.id,
                          'name':vlevpzfull.name,
                          'limited':limited_value(vlevpzfull) }
                vlepkg = {'id':vlevpzfull.vlepkg.id,
                          'name':vlevpzfull.vlepkg.name,
                          'vlevpz_list':[vlevpz]}
                vlerep = {'id':vlevpzfull.vlepkg.vlerep.id,
                      'name':vlevpzfull.vlepkg.vlerep.name,
                      'vlepkg_list':[vlepkg]}
                vlerep_list = [vlerep]
            else :
                vlerep_list = None
        return vlerep_list

class MenuPageList(MenuPage):
    pass

class MenuPageDetail(MenuPage):
    def init_page(self, pk=None):
        super(MenuPageDetail, self).init_page(pk=pk)
        self.context['first_one'] = 'first_one'

class MenuMainPageList(MenuPageList):
    """menu page

    Some actions menus on vlevpz after having selected it by lists of vlerep, \
    vlepkg, vlevpz (see also MenuPage).
    """

    def get(self, request, format=None):
        self.init_page()
        self.activate_postvpzinput()
        self.activate_postvpzinout_modify()
        self.activate_postvpzoutput()
        self.activate_postvpzinout()
        return render(request, self.template_name, self.context)

class MenuVpzInputPageList(MenuPageList):
    """menu page list vpz/input """

    def get(self, request, format=None):
        self.init_page()
        self.activate_postvpzinput()
        return render(request, self.template_name, self.context)

class MenuVpzOutputPageList(MenuPageList):
    """menu page list vpz/output """

    def get(self, request, format=None):
        self.init_page()
        self.activate_postvpzoutput()
        return render(request, self.template_name, self.context)

class MenuVpzInoutPageList(MenuPageList):
    """menu page list vpz/inout """

    def get(self, request, format=None):
        self.init_page()
        self.activate_postvpzinout()
        return render(request, self.template_name, self.context)

class MenuVpzInoutModifyPageList(MenuPageList):
    """menu page list vpz/experiment """

    def get(self, request, format=None):
        self.init_page()
        self.activate_postvpzinout_modify()
        return render(request, self.template_name, self.context)

class MenuMainPageDetail(MenuPageDetail):
    """menu page for the vlevpz with id

    Some actions menus on vlevpz (see also MenuPage).
    """

    def get(self, request, pk, format=None):
        self.init_page(pk=pk)
        self.activate_postvpzinput()
        self.activate_postvpzinout_modify()
        self.activate_postvpzoutput()
        self.activate_postvpzinout()
        return render(request, self.template_name, self.context)

class MenuVpzInputPageDetail(MenuPageDetail):
    """menu page detail vpz/input """

    def get(self, request, pk, format=None):
        self.init_page(pk=pk)
        self.activate_postvpzinput()
        return render(request, self.template_name, self.context)

class MenuVpzOutputPageDetail(MenuPageDetail):
    """menu page detail vpz/output """

    def get(self, request, pk, format=None):
        self.init_page(pk=pk)
        self.activate_postvpzoutput()
        return render(request, self.template_name, self.context)

class MenuVpzInoutPageDetail(MenuPageDetail):
    """menu page detail vpz/inout """

    def get(self, request, pk, format=None):
        self.init_page(pk=pk)
        self.activate_postvpzinout()
        return render(request, self.template_name, self.context)

class MenuVpzInoutModifyPageDetail(MenuPageDetail):
    """menu page detail vpz/experiment """

    def get(self, request, pk, format=None):
        self.init_page(pk=pk)
        self.activate_postvpzinout_modify()
        return render(request, self.template_name, self.context)

#------------------------------------------------------------------------------

@api_view(('GET',))
def home_lists( request, format=None ):
    """erecord_db application

    The erecord_db application manages the database.
    The database contains the models repositories and their tree (vle \
    packages and vpz files).

    Lists menu
    """

    titles['vlerep'] = "All the models repositories."
    titles['vlepkg'] = "All the vle packages."
    titles['vlevpz'] = "All the vpz files."

    viewnames = {
        'vlerep': 'erecord_db-rep-list',
        'vlepkg': 'erecord_db-pkg-list',
        'vlevpz': 'erecord_db-vpz-list',
    }

    home_list = dict()
    for case in objectcases :
        home_list[case] = [ titles[case] ]
        for f in cases_format_style :
            line = titles[f] + url_add_options(reverse(viewnames[case], 
                request=request), options_format_style[f])
            home_list[case].append(line)

    titles_filters = dict()
    titles_filters['vlerep'] = "The models repositories that are " + \
        "consistent with filters."
    titles_filters['vlepkg'] = "The vle packages that are " + \
        "consistent with filters."
    titles_filters['vlevpz'] = "The vpz files that are " + \
        "consistent with filters."

    options_filters = {
    'vlerep' : { 'rep':1 },
    'vlepkg' : { 'rep':1, 'pkg':1, },
    'vlevpz' : { 'rep':1, 'pkg':1, 'vpz':1, },
    }

    home_list_filters = dict()
    for case in objectcases :
        home_list_filters[case] = [ titles_filters[case] ]
        for f in cases_format_style :
            options = options_filters[case]
            for k,v in options_format_style[f].iteritems() :
                options[k] = v
            line = titles[f] + url_add_options(reverse(viewnames[case], 
                request=request), options)
            home_list_filters[case].append(line)

    home_lists = [
        "VleRep, models repositories.", 
        home_list['vlerep'],

        "VlePkg, vle packages.", 
        home_list['vlepkg'] ,

        "VleVpz, vpz files.", 
        home_list['vlevpz'] ,
        ]

    def line_option(name,classname) :
        return "('"+name+"' value to be replaced by the id value of " + \
               "the "+ classname + " to keep, or else no '"+name+"' option)."

    home_lists_filters = [
        "VleRep, models repositories.", 
        line_option('rep','VleRep'),

        home_list_filters['vlerep'],

        "VlePkg, vle packages.", 
        line_option('rep','VleRep'),
        line_option('pkg','VlePkg'),

        home_list_filters['vlepkg'] ,

        "VleVpz, vpz files.", 
        line_option('rep','VleRep'),
        line_option('pkg','VlePkg'),
        line_option('vpz','VleVpz'),

        home_list_filters['vlevpz'] ,
        ]


    text = [
        ".... VleRep .... VlePkg .... VleVpz ....",
        ]

    home_page = [ 
        'Introduction', text,

        "To see the lists of the existing objects of the database " + \
        "(vlerep, vlepkg...).", 
        home_lists,

        "To see the lists of objects of the database " + \
        "(vlerep, vlepkg...), filtered by adding some options :",
        "The 'rep' option is used to specify which models repository to " + \
        "keep. The 'rep' value is the id of the selected VleRep, " + \
        "or else no 'rep' option.",
        "The 'pkg' option is used to specify which vle package to keep : " + \
        "The 'pkg' value is the id of the selected VlePkg, " + \
        "or else no 'pkg' option.",
        "The 'vpz' option is used to specify which vpz file to keep : " + \
        "The 'vpz' value is the id of the selected VleVpz, " + \
        "or else no 'vpz' option.",
        "To know a VleRep id, a VlePkg id, a VleVpz id, " + \
        "go to the LISTS submenu.",
        home_lists_filters,
    ]
    return Response( home_page )


@api_view(('GET',))
def home_details( request, pk, format=None ):
    """erecord_db application

    The erecord_db application manages the database.
    The database contains the models repositories and their tree (vle \
    packages and vpz files).

    Detail menu
    """

    titles['vlerep'] = "All the models repositories."
    titles['vlepkg'] = "All the vle packages."
    titles['vlevpz'] = "All the vpz files."


    titles['vlerep'] = "The models repository whose id is "+str(pk)+ \
        " (value to be replaced by the VleRep id value)."
    titles['vlepkg'] = "The vle package whose id is "+str(pk)+ \
        " (value to be replaced by the VlePkg id value)."
    titles['vlevpz'] = "The vpz file whose id is "+str(pk)+ \
        " (value to be replaced by the VleVpz id value)."

    viewnames = {
        'vlerep': 'erecord_db-rep-detail',
        'vlepkg': 'erecord_db-pkg-detail',
        'vlevpz': 'erecord_db-vpz-detail',
    }

    home_detail = dict()
    for case in objectcases :
        home_detail[case] = [ titles[case] ]
        for f in cases_format_style :
            for k,v in options_format_style[f].iteritems() :
                line = titles[f] + url_add_options(reverse(viewnames[case], 
                    args=(pk,), request=request), options_format_style[f])
            home_detail[case].append(line)

    home_details = [

        "VleRep.",
        "The VleRep is selected by its id. To know a VleRep id, " + \
        "go to the LISTS submenu.",
        home_detail['vlerep'],

        "VlePkg.",
        "The VlePkg is selected by its id. To know a VlePkg id, " + \
        "go to the LISTS submenu.",
        home_detail['vlepkg'],

        "VleVpz.",
        "The VleVpz is selected by its id. To know a VleVpz id, " + \
        "go to the LISTS submenu.",
        home_detail['vlevpz'],

        ]

    text = [
        ".... VleRep .... VlePkg .... VleVpz ....",
        ]

    home_page = [ 
        'Introduction', text,

        "To see one existing object of the database (vlerep, vlepkg...).", 
        "The object is selected by its id. To know an object id, " + \
        "go to the LISTS submenu.", 
        home_details,

    ]
    return Response( home_page )

#------------------------------------------------------------------------------

