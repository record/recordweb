# -*- coding: UTF-8 -*-
"""erecord_db.views.objects """

from rest_framework.response import Response
###from rest_framework import status
from rest_framework import generics

from erecord_db.models import VleVersion
from erecord_db.models import VleRep
from erecord_db.models import VlePkg
from erecord_db.models import VleVpz

from erecord_db.serializers import VleOptionSerializer
from erecord_db.serializers import RepOptionSerializer
from erecord_db.serializers import PkgOptionSerializer
from erecord_db.serializers import VpzOptionSerializer
from erecord_db.serializers import VleVersionSerializerLink
from erecord_db.serializers import VleRepSerializerLink
from erecord_db.serializers import VlePkgSerializerLink
from erecord_db.serializers import VleVpzSerializerLink
from erecord_db.serializers import VleVersionSerializerTree
from erecord_db.serializers import VleRepSerializerTree
from erecord_db.serializers import VlePkgSerializerTree
from erecord_db.serializers import VleVpzSerializerTree

from erecord_db.forms import VleVersionUserForm
from erecord_db.forms import VleRepUserForm
from erecord_db.forms import VlePkgUserForm
from erecord_db.forms import VleVpzUserForm

from erecord_cmn.views_mixins import ListViewMixin
from erecord_cmn.views_mixins import DetailViewMixin
from erecord_cmn.views_mixins import StyleViewMixin
from erecord_cmn.views_mixins import FormatViewMixin


#headsubtitle = "erecord_db application (models repositories and their tree)"
headsubtitle = "(erecord_db)"

#------------------------------------------------------------------------------

class VleVersionList(StyleViewMixin, FormatViewMixin, ListViewMixin,
        generics.ListAPIView):
    """The requested vle versions

    For more information, see the online documentation
    (:ref:`get_db_vle`).

    """

    model = VleVersion

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VleVersionSerializerTree, 
                serializer_link_class=VleVersionSerializerLink) 

    def get_queryset(self):
        p = VleOptionSerializer(data=self.request.query_params)
        p.is_valid()
        vle = p.data['vle']
        if vle is not None :
            q = VleVersion.objects.filter(id=vle)
        else :
            q = VleVersion.objects.all()
        return q

    def get(self, request, format=None, **kwargs):
        context = super(VleVersionList, self).get(request, format, **kwargs)
        if format is None :
            format = self.get_format_value(data=request.query_params)
        if format=='html' :
            context = { 'headsubtitle':headsubtitle, }
            context['title'] = 'Vle versions'
            context['form_list'] = [ VleVersionUserForm(instance=o) 
                    for o in self.get_queryset() ] #for o in self.object_list ] 
            return Response(context )
        else :
            return context

class VleRepList(StyleViewMixin, FormatViewMixin, ListViewMixin,
        generics.ListAPIView):
    """The requested models repositories

    For more information, see the online documentation
    (:ref:`get_db_rep`).

    """

    model = VleRep

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VleRepSerializerTree, 
                serializer_link_class=VleRepSerializerLink) 

    def get_queryset(self):
        p = RepOptionSerializer(data=self.request.query_params)
        p.is_valid()
        rep = p.data['rep']
        if rep is not None :
            q = VleRep.objects.filter(id=rep)
        else :
            q = VleRep.objects.all()
        return q

    def get(self, request, format=None, **kwargs):
        context = super(VleRepList, self).get(request, format, **kwargs)
        if format is None :
            format = self.get_format_value(data=request.query_params)
        if format=='html' :
            context = { 'headsubtitle':headsubtitle, }
            context['title'] = 'Models repositories'
            context['form_list'] = [ VleRepUserForm(instance=o) 
                    for o in self.get_queryset() ] #for o in self.object_list ] 
            return Response(context )
        else :
            return context

class VlePkgList(StyleViewMixin, FormatViewMixin, ListViewMixin,
        generics.ListAPIView):
    """The requested vle packages

    For more information, see the online documentation
    (:ref:`get_db_pkg`).

    """

    model = VlePkg

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VlePkgSerializerTree, 
                serializer_link_class=VlePkgSerializerLink) 

    def get_queryset(self):
        p = RepOptionSerializer(data=self.request.query_params)
        p.is_valid()
        rep = p.data['rep']
        p = PkgOptionSerializer(data=self.request.query_params)
        p.is_valid()
        pkg = p.data['pkg']
        if rep is not None :
            q = VlePkg.objects.filter(vlerep=rep)
        else :
            q = VlePkg.objects.all()
        if pkg is not None :
            q = q.filter(id=pkg)
        return q

    def get(self, request, format=None, **kwargs):
        context = super(VlePkgList, self).get(request, format, **kwargs)
        if format is None :
            format = self.get_format_value(data=request.query_params)
        if format=='html' :
            context = { 'headsubtitle':headsubtitle, }
            context['title'] = 'Models' # 'Vle packages'
            context['form_list'] = [ VlePkgUserForm(instance=o) 
                    for o in self.get_queryset() ] #for o in self.object_list ] 
            return Response(context )
        else :
            return context

class VleVpzList(StyleViewMixin, FormatViewMixin, ListViewMixin,
        generics.ListAPIView):
    """The requested vpz files

    For more information, see the online documentation
    (:ref:`get_db_vpz`).

    """

    model = VleVpz

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VleVpzSerializerTree, 
                serializer_link_class=VleVpzSerializerLink) 

    def get_queryset(self):
        p = RepOptionSerializer(data=self.request.query_params)
        p.is_valid()
        rep = p.data['rep']
        p = PkgOptionSerializer(data=self.request.query_params)
        p.is_valid()
        pkg = p.data['pkg']
        p = VpzOptionSerializer(data=self.request.query_params)
        p.is_valid()
        vpz = p.data['vpz']

        q = VleVpz.objects.all()
        if rep is not None :
            q = q.filter(vlepkg__vlerep=rep)
        if pkg is not None :
            q = q.filter(vlepkg=pkg)
        if vpz is not None :
            q = q.filter(id=vpz)
        return q

    def get(self, request, format=None, **kwargs):
        context = super(VleVpzList, self).get(request, format, **kwargs)
        if format is None :
            format = self.get_format_value(data=request.query_params)
        if format=='html' :
            context = { 'headsubtitle':headsubtitle, }
            context['title'] = 'Simulators' # 'Vpz files'
            context['form_list'] = [ VleVpzUserForm(instance=o) 
                    for o in self.get_queryset() ] #for o in self.object_list ] 
            return Response(context )
        else :
            return context

class VleVersionDetail(StyleViewMixin, FormatViewMixin, DetailViewMixin,
        generics.RetrieveAPIView):
    """One vle version

    For more information, see the online documentation
    (:ref:`get_db_vle_id`).

    """

    model = VleVersion

    def get_queryset(self):
        return VleVersion.objects.all()

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VleVersionSerializerTree, 
                serializer_link_class=VleVersionSerializerLink) 

    def get(self, request, format=None, **kwargs):
        context = super(VleVersionDetail, self).get(request, format, **kwargs)
        if format is None :
            format = self.get_format_value(data=request.query_params)
        if format=='html' :
            vleversion = VleVersion.objects.get(pk=kwargs['pk'])
            context = dict()
            title = 'Vle version (Id ' + str(vleversion.id) + ')'
            context['title'] = title # 'Vle version'
            context['headsubtitle'] = headsubtitle
            context['form'] = VleVersionUserForm(instance=vleversion)
            return Response(context )
        else :
            return context

class VleRepDetail(StyleViewMixin, FormatViewMixin, DetailViewMixin,
        generics.RetrieveAPIView):
    """One models repository 

    For more information, see the online documentation
    (:ref:`get_db_rep_id`).

    """

    model = VleRep

    def get_queryset(self):
        return VleRep.objects.all()

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VleRepSerializerTree, 
                serializer_link_class=VleRepSerializerLink) 

    def get(self, request, format=None, **kwargs):
        context = super(VleRepDetail, self).get(request, format, **kwargs)
        if format is None :
            format = self.get_format_value(data=request.query_params)
        if format=='html' :
            vlerep = VleRep.objects.get(pk=kwargs['pk'])
            context = dict()
            title = 'Models repository (Id ' + str(vlerep.id) + ')'
            context['title'] = title # 'Models repository'
            context['headsubtitle'] = headsubtitle
            context['form'] = VleRepUserForm(instance=vlerep)
            return Response(context )
        else :
            return context

class VlePkgDetail(StyleViewMixin, FormatViewMixin, DetailViewMixin,
        generics.RetrieveAPIView):
    """One vle package

    For more information, see the online documentation
    (:ref:`get_db_pkg_id`).

    """

    model = VlePkg

    def get_queryset(self):
        return VlePkg.objects.all()
            
    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VlePkgSerializerTree, 
                serializer_link_class=VlePkgSerializerLink) 

    def get(self, request, format=None, **kwargs):

        context = super(VlePkgDetail, self).get(request, format, **kwargs)
        if format is None :
            format = self.get_format_value(data=request.query_params)
        if format=='html' :
            vlepkg = VlePkg.objects.get(pk=kwargs['pk'])
            context = dict()
            title = 'Model (Id ' + str(vlepkg.id) + ')'
            context['title'] = title # 'Vle package'
            context['headsubtitle'] = headsubtitle
            context['form'] = VlePkgUserForm(instance=vlepkg)
            return Response(context )
        else :
            return context

class VleVpzDetail(StyleViewMixin, FormatViewMixin, DetailViewMixin,
        generics.RetrieveAPIView):
    """One vpz file 

    For more information, see the online documentation
    (:ref:`get_db_vpz_id`).

    """

    model = VleVpz

    def get_queryset(self):
        return VleVpz.objects.all()

    def get_serializer_class(self):
        return self.get_serializer_class_style(
                serializer_tree_class=VleVpzSerializerTree, 
                serializer_link_class=VleVpzSerializerLink) 

    def get(self, request, format=None, **kwargs):
        context = super(VleVpzDetail, self).get(request, format, **kwargs)
        if format is None :
            format = self.get_format_value(data=request.query_params)
        if format=='html' :
            vlevpz = VleVpz.objects.get(pk=kwargs['pk'])
            context = dict()
            title = 'Simulator (Id ' + str(vlevpz.id) + ')'
            context['title'] = title # 'Vpz file'
            context['headsubtitle'] = headsubtitle
            context['form'] = VleVpzUserForm(instance=vlevpz)
            return Response(context )
        else :
            return context

#------------------------------------------------------------------------------

