# -*- coding: utf-8 -*-
"""erecord_db.urls

Urls of the erecord_db application

"""

from django.conf.urls import include, url

from django.conf import settings
from django.conf.urls.static import static

from django.views.generic import TemplateView
from django.views.generic.base import RedirectView

from rest_framework.urlpatterns import format_suffix_patterns

from erecord_db.views import menus as db_views_menus
from erecord_db.views import objects as db_views_objects
from erecord_db.views import views as db_views_views

# to enable the admin:
#from django.contrib import admin
#admin.autodiscover()


    #url(r'^go-to-django/$', RedirectView.as_view(url='http://djangoproject.com'), name='go-to-django'),


urlpatterns = [
        
    #url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    #url(r'^admin/', admin.site.urls),

    # root page
    url(r'^$', TemplateView.as_view(template_name='erecord_cmn/index.html'),
        name="erecord_db-index-page"),

    # menu pages

    url(r'^menu/misc/$',
        TemplateView.as_view(template_name='erecord_db/menu_misc.html'),
        name='erecord_db-menu-misc-page'),
    url(r'^menu/$', db_views_menus.MenuMainPageList.as_view(),
        name='erecord_db-menu-main-page-list'),
    url(r'^menu/input/$', db_views_menus.MenuVpzInputPageList.as_view(),
        name='erecord_db-menu-vpzinput-page-list'),
    url(r'^menu/output/$', db_views_menus.MenuVpzOutputPageList.as_view(),
        name='erecord_db-menu-vpzoutput-page-list'),
    url(r'^menu/inout/$', db_views_menus.MenuVpzInoutPageList.as_view(),
        name='erecord_db-menu-vpzinout-page-list'),
    url(r'^menu/experiment/$',
        db_views_menus.MenuVpzInoutModifyPageList.as_view(),
        name='erecord_db-menu-vpzinoutmodify-page-list'),

    url(r'^menu/(?P<pk>[0-9]+)/$', db_views_menus.MenuMainPageDetail.as_view(),
        name="erecord_db-menu-main-page-detail"),
    url(r'^menu/input/(?P<pk>[0-9]+)/$',
        db_views_menus.MenuVpzInputPageDetail.as_view(),
        name="erecord_db-menu-vpzinput-page-detail"),
    url(r'^menu/output/(?P<pk>[0-9]+)/$',
        db_views_menus.MenuVpzOutputPageDetail.as_view(),
        name="erecord_db-menu-vpzoutput-page-detail"),
    url(r'^menu/inout/(?P<pk>[0-9]+)/$',
        db_views_menus.MenuVpzInoutPageDetail.as_view(),
        name="erecord_db-menu-vpzinout-page-detail"),
    url(r'^menu/experiment/(?P<pk>[0-9]+)/$',
        db_views_menus.MenuVpzInoutModifyPageDetail.as_view(),
        name="erecord_db-menu-vpzinoutmodify-page-detail"),

    # home menus

    url(r'^home/$', db_views_menus.home, name="erecord_db-home-menu"),

    url(r'^home/lists/$', db_views_menus.home_lists,
        name="erecord_db-home-menu-lists"),
    url(r'^home/detail/(?P<pk>[0-9]+)/$', db_views_menus.home_details,
        name="erecord_db-home-menu-detail"),

    # lists and details
    # formats : .api,.json,.html
    # options : mode=tree,link ; format=api,json,html

    url(r'^vle/$', db_views_objects.VleVersionList.as_view(),
        name='erecord_db-vle-list' ),
    url(r'^rep/$', db_views_objects.VleRepList.as_view(),
        name='erecord_db-rep-list' ),
    url(r'^pkg/$', db_views_objects.VlePkgList.as_view(),
        name='erecord_db-pkg-list' ),
    url(r'^vpz/$', db_views_objects.VleVpzList.as_view(),
        name='erecord_db-vpz-list' ),

    url(r'^vle/(?P<pk>[0-9]+)/$', db_views_objects.VleVersionDetail.as_view(),
        name='erecord_db-vle-detail' ),
    url(r'^rep/(?P<pk>[0-9]+)/$', db_views_objects.VleRepDetail.as_view(),
        name='erecord_db-rep-detail' ),
    url(r'^pkg/(?P<pk>[0-9]+)/$', db_views_objects.VlePkgDetail.as_view(),
        name='erecord_db-pkg-detail' ),
    url(r'^vpz/(?P<pk>[0-9]+)/$', db_views_objects.VleVpzDetail.as_view(),
        name='erecord_db-vpz-detail' ),

    # name, datalist, access

    url(r'^rep/(?P<pk>[0-9]+)/name/$', db_views_views.VleRepNameView.as_view(),
        name='erecord_db-rep-name' ),
    url(r'^pkg/(?P<pk>[0-9]+)/name/$', db_views_views.VlePkgNameView.as_view(),
        name='erecord_db-pkg-name' ),
    url(r'^vpz/(?P<pk>[0-9]+)/name/$', db_views_views.VleVpzNameView.as_view(),
        name='erecord_db-vpz-name' ),

    url(r'^pkg/(?P<pk>[0-9]+)/datalist/$',
        db_views_views.VlePkgDatalistView.as_view(),
        name='erecord_db-pkg-datalist' ),

#   url(r'^vpz/(?P<pk>[0-9]+)/access/$',
#       db_views_views.VleVpzAccessView.as_view(),
#       name='erecord_db-vpz-access' ),

#   # accessible simulators (VleVpz)
#   url(r'^vpz/accessible/id/$',
#       db_views_views.VleVpzAccessibleIdView.as_view(),
#       name='erecord_db-vpz-accessible-id' ),

#   # id of authorized users of a simulator (VleVpz) in limited access
#   url(r'^vpz/(?P<pk>[0-9]+)/user/id/$',
#       db_views_views.VleVpzUserIdView.as_view(),
#       name='erecord_db-vpz-user-id' ),

#   # name of authorized users of a simulator (VleVpz) in limited access
#   url(r'^vpz/(?P<pk>[0-9]+)/user/name/$',
#       db_views_views.VleVpzUserNameView.as_view(),
#       name='erecord_db-vpz-user-name' ),

]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns = format_suffix_patterns(urlpatterns,
                  allowed=['json', 'api', 'html', 'yaml', 'xml',])

