# -*- coding: utf-8 -*-
"""erecord_acs.urls

Urls of the erecord_acs application

"""

from django.conf.urls import include, url

from django.conf import settings
from django.conf.urls.static import static

from rest_framework.urlpatterns import format_suffix_patterns

from rest_framework_jwt.views import obtain_jwt_token
#from rest_framework_jwt.views import refresh_jwt_token
from rest_framework_jwt.views import verify_jwt_token

from erecord_acs import views as acs_views

# to enable the admin:
#from django.contrib import admin
#admin.autodiscover()

urlpatterns = [
        
    #url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    #url(r'^admin/', admin.site.urls),

    # JWT Authentication

    url(r'^jwt/obtain/', obtain_jwt_token, name="token-auth"),
    #url(r'^jwt/refresh/', refresh_jwt_token),
    url(r'^jwt/verify/', verify_jwt_token, name="token-verify"),
    url(r'^login', obtain_jwt_token),

    # simulator (VleVpz) access
    url(r'^vpz/(?P<pk>[0-9]+)/access/$', acs_views.VleVpzAccessView.as_view(),
        name='erecord_acs-vpz-access' ),

    # accessible simulators (VleVpz)
    url(r'^vpz/accessible/id/$', acs_views.VleVpzAccessibleIdView.as_view(),
        name='erecord_acs-vpz-accessible-id' ),

    # id of authorized users of a simulator (VleVpz) in limited access
    url(r'^vpz/(?P<pk>[0-9]+)/user/id/$', acs_views.VleVpzUserIdView.as_view(),
        name='erecord_acs-vpz-user-id' ),

    # name of authorized users of a simulator (VleVpz) in limited access
    url(r'^vpz/(?P<pk>[0-9]+)/user/name/$',
        acs_views.VleVpzUserNameView.as_view(),
        name='erecord_acs-vpz-user-name' ),

    # simulator (VpzPath) access
    url(r'^vpzpath/(?P<pk>[0-9]+)/access/$',
        acs_views.VpzPathAccessView.as_view(),
        name='erecord_acs-vpzpath-access' ),

    # accessible simulators (VpzPath)
    url(r'^vpzpath/accessible/id/$',
        acs_views.VpzPathAccessibleIdView.as_view(),
        name='erecord_acs-vpzpath-accessible-id' ),

    # id of authorized users of a simulator (VpzPath) in limited access
    url(r'^vpzpath/(?P<pk>[0-9]+)/user/id/$',
        acs_views.VpzPathUserIdView.as_view(),
        name='erecord_acs-vpzpath-user-id' ),

    # name of authorized users of a simulator (VpzPath) in limited access
    url(r'^vpzpath/(?P<pk>[0-9]+)/user/name/$',
        acs_views.VpzPathUserNameView.as_view(),
        name='erecord_acs-vpzpath-user-name' ),

]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns = format_suffix_patterns(urlpatterns,
                  allowed=['json', 'api', 'html', 'yaml', 'xml',])

