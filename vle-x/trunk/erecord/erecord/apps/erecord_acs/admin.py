# -*- coding: utf-8 -*-
"""erecord_acs.admin"""

from django.contrib import admin

from erecord_acs.models import Lock
from erecord_acs.models import VpzLock
from erecord_acs.models import ActivityLock
from erecord_acs.models import LoadLock

from django.contrib.auth.models import User
from erecord_db.models import VleVpz
from erecord_vpz.models import VpzPath
from erecord_vpz.models import VpzAct
from erecord_vpz.models import VpzOrigin
from erecord_vpz.models import VpzInput
from erecord_vpz.models import VpzOutput
from erecord_vpz.models import VleBegin
from erecord_vpz.models import VleDuration
from erecord_vpz.models import VleCond
from erecord_vpz.models import VlePar
from erecord_vpz.models import VleView
from erecord_vpz.models import VleOut
from erecord_vpz.models import VpzWorkspace
from erecord_slm.models import UploadVpzDocument
from erecord_slm.models import DownloadVpzDocument

# To show/hide some admin parts

#from django.contrib.auth.models import User
#from django.contrib.auth.models import Group
##admin.site.unregister(User)
##admin.site.unregister(Group)

from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group
#admin.site.unregister(Group)
admin.site.unregister(User)

lock_general_description = \
        "Lock is used to limit accesses. \
        Lock makes a link between (i) some users and (ii) some objects with \
        limited access that are allowed to them. \
        Lock is controlled by 0..n Users. \
        Lock controls 0..n objects such as VleVpz or VpzPath or any object \
        built from them \
        (VpzAct, VpzInput, VleDuration,..., UploadVpzDocument)."


vpzlock_general_description = \
        "VpzLock is used to limit accesses. \
        VpzLock makes a link between (i) some users and (ii) some objects with \
        limited access that are allowed to them. \
        VpzLock is controlled by 0..n Users. \
        VpzLock controls 0..n objects such as VleVpz or VpzPath."

vpzlock_identification_description = \
        ""
class VpzLockingUserInline(admin.TabularInline):
    model = VpzLock.user_list.through
class VpzLockingVleVpzInline(admin.TabularInline):
    model = VpzLock.vlevpz_list.through
class VpzLockingVpzPathInline(admin.TabularInline):
    model = VpzLock.vpzpath_list.through

class VpzLockAdmin(admin.ModelAdmin):
    list_display = ('id', 'text', )
    inlines = [VpzLockingUserInline,
               VpzLockingVleVpzInline, VpzLockingVpzPathInline, ]
    exclude = ['user_list', 'vlevpz_list', 'vpzpath_list', ]

    fieldsets = (
        (None, {
            'description': vpzlock_general_description,
            'fields': (),
        }),
        ('Identification', {
            'description' : vpzlock_identification_description,
            'fields': ('text',),
        }),
    )

admin.site.register(VpzLock, VpzLockAdmin)

activitylock_general_description = \
        "ActivityLock is used to limit accesses. \
        ActivityLock makes a link between (i) some users and (ii) some \
        objects with limited access that are allowed to them. \
        ActivityLock is controlled by only 1 User. \
        ActivityLock controls only 1 VpzAct and its parts \
        (VpzInput, VleDuration,...)."

activitylock_identification_description = \
        ""
class ActivityLockingUserInline(admin.TabularInline):
    model = ActivityLock.user_list.through
class ActivityLockingVpzActInline(admin.TabularInline):
    model = ActivityLock.vpzact_list.through
class ActivityLockingVpzOriginInline(admin.TabularInline):
    model = ActivityLock.vpzorigin_list.through
class ActivityLockingVpzInputInline(admin.TabularInline):
    model = ActivityLock.vpzinput_list.through
class ActivityLockingVpzOutputInline(admin.TabularInline):
    model = ActivityLock.vpzoutput_list.through
class ActivityLockingVleBeginInline(admin.TabularInline):
    model = ActivityLock.vlebegin_list.through
class ActivityLockingVleDurationInline(admin.TabularInline):
    model = ActivityLock.vleduration_list.through
class ActivityLockingVleCondInline(admin.TabularInline):
    model = ActivityLock.vlecond_list.through
class ActivityLockingVleParInline(admin.TabularInline):
    model = ActivityLock.vlepar_list.through
class ActivityLockingVleViewInline(admin.TabularInline):
    model = ActivityLock.vleview_list.through
class ActivityLockingVleOutInline(admin.TabularInline):
    model = ActivityLock.vleout_list.through
class ActivityLockingVpzWorkspaceInline(admin.TabularInline):
    model = ActivityLock.vpzworkspace_list.through

class ActivityLockAdmin(admin.ModelAdmin):
    list_display = ('id', 'text', )
    inlines = [ActivityLockingUserInline,
               ActivityLockingVpzActInline, ActivityLockingVpzOriginInline,
               ActivityLockingVpzInputInline, ActivityLockingVpzOutputInline,
               ActivityLockingVleBeginInline, ActivityLockingVleDurationInline,
               ActivityLockingVleCondInline, ActivityLockingVleParInline,
               ActivityLockingVleViewInline, ActivityLockingVleOutInline,
               ActivityLockingVpzWorkspaceInline,]

    exclude = ['user_list',
               'vpzact_list', 'vpzorigin_list',
               'vpzinput_list', 'vpzoutput_list',
               'vlebegin_list', 'vleduration_list',
               'vlecond_list', 'vlepar_list',
               'vleview_list', 'vleout_list',
               'vpzworkspace_list', ]

    fieldsets = (
        (None, {
            'description': activitylock_general_description,
            'fields': (),
        }),
        ('Identification', {
            'description' : activitylock_identification_description,
            'fields': ('text',),
        }),
    )

admin.site.register(ActivityLock, ActivityLockAdmin)


loadlock_general_description = \
        "LoadLock is used to limit accesses. \
        LoadLock makes a link between (i) some users and (ii) some objects \
        with limited access that are allowed to them. \
        LoadLock is controlled by only 1 User. \
        LoadLock controls only 1 object such as DownloadVpzDocument or \
        UploadVpzDocument."

loadlock_identification_description = \
        ""
class LoadLockingUserInline(admin.TabularInline):
    model = LoadLock.user_list.through
class LoadLockingUploadVpzDocumentInline(admin.TabularInline):
    model = LoadLock.uploadvpzdocument_list.through
class LoadLockingDownloadVpzDocumentInline(admin.TabularInline):
    model = LoadLock.downloadvpzdocument_list.through

class LoadLockAdmin(admin.ModelAdmin):
    list_display = ('id', 'text', )
    inlines = [LoadLockingUserInline,
               LoadLockingUploadVpzDocumentInline,
               LoadLockingDownloadVpzDocumentInline,]
    exclude = ['user_list', 
               'uploadvpzdocument_list', 'downloadvpzdocument_list',]

    fieldsets = (
        (None, {
            'description': loadlock_general_description,
            'fields': (),
        }),
        ('Identification', {
            'description' : loadlock_identification_description,
            'fields': ('text',),
        }),
    )

admin.site.register(LoadLock, LoadLockAdmin)


class LockingUserInline(admin.TabularInline):
    model = Lock.user_list.through

class MyUserAdmin(UserAdmin):
    inlines = [LockingUserInline,]
    fieldsets = (
        (None, {
            'description': "Users are the admin ones and users that are allowed ton access some limited models",
            'fields':(),
        }),
    ) + UserAdmin.fieldsets

admin.site.register(User, MyUserAdmin)
