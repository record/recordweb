# -*- coding: utf-8 -*-
"""erecord_acs.router"""

from erecord_cmn.router import DatabaseRouter
from erecord_cmn.configs.config import DB_NAME_ACS


class AcsRouter(DatabaseRouter):
    """
    The application database router.
    """
    db_name = DB_NAME_ACS
    app_name = 'erecord_acs'

