# -*- coding: utf-8 -*-
"""erecord.apps

*This package is part of erecord - Record platform web development*

:copyright: Copyright (C) 2018-2022 INRA http://www.inra.fr.
:license: GPLv3, see :ref:`LICENSE` for more details.
:authors: see :ref:`AUTHORS`.

django applications of the erecord package

The django applications are subdirectories of apps.

The 'apps' path must be included into PYTHONPATH.

Documentation of each 'xxx' django application : see its 'xxx' package.

See also python code in django projects (erecord.projects)
 
"""

