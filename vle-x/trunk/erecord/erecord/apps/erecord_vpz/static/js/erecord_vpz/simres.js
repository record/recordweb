// Simulation results : numeric, graphic, storage

// Init
'option strict';


// methods to manage output data names in graphic menus :
// - ending or not with [a,b] (depending on 'linear' or 'single' plan case)
// - beginning or not with **exp_name** (if previous or current simulation)

// make_name, decompose_name, has_a_b are linked
function make_name(name,a){ // builds name[a]
    return (name+"["+a+"]");
}
function has_a(name){ // tests if name ends with [a]
  return ( name.split('[').length > 1 );
}

function decompose_name(name_a)
{ // name_a looking like name[a], extract name, a
    var name = name_a.split('[')[0];
    var a = name_a.split('[')[1].split(']')[0];
    return {name:name, a:a};
}

// make_name_y_of_x, decompose_name_y_of_x are linked
function make_name_y_of_x(x,y){
    // without ')' at the end : y(x
    var value = y + '(' +x;
    // y(x)
    var text = y + ' ( ' +x + ' ) ';
    return {value:value, text:text};
}
function decompose_name_y_of_x(xy_name){
    // without ')' at the end, xy_name looking like y(x
    var x = xy_name.split('(')[1];
    var y = xy_name.split('(')[0];
    return {x:x, y:y};
}

// s_get_experiment_name_separator, is_from_current_experiment,
// s_make_experiment_name_prefix, s_decompose_name_y_of_x are linked
function s_get_experiment_name_separator()
{ // used in a prefix identifying a previous simulation
    return ("**");
}
function is_from_current_experiment(XY_name)
{ // true if XY_name without any prefix identifying a previous simulation
    return (XY_name.indexOf(s_get_experiment_name_separator())==-1); // not found
}
function s_make_experiment_name_prefix(exp_name)
{ // build and return the prefix identifying the previous simulation exp_name
    return (s_get_experiment_name_separator()+exp_name+s_get_experiment_name_separator());
}
function s_decompose_name_y_of_x(s_xy_name){
    // without ')' at the end, s_xy_name looking like **exp_name**y(x

    var exp_name = s_xy_name.split(s_get_experiment_name_separator())[1];
    var xy_name = s_xy_name.split(s_make_experiment_name_prefix(exp_name))[1];
    var d = decompose_name_y_of_x(xy_name);
    return {exp_name:exp_name, x:d.x, y:d.y};
}

function make_textarea(v)
{
    return ("<textarea readonly rows='1' cols='100'>"+v+"</textarea>");
}
function make_table(matrix)
{ // builds matrix as a table
    var table = "<table border=1>"
    for (var i in matrix){ 
        var line = matrix[i];
        table += "<tr>";
        for (var j in line){ table += "<td>"+line[j]+"</td>"; }
        table += "</tr>";
    } 
    table += "</table>";
    return table;
}
function add_row_to_table(table, c1, c2)
{ // create row containing 2 cells and add it to table
    var row = table.insertRow(0);
    row.insertCell(0).innerHTML = c1;
    row.insertCell(1).innerHTML = c2;
}

function make_numeric(res, plan, restype, element_block)
{ // fill in element element_block with res (as table)
    // depending on plan and restype

      var numeric_table = document.createElement("table");
      numeric_table.setAttribute("border", "1");
      if (restype=='dataframe' && plan=='single'){
        for (var name in res){ 
          add_row_to_table(numeric_table, name, make_textarea(res[name]));
        }
      } else if (restype=='dataframe' && plan=='linear'){
        for (var a in res){
            var res_a = res[a];
            for (var name in res_a){ 
                var name_a = make_name(name,a);
                add_row_to_table(numeric_table, name_a,
                                 make_textarea(res_a[name]));
            }
        }
      } else if (restype=='matrix' && plan=='single'){
        for (var name in res){ 
          add_row_to_table(numeric_table, name, make_table(res[name]));
        }
      } else if (restype=='matrix' && plan=='linear'){
        for (var a in res){
            var res_a = res[a];
            for (var name in res_a){ 
                var name_a = make_name(name,a);
                add_row_to_table(numeric_table, name_a,
                                 make_table(res_a[name]));
            }
        }
      } else {
          add_row_to_table(numeric_table,
                           'res', make_textarea(JSON.stringify(res)));
      }
      element_block.appendChild(numeric_table);
}


/* Vars */

// current simulation
var res; var plan; var restype; var style;

// current simulation and graphic
// output_name_list contains the output data names found in res, maybe
// modified (adding [a]).
var output_name_list;

// previous simulations (stored into localStorage)
var storage_index_name = "erecord_index";
var storage_prefix = "erecord__"; // prefix of the experiment keys
var storage_error_message = "Failed, no web storage support in your web browser";
// storage_index is 'same as' storage_index_name key value in localStorage
var storage_index = null; // default

function graphable_and_storable_current_simulation()
{ // true if current simulation is both taken into account in graphic and
    // able to be stored
    return (style=='compact' && restype=='dataframe');
}

function is_in_storage_mode()
{ // true if web storage is supported and asked/active
    return (window.localStorage && storage!=null);
}

/* initialisation */
window.addEventListener('load', function(){

    // init vars

    res = null;
    var e = document.getElementById("res");
    if (e!=null){ res = JSON.parse(e.value); }
    plan = null;
    var e = document.getElementById("plan");
    if (e!=null){ plan = e.value; }
    restype = null;
    var e = document.getElementById("restype");
    if (e!=null){ restype = e.value; }
    style = null;
    var e = document.getElementById("style");
    if (e!=null){ style = e.value; }
    storage = null;
    var e = document.getElementById("storage");
    if (e!=null){
        if (e.value!=""){ storage = 'storage'; }
    }

    output_name_list = [];
    if (graphable_and_storable_current_simulation()){
    // it is in particular dataframe case
      if (plan=='single'){
          for (var name in res){ 
              output_name_list.push(name);
          }
      } else if (plan=='linear'){
          for (var a in res){
              var res_a = res[a];
              for (var name in res_a){ 
                output_name_list.push(make_name(name,a));
              }
          }
      }
    }

    // init graphic menu relative to current simulation
    var cstext = "";
    if (graphable_and_storable_current_simulation()){
        cstext = "You can choose some simulation results to be seen in the graph.";
    } else {
        cstext = "The "+"<b>"+"current simulation results can not been seen in the graph"+"</b> <i>"+"(it is not a simulation case in 'dataframe' restype and 'compact' style)"+"</i>"+".";
    }
    var pstext = "";
    if (is_in_storage_mode()){
        pstext = "Maybe results of some previous simulations can be seen in the graph (see menus below).";
    }
    var text = cstext+" "+pstext;
    var e = document.getElementById("graphic_introduction");
    if (e!=null){
        var p = document.createElement("p");
        p.innerHTML = text;
        e.appendChild(p);
    }

    if (!graphable_and_storable_current_simulation() && !is_in_storage_mode()){
        var e = document.getElementById("main_graphic_menu");
        if (e!=null){ e.style.display = 'none'; }
    }
    if (graphable_and_storable_current_simulation()){
    // it is in particular dataframe case

        // fill in x_select and y_select options
        var e_x_select = document.getElementById('x_select');
        var e_y_select = document.getElementById('y_select');
        if (e_x_select!=null && e_y_select!=null){
            for (var i in output_name_list){ 
                var name = output_name_list[i];
                add_option(name, e_x_select);
                add_option(name, e_y_select);
            }
        }
    }

    // init storage
    if (is_in_storage_mode()){ stored_experiments(); }

});


/* numeric */

// about current simulation
function numeric()
{ // fill in numeric_block and update its menus (if there)

    var id_block = "numeric_block";
    var id_block_init = id_block + "_init"; // to be removed
    var id_block_after = id_block + "_after"; // to make visible
    var e_block = document.getElementById(id_block);
    var e_block_init = document.getElementById(id_block_init);
    var e_block_after = document.getElementById(id_block_after);
    if (e_block!=null){
        make_numeric(res, plan, restype, e_block);
        if (e_block_init!=null){ e_block_init.remove(); }
        if (e_block_after!=null){ e_block_after.style.display = 'block'; }
        e_block.style.display = 'block';
    }
}

// about previous simulation
function s_numeric_reset()
{
    var e = document.getElementById("s_numeric_block");
    if (e!=null){ clear_element_content(e); }
}
// about previous simulation
function s_numeric()
{ // fill in s_numeric_block and update its menus (if there)

    if (!is_in_storage_mode()){
        alert(storage_error_message); return;
    }

    var id_block = "s_numeric_block";
    var id_block_init = id_block + "_init"; // to be removed
    var id_block_after = id_block + "_after"; // to make visible
    var id_exp_select = "s_numeric_exp_select";
    s_numeric_reset();
    var e_block = document.getElementById(id_block);
    var e_block_init = document.getElementById(id_block_init);
    var e_block_after = document.getElementById(id_block_after);
    var e_exp_select = document.getElementById(id_exp_select);
    if (e_block!=null){

        var exp_name = null; // default
        if (e_exp_select!=null){
            var value = e_exp_select.value;
            if (value!=""){ exp_name = value; }
        }
        if (exp_name!=null && storage_index!=null){
            if (exp_name in storage_index){
                var exp = storage_index[exp_name];
                var s_plan = exp.plan;
                var s_restype = exp.restype;
                var exp_key = make_exp_key(exp_name);
                if (exp_key in localStorage){
                    var s_res = JSON.parse(localStorage.getItem(exp_key));
                    make_numeric(s_res, s_plan, s_restype, e_block);
                    if (e_block_after!=null){
                        e_block_after.style.display = 'block';
                    }
                    e_block.style.display = 'block';
                }
            }
        }
    }
}

/* graphic */

// methods to manage xy_list

// about current simulation
function add_in_xy_list()
{
    var id_x_select = "x_select";
    var id_y_select = "y_select";
    var id_xy_list = "xy_list";

    var e_x = document.getElementById(id_x_select);
    var e_y = document.getElementById(id_y_select);
    var e = document.getElementById(id_xy_list);
    if (e_x!=null && e_y!=null && e!=null){
        var Xname = e_x.value;
        var Yname = e_y.value;
        if (Xname!="" && Yname!=""){
            d = make_name_y_of_x(Xname,Yname);
            option = document.createElement('option');
            option.value = d.value;
            option.text = d.text;
            //option.selected = 'selected';
            e.add( option );
        }
    }
}

// about previous simulation
function s_add_in_xy_list()
{
    var id_exp_select = "s_graphic_exp_select";
    var id_x_select = "s_graphic_x_select";
    var id_y_select = "s_graphic_y_select";
    var id_xy_list = "xy_list";
    var e_exp = document.getElementById(id_exp_select);
    var e_x = document.getElementById(id_x_select);
    var e_y = document.getElementById(id_y_select);
    var e = document.getElementById(id_xy_list);
    if (e_exp!=null && e_x!=null && e_y!=null && e!=null){
        var exp_name = e_exp.value;
        var Xname = e_x.value;
        var Yname = e_y.value;
        if (exp_name!="" && Xname!="" && Yname!=""){
            d = make_name_y_of_x(Xname,Yname);
            option = document.createElement('option');
            option.value = s_make_experiment_name_prefix(exp_name) + d.value;
            option.text = s_make_experiment_name_prefix(exp_name) + d.text;
            //option.selected = 'selected';
            e.add( option );
        }
    }
}

function suppr_in_xy_list()
{
    var e = document.getElementById("xy_list");
    if (e!=null){
        for (var i=0; i<e.options.length; i++) {
            opt = e.options[i];
            if (opt.selected) { opt.remove(); }
        }
    }
}

function reset_xy_list()
{
    var e = document.getElementById("xy_list");
    if (e!=null){ clear_element_content(e); }
}

function graph()
{
    var XY_names = document.getElementById("xy_list");
    if (XY_names!=null){
        var XY = [];
        for (var i = 0; i < XY_names.length; i++){
            var XY_name = XY_names[i].value;

            if ( is_from_current_experiment(XY_name) ){
                d = decompose_name_y_of_x(XY_name);
                var X_name = d.x;
                var Y_name = d.y;
                var X_values; var Y_values;
                if ( has_a(X_name) ){
                    d = decompose_name(X_name);
                    X_values = res[parseInt(d.a)][d.name];
                } else {
                    X_values = res[X_name];
                }
                if ( has_a(Y_name) ){
                    d = decompose_name(Y_name);
                    Y_values = res[parseInt(d.a)][d.name];
                } else {
                    Y_values = res[Y_name];
                }
    
            } else {
    
                d = s_decompose_name_y_of_x(XY_name);
    
                var X_name = d.x;
                var Y_name = d.y;
                var X_values; var Y_values;
                var exp_name = d.exp_name;
                var exp_key = make_exp_key(exp_name);
                if (exp_key in localStorage){
                    s_res = JSON.parse(localStorage.getItem(exp_key));
                }
                if ( has_a(X_name) ){
                    d = decompose_name(X_name);
                    X_values = s_res[parseInt(d.a)][d.name];
                } else {
                    X_values = s_res[X_name];
                }
                if ( has_a(Y_name) ){
                    d = decompose_name(Y_name);
                    Y_values = s_res[parseInt(d.a)][d.name];
                } else {
                    Y_values = s_res[Y_name];
                }
            }
            XY.push({ 'X_name':X_name, 'X_values':X_values,
                      'Y_name':Y_name, 'Y_values':Y_values});
        }
        var size = document.getElementById("graphic_size").value;
        draw_list(size, XY);
      }
}

/* local storage */

// make_exp_key, get_exp_name are linked
function make_exp_key(exp_name)
{ // add storage_prefix
      return (storage_prefix + exp_name);
}
function get_exp_name(exp_key)
{ // remove storage_prefix
      return (exp_key.replace(storage_prefix, ""));
}
function is_part_of_experiment_keys(key)
{ // erecord keys have all the same storage_prefix
      return (key.indexOf(storage_prefix) == 0); 
}

function update_storage()
{ // update global storage_index and localStorage

    if (!is_in_storage_mode()){
        alert(storage_error_message); return;
    }
    storage_index = null;
    if (storage_index_name in localStorage){
        storage_index = JSON.parse(localStorage.getItem(storage_index_name));
        for (var exp_name in storage_index){
            if (!(make_exp_key(exp_name) in localStorage)){
                delete storage_index[exp_name]; // extracted (unavailable)
            }
        }
        // updating in case of change
        localStorage.setItem(storage_index_name, JSON.stringify(storage_index));
    }
}

function stored_experiments()
{ // refresh storage state and the corresponding select elements

    if (!is_in_storage_mode()){
        alert(storage_error_message); return;
    }
    update_storage();
    var e_numeric_exp_select = document.getElementById("s_numeric_exp_select");
    var e_graphic_exp_select = document.getElementById("s_graphic_exp_select");
    var e_graphic_x_select = document.getElementById("s_graphic_x_select");
    var e_graphic_y_select = document.getElementById("s_graphic_y_select");
    var e_remove_exp_select = document.getElementById("s_remove_exp_select");
    var e_stored_experiments = document.getElementById("s_stored_experiments");

    // reset
    var e_list = [e_numeric_exp_select,
                  e_graphic_exp_select, e_graphic_x_select, e_graphic_y_select,
                  e_remove_exp_select, e_stored_experiments];
    for (var i in e_list){
        var e = e_list[i];
        if (e!=null){ clear_element_content(e); }
    }

    // *_exp_select
    if (storage_index!=null && e_numeric_exp_select!=null
        && e_graphic_exp_select!=null && e_remove_exp_select!=null){
        for (var exp_name in storage_index){
            add_option(exp_name, e_numeric_exp_select);
            add_option(exp_name, e_graphic_exp_select);
            add_option(exp_name, e_remove_exp_select);
        }
    }
    s_update_graphic_xy(); // e_graphic_x_select, e_graphic_y_select
    s_numeric(); // s_numeric_reset();

    // e_stored_experiments
    if (storage_index!=null && e_stored_experiments!=null){
        var t = document.createElement("table");
        t.setAttribute("border", "1");
        for (var exp_name in storage_index){
            var exp = storage_index[exp_name];
            var row = t.insertRow(0);
            row.insertCell(0).innerHTML = exp_name;
            row.insertCell(1).innerHTML = exp.desc;
            var output_names = document.createElement("select");
            for (var i in exp.output_names){
                option = document.createElement('option');
                option.text = exp.output_names[i];
                output_names.add(option);
            }
            row.insertCell(2).appendChild(output_names);
            row.insertCell(3).innerHTML = exp.plan;
            row.insertCell(4).innerHTML = exp.restype;
            row.insertCell(5).innerHTML = exp.date;
        }
        e_stored_experiments.appendChild(t);
    }
}

function s_update_graphic_xy()
{ // update x_select, y_select according to exp_select

    if (!is_in_storage_mode()){
        alert(storage_error_message); return;
    }
    var e_exp_select = document.getElementById("s_graphic_exp_select");
    var e_x_select = document.getElementById("s_graphic_x_select");
    var e_y_select = document.getElementById("s_graphic_y_select");

    // reset
    if (e_x_select!=null){ clear_element_content(e_x_select); }
    if (e_y_select!=null){ clear_element_content(e_y_select); }

    var exp_name = null; // default
    if (e_exp_select!=null){
        var value = e_exp_select.value;
        if (value!=""){ exp_name = value; }
    }
    if (exp_name!=null && storage_index!=null){
        if (exp_name in storage_index){
            var exp = storage_index[exp_name];
            for (var i in exp.output_names){ 
                var output_name = exp.output_names[i];
                if (e_x_select!=null){ add_option(output_name, e_x_select); }
                if (e_y_select!=null){ add_option(output_name, e_y_select); }
            }
        }
    }
}

function clear_one_stored_experiment()
{ // remove exp from localStorage and storage_index

    if (!is_in_storage_mode()){
        alert(storage_error_message); return;
    }
    var e_exp_select = document.getElementById("s_remove_exp_select");
    var exp_name = null; // default
    if (e_exp_select!=null){
        var value = e_exp_select.value;
        if (value!=""){ exp_name = value; }
    }
    if (storage_index!=null){
        if (exp_name in storage_index){ delete storage_index[exp_name]; }
        localStorage.setItem(storage_index_name, JSON.stringify(storage_index));
        var exp_key = make_exp_key(exp_name);
        if (exp_key in localStorage){ localStorage.removeItem(exp_key); }
    }
    stored_experiments(); // update
}

function clear_stored_experiments()
{ // clear localStorage and storage_index

    if (!is_in_storage_mode()){
        alert(storage_error_message); return;
    }
    for (var i=0; i<localStorage.length; i++){
        var key = localStorage.key(i);
        if (is_part_of_experiment_keys(key)){
            localStorage.removeItem(key);
        }
    }
    if (storage_index_name in localStorage){
        localStorage.removeItem(storage_index_name);
    }
    stored_experiments(); // update
}

function get_current_experiment_name(exp_name)
{ // get and return experiment_name after modification if needed
    // value "" if not ok

    var e = document.getElementById('experiment_name');
    if (e.checkValidity() == false){ 
        return "";
    } else {
      var experiment_name = e.value;
      experiment_name = experiment_name.replace(/ /g, "_"); // one word
      // ...+?
      return experiment_name;
    }
}
function save_current_experiment()
{ // save current experiment in localStorage and global storage_index

    if (!is_in_storage_mode()){
        alert(storage_error_message); return;
    }
    if (graphable_and_storable_current_simulation()){
        var json_res = null; // default
        var e = document.getElementById("res");
        if (e!=null){ json_res = e.value; }
        var exp_name = get_current_experiment_name();
        if ( exp_name == "" || json_res==null){
          alert('impossible to save');
        } else {
          var exp_desc = ""; // default
          var e = document.getElementById("experiment_desc");
          if (e!=null){ exp_desc = e.value; }
          var date = Date();
          var current_exp = {desc:exp_desc, output_names:output_name_list,
                             plan:plan, restype:restype, date:date};
          var si; // storage index
          if (storage_index_name in localStorage){
              si = JSON.parse(localStorage.getItem(storage_index_name));
              while ( exp_name in si){ // to not overwrite
                  exp_name = exp_name + "_"
              }
              localStorage.removeItem(storage_index_name);
          } else {
              si = {};
          }
          si[exp_name] = current_exp;
          localStorage.setItem(storage_index_name, JSON.stringify(si));
          var exp_key = make_exp_key(exp_name);
          if (exp_key in localStorage){ localStorage.removeItem(exp_key); }
          localStorage.setItem(exp_key, json_res);
          alert('the simulation has been saved as '+exp_name);
          stored_experiments(); // update
        }
    }
}
