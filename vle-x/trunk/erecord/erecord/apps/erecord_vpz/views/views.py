# -*- coding: UTF-8 -*-
"""erecord_vpz.views.views """

from rest_framework.response import Response
from rest_framework import generics

from erecord_vpz.models import VpzAct
from erecord_vpz.models import VpzOrigin
from erecord_vpz.models import VpzInput
from erecord_vpz.models import VpzOutput
from erecord_vpz.models import VpzPath
from erecord_vpz.models import VleBegin
from erecord_vpz.models import VleDuration
from erecord_vpz.models import VleCond
from erecord_vpz.models import VlePar
from erecord_vpz.models import VleView
from erecord_vpz.models import VleOut
from erecord_vpz.models import VpzWorkspace 

from erecord_cmn.views_mixins import FormatViewMixin
from erecord_cmn.views_mixins import DataViewMixin
from erecord_cmn.views_mixins import DetailViewMixin
from erecord_acs.views_mixins import AccessViewMixin

from erecord_cmn.utils.logger import get_logger
LOGGER = get_logger(__name__)

headsubtitle = "(erecord_vpz)"

class VpzPathAccessView(AccessViewMixin, FormatViewMixin, DataViewMixin,
                       DetailViewMixin, generics.RetrieveAPIView):
    """Access of one vpz file path

    For more information, see the online documentation.
    """

    def get(self, request, format=None, **kwargs):
        obj = VpzPath.objects.get(pk=kwargs['pk'])
        title = 'Access of simulator path VpzPath (Id ' + str(obj.id) + ')'
        return self.access_response(request=request, format=format,
                                    obj=obj, title=title)

class VpzActAccessView(AccessViewMixin, FormatViewMixin, DataViewMixin,
                       DetailViewMixin, generics.RetrieveAPIView):

    def get(self, request, format=None, **kwargs):
        obj = VpzAct.objects.get(pk=kwargs['pk'])
        title = 'Access of activity VpzAct (Id ' + str(obj.id) + ')'
        return self.access_response(request=request, format=format,
                                    obj=obj, title=title)

class VpzOriginAccessView(AccessViewMixin, FormatViewMixin, DataViewMixin,
                       DetailViewMixin, generics.RetrieveAPIView):

    def get(self, request, format=None, **kwargs):
        obj = VpzOrigin.objects.get(pk=kwargs['pk'])
        title = 'Access of VpzOrigin (Id ' + str(obj.id) + ')'
        return self.access_response(request=request, format=format,
                                    obj=obj, title=title)

class VpzInputAccessView(AccessViewMixin, FormatViewMixin, DataViewMixin,
                       DetailViewMixin, generics.RetrieveAPIView):

    def get(self, request, format=None, **kwargs):
        obj = VpzInput.objects.get(pk=kwargs['pk'])
        title = 'Access of VpzInput (Id ' + str(obj.id) + ')'
        return self.access_response(request=request, format=format,
                                    obj=obj, title=title)

class VpzOutputAccessView(AccessViewMixin, FormatViewMixin, DataViewMixin,
                       DetailViewMixin, generics.RetrieveAPIView):

    def get(self, request, format=None, **kwargs):
        obj = VpzOutput.objects.get(pk=kwargs['pk'])
        title = 'Access of VpzOutput (Id ' + str(obj.id) + ')'
        return self.access_response(request=request, format=format,
                                    obj=obj, title=title)

class VleBeginAccessView(AccessViewMixin, FormatViewMixin, DataViewMixin,
                       DetailViewMixin, generics.RetrieveAPIView):

    def get(self, request, format=None, **kwargs):
        obj = VleBegin.objects.get(pk=kwargs['pk'])
        title = 'Access of begin VleBegin (Id ' + str(obj.id) + ')'
        return self.access_response(request=request, format=format,
                                    obj=obj, title=title)

class VleDurationAccessView(AccessViewMixin, FormatViewMixin, DataViewMixin,
                       DetailViewMixin, generics.RetrieveAPIView):

    def get(self, request, format=None, **kwargs):
        obj = VleDuration.objects.get(pk=kwargs['pk'])
        title = 'Access of duration VleDuration (Id ' + str(obj.id) + ')'
        return self.access_response(request=request, format=format,
                                    obj=obj, title=title)

class VleCondAccessView(AccessViewMixin, FormatViewMixin, DataViewMixin,
                       DetailViewMixin, generics.RetrieveAPIView):

    def get(self, request, format=None, **kwargs):
        obj = VleCond.objects.get(pk=kwargs['pk'])
        title = 'Access of condition VleCond (Id ' + str(obj.id) + ')'
        return self.access_response(request=request, format=format,
                                    obj=obj, title=title)

class VleParAccessView(AccessViewMixin, FormatViewMixin, DataViewMixin,
                       DetailViewMixin, generics.RetrieveAPIView):

    def get(self, request, format=None, **kwargs):
        obj = VlePar.objects.get(pk=kwargs['pk'])
        title = 'Access of parameter VlePar (Id ' + str(obj.id) + ')'
        return self.access_response(request=request, format=format,
                                    obj=obj, title=title)

class VleViewAccessView(AccessViewMixin, FormatViewMixin, DataViewMixin,
                       DetailViewMixin, generics.RetrieveAPIView):

    def get(self, request, format=None, **kwargs):
        obj = VleView.objects.get(pk=kwargs['pk'])
        title = 'Access of view VleView (Id ' + str(obj.id) + ')'
        return self.access_response(request=request, format=format,
                                    obj=obj, title=title)

class VleOutAccessView(AccessViewMixin, FormatViewMixin, DataViewMixin,
                       DetailViewMixin, generics.RetrieveAPIView):

    def get(self, request, format=None, **kwargs):
        obj = VleOut.objects.get(pk=kwargs['pk'])
        title = 'Access of VleOut (Id ' + str(obj.id) + ')'
        return self.access_response(request=request, format=format,
                                    obj=obj, title=title)

class VpzWorkspaceAccessView(AccessViewMixin, FormatViewMixin, DataViewMixin,
                       DetailViewMixin, generics.RetrieveAPIView):

    def get(self, request, format=None, **kwargs):
        obj = VpzWorkspace.objects.get(pk=kwargs['pk'])
        title = 'Access of VpzWorkspace (Id ' + str(obj.id) + ')'
        return self.access_response(request=request, format=format,
                                    obj=obj, title=title)

