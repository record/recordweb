# -*- coding: utf-8 -*-
"""erecord_cmn

*This package is part of erecord - Record platform web development*

:copyright: Copyright (C) 2014-2015 INRA http://www.inra.fr.
:license: GPLv3, see :ref:`LICENSE` for more details.
:authors: see :ref:`AUTHORS`.

Common application
 
The erecord_cmn common application contains resources for the other
applications.

See :ref:`index`

"""
