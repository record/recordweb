# -*- coding: utf-8 -*-
"""erecord_cmn.router

Database router, for multiple databases case

"""

class DatabaseRouter(object):
    """ A router to control all database operations on models in an application.

    DatabaseRouter is an abstract class, to be heritated by an application in
    multiple databases case.
    The child class defines db_name and app_name.

    """

    def db_for_read(self, model, **hints):
        """
        Attempts to read app_name models go to db_name.
        """
        if model._meta.app_label == self.app_name:
            return self.db_name
        return None # no suggestion

    def db_for_write(self, model, **hints):
        """
        Attempts to write app_name models go to db_name.
        """
        if model._meta.app_label == self.app_name:
            return self.db_name
        return None # no suggestion

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the app_name app is involved.
        """
        if obj1._meta.app_label == self.app_name or \
           obj2._meta.app_label == self.app_name:
           return True
        return None

    def allow_syncdb(self, db, model):
        """
        Make sure the app_name app only appears in the db_name database.
        """
        if db == self.db_name:
            return model._meta.app_label == self.app_name
        elif model._meta.app_label == self.app_name:
            return False
        return None

    def allow_migrate(self, db, model):
        """
        Make sure the app_name app only appears in the db_name
        database.
        """
        if db == self.db_name:
            return model._meta.app_label == self.app_name
        elif model._meta.app_label == self.app_name:
            return False
        return None

