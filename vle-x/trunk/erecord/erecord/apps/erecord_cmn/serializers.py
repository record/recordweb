# -*- coding: utf-8 -*-
"""erecord_cmn.serializers

Serializers common to erecord applications

"""

from rest_framework import serializers

# Some serializer common to erecord applications

def getKeyOptionValues(data, key):
    """returns list of values of key option"""

    from django.http.request import QueryDict
    if isinstance(data, QueryDict) :
        #d = dict(data._iterlists())
        #s = d.get(key, [])
        s = data.getlist(key)
    elif isinstance(data, dict) :
        s = data.get(key, [])
        if not isinstance(s, list) :
            s = [ s ]
    return s

class FormatOptionSerializer(serializers.Serializer):
    """Option 'format' """
    format = serializers.CharField( required=False )
    def validate(self, attrs):
        if 'format' not in attrs.keys() :
            attrs['format']=None
        return attrs

class JwtOptionSerializer(serializers.Serializer):
    """Option 'jwt' for JWT (JSON Web Token) """
    jwt = serializers.CharField( required=False )
    def validate(self, attrs):
        if 'jwt' not in attrs.keys() :
            attrs['jwt']=None
        return attrs

