# -*- coding: utf-8 -*-
"""erecord_cmn.utils.using.content_simulation_results_compact

Methods that may be used by a user calling the erecord web services from python

"""

from __future__ import print_function # python 2.7 version case

def content_simulation_results_compact(res, plan, restype):
    """Content of simulation results (res) in 'compact' style of presentation,
       according to plan values ('single', 'linear') and
       restype values ('dataframe' or 'matrix')
    """
#
    res = json.loads(res)
    print("plan, restype :", plan, restype)
    print("res :", res)
    print("\nDetailing the results :")
    if restype=='dataframe' and plan=='single' :
        print("(Output data name (ie selection_name) and value)")
        for compact_outputname,val in res.items() :
            print("- ", compact_outputname, val)
    elif restype=='dataframe' and plan=='linear' :
        for (a,res_a) in enumerate(res) :
            print("*** simulation number ", a, ":")
            print("(Output data name (ie selection_name) and value)")
            for compact_outputname,val in res_a.items() :
                print("- ", compact_outputname, val)
    elif restype=='matrix' and plan=='single' :
        print("(View name (ie selection_name) and value)")
        for viewname,v in res.items() :
            print("- ", viewname, v)
    elif restype=='matrix' and plan=='linear' :
        for (a,res_a) in enumerate(res) :
            print("*** simulation number ", a, ":")
            print("(View name (ie selection_name) and value)")
            for viewname,v in res_a.items() :
                print("- ", viewname, v)
    else : # error (unexpected)
        pass
#

