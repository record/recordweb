# -*- coding: utf-8 -*-
"""erecord_cmn.utils.using.send_post_and_receive

Methods that may be used by a user calling the erecord web services from python

"""

import pycurl
import io
import json

def send_post_and_receive(url, inputdata):
    """Send POST request and return response datas"""
#
    buffer = io.BytesIO()
    c = pycurl.Curl()
    c.setopt(c.POST, 1)
    c.setopt(c.HTTPHEADER, ['Content-Type: application/json'])
    c.setopt(c.URL, url)
    #c.setopt(c.FOLLOWLOCATION, True)
    json_inputdata = json.dumps( inputdata)
    c.setopt(c.POSTFIELDS, json_inputdata)
    c.setopt(c.WRITEFUNCTION, buffer.write)
    c.perform()
    buffer_str = buffer.getvalue()
    buffer.close()
    buffer_str = buffer_str.decode("utf8")
    responsedata = json.loads(buffer_str)
    return responsedata
#

