# -*- process: utf-8 -*-
"""erecord_cmn.utils.process

Subprocess methods

"""

import os
import subprocess

def call_subprocess(cmd, runvle_path, stderr_path, stdout_path) :

    tmp_cmd_file = os.path.join(runvle_path, "cmd")
    f = open(tmp_cmd_file, 'w')
    f.write(cmd)
    f.close()
    #envir = os.environ
    stderr = open(stderr_path,'w')
    stdout = open(stdout_path,'w')
    retcode = subprocess.call(cmd, shell=True, stderr=stderr, stdout=stdout)
    #retcode = subprocess.call(cmd, shell=True, stderr=stderr, stdout=stdout, env=envir)
    stderr.close()
    stdout.close()
    return retcode

