# -*- coding: UTF-8 -*-
"""erecord_cmn.utils.urls

Methods about urls

"""

import urllib
def url_add_options(path, options):
    return path + '?' + urllib.urlencode(options, doseq=True)

