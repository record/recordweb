===========================================================
erecord, record platform web development for vle-1.1 models
===========================================================

-------------------------------------------------------------------------------
Several versions are saved for some files, depending on os, server...

Those files are to be copied under erecord/erecord/projects/ws/ws :

    - erecord_ws
    - wsgi.py

-------------------------------------------------------------------------------
erecord_ws configuration file :

    - debian/apache2.2/erecord_ws
    - debian/apache2.4/erecord_ws
    - centos/erecord_ws

-------------------------------------------------------------------------------
wsgi.py file :

    - debian/wsgi.py
    - centos/wsgi.py

-------------------------------------------------------------------------------

Note : in this vle-x development of erecord, erecord is intended to accept models of several vle versions (vle-1.1, vle-2...).
