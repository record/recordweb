# -*- coding: utf-8 -*-
"""erecord.projects.ws

*This package is part of erecord - Record platform web development*

:copyright: Copyright (C) 2018-2022 INRA http://www.inra.fr.
:license: GPLv3, see :ref:`LICENSE` for more details.
:authors: see :ref:`AUTHORS`.

Django project container of web services
 
See :ref:`index`

"""
