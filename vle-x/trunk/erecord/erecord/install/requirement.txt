Django==1.11
Sphinx
djangorestframework==3.6.4
pyyaml
pycurl
djangorestframework-xml
djangorestframework-jsonp
djangorestframework-yaml
djangorestframework-jwt
xlwt
xlrd
