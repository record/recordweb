*******************************************************************************

              Install vle and models repositories (vle packages)

*******************************************************************************

Notes
=====

Some rules about models repositories :

    see /opt/erecord/erecord/install/install_vle_repositories_notes.txt


*******************************************************************************
                                   Install vle
*******************************************************************************

Case of vle-1.1.3 version
=========================

      see install_vle_1.1.3.txt


Case of vle-2.0.0 version
=========================

      see install_vle_2.0.0.txt


*******************************************************************************
                     Install models repositories (vle packages)
*******************************************************************************


Case of vle-1.1.3 version
=========================

      see install_vle_1.1.3_repositories.txt

Case of vle-2.0.0 version
=========================

      see install_vle_2.0.0_repositories.txt

*******************************************************************************

