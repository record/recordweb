*******************************************************************************

         Run vle simulation (vpzname.vpz simulator of pkgname vle package)

*******************************************************************************

"RENAME" erecord path as /opt/erecord, for example :
ln -s /home/nrousse/workspace_git/DEVELOPPEMENT_WEB/recordweb/vle-x/trunk/erecord /opt/erecord


Case of vle-1.1.3 version
=========================

  source /opt/erecord/erecord/install/vle_1.1.3_precmd.sh ; vle --version

  export VLE_HOME=/opt/erecord/repositories/vle-1.1.3/pkgname

  vle -P pkgname vpzname.vpz


Case of vle-2.0.0 version
=========================

  source /opt/erecord/erecord/install/vle_2.0.0_precmd.sh ; vle --version

  export VLE_HOME=/opt/erecord/repositories/vle-2.0.0/pkgname

  vle -P pkgname vpzname.vpz

*******************************************************************************

