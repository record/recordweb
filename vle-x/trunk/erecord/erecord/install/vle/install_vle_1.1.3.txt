*******************************************************************************
                              Install vle

                       Case of vle-1.1.3 version

  Case of Ubuntu 16.04
  Case of Debian 9.5 (Stretch)

*******************************************************************************

"Rename" erecord path as /opt/erecord, if erecord is installed somewhere else
(ln -s 'erecord path' /opt/erecord).


INSTALL vle-1.1.3 and its dependencies
======================================

* Dependencies :

  - Ubuntu 16.04 case (to be updated) :
    sudo apt-get update
    sudo apt-get install curl libxml2-dev libboost-dev cmake pkg-config g++

  - Debian 9.5 (Stretch) case :

    sudo apt-get update
    sudo apt-get install curl libxml2-dev libboost-dev cmake pkg-config g++
    sudo apt-get install libarchive-dev libglibmm-2.4-dev libxml2
    sudo apt-get install libcairomm-1.0-dev
    sudo apt-get install libboost-date-time-dev libboost-filesystem-dev
    sudo apt-get install libboost-test-dev libboost-regex-dev
    sudo apt-get install libboost-program-options-dev libboost-thread-dev

    sudo apt-get install libboost-mpi-dev libboost-serialization-dev 
    sudo apt-get install libgtkmm-2.4-dev

* VLE : 

  - Get the source :

    mkdir /opt/erecord/factory/install/vle-1.1.3
    cd /opt/erecord/factory/install/vle-1.1.3
    git clone https://github.com/vle-forge/vle.git
    cd vle
    ##git checkout -b v1.1.3 v1.1.3
    git branch -av
    git checkout master1.1


  - Build the code :

    cd /opt/erecord/factory/install/vle-1.1.3/vle
    mkdir build && cd build
    mkdir /opt/erecord/factory/install/usr_1.1.3

    cmake -DWITH_GTKSOURCEVIEW=OFF -DWITH_GTK=ON -DWITH_CAIRO=ON \
      -DWITH_MPI=OFF \
      -DBUILD_SHARED_LIBS=ON -DCMAKE_BUILD_TYPE=RelWithDebInfo \
      -DCMAKE_INSTALL_PREFIX=/opt/erecord/factory/install/usr_1.1.3 ..
    make
    make install

* Control

    source /opt/erecord/erecord/install/vle_1.1.3_precmd.sh ; vle --version

*******************************************************************************

