
-------------------------------------------------------------------------------
                     erecord vle package
-------------------------------------------------------------------------------

The erecord vle package MUST BE ADDED to each installed models repository.


-------------------------------------------------------------------------------
                     erecord_conf directory
-------------------------------------------------------------------------------

The erecord_conf folder must be copied into the directory of a models repository, for its simulators (vpz file) TO BE ABLE TO BE DECLARED AS VpzPath.

The erecord_conf/VLE_VERSION file (containing the vle version name) and erecord_conf/VLE_USR_PATH file (containing the vle install path name) must be adapted.

-------------------------------------------------------------------------------

