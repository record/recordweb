# -*- coding: utf-8 -*-
"""factory

*This package is part of erecord - Record platform web development*

:copyright: Copyright (C) 2018-2022 INRA http://www.inra.fr.
:license: GPLv3, see LICENSE for more details.
:authors: see AUTHORS.

Factory of the erecord project

The factory directory is the workspace to manage the information produced 
for and during processing.

Content hierarchy :
See the packages of the factory package : erecord, install, static, log, run, media...

In particular 'erecordenv' under 'install' subdirectory : the python virtualenv

See :ref:`index`

"""
