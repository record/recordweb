# -*- coding: utf-8 -*-
"""factory.closedmedia

closed media files

Contains closed media files (controlled access) of erecord package.

For the moment, closedmedia is used for downloadable (and later uploaded ?)
files.

See :ref:`index`

"""

