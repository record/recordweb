# -*- coding: utf-8 -*-
"""factory.log

log files

Contains the log files written during processing

Content :

  - erecord.log file, produced by erecord
  - access.log, built from apache log files (daily updated by cron) : see
    ../../erecord/install/prod/install_log.txt.
  - tmp folder

See :ref:`index`

"""
