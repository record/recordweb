.. _domain_dm_erecord_vpz:

=====================================
Data model of erecord_vpz application
=====================================

Data model of the erecord_vpz application

Schema
======

    - :ref:`dm_vpzpath`
    - :ref:`dm_vpzact`
        - one :ref:`dm_vpzorigin`
        - one :ref:`dm_vpzinput`
            - one :ref:`dm_vlebegin`
            - one :ref:`dm_vleduration`
            - many :ref:`dm_vlecond`
                - many :ref:`dm_vlepar`
            - many :ref:`dm_vleview`
                - many :ref:`dm_vleout`
        - one :ref:`dm_vpzoutput`
        - one :ref:`dm_vpzworkspace`

:ref:`dm_vpzact` 'comes' (is built) from a :ref:`dm_vpzpath` or a :ref:`dm_vlevpz`

Data model
==========

.. _dm_vlebegin:

VleBegin
--------
Vle begin

.. _dm_vlecond:

VleCond
-------
Vle condition

.. _dm_vleduration:

VleDuration
-----------
Vle duration

.. _dm_vleout:

VleOut
------
Vle output data

Vle output data (of a VleView)

.. _dm_vlepar:

VlePar
------
Vle parameter 

Vle parameter (of a VleCond).

It contains some information about the parameter (name, value...), that is got or not into a request answer (it depends on the options given into the request).

A value type can value : "boolean", "integer", "double", "string", "xml", "set", "map", "tuple", "table", "matrix, "none", "".

.. _dm_vleview:

VleView
-------
Vle view

.. _dm_vpzact:

VpzAct
------
Vpz activity

It is defined from a VleVpz or a VpzPath


.. _dm_vpzinput:

VpzInput
--------
Vpz input

Other denominations : input information of a vpz ; vpz input information,
input conditions of a simulator.

The vpz input information corresponds with the configuration (experiment) defined into the vpz file (information such as parameters, duration, begin...).

.. _dm_vpzorigin:

VpzOrigin
---------
Vpz origin

.. _dm_vpzoutput:

VpzOutput
---------
Vpz output

Other denominations : output information of a vpz ; vpz output information,
simulation results of a simulator.

The vpz output information corresponds with the simulation results.

.. _dm_vpzpath:

VpzPath
-------
Vpz path

A VpzPath corresponds with a vpz file absolute path that gives : its location into a vle package into a models repository (providing the required environment for running simulations) and its vpz name.

.. _dm_vpzworkspace:

VpzWorkspace
------------
Vpz workspace

A VpzWorkspace is attached to a VpzAct. It defines a workspace directory dedicated to the Vpz activity (manipulation).

