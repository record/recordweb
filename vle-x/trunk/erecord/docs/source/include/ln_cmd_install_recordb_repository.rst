
.. _cmd_install_recordb_repository:

==============================
Install the recordb repository
==============================

.. literalinclude:: ../devel/erecord/repositories/include/cmd_install_recordb_repository.rst

