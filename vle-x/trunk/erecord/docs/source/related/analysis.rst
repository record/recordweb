.. _model_analysis:

==============
Model Analysis
==============

Introduction
============

The erecord web services can be used for models analysis, that can be 
managed for example from a R program to which the :ref:`post_vpz_output`
resource can provide some simulations.

Example of Sensitivity Analysis in R language
=============================================

For the simulator ‘wwdm.vpz’ whose Id is :ref:`wwdm_vpz_id`
(:ref:`more <wwdm_vpz_id>`) :

- :download:`download the R file <analysis/wwdm.R>` and call it,

- or enter in a R interpreter the following R code/instructions :

    .. literalinclude:: analysis/wwdm.R

    .. literalinclude:: analysis/wwdm_run.R

