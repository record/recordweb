.. _webrecord_project:

=================
Webrecord project
=================

The **webrecord** project (2011-2014) is a previous work about web solutions
for models of the :term:`Record` platform. Productions are :

    * A web application prototype that enables remote simulations of some
      existing agronomic models, that have been previously loaded into the
      web tool.

    * Some helping information and source code produced in order to facilitate
      the work of modelisation teams who would intend to develop a web
      application dedicated to their own agronomic model.

The website, that was dedicated to the webrecord project presentation and
productions, is no longer online. See an `archive of the webrecord site <http://147.100.179.168/misc/webrecord/webrecord.pdf>`_ *(fr) (archive du site internet webrecord)*.

