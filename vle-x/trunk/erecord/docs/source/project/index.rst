.. _erecord_software_project:

========================
erecord software project
========================

Intellectual property
---------------------

   See : :ref:`erecord_ip`
   (:ref:`AUTHORS` | :ref:`LICENSE` | :ref:`citing_erecord`)

Source code repository
----------------------

    The erecord source code repository is hosted on the **recordweb**
    project of **forgemia** (*forge of MIA Department of INRA Institute*)
    https://forgemia.inra.fr/record/recordweb.

.. _erecord_contacts:

Contacts
--------

    You may find answers to your own questions in :ref:`faqs`.

    You can send mail to Nathalie Rousse (INRA, MIAT, RECORD Team Member)
    : nathalie.rousse@inra.fr (*help, notice a bug, ask for new functions...*).

More
====

.. include:: ../include/more.rst

