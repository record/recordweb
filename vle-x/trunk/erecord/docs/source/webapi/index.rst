.. _webapi:

=======
Web API
=======

Introduction
============

The web services provided by :term:`erecord` allow to edit, modify and simulate
some :term:`agronomic model` s of the :term:`Record` platform, and more
generally some :term:`vle model` s.

The **'vpz'** web services allow to **modify and run simulators**.

To act on a simulator, it is necessary to know its Id. This one can be found
thanks to the **'db'** web services, that allow to **identify the models
repositories, their models and simulators**.

Some models are in **public access** and some models are in **limited access**. **Authentication** is required in limited access case (see :ref:`erecord_authentication`).

Resources
=========

- The :ref:`'vpz' web services <webapi_vpz>` to **modify and run simulators** :

========================= ========================================= ===
Resource                  Description 
========================= ========================================= ===
:ref:`get_vpz_input`      to see a simulator
------------------------- ----------------------------------------- ---
:ref:`post_vpz_input`     to see a modified simulator               :ref:`Try <online_url_input_menu>`
------------------------- ----------------------------------------- ---
:ref:`get_vpz_output`     to run a simulator
------------------------- ----------------------------------------- ---
:ref:`post_vpz_output`    to run a modified simulator               :ref:`Try <online_url_output_menu>`
------------------------- ----------------------------------------- ---
:ref:`get_vpz_inout`      to see and run a simulator
------------------------- ----------------------------------------- ---
:ref:`post_vpz_inout`     to see and run a modified simulator       :ref:`Try <online_url_inout_menu>`
========================= ========================================= ===

_

=========================== ======================================== ====
Resource                    Description 
=========================== ======================================== ====
:ref:`get_vpz_experiment`   to see a simulator by xls file           :ref:`Try <online_url_inoutmodify_menu>` ('first step')
--------------------------- ---------------------------------------- ----
:ref:`post_vpz_experiment`  to run a modified simulator by xls file  :ref:`Try <online_url_inoutmodify_menu>`
=========================== ======================================== ====

_

================================= =============================================
Resource                          Description 
================================= =============================================
:ref:`get_vpz_report`             for reports about a simulator
                                  (conditions and results)
--------------------------------- ---------------------------------------------
:ref:`post_vpz_report`            for reports about a modified simulator 
                                  (conditions and results)
--------------------------------- ---------------------------------------------
:ref:`get_vpz_report_conditions`  for reports about a simulator conditions
--------------------------------- ---------------------------------------------
:ref:`post_vpz_report_conditions` for reports about a modified simulator conditions
================================= =============================================

- The :ref:`'db' web services <webapi_db>` to identify the **models
  repositories, their models and simulators** :

========================= ==================================================
Resource                  Description 
========================= ==================================================
:ref:`get_db_rep`         to identify some models repositories
------------------------- --------------------------------------------------
:ref:`get_db_rep_id`      to identify a models repository
------------------------- --------------------------------------------------
:ref:`get_db_pkg`         to identify some models
------------------------- --------------------------------------------------
:ref:`get_db_pkg_id`      to identify a model
------------------------- --------------------------------------------------
:ref:`get_db_vpz`         to identify some simulators
------------------------- --------------------------------------------------
:ref:`get_db_vpz_id`      to identify a simulator
========================= ==================================================

_

=============================== ============================================
Resource                        Description 
=============================== ============================================
:ref:`get_db_rep_id_name`       to get the name of a models repository
------------------------------- --------------------------------------------
:ref:`get_db_pkg_id_name`       to get the name of a model
------------------------------- --------------------------------------------
:ref:`get_db_pkg_id_datalist`   to get the list of names of data files of a model
------------------------------- --------------------------------------------
:ref:`get_db_vpz_id_name`       to get the name of a simulator
=============================== ============================================


- The :ref:`'slm' web services <webapi_slm>` to **download some result
  files** :

================================= ==========================================
Resource                          Description 
================================= ==========================================
:ref:`get_slm_download`           to download a result file
================================= ==========================================

- The :ref:`'acs' web services <webapi_acs>` to manage simulators **access (public or limited)** :

**Authentication** by **JWT (JSON Web Token)** for limited access case :

=========================================== ================================
Resource                                    Description 
=========================================== ================================
:ref:`post_acs_jwt_obtain`                  to get a JWT value
------------------------------------------- --------------------------------
:ref:`post_acs_jwt_verify`                  to verify a JWT value
=========================================== ================================

_

===================================== ======================================
Resource                              Description 
===================================== ======================================
:ref:`get_acs_vpz_id_access`          to get the access type (limited or public) of a simulator (vpz) 
------------------------------------- --------------------------------------
:ref:`get_acs_vpz_id_user_id`         to get the list of id of a simulator (vpz) authorized users
------------------------------------- --------------------------------------
:ref:`get_acs_vpz_id_user_name`       to get the list of name of a simulator (vpz) authorized users
------------------------------------- --------------------------------------
:ref:`get_acs_vpz_accessible_id`      to get the list of id of the accessible simulators (vpz)
===================================== ======================================

_

========================================== =================================
Resource                                   Description 
========================================== =================================
:ref:`get_acs_vpzpath_id_access`           to get the access type (limited or public) of a simulator (vpzpath)
------------------------------------------ ---------------------------------
:ref:`get_acs_vpzpath_id_user_id`          to get the list of id of a simulator (vpzpath) authorized users
------------------------------------------ ---------------------------------
:ref:`get_acs_vpzpath_id_user_name`        to get the list of name of a simulator (vpzpath) authorized users
------------------------------------------ ---------------------------------
:ref:`get_acs_vpzpath_accessible_id`       to get the list of id of the accessible simulators (vpzpath)
========================================== =================================


More
====

.. include:: ../include/more.rst

