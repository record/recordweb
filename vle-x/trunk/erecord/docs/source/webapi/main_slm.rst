.. _webapi_slm:

=============
Web API 'slm'
=============

Overview
========
.. include:: ../devel/design/webapi/include/slm_intro.rst

Resources
=========

.. include:: ../devel/design/webapi/slm_resources.rst

More
====

.. include:: ../include/more.rst

