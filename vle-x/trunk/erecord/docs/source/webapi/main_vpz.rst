.. _webapi_vpz:

=============
Web API 'vpz'
=============

Overview
========
.. include:: ../devel/design/webapi/include/vpz_intro.rst

Resources
=========

.. include:: ../devel/design/webapi/vpz_resources.rst

More
====

.. include:: ../include/more.rst

