.. _faqs_developers:

===============
Developers FAQs
===============

Where can I find help as developer ?
------------------------------------

    - :ref:`How to install/deploy the erecord software <devel_deployment>`
    - :ref:`How to install/declare a model <repositories>`
    - :ref:`How to build documentation <devel_documentation>`
    - :ref:`Testing the erecord software <devel_test>`

