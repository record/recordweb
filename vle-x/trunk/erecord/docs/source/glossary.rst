.. _glossary:

========
Glossary
========

.. glossary::
   :sorted:

   erecord project
       The erecord project is dedicated to the :term:`Record` platform web
       development for :term:`vle model` s of several vle versions (vle-1.1,
       vle-2...). See :ref:`erecord_project`.

   erecord package
       The erecord package is part of the erecord project. It is a python
       package, that contains the software production (source code, tests...).
       See :ref:`erecord_package`.

   erecord
       See :term:`erecord project`. See :term:`erecord package`

   erecord_db
       erecord_db is a :term:`django application` of the
       :term:`erecord project`. See :ref:`apiref_index`.

   erecord_vpz
       erecord_vpz is a :term:`django application` of the
       :term:`erecord project`. See :ref:`apiref_index`.

   erecord_cmn
       erecord_cmn is a :term:`django application` of the
       :term:`erecord project`. See :ref:`apiref_index`.
      
   erecord_slm
       erecord_slm is a :term:`django application` of the
       :term:`erecord project`. See :ref:`apiref_index`.

   erecord_acs
       erecord_acs is a :term:`django application` of the
       :term:`erecord project`. See :ref:`apiref_index`.

   package (vle notion)
       See :term:`vle package`.

   project (vle notion)
       See :term:`vle project`.

   model
       See :term:`vle model`

   agronomic model
       See :term:`vle model`.

   vle package
       A vle package corresponds with the vle notion of package.

       *Vle details : A package in Vle is an autonomous project which
       regroups source code of models, their vpz files, documentation and
       datas. The goal of the packages in Vle is to propose an easily aspect
       to develop big models and share models between modelers.*

       Developing a vle package is the way how an agronomic model is developed
       in :term:`Vle`. A vle package containing an agronomic model can depend
       on some other vle packages containing other agronomic models to which
       the model in question would be coupled. A vle package generally also
       depends on vle packages of another kind, directly delivered by Vle,
       that provide the used modelling formalisms (difference equation,
       statechart...). We will talk about the 'main' package (containing the
       vpz file of the simulation) and its dependency packages. 

       See also :term:`vle model`.

   vle project
       In :term:`Vle`, a vle project is also called a :term:`vle package`.

   vle model
       A vle model is a model developed under the :term:`Vle` platform.
       With the :term:`Vle` platform, a model is developed as a
       :term:`package <vle package>`.

       A vle model is handled as a :ref:`dm_vlepkg` object of the db
       database (see :ref:`databases`).

   models repository
       A models repository provides the required environment for running
       :term:`simulator` s of the :term:`vle model` s that it contains.

       A models repository is a self independant and consistent group of some
       :term:`vle package` s. It is physically installed into a local
       directory that is structured as a VLE_HOME directory.

       A models repository is handled as a :ref:`dm_vlerep` object of the db
       database (see :ref:`databases`).

   vpz file
       See :term:`simulator`

   vpz (vle notion)
       See :term:`simulator`
       
   vpz file (vle notion)
       See :term:`simulator`
       
   simulator
       A simulator corresponds with the vle notion of vpz, or vpz file.
       A simulator *(vpz file)* belongs to a vle model *(vle package)* for
       which it defines an experiment plan (and other things).

       A simulator can be handled as a :ref:`dm_vlevpz` object of the db
       database, or it can also be identified by a :ref:`dm_vpzpath` object of
       the vpz database (see :ref:`databases`).

   simulation scenario
       See :term:`simulator`

   vpz (as VleVpz)
       See :ref:`dm_vlevpz`. See also :term:`simulator`.

   vpz (as VpzAct)
       See :ref:`dm_vpzact`

   vpz path
       See :ref:`dm_vpzpath`. See also :term:`simulator`.

   vpz activity
       See :ref:`dm_vpzact`

   vpz input
       See :ref:`dm_vpzinput`

   input information of a vpz
       See :ref:`dm_vpzinput`

   vpz input information
       See :ref:`dm_vpzinput`

   vpz output
       See :ref:`dm_vpzoutput`

   output information of a vpz
       See :ref:`dm_vpzoutput`

   vpz output information
       See :ref:`dm_vpzoutput`

   vpz origin
       See :ref:`dm_vpzorigin`

   vpz workspace
       See :ref:`dm_vpzworkspace`

   Django
   django
       Django is a Python web framework.
       See https://www.djangoproject.com .

       Django notions of project and application :

       - A django project corresponds with a web project dedicated to a web
         site. It is a collection of configuration and django
         applications for a particular web site.

       - A django application is a Python package that follows a certain
         convention. It provides some web application functions. Developing
         django applications is the way how code is shared and reused between
         django projects.

   Django application
   django application
       See :term:`django`

   Django project
   django project
       See :term:`django`

   Vle
   VLE
   vle
       Vle is a multi-modeling and simulation platform.
       See http://www.vle-project.org .

   Record
   RECORD
   record
       Record is a simulation platform for agro-ecosystems developped at
       :term:`INRA`.
       See http://www.inra.fr/record .

   INRA
       French National Institute for Agricultural Research.
       See http://www.inra.fr .

