
Install vle repository
======================

cd /opt/erecord/repositories

* to create vle (/opt/erecord/repositories/vle) as a 'vle_home' environment :

  mkdir vle
  export VLE_HOME=/opt/erecord/repositories/vle
  vle -R update

* To generate remote.pkg :

  vle -R update

* to know/define packages_list, the list of the existing vle repository
  packages :

  (the command 'grep Package' applied to pkgs-1.1/remote.pkg helps)

  grep Package /opt/erecord/repositories/vle/pkgs-1.1/remote.pkg

* to install packages of packages_list :

  for pkg in packages_list (for all of them or else select some of them) :
  vle -R install pkg

  vle -R install ext.muparser
  vle -R install vle.extension.dsdevs
  vle -R install vle.extension.difference-equation
  vle -R install vle.extension.petrinet
  vle -R install vle.output
  vle -R install vle.extension.differential-equation
  vle -R install vle.extension.celldevs
  vle -R install vle.extension.cellqss
  vle -R install vle.extension.decision
  vle -R install vle.forrester
  vle -R install vle.extension.fsa
  vle -R install vle.examples

