.. _repositories:

===================
Models repositories
===================

Introduction
============

The web services provided by :term:`erecord` allow to edit, modify and simulate
some vle models.

For this, :term:`erecord` manages some models repositories containing some vle
models, containing the simulators that are proposed to users to be manipulated
(seen, modified, simulated).

Before being taken into account, a :term:`simulator` must have been
(i) physically stored into a models repository and (ii) recorded into an
appropriate database (see :ref:`databases`).

A models repository is a self independant and consistent group of some
:term:`vle package` s. It is physically installed into a local directory that
is structured as a VLE_HOME directory. The models repositories are installed
into the 'repositories' directory, that so contains some models repositories
that contain some vle models that contain some simulators.

The physical models repository
==============================

Content
-------

The local directory where the models repository is physically installed is
structured as a VLE_HOME directory.

As a self independant and consistent group of vle packages, the models
repository pkgs-1.1 subdirectory includes all the needed extensions packages
and other dependencies. And its packages versions are all consistent.

Installation
------------

First of all (before being recorded into the database), the models repository
must be installed, with its vle packages in state ready to be simulated. It can
be done manually.  

The models repository directory is built once and for all, repeated 'only' in
case of new version installation *(then copies -for main package- and links
-for dependencies packages- will be done from it to each VLE_HOME relative to
each request)*.

Manual installation from some remote distributions
++++++++++++++++++++++++++++++++++++++++++++++++++

The models repository based on the erecordb distribution is here taken as an
example to illustrate how to manually install/build a models repository
directory from some remote distributions.

For the models repository corresponding with the recordb repository, the
required remote distributions (recordb repository and its dependencies) are :

    - the vle packages distribution (extensions, outputs, etc..) relative to
      vle 1.1 version : http://www.vle-project.org/pub/1.1
    - the record packages distribution (vle-1.1 version) :
      http://recordb.toulouse.inra.fr/distributions/1.1

In the installation directory (structured as a VLE_HOME directory), the remote
distributions are identified by the vle.remote.url variable into the vle.conf
file. The models repository directory is built by 'vle -R install' commands.

The manual instructions : see :ref:`cmd_install_recordb_repository`

