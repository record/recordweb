
.. _erecord_package:

===============
erecord package
===============

Introduction
============

:term:`Record` is a simulation platform for agro-ecosystems developped at
:term:`INRA`.  

The :ref:`erecord project <erecord_project>` is dedicated to the Record
platform web development for vle models.

The erecord package is part of the erecord project. It contains the production
(source code, tests...).

Content hierarchy
=================

The erecord package contains mainly :

    - projects : django projects of the erecord package
    - apps : django applications of the erecord package. The django
      applications are subdirectories of apps. The 'apps' path must be
      included into PYTHONPATH (see the settings.py file of the django project).

Intellectual property
=====================

.. toctree::
   :maxdepth: 1

   intellectual_property/index

Source code API reference
=========================

See :

.. toctree::
   :maxdepth: 2

   ../../../ref/autogen/project/erecord

The erecord package also contains an erecord/apps subdirectory. See :

.. toctree::
   :maxdepth: 2

   ../../../ref/autogen/APPS/index

More
====
.. include:: ../../../include/more.rst

