
======================
More about db/pkg/{Id}
======================

The 'db/pkg/{Id}' resource is one of the resources of :ref:`webapi_db`.

Code
====

The corresponding class is VlePkgDetail.

For more about VlePkgList, see :

    .. autoclass:: erecord_db.views.objects.VlePkgDetail
       :members:
