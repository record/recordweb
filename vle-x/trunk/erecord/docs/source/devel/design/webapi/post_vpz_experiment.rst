.. _post_vpz_experiment:

===================
POST vpz/experiment
===================

Resource URL
============
:ref:`online_url`\ **/vpz/experiment**

Description
===========

Modifies the experiment conditions of a simulator (see
:term:`input information of a vpz`) according to the **experiment plan**
defined into the **posted file**.
In addition with this experiment conditions modification (mandatory posted
file), maybe modifies the **input datas folder** of the simulator according
to another posted file (optional).
Then runs the simulation, produces and **returns a file** containing
the **simulation results** (see :term:`output information of a vpz`) and
also the **experiment conditions** (see :term:`input information of a vpz`).

Details
-------

*jwt*
    .. include:: include/acs_jwt.rst

*posted (and returned) file of experiment plan (and simulation results)*

    The **posted file** is a xls file given by the user into the POST request
    (HTTP multipart request) as 'experimentfile' parameter (see below
    :ref:`post_vpz_experiment_posted_file`).

    The **returned file** is a xls file (see below 
    :ref:`post_vpz_experiment_returned_file`).

*input datas folder*
    .. include:: include/vpz_modify_datafolder.rst

Help
----

See :ref:`an example as help on how to know the simulators that can be chosen
with the 'vpz' option <wwdm_ident>`.

Before a :ref:`post_vpz_experiment` command, it may be useful to do a
:ref:`get_vpz_experiment` command in order to easierly build the xls file that
is required into :ref:`post_vpz_experiment` request. Indeed :

    - .. include:: include/experimentfile_format.rst
    - .. include:: include/experimentfile_help.rst
    - .. include:: include/experimentfile_example.rst

Request parameters
==================

**Required** : either vpz or vpzpath must be provided.

**Required** : experimentfile.

**Required** : jwt in limited access case.

* :ref:`webapi_activity_vpz_choice`

* :ref:`webapi_opt_experimentfile`

* :ref:`webapi_opt_datafolder`
* :ref:`webapi_opt_datafoldercopy`

* :ref:`webapi_opt_todownload`
* :ref:`webapi_opt_format` 

* :ref:`webapi_opt_jwt`

.. _post_vpz_experiment_posted_file:

The posted file (experimentfile parameter)
--------------------------------------------

  .. include:: include/experimentfile_format.rst

  The **posted xls file** contains the experiment conditions from which
  the simulator has to be updated :

    - *General information* :
      plan *('single' or 'linear' values)*,
      restype *('dataframe' or 'matrix' values)*,
      begin, duration.

    - *Parameters values* : list of the parameters values to be modified
      *(the unchanged values may remain but it is not necessary)*.

    - *Parameters identification* : list of the parameters to be given into
      the :ref:`returned file <post_vpz_experiment_returned_file>`.

    - *Output datas identification* : list of the output datas to be given
      into the :ref:`returned file <post_vpz_experiment_returned_file>`.

  :download:`Example of a posted xls file<../../../using/examples/include/experiment_post_in/experiment.xls>`

Response result
===============

The returned result is a file report about both :ref:`dm_vpzoutput` and
:ref:`dm_vpzinput`.

.. _post_vpz_experiment_returned_file:

The returned file
-----------------

  The **returned xls file** contains the simulation results and experiment
  conditions of the simulator modified according to the
  :ref:`posted file<post_vpz_experiment_posted_file>` :

    - *Simulation results* :

      Values array(s) of the selected output datas.

    - *Experiment conditions* :

      - *General information* : plan, restype, begin, duration according to
        the :ref:`posted file information<post_vpz_experiment_posted_file>`.
  
      - *Parameters values* : list of the selected parameters values according
        to the :ref:`posted file information<post_vpz_experiment_posted_file>`.
  
      - *Parameters identification* : list of the selected parameters.
  
      - *Output datas identification* : list of the selected output datas.

  Only the selected output datas and parameters are present into the returned
  file :
  
    - a parameter is selected if it has been declared into the 
      :ref:`posted file<post_vpz_experiment_posted_file>`, in the dedicated
      list *"Parameters identification"*.
    - an output data is selected if it has been declared into the
      :ref:`posted file<post_vpz_experiment_posted_file>`, in the dedicated
      list *"Output datas identification"*.

  Limit : in case of xls file overflow
  *(sheets limited to 256 columns and 65536 rows)* the results will be
  truncated (without any warning).

  :download:`Example of a returned xls file<../../../using/examples/include/experiment_post_out/experiment.xls>`

Example
=======

Example : :ref:`In command line with cURL <erecord_use_capture_170>`

Example : in menus under the page :ref:`online_url_inoutmodify_menu`, the
*'Third step : send your own experiment plan'* consists in a
:ref:`post_vpz_experiment` request. :ref:`Try <online_url_inoutmodify_menu>`.

More
====

This resource is part of :ref:`webapi_vpz`

