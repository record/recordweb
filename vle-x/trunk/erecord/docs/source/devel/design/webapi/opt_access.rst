.. _webapi_opt_access:

======
access
======

* access 

    - value 'public' for a public access case.
    - value 'limited' for a limited access case.

For more see :ref:`webapi_feat_access` | :ref:`erecord_authentication`.

