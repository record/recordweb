
====================
More about vpz/input
====================

The 'vpz/input' resource is one of the activities on a vpz (see :ref:`webapi_vpz`).

Code
====

The corresponding class is InputView.

For more about InputView, see :

    .. autoclass:: erecord_vpz.views.activities.InputView
       :members:
