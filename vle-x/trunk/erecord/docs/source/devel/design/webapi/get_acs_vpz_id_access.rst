.. _get_acs_vpz_id_access:

========================
GET acs/vpz/{Id}/access
========================

Resource URL
============
:ref:`online_url`\ **/acs/vpz/{Id}/access**

Description
===========

Returns the access type of the :term:`simulator` (as :ref:`dm_vlevpz`) having the Id value (as :ref:`dm_vlevpz`).

Request parameters
==================

* *Id* : id value of the simulator as :ref:`dm_vlevpz`

* :ref:`webapi_opt_format` 

Response result
===============

* :ref:`access <webapi_access_type>`

Example
=======

*under construction*

More
====

This resource is part of :ref:`webapi_acs`

