
The 'slm' web services are provided by the :term:`erecord_slm` application
that is dedicated to downloadable files management.

A downloadable file results from a request (*such as*
:ref:`get_vpz_report_conditions`, :ref:`post_vpz_report`,
:ref:`post_vpz_experiment`...) that had been sent by asking to download the
result file later on (see :ref:`webapi_opt_todownload` option).

