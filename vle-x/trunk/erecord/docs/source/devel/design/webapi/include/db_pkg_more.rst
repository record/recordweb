
=================
More about db/pkg
=================

The 'db/pkg' resource is one of the resources of :ref:`webapi_db`.

Code
====

The corresponding class is VlePkgList.

For more about VlePkgList, see :

    .. autoclass:: erecord_db.views.objects.VlePkgList
       :members:
