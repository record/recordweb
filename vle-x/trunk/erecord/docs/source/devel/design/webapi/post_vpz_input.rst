.. _post_vpz_input:

==============
POST vpz/input
==============

Resource URL
============
:ref:`online_url`\ **/vpz/input**

:ref:`Try it <online_url_input_menu>`
-------------------------------------

Description
===========

Returns the **input conditions** of a simulator (see
:term:`input information of a vpz`), maybe after modifications *(depending on
the request parameters)*.

Details
-------

*jwt*
    .. include:: include/acs_jwt.rst

*input information*

    .. include:: include/vpz_modify_input.rst

*filtering*

    It is possible to ask for filtering some of the returned information (see 
    'parselect').

*style*
    .. include:: include/vpz_style_input.rst

Help
----

See :ref:`an example as help on how to know the simulators that can be chosen
with the 'vpz' option <wwdm_ident>`.

Before a :ref:`post_vpz_input` command, it may be useful to do a
:ref:`get_vpz_input` command with 'compact' or 'compactlist' style (see
:ref:`webapi_opt_style`) in order to make easier some modifications
(see :ref:`webapi_opt_pars`, :ref:`webapi_opt_parselect`).
See also :ref:`help to know how some information is identified/selected in
the request <wwdm_ident_fields>`.

Request parameters
==================

**Required** : either vpz or vpzpath must be provided.

**Required** : jwt in limited access case.

* :ref:`webapi_activity_vpz_choice`

* :ref:`webapi_opt_begin`
* :ref:`webapi_opt_duration`
* :ref:`webapi_opt_pars`

* :ref:`webapi_opt_parselect`
* :ref:`webapi_opt_outselect`

* :ref:`webapi_opt_style` 

* :ref:`webapi_opt_format` 

* :ref:`webapi_opt_jwt`

Response result
===============

The returned result is about :ref:`dm_vpzinput`.


Example
=======

.. include:: ../../../using/examples/include/wwdm_editmodif_quickoverview_tree.rst
.. include:: ../../../using/examples/include/wwdm_editmodif_quickoverview.rst

See others examples in :ref:`wwdm_editmodif`.

More
====

This resource is part of :ref:`webapi_vpz`

Go to :doc:`include/vpz_input_more`

