.. _webapi_acs_resources:

- **JSON Web Token**

    - :ref:`post_acs_jwt_obtain` : to get a JWT value
    - :ref:`post_acs_jwt_verify` : to verify a JWT value

- **access information** for simulators as :ref:`dm_vlevpz` :

    - :ref:`get_acs_vpz_id_access`     : access type (limited or public) of a simulator
    - :ref:`get_acs_vpz_id_user_id`    : list of id of a simulator authorized users
    - :ref:`get_acs_vpz_id_user_name`  : list of name of a simulator authorized users
    - :ref:`get_acs_vpz_accessible_id` : list of id of the accessible simulators

- **access information** for simulators as :ref:`dm_vpzpath` :

    - :ref:`get_acs_vpzpath_id_access`     : access type (limited or public) of a simulator
    - :ref:`get_acs_vpzpath_id_user_id`    : list of id of a simulator authorized users
    - :ref:`get_acs_vpzpath_id_user_name`  : list of name of a simulator authorized users
    - :ref:`get_acs_vpzpath_accessible_id` : list of id of the accessible simulators

