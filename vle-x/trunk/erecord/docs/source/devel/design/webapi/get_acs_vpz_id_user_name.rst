.. _get_acs_vpz_id_user_name:

==========================
GET acs/vpz/{Id}/user/name
==========================

Resource URL
============
:ref:`online_url`\ **/acs/vpz/{Id}/user/name**

Description
===========

Returns the list of username of users authorized for a :term:`simulator` (as :ref:`dm_vlevpz`) having the Id value (as :ref:`dm_vlevpz`).

Request parameters
==================

* *Id* : id value of the simulator as :ref:`dm_vlevpz`

* :ref:`webapi_opt_format` 

Response result
===============

"list" : list of users username.


More
====

This resource is part of :ref:`webapi_acs`

