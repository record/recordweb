.. _webapi_opt_parselect:

=========
parselect
=========

The 'parselect' option is used to choose the restituted parameters.

* parselect

    - value 'all' : to select all parameters of all conditions.

    - value condname : to select all parameters of the condition named condname.

    - value cname.pname : to select the parameter named pname of the condition
      named cname (the parameters of this condition, that are not selected
      like this, are unselected).

