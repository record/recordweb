.. _post_vpz_output:

===============
POST vpz/output
===============

Resource URL
============
:ref:`online_url`\ **/vpz/output**

:ref:`Try it <online_url_output_menu>`
--------------------------------------

Description
===========

Maybe modifies the input conditions of a simulator (see
:term:`input information of a vpz`) *(depending on the request parameters)*.
In addition with this input conditions modification (optional), maybe
modifies the **input datas folder** of the simulator according to a
posted file (optional).
Then runs the simulation and returns the **simulation results** (see
:term:`output information of a vpz`).

Details
-------

*jwt*
    .. include:: include/acs_jwt.rst

*input information*

    .. include:: include/vpz_modify_input.rst

*input datas folder*
    .. include:: include/vpz_modify_datafolder.rst

*filtering*

    It is possible to ask for filtering some of the returned information (see 
    'outselect').

*style*
    .. include:: include/vpz_style_output.rst

*style, restype*
    .. include:: include/vpz_style_and_restype.rst

Help
----

See :ref:`an example as help on how to know the simulators that can be chosen
with the 'vpz' option <wwdm_ident>`.

Before a :ref:`post_vpz_output` command, it may be useful to do a
:ref:`get_vpz_input` command with 'compact' or 'compactlist' style (see
:ref:`webapi_opt_style`) in order to make easier some modifications (see
:ref:`webapi_opt_pars`, :ref:`webapi_opt_outselect`).
See also :ref:`help to know how some information is identified/selected in the request <wwdm_ident_fields>`.

Request parameters
==================

**Required** : either vpz or vpzpath must be provided.

**Required** : jwt in limited access case.

* :ref:`webapi_activity_vpz_choice`

* :ref:`webapi_opt_plan_restype`

* :ref:`webapi_opt_begin`
* :ref:`webapi_opt_duration`
* :ref:`webapi_opt_pars`

* :ref:`webapi_opt_datafolder`
* :ref:`webapi_opt_datafoldercopy`

* :ref:`webapi_opt_outselect`

* :ref:`webapi_opt_style`

* :ref:`webapi_opt_storage`

* :ref:`webapi_opt_format` 

* :ref:`webapi_opt_jwt`

Response result
===============

The returned result is about :ref:`dm_vpzoutput`.

Example
=======

.. include:: ../../../using/examples/include/wwdm_runmodif_quickoverview_tree.rst
.. include:: ../../../using/examples/include/wwdm_runmodif_quickoverview.rst

-> *Example using datafolder, datafoldercopy* :
:ref:`In command line with cURL <erecord_use_capture_135_3>`

See :ref:`wwdm_runmodif`.

More
====

This resource is part of :ref:`webapi_vpz`

Go to :doc:`include/vpz_output_more`

