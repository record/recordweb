.. _get_db_pkg_id_name:

====================
GET db/pkg/{Id}/name
====================

Resource URL
============
:ref:`online_url`\ **/db/pkg/{Id}/name**

Description
===========

Returns the name of the :term:`vle model` having the Id value (as
:ref:`dm_vlepkg`).

Request parameters
==================

* *Id* : id value of the vle model as :ref:`dm_vlepkg`

* :ref:`webapi_opt_format` 

Response result
===============

"name" : name of of the vle model as :ref:`dm_vlepkg`.

