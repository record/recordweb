.. _webapi_erecord:

===============
erecord Web API
===============

Introduction
============

The web services provided by :term:`erecord` allow to edit, modify and simulate
some :term:`agronomic model` s of the :term:`Record` platform, and more
generally some :term:`vle model` s.

The 'vpz' web services
----------------------

.. include:: include/vpz_intro.rst

The 'db' web services
---------------------

.. include:: include/db_intro.rst

The 'slm' web services
---------------------

.. include:: include/slm_intro.rst

The 'acs' web services
---------------------

.. include:: include/acs_intro.rst

Presentation by application
===========================

.. toctree::
   :maxdepth: 1

   main_erecord_db
   main_erecord_vpz
   main_erecord_cmn
   main_erecord_slm
   main_erecord_acs

More
====
.. include:: ../../../include/more.rst

