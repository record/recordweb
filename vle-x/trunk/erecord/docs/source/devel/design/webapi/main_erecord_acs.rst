.. _webapi_erecord_acs:

===============================
Web API erecord_acs application
===============================

Overview
========
.. include:: include/acs_intro.rst

Main resources
==============
.. include:: acs_resources.rst

Other resources
===============
.. include:: acs_others.rst

Options
=======

* :doc:`opt_access`
* :doc:`opt_jwt`
* *username*
* *password*
* *token* : JWT value to be verified.

More
====
.. include:: ../../../include/more.rst

