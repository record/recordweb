.. _post_vpz_report_conditions:

==========================
POST vpz/report/conditions
==========================

Resource URL
============
:ref:`online_url`\ **/vpz/report/conditions**

Description
===========

Maybe modifies the input conditions of a simulator (see
:term:`input information of a vpz`) *(depending on the request parameters)*,
then produces and returns the required **reports**,
from the **input conditions** (see :term:`input information of a vpz`).

Details
-------

*jwt*
    .. include:: include/acs_jwt.rst

*input information*

    .. include:: include/vpz_modify_input.rst

*filtering*

    It is possible to ask for filtering some of the returned information (see
    'parselect', 'outselect').

*reports*

    The built reports are gathered and returned into a '.zip' file.

Help
----

See :ref:`an example as help on how to know the simulators that can be chosen
with the 'vpz' option <wwdm_ident>`.

Before a :ref:`post_vpz_report_conditions` command, it may be useful to do a
:ref:`get_vpz_input` command with 'compact' or 'compactlist' style (see
:ref:`webapi_opt_style`) in order to make easier some modifications (see
:ref:`webapi_opt_pars`, :ref:`webapi_opt_parselect`,
:ref:`webapi_opt_outselect`).
See also :ref:`help to know how some information is identified/selected in the request <wwdm_ident_fields>`.

Request parameters
==================

**Required** : either vpz or vpzpath must be provided.

**Required** : jwt in limited access case.

* :ref:`webapi_activity_vpz_choice`

* :ref:`webapi_opt_begin`
* :ref:`webapi_opt_duration`
* :ref:`webapi_opt_pars`

* :ref:`webapi_opt_parselect`

* :ref:`webapi_opt_bycol`
* :ref:`webapi_opt_todownload`
* :ref:`webapi_opt_format` 

* :ref:`webapi_opt_jwt`

Response result
===============

The returned result is reports about the simulation conditions (from
:ref:`dm_vpzinput`).

Example
=======

.. literalinclude:: ../../../using/examples/include/memo_request_162.rst

More
====

This resource is part of :ref:`webapi_vpz`

