.. _get_db_rep:

==========
GET db/rep
==========

Resource URL
============
:ref:`online_url`\ **/db/rep**

Description
===========

Returns the identification information of some :term:`models repository`\ s
(as :ref:`dm_vlerep`).

Details
-------

*style*
    .. include:: include/db_style.rst

Help
----

See :ref:`an example that may help on how to know the values that can be chosen
for the following options <wwdm_ident>` : :ref:`webapi_db_rep_filter`.

Request parameters
==================

* :ref:`webapi_db_rep_filter`

* :ref:`webapi_opt_style` 

* :ref:`webapi_opt_format` 

Response result
===============

The returned result is about :ref:`dm_vlerep`.

Example
=======

An overview example that you can adapt to what you need :

    .. include:: ../../../using/examples/include/db_quickoverview.rst

Some use case examples in : :ref:`wwdm_ident` (from
:ref:`examples <erecord_examples>`).

More
====

This resource is part of :ref:`webapi_db`

Go to :doc:`include/db_rep_more`

