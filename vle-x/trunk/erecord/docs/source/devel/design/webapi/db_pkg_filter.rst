.. _webapi_db_pkg_filter:

===
pkg
===

The 'pkg' option is used to specify which :term:`vle model` to keep (filtering all the other ones). The vle model under consideration is a :ref:`dm_vlepkg`.

- pkg

    - value : id of the relevant :ref:`dm_vlepkg`

.. note::

   To know the existing vle models : :ref:`get_db_pkg` resource (see 
   :ref:`the 'db' web services <webapi_db_resources>`)

   You will find help in the :ref:`examples <erecord_examples>`, especially
   in :ref:`wwdm_ident`.

