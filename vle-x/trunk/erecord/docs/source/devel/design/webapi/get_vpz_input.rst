.. _get_vpz_input:

=============
GET vpz/input
=============

Resource URL
============
:ref:`online_url`\ **/vpz/input**

Description
===========

Returns the **input conditions** of a simulator (see
:term:`input information of a vpz`).

Details
-------

*jwt*
    .. include:: include/acs_jwt.rst

*style*
    .. include:: include/vpz_style_input.rst

Help
----

See :ref:`an example as help on how to know the simulators that can be chosen
with the 'vpz' option <wwdm_ident>`.

Request parameters
==================

**Required** : either vpz or vpzpath must be provided.

**Required** : jwt in limited access case.

* :ref:`webapi_activity_vpz_choice`

* :ref:`webapi_opt_parselect`

* :ref:`webapi_opt_style` 

* :ref:`webapi_opt_format` 

* :ref:`webapi_opt_jwt`

Response result
===============

The returned result is about :ref:`dm_vpzinput`.

Example
=======

    See :ref:`wwdm_editasis`.

More
====

This resource is part of :ref:`webapi_vpz`

Go to :doc:`include/vpz_input_more`

