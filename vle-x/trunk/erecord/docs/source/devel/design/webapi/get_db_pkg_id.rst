.. _get_db_pkg_id:

===============
GET db/pkg/{Id}
===============

Resource URL
============
:ref:`online_url`\ **/db/pkg/{Id}**

Description
===========

Returns the identification information of the :term:`vle model` having
the Id value (as :ref:`dm_vlepkg`).

Details
-------

*style*
    .. include:: include/db_style.rst

Request parameters
==================

* *Id* : id value of the vle model as :ref:`dm_vlepkg`

* :ref:`webapi_opt_style` 

* :ref:`webapi_opt_format` 

Response result
===============

The returned result is about :ref:`dm_vlepkg`.

Example
=======

An overview example that you can adapt to what you need :

    .. include:: ../../../using/examples/include/db_quickoverview.rst

Some use case examples in : :ref:`wwdm_ident` (from
:ref:`examples <erecord_examples>`).

More
====

This resource is part of :ref:`webapi_db`

Go to :doc:`include/db_pkg_id_more`

