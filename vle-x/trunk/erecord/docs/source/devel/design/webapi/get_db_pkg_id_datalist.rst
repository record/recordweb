.. _get_db_pkg_id_datalist:

========================
GET db/pkg/{Id}/datalist
========================

Resource URL
============
:ref:`online_url`\ **/db/pkg/{Id}/datalist**

Description
===========

Returns the list of names of data files of the :term:`vle model` having the
Id value (as :ref:`dm_vlepkg`). Data files are those present into its
:term:`vle package` default data folder.

Request parameters
==================

* *Id* : id value of the vle model as :ref:`dm_vlepkg`

* :ref:`webapi_opt_format` 

Response result
===============

"datalist" : list of data file names.

