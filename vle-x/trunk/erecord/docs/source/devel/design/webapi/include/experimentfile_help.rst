
The xls file returned by the :ref:`get_vpz_experiment` request contains some
instructions (in blue text) helping to modify it, if wanted to use it as the
posted xls file of a :ref:`post_vpz_experiment` request.

