.. _webapi_opt_format:

=======================
format or format suffix
=======================

The 'format' option is used to choose the format, that can also be chosen by
adding a format suffix to the URL.

All the following format values are not supported in all the situations.

* format
    
    - value api
    - value json
    - value yaml
    - value xml
    - value html

Format suffix
=============

    - value .api
    - value .json
    - value .yaml
    - value .xml
    - value .html

