.. _get_db_vpz_id:

===============
GET db/vpz/{Id}
===============

Resource URL
============
:ref:`online_url`\ **/db/vpz/{Id}**

Description
===========

Returns the identification information of the :term:`simulator` 
(as :ref:`dm_vlevpz`) having the Id value (as :ref:`dm_vlevpz`).

Details
-------

*style*
    .. include:: include/db_style.rst

Request parameters
==================

* *Id* : id value of the simulator as :ref:`dm_vlevpz`

* :ref:`webapi_opt_style` 

* :ref:`webapi_opt_format` 

Response result
===============

The returned result is about :ref:`dm_vlevpz`.

Example
=======

An overview example that you can adapt to what you need :

    .. include:: ../../../using/examples/include/db_quickoverview.rst

Some use case examples in : :ref:`wwdm_ident` (from
:ref:`examples <erecord_examples>`).

More
====

This resource is part of :ref:`webapi_db`

Go to :doc:`include/db_vpz_id_more`

