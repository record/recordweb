
=========================
More about mode for style
=========================

Some resources for which the mode option is available to choose a style :

- some resources for which the mode option is available to choose a style
  value into the main values 'link' and 'tree':
  db/rep, db/pkg, db/vpz,
  vpz/vpzpath
  vpz/vpzinput, vpz/vpzoutput, vpz/vpzact,
  vpz/begin, vpz/duration, vpz/cond, vpz/par, vpz/view, vpz/out,
  vpz/vpzorigin, vpz/vpzworkspace
- some resources for which the mode option is available to choose a style
  value into 'link', 'tree', 'compact', 'compactlist':
  vpz/input, vpz/inout, vpz/vpzinput
