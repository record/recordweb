
=================
More about db/rep
=================

The 'db/rep' resource is one of the resources of :ref:`webapi_db`.

Code
====

The corresponding class is VleRepList.

For more about VleRepList, see :

    .. autoclass:: erecord_db.views.objects.VleRepList
       :members:
