.. _webapi_opt_storage:

================
mode for storage
================

The :ref:`'mode' option <webapi_opt_mode>` is used to choose whether the
:ref:`web storage capability <webapi_feat_storage>` (if supported by the web
browser) is used or not.

Value 'storage' (mode=storage) for the web storage to be used.
