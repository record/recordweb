.. _webui:

==================
Web User Interface
==================

Overview
========

The `Web User Interface <http://erecord.toulouse.inra.fr:8000/db/menu/>`_ makes easier calling some :term:`erecord` web services.

The **main menu** allows to select a simulator, and then see or/and run it.

Access to main menu
-------------------

http://erecord.toulouse.inra.fr:8000/home
--------------------------

.

There are **some other entry points**.

Submenus
========

Menu to see a simulator *after having selected it if {Id} was not given* :
  http://erecord.toulouse.inra.fr:8000/db/menu/input/{Id}

Menu to run a simulator *after having selected it if {Id} was not given* :
 http://erecord.toulouse.inra.fr:8000/db/menu/output/{Id}

Menu to see and run a simulator *after having selected it if {Id} was not given* :
 http://erecord.toulouse.inra.fr:8000/db/menu/inout/{Id}

Menu to modify and run a simulator
*after having selected it if {Id} was not given* :
 http://erecord.toulouse.inra.fr:8000/db/menu/experiment/{Id}

Menu to see or/and modify or/and run a simulator
*after having selected it if {Id} was not given* :
 http://erecord.toulouse.inra.fr:8000/db/menu/{Id}

.. note::
   The :ref:`webapi_db_rep_filter`, :ref:`webapi_db_pkg_filter` and
   :ref:`webapi_db_vpz_filter` options can be used to filter the simulators
   shown to be selected.

   Examples :

   http://erecord.toulouse.inra.fr:8000/db/menu/input/?rep=2

   http://erecord.toulouse.inra.fr:8000/db/menu/?pkg=19

   http://erecord.toulouse.inra.fr:8000/db/menu/output/?pkg=41

.. note::
   About some of the URLs above : *'menu'* can be directly used instead of
   *'db/menu'*

