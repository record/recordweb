.. _webapi_feat_access:

================================
Model access (public or limited)
================================

Introduction
============

The access to a :term:`model` can be a public access or a limited access, as decided by the model owner when he has delivered his model to erecord :

    - The erecord web services allow anyone to edit, modify and simulate a :term:`simulator` that is in **public access**.

    - Only some authorized users are allowed to edit, modify and simulate a :term:`simulator` that is in **limited access** and consequently, this simulator agronomic parameters values and simulation results are hidden to all the others users.


Users management
================

Models delivery
---------------

When a model owner delivers his model to erecord, he chooses the access mode of each simulator of his model : he may choose a public access for some simulators and keep some other simulators in limited access, for example if he wants to hide their agronomic parameters values.  Then for each simulator in limited access, the model owner selects the authorized **users**.

User notion
-----------

For erecord web services, **a user is an identifier that can be shared by several persons**. It is not systematically dedicated to a single person.

Here is a possible **way of proceeding** : When a model owner delivers to erecord his model declared in limited access, it can be created into erecord a user dedicated to the model (*more exactly it can be created one or many dedicated users : for example a user for the regular users group, a user for casual users...*). This created user belongs to the model owner who is in charge of managing it as desired : the model owner can choose to keep it for himself or he can decide to share it, by giving it to some  colleagues, to someone who asks him for permission of using his model...

.. include:: no_declaration.rst

JSON Web Token authentication
=============================

The access control management by erecord is based on JWT (JSON Web Token).

For more details about JWT : https://en.wikipedia.org/wiki/JSON_Web_Token.

In practice, a user gets a JWT value from his user name and password. He uses his JWT value to call any of the simulators in limited access that he is allowed to use. A JWT value is available for a limited duration (see in :ref:`source code <apiref_index>` TOKEN_LIFETIME, the duration before a token expiration). So several days after having acquired a JWT value, a user will have to ask for a new one. For more details : :ref:`JWT in practice <erecord_authentication>`.

See also
========

    :ref:`erecord_authentication` | :ref:`howto_get_jwt`

