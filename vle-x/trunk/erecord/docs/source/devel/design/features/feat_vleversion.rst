
.. _webapi_feat_vleversion:

====================
Several vle versions
====================

Introduction
------------

erecord (*from the vle-x version of erecord*) accepts models repositories developped with **different vle versions**.

Description
-----------

A vle version is defined by :

- the **vle version name**, structured as vle-V.rev.

  For example vle-1.1.3, vle-2.0.0, vle-1.1...

- the **vle usr path** where the vle version has been installed.

  Containing subdirectories : 

    - bin
    - lib
    - lib/pkgconfig


How is it managed
-----------------

In case of a simulator defined/recorded as vpz (:ref:`dm_vlevpz`) :

  - Each vle version (:ref:`dm_vleversion`) is recorded into the erecord database.
  - Each models repository (:ref:`dm_vlerep`) is attached to a vle version (:ref:`dm_vleversion`).

In case of a simulator defined/recorded as vpzpath (:ref:`dm_vpzpath`) :

    The following rule has been defined in order to be able to identify the
    vle version of such simulators.

Configuration rule
++++++++++++++++++

For a simulator to be able to be declared as a VpzPath, the directory of the models repository containing it must contain a subdirectory named **"erecord_conf"**. This "erecord_conf" folder must contain :

  - a file named **"VLE_VERSION"** : containing the vle version name.
  - a file named **"VLE_USR_PATH"** : containing the vle install path name.

VleRep.path contains :

  - erecord_conf / VLE_VERSION
  - record_conf / VLE_USR_PATH

