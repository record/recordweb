.. _devel_documentation:

=============
Documentation
=============

Introduction
============

The **main documentation** of erecord project is produced with Sphinx *(Sphinx is a documentation generator which converts reStructuredText files into HTML websites and other formats including PDF, EPub and man)*.

There is some **other external documentation**, for example html home pages produced for erecord simulators *(with a specific python script)*.

Production and generation
=========================




Main documentation
------------------

    See "**Main documentation generation**" into
    :ref:`devel_deployment_install_erecord`.

Some other external documentation
---------------------------------

    See "**Simulators home pages generation**" into
    :ref:`devel_deployment_install_erecord`.

