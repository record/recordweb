
=====================
Deployment path names
=====================

Here is the presentation of :

- Some names used in the deployment description, to call some paths.
- Some existing files invoked in the deployment description.

.. _deploy_root_path:

root_path
=========

Let's name **root_path** the installation root path. 

In the deployment example, :ref:`deploy_root_path` is '/opt'.

.. _deploy_erecord_path:

erecord_path
============

Let's name **erecord_path** the erecord project software installation path. 
erecord_path is 'erecord' under :ref:`deploy_root_path`.

In the deployment example, erecord_path is '/opt/erecord'.

.. _deploy_erecord_docs_path:

erecord_docs_path
=================

Let's name **erecord_docs_path** the documentation directory of the erecord
project software. erecord_docs_path is 'docs' under :ref:`deploy_erecord_path`.

In the deployment example, erecord_docs_path is '/opt/erecord/docs'.

.. _deploy_erecord_repositories_path:

erecord_repositories_path
=========================

Let's name **erecord_repositories_path** the models repositories directory of
the erecord project software. erecord_repositories_path is 'repositories' under :ref:`deploy_erecord_path`.

In the deployment example, erecord_repositories_path is
'/opt/erecord/repositories'.

