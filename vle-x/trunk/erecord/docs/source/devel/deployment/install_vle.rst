.. _devel_deployment_install_vle:

===================
Installation of vle
===================

Case of vle-1.1.3 version
=========================

    .. literalinclude:: ../../../../erecord/install/vle/install_vle_1.1.3.txt

Case of vle-2.0.0 version
=========================

    .. literalinclude:: ../../../../erecord/install/vle/install_vle_2.0.0.txt

