.. _devel_deployment:

==========
Deployment
==========

Introduction
============

The erecord project software, once developed, can be deployed to a production
server in order to be used by web users.

This implies a deployment environment, in addition to the tools and libraries
already used for development.

Source software
---------------

The source software includes :

    - The erecord project software

    - Some models software, stored as elements of models repositories installed
      into the 'repositories' subdirectory of the erecord project.

Once the installation finished, the resulting hierarchy is such as :

    .. literalinclude:: include/hierarchy_source.rst

Development environment
-----------------------

The erecord project software is developed with :

    - python language (python 2.7 version),
    - django framework,
    - some django applications, like for example django-rest-framework. 
    - SQLite for databases.
    - Sphinx for documentation.

    See the requirement.txt file :

    .. literalinclude:: ../../../../erecord/install/requirement.txt

The stored models software have previously been developed with :term:`vle`, that is a C++ platform.

Deployment environment
----------------------

The software has been deployed on a virtual machine with **Debian 9.5**
(*Stretch*), **Apache2.4** server and **mod_wsgi**.

    *Django’s primary deployment platform is WSGI, the Python standard for web
    servers and applications. mod_wsgi is an Apache module which can host any
    Python WSGI application, including Django.*

Installation procedure
======================

  - :ref:`devel_deployment_install_erecord`
  - :ref:`devel_deployment_install_vle`
  - :ref:`devel_deployment_install_vle_repositories`

More
====

  :ref:`Installation of erecord in case of development situation <devel_deployment_install_erecord_devcase>`

