.. _devel_deployment_install_vle_repositories:

==================================================
Installation of models repositories (vle packages)
==================================================

  .. literalinclude:: ../../../../erecord/install/vle/install_vle_repositories_notes.txt

Case of vle-1.1.3 version
=========================

  .. literalinclude:: ../../../../erecord/install/vle/install_vle_1.1.3_repositories.txt

Case of vle-2.0.0 version
=========================

  .. literalinclude:: ../../../../erecord/install/vle/install_vle_2.0.0_repositories.txt

