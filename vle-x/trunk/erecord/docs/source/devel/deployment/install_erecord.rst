.. _devel_deployment_install_erecord:

=======================
Installation of erecord
=======================

  .. literalinclude:: ../../../../erecord/install/prod/install.txt

Other installations to be done
==============================

Vle environment
---------------

  - :ref:`devel_deployment_install_vle` (several vle versions can be installed)

  - :ref:`devel_deployment_install_vle_repositories`

Simulators HTML home pages
--------------------------

  - :ref:`devel_deployment_install_simulators_html_home_pages`


Misc
====

  - :ref:`devel_deployment_install_log`

  - :ref:`devel_deployment_install_help`

