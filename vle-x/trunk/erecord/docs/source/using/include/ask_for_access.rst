
If you want to use a simulator that is in limited access, contact nathalie.rousse@inra.fr (erecord project) to ask for access authorization (*you can also directly contact the model owner if you know him*).

If your request is accepted, it will be given to you the information of an authorized user of the simulator you are interested in.

With this user information (*username* and *password*), you will be able to ask for a JWT value, thanks to which you will be able to send requests about all the simulators that this user is allowed to use.

