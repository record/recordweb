.. _using:

======================
Using the web services
======================

.. toctree::
   :maxdepth: 1

   callways
   examples/index
   Tutorial (through some examples) <examples/index>
   Case of models in limited access <authentication>

See also the :ref:`webui`

See also the :ref:`webapi`

See also : `Using an agronomic model developped with the Record platform and installed into the erecord web services <http://147.100.179.168/docs/erecord/model_user.pdf>`_ *(fr) (Utiliser un modèle agronomique développé sous la plateforme Record et disponible dans les services web erecord)*.

