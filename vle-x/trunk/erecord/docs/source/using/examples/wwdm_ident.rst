.. _wwdm_ident:

===================================================
Identifying the simulator from erecord point of vue
===================================================

Introduction
============

*The erecord web services used in this part are some of the ‘db’ web services*
(see :ref:`webapi_db`).

Identifying the 'wwdm.vpz' simulator consists in identifying its Id into the erecord databases. It is necessary to know the simulator's Id when asking for seeing, modifying and simulating it.

In our case, the 'wwdm.vpz' simulator is recorded into the erecord databases as a :ref:`dm_vlevpz` (for more, see :ref:`wwdm_databases`).

Step by step 
============

- You can enter in a webbrowser the URL to list all the models repositories :

  http://erecord.toulouse.inra.fr:8000/db/rep/

  It shows in particular the 'recordb' models repository that hosts our
  'wwdm.vpz' simulator. We see that the 'recordb' models repository's Id is 2.

  .. literalinclude:: include/recordb_id.rst

  .. note:: The equivalent command line with cURL is :

     curl http://erecord.toulouse.inra.fr:8000/db/rep/

- You can then enter in your webbrowser the URL to list all the models
  of the 'recordb' models repository *(whose Id is 2)* :

  http://erecord.toulouse.inra.fr:8000/db/pkg/?rep=2

  It shows in particular the 'wwdm' model that contains our 'wwdm.vpz'
  simulator. We see that the 'wwdm' model's Id is 41.

  .. literalinclude:: include/wwdm_vlepkg_id.rst

- You can then enter in a webbrowser the URL to list all the simulators
  of the 'wwdm' model *(whose Id is 41)* :

  http://erecord.toulouse.inra.fr:8000/db/vpz/?pkg=41
  
  It shows in particular our simulator 'wwdm.vpz' whose Id is 266.

  .. literalinclude:: include/wwdm_vlevpz_id.rst

  .. note:: Command line with cURL to control that the 'wwdm.vpz' simulator's
     Id is 266 :

     curl http://erecord.toulouse.inra.fr:8000/db/vpz/266/

Directly
========

- You can enter in a webbrowser the URL to list all the simulators :

  http://erecord.toulouse.inra.fr:8000/db/vpz/

  It shows in particular our simulator 'wwdm.vpz' whose Id is 266.

  .. literalinclude:: include/wwdm_vlevpz_id.rst

  *If you knew only the name of your simulator 'wwdm.vpz', or only the name of
  the model 'wwdm' (without knowing into which models repository they are),
  you could search for the words you know ('wwdm.vpz', 'wwdm'...) in the
  resulting list.*

  .. note:: The equivalent command line with cURL is :

     curl http://erecord.toulouse.inra.fr:8000/db/vpz/

Browsing
========

Maybe you don't know anything about the models repositories, models, simulators of erecord.

- You can enter in a webbrowser the URL to list all the models repositories :

  http://erecord.toulouse.inra.fr:8000/db/rep/

  You can then browse among the links to discover the different models
  repositories, models, simulators.

  .. note:: The equivalent command line with cURL is :

     curl http://erecord.toulouse.inra.fr:8000/db/rep/

Miscellaneous
=============

*Some other commands that may be useful*

- You can enter in a webbrowser the URL to list all the models :

  http://erecord.toulouse.inra.fr:8000/db/pkg/

  or http://erecord.toulouse.inra.fr:8000/db/pkg/?mode=tree 
  to deploy information

- You can enter in a webbrowser the URL to list all the models repositories :

  http://erecord.toulouse.inra.fr:8000/db/rep/

  or http://erecord.toulouse.inra.fr:8000/db/rep/?mode=tree 
  to deploy information

- You can enter in a webbrowser the URL to control that the 'wwdm' model's Id
  is 41 :

  http://erecord.toulouse.inra.fr:8000/db/pkg/41

  .. note:: The equivalent command line with cURL is :

     curl http://erecord.toulouse.inra.fr:8000/db/pkg/41/

Highlights
==========

The 'wwdm.vpz' simulator is recorded into the erecord databases as the
:ref:`dm_vlevpz` whose Id is :ref:`wwdm_vpz_id`.

More
====

*Next step :* :ref:`wwdm_edit` | :ref:`wwdm_run` | :ref:`wwdm_both`

*Back to :* :ref:`erecord_examples`

