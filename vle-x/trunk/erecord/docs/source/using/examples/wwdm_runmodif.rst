.. _wwdm_runmodif:

==============================
Running the modified simulator
==============================

Introduction
============

Running the modified simulator corresponds with the :ref:`post_vpz_output` resource.

You may find here : :ref:`help to know how some information is identified/selected in the request <wwdm_ident_fields>`.

A quick overview example
========================

    .. include:: include/wwdm_runmodif_quickoverview_tree.rst

*or lighter* :

    .. include:: include/wwdm_runmodif_quickoverview.rst

Modifications
=============

The different possible modifications are illustrated bellow.

.. _choosing_the_simulation_running_mode:

Choosing the simulation running mode
------------------------------------
*For more see* :ref:`post_vpz_output`.

You can choose the simulation running mode, through values for plan and
restype (see :ref:`webapi_opt_plan_restype`).

    .. include:: include/wwdm_runmodif_quickoverview_tree.rst

    .. include:: include/wwdm_runmodif_quickoverview.rst

    -> *Example* : :ref:`In a webbrowser <erecord_use_capture_021>`

    -> *Example* : :ref:`In a webbrowser <erecord_use_capture_022>`

    -> *Example* : :ref:`In a webbrowser <erecord_use_capture_023>`

    -> *Example* : :ref:`In a webbrowser <erecord_use_capture_024>`

Modifying begin and duration of simulation
------------------------------------------
*For more see* :ref:`post_vpz_output`.

    .. include:: include/wwdm_runmodif_quickoverview_tree.rst

    .. include:: include/wwdm_runmodif_quickoverview.rst

    -> *Example* : :ref:`In a webbrowser <erecord_use_capture_020>`

.. _modifying_some_parameters:

Modifying some parameters
-------------------------
*For more see* :ref:`post_vpz_output`.

To modify the simulator parameters, it is necessary to know their information 
identification ('cname', 'pname'...) : see :ref:`wwdm_ident_parameters`.

A parameter can be modified by two different ways : with 'pars' or
'cname.pname' (see :ref:`webapi_opt_pars`).

Modifying parameters with 'cname.pname'
+++++++++++++++++++++++++++++++++++++++

    .. include:: include/wwdm_runmodif_quickoverview.rst

    -> *Example* : :ref:`In a webbrowser <erecord_use_capture_020>`

Modifying parameters with 'pars'
++++++++++++++++++++++++++++++++

    .. include:: include/wwdm_runmodif_quickoverview_tree.rst

Choosing the restituted output datas
------------------------------------
*For more see* :ref:`post_vpz_output`.

You can choose the returned output datas, ask for filtering some of them (see :ref:`webapi_opt_outselect`).

To select output datas or views (sets of output datas) to be returned, it is
necessary to know its information identification ('selection_name') : see
:ref:`wwdm_ident_outputs`.

    .. include:: include/wwdm_runmodif_quickoverview_tree.rst

    .. include:: include/wwdm_runmodif_quickoverview.rst

    -> *Example* : :ref:`In a webbrowser <erecord_use_capture_020>`

    -> *Example* : :ref:`In a webbrowser <erecord_use_capture_021>`

    -> *Example* : :ref:`In a webbrowser <erecord_use_capture_022>`

Choosing style of presentation 
------------------------------
*For more see* :ref:`post_vpz_output`.

You can choose your style of presentation (see :ref:`webapi_opt_style`)
between 'link', 'tree', 'compact' (or 'compactlist').

    .. include:: include/wwdm_runmodif_quickoverview_tree.rst

    .. include:: include/wwdm_runmodif_quickoverview.rst

    -> *Example* : :ref:`In a webbrowser <erecord_use_capture_025>`

    -> *Example* : :ref:`In a webbrowser <erecord_use_capture_026>`

Multiple simulation case
========================

It consists in :

    - choosing 'linear' as plan

      *(see* :ref:`choosing_the_simulation_running_mode` *above)*.

    - modifying some parameters giving them multiple values

      *(see* :ref:`modifying_some_parameters` *above)*.

    .. include:: include/wwdm_runmodif_quickoverview_tree.rst

    .. include:: include/wwdm_runmodif_quickoverview.rst

.. note::
   For a multiple simulation composed by N simulations, each parameter has
   exactly N values or only 1 value. If not, the simulation fails.
 
More
====

*Back to :* :ref:`wwdm_run` | :ref:`erecord_examples`

