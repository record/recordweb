
.. _wwdm_runasis:

===========================
Running the simulator as is
===========================

Running the simulator as is corresponds with the :ref:`get_vpz_output` resource.

    ... under construction ...

More
====

*Back to :* :ref:`wwdm_run` | :ref:`erecord_examples`

