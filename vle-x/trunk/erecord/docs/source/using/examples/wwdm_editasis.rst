.. _wwdm_editasis:

==========================
Seeing the simulator as is
==========================

Seeing the simulator as is corresponds with the :ref:`get_vpz_input` resource.

In a webbrowser
===============

For the simulator 'wwdm.vpz' whose Id is :ref:`wwdm_vpz_id`, enter in a webbrowser the URL :

    http://erecord.toulouse.inra.fr:8000/vpz/input/?vpz=266

In fact you have the choice between different styles of presentation (see :ref:`webapi_opt_style`), different formats (see :ref:`webapi_opt_format`) :

    http://erecord.toulouse.inra.fr:8000/vpz/input/?vpz=266&mode=link&format=api
    http://erecord.toulouse.inra.fr:8000/vpz/input/?vpz=266&mode=tree&format=api
    http://erecord.toulouse.inra.fr:8000/vpz/input/?vpz=266&mode=compact&format=api
    http://erecord.toulouse.inra.fr:8000/vpz/input/?vpz=266&mode=compactlist&format=api


    http://erecord.toulouse.inra.fr:8000/vpz/input/?vpz=266&mode=compactlist&format=json
    http://erecord.toulouse.inra.fr:8000/vpz/input/?vpz=266&mode=compact&format=yaml
    http://erecord.toulouse.inra.fr:8000/vpz/input/?vpz=266&mode=tree&format=html
    http://erecord.toulouse.inra.fr:8000/vpz/input/?vpz=266&mode=link&format=xml

More
====

*Back to :* :ref:`wwdm_edit` | :ref:`erecord_examples`

