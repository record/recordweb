.. _wwdm_homepage:

==================================
A html home page for the simulator
==================================

*The erecord web services used in this part are some of the ‘vpz’ web
services* (see :ref:`webapi_vpz`).

A `html home page <http://147.100.179.168/docs/models/wwdm/accueil_wwdm.html>`_
*(fr)* has been written/built for the WWDM model, more precisely for its
simulator ‘wwdm.vpz’ whose Id is :ref:`wwdm_vpz_id`
(:ref:`more <wwdm_vpz_id>`).

This `home page <http://147.100.179.168/docs/models/wwdm/accueil_wwdm.html>`_
*(fr)* contains some examples of customization :

  - Some information has been added to help the user : parameters definition, unit...
  - Some parameters are shown, some others are hidden, some others readonly.
  - The html code controls some values chosen by the user (min and max values...).
  - ...

**Acces to the html page** : `a home page for WWDM <http://147.100.179.168/docs/models/wwdm/accueil_wwdm.html>`_ *(fr)*.

.. note::
   There can exist for the WWDM model as many such html home pages as use
   cases of it.

.. note::
   You can save 
   `the page <http://147.100.179.168/docs/models/wwdm/accueil_wwdm.html>`_
   *(fr)* and modify it as you want according to your own use case, then keep
   it for yourself or share it, that is to say send it to some colleagues.

More
====

*Back to :* :ref:`erecord_examples`

