
-> *Example* :
:ref:`In a webbrowser <erecord_use_capture_036>` |
:ref:`In command line with cURL <erecord_use_capture_136>` |
:ref:`In Python language <erecord_use_capture_236>` |
:ref:`In R language <erecord_use_capture_336>` |
:ref:`In PHP language <erecord_use_capture_436>` |
:ref:`In C++ language <erecord_use_capture_536>`

