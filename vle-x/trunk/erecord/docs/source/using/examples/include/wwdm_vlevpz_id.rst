* simulator 'wwdm.vpz' (vle vpz) into the erecord databases :

    ...
    {
        "id": 266, 
        "verbose_name": "'wwdm.vpz' (from 'wwdm' (from 'recordb'))", 
        "name": "wwdm.vpz", 
        "vlepkg": {
            "id": 41, 
            "name": "wwdm", 
            "verbose_name": "Winter Wheat Dry Matter (from 'recordb')", 
            "vlerep": {
                "id": 2, 
                "name": "recordb", 
                "verbose_name": "recordb (models)", 
                "path": "/opt/erecord/repositories/recordb"
            }
        }
    } 
    ...

