.. _erecord_use_capture_012:

================================================
Example of :ref:`post_vpz_input` in a webbrowser
================================================

    .. include:: wwdm_wb_intro_postinput.rst

Example
=======

Example illustrating :

  - '**all**' as 'outselect'
  - '**compact**' as style of presentation
  - with 'application/x-www-form-**urlencoded**' as 'Media type'

:download:`REQUEST <capture_request_012.png>`

    *Memo*

    .. literalinclude:: memo_request_012.rst

Example
=======

Example illustrating :

  - '**all**' as 'outselect'
  - '**compact**' as style of presentation
  - with 'application/**json**' as 'Media type'

:download:`REQUEST <capture_request_013.png>` 

    *Memo*

    .. literalinclude:: memo_request_013.rst






