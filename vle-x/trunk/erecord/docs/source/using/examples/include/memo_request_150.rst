
cURL command lines for all the models repositories in 'tree' or 'link' style :

curl -G http://erecord.toulouse.inra.fr:8000/db/rep/ -d "mode=tree"
curl -G http://erecord.toulouse.inra.fr:8000/db/rep/ -d "mode=link"

cURL command lines for the models repository with id=2 ("recordb") in 'tree' or 'link' style :

curl -G http://erecord.toulouse.inra.fr:8000/db/rep/2/ -d "mode=tree"
curl -G http://erecord.toulouse.inra.fr:8000/db/rep/2/ -d "mode=link"

cURL command lines for all the models of the models repository with id=2 ("recordb") in 'tree' or 'link' style :

curl -G http://erecord.toulouse.inra.fr:8000/db/pkg/ -d "mode=tree&rep=2"
curl -G http://erecord.toulouse.inra.fr:8000/db/pkg/ -d "mode=link&rep=2"

cURL command lines for the model with id=19 ("2CV") in 'tree' or 'link' style :

curl -G http://erecord.toulouse.inra.fr:8000/db/pkg/19/ -d "mode=tree"
curl -G http://erecord.toulouse.inra.fr:8000/db/pkg/19/ -d "mode=link"

cURL command lines for all the simulators of the model with id=19 ("2CV") in 'tree' or 'link' style :

curl -G http://erecord.toulouse.inra.fr:8000/db/vpz/ -d "mode=tree&pkg=19"
curl -G http://erecord.toulouse.inra.fr:8000/db/vpz/ -d "mode=link&pkg=19"

cURL command lines for the model with id=41 ("wwdm") in 'tree' or 'link' style :

curl -G http://erecord.toulouse.inra.fr:8000/db/pkg/41/ -d "mode=tree"
curl -G http://erecord.toulouse.inra.fr:8000/db/pkg/41/ -d "mode=link"

cURL command lines for the simulator with id=266 ("wwdm.vpz") in 'link' or 'tree' style :

curl -G http://erecord.toulouse.inra.fr:8000/db/vpz/266/ -d "mode=link"
curl -G http://erecord.toulouse.inra.fr:8000/db/vpz/266/ -d "mode=tree"

