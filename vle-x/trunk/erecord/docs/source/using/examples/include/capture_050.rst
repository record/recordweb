.. _erecord_use_capture_050:

=======================================================================
Example of some :ref:`'db' resources <webapi_db>` calls in a webbrowser
=======================================================================

The following resources are used in this example :
  - :ref:`get_db_rep`, :ref:`get_db_rep_id`
  - :ref:`get_db_pkg`, :ref:`get_db_pkg_id`
  - :ref:`get_db_vpz`, :ref:`get_db_vpz_id`

Enter in a webbrowser the URLs given bellow. The request parameters described for the first of them (:ref:`style of presentation <webapi_opt_style>` and :ref:`format <webapi_opt_format>`) can be used in the same way (same values) with all of the other ones.

- URLs for all the models repositories : http://erecord.toulouse.inra.fr:8000/db/rep/

  you can choose :ref:`style of presentation <webapi_opt_style>` and :ref:`format <webapi_opt_format>` :

  - In '**tree**' style of presentation :

=================== ===================================================================
'**api**' format    http://erecord.toulouse.inra.fr:8000/db/rep/?mode=tree&format=api 
------------------- -------------------------------------------------------------------
'**json**'' format  http://erecord.toulouse.inra.fr:8000/db/rep/?mode=tree&format=json
------------------- -------------------------------------------------------------------
'**yaml**' format   http://erecord.toulouse.inra.fr:8000/db/rep/?mode=tree&format=yaml
------------------- -------------------------------------------------------------------
'**xml**' format    http://erecord.toulouse.inra.fr:8000/db/rep/?mode=tree&format=xml 
------------------- -------------------------------------------------------------------
'**html**' format   http://erecord.toulouse.inra.fr:8000/db/rep/?mode=tree&format=html
=================== ===================================================================

  - In '**link**' style of presentation :

=================== ===================================================================
'**api**' format    http://erecord.toulouse.inra.fr:8000/db/rep/?mode=link&format=api
------------------- -------------------------------------------------------------------
'**json**'' format  http://erecord.toulouse.inra.fr:8000/db/rep/?mode=link&format=json
------------------- -------------------------------------------------------------------
'**yaml**' format   http://erecord.toulouse.inra.fr:8000/db/rep/?mode=link&format=yaml
------------------- -------------------------------------------------------------------
'**xml**' format    http://erecord.toulouse.inra.fr:8000/db/rep/?mode=link&format=xml
------------------- -------------------------------------------------------------------
'**html**' format   http://erecord.toulouse.inra.fr:8000/db/rep/?mode=link&format=html
=================== ===================================================================

- URLs for the models repository with id=2 ("recordb") :
  http://erecord.toulouse.inra.fr:8000/db/rep/2/

- URLs for all the models of the models repository with id=2 ("recordb") :
  http://erecord.toulouse.inra.fr:8000/db/pkg/?rep=2

- URLs for the model with id=19 ("2CV") :
  http://erecord.toulouse.inra.fr:8000/db/pkg/19/

- URLs for all the simulators of the model with id=19 ("2CV") :
  http://erecord.toulouse.inra.fr:8000/db/vpz/?pkg=19

- URLs for the model with id=41 ("wwdm") :
  http://erecord.toulouse.inra.fr:8000/db/pkg/41/

- URLs for the simulator with id=266 ("wwdm.vpz") :
  http://erecord.toulouse.inra.fr:8000/db/vpz/266/

