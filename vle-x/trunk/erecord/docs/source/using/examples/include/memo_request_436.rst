# PHP code :

#######################
# request and response
#######################

$inputdata = ["vpz"=>266, "mode"=>"compact", "format"=>"json"];
$inputdata['parselect'] = "all";
$inputdata['outselect'] = "all";

$responsedata = send_post_and_receive($url="http://erecord.toulouse.inra.fr:8000/vpz/input/", $inputdata=$inputdata);

##########################################################
# responsedata in case of 'compact' style of presentation
##########################################################

# duration in 'compact' style of presentation
if (array_key_exists("duration", $responsedata)){
    $duration_value = $responsedata['duration'];
    print ("duration value : ".$duration_value."<br />");
}

# begin in 'compact' style of presentation
if (array_key_exists('begin', $responsedata)){
    $begin_value = $responsedata['begin'];
    print ("begin value : ".$begin_value."<br />");
}

# parameters in 'compact' style of presentation
if (array_key_exists('pars', $responsedata)){
    $pars = $responsedata['pars'];
    print ("parameters (selection_name and value, condition name and parameter name) : <br />");
    foreach ($pars as $par){
        $par_selection_name = $par['selection_name'];
        $par_cname = $par['cname'];
        $par_pname = $par['pname'];
        $par_value = $par['value'];
        print ($par_selection_name." ".$par_value.", ".$par_cname." ".$par_pname."<br />");
    }
}

# conditions identity in 'compact' style of presentation
if (array_key_exists('conds', $responsedata)){
    $conds = $responsedata['conds'];
    foreach ($conds as $cond_selection_name){
        $cond_selection_name = $cond_selection_name['selection_name'];
        print ("condition selection_name : ".$cond_selection_name."<br />");
    }
}

# views identity in 'compact' style of presentation
if (array_key_exists('views', $responsedata)){
    $views = $responsedata['views'];
    foreach ($views as $view_selection_name){
        $view_selection_name = $view_selection_name['selection_name'];
        print ("view selection_name : ".$view_selection_name."<br />");
    }
}

# output datas identity in 'compact' style of presentation
if (array_key_exists('outs', $responsedata)){
    $outs = $responsedata['outs'];
    foreach ($outs as $out_selection_name){
        $out_selection_name = $out_selection_name['selection_name'];
        print ("output data selection_name : ".$out_selection_name."<br />");
    }
}

