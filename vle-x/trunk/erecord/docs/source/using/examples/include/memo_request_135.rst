
cURL command line (case of single plan (single simulation)) :

curl -L -H "Content-Type: application/json" -d '{"vpz":266, "duration":6, "begin":2453982.0, "cond_wwdm.A":0.0064, "cond_wwdm.Eb":1.86, "mode":["compact","single","dataframe"], "outselect":["view.top:wwdm.LAI", "view.top:wwdm.ST"]}' http://erecord.toulouse.inra.fr:8000/vpz/output/

cURL command line (case of linear plan (multiple simulation)) :

curl -L -H "Content-Type: application/json" -d '{"vpz":266, "duration":6, "begin":2453982.0, "cond_wwdm.A":[0.0064,0.0065,0.0066], "cond_wwdm.Eb":[1.84,1.85,1.86], "mode":["compact","linear","dataframe"], "outselect":["view.top:wwdm.LAI", "view.top:wwdm.ST"]}' http://erecord.toulouse.inra.fr:8000/vpz/output/

(rq : or with "matrix" instead of "dataframe")

