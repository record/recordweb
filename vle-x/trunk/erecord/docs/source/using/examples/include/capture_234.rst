.. _erecord_use_capture_234:

===================================================
Example of :ref:`post_vpz_input` in Python language
===================================================

    .. include:: wwdm_python_intro.rst

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters with '**pars**',

  - value '**all**' as '**parselect**',

  - '**tree**' as style of presentation,
  - '**json**' as format,

  - with 'application/**json**' as 'Content-Type'

    *Memo*

    .. literalinclude:: ../../../../../erecord/apps/erecord_cmn/utils/using/send_post_and_receive.py

    .. literalinclude:: ../../../../../erecord/apps/erecord_cmn/utils/using/content_simulation_inputs_tree.py

    .. literalinclude:: memo_request_234.rst

