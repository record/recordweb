.. _erecord_use_capture_332:

===============================================
Example of :ref:`post_vpz_output` in R language
===============================================

    .. include:: wwdm_r_intro.rst

.. to do
  - modifying some parameters by '**pars**',
  instead of
  - modifying some parameters by '**cname.pname**',

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - '**tree**' as style of presentation,
  - '**json**' as format,

  - value '**single**' as plan, or
    value '**linear**' in case of  multiple simulation
  - value '**dataframe**' as restype *(or 'matrix')*

  - value '**all**' as '**outselect**',

  - with 'application/**json**' as 'Content-Type'

    *Memo*

    .. literalinclude:: memo_request_301_tree.rst
    .. literalinclude:: memo_request_332.rst

