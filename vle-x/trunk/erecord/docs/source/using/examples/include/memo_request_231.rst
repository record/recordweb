# Python code :

from __future__ import print_function # python 2.7 version case

#######################
# request and response
#######################

inputdata = {"vpz":266, "mode":"compactlist", "format":"json"}
inputdata["duration"] = 6
inputdata["begin"] = 2453982.0

# some parameters modification with 'cname.pname'
inputdata["cond_wwdm.A"] = 0.0064
inputdata["cond_wwdm.Eb"] = 1.86

inputdata["parselect"] = ["cond_wwdm.A", "cond_wwdm.B", "cond_wwdm.Eb",
                          "cond_wwdm.TI"]

responsedata = send_post_and_receive(
    url="http://erecord.toulouse.inra.fr:8000/vpz/input/",
    inputdata=inputdata)

responsedata
keys = responsedata.keys()
keys

##############################################################
# responsedata in case of 'compactlist' style of presentation 
##############################################################

# duration in 'compactlist' style of presentation
if "duration" in keys :
    duration_value = responsedata['duration']
    print("duration value : ", duration_value)

# begin in 'compactlist' style of presentation
if "begin" in keys :
    begin_value = responsedata['begin']
    print("begin value : ", begin_value)

# parameters in 'compactlist' style of presentation
for par_selection_name in ('cond_wwdm.B',
                           'cond_wwdm.Eb',
                           'cond_wwdm.TI',
                           'cond_wwdm.A'):
    if par_selection_name in keys :
        par_value= json.loads(responsedata[par_selection_name])
        print("parameter selection_name and value :", par_selection_name, par_value)

# conditions identity in 'compactlist' style of presentation
if "conds" in keys :
    conds = responsedata['conds']
    for cond_selection_name in conds :
        print("condition selection_name : ", cond_selection_name)

# views identity in 'compactlist' style of presentation
if "views" in keys :
    views = responsedata['views']
    for view_selection_name in views :
        print("view selection_name : ", view_selection_name)

# output datas identity in 'compactlist' style of presentation
if "outs" in keys :
    outs = responsedata['outs']
    for out_selection_name in outs :
        print("output data selection_name : ", out_selection_name)

pass

