# PHP code :

#######################
# request and response
#######################

$inputdata = ["vpz"=>266, "mode"=>"compactlist", "format"=>"json"];
$inputdata["duration"] = 6;
$inputdata["begin"] = 2453982.0;

# some parameters modification with 'cname.pname'
$inputdata["cond_wwdm.A"] = 0.0064;
$inputdata["cond_wwdm.Eb"] = 1.86;

$inputdata["parselect"] = ["cond_wwdm.A", "cond_wwdm.B", "cond_wwdm.Eb", "cond_wwdm.TI"];

$responsedata = send_post_and_receive($url="http://erecord.toulouse.inra.fr:8000/vpz/input/", $inputdata=$inputdata);


##############################################################
# responsedata in case of 'compactlist' style of presentation 
##############################################################

# duration in 'compactlist' style of presentation
if (array_key_exists('duration', $responsedata)){
    $duration_value = $responsedata['duration'];
    print ("duration value : ".$duration_value."<br />");
}
# begin in 'compactlist' style of presentation
if (array_key_exists('begin', $responsedata)){
    $begin_value = $responsedata['begin'];
    print ("begin value : ".$begin_value."<br />");
}

# parameters in 'compactlist' style of presentation
foreach (array('cond_wwdm.B','cond_wwdm.Eb','cond_wwdm.TI','cond_wwdm.A') as $par_selection_name){
    if (array_key_exists($par_selection_name, $responsedata)){
        $par_value= $responsedata[$par_selection_name];
        print_r ("parameter selection_name and value : ".$par_selection_name."\n".$par_value."<br />");
    }
}

# conditions identity in 'compactlist' style of presentation
if (array_key_exists('conds', $responsedata)){
    $conds = $responsedata['conds'];
    foreach ($conds as $cond_selection_name){
        print ("condition selection_name : ".$cond_selection_name."<br />");
    }
}

# views identity in 'compactlist' style of presentation
if (array_key_exists('views', $responsedata)){
    $views = $responsedata['views'];
    foreach ($views as $view_selection_name){
        print ("view selection_name : ".$view_selection_name."<br />");
    }
}

# output datas identity in 'compactlist' style of presentation
if (array_key_exists('outs', $responsedata)){
    $outs = $responsedata['outs'];
    foreach ($outs as $out_selection_name){
        print ("output data selection_name : ".$out_selection_name."<br />");
    }
}

