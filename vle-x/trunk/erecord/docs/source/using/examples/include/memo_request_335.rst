# R code :

library('RCurl')
library('rjson')

#######################
# request and response
#######################

header = c('Content-Type'='application/json', Accept='application/json')

options(RCurlOptions=list(followlocation=TRUE))

# case of single plan (single simulation)
postfields = toJSON(list(
    vpz=266,
    duration=6,
    begin=2453982.0,
    cond_wwdm.A=0.0064,
    cond_wwdm.Eb=1.86,
    mode=c("compact", "single", "dataframe"),
    # or mode=c("compact", "single", "matrix"),
    outselect=c("view.top:wwdm.LAI", "view.top:wwdm.ST"),
    format="json"))

# case of linear plan (multiple simulation)
postfields = toJSON(list(
    vpz=266,
    duration=6,
    begin=2453982.0,
    cond_wwdm.A=c(0.0064,0.0065,0.0066),
    cond_wwdm.Eb=c(1.84,1.85,1.86),
    mode=c("compact", "linear", "dataframe"),
    # or mode=c("compact", "linear", "matrix"),
    outselect=c("view.top:wwdm.LAI", "view.top:wwdm.ST"),
    format="json"))

res = postForm(uri="http://erecord.toulouse.inra.fr:8000/vpz/output/",
    .opts=list(postfields=postfields, httpheader=header))

responsedata = fromJSON(res)
#responsedata

##########################################################
# responsedata in case of 'compact' style of presentation
##########################################################

content_simulation_results_compact( res=responsedata$res,
                                    plan=responsedata$plan,
                                    restype=responsedata$restype)

