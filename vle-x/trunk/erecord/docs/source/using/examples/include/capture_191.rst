.. _erecord_use_capture_191:

===========================================================
Example of authenticated requests in command line with cURL
===========================================================

    Enter the cURL commands in a command line window.

Example illustrating requests about a simulator in limited access, where an available JWT value is required, as a :ref:`webapi_opt_jwt` parameter of the requests.

    *Memo*

    .. literalinclude:: memo_request_191.rst

