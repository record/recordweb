.. _erecord_use_capture_336:

==============================================
Example of :ref:`post_vpz_input` in R language
==============================================

    .. include:: wwdm_r_intro.rst

Example illustrating :

  - '**compact**' as style of presentation,
  - '**json**' as format,
  
  - value '**all**' as '**parselect**'
    (to receive information of all the existing parameters and conditions)
  
  - value '**all**' as '**outselect**'
    (to receive information of all the existing output datas and views)

  - with 'application/**json**' as 'Content-Type'

    *Memo*

    .. literalinclude:: memo_request_336.rst

