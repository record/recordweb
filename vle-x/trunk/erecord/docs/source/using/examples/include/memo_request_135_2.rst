-------------------------------------------------------------------------------
- Content-Type: application/x-www-form-urlencoded
- single plan (mode)
- dataframe restype (mode)
- format yaml

curl -H "Content-Type: application/x-www-form-urlencoded" -d 'vpz=266&duration=6&begin=2453982.0&cond_wwdm.A=0.0064&cond_wwdm.Eb=1.86&mode=compact&plan=single&restype=dataframe&outselect=view.top:wwdm.LAI&outselect=view.top:wwdm.ST&format=yaml' http://erecord.toulouse.inra.fr:8000/vpz/output/

-------------------------------------------------------------------------------
- Content-Type: application/x-www-form-urlencoded
- linear plan (mode)
- dataframe restype (mode)
- format yaml

curl -H "Content-Type: application/x-www-form-urlencoded" -d 'vpz=266&duration=6&begin=2453982.0&cond_wwdm.A=[0.0064,0.0065,0.0066]&cond_wwdm.Eb=[1.84,1.85,1.86]&mode=compact&plan=linear&restype=dataframe&outselect=view.top:wwdm.LAI&outselect=view.top:wwdm.ST&format=yaml' http://erecord.toulouse.inra.fr:8000/vpz/output/

-------------------------------------------------------------------------------

