.. _erecord_use_capture_134:

==========================================================
Example of :ref:`post_vpz_input` in command line with cURL
==========================================================

    .. include:: wwdm_curl_intro.rst

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**pars**',

  - value '**all**' as '**parselect**',

  - '**tree**' as style of presentation,

  - with 'application/**json**' as 'Content-Type'

:download:`REQUEST and RESPONSE<capture_request_134.png>`

    *Memo*

    .. literalinclude:: memo_request_134.rst

