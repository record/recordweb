# Python code :

from __future__ import print_function # python 2.7 version case

#######################
# request and response
#######################

inputdata = {"vpz":266, "mode":"compact", "format":"json"}
inputdata['parselect'] = "all"
inputdata['outselect'] = "all"

responsedata = send_post_and_receive(
    url="http://erecord.toulouse.inra.fr:8000/vpz/input/",
    inputdata=inputdata)

responsedata
keys = responsedata.keys()
keys

##########################################################
# responsedata in case of 'compact' style of presentation 
##########################################################

# duration in 'compact' style of presentation
if "duration" in keys :
    duration_value = responsedata['duration']
    print("duration value : ", duration_value)

# begin in 'compact' style of presentation
if "begin" in keys :
    begin_value = responsedata['begin']
    print("begin value : ", begin_value)

# parameters in 'compact' style of presentation 
if "pars" in keys :
    pars = responsedata['pars']
    print("parameters (selection_name and value, condition name and parameter name) :")
    for par in pars :
        #par.keys()
        par_selection_name = par['selection_name']
        par_cname = par['cname']
        par_pname = par['pname']
        par_value = par['value']
        print(par_selection_name, par_value, ",", par_cname, par_pname)

# conditions identity in 'compact' style of presentation
if "conds" in keys :
    conds = responsedata['conds']
    for cond in conds :
        cond_selection_name = cond['selection_name']
        print("condition selection_name : ", cond_selection_name)

# views identity in 'compact' style of presentation
if "views" in keys :
    views = responsedata['views']
    for view in views :
        view_selection_name = view['selection_name']
        print("view selection_name : ", view_selection_name)

# output datas identity in 'compact' style of presentation
if "outs" in keys :
    outs = responsedata['outs']
    for out in outs :
        out_selection_name = out['selection_name']
        print("output data selection_name : ", out_selection_name)

pass

