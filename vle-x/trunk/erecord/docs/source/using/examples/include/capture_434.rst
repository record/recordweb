.. _erecord_use_capture_434:

================================================
Example of :ref:`post_vpz_input` in PHP language
================================================

    .. include:: wwdm_php_intro.rst

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters with '**pars**',

  - value '**all**' as '**parselect**',

  - '**tree**' as style of presentation,
  - '**json**' as format,

  - with 'application/**json**' as 'Content-Type'

    *Memo*

    .. literalinclude:: memo_request_400.rst
    .. literalinclude:: memo_request_402_tree__version434.rst
    .. literalinclude:: memo_request_434.rst

.. NB: memo_request_402_tree.rst replaced (temporary) by xxx__version434.rst

