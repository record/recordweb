.. _erecord_use_capture_230:

===================================================
Example of :ref:`post_vpz_inout` in Python language
===================================================

    .. include:: wwdm_python_intro.rst

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - '**tree**' as style of presentation,
  - '**json**' as format,

  - values of type *cname.pname* as '**parselect**' :

    value 'cond_wwdm.A' to select the parameter named 'A' of the condition
    named 'cond_wwdm'.

    value 'cond_wwdm.B' to select the parameter named 'B' of the condition
    named 'cond_wwdm'.

    value 'cond_wwdm.Eb' to select the parameter named 'Eb' of the condition
    named 'cond_wwdm'.

    value 'cond_wwdm.TI' to select the parameter named 'TI' of the condition
    named 'cond_wwdm'.


  - value '**single**' as plan
  - value '**dataframe**' as restype

  - value of type *vname.oname* as '**outselect**' :

    value 'view.top:wwdm.LAI' to select the 'LAI' output data of the view
    named 'view'.

    value 'view.top:wwdm.ST' to select the 'ST' output data of the view
    named 'view'.

  - with 'application/**json**' as 'Content-Type'

    *Memo*

    .. literalinclude:: ../../../../../erecord/apps/erecord_cmn/utils/using/send_post_and_receive.py

    .. literalinclude:: ../../../../../erecord/apps/erecord_cmn/utils/using/content_simulation_inputs_tree.py

    .. literalinclude:: ../../../../../erecord/apps/erecord_cmn/utils/using/content_simulation_results_tree.py

    .. literalinclude:: memo_request_230.rst

