.. _erecord_use_capture_135_3:

===========================================================
Example of :ref:`post_vpz_output` in command line with cURL
===========================================================

    .. include:: wwdm_curl_intro.rst

Example illustrating :

  - modifying the input datas folder by using datafolder and datafoldercopy.

*Memo*

    .. literalinclude:: memo_request_datafolder.rst

    .. literalinclude:: memo_request_135_3.rst

