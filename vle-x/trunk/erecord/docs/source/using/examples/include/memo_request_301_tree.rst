# R code :

library('rjson')

# content of simulation results (res) in 'tree' style of presentation,
# according to plan values ('single', 'linear') and
# restype values ('dataframe' or 'matrix')
content_simulation_results_tree <- function(res, plan, restype){

    res = fromJSON(res)
    cat( "plan, restype :", plan, restype, "\n")
    #cat( "res :", "\n")
    #print(res)
    cat("\nDetailing the results :", "\n")
    if (restype=='dataframe' && plan=='single'){
        cat("(view name, output data name, value)", "\n")
        for (viewname in names(res)){ 
            outputs = res[[viewname]]
            for (outputname in names(outputs)){ 
                val = outputs[[outputname]]
                cat("\n- ", viewname, outputname, "\n")
                print(val)
            }
        }
    } else if (restype=='dataframe' && plan=='linear'){
        for (res_a in res){
            cat("\n*** simulation :", "\n")
            cat("(view name, output data name, value)", "\n")
            for (viewname in names(res_a)){ 
                outputs = res_a[[viewname]]
                for (outputname in names(outputs)){ 
                    val = outputs[[outputname]]
                    cat("\n- ", viewname, outputname, "\n")
                    print(val)
                }
            }
        }
    } else if (restype=='matrix' && plan=='single'){
        cat("(view name, value)", "\n")
        for (viewname in names(res)){ 
            v = res[[viewname]]
            cat("\n- ", viewname, "\n")
            print(v)
        }
    } else if (restype=='matrix' && plan=='linear'){
        for (res_a in res){
            cat("\n*** simulation :", "\n")
            cat("(view name, value)", "\n")
            for (viewname in names(res_a)){ 
                v = res_a[[viewname]]
                cat("\n- ", viewname, "\n")
                print(v)
            }
        }
    } else {
        cat("error (unexpected)", "\n")
    }
}

