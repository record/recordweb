.. _erecord_use_capture_022:

=================================================
Example of :ref:`post_vpz_output` in a webbrowser
=================================================

    .. include:: wwdm_wb_intro_postoutput.rst

Example illustrating :

  - modifying **duration**,
  - value '**linear**' as plan
  - value '**dataframe**' as restype

  - value of type *vname.oname* as '**outselect**' :

    value 'view.top:wwdm.LAI' to select the 'LAI' output data of the view
    named 'view'.

    value 'view.top:wwdm.ST' to select the 'ST' output data of the view
    named 'view'.

  - with 'application/x-www-form-**urlencoded**' as 'Media type'

    *Memo*

    .. literalinclude:: memo_request_022.rst

