
cURL command lines :

curl -H "Content-Type: application/json" -d '{"vpz":266,"duration":4,"cond_wwdm.TI":901.0,"parselect":["cond_wwdm.A","cond_wwdm.B","cond_wwdm.TI"],"outselect":["view.top:wwdm.LAI","view.top:wwdm.U"],"mode":["todownload","single","dataframe"],"format":"yaml","report":"all"}' http://erecord.toulouse.inra.fr:8000/vpz/report/

curl -H "Content-Type: application/json" -d '{"vpz":266,"duration":4,"cond_wwdm.TI":[899.0,900.0,901.0],"parselect":["cond_wwdm.A","cond_wwdm.B","cond_wwdm.TI"],"outselect":["view.top:wwdm.LAI","view.top:wwdm.U"],"mode":["todownload","linear","dataframe"],"format":"yaml"}' http://erecord.toulouse.inra.fr:8000/vpz/report/

curl -H "Content-Type: application/x-www-form-urlencoded" -d "vpz=266&duration=4&cond_wwdm.TI=901.0&parselect=cond_wwdm.A&parselect=cond_wwdm.B&parselect=cond_wwdm.TI&outselect=view.top:wwdm.LAI&outselect=view.top:wwdm.U&mode=todownload&mode=single&mode=dataframe&format=yaml&report=all" http://erecord.toulouse.inra.fr:8000/vpz/report/

curl -H "Content-Type: application/x-www-form-urlencoded" -d "vpz=266&duration=4&cond_wwdm.TI=[899.0,900.0,901.0]&parselect=cond_wwdm.A&parselect=cond_wwdm.B&parselect=cond_wwdm.TI&outselect=view.top:wwdm.LAI&outselect=view.top:wwdm.U&mode=todownload&mode=linear&mode=dataframe&format=yaml" http://erecord.toulouse.inra.fr:8000/vpz/report/

