
-> *Example* :
:ref:`In a webbrowser <erecord_use_capture_035>` |
:ref:`In command line with cURL <erecord_use_capture_135>` |
:ref:`In Python language <erecord_use_capture_235>` |
:ref:`In R language <erecord_use_capture_335>` |
:ref:`In PHP language <erecord_use_capture_435>` |
:ref:`In C++ language <erecord_use_capture_535>`

