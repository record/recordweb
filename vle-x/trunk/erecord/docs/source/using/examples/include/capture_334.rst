.. _erecord_use_capture_334:

==============================================
Example of :ref:`post_vpz_input` in R language
==============================================

    .. include:: wwdm_r_intro.rst

.. to do
  - modifying some parameters by '**pars**',
  instead of
  - modifying some parameters by '**cname.pname**',

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - value '**all**' as '**parselect**',

  - '**tree**' as style of presentation,
  - '**json**' as format,

  - with 'application/**json**' as 'Content-Type'

    *Memo*

    .. literalinclude:: memo_request_302_tree.rst
    .. literalinclude:: memo_request_334.rst

