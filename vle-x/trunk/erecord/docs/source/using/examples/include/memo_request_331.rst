
# R code :

library('RCurl')
library('rjson')

#######################
# request and response
#######################

header = c('Content-Type'='application/json', Accept='application/json')

options(RCurlOptions=list(followlocation=TRUE))

postfields = toJSON(list(
    vpz=266,
    duration=6,
    begin=2453982.0,
    cond_wwdm.A=0.0064,
    cond_wwdm.Eb=1.86,
    mode="compactlist",
    parselect=c("cond_wwdm.A", "cond_wwdm.B", "cond_wwdm.Eb", "cond_wwdm.TI"),
    format="json"))

res = postForm(uri="http://erecord.toulouse.inra.fr:8000/vpz/input/",
    .opts=list(postfields=postfields, httpheader=header))

responsedata = fromJSON(res)
#responsedata

##############################################################
# responsedata in case of 'compactlist' style of presentation 
##############################################################

# duration in 'compactlist' style of presentation
duration_value = responsedata$duration
cat("duration value :", duration_value, "\n")

# begin in 'compactlist' style of presentation
begin_value = responsedata$begin
cat("begin value :", begin_value, "\n")

# parameters in 'compactlist' style of presentation
for (par_selection_name in c('cond_wwdm.B', 'cond_wwdm.Eb',
                             'cond_wwdm.TI', 'cond_wwdm.A')){
    par_value= fromJSON(responsedata[[par_selection_name]])
    cat("Parameter selection_name and value :", par_selection_name, "\n")
    print(par_value)
}

# conditions identity in 'compactlist' style of presentation
conds = responsedata$conds
for (cond_selection_name in conds){
    cat("Condition selection_name : ", cond_selection_name, "\n")
}

# views identity in 'compactlist' style of presentation
views = responsedata$views
for (view_selection_name in views){
    cat("View selection_name : ", view_selection_name, "\n")
}

# output datas identity in 'compactlist' style of presentation
outs = responsedata$outs
for (out_selection_name in outs){
    cat("Output data selection_name : ", out_selection_name, "\n")
}

