
# R code :

library('RCurl')
library('rjson')

#######################
# request and response
#######################

header = c('Content-Type'='application/json', Accept='application/json')

options(RCurlOptions=list(followlocation=TRUE))

# case of single plan (single simulation)
postfields = toJSON(list(
    vpz=266,
    duration=6,
    begin=2453982.0,
    cond_wwdm.A=0.0064,
    cond_wwdm.Eb=1.86,
    mode=c("tree", "single", "dataframe"),
    # or mode=c("tree", "single", "matrix"),
    outselect="all",
    format="json"))

# case of linear plan (multiple simulation)
postfields = toJSON(list(
    vpz=266,
    duration=6,
    begin=2453982.0,
    cond_wwdm.A=c(0.0064,0.0065,0.0066),
    cond_wwdm.Eb=c(1.84,1.85,1.86),
    mode=c("tree", "linear", "dataframe"),
    # or mode=c("tree", "linear", "matrix"),
    outselect="all",
    format="json"))

res = postForm(uri="http://erecord.toulouse.inra.fr:8000/vpz/output/",
    .opts=list(postfields=postfields, httpheader=header))

responsedata = fromJSON(res)
#responsedata

#######################################################
# responsedata in case of 'tree' style of presentation
#######################################################

# id as VpzOutput
id = responsedata$id
cat("id :", id, "\n")

content_simulation_results_tree( res=responsedata$res,
                                 plan=responsedata$plan,
                                 restype=responsedata$restype)

