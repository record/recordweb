
cURL command line :

curl -X POST -d "username=alphateam&password=ahpla2017" http://erecord.toulouse.inra.fr:8000/acs/jwt/obtain/

=> Response :

{"token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Ind3ZG10ZWFtIiwidXNlcl9pZCI6MiwiZW1haWwiOiIiLCJleHAiOjE0OTg4NjY2NjV9.kQhtSlfY0NWIXnig7p5J-wdq0_p81m5FwQc7gLze6vE"}


Or cURL command line (with 'application/**json**' as 'Content-Type') :

curl -H "Content-Type: application/json" -d '{"username":"alphateam", "password":"ahpla2017"}' http://erecord.toulouse.inra.fr:8000/acs/jwt/obtain/

