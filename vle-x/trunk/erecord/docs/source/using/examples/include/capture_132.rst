.. _erecord_use_capture_132:

===========================================================
Example of :ref:`post_vpz_output` in command line with cURL
===========================================================

    .. include:: wwdm_curl_intro.rst

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**pars**',

  - '**tree**' as style of presentation,

  - value '**single**' as plan, or
    value '**linear**' in case of  multiple simulation
  - value '**dataframe**' as restype *(or 'matrix')*

  - value '**all**' as '**outselect**',

  - with 'application/**json**' as 'Content-Type'


:download:`REQUEST and RESPONSE in case of single plan (single simulation) <capture_request_132.png>`

:download:`REQUEST and RESPONSE in case of linear plan (multiple simulation) <capture_request2_132.png>`

    *Memo*

    .. literalinclude:: memo_request_132.rst

