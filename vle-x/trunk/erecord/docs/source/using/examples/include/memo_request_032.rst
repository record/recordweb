URL :
http://erecord.toulouse.inra.fr:8000/vpz/output/

Content (case of single plan (single simulation)) :
{"vpz":266,"duration":6,"begin":2453982.0,"pars":[{"selection_name":"cond_wwdm.A","cname":"cond_wwdm","pname":"A","value":0.0064},{"selection_name":"cond_wwdm.Eb","cname":"cond_wwdm","pname":"Eb","value":1.86}],"mode":["tree","single","dataframe"],"outselect":"all"}

Content (case of linear plan (multiple simulation)) :
{"vpz":266,"duration":6,"begin":2453982.0,"pars":[{"selection_name":"cond_wwdm.A","cname":"cond_wwdm","pname":"A","value":"[0.0064,0.0065,0.0066]"},{"selection_name":"cond_wwdm.Eb","cname":"cond_wwdm","pname":"Eb","value":"[1.84,1.85,1.86]"}],"mode":["tree","linear","dataframe"],"outselect":"all"}

(rq : or with "matrix" instead of "dataframe")

