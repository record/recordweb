.. _erecord_use_capture_023:

=================================================
Example of :ref:`post_vpz_output` in a webbrowser
=================================================

    .. include:: wwdm_wb_intro_postoutput.rst

Example illustrating :

  - modifying **duration**,
  - value '**single**' as plan
  - value '**matrix**' as restype
  - value '**all**' as '**outselect**',
    to select all output datas of all views.
  - with 'application/x-www-form-**urlencoded**' as 'Media type'

    *Memo*

    .. literalinclude:: memo_request_023.rst

