# R code :

library('RCurl')
library('rjson')

#######################
# request and response
#######################

header = c('Content-Type'='application/json', Accept='application/json')

options(RCurlOptions=list(followlocation=TRUE))

postfields = toJSON(list(
    vpz=266,
    duration=6,
    begin=2453982.0,
    cond_wwdm.A=0.0064,
    cond_wwdm.Eb=1.86,
    mode="tree",
    parselect="all",
    format="json"))

res = postForm(uri="http://erecord.toulouse.inra.fr:8000/vpz/input/",
    .opts=list(postfields=postfields, httpheader=header))

responsedata = fromJSON(res)
#responsedata

#######################################################
# responsedata in case of 'tree' style of presentation 
#######################################################

# id as VpzInput
id = responsedata$id
cat("id :", id, "\n")

content_simulation_inputs_tree(vpzinput=responsedata)

