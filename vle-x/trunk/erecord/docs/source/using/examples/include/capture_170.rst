.. _erecord_use_capture_170:

===============================================================================
Example of :ref:`get_vpz_experiment` and :ref:`post_vpz_experiment` in command line with cURL
===============================================================================

    .. include:: wwdm_curl_intro.rst

Example illustrating how to build and run one's own experiment plan based on
a chosen simulator, by filling the experiment conditions into a xls file
and receiving the simulation results into a xls file.

First step : :ref:`get_vpz_experiment` request 
----------------------------------------------

A :ref:`get_vpz_experiment` request is done to get the experiment conditions of the simulator in its original state.

.. literalinclude:: memo_request_170.rst

The received :download:`experiment.xls<experiment_get/experiment.xls>` file
contains the experiment conditions of the simulator in its original state.

Second step : build one's own experiment plan into a xls file
-------------------------------------------------------------

Into the :download:`experiment.xls<experiment_get/experiment.xls>` file
received from the :ref:`get_vpz_experiment` request *(see 'first step' above)*,
or a copy of it, one can modify the experiment conditions as wanted : for
example, a modified
:download:`experiment.xls<experiment_post_in/experiment.xls>` file.

Third step : :ref:`post_vpz_experiment` request 
-----------------------------------------------

A :ref:`post_vpz_experiment` request is done to run the experiment plan
defined into the built
:download:`experiment.xls<experiment_post_in/experiment.xls>` file
*(see 'second step')*.

.. literalinclude:: memo_request_171.rst

The returned :download:`experiment.xls<experiment_post_out/experiment.xls>`
file contains the simulation results and experiment conditions.

A variant
+++++++++

A :ref:`post_vpz_experiment` request to **modify the input datas folder**
(by using datafolder and datafoldercopy)
and run the experiment plan defined into the built
:download:`experiment.xls<experiment_post_in_3/experiment.xls>` file.

.. literalinclude:: memo_request_datafolder.rst
.. literalinclude:: memo_request_171_3.rst

The returned file : :download:`experiment.xls<experiment_post_out_3/experiment.xls>`.

