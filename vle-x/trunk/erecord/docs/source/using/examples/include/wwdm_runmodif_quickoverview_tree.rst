
-> *Example* :
:ref:`In a webbrowser <erecord_use_capture_032>` |
:ref:`In command line with cURL <erecord_use_capture_132>` |
:ref:`In Python language <erecord_use_capture_232>` |
:ref:`In R language <erecord_use_capture_332>` |
:ref:`In PHP language <erecord_use_capture_432>` |
:ref:`In C++ language <erecord_use_capture_532>`

