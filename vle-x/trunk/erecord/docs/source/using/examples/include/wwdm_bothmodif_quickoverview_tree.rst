
-> *Example* :
:ref:`In a webbrowser <erecord_use_capture_030>` |
:ref:`In command line with cURL <erecord_use_capture_130>` |
:ref:`In Python language <erecord_use_capture_230>` |
:ref:`In R language <erecord_use_capture_330>` |
:ref:`In PHP language <erecord_use_capture_430>` |
:ref:`In C++ language <erecord_use_capture_530>`

