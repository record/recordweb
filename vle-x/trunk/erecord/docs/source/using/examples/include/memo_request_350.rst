# R code :

library('RCurl')
library('rjson')

##############################################
# All the models repositories in 'tree' style
##############################################
url = "http://erecord.toulouse.inra.fr:8000/db/rep/"
res = getForm(url, mode='tree')
vlerep_list = fromJSON(res)
for (vlerep in vlerep_list){
    content_rep_tree(vlerep)
}

################################################################################
# Among all the models repositories, looking for the model with name model_name
################################################################################
url = "http://erecord.toulouse.inra.fr:8000/db/rep/"
res = getForm(url, mode='tree')
vlerep_list = fromJSON(res)
for (model_name in c("2CV", "wwdm")){
    for (vlerep in vlerep_list){
        vlerep_vlepkg_list = vlerep$vlepkg_list
        for (vlepkg in vlerep_vlepkg_list){
            vlepkg_id = vlepkg$id
            vlepkg_name = vlepkg$name
            if (vlepkg_name == model_name){
            #################################################################
            # New request for the model with name model_name in 'tree' style
            #################################################################
                cat("\n\nMore about the model with name ", model_name, "\n")
                url = paste0("http://erecord.toulouse.inra.fr:8000/db/pkg/", vlepkg_id,"/")
                res = getForm(url, mode='tree')
                vlepkg = fromJSON(res)
                content_pkg_tree(vlepkg)
            }
        }
    }
}

#####################################################################
# All the simulators of the model with id=19 ("2CV") in 'tree' style
#####################################################################
vlepkg_id = 19
url = "http://erecord.toulouse.inra.fr:8000/db/vpz/"
res = getForm(url, mode='tree', pkg=vlepkg_id)
vlevpz_list = fromJSON(res)
cat("\nSimulators of the model with id=", vlepkg_id, " :\n")
for (vlevpz in vlevpz_list){
    content_vpz_tree(vlevpz)
}

##################################
# domino requests in 'link' style
##################################
# (models repositories => theirs models => their simulators)
url = "http://erecord.toulouse.inra.fr:8000/db/rep/"
res = getForm(url, mode='link')
vlerep_list = fromJSON(res)
for (vlerep in vlerep_list){
    #print(vlerep)
    vlerep_id = vlerep$id
    vlerep_name = vlerep$name
    vlerep_verbose_name = vlerep$verbose_name
    vlerep_path = vlerep$path
    vlepkg_url_list = vlerep$vlepkg_list
    cat("=> models repository ", vlerep_id, vlerep_name, vlerep_verbose_name, vlerep_path, vlepkg_url_list, "\n")
    for (vlepkg_url in vlepkg_url_list){
        res = getForm(vlepkg_url, mode='link')
        vlepkg = fromJSON(res)
        #print(vlepkg)
        vlepkg_id = vlepkg$id
        vlepkg_name = vlepkg$name
        vlepkg_verbose_name = vlepkg$verbose_name
        vlerep_url = vlepkg$vlerep
        vlevpz_url_list = vlepkg$vlevpz_list
        cat("   => model ",  vlepkg_id, vlepkg_name, vlepkg_verbose_name, vlerep_url, vlevpz_url_list, "\n")
        for (vlevpz_url in vlevpz_url_list){
            res = getForm(vlevpz_url, mode='link')
            vlevpz = fromJSON(res)
            #print(vlevpz)
            vlevpz_id = vlevpz$id
            vlevpz_name = vlevpz$name
            vlevpz_verbose_name = vlevpz$verbose_name
            vlevpz_vlepkg_url = vlevpz$vlepkg
            cat("      => simulator ", vlevpz_id, vlevpz_name, vlevpz_verbose_name, vlevpz_vlepkg_url, "\n")
        }
    }
}

