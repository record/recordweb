.. _erecord_use_capture_550:

========================================================================
Example of some :ref:`'db' resources <webapi_db>` calls in C++ language
========================================================================

*... under construction ...*

The following resources are directly used in this example or could be used in a similar way :
  - :ref:`get_db_rep`, :ref:`get_db_rep_id`
  - :ref:`get_db_pkg`, :ref:`get_db_pkg_id`
  - :ref:`get_db_vpz`, :ref:`get_db_vpz_id`

