* 'recordb' models repository into the erecord databases :

    ...
    {
        "id": 2, 
        "name": "recordb", 
        "verbose_name": "recordb (models)", 
        "path": "/opt/erecord/repositories/recordb", 
        "vlepkg_list": [
            {
                "id": 4, 
                "name": "CallForOptim", 
                "verbose_name": "'CallForOptim' (from 'recordb')", 
                "vlerep": {
                    "id": 2, 
                    "name": "recordb", 
                    "verbose_name": "recordb (models)", 
                    "path": "/opt/erecord/repositories/recordb"
                }
            }, 
            {
                "id": 5, 
                "name": "meteo", 
                "verbose_name": "'meteo' (from 'recordb')", 
                "vlerep": { ... }
            }, 

            ...

            {
                "id": 41, 
                "name": "wwdm", 
                "verbose_name": "Winter Wheat Dry Matter (from 'recordb')", 
                "vlerep": { ... }
            }, 
            {
                "id": 45, 
                "name": "GenCSVcan", 
                "verbose_name": "'GenCSVcan' (from 'recordb')", 
                "vlerep": { ... }
            }
        ]
    }, 
    ...

