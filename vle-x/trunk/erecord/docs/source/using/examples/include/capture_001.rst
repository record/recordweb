.. _erecord_use_capture_001:

================================================
Example of :ref:`post_vpz_input` in a webbrowser
================================================

    .. include:: wwdm_wb_intro_postinput.rst

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',
  - with 'application/x-www-form-**urlencoded**' as 'Media type'

:download:`REQUEST <capture_request_001.png>`

    *Memo*

    .. literalinclude:: memo_request_001.rst

