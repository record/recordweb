.. _erecord_use_capture_430:

================================================
Example of :ref:`post_vpz_inout` in PHP language
================================================

    .. include:: wwdm_php_intro.rst

Example illustrating :

  - modifying **begin**,
  - modifying **duration**,
  - modifying some parameters by '**cname.pname**',

  - '**tree**' as style of presentation,
  - '**json**' as format,

  - values of type *cname.pname* as '**parselect**' :

    value 'cond_wwdm.A' to select the parameter named 'A' of the condition
    named 'cond_wwdm'.

    value 'cond_wwdm.B' to select the parameter named 'B' of the condition
    named 'cond_wwdm'.

    value 'cond_wwdm.Eb' to select the parameter named 'Eb' of the condition
    named 'cond_wwdm'.

    value 'cond_wwdm.TI' to select the parameter named 'TI' of the condition
    named 'cond_wwdm'.


  - value '**single**' as plan
  - value '**dataframe**' as restype

  - value of type *vname.oname* as '**outselect**' :

    value 'view.top:wwdm.LAI' to select the 'LAI' output data of the view
    named 'view'.

    value 'view.top:wwdm.ST' to select the 'ST' output data of the view
    named 'view'.

  - with 'application/**json**' as 'Content-Type'

    *Memo*

    .. literalinclude:: memo_request_400.rst
    .. literalinclude:: memo_request_402_tree__version430.rst
    .. literalinclude:: memo_request_401_tree__version430.rst
    .. literalinclude:: memo_request_430.rst

.. NB: memo_request_402_tree.rst replaced (temporary) by xxx__version430.rst
.. NB: memo_request_401_tree.rst replaced (temporary) by xxx__version430.rst

