.. _erecord_use_capture_008:

================================================
Example of :ref:`post_vpz_input` in a webbrowser
================================================

    .. include:: wwdm_wb_intro_postinput.rst

Example illustrating :

  - modifying some parameters by '**cname.pname**',
  - value of type *condname* as '**parselect**' :

    value 'cond_wwdm' to select all parameters of the condition named
    'cond_wwdm'.

  - value of type *cname.pname* as '**parselect**' :

    value 'cond_meteo.meteo_file' to select the parameter named 'meteo_file'
    of the condition named 'cond_meteo'.

  - with 'application/x-www-form-**urlencoded**' as 'Media type'

:download:`REQUEST <capture_request_008.png>`

    *Memo*

    .. literalinclude:: memo_request_008.rst

