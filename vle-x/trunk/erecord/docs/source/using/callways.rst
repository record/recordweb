.. _erecord_callways:

=================================
Ways how to call the web services
=================================

Overview
========

As RESTful web services, the :term:`erecord` web services are based on the HTTP protocol (a call takes the form of an HTTP request). As a consequence, there are different ways how to call the :term:`erecord` web services : they can be called from 'anything' that can send an HTTP request.

An `illustration <http://147.100.179.168/docs/erecord/erecord.pdf>`_ *(fr)*

The :term:`erecord` web services can be called :

- from **software programs** written **in different languages** (any language supporting the HTTP protocol, such as for example Python, R, C++, Java, Php...).
- from **command line tools** such as for example **cURL** that is  a command-line tool for transferring data using various protocols among which the HTTP one. 
- from a **webbrowser**

In addition a **Web User Interface** has been developped to make easier calling some erecord web services : see :ref:`webui`.

See also : `Using an agronomic model developped with the Record platform and installed into the erecord web services <http://147.100.179.168/docs/erecord/model_user.pdf>`_ *(fr) (Utiliser un modèle agronomique développé sous la plateforme Record et disponible dans les services web erecord)*.

Calling the web services from programs
======================================

Calling the web services from a Python program
----------------------------------------------

    See examples in :ref:`erecord_examples`, for example
    :ref:`this one <erecord_use_capture_232>`.

Calling the web services from a R program
-----------------------------------------

    See examples in :ref:`erecord_examples`, for example
    :ref:`this one <erecord_use_capture_332>`.

Calling the web services from a PHP program
-------------------------------------------

    See examples in :ref:`erecord_examples`, for example
    :ref:`this one <erecord_use_capture_432>`.

Calling the web services from a C++ program
-------------------------------------------

    :ref:`soon <tmp_under_construction>`

Using the web services by cURL command line tool
================================================

    See examples in :ref:`erecord_examples`, for example
    :ref:`this one <erecord_use_capture_132>`.

Using the web services by webbrowser
====================================

Build request into a webbrowser
-------------------------------

    You can write your request into a webbrowser (the request and its
    parameters) :

    See examples in :ref:`erecord_examples`, for example
    :ref:`this one <erecord_use_capture_032>`.

Call the Web User Interface
---------------------------

    You can call the :ref:`webui`.

Customize html home pages
-------------------------

    You can build a html home page dedicated to the model you are interested
    in, and customize it : choose which parameters to show, add some
    information (units...), control the filled in values...

    Example : `a WWDM home page <http://147.100.179.168/docs/models/wwdm/accueil_wwdm.html>`_ *(fr)* (for more see : :ref:`html home pages for the WWDM model <wwdm_homepage>`).

    Such home pages may already exist for some models, for example if the
    model owner has given one when he has delivered its model to erecord).
    Examples : `a WWDM home page <http://147.100.179.168/docs/models/wwdm/accueil_wwdm.html>`_ *(fr)*,  `a recordschool home page <http://147.100.179.168/docs/models/recordschool/accueil_recordschool.html>`_ *(fr)*.

    You can modify an existing home page as you want according to your own
    case, then keep it for yourself or share it, that is to say send it to
    some colleagues.

