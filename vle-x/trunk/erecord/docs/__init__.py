# -*- coding: utf-8 -*-
"""docs

*This package is part of erecord - Record platform web development*

:copyright: Copyright (C) 2018-2022 INRA http://www.inra.fr.
:license: GPLv3, see LICENSE for more details.
:authors: see AUTHORS.

Documentation of the erecord project.

erecord’s documentation uses the Sphinx documentation system.

Contents
--------

- source : the source documentation (.rst files...).
- build : the generated documentation in different formats (html...).
- Makefile : commands to generate the documentation in different formats into
  build, from source (make html, make pdf...).

Particular case of the auto generated source code documentation
---------------------------------------------------------------

The source code documentation is auto generated from source code (.py files) 
into : source/ref/autogen.

Into Makefile : command to auto generate source code documentation (make ref).

source/ref/autogen must not be manually modified (manual modifications will be
lost by the next auto generation command).

See :ref:`index`

"""

