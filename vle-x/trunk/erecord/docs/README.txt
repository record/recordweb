erecord/docs

* documentation generation commands :

  - clean :
    make clean
    make cleanref

  - gen :
    make ref
    make html
    make pdf

* some other documentation generation commands :

  - clean :
    make cleanvpzpages

  - gen :

    - for a user (username, password) who is authorized for every simulator
      in limited access, generate a token :

        make username=... password=... token_value

    - generate pages :

        make genvpzpages

    - clean token :

        rm -f token_value

