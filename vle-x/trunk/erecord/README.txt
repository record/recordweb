=======================================================
erecord, record platform web development for vle models
=======================================================

The erecord project is dedicated to the Record platform web development for
vle models of several vle versions (vle-1.1, vle-2...).

Content hierarchy : docs, repositories, erecord (the erecord package),
databases, factory...

See __init__.py files, docs directory.

